cmake_minimum_required(VERSION 2.8)

project(XdevLSDK)

#set(CMAKE_DEBUG_POSTFIX d)

#
# Set the main variables that holds project folders
#

option(XDEVL_BUILD_MODULAR "Builds the Network plugin" On)
set(XDEVL_BUILD_STATIC Off)

set(CMAKE_MODULE_PATH "${XdevLSDK_SOURCE_DIR}/cmake")
set(XDEVL_SDK ${XdevLSDK_SOURCE_DIR})
set(XDEVL_HOME ${XDEVL_SDK}/XdevL)
set(XDEVL_OUTPUT_DIR ${XDEVL_HOME}/lib)

set(XDEVL_PLUGINS ${XDEVL_SDK}/XdevLPlugins)
set(XDEVL_PLUGINS_OUTPUT_DIR ${XDEVL_PLUGINS}/lib)
set(XDEVL_DEMOS ${XDEVL_SDK}/XdevLDemos)
set(XDEVL_TOOLS ${XDEVL_SDK}/XdevLUtils)
set(XDEVL_EXTERNALS ${XDEVL_SDK}/XdevLExternals)
set(CMAKE_CODELITE_USE_TARGETS OFF)

#
# Where shall we install the libs, headers and plugins?
#

set(XDEVL_INCLUDE_INSTALL_PATH "/usr/include/XdevL")
set(XDEVL_LIB_INSTALL_PATH "/usr/lib")
set(XDEVL_PLUGINS_INCLUDE_INSTALL_PATH "/usr/include/XdevLPlugins")
set(XDEVL_PLUGINS_INSTALL_PATH "/usr/lib/XdevLPlugins")
set(XDEVL_SHARED_LIBRARY_FILE_PREFIX "")
set(XDEVL_STATIC_LIBRARY_FILE_PREFIX "")


#
# Set the XdevL version
#

set(XDEVLSDK_MAJOR_VERSION 0)
set(XDEVLSDK_MINOR_VERSION 7)
set(XDEVLSDK_PATCH_VERSION 0)
set(XDEVLSDK_VERSION "${XDEVLSDK_MAJOR_VERSION}.${XDEVLSDK_MINOR_VERSION}.${XDEVLSDK_MICRO_VERSION}")

#
# Check for 64 or 32 bit
#

set(SIZEOF_VOIDP ${CMAKE_SIZEOF_VOID_P})
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
  set(ARCH_64 TRUE)
  set(PROCESSOR_ARCH "x64")
else()
  set(ARCH_64 FALSE)
  set(PROCESSOR_ARCH "x86")
endif()


#
# Check compiler
#

if(CMAKE_COMPILER_IS_GNUCC)
  set(COMPILER_GCC TRUE)
  message(STATUS "Using gcc compiler.")
elseif(CMAKE_C_COMPILER_ID MATCHES "Clang")
  set(COMPILER_CLANG TRUE)
  message(STATUS "Using clang compiler.")
elseif(MSVC_VERSION GREATER 1400) # VisualStudio 8.0+
  set(COMPILER_VISUAL_STUDIO TRUE)
  message(STATUS "Using VS compiler.")
endif()

set_property(GLOBAL PROPERTY CXX_STANDARD 14)

#
# Check for platform
#
set(PLATFORM_WINDOWS FALSE)
set(PLATFORM_MINGW FALSE)
set(PLATFORM_LINUX FALSE)
set(PLATFORM_DARWIN FALSE)
set(PLATFORM_MACOSX FALSE)

if(MINGW)
	set(PLATFORM_MINGW TRUE)
elseif(WIN32)
	set(PLATFORM_WINDOWS TRUE)
elseif(UNIX AND NOT APPLE)
	if(CMAKE_SYSTEM_NAME MATCHES ".*Linux")
		set(PLATFORM_LINUX TRUE)
	endif()
elseif(APPLE)
	if(CMAKE_SYSTEM_NAME MATCHES ".*Darwin.*")
		set(PLATFORM_DARWIN TRUE)
	elseif(CMAKE_SYSTEM_NAME MATCHES ".*MacOS.*")
		set(PLATFORM_MACOSX TRUE)
	endif()
endif()

if(XDEVL_BUILD_MODULAR)
	if(PLATFORM_ANDROID)
		set(XDEVL_SHARED_LIBRARY_FILE_EXTENSION ".so")
		set(XDEVL_AUDIO_SHARED_LIBRARY_FILE_EXTENSION ".so")
		set(XDEVL_MODEL_SHARED_LIBRARY_FILE_EXTENSION ".so")
		set(XDEVL_IMAGE_SHARED_LIBRARY_FILE_EXTENSION ".so")
		set(XDEVL_SHARED_LIBRARY_FILE_EXTENSION ${CMAKE_SHARED_LIBRARY_SUFFIX})
		set(XDEVL_SHARED_LIBRARY_FILE_PREFIX ${CMAKE_SHARED_LIBRARY_PREFIX})
		set(XDEVL_STATIC_LIBRARY_FILE_EXTENSION ".a")
		set(XDEVL_STATIC_LIBRARY_FILE_PREFIX "lib")
	else()
		set(XDEVL_SHARED_LIBRARY_FILE_EXTENSION ".xdp")
		set(XDEVL_AUDIO_SHARED_LIBRARY_FILE_EXTENSION ".xap")
		set(XDEVL_MODEL_SHARED_LIBRARY_FILE_EXTENSION ".xmp")
		set(XDEVL_IMAGE_SHARED_LIBRARY_FILE_EXTENSION ".xip")
		set(XDEVL_SHARED_LIBRARY_FILE_PREFIX "")
		if(PLATFORM_WINDOWS)
			set(XDEVL_STATIC_LIBRARY_FILE_EXTENSION ".lib")
			set(XDEVL_STATIC_LIBRARY_FILE_PREFIX "")	
		else()
			set(XDEVL_STATIC_LIBRARY_FILE_EXTENSION ".a")
			set(XDEVL_STATIC_LIBRARY_FILE_PREFIX "lib")
		endif()
	endif()
else(XDEVL_BUILD_MODULAR)
	set(XDEVL_SHARED_LIBRARY_FILE_EXTENSION ${CMAKE_SHARED_LIBRARY_SUFFIX})
	set(XDEVL_SHARED_LIBRARY_FILE_PREFIX ${CMAKE_SHARED_LIBRARY_PREFIX})
endif(XDEVL_BUILD_MODULAR)

message(STATUS ${CMAKE_SYSTEM_NAME})

# This increases the load times for dlopen. TODO Needs more testing.
if(COMPILER_GCC)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wl,--as-needed")
endif()

if(COMPILER_GCC OR COMPILER_CLANG)
	if(PLATFORM_ANDROID)
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g -pipe -Werror -W -Wall -Wno-missing-field-initializers -Wno-unused-parameter -Wextra")
		set(CMAKE_CXX_STANDARD 11 )
	else()
		if(USE_LIBCXX AND COMPILER_CLANG)
			set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -stdlib=libc++")
			set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
		endif()

		if(PLATFORM_UNIX)
			set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g -pipe -Werror -W -Wall -Wno-implicit-fallthrough -Wno-ignored-qualifiers -Wno-missing-field-initializers -Wno-unused-parameter -fsigned-char -Wsequence-point")
			set(CMAKE_CXX_STANDARD 11 )
		else()
		message(STATUS "MinGW BUILD PROCESS")
			set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g -pipe -Werror -W -Wall -Wno-implicit-fallthrough -Wno-ignored-qualifiers -Wno-missing-field-initializers -Wno-unused-parameter -fsigned-char -Wsequence-point")
			set(CMAKE_CXX_STANDARD 11 )
		endif()
	endif()
elseif(COMPILER_VISUAL_STUDIO)
	# Visual Studio has issues with strcpy.
	add_definitions(-D_CRT_SECURE_NO_WARNINGS=1)
endif()

#
# TODO Do it the cmake way. This is a hack.
# Set external libraries

if(APPLE)
	set(CMAKE_MACOSX_RPATH OFF)
endif(APPLE)

#
# Build for Android

if(BUILD_ANDROID_TOOLCHAIN)

  # specify the cross compiler
  if(APPLE)
	set(ANDROID_SYSTEM darwin-x86_64)
  elseif(UNIX AND NOT APPLE)
	set(ANDROID_SYSTEM linux-x86_64)
  endif()

  set(NDK_ROOT $ENV{NDKROOT})
  set(ANDROID_TOOLCHAIN_FOLDER ${XDEVL_EXTERNALS}/android/${ANDROID_ARCH})

  if(NOT IS_DIRECTORY "${ANDROID_TOOLCHAIN_FOLDER}")
	execute_process(COMMAND mkdir -p ${XDEVL_EXTERNALS}/android)
	execute_process(COMMAND ${NDK_ROOT}/build/tools/make-standalone-toolchain.sh --arch=${ANDROID_ARCH} --ndk-dir=${NDK_ROOT} --install-dir=${ANDROID_TOOLCHAIN_FOLDER})
  endif()

endif()

#
# Select plugins to compile.
#

option(BUILD_NETWORK "Builds the Network plugin" ON)
option(BUILD_KEYBOARD "Builds the Keyboard plugin" ON)
option(BUILD_MOUSE "Builds the Mouse plugin" ON)
option(BUILD_JOYSTICK "Builds the Joystick plugin" ON)
option(BUILD_SERIAL "Builds the serial port plugin" ON)
option(BUILD_BLUETOOTH "Builds the bluetooth plugin" ON)
option(BUILD_FTDI "Builds the FTDI Serial Port plugin" OFF)
option(BUILD_AUDIO "Build audio plugin" ON)
option(BUILD_NATIVE_AUDIO "Builds the audio plugin" OFF)
option(BUILD_OPENGL "Builds the OpenGL plugin" ON)
option(BUILD_VULKAN "Builds the Vulkan implementation of XdevLRAI" OFF)
option(BUILD_FONT "Builds the Font plugin" ON)
option(BUILD_FONTGENERATOR "Build the Font Generator." OFF)
option(BUILD_SDL2 "Builds the SDL plugin" OFF)
option(BUILD_ANDROID "Builds the Android plugin" OFF)
option(BUILD_OGRE "Builds the ogre plugin" OFF)
option(BUILD_GL_CONTEXT "Builds the new OpenGL context plugin" ON)
option(BUILD_WAYLAND "Builds the Wayland plugin" OFF)
option(BUILD_MOUSE_SERVER "Builds the raw mouse server" OFF)
option(BUILD_JOYSTICK_SERVER "Builds the raw joystick server" ON)
option(BUILD_FILESYSTEM "Builds the Directory module" ON)
option(BUILD_NATIVE_WINDOWS "Build native window support" ON)
option(BUILD_DEMOS "Builds all demos" OFF)
option(BUILD_EGL "Builds the EGL OpenGL context" OFF)
option(BUILD_COMPUTE_DEVICE "Build the OpenCL plugins" OFF)
option(BUILD_UI "Build the UI plugins" ON)
option(BUILD_VR "Build the VR plugins" OFF)
option(BUILD_NUKLEAR "Build the Nuklear UI plugin" OFF)
option(BUILD_IMGUI "Build the ImGUI UI plugin" OFF)
option(BUILD_MODEL_IMPORT "Build the import model plugin" OFF)
option(BUILD_INPUT_ACTION_SYSTEM "Build the input action plugin" ON)

if(NOT PLATFORM_LINUX)
	set(BUILD_X11 OFF)
	set(BUILD_WAYLAND OFF)
	set(BUILD_X11 OFF)
	set(BUILD_MOUSE_SERVER OFF)
	set(BUILD_FTDI OFF)
endif()

if(PLATFORM_ANDROID)
	set(BUILD_X11 OFF)
	set(BUILD_WAYLAND OFF)
	set(BUILD_MOUSE_SERVER OFF)
	set(BUILD_FTDI OFF)
	set(BUILD_OGRE OFF)
	set(BUILD_MOUSE_SERVER OFF)
	set(BUILD_NATIVE_AUDIO OFF)
	set(BUILD_SERIAL OFF)
endif()

#
# This definition we need for the XdevL library

if(CMAKE_BUILD_TYPE MATCHES Debug)
	add_definitions(-D_LOG -DXDEVL_DEBUG -DXDEVL_SHARED_LIBRARY_FILE_EXTENSION -DXDEVL_PROFILER)
else()
	add_definitions(-D_LOG -DXDEVL_SHARED_LIBRARY_FILE_EXTENSION)
endif()


if(PLATFORM_ANDROID)
  #
  # Set the output directories

  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})

  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})

  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${XDEVL_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})

	set(EXECUTABLE_OUTPUT_PATH  ${CMAKE_SOURCE_DIR}/bin)
	set(EXECUTABLE_OUTPUT_PATH_DEBUG ${CMAKE_SOURCE_DIR}/bin)
	set(EXECUTABLE_OUTPUT_PATH_RELEASE ${CMAKE_SOURCE_DIR}/bin)

  add_subdirectory(XdevL)

  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})

  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})

  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${XDEVL_PLUGINS_OUTPUT_DIR}/${PLATFORM_ANDROID_ARCH})

	set(EXECUTABLE_OUTPUT_PATH  ${CMAKE_SOURCE_DIR}/bin)
	set(EXECUTABLE_OUTPUT_PATH_DEBUG ${CMAKE_SOURCE_DIR}/bin)
	set(EXECUTABLE_OUTPUT_PATH_RELEASE ${CMAKE_SOURCE_DIR}/bin)

  add_subdirectory(XdevLExternals/physfs)
  add_subdirectory(XdevLPlugins/XdevLCore)
  add_subdirectory(XdevLPlugins/XdevLInput/XdevLKeyboard)
  add_subdirectory(XdevLPlugins/XdevLInput/XdevLMouse)
  add_subdirectory(XdevLPlugins/XdevLInput/XdevLJoystick)
  add_subdirectory(XdevLPlugins/XdevLNetwork)
  add_subdirectory(XdevLPlugins/XdevLFileSystem)
  add_subdirectory(XdevLPlugins/XdevLWindow/XdevLWindowAndroid)
  add_subdirectory(XdevLPlugins/XdevLOpenGLContext/XdevLOpenGLContextEGL)
#  add_subdirectory(XdevLPlugins/XdevLVulkanContext/XdevLVulkanContext)
  add_subdirectory(XdevLPlugins/XdevLRAI/XdevLRAIGL)
  add_subdirectory(XdevLPlugins/XdevLFont)
  add_subdirectory(XdevLPlugins/XdevLUI/XdevLUIImGUI)
  add_subdirectory(XdevLPlugins/XdevLAudio/XdevLAudioOpenAL)
  add_subdirectory(XdevLPlugins/XdevLImage)
  add_subdirectory(XdevLPlugins/XdevLAudio/XdevLAudioImportPlugins/XdevLAudioImportPluginWAV)
  add_subdirectory(XdevLPlugins/XdevLImage/XdevLImageImportPlugins/XdevLImageImportPluginBMP)
else()
  #
  # Set the output directories

  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${XDEVL_OUTPUT_DIR})
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${XDEVL_OUTPUT_DIR})
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${XDEVL_OUTPUT_DIR})

  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${XDEVL_OUTPUT_DIR})
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${XDEVL_OUTPUT_DIR})
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${XDEVL_OUTPUT_DIR})

  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${XDEVL_OUTPUT_DIR})
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${XDEVL_OUTPUT_DIR})
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${XDEVL_OUTPUT_DIR})

  add_subdirectory(XdevL)

  #
  # Set the output directories

  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${XDEVL_PLUGINS_OUTPUT_DIR})
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${XDEVL_PLUGINS_OUTPUT_DIR})
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${XDEVL_PLUGINS_OUTPUT_DIR})

  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${XDEVL_PLUGINS_OUTPUT_DIR})
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${XDEVL_PLUGINS_OUTPUT_DIR})
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${XDEVL_PLUGINS_OUTPUT_DIR})

  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${XDEVL_PLUGINS_OUTPUT_DIR})
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${XDEVL_PLUGINS_OUTPUT_DIR})
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${XDEVL_PLUGINS_OUTPUT_DIR})

  add_subdirectory(XdevLPlugins/XdevLCore)
  add_subdirectory(XdevLExternals/physfs)

	if(BUILD_INPUT_ACTION_SYSTEM)
		add_subdirectory(XdevLPlugins/XdevLInput/XdevLInputActionSystem)
	endif()

  # Build the Keybaord plugin.
  if(BUILD_KEYBOARD)
	add_subdirectory(XdevLPlugins/XdevLInput/XdevLKeyboard)
  endif()

  # Build the Mouse plugin.
  if(BUILD_MOUSE)
	add_subdirectory(XdevLPlugins/XdevLInput/XdevLMouse)
  endif()

  # Build the Joystick plugin.
  if(BUILD_JOYSTICK)
	add_subdirectory(XdevLPlugins/XdevLInput/XdevLJoystick)
	if(WIN32)
	elseif(APPLE)
			add_subdirectory(XdevLPlugins/XdevLInput/XdevLJoystick/XdevLJoystickServerMac)
	elseif(UNIX)
		find_package(UDev)
		if(UDEV_FOUND)
			add_subdirectory(XdevLPlugins/XdevLInput/XdevLJoystick/XdevLJoystickServerLinux)
		else()
			message(STATUS "XdevLJoystickServerLinux and XdevLJoystick will not be build.")
		endif()
	endif()
  endif()

  # Build the Network plugin.
  if(BUILD_NETWORK)
	add_subdirectory(XdevLPlugins/XdevLNetwork)
  endif()

  # Build the Directory plugin.
  if(BUILD_FILESYSTEM)
	add_subdirectory(XdevLPlugins/XdevLFileSystem)
  endif()

  # Build the Serial Port Communication plugin.
  if(BUILD_SERIAL)
  	add_subdirectory(XdevLPlugins/XdevLSerial)
  endif()

  # Build the Serial Port Communication plugin.
	if(BUILD_IMGUI)
		add_subdirectory(XdevLPlugins/XdevLUI/XdevLUIImGUI)
	endif()

	if(BUILD_NUKLEAR)
		add_subdirectory(XdevLPlugins/XdevLUI/XdevLUINuklear)
	endif()

	add_subdirectory(XdevLPlugins/XdevLUI/XdevLUIRAI)


  # Build the Bluetooth Port Communication plugin.
  if(BUILD_BLUETOOTH)
	find_package(Bluetooth)
	if(Bluetooth_FOUND)
		add_subdirectory(XdevLPlugins/XdevLBluetooth)
	else()
		message(STATUS "XdevLBluetooth will not be build.")
	endif()
  endif()

  add_subdirectory(XdevLPlugins/XdevLBlankPlugin)

	# Build the Audio plugin.
	if(BUILD_AUDIO)

		find_package(PulseAudio)
		if(PULSEAUDIO_FOUND)
			add_subdirectory(XdevLPlugins/XdevLAudio/XdevLAudioPulseAudio)
		else()
			message(WARNING "XdevLAudioPulseAudio will not be build.")
		endif()

		if(BUILD_NATIVE_AUDIO)
			if(WIN32)
				# TODO Add here window audio handling
			elseif(APPLE)
				add_subdirectory(XdevLPlugins/XdevLAudio/XdevLAudioCoreAudio)
			elseif(UNIX)
				find_package(ALSA)
				if(ALSA_FOUND)
					add_subdirectory(XdevLPlugins/XdevLAudio/XdevLAudioAlsa)
				else()
					message(WARNING "XdevLAudioAlsa will not be build.")
				endif()
			endif(WIN32)
		else()
			find_package(OpenAL)
			if(OPENAL_FOUND)
				add_subdirectory(XdevLPlugins/XdevLAudio/XdevLAudioOpenAL)
			else()
				message(STATUS "XdevLAudioOpenAL will not be build.")
			endif()
		endif()
		add_subdirectory(XdevLPlugins/XdevLAudio/XdevLAudioImportPlugins/XdevLAudioImportPluginWAV)
	endif(BUILD_AUDIO)

	if(BUILD_VULKAN)
		add_subdirectory(${XDEVL_EXTERNALS}/glslang)
		add_subdirectory(XdevLPlugins/XdevLVulkanContext/XdevLVulkanContext)
		add_subdirectory(XdevLPlugins/XdevLRAI/XdevLRAIVulkan)
	endif(BUILD_VULKAN)

	# Build the OpenGL plugin.
	if(BUILD_OPENGL)
		find_package(OpenGL)
		if(OPENGL_FOUND)
			add_subdirectory(XdevLPlugins/XdevLRAI/XdevLRAIGL)
		else()
			message(STATUS "XdevLRAIGL will not be build.")
		endif()
	endif()

  # Build the Font plugin.
  if(BUILD_FONT)
	add_subdirectory(XdevLPlugins/XdevLFont)
  endif()

  if(BUILD_FONTGENERATOR)
	add_subdirectory(XdevLPlugins/XdevLFontGenerator/TrueTypeFont)
  endif()

	# Build the SDL plugin.
	if(BUILD_SDL2)
		add_subdirectory(XdevLPlugins/XdevLWindow/XdevLWindowSDL)
	endif()

  # Build Android native window plugin.
  if(BUILD_ANDROID)
  	add_subdirectory(XdevLPlugins/XdevLWindow/XdevLWindowAndroid)
  endif()

  #Build native window plugins.
  if(BUILD_NATIVE_WINDOWS)
	if(WIN32)
		add_subdirectory(XdevLPlugins/XdevLWindow/XdevLWindowWindows)
	elseif(APPLE)
		add_subdirectory(XdevLPlugins/XdevLWindow/XdevLWindowCocoa)
	elseif(UNIX)
		find_package(Xrandr)
		if(Xrandr_FOUND)
			add_subdirectory(XdevLPlugins/XdevLWindow/XdevLWindowX11)
		else()
			message(WARNING "XdevLWindowX11 will not be build.")
		endif()

			if(BUILD_WAYLAND)
				find_package(Wayland)
				if(WAYLAND_FOUND)
					add_subdirectory(XdevLPlugins/XdevLWindow/XdevLWindowWayland)
				else()
					message(STATUS "XdevLWindowWayland will not be build.")
				endif(WAYLAND_FOUND)
			endif(BUILD_WAYLAND)
	endif(WIN32)
  endif(BUILD_NATIVE_WINDOWS)

  # Build the ogre plugin.
  if(BUILD_OGRE)
	find_package(OGRE)
	find_package(Boost COMPONENTS system)

	if(OGRE_FOUND AND Boost_FOUND)
		add_subdirectory(XdevLPlugins/XdevLOgre)
	else()
		message(STATUS "XdevLOgre will not be build.")
	endif()
  endif()

  # Build the FTDI Serial Port plugin.
  if(BUILD_FTDI)
	add_subdirectory(XdevLPlugins/XdevLFTDI)
  endif()

  # Build Compute device plugin.
  if(BUILD_COMPUTE_DEVICE)
	find_package(OpenCL)
	if(OPENCL_FOUND)
		message(STATUS "XdevLComputeDeviceCL will be build.")
		add_subdirectory(XdevLPlugins/XdevLComputeDevice/XdevLComputeDeviceCL)
	else()
		message(STATUS "XdevLComputeDeviceCL will not be build.")
	endif()
  endif()

	if(BUILD_VR)
		add_subdirectory(XdevLPlugins/XdevLVR/XdevLVRSteamVR)
	endif()


  # Build the Mouse Server plugins for raw mouse events.
  if(BUILD_MOUSE_SERVER)
	if(PLATFORM_WINDOWS)
	elseif(PLATFORM_LINUX)
		add_subdirectory(XdevLPlugins/XdevLInput/XdevLMouse/XdevLMouseServerLinux)
	elseif(PLATFORM_MACOSX)
	endif()
  endif()

  # Build the new OpenGL Contect plugin.
  if(BUILD_GL_CONTEXT)

	if(BUILD_EGL)
		find_package(EGL)
		if(EGL_FOUND)
			add_subdirectory(XdevLPlugins/XdevLOpenGLContext/XdevLOpenGLContextEGL)
		endif()
	endif()

	if(OPENGL_FOUND)
		find_package(SDL2)
		if(SDL2_FOUND)
			add_subdirectory(XdevLPlugins/XdevLOpenGLContext/XdevLOpenGLContextSDL)
		else()
			message(STATUS "XdevLOpenGLContextSDL will not be build.")
		endif()

		if(WIN32)
			add_subdirectory(XdevLPlugins/XdevLOpenGLContext/XdevLOpenGLContextWGL)
		elseif(APPLE)
			add_subdirectory(XdevLPlugins/XdevLOpenGLContext/XdevLOpenGLContextCocoa)
		elseif(UNIX)
			add_subdirectory(XdevLPlugins/XdevLOpenGLContext/XdevLOpenGLContextGLX)
		endif()
	else()
		message(STATUS "XdevLOpenGLContextSDL, XdevLOpenGLContextWGL, XdevLOpenGLContextCocoa or XdevLOpenGLContextGLX will not be build.")
	endif()
  endif()

	add_subdirectory(XdevLPlugins/XdevLParticles)
  add_subdirectory(XdevLUtils/XdevLCodeLiteGenerator)
	add_subdirectory(XdevLPlugins/XdevLImage)
	
	if(BUILD_MODEL_IMPORT)
	  add_subdirectory(XdevLPlugins/XdevLModel)
	endif()
	
	add_subdirectory(XdevLPlugins/XdevLGraphics)



endif()
if(BUILD_DEMOS)
	add_subdirectory(XdevLDemos)
endif()


# All packaging stuff

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Cengiz Terzibas") #required
set(CPACK_DEBIAN_PACKAGE_SECTION "devel")
set(CPACK_DEBIAN_PACKAGE_RECOMMENDS   "build-essential, git, cmake, gdb, xterm, gcc, g++")
if( ${PROCESSOR_ARCH} EQUAL x86 )
	message("CPACK_DEBIAN_PACKAGE_ARCHITECTURE i386")
	set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "i386")
else ()
	message("CPACK_DEBIAN_PACKAGE_ARCHITECTURE amd64")
	set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "amd64")
endif ()

set(CPACK_PACKAGE_VENDOR "Cengiz Terzibas")
set(CPACK_PACKAGE_VERSION ${XDEVLSDK_VERSION})
set(CPACK_PACKAGE_VERSION_MAJOR ${XDEVLSDK_MAJOR_VERSION})
set(CPACK_PACKAGE_VERSION_MINOR ${XDEVLSDK_MINOR_VERSION})
set(CPACK_PACKAGE_VERSION_PATCH ${XDEVLSDK_PATCH_VERSION})
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "The XdevLSDK framework.")

include(CPack)
