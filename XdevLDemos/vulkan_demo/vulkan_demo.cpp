/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLVulkanContext/XdevLVulkanContext.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>

#include <tm/tm.h>

xdl::IPXdevLCore core = nullptr;
xdl::IPXdevLWindow window = nullptr;
xdl::IPXdevLKeyboard keyboard = nullptr;
xdl::IPXdevLButton esc = nullptr;
xdl::IPXdevLVulkanContext vulkan = nullptr;
xdl::IPXdevLVulkanInstance vulkan_instance = nullptr;
xdl::IPXdevLVulkanDevice vulkan_device = nullptr;
xdl::IPXdevLVulkanSurface vulkan_surface = nullptr;
xdl::IPXdevLVulkanSwapChain vulkan_swapchain = nullptr;

xdl::xdl_bool running = xdl::xdl_true;

//
// Vulkan native handles.
//
VkInstance instance = VK_NULL_HANDLE;
VkDevice device = VK_NULL_HANDLE;
VkSurfaceKHR surface = VK_NULL_HANDLE;
VkSwapchainKHR swapchain = VK_NULL_HANDLE;
VkRenderPass renderpass = VK_NULL_HANDLE;
std::vector<VkFramebuffer> framebuffers {VK_NULL_HANDLE};

VkPipelineCache pipelineCache = VK_NULL_HANDLE;
VkPipeline graphicsPipeline = VK_NULL_HANDLE;

VkDescriptorSetLayout descriptorSetLayout	= VK_NULL_HANDLE;
VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

VkQueue graphicsQueue = VK_NULL_HANDLE;
VkQueue presentQueue = VK_NULL_HANDLE;
VkCommandPool commandPool = VK_NULL_HANDLE;
std::vector<VkCommandBuffer> drawCommandBuffers	{VK_NULL_HANDLE};
std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages {};

struct Vertex {
	xdl::xdl_float position[3];
	xdl::xdl_float color[4];
};

struct VertexBufferObject {
	VkDeviceMemory memory;
	VkBuffer buffer;
};

struct IndexBufferObject {
	VkDeviceMemory memory;
	VkBuffer buffer;
	uint32_t count;
};

struct UniformBufferObject {
	VkBuffer buffer;
	VkDeviceMemory memory;
	VkDescriptorBufferInfo descriptor;
};

struct UniformBufferForVertexShader {
	tmath::mat4 projectionMatrix;
	tmath::mat4 modelMatrix;
	tmath::mat4 viewMatrix;
};

struct DepthStencilObject {
	VkImage image;
	VkDeviceMemory mem;
	VkImageView view;
};

UniformBufferForVertexShader uboVS;
VertexBufferObject vertices;
IndexBufferObject indices;
UniformBufferObject uniformDataVS;
DepthStencilObject depthStencil;


VkShaderModule loadSPIRVShader(const xdl::XdevLFileName filename) {

	std::ifstream is(filename.toString().c_str(), std::ios::binary | std::ios::in | std::ios::ate);
	if (is.is_open()) {

		//
		// Get the size of the binary file.
		//
		auto shaderSize = is.tellg();
		is.seekg(0, std::ios::beg);
		auto shaderCode = new xdl::xdl_char[shaderSize];
		is.read(shaderCode, shaderSize);
		is.close();

		if(0 == shaderSize) {
			XDEVL_MODULEX_ERROR(loadSPIRVShader, "When trying to load the SPRIV shader it seems like the size is = 0.");
			return VK_NULL_HANDLE;
		}

		//
		// Create the Vulkan Shader Module.
		//
		VkShaderModuleCreateInfo moduleCreateInfo {};
		moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		moduleCreateInfo.codeSize = shaderSize;
		moduleCreateInfo.pCode = (uint32_t*)shaderCode;

		VkShaderModule shaderModule;
		VK_CHECK_RESULT(vkCreateShaderModule(device, &moduleCreateInfo, NULL, &shaderModule));

		delete[] shaderCode;

		return shaderModule;
	}

	std::cerr << "Error: Could not open shader file \"" << filename << "\"" << std::endl;
	return VK_NULL_HANDLE;

}


void setupCommandPool() {

	// -------------------------------------------------------------------------
	// Create command pool.
	//

	VkCommandPoolCreateInfo commandPoolCreateInfo {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	commandPoolCreateInfo.queueFamilyIndex = vulkan_device->getGraphicsFamilyQueueIndex();

	VK_CHECK_RESULT(vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool));

	// -------------------------------------------------------------------------

}

void setupPipelineCache() {

	// -------------------------------------------------------------------------
	// Create Pipeline
	//

	VkPipelineCacheCreateInfo pipelineCacheCreateInfo {};
	pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
	VK_CHECK_RESULT(vkCreatePipelineCache(device, &pipelineCacheCreateInfo, nullptr, &pipelineCache));

	// -------------------------------------------------------------------------
}

void setupDescriptorSetLayout() {

	// -------------------------------------------------------------------------
	// Create descriptor set layout.
	//

	// We are using a Uniform Buffer to pass some matrices to the vertex shader.
	// This is where do we assign the binding.
	VkDescriptorSetLayoutBinding descriptorSetLayoutBinding {};
	descriptorSetLayoutBinding.binding = 0;
	descriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	descriptorSetLayoutBinding.descriptorCount = 1;

	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo {};
	descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetLayoutCreateInfo.bindingCount = 1;
	descriptorSetLayoutCreateInfo.pBindings = &descriptorSetLayoutBinding;

	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, NULL, &descriptorSetLayout));

	// -------------------------------------------------------------------------
}

void setupDescriptorPool() {

	// -------------------------------------------------------------------------
	// Create DescriptorPool
	//

	std::vector<VkDescriptorPoolSize> typeCounts;

	VkDescriptorPoolSize descriptorPoolSize;
	descriptorPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorPoolSize.descriptorCount = 1;

	typeCounts.push_back(descriptorPoolSize);

	VkDescriptorPoolCreateInfo descriptorPoolInfo {};
	descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	descriptorPoolInfo.poolSizeCount = (uint32_t)typeCounts.size();
	descriptorPoolInfo.pPoolSizes = typeCounts.data();
	descriptorPoolInfo.maxSets = typeCounts.size();

	VK_CHECK_RESULT(vkCreateDescriptorPool(device, &descriptorPoolInfo, nullptr, &descriptorPool));


	// -------------------------------------------------------------------------
}

void setupDescriptorSet() {

	// -------------------------------------------------------------------------
	// Create descriptor set.
	//

	VkDescriptorSetAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = &descriptorSetLayout;

	VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &allocInfo, &descriptorSet));

	VkWriteDescriptorSet writeDescriptorSet {};

	// Binding 0 : Uniform buffer
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.dstSet = descriptorSet;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeDescriptorSet.pBufferInfo = &uniformDataVS.descriptor;
	writeDescriptorSet.dstBinding = 0;

	vkUpdateDescriptorSets(device, 1, &writeDescriptorSet, 0, NULL);

	// -------------------------------------------------------------------------
}

void setupPipelineLayout() {

	// -------------------------------------------------------------------------
	// Create Pipeline Layout.
	//

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo {};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;

	VK_CHECK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

	// -------------------------------------------------------------------------
}

void setupGraphicsPipelines() {

	// -------------------------------------------------------------------------
	// Create Pipeline
	//

	VkPipelineRasterizationStateCreateInfo rasterizationState {};
	rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizationState.depthClampEnable = VK_FALSE;
	rasterizationState.rasterizerDiscardEnable = VK_FALSE;
	rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizationState.lineWidth = 1.0f;
	rasterizationState.cullMode = VK_CULL_MODE_NONE;
	rasterizationState.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizationState.depthBiasEnable = VK_FALSE;

	VkPipelineViewportStateCreateInfo viewportState {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.scissorCount = 1;

	std::vector<VkDynamicState> dynamicStateEnables {};
	dynamicStateEnables.push_back(VK_DYNAMIC_STATE_VIEWPORT);
	dynamicStateEnables.push_back(VK_DYNAMIC_STATE_SCISSOR);

	VkPipelineDynamicStateCreateInfo dynamicState {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.pDynamicStates = dynamicStateEnables.data();
	dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());

	VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo {};
	pipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	pipelineMultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	// Blending is not used in this example
	VkPipelineColorBlendAttachmentState blendAttachmentState {};
	blendAttachmentState.colorWriteMask = 0xf;
	blendAttachmentState.blendEnable = VK_FALSE;

	VkPipelineColorBlendStateCreateInfo colorBlendState = {};
	colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendState.attachmentCount = 1;
	colorBlendState.pAttachments = &blendAttachmentState;


	// -------------------------------------------------------------------------
	// Create and attach shaders.
	//

	auto vertexShaderModule = loadSPIRVShader(xdl::XdevLFileName("shaders/vulkan_demo.vert.spv"));
	auto fragmentShaderModule = loadSPIRVShader(xdl::XdevLFileName("shaders/vulkan_demo.frag.spv"));
	if(VK_NULL_HANDLE == vertexShaderModule || VK_NULL_HANDLE == fragmentShaderModule) {
		XDEVL_ASSERT(false, "Shader Module couldn't be created.");
	}

	shaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
	shaderStages[0].module = vertexShaderModule;
	shaderStages[0].pName = "main";
	assert(shaderStages[0].module != VK_NULL_HANDLE);

	shaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	shaderStages[1].module = fragmentShaderModule;
	shaderStages[1].pName = "main";
	assert(shaderStages[1].module != VK_NULL_HANDLE);

	std::array<VkVertexInputAttributeDescription,2> vertexInputAttributs;
	vertexInputAttributs[0].binding = 0;
	vertexInputAttributs[0].location = 0;
	vertexInputAttributs[0].format = VK_FORMAT_R32G32B32_SFLOAT;
	vertexInputAttributs[0].offset = offsetof(Vertex, position);

	vertexInputAttributs[1].binding = 0;
	vertexInputAttributs[1].location = 1;
	vertexInputAttributs[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	vertexInputAttributs[1].offset = offsetof(Vertex, color);

	// -------------------------------------------------------------------------
	VkVertexInputBindingDescription vertexInputBinding {};
	vertexInputBinding.binding = 0;
	vertexInputBinding.stride = sizeof(Vertex);
	vertexInputBinding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo {};
	pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
	pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = &vertexInputBinding;
	pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = 2;
	pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = vertexInputAttributs.data();

	VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo {};
	pipelineDepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	pipelineDepthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
	pipelineDepthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
	pipelineDepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	pipelineDepthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
	pipelineDepthStencilStateCreateInfo.back.failOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.back.passOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
	pipelineDepthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;
	pipelineDepthStencilStateCreateInfo.front = pipelineDepthStencilStateCreateInfo.back;

	VkGraphicsPipelineCreateInfo pipelineCreateInfo {};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.flags = VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT;
	pipelineCreateInfo.pViewportState = &viewportState;
	pipelineCreateInfo.pColorBlendState = &colorBlendState;
	pipelineCreateInfo.pRasterizationState = &rasterizationState;
	pipelineCreateInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
	pipelineCreateInfo.pDepthStencilState = &pipelineDepthStencilStateCreateInfo;
	pipelineCreateInfo.pMultisampleState = &pipelineMultisampleStateCreateInfo;
	pipelineCreateInfo.pDynamicState = &dynamicState;
	pipelineCreateInfo.renderPass = renderpass;
	pipelineCreateInfo.layout = pipelineLayout;
	pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineCreateInfo.pStages = shaderStages.data();

	VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo {};
	pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	pipelineInputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	pipelineCreateInfo.pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo;

	VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &graphicsPipeline));

	// -------------------------------------------------------------------------
}

void setupRenderPass() {

	// -------------------------------------------------------------------------
	// Create Render Pass.
	//

	VkAttachmentDescription colorAttachmentDescription {};
	colorAttachmentDescription.format = vulkan_surface->getColorFormat();
	colorAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentDescription depthStencilAttachmentDescription {};
	depthStencilAttachmentDescription.format = vulkan_device->getSupportedDepthStencilFormat();
	depthStencilAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
	depthStencilAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthStencilAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthStencilAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthStencilAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthStencilAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthStencilAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference colorReference {};
	colorReference.attachment = 0;
	colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference depthReference = {};
	depthReference.attachment = 1;
	depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;


	std::array<VkSubpassDependency, 1> dependencies {};

	// First dependency at the start of the renderpass
	// Does the transition from final to initial layout
	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[0].dstSubpass = 0;
	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].srcAccessMask = 0;
	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkSubpassDescription subpassDescription {};
	subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDescription.colorAttachmentCount = 1;
	subpassDescription.pColorAttachments = &colorReference;
	subpassDescription.pDepthStencilAttachment = &depthReference;

	std::vector<VkAttachmentDescription> attachments;
	attachments.push_back(colorAttachmentDescription);
	attachments.push_back(depthStencilAttachmentDescription);

	VkRenderPassCreateInfo renderPassCreateInfo {};
	renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCreateInfo.attachmentCount = (uint32_t)attachments.size();
	renderPassCreateInfo.pAttachments = attachments.data();
	renderPassCreateInfo.subpassCount = 1;
	renderPassCreateInfo.pSubpasses = &subpassDescription;
	renderPassCreateInfo.dependencyCount = (uint32_t)dependencies.size();
	renderPassCreateInfo.pDependencies = dependencies.data();

	VK_CHECK_RESULT(vkCreateRenderPass(device, &renderPassCreateInfo, nullptr, &renderpass));

	// -------------------------------------------------------------------------
}

void setupSwapChainFrameBuffer() {

	// -------------------------------------------------------------------------
	// Create Framebuffer for each swapchain image.
	//
	VkFramebufferCreateInfo framebufferCreateInfo {};
	framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferCreateInfo.renderPass = renderpass;
	framebufferCreateInfo.attachmentCount = 2;
	framebufferCreateInfo.width = vulkan_surface->getExtend().width;
	framebufferCreateInfo.height = vulkan_surface->getExtend().height;
	framebufferCreateInfo.layers = 1;

	framebuffers.reserve(vulkan_swapchain->getSwapChainImages().size());
	framebuffers.resize(vulkan_swapchain->getSwapChainImages().size());
	for(int idx = 0; idx < vulkan_swapchain->getSwapChainImages().size(); idx++) {
		VkImageView attachments[] = {
			vulkan_swapchain->getSwapChainImages()[idx].imageView,
			depthStencil.view
		};
		framebufferCreateInfo.pAttachments = attachments;

		vkCreateFramebuffer(device, &framebufferCreateInfo, nullptr, &framebuffers[idx]);
	}

	// -------------------------------------------------------------------------
}

void setupDepthStencil() {

	VkImageCreateInfo image {};
	image.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	image.imageType = VK_IMAGE_TYPE_2D;
	image.format = vulkan_device->getSupportedDepthStencilFormat();
	image.extent = { vulkan_swapchain->getExtent2D().width, vulkan_swapchain->getExtent2D().height, 1 };
	image.mipLevels = 1;
	image.arrayLayers = 1;
	image.samples = VK_SAMPLE_COUNT_1_BIT;
	image.tiling = VK_IMAGE_TILING_OPTIMAL;
	image.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	image.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	VK_CHECK_RESULT(vkCreateImage(device, &image, nullptr, &depthStencil.image));

	// Allocate memory for the image (device local) and bind it to our image
	VkMemoryAllocateInfo memAlloc {};
	memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	VkMemoryRequirements memReqs;
	vkGetImageMemoryRequirements(device, depthStencil.image, &memReqs);
	memAlloc.allocationSize = memReqs.size;
	memAlloc.memoryTypeIndex = xdl::getMemoryTypeIndex(vulkan_instance->getPhysicalDeviceMemoryProperties(), memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	VK_CHECK_RESULT(vkAllocateMemory(device, &memAlloc, nullptr, &depthStencil.mem));
	VK_CHECK_RESULT(vkBindImageMemory(device, depthStencil.image, depthStencil.mem, 0));

	VkImageViewCreateInfo depthStencilView {};
	depthStencilView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	depthStencilView.viewType = VK_IMAGE_VIEW_TYPE_2D;
	depthStencilView.format = VK_FORMAT_D32_SFLOAT_S8_UINT;
	depthStencilView.subresourceRange = {};
	depthStencilView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
	depthStencilView.subresourceRange.baseMipLevel = 0;
	depthStencilView.subresourceRange.levelCount = 1;
	depthStencilView.subresourceRange.baseArrayLayer = 0;
	depthStencilView.subresourceRange.layerCount = 1;
	depthStencilView.image = depthStencil.image;

	VK_CHECK_RESULT(vkCreateImageView(device, &depthStencilView, nullptr, &depthStencil.view));
}


void updateUniformBuffers() {

	// -------------------------------------------------------------------------
	// Update Uniform buffer.
	//

	xdl::xdl_float aspect = (xdl::xdl_float)window->getWidth()/(xdl::xdl_float)window->getHeight();
	xdl::xdl_float fov = 60.0f;

//	tmath::perspective(fov, aspect, 0.6f, 10.0f, uboVS.projectionMatrix);
	tmath::frustum(-0.31f, 0.31f, -0.31f, 0.31f, 1.0f, 100.0f, uboVS.projectionMatrix);
	tmath::translate(0.0f, 0.0f, -4.0f, uboVS.viewMatrix);

	static xdl::xdl_float angle = 0.0f;
	tmath::rotate_y(angle, uboVS.modelMatrix);

	angle += core->getDT() * 90.0f;

	uint8_t* buffer = nullptr;
	size_t bufferSize = sizeof(UniformBufferForVertexShader);
	VK_CHECK_RESULT(vkMapMemory(device, uniformDataVS.memory, 0, sizeof(UniformBufferForVertexShader), 0, (void **)&buffer));
	memcpy(buffer, &uboVS, bufferSize);
	vkUnmapMemory(device, uniformDataVS.memory);

	// -------------------------------------------------------------------------
}

void setupUniformBuffers() {

	// -------------------------------------------------------------------------
	// Setup Uniform buffer.
	//

	VkBufferCreateInfo bufferInfo {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = sizeof(UniformBufferForVertexShader);
	bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VK_CHECK_RESULT(vkCreateBuffer(device, &bufferInfo, nullptr, &uniformDataVS.buffer));

	VkMemoryRequirements memReqs;
	vkGetBufferMemoryRequirements(device, uniformDataVS.buffer, &memReqs);

	VkMemoryAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memReqs.size;
	allocInfo.memoryTypeIndex = xdl::getMemoryTypeIndex(vulkan_instance->getPhysicalDeviceMemoryProperties(), memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	VK_CHECK_RESULT(vkAllocateMemory(device, &allocInfo, nullptr, &(uniformDataVS.memory)));
	VK_CHECK_RESULT(vkBindBufferMemory(device, uniformDataVS.buffer, uniformDataVS.memory, 0));

	uniformDataVS.descriptor.buffer = uniformDataVS.buffer;
	uniformDataVS.descriptor.offset = 0;
	uniformDataVS.descriptor.range = sizeof(uboVS);

	updateUniformBuffers();

	// -------------------------------------------------------------------------

}


void setupVertices() {

	std::vector<Vertex> vertexBuffer = {
		{0.0f, -0.5f, 0.0f,    1.0f, 0.0f, 0.0f, 1.0f},
		{0.5f, 0.5f, 0.0f,     0.0f, 1.0f, 0.0f, 1.0f},
		{-0.5f, 0.5f, 0.0f,    0.0f, 0.0f, 1.0f, 1.0f}
	};
	uint32_t vertexBufferSize = static_cast<uint32_t>(vertexBuffer.size()) * sizeof(Vertex);

	// Setup indices
	std::vector<uint32_t> indexBuffer = { 0, 1, 2 };
	indices.count = static_cast<uint32_t>(indexBuffer.size());
	uint32_t indexBufferSize = indices.count * sizeof(uint32_t);

	VkMemoryAllocateInfo memAlloc = {};
	memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	VkMemoryRequirements memReqs;

	void *data;

	// Vertex buffer
	VkBufferCreateInfo vertexBufferInfo = {};
	vertexBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	vertexBufferInfo.size = vertexBufferSize;
	vertexBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

	// Copy vertex data to a buffer visible to the host
	VK_CHECK_RESULT(vkCreateBuffer(device, &vertexBufferInfo, nullptr, &vertices.buffer));
	vkGetBufferMemoryRequirements(device, vertices.buffer, &memReqs);
	memAlloc.allocationSize = memReqs.size;

	memAlloc.memoryTypeIndex = xdl::getMemoryTypeIndex(vulkan_instance->getPhysicalDeviceMemoryProperties(), memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
	VK_CHECK_RESULT(vkAllocateMemory(device, &memAlloc, nullptr, &vertices.memory));
	VK_CHECK_RESULT(vkMapMemory(device, vertices.memory, 0, memAlloc.allocationSize, 0, &data));
	memcpy(data, vertexBuffer.data(), vertexBufferSize);
	vkUnmapMemory(device, vertices.memory);
	VK_CHECK_RESULT(vkBindBufferMemory(device, vertices.buffer, vertices.memory, 0));

	// Index buffer
	VkBufferCreateInfo indexbufferInfo = {};
	indexbufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	indexbufferInfo.size = indexBufferSize;
	indexbufferInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;

	// Copy index data to a buffer visible to the host
	VK_CHECK_RESULT(vkCreateBuffer(device, &indexbufferInfo, nullptr, &indices.buffer));
	vkGetBufferMemoryRequirements(device, indices.buffer, &memReqs);
	memAlloc.allocationSize = memReqs.size;
	memAlloc.memoryTypeIndex = xdl::getMemoryTypeIndex(vulkan_instance->getPhysicalDeviceMemoryProperties(), memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
	VK_CHECK_RESULT(vkAllocateMemory(device, &memAlloc, nullptr, &indices.memory));
	VK_CHECK_RESULT(vkMapMemory(device, indices.memory, 0, indexBufferSize, 0, &data));
	memcpy(data, indexBuffer.data(), indexBufferSize);
	vkUnmapMemory(device, indices.memory);
	VK_CHECK_RESULT(vkBindBufferMemory(device, indices.buffer, indices.memory, 0));

}


void setupQueue() {

	// -------------------------------------------------------------------------
	// Create queue.
	//
	auto queueFamilies = vulkan_instance->getQueueFamilyIndices();
	vkGetDeviceQueue(device, queueFamilies->getGraphicsQueueIndex(), 0, &graphicsQueue);
	vkGetDeviceQueue(device, queueFamilies->getPresentQueueIndex(), 0, &presentQueue);

	// -------------------------------------------------------------------------
}

void setupCommandBuffers() {

	// -------------------------------------------------------------------------
	// Create Command Buffers
	//
	drawCommandBuffers.resize(vulkan_swapchain->getSwapChainImages().size());
	drawCommandBuffers.reserve(vulkan_swapchain->getSwapChainImages().size());

	VkCommandBufferAllocateInfo commandBufferAllocateInfo {};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandPool = commandPool;
	commandBufferAllocateInfo.commandBufferCount = drawCommandBuffers.size();

	vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, drawCommandBuffers.data());

	// -------------------------------------------------------------------------
}


void fillCommandBuffers() {
	VkCommandBufferBeginInfo commandBufferBeginInfo {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

	std::array<VkClearValue, 2> clearValues {};
	clearValues[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
	clearValues[1].depthStencil = { 1.0f, 0 };

	VkRenderPassBeginInfo renderPassBeginInfo {};
	renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBeginInfo.renderPass = renderpass;
	renderPassBeginInfo.renderArea.offset.x = 0;
	renderPassBeginInfo.renderArea.offset.y = 0;
	renderPassBeginInfo.renderArea.extent = vulkan_swapchain->getExtent2D();
	renderPassBeginInfo.clearValueCount = clearValues.size();
	renderPassBeginInfo.pClearValues = clearValues.data();

	for (int32_t i = 0; i < drawCommandBuffers.size(); ++i) {

		renderPassBeginInfo.framebuffer = framebuffers[i];

		vkBeginCommandBuffer(drawCommandBuffers[i], &commandBufferBeginInfo);
		{
			vkCmdBeginRenderPass(drawCommandBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
			{
				VkViewport viewport {};
				viewport.width = (xdl::xdl_float)window->getWidth();
				viewport.height = (xdl::xdl_float)window->getHeight();
				viewport.minDepth = (xdl::xdl_float) 0.0f;
				viewport.maxDepth = (xdl::xdl_float) 1.0f;
				vkCmdSetViewport(drawCommandBuffers[i], 0, 1, &viewport);

				VkRect2D scissor {};
				scissor.extent = vulkan_swapchain->getExtent2D();

				vkCmdSetScissor(drawCommandBuffers[i], 0, 1, &scissor);

				vkCmdBindDescriptorSets(drawCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);

				vkCmdBindPipeline(drawCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

				VkDeviceSize offsets[1] = { 0 };
				vkCmdBindVertexBuffers(drawCommandBuffers[i], 0, 1, &vertices.buffer, offsets);

//				vkCmdDraw(drawCommandBuffers[i], 3, 1, 0, 0 );

				vkCmdBindIndexBuffer(drawCommandBuffers[i], indices.buffer, 0, VK_INDEX_TYPE_UINT32);
				vkCmdDrawIndexed(drawCommandBuffers[i], indices.count, 1, 0, 0, 1);
			}
			vkCmdEndRenderPass(drawCommandBuffers[i]);
		}

		vkEndCommandBuffer(drawCommandBuffers[i]);
	}
}

void notify(xdl::XdevLEvent& event) {
	switch(event.type) {
		case xdl::XDEVL_CORE_EVENT: {
			if(event.core.type == xdl::XDEVL_CORE_SHUTDOWN) {
				running = xdl::xdl_false;
			}
		}
		break;
		case xdl::XDEVL_WINDOW_EVENT: {
			switch(event.window.event) {
				case xdl::XDEVL_WINDOW_MOVED:
				case xdl::XDEVL_WINDOW_RESIZED:
					break;
			}
		}
		break;
	}
}

xdl::xdl_int initXdevL(int argc, char* argv[]) {

	// Create the core system.
	if(xdl::createCore(&core, argc, argv, xdl::XdevLFileName("vulkan_demo.xml")) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	// Register the specified instance as observer.
	auto ret = core->registerListener(notify);
	if(xdl::RET_SUCCESS != ret) {
		return xdl::RET_FAILED;
	}

	// Create a window so that we can draw something.
	window = xdl::getModule<xdl::IPXdevLWindow>(core, xdl::XdevLID("MyWindow"));
	if(!window) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	ret = window->create();
	if(xdl::RET_SUCCESS != ret) {
		return xdl::RET_FAILED;
	}
	window->show();

	// Get the instance to the keyboard module.
	keyboard = xdl::getModule<xdl::IPXdevLKeyboard>(core, xdl::XdevLID("MyKeyboard"));
	if(!keyboard) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}
	if(keyboard->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	keyboard->getButton(xdl::KEY_ESCAPE, &esc);

	// Get the OpenGL Rendering System.
	vulkan = xdl::getModule<xdl::IPXdevLVulkanContext>(core, xdl::XdevLID("MyVulkanContext"));
	if(!vulkan) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	if(vulkan->createContext(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	//
	// Get the needed Vulkan modules.
	//
	vulkan_instance = vulkan->getInstance();
	vulkan_device = vulkan->getDevice();
	vulkan_surface = vulkan->getSurface();
	vulkan_swapchain = vulkan->getSwapChain();

	//
	// Get native Vulkan handles.
	//
	instance = (VkInstance)vulkan_instance->getNativeHandle();
	device = (VkDevice)vulkan_device->getNativeHandle();
	surface = (VkSurfaceKHR)vulkan_surface->getNativeHandle();
	swapchain = (VkSwapchainKHR)vulkan_swapchain->getNativeHandle();

	return xdl::RET_SUCCESS;
}




//
// 1. Create Instance.
// 2. Create Device.
// 3. Create Surface.
// 4. Create SwapChain.
// All the steps above are done in the XdevLVulkanContext class when using
// XdevLVulkanContext::createContext. You may do all those steps manually.
// 5. Create Render Pass.
// 6. Create Framebuffer from SwapChain Color and DepthStencil images.
// 7. Create Pipeline.


int main(int argc, char* argv[]) {

	//
	//
	// Initialize all XdevL plugins and modules.
	//
	if(initXdevL(argc, argv) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	setupRenderPass();
	setupDepthStencil();
	setupSwapChainFrameBuffer();
	setupPipelineCache();
	setupUniformBuffers();
	setupVertices();
	setupDescriptorSetLayout();
	setupPipelineLayout();
	setupGraphicsPipelines();
	setupCommandPool();
	setupDescriptorPool();
	setupDescriptorSet();
	setupQueue();
	setupCommandBuffers();
	fillCommandBuffers();

	VkSemaphore presentSemaphore = VK_NULL_HANDLE;
	VkSemaphoreCreateInfo presentCompleteSemaphoreCreateInfo {};
	presentCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &presentCompleteSemaphoreCreateInfo, nullptr, &presentSemaphore);


	VkSemaphore renderSemaphore = VK_NULL_HANDLE;
	VkSemaphoreCreateInfo renderCompleteSemaphoreCreateInfo {};
	renderCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &renderCompleteSemaphoreCreateInfo, nullptr, &renderSemaphore);


	//
	// Start main loop.
	//
	while(running) {
		core->update();

		if(esc->getClicked()) {
			break;
		}

		updateUniformBuffers();

		xdl::xdl_uint32 currentBuffer;
		VkResult result = vkAcquireNextImageKHR(device, vulkan_swapchain->getNativeHandle(), std::numeric_limits<uint64_t>::max(), presentSemaphore, VK_NULL_HANDLE, &currentBuffer);
		if(VK_SUCCESS == result || VK_SUBOPTIMAL_KHR == result) {

			//
			// Submit the commands.
			//
			VkPipelineStageFlags waitStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			VkSubmitInfo submitInfo {};
			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submitInfo.pWaitDstStageMask = &waitStageMask;
			submitInfo.pWaitSemaphores = &presentSemaphore;
			submitInfo.waitSemaphoreCount = 1;
			submitInfo.pSignalSemaphores = &renderSemaphore;
			submitInfo.signalSemaphoreCount = 1;
			submitInfo.pCommandBuffers = &drawCommandBuffers[currentBuffer];
			submitInfo.commandBufferCount = 1;

			vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);

			//
			// When ready present the framebuffer.
			//
			VkPresentInfoKHR present {};
			present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
			present.swapchainCount = 1;
			present.waitSemaphoreCount = 1;
			present.pWaitSemaphores = &renderSemaphore;
			present.pSwapchains = vulkan_swapchain->getNativeHandlePtr();
			present.pImageIndices = &currentBuffer;

			VkResult result = vkQueuePresentKHR(presentQueue, &present);
			if(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR) {
				result = vkQueueWaitIdle(presentQueue);
			}
		}


	}

	vkDeviceWaitIdle(device);

	vkDestroySemaphore(device, presentSemaphore, nullptr);
	vkDestroySemaphore(device, renderSemaphore, nullptr);

	for(const auto& shaderStage : shaderStages) {
		vkDestroyShaderModule(device, shaderStage.module, nullptr);
	}

	vkFreeMemory(device, depthStencil.mem, nullptr);
	vkDestroyImage(device, depthStencil.image, nullptr);
	vkDestroyImageView(device, depthStencil.view, nullptr);

	vkFreeMemory(device, uniformDataVS.memory, nullptr);
	vkFreeMemory(device, vertices.memory, nullptr);
	vkFreeMemory(device, indices.memory, nullptr);

	vkDestroyBuffer(device, uniformDataVS.buffer, nullptr);
	vkDestroyBuffer(device, vertices.buffer, nullptr);
	vkDestroyBuffer(device, indices.buffer, nullptr);

	vkDestroyPipeline(device, graphicsPipeline, nullptr);
	vkDestroyPipelineCache(device, pipelineCache, nullptr);
	vkDestroyPipelineLayout(device, pipelineLayout, nullptr);

	vkFreeCommandBuffers(device, commandPool, drawCommandBuffers.size(), drawCommandBuffers.data());
	vkFreeDescriptorSets(device, descriptorPool, 1, &descriptorSet);
	vkDestroyCommandPool(device, commandPool, nullptr);
	vkDestroyDescriptorPool(device, descriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
	vkDestroyRenderPass(device, renderpass, nullptr);

	for(const auto& frameBuffer : framebuffers) {
		vkDestroyFramebuffer(device, frameBuffer, nullptr);
	}

	vulkan_swapchain = nullptr;
	vulkan_surface = nullptr;
	vulkan_device = nullptr;
	vulkan_instance = nullptr;

	xdl::destroyCore(core);

	return 0;
}
