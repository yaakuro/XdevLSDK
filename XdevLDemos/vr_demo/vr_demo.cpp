/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLFileSystem/XdevLArchive.h>
#include <XdevLVulkanContext/XdevLVulkanContext.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>
#include <XdevLVR/XdevLVR.h>

#include <tm/tm.h>

xdl::IPXdevLCore core = nullptr;
xdl::IPXdevLWindow window = nullptr;
xdl::IPXdevLArchive archive = nullptr;
xdl::IPXdevLKeyboard keyboard = nullptr;
xdl::IPXdevLButton esc = nullptr;
xdl::IPXdevLVulkanContext vulkan_context = nullptr;
xdl::IPXdevLVulkanInstance vulkan_instance = nullptr;
xdl::IPXdevLVulkanDevice vulkan_device = nullptr;
xdl::IPXdevLVulkanSurface vulkan_surface = nullptr;
xdl::IPXdevLVulkanSwapChain vulkan_swapchain = nullptr;
xdl::IPXdevLVulkanVertexBuffer vulkan_vertex_buffer = nullptr;
xdl::IPXdevLVulkanIndexBuffer vulkan_index_buffer = nullptr;
xdl::IPXdevLVulkanUniformBuffer vulkan_uniform_buffer[2]= {nullptr, nullptr};
xdl::IPXdevLVulkanShader vulkan_vertex_shader = nullptr;
xdl::IPXdevLVulkanShader vulkan_fragment_shader = nullptr;
xdl::IPXdevLVulkanFrameBuffer vulkan_frame_buffer[2] = {nullptr, nullptr};
xdl::IPXdevLVR vr = nullptr;
xdl::IXdevLVRControllerAccesor vr_left_hand_controller;
xdl::xdl_bool running = xdl::xdl_true;

//
// Vulkan native handles.
//
VkInstance instance = VK_NULL_HANDLE;
VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
VkDevice device = VK_NULL_HANDLE;
VkSurfaceKHR surface = VK_NULL_HANDLE;
VkSwapchainKHR swapchain = VK_NULL_HANDLE;


VkPipelineCache pipelineCache = VK_NULL_HANDLE;
VkPipeline graphicsPipeline = VK_NULL_HANDLE;

VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
VkDescriptorSet descriptorSet[2] = {VK_NULL_HANDLE, VK_NULL_HANDLE};

VkQueue graphicsQueue = VK_NULL_HANDLE;
VkQueue presentQueue = VK_NULL_HANDLE;
VkCommandPool commandPool = VK_NULL_HANDLE;
VkCommandBuffer drawCommandBuffers = VK_NULL_HANDLE;
VkCommandBuffer presentCommandBuffers = VK_NULL_HANDLE;

xdl::xdl_float angleX = 0.0f;
xdl::xdl_float angleY = 0.0f;

struct Vertex {
	xdl::xdl_float position[4];
	xdl::xdl_float normal[3];
	xdl::xdl_float color[4];
};


struct UniformBufferForVertexShader {
	tmath::mat4 projectionMatrix;
	tmath::mat4 viewMatrix;
	tmath::mat4 modelMatrix;
};

UniformBufferForVertexShader uboVS[2];

struct HMDRenderTarget {
	xdl::XdevLSizeUI size;
};

HMDRenderTarget renderTarget;


void setupCommandPool() {

	// -------------------------------------------------------------------------
	// Create command pool.
	//

	VkCommandPoolCreateInfo commandPoolCreateInfo {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	commandPoolCreateInfo.queueFamilyIndex = vulkan_device->getGraphicsFamilyQueueIndex();

	VK_CHECK_RESULT(vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool));

	// -------------------------------------------------------------------------

}

void setupQueue() {

	// -------------------------------------------------------------------------
	// Create queue.
	//

	auto queueFamilies = vulkan_instance->getQueueFamilyIndices();
	vkGetDeviceQueue(device, queueFamilies->getGraphicsQueueIndex(), 0, &graphicsQueue);
	vkGetDeviceQueue(device, queueFamilies->getPresentQueueIndex(), 0, &presentQueue);

	// -------------------------------------------------------------------------
}

void setupCommandBuffers() {

	// -------------------------------------------------------------------------
	// Create Command Buffers
	//

	VkCommandBufferAllocateInfo commandBufferAllocateInfo {};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandPool = commandPool;
	commandBufferAllocateInfo.commandBufferCount = 1;

	vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &drawCommandBuffers);

	vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &presentCommandBuffers);

	// -------------------------------------------------------------------------
}

void setupDescriptorSetLayout() {

	// -------------------------------------------------------------------------
	// Create descriptor set layout.
	//

	// We are using a Uniform Buffer to pass some matrices to the vertex shader.
	// This is where do we assign the binding.
	VkDescriptorSetLayoutBinding descriptorSetLayoutBinding {};
	descriptorSetLayoutBinding.binding = 0;
	descriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	descriptorSetLayoutBinding.descriptorCount = 1;

	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo {};
	descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetLayoutCreateInfo.bindingCount = 1;
	descriptorSetLayoutCreateInfo.pBindings = &descriptorSetLayoutBinding;

	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, NULL, &descriptorSetLayout));

	// -------------------------------------------------------------------------
}

void setupUniformBufferDescriptorPool() {

	// -------------------------------------------------------------------------
	// Create DescriptorPool
	//

	std::vector<VkDescriptorPoolSize> descriptorPoolSize;

	descriptorPoolSize.push_back( {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 2});

	VkDescriptorPoolCreateInfo descriptorPoolInfo {};
	descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	descriptorPoolInfo.poolSizeCount = (uint32_t)descriptorPoolSize.size();
	descriptorPoolInfo.pPoolSizes = descriptorPoolSize.data();
	descriptorPoolInfo.maxSets = 2;

	VK_CHECK_RESULT(vkCreateDescriptorPool(device, &descriptorPoolInfo, nullptr, &descriptorPool));

	// -------------------------------------------------------------------------
}

void setupUniformBufferDescriptorSet() {

	// -------------------------------------------------------------------------
	// Create descriptor set.
	//

	for(int eye = 0; eye < 2; eye++) {
		VkDescriptorSetAllocateInfo allocInfo {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = descriptorPool;
		allocInfo.descriptorSetCount = 1;
		allocInfo.pSetLayouts = &descriptorSetLayout;

		VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &allocInfo, &descriptorSet[eye]));

		VkWriteDescriptorSet writeDescriptorSet {};

		// Binding 0 : Uniform buffer
		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.dstSet = descriptorSet[eye];
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

		auto desscriptorBufferInfo = vulkan_uniform_buffer[eye]->getDescriptorBufferInfo();
		writeDescriptorSet.pBufferInfo = &desscriptorBufferInfo;
		writeDescriptorSet.dstBinding = 0;

		vkUpdateDescriptorSets(device, 1, &writeDescriptorSet, 0, NULL);
	}
	// -------------------------------------------------------------------------
}

void setupPipelineLayout() {

	// -------------------------------------------------------------------------
	// Create Pipeline Layout.
	//

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo {};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;

	VK_CHECK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

	// -------------------------------------------------------------------------
}

void setupGraphicsPipelines() {

	// -------------------------------------------------------------------------
	// Create Pipeline
	//

	VkPipelineRasterizationStateCreateInfo rasterizationState {};
	rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizationState.depthClampEnable = VK_FALSE;
	rasterizationState.rasterizerDiscardEnable = VK_FALSE;
	rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizationState.lineWidth = 1.0f;
	rasterizationState.cullMode = VK_CULL_MODE_NONE;
	rasterizationState.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizationState.depthBiasEnable = VK_FALSE;

	VkPipelineViewportStateCreateInfo viewportState {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.scissorCount = 1;

	std::vector<VkDynamicState> dynamicStateEnables {};
	dynamicStateEnables.push_back(VK_DYNAMIC_STATE_VIEWPORT);
	dynamicStateEnables.push_back(VK_DYNAMIC_STATE_SCISSOR);

	VkPipelineDynamicStateCreateInfo dynamicState {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.pDynamicStates = dynamicStateEnables.data();
	dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());

	VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo {};
	pipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	pipelineMultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	// Blending is not used in this example
	VkPipelineColorBlendAttachmentState blendAttachmentState {};
	blendAttachmentState.colorWriteMask = 0xf;
	blendAttachmentState.blendEnable = VK_FALSE;

	VkPipelineColorBlendStateCreateInfo colorBlendState = {};
	colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendState.attachmentCount = 1;
	colorBlendState.pAttachments = &blendAttachmentState;


	// -------------------------------------------------------------------------
	// Create and attach shaders.
	//
	xdl::XdevLFileName vertex_shader_filename("shaders/vr_demo/vr_demo.vert.spv");
	xdl::XdevLFileName fragment_shader_filename("shaders/vr_demo/vr_demo.frag.spv");

	if(archive->fileExists(vertex_shader_filename) == xdl::xdl_false) {
		XDEVL_ASSERT(false, "Vertex Shader file doesn't exists.");
	}
	if(archive->fileExists(fragment_shader_filename) == xdl::xdl_false) {
		XDEVL_ASSERT(false, "Fragment Shader file doesn't exists.");
	}
	vulkan_vertex_shader = vulkan_device->createVertexShader();
	vulkan_fragment_shader = vulkan_device->createFragmentShader();

	vulkan_vertex_shader->create();
	vulkan_fragment_shader->create();

	auto file = archive->open(xdl::XdevLOpenForReadOnly(), vertex_shader_filename);
	vulkan_vertex_shader->load(file);
	file->close();
	file =  archive->open(xdl::XdevLOpenForReadOnly(), fragment_shader_filename);
	vulkan_fragment_shader->load(file);

	std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages {};
	shaderStages[0] = vulkan_vertex_shader->getPipelineShaderStageCreateInfo();
	shaderStages[1] = vulkan_fragment_shader->getPipelineShaderStageCreateInfo();

	// -------------------------------------------------------------------------


//	auto tmp = vulkan_context->createTexture();
//	tmp->create(vulkan_device->getNativeHandle(), vulkan_instance->getPhysicalDeviceMemoryProperties(), 512, 512, VK_FORMAT_R8G8B8A8_UINT);

	// -------------------------------------------------------------------------
	// Set buffer binding information.
	//

	VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo = vulkan_vertex_buffer->getPipelineVertexInputStateCreateInfo();

	// -------------------------------------------------------------------------

	VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo {};
	pipelineDepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	pipelineDepthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
	pipelineDepthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
	pipelineDepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	pipelineDepthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
	pipelineDepthStencilStateCreateInfo.back.failOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.back.passOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
	pipelineDepthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;
	pipelineDepthStencilStateCreateInfo.front = pipelineDepthStencilStateCreateInfo.back;

	VkGraphicsPipelineCreateInfo pipelineCreateInfo {};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.flags = VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT;
	pipelineCreateInfo.pViewportState = &viewportState;
	pipelineCreateInfo.pColorBlendState = &colorBlendState;
	pipelineCreateInfo.pRasterizationState = &rasterizationState;
	pipelineCreateInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
	pipelineCreateInfo.pDepthStencilState = &pipelineDepthStencilStateCreateInfo;
	pipelineCreateInfo.pMultisampleState = &pipelineMultisampleStateCreateInfo;
	pipelineCreateInfo.pDynamicState = &dynamicState;
	pipelineCreateInfo.renderPass = vulkan_frame_buffer[0]->getRenderPass();
	pipelineCreateInfo.layout = pipelineLayout;
	pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineCreateInfo.pStages = shaderStages.data();

	VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo {};
	pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	pipelineInputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	pipelineCreateInfo.pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo;


	VkPipelineCacheCreateInfo pipelineCacheCreateInfo {};
	pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
	VK_CHECK_RESULT(vkCreatePipelineCache(device, &pipelineCacheCreateInfo, nullptr, &pipelineCache));

	VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &graphicsPipeline));

	// -------------------------------------------------------------------------
}

void updateUniformBuffers() {

	// -------------------------------------------------------------------------
	// Update Uniform buffer.
	//

	uboVS[0].projectionMatrix = vr->getProjectionForLeftEye();
	uboVS[0].viewMatrix = vr->getPosLeftEye() * vr->getHMDPose();


	tmath::mat4 rx, ry, trans;
	tmath::rotate_x(-angleY, rx);
	tmath::rotate_y(angleX, ry);
	tmath::translate(0.0f, 0.0f, -0.2f, trans);
	uboVS[0].modelMatrix =  vr_left_hand_controller->getPoseMatrix();// vr->getControllerPose(0);//trans * rx * ry  ;



	vulkan_uniform_buffer[0]->lock();
	vulkan_uniform_buffer[0]->upload((xdl::xdl_uint8*)&uboVS[0], sizeof(UniformBufferForVertexShader));
	vulkan_uniform_buffer[0]->unlock();


	uboVS[1].projectionMatrix = vr->getProjectionForRightEye();
	uboVS[1].viewMatrix = vr->getPosRightEye() * vr->getHMDPose();
	uboVS[1].modelMatrix = uboVS[0].modelMatrix;


	vulkan_uniform_buffer[1]->lock();
	vulkan_uniform_buffer[1]->upload((xdl::xdl_uint8*)&uboVS[1], sizeof(UniformBufferForVertexShader));
	vulkan_uniform_buffer[1]->unlock();
	// -------------------------------------------------------------------------
}

void setupVertexIndexAndUniformBuffer() {

	// -------------------------------------------------------------------------
	// Setup vertices and indices.
	//
	const xdl::xdl_float sizex = 0.05f;
	const xdl::xdl_float sizey = 0.05f;
	const xdl::xdl_float sizez = 0.1f;

	std::vector<Vertex> vertices = {
		{-sizex,-sizey,-sizez, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex,-sizey, sizez, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex, sizey, sizez, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex, sizey,-sizez, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex,-sizey,-sizez, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex, sizey,-sizez, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex,-sizey, sizez, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex,-sizey,-sizez, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex,-sizey,-sizez, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex, sizey,-sizez, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex,-sizey,-sizez, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex,-sizey,-sizez, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{-sizex,-sizey,-sizez, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex, sizey, sizez, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex, sizey,-sizez, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex,-sizey, sizez, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex,-sizey, sizez, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex,-sizey,-sizez, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{-sizex, sizey, sizez, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex,-sizey, sizez, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex,-sizey, sizez, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex, sizey, sizez, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex,-sizey,-sizez, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex, sizey,-sizez, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex,-sizey,-sizez, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex, sizey, sizez, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex,-sizey, sizez, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex, sizey, sizez, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex, sizey,-sizez, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex, sizey,-sizez, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex, sizey, sizez, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex, sizey,-sizez, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex, sizey, sizez, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ sizex, sizey, sizez, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-sizex, sizey, sizez, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ sizex,-sizey, sizez, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f}
	};
	uint32_t verticesSizeInBytes = static_cast<uint32_t>(vertices.size()) * sizeof(Vertex);

	vulkan_vertex_buffer = vulkan_device->createVertexBuffer();
	vulkan_vertex_buffer->addElement(0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, offsetof(Vertex, position));
	vulkan_vertex_buffer->addElement(0, 1, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, normal));
	vulkan_vertex_buffer->addElement(0, 2, VK_FORMAT_R32G32B32A32_SFLOAT, offsetof(Vertex, color));
	vulkan_vertex_buffer->init((xdl::xdl_uint8*)vertices.data(), verticesSizeInBytes);


	//
	// Setup indices.
	//
	std::vector<uint32_t> indexBuffer = {
		0, 1, 2,
		3, 4, 5,
		6, 7, 8,
		9, 10, 11,
		12, 13, 14,
		15, 16, 17,
		18, 19, 20,
		21, 22, 23,
		24, 25, 26,
		27, 28, 29,
		30, 31, 32,
		33, 34, 35
	};
	uint32_t indexBufferSize = indexBuffer.size() * sizeof(uint32_t);

	vulkan_index_buffer = vulkan_device->createIndexBuffer();
	vulkan_index_buffer->init((xdl::xdl_uint8*)indexBuffer.data(), indexBufferSize, indexBuffer.size());

	//
	// Setup Uniform Buffer.
	//
	vulkan_uniform_buffer[0] = vulkan_device->createUniformBuffer();
	vulkan_uniform_buffer[0]->init(sizeof(UniformBufferForVertexShader));

	vulkan_uniform_buffer[1] = vulkan_device->createUniformBuffer();
	vulkan_uniform_buffer[1]->init(sizeof(UniformBufferForVertexShader));


	// -------------------------------------------------------------------------
}


void renderCube(VkRenderPassBeginInfo renderPassBeginInfo, VkCommandBufferBeginInfo commandBufferBeginInfo, uint32_t eye) {
	renderPassBeginInfo.framebuffer = vulkan_frame_buffer[eye]->getNativeHandle();
	vkCmdBeginRenderPass(drawCommandBuffers, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
	{
		VkViewport viewport {};
		viewport.width = (xdl::xdl_float)renderTarget.size.width;
		viewport.height = (xdl::xdl_float)renderTarget.size.height;
		viewport.minDepth = (xdl::xdl_float) 0.0f;
		viewport.maxDepth = (xdl::xdl_float) 1.0f;
		vkCmdSetViewport(drawCommandBuffers, 0, 1, &viewport);

		VkRect2D scissor {};
		scissor.extent.width = renderTarget.size.width;
		scissor.extent.height = renderTarget.size.height;

		vkCmdSetScissor(drawCommandBuffers, 0, 1, &scissor);

		vkCmdBindDescriptorSets(drawCommandBuffers, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSet[eye], 0, nullptr);

		vkCmdBindPipeline(drawCommandBuffers, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

		VkDeviceSize offsets[1] = { 0 };
		VkBuffer vertex_buffer = vulkan_vertex_buffer->getBufferHandle();
		vkCmdBindVertexBuffers(drawCommandBuffers, 0, 1, &vertex_buffer, offsets);

		vkCmdBindIndexBuffer(drawCommandBuffers, vulkan_index_buffer->getBufferHandle(), 0, VK_INDEX_TYPE_UINT32);
		vkCmdDrawIndexed(drawCommandBuffers, vulkan_index_buffer->getNumberOfIndices(), 1, 0, 0, 1);
	}
	vkCmdEndRenderPass(drawCommandBuffers);

}


void fillCommandBuffers() {

	// -------------------------------------------------------------------------
	// Fill the command buffer.
	//

	VkCommandBufferBeginInfo commandBufferBeginInfo {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

	std::array<VkClearValue, 2> clearValues {};
	clearValues[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
	clearValues[1].depthStencil = { 1.0f, 0 };

	VkRenderPassBeginInfo renderPassBeginInfo {};
	renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBeginInfo.renderPass = vulkan_frame_buffer[0]->getRenderPass();
	renderPassBeginInfo.renderArea = {0, 0, renderTarget.size.width, renderTarget.size.height};
	renderPassBeginInfo.clearValueCount = clearValues.size();
	renderPassBeginInfo.pClearValues = clearValues.data();

	vkBeginCommandBuffer(drawCommandBuffers, &commandBufferBeginInfo);

	renderCube(renderPassBeginInfo, commandBufferBeginInfo, 0);
	renderCube(renderPassBeginInfo, commandBufferBeginInfo, 1);



	vkEndCommandBuffer(drawCommandBuffers);

	// -------------------------------------------------------------------------
}


const xdl::XdevLID ControllerButtonPressed("XDEVL_CONTROLLER_BUTTON_PRESSED");
const xdl::XdevLID ControllerButtonReleased("XDEVL_CONTROLLER_BUTTON_RELEASED");
const xdl::XdevLID ControllerTouchpadPressed("XDEVL_CONTROLLER_TOUCHPAD_PRESSED");
const xdl::XdevLID ControllerTouchpadReleased("XDEVL_CONTROLLER_TOUCHPAD_RELEASED");
const xdl::XdevLID ControllerTouchpadMove("XDEVL_CONTROLLER_TOUCHPAD_MOVE");

void notify(xdl::XdevLEvent& event) {
	switch(event.type) {
		case xdl::XDEVL_CORE_EVENT: {
			if(event.core.type == xdl::XDEVL_CORE_SHUTDOWN) {
				running = xdl::xdl_false;
			}
		}
		break;
		case xdl::XDEVL_WINDOW_EVENT: {
			switch(event.window.event) {
				case xdl::XDEVL_WINDOW_MOVED:
				case xdl::XDEVL_WINDOW_RESIZED:
					break;
			}
		}
		break;
	}

	if(ControllerButtonPressed.getHashCode() == event.type) {
		vr_left_hand_controller->triggerHapticPulse(0, 100000);

		std::cout << "Device: " << event.cbutton.controllerid << " " << xdl::xdevLButtonIdToString((xdl::XdevLButtonId)event.cbutton.buttonid) << std::endl;
	} else if(ControllerButtonReleased.getHashCode() == event.type) {
		std::cout << "Device: " << event.cbutton.controllerid << " " << xdl::xdevLButtonIdToString((xdl::XdevLButtonId)event.cbutton.buttonid) << std::endl;
	}  else if(ControllerTouchpadMove.getHashCode() == event.type) {
		std::cout << "Device: " << event.ctouchpadaxis.controllerid << " " << event.ctouchpadaxis.x << " " << event.ctouchpadaxis.y << std::endl;
		angleX += event.ctouchpadaxis.x;
		angleY += event.ctouchpadaxis.y;
	} else if(ControllerTouchpadPressed.getHashCode() == event.type) {
		std::cout << "Device: " << event.cbutton.controllerid << " " << xdl::xdevLButtonIdToString((xdl::XdevLButtonId)event.cbutton.buttonid) << std::endl;
	} else if(ControllerTouchpadReleased.getHashCode() == event.type) {
		std::cout << "Device: " << event.cbutton.controllerid << " " << xdl::xdevLButtonIdToString((xdl::XdevLButtonId)event.cbutton.buttonid) << std::endl;
	}
}

xdl::xdl_int initXdevL(int argc, char* argv[]) {

	// Create the core system.
	if(xdl::createCore(&core, argc, argv, xdl::XdevLFileName("vr_demo.xml")) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	// Register the specified instance as observer.
	auto ret = core->registerListener(notify);
	if(xdl::RET_SUCCESS != ret) {
		return xdl::RET_FAILED;
	}

	// Create a window so that we can draw something.
	window = xdl::getModule<xdl::IPXdevLWindow>(core, xdl::XdevLID("MyWindow"));
	if(!window) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	ret = window->create();
	if(xdl::RET_SUCCESS != ret) {
		return xdl::RET_FAILED;
	}
	window->show();

	// Get the instance to the keyboard module.
	keyboard = xdl::getModule<xdl::IPXdevLKeyboard>(core, xdl::XdevLID("MyKeyboard"));
	if(!keyboard) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}
	if(keyboard->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	keyboard->getButton(xdl::KEY_ESCAPE, &esc);

	archive = xdl::getModule<xdl::IPXdevLArchive> (core,  xdl::XdevLID("MyArchive"));
	if(nullptr == archive) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}
	archive->mount(xdl::XdevLFileName("."));

	// Get the OpenGL Rendering System.
	vulkan_context = xdl::getModule<xdl::IPXdevLVulkanContext>(core, xdl::XdevLID("MyVulkanContext"));
	if(!vulkan_context) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	vr = xdl::getModule<xdl::IPXdevLVR>(core, xdl::XdevLID("MyVR"));
	if(xdl::isModuleNotValid(vr)) {
		return xdl::RET_FAILED;
	}
	vr->setNearFar(0.1f, 100.0f);
	renderTarget.size = vr->getRecommendedSize();

	vr->create(vulkan_context);
	vr_left_hand_controller = vr->getController(xdl::XdevLVRControllerHand::RIGHT);

	//
	// Create the XdevL Vulkan objects.
	//
	vulkan_instance = vulkan_context->createInstance();
	vulkan_device = vulkan_context->createDevice();
	vulkan_surface = vulkan_context->createSurface();
	vulkan_swapchain = vulkan_context->createSwapChain();
	vulkan_frame_buffer[0] = vulkan_context->createFrameBuffer();
	vulkan_frame_buffer[1] = vulkan_context->createFrameBuffer();

	//
	// Create them.
	//
	auto instanceExtensionsRequired = vr->getInstanceExtensionsRequired();
	if(instanceExtensionsRequired.size() > 0) {
		vulkan_instance->addExtensionsToEnable(instanceExtensionsRequired);
	}

	VkApplicationInfo applicationInfo {};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 0);
	vulkan_instance->create(applicationInfo);

	auto deviceExtensionsRequired = vr->getDeviceExtensionsRequired(vulkan_instance->getPhysicalDevice());
	if(deviceExtensionsRequired.size() > 0) {
		vulkan_device->addExtensionsToEnable(deviceExtensionsRequired);
	}
	vulkan_device->create(vulkan_instance->getPhysicalDevice(),
	                      vulkan_instance->getQueueFamilyIndices());

	vulkan_surface->create(vulkan_instance->getNativeHandle(),
	                       vulkan_instance->getPhysicalDevice(),
	                       window,
	                       vulkan_instance->getQueueFamilyIndices());

	vulkan_swapchain->create(vulkan_instance->getNativeHandle(),
	                         vulkan_instance->getPhysicalDevice(),
	                         vulkan_device->getNativeHandle(),
	                         vulkan_surface->getNativeHandle(),
	                         vulkan_surface->getColorFormat(),
	                         vulkan_device->getSupportedDepthStencilFormat(),
	                         vulkan_surface->getColorSpace(),
	                         vulkan_surface->getSurfaceCapabilities(),
	                         vulkan_instance->getQueueFamilyIndices());

	vulkan_swapchain->createFramebuffers();

	vulkan_frame_buffer[0]->addColorTarget(0, VK_FORMAT_R8G8B8A8_SRGB);
	vulkan_frame_buffer[0]->addDepthStencil(VK_FORMAT_D24_UNORM_S8_UINT);
	vulkan_frame_buffer[0]->create(vulkan_instance->getNativeHandle(),
	                               vulkan_instance->getPhysicalDevice(),
	                               vulkan_device->getNativeHandle(),
	                               renderTarget.size.width,
	                               renderTarget.size.height);

	vulkan_frame_buffer[1]->addColorTarget(0, VK_FORMAT_R8G8B8A8_SRGB);
	vulkan_frame_buffer[1]->addDepthStencil(VK_FORMAT_D24_UNORM_S8_UINT);
	vulkan_frame_buffer[1]->create(vulkan_instance->getNativeHandle(),
	                               vulkan_instance->getPhysicalDevice(),
	                               vulkan_device->getNativeHandle(),
	                               renderTarget.size.width,
	                               renderTarget.size.height);

	//
	// Get native Vulkan handles.
	//
	instance = (VkInstance)vulkan_instance->getNativeHandle();
	physicalDevice = vulkan_instance->getPhysicalDevice();
	device = (VkDevice)vulkan_device->getNativeHandle();
	surface = (VkSurfaceKHR)vulkan_surface->getNativeHandle();
	swapchain = (VkSwapchainKHR)vulkan_swapchain->getNativeHandle();



	return xdl::RET_SUCCESS;
}




//
// 1. Create Instance.
// 2. Create Device.
// 3. Create Surface.
// 4. Create SwapChain.
// All the steps above are done in the XdevLVulkanContext class when using
// XdevLVulkanContext::createContext. You may do all those steps manually.


int main(int argc, char* argv[]) {

	//
	//
	// Initialize all XdevL plugins and modules.
	//
	if(initXdevL(argc, argv) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	setupVertexIndexAndUniformBuffer();
	setupDescriptorSetLayout();
	setupPipelineLayout();
	setupGraphicsPipelines();
	setupCommandPool();
	setupUniformBufferDescriptorPool();
	setupUniformBufferDescriptorSet();
	setupQueue();
	setupCommandBuffers();
	fillCommandBuffers();


	VkSemaphore presentSemaphore = VK_NULL_HANDLE;
	VkSemaphoreCreateInfo presentCompleteSemaphoreCreateInfo {};
	presentCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &presentCompleteSemaphoreCreateInfo, nullptr, &presentSemaphore);


	VkSemaphore renderSemaphore = VK_NULL_HANDLE;
	VkSemaphoreCreateInfo renderCompleteSemaphoreCreateInfo {};
	renderCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &renderCompleteSemaphoreCreateInfo, nullptr, &renderSemaphore);


	//
	// Start main loop.
	//
	while(running) {
		core->update();

		if(esc->getClicked()) {
			break;
		}

		updateUniformBuffers();

		xdl::xdl_uint32 currentBuffer;
		VkResult result = vkAcquireNextImageKHR(device, swapchain, std::numeric_limits<uint64_t>::max(), presentSemaphore, VK_NULL_HANDLE, &currentBuffer);
		if(VK_SUCCESS == result || VK_SUBOPTIMAL_KHR == result) {

			//
			// Submit the commands.
			//
			VkPipelineStageFlags waitStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			VkSubmitInfo submitInfo {};
			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submitInfo.pWaitDstStageMask = &waitStageMask;
			submitInfo.pWaitSemaphores = &presentSemaphore;
			submitInfo.waitSemaphoreCount = 1;
			submitInfo.pSignalSemaphores = &renderSemaphore;
			submitInfo.signalSemaphoreCount = 1;
			submitInfo.pCommandBuffers = &drawCommandBuffers;
			submitInfo.commandBufferCount = 1;

			vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);


			//
			// OpenVR
			//
			xdl::XdevLVRSubmitParameterVulkan vrSubmitParameterVulkan {};
			vrSubmitParameterVulkan.instance = instance;
			vrSubmitParameterVulkan.physicalDevice = physicalDevice;
			vrSubmitParameterVulkan.device = device;
			vrSubmitParameterVulkan.queue = graphicsQueue;
			vrSubmitParameterVulkan.queueFamilyIndex = vulkan_device->getGraphicsFamilyQueueIndex();
			vrSubmitParameterVulkan.renderFinishedSemaphore = renderSemaphore;
			vrSubmitParameterVulkan.commandBuffer = drawCommandBuffers;

			vrSubmitParameterVulkan.width = renderTarget.size.width;
			vrSubmitParameterVulkan.height = renderTarget.size.height;
			vrSubmitParameterVulkan.image = vulkan_frame_buffer[0]->getColorAttachment(0).image;

			vr->submitVulkan(xdl::XdevLVREye::LEFT, vrSubmitParameterVulkan);

			vrSubmitParameterVulkan.image = vulkan_frame_buffer[1]->getColorAttachment(0).image;
			vr->submitVulkan(xdl::XdevLVREye::RIGHT, vrSubmitParameterVulkan);

			// -----------------------------------------------------------------



//			VkImageBlit region;
//
//			region.srcOffsets[0].x = 0;
//			region.srcOffsets[0].y = 0;
//			region.srcOffsets[0].z = 0;
//			region.srcOffsets[1].x = renderTarget.size.width;
//			region.srcOffsets[1].y = renderTarget.size.height;
//			region.srcOffsets[1].z = 1;
//			region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
//			region.srcSubresource.mipLevel = 0;
//			region.srcSubresource.baseArrayLayer = 0;
//			region.srcSubresource.layerCount = 1;
//
//
//			region.dstOffsets[0].x = 0;
//			region.dstOffsets[0].y = 0;
//			region.dstOffsets[0].z = 0;
//			region.dstOffsets[1].x = vulkan_swapchain->getExtent2D().width;
//			region.dstOffsets[1].y = vulkan_swapchain->getExtent2D().height;
//			region.dstOffsets[1].z = 1;
//			region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
//			region.dstSubresource.mipLevel = 0;
//			region.dstSubresource.baseArrayLayer = 0;
//			region.dstSubresource.layerCount = 1;
//
//
//			vkCmdBlitImage(presentCommandBuffers,
//			               vulkan_frame_buffer[0]->getColorAttachment(0).image,
//			               VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
//			               vulkan_swapchain->getSwapChainImages()[currentBuffer].image,
//			               VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
//			               1,
//			               &region,
//			               VK_FILTER_NEAREST);
//
//			VkPipelineStageFlags waitStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
//			VkSubmitInfo submitInfo {};
//			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
//			submitInfo.pWaitDstStageMask = &waitStageMask;
//			submitInfo.pWaitSemaphores = &presentSemaphore;
//			submitInfo.waitSemaphoreCount = 1;
//			submitInfo.pSignalSemaphores = &renderSemaphore;
//			submitInfo.signalSemaphoreCount = 1;
//			submitInfo.pCommandBuffers = &presentCommandBuffers;
//			submitInfo.commandBufferCount = 1;
//
//			vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);



			//
			// When ready present the framebuffer.
			//
			VkPresentInfoKHR presentInfo {};
			presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
			presentInfo.swapchainCount = 1;
			presentInfo.waitSemaphoreCount = 1;
			presentInfo.pWaitSemaphores = &renderSemaphore;
			presentInfo.pSwapchains = &swapchain;
			presentInfo.pImageIndices = &currentBuffer;

			VkResult result = vkQueuePresentKHR(presentQueue, &presentInfo);
			if (result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR) {

				result = vkQueueWaitIdle(presentQueue);
			}
		}
	}

	vkDeviceWaitIdle(device);

	vkDestroySemaphore(device, presentSemaphore, nullptr);
	vkDestroySemaphore(device, renderSemaphore, nullptr);

	vkDestroyPipeline(device, graphicsPipeline, nullptr);
	vkDestroyPipelineCache(device, pipelineCache, nullptr);
	vkDestroyPipelineLayout(device, pipelineLayout, nullptr);

	vkFreeCommandBuffers(device, commandPool, 1, &drawCommandBuffers);

	for(auto eye = 0; eye < 2; eye++) {
		vkFreeDescriptorSets(device, descriptorPool, 1, &descriptorSet[eye]);
	}

	vkDestroyCommandPool(device, commandPool, nullptr);
	vkDestroyDescriptorPool(device, descriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);

	vulkan_frame_buffer[0] = nullptr;
	vulkan_frame_buffer[1] = nullptr;
	vulkan_uniform_buffer[0] = nullptr;
	vulkan_uniform_buffer[1] = nullptr;
	vulkan_vertex_buffer = nullptr;
	vulkan_index_buffer = nullptr;
	vulkan_vertex_shader = nullptr;
	vulkan_fragment_shader = nullptr;
	vulkan_swapchain = nullptr;
	vulkan_surface = nullptr;
	vulkan_device = nullptr;
	vulkan_instance = nullptr;

	xdl::destroyCore(core);

	return 0;
}
