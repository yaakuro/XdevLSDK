/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of 
	this software and associated documentation files (the "Software"), to deal in the 
	Software without restriction, including without limitation the rights to use, copy, 
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
	and to permit persons to whom the Software is furnished to do so, subject to the 
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies 
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
	DEALINGS IN THE SOFTWARE.


*/
#include <XdevL.h>

struct myArray {
	xdl::xdl_double	time;
};

int main(int argc, char* argv[]) {
	// To get the XdevL system ready we have to create the core system.
	xdl::XdevLCorePtr core = xdl::createCore(argc, argv,"network_client_demo.xml");

	// Get the instance to the socket module.
	xdl::XdevLUDPSocket* socket = xdl::getModule<xdl::XdevLUDPSocket*>(core, "MyUDPSocket");
	if(!socket){
		return xdl::RET_FAILED;
	}

	socket->bind(4321);
	
	myArray data;
	
	// Just do that for 10 seconds.
	if(socket->receive((xdl::xdl_int8*)&data, sizeof(myArray)) != xdl::RET_FAILED)
		std::cout << "Core server time: " << data.time << "\n\n";	
			
	socket->close();

	return 0;
}

