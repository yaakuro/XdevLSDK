/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLFileSystem/XdevLArchive.h>

#include <XdevLWindow/XdevLWindow.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLUI/XdevLUI.h>
#include <XdevLParticles/XdevLParticles.h>

#include <XdevLInput/XdevLMouse/XdevLMouse.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>
#include <iomanip>

#include <tm/tm.h>

xdl::IPXdevLCore core = nullptr;
xdl::IPXdevLWindow window = nullptr;
xdl::IPXdevLCursor cursor = nullptr;
xdl::IPXdevLKeyboard keyboard = nullptr;
xdl::IPXdevLMouse mouse = nullptr;
xdl::IPXdevLRAI rai = nullptr;
xdl::IPXdevLArchive archive = nullptr;
xdl::ui::IPXdevLUI ui = nullptr;

static xdl::xdl_bool fullscreenflag = false;


xdl::xdl_bool running = xdl::xdl_true;

void notify(xdl::XdevLEvent& event) {
	switch(event.type) {
		case xdl::XDEVL_CORE_EVENT: {
			if(event.core.type == xdl::XDEVL_CORE_SHUTDOWN) {
				running = xdl::xdl_false;
			}
		}
		break;
		case xdl::XDEVL_WINDOW_EVENT: {
			switch(event.window.event) {
				case xdl::XDEVL_WINDOW_MOVED:
				case xdl::XDEVL_WINDOW_RESIZED: {
					xdl::XdevLViewPort viewport {};
					viewport.x = 0.0f;
					viewport.y = 0.0f;
					viewport.width = event.window.width;
					viewport.height = event.window.height;
					rai->setViewport(viewport);
				}
				break;
			}
		}
		break;
	}
}

void buttonClickedCallback(const xdl::ui::XdevLUIClickedEvent& event) {
	running = false;
}

void checkboxCallback(const xdl::ui::XdevLUICheckedEvent& event) {
	window->setFullscreen(event.checked);
}

static xdl::xdl_int modulationColorItemSelector = 0;
void radioButtonCallback(const xdl::ui::XdevLUIActivatedEvent& event) {
	if(event.state == 0) {
		modulationColorItemSelector = 0;
	} else if(event.state == 1) {
		modulationColorItemSelector = 1;
	}
}

int main(int argc, char* argv[]) {

	// Create the core system.
	if(xdl::createCore(&core, argc, argv, xdl::XdevLFileName("ui_demo.xml")) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	// Register the specified instance as observer.
	auto result = core->registerListener(notify);
	if(xdl::RET_SUCCESS != result) {
		return xdl::RET_FAILED;
	}

	// Create a window so that we can draw something.
	window = xdl::getModule<xdl::IPXdevLWindow>(core, xdl::XdevLID("MyWindow"));
	if(!window) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	result = window->create();
	if(xdl::RET_SUCCESS != result) {
		return xdl::RET_FAILED;
	}

	// Create a window so that we can draw something.
	cursor = xdl::getModule<xdl::IPXdevLCursor>(core, xdl::XdevLID("XdevLCursor"));
	if(!cursor) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Get the instance to the keyboard module.
	keyboard = xdl::getModule<xdl::IPXdevLKeyboard>(core, xdl::XdevLID("MyKeyboard"));
	if(!keyboard) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Get the instance to the keyboard module.
	mouse = xdl::getModule<xdl::IPXdevLMouse>(core, xdl::XdevLID("MyMouse"));
	if(!mouse) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	if(cursor->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Get the OpenGL Rendering System.
	rai = xdl::getModule<xdl::IPXdevLRAI>(core, xdl::XdevLID("MyRAI"));
	if(!rai) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Get Archive instance.
	archive = xdl::getModule<xdl::XdevLArchive*>(core, xdl::XdevLID("MyArchive"));
	if(!archive) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}
	// We tell the archive to load files relative to the apps position.
	archive->mount(xdl::XdevLFileName("."));

	// Create the RAI system.
	if(rai->create(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Attach the keyboard to the window.
	if(keyboard->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Attach the mouse to the window.
	if(mouse->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	ui = xdl::getModule<xdl::ui::XdevLUI*>(core, xdl::XdevLID("MyUI"));
	if(!ui) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Attach the mouse to the window.
	if(ui->create(window, rai, archive) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	auto fontConfigFileName = xdl::XdevLFileName("fonts/default_info.txt");
	if(archive->fileExists(fontConfigFileName) == xdl::xdl_false) {
		return xdl::RET_FAILED;
	}
	auto fontFile = archive->open(xdl::XdevLOpenForReadOnly(), fontConfigFileName);
	ui->setupFontFromFile(fontFile);
	fontFile->close();

	//
	// Setup some other stuff
	//

	// Get a valid button object for the escape key.
	xdl::IPXdevLButton esc = nullptr;
	keyboard->getButton(xdl::KEY_ESCAPE, &esc);

	xdl::XdevLButton* showPropertiesKey;
	keyboard->getButton(xdl::KEY_S, &showPropertiesKey);

	xdl::XdevLButton* toggleFullscreen = nullptr;
	keyboard->getButton(xdl::KEY_F, &toggleFullscreen);

	xdl::IPXdevLAxis x_axis = nullptr;
	xdl::IPXdevLAxis y_axis = nullptr;
	xdl::IPXdevLButton left_mouse_button = nullptr;
	mouse->getAxis(xdl::AXIS_0, &x_axis);
	mouse->getAxis(xdl::AXIS_1, &y_axis);
	mouse->getButton(xdl::BUTTON_0, &left_mouse_button);

	window->show();


	//
	// Create interactive surfaces.
	//
	xdl::ui::XdevLUIVerticalBox vb;

	xdl::ui::XdevLUIText textTitle(xdl::XdevLString("This is a simple text widget."));
	textTitle.setPosition(xdl::ui::XdevLUIPosition(400, 550));

	xdl::ui::XdevLUIButton buttonPlay(xdl::XdevLString("PRESS ME"));
	buttonPlay.setPosition(xdl::ui::XdevLUIPosition(325, 400));
	buttonPlay.setSize(xdl::ui::XdevLUISize(64, 24));

	xdl::ui::XdevLUIButton buttonQuit(xdl::XdevLString("QUIT"));
	buttonQuit.setPosition(xdl::ui::XdevLUIPosition(325, 300));
	buttonQuit.setSize(xdl::ui::XdevLUISize(64, 24));
	buttonQuit.bind(xdl::ui::XdevLDelegateClicked::Create<&buttonClickedCallback>());

	xdl::ui::XdevLUICheckBox checkboxFullscreen(xdl::XdevLString("Fullscreen"));
	checkboxFullscreen.setPosition(xdl::ui::XdevLUIPosition(325, 200));
	checkboxFullscreen.setSize(xdl::ui::XdevLUISize(18, 18));
	checkboxFullscreen.bind(xdl::ui::XdevLDelegateChecked::Create<&checkboxCallback>());

	xdl::ui::XdevLUIRadioButton radioButton1(xdl::XdevLString("Background color 1"), 0);
	radioButton1.setPosition(xdl::ui::XdevLUIPosition(325, 145));
	radioButton1.setSize(xdl::ui::XdevLUISize(16, 16));
	radioButton1.bind(xdl::ui::XdevLDelegateActivated::Create<&radioButtonCallback>());

	xdl::ui::XdevLUIRadioButton radioButton2(xdl::XdevLString("Background color 2"), 1, radioButton1);
	radioButton2.setPosition(xdl::ui::XdevLUIPosition(325, 100));
	radioButton2.setSize(xdl::ui::XdevLUISize(16, 16));
	radioButton2.bind(xdl::ui::XdevLDelegateActivated::Create<&radioButtonCallback>());

	xdl::ui::XdevLUITextInputField textInputField(xdl::XdevLString("Name"));
	textInputField.setPosition(xdl::ui::XdevLUIPosition(325, 18));
	textInputField.setSize(xdl::ui::XdevLUISize(256, 24));

	vb.setPosition(xdl::ui::XdevLUIPosition(0.0f, 0.0f));
	vb.setSize(xdl::ui::XdevLUISize(800, 600));
	vb.addChild(&textTitle);
	vb.addChild(&buttonPlay);
	vb.addChild(&buttonQuit);
	vb.addChild(&checkboxFullscreen);
	vb.addChild(&radioButton2);
	vb.addChild(&radioButton1);
	vb.addChild(&textInputField);
	
	xdl::xdl_float modulationColorItem[2][4] = {
		{0.0f, 0.0f, 0.0f, 1.0f},
		{0.3, 0.7f, 0.4f, 1.0f}
	};

	//
	// Start new window and add surfaces on to it.
	//
	ui->setFontSize(xdl::ui::XdevLUISize(1.0f, 1.0f));
	ui->lock(xdl::XdevLString("Rendering Settings"));
//	ui->add(vb);
	ui->add(buttonPlay);
	ui->add(buttonQuit);
	ui->add(textTitle);
	ui->add(checkboxFullscreen);
	ui->add(radioButton1);
	ui->add(radioButton2);
	ui->add(textInputField);
	ui->unlock();

	//
	// Start main loop.
	//
	while(running) {
		core->update();

		if(esc->getClicked()) {
			break;
		}

		rai->clearColorTargets(modulationColorItem[modulationColorItemSelector][0],
		modulationColorItem[modulationColorItemSelector][1],
		modulationColorItem[modulationColorItemSelector][2],
		modulationColorItem[modulationColorItemSelector][3]);

		ui->render();

		rai->swapBuffers();

	}

	xdl::destroyCore(core);

	return 0;
}
