/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLApplication.h>

#include <iostream>
#include <iomanip>
#include <cmath>

namespace xdl {

	class AudioDemoApp : public XdevLApplication {
		public:
			XDEVL_APP_BODY_XML(AudioDemoApp)
				: Super(title, argc, argv, xml_filename)
				, play_pause(xdl_true)
				, play_pause2(xdl_false)
				, sound_source_gain(0.5f) {}

			void preInit(const Arguments& argv) override final {

				XdevLFileName sound1FileName("media/loop_a.wav");

				if(getArchive()->fileExists(sound1FileName) == xdl_false) {
					exit(RET_FAILED);
				}

				auto file = getArchive()->open(XdevLOpenForReadOnly(), sound1FileName);

				// To store sounds we need a buffer so let's create one.
				abuffer = getAudio()->createAudioBufferFromFile(file);
				if(abuffer == nullptr) {
					std::cerr << "Could not create audio buffer.\n";
					exit(RET_FAILED);
				}
				file->close();

				// Ok, now we need a source in the virtual environment that emits the sounds.
				// Usualy in the real world everything is a sound emitter. Every object can
				// create sound and the class XdevLAudioSource is excatly for that. We can
				// give a location and speed to a source.
				sound_source_1 = getAudio()->createAudioSource(abuffer);
				if(sound_source_1 == nullptr) {
					exit(RET_FAILED);
				}

				XdevLFileName sound2FileName("media/loop_b.wav");
				if(getArchive()->fileExists(sound2FileName) == xdl_false) {
					exit(RET_FAILED);
				}

				file = getArchive()->open(XdevLOpenForReadOnly(), sound2FileName);

				bbuffer = getAudio()->createAudioBufferFromFile(file);
				if(bbuffer == nullptr) {
					std::cerr << "Could not create audio buffer.\n";
					exit(RET_FAILED);
				}
				file->close();

				sound_source_2 = getAudio()->createAudioSource(bbuffer);
				if(sound_source_2 == nullptr) {
					std::cerr << "Could not create audio source for audio buffer.\n";
					exit(RET_FAILED);
				}

				// Set the loop state.
				sound_source_1->setLoop(true);
				sound_source_2->setLoop(true);

				std::cout << "Press F1: play/pause first sound\n";
				std::cout << "Press F2: play/pause second sound\n";
				std::cout << "Press I/D increase/decrease the main volume.\n";
				std::cout << "Sounds by (c) 2009 Cengiz Terzibas.\n\n";

				getAudio()->setGain(0.5f);

//	sound_source_1->setAudioBuffer(abuffer);
//	sound_source_2->setAudioBuffer(bbuffer);
				sound_source_1->setLoop(true);

				// Start playing sound.
				sound_source_1->play();

			}

			void initInputActions(IPXdevLInputActionSystem& actionSystem) override final {
				Super::initInputActions(actionSystem);

				auto incraseVolumeDelegate = XdevLActionDelegateType::Create<AudioDemoApp, &AudioDemoApp::increaseVolumeEvent>(this);
				auto decreaseVolumeDelegate = XdevLActionDelegateType::Create<AudioDemoApp, &AudioDemoApp::decreaseVolumeEvent>(this);
				auto playSoundDelegate = XdevLActionDelegateType::Create<AudioDemoApp, &AudioDemoApp::playSoundEvent>(this);
				auto playSound2Delegate = XdevLActionDelegateType::Create<AudioDemoApp, &AudioDemoApp::playSound2Event>(this);

				actionSystem->registerButtonAction(XdevLString(TEXT("IncreaseVolume")), KEY_I, incraseVolumeDelegate);
				actionSystem->registerButtonAction(XdevLString(TEXT("DecreaseVolume")), KEY_D, decreaseVolumeDelegate);
				actionSystem->registerButtonAction(XdevLString(TEXT("PlaySound")), KEY_F1, playSoundDelegate);
				actionSystem->registerButtonAction(XdevLString(TEXT("PlaySound2")), KEY_F2, playSound2Delegate);
			}

			void increaseVolumeEvent(const XdevLDelegateActionParameter& actionParameter) {
				sound_source_gain += 0.1f;
				getAudio()->setGain(sound_source_gain);
			}

			void decreaseVolumeEvent(const XdevLDelegateActionParameter& actionParameter) {
				sound_source_gain -= 0.1f;
				getAudio()->setGain(sound_source_gain);
			}

			void playSoundEvent(const XdevLDelegateActionParameter& actionParameter) {
				if(actionParameter.state == BUTTON_RELEASED) {
					play_pause = !play_pause;

					if(play_pause)
						sound_source_1->play();
					else
						sound_source_1->pause();
				}
			}

			void playSound2Event(const XdevLDelegateActionParameter& actionParameter) {
				if(actionParameter.state == BUTTON_RELEASED) {
					play_pause2 = !play_pause2;

					if(play_pause2)
						sound_source_2->play();
					else
						sound_source_2->pause();
				}
			}

			void tick(xdl_float dT) override final {

				std::cout << std::setprecision(3);

				xdl_int play_time = static_cast<xdl_int>(sound_source_1->getPlayPosSec() *1000);
				std::cout << "Loop Time1 " << play_time/60000 << ":" << play_time/1000%60 <<":" << play_time/10%100;
				xdl_int play_time2 = static_cast<xdl_int>(sound_source_2->getPlayPosSec() *1000);
				std::cout << std::setw(5) << "  Loop Time2 " << play_time2/60000 << ":" << play_time2/1000%60 <<":" << play_time2/10%100;
				std::cout << "\r";
			}


		private:

			audio::IPXdevLAudioBuffer abuffer = nullptr;
			audio::IPXdevLAudioBuffer bbuffer = nullptr;
			audio::IPXdevLAudioSource sound_source_1 = nullptr;
			audio::IPXdevLAudioSource sound_source_2 = nullptr;
			xdl_bool play_pause;
			xdl_bool play_pause2;
			xdl_float sound_source_gain;
	};

}

XdevLStartMainXML(xdl::AudioDemoApp, xdl::XdevLWindowTitle(), xdl::XdevLFileName(TEXT("audio_demo.xml")))
