/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLApplication.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLFont/XdevLFont.h>
#include <XdevLFont/XdevLFontSystem.h>
#include <XdevLFont/XdevLTextLayout.h>
#include <XdevLFileSystem/XdevLArchive.h>

#include <cmath>
#include <tm/tm.h>

class XDEVL_VERTEX_COLOR;


static const xdl::xdl_float g_vertex_buffer_data[] = {
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,

	1.0f, 1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,

	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,

	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,

	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,

	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,

	-1.0f, 1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,

	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,

	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f
};

static const xdl::xdl_float g_normal_buffer_data[] = {
	-1.0f, 0.0f,0.0f,
	-1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f,

	0.0f, 0.0f,-1.0f,
	0.0f, 0.0f,-1.0f,
	0.0f, 0.0f,-1.0f,

	0.0f, -1.0f, 0.0f,
	0.0f, -1.0f,0.0f,
	0.0f, -1.0f,0.0f,

	0.0f, 0.0f,-1.0f,
	0.0f, 0.0f,-1.0f,
	0.0f, 0.0f,-1.0f,

	-1.0f, 0.0f,0.0f,
	-1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f,0.0f,

	0.0f,-1.0f, 0.0f,
	0.0f,-1.0f, 0.0f,
	0.0f,-1.0f,0.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,

	1.0f, 0.0f, 0.0f,
	1.0f,0.0f,0.0f,
	1.0f, 0.0f,0.0f,

	1.0f,0.0f,0.0f,
	1.0f, 0.0f, 0.0f,
	1.0f,0.0f, 0.0f,

	0.0f, 1.0f, 0.0f,
	0.0f, 1.0f,0.0f,
	0.0f, 1.0f,0.0f,

	0.0f, 1.0f, 0.0f,
	0.0f, 1.0f,0.0f,
	0.0f, 1.0f, 0.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f
};

static const xdl::xdl_float g_color_buffer_data[] = {
	0.583f,  0.771f,  0.014f, 1.0f,
	0.609f,  0.115f,  0.436f, 1.0f,
	0.327f,  0.483f,  0.844f, 1.0f,
	0.822f,  0.569f,  0.201f, 1.0f,
	0.435f,  0.602f,  0.223f, 1.0f,
	0.310f,  0.747f,  0.185f, 1.0f,
	0.597f,  0.770f,  0.761f, 1.0f,
	0.559f,  0.436f,  0.730f, 1.0f,
	0.359f,  0.583f,  0.152f, 1.0f,
	0.483f,  0.596f,  0.789f, 1.0f,
	0.559f,  0.861f,  0.639f, 1.0f,
	0.195f,  0.548f,  0.859f, 1.0f,
	0.014f,  0.184f,  0.576f, 1.0f,
	0.771f,  0.328f,  0.970f, 1.0f,
	0.406f,  0.615f,  0.116f, 1.0f,
	0.676f,  0.977f,  0.133f, 1.0f,
	0.971f,  0.572f,  0.833f, 1.0f,
	0.140f,  0.616f,  0.489f, 1.0f,
	0.997f,  0.513f,  0.064f, 1.0f,
	0.945f,  0.719f,  0.592f, 1.0f,
	0.543f,  0.021f,  0.978f, 1.0f,
	0.279f,  0.317f,  0.505f, 1.0f,
	0.167f,  0.620f,  0.077f, 1.0f,
	0.347f,  0.857f,  0.137f, 1.0f,
	0.055f,  0.953f,  0.042f, 1.0f,
	0.714f,  0.505f,  0.345f, 1.0f,
	0.783f,  0.290f,  0.734f, 1.0f,
	0.722f,  0.645f,  0.174f, 1.0f,
	0.302f,  0.455f,  0.848f, 1.0f,
	0.225f,  0.587f,  0.040f, 1.0f,
	0.517f,  0.713f,  0.338f, 1.0f,
	0.053f,  0.959f,  0.120f, 1.0f,
	0.393f,  0.621f,  0.362f, 1.0f,
	0.673f,  0.211f,  0.457f, 1.0f,
	0.820f,  0.883f,  0.371f, 1.0f,
	0.982f,  0.099f,  0.879f, 1.0f
};

class MyGLSLApplication : public xdl::XdevLApplication {
	public:

		MyGLSLApplication(const xdl::XdevLWindowTitle& title, int argc, char** argv, const xdl::XdevLFileName& xml_filename) throw() :
			Super(title, argc, argv, xml_filename),
			m_frameBuffer(nullptr),
			m_frameBufferArray(nullptr),
			m_modelMatrix(0),
			m_projViewMatrix(0),
			m_frameBufferProjectionMatrix(0),
			m_frameBufferTexture(0),
			m_leftMouseButton(xdl::xdl_false),
			m_rx(0.0f),
			m_ry(0.0f) {
		}

		void initInputActions(xdl::IPXdevLInputActionSystem& inputActionSystem) override final {
			Super::initInputActions(inputActionSystem);


			xdl::XdevLAxisActionDelegateType mouseAxisX = xdl::XdevLAxisActionDelegateType::Create<MyGLSLApplication, &MyGLSLApplication::handleAxisX>(this);
			xdl::XdevLAxisActionDelegateType mouseAxisY = xdl::XdevLAxisActionDelegateType::Create<MyGLSLApplication, &MyGLSLApplication::handleAxisY>(this);
			xdl::XdevLActionDelegateType activateRotationDelegate = xdl::XdevLActionDelegateType::Create<MyGLSLApplication, &MyGLSLApplication::handleActivateRotation>(this);


			inputActionSystem->registerAxisAction(xdl::XdevLString(TEXT("MouseAxisX")), xdl::AXIS_0, mouseAxisX);
			inputActionSystem->registerAxisAction(xdl::XdevLString(TEXT("MouseAxisY")), xdl::AXIS_1, mouseAxisY);
			inputActionSystem->registerButtonAction(xdl::XdevLString(TEXT("ActivateRotation")), xdl::BUTTON_0, activateRotationDelegate);


		}

		void preInit(const Arguments& argv) {
			if(initRenderDevice() != xdl::RET_SUCCESS) {
				return;
			}

			if(initFramebuffer() != xdl::RET_SUCCESS) {
				return;
			}

			if(initRenderAssets() != xdl::RET_SUCCESS) {
				return;
			}

			getWindow()->show();
		}

		void tick(xdl::xdl_float dT) throw() override final {
			handleGraphics(getCore()->getDT());
		}

		virtual xdl::xdl_int notify(xdl::XdevLEvent& event) {
			switch(event.type) {
				case xdl::XDEVL_WINDOW_EVENT: {
					switch(event.window.event) {
						case xdl::XDEVL_WINDOW_RESIZED: {
							createScreenVertexArray(getWindow());
						}
						break;
					}
				}
				break;
			}
			return Super::notify(event);
		}

		void handleGraphics(xdl::xdl_double dT) {

			//
			// Let's render stuff into the framebuffer object with low resolution.
			//
			m_frameBuffer->activate();

			xdl::XdevLFrameBufferColorTargets list[] = {xdl::XdevLFrameBufferColorTargets::TARGET0};
			m_frameBuffer->activateColorTargets(1, list);

			m_frameBuffer->clearColorTargets(0.0f, 0.305f, 0.596f, 1.0f);
			m_frameBuffer->setActiveDepthTest(xdl::xdl_true);
			m_frameBuffer->clearDepthTarget(1.0f);

			xdl::xdl_float aspect_ratio = (xdl::xdl_float)getWindow()->getWidth()/(xdl::xdl_float)getWindow()->getHeight();

			tmath::mat4 proj, rotx, roty, trans, model;
			tmath::perspective(90.0f, aspect_ratio, 1.0f, 110.0f, proj);

			tmath::rotate_x(m_rx, rotx);
			tmath::rotate_y(m_ry, roty);
			tmath::translate(0.0f, 0.0f, -3.0f, trans);

			model = trans * rotx * roty;

			m_sp->activate();
			m_sp->setUniformMatrix4(m_projViewMatrix, 1, proj);
			m_sp->setUniformMatrix4(m_modelMatrix, 1, model);
			m_sp->deactivate();

			getRAI()->setActiveVertexArray(m_va);
			getRAI()->setActiveShaderProgram(m_sp);

			getRAI()->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLES, 36);

			//
			// Now stop rendering into the frambuffer object.
			//
			m_frameBuffer->deactivate();

			//
			// Render into the second half of the normal framebuffer.
			//

			xdl::XdevLViewPort viewportDefaultFramebuffer {};
			viewportDefaultFramebuffer.x = 0.0f;
			viewportDefaultFramebuffer.y = 0.0f;
			viewportDefaultFramebuffer.width = ((xdl::xdl_float)getWindow()->getWidth())/2.0f;
			viewportDefaultFramebuffer.height = (xdl::xdl_float)getWindow()->getHeight();
			getRAI()->setViewport(viewportDefaultFramebuffer);

			getRAI()->clearColorTargets(0.0f, 0.305f, 0.596f, 1.0f);
			getRAI()->clearDepthTarget(1.0f);

			getRAI()->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLES,36);

			//
			// Use the rendered texture from the framebuffer object to render into a squad.
			// Render that squad into the first half of the viewport.
			//

			xdl::XdevLViewPort viewportQuad {};
			viewportQuad.x = ((xdl::xdl_float)getWindow()->getWidth())/2.0f;
			viewportQuad.y = 0.0f;
			viewportQuad.width = ((xdl::xdl_float)getWindow()->getWidth())/2.0f;
			viewportQuad.height = (xdl::xdl_float)getWindow()->getHeight();
			getRAI()->setViewport(viewportQuad);

			getRAI()->setActiveDepthTest(xdl::xdl_false);
			tmath::mat4 fbProjection;
			tmath::ortho(0.0f,
			             (float)getWindow()->getWidth(),
			             0.0f,
			             (float)getWindow()->getHeight(),
			             -1.0f,
			             1.0f, fbProjection);

			m_frameBufferSP->activate();
			m_frameBufferSP->setUniformMatrix4(m_frameBufferProjectionMatrix, 1, fbProjection);
			m_frameBufferSP->setUniformi(m_frameBufferTexture, 0);
			m_frameBuffer->getTexture(0)->activate(xdl::XdevLTextureStage::STAGE0);
			m_frameBufferSP->deactivate();

			getRAI()->setActiveShaderProgram(m_frameBufferSP);
			getRAI()->setActiveVertexArray(m_frameBufferArray);

			getRAI()->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLES, 6);


			xdl::XdevLViewPort viewportFont {};
			viewportFont.x = 0.0f;
			viewportFont.y = 0.0f;
			viewportFont.width = (xdl::xdl_float)getWindow()->getWidth();
			viewportFont.height = (xdl::xdl_float)getWindow()->getHeight();
			getRAI()->setViewport(viewportFont);

			std::wstringstream tmp;
			tmp << L"OpenGL Demo using XdevLFont: FPS: " << getFPS();
			xdl::XdevLString fps = xdl::XdevLString::wideToString(tmp.str());

			textLayoutSystem->setColor(255, 255, 255, 255);
			textLayoutSystem->printText(fps, xdl::XdevLPositionF(-1.0f, 0.9f));

			getRAI()->swapBuffers();

		}

		//
		// Initialize the rendering device.
		//
		xdl::xdl_int initRenderDevice() {

			// Get the FontSystem
			fontSystem = xdl::getModule<xdl::XdevLFontSystem*>(getCore(), xdl::XdevLID("MyFontSystem"));
			if(!fontSystem) {
				return xdl::RET_FAILED;
			}

			// Get the Text Layout System.
			textLayoutSystem = xdl::getModule<xdl::XdevLTextLayout*>(getCore(), xdl::XdevLID("MyTextLayout"));
			if(!textLayoutSystem) {
				return xdl::RET_FAILED;
			}

			// We tell the archive to load files relative to the apps position.
			getArchive()->mount(xdl::XdevLFileName("."));
			getArchive()->setWriteDir(xdl::XdevLFileName("."));

			return xdl::RET_SUCCESS;
		}

		void handleActivateRotation(const xdl::XdevLDelegateActionParameter& actionParameter) {
			m_leftMouseButton = actionParameter.state == xdl::BUTTON_PRESSED ? xdl::xdl_true : xdl::xdl_false;
		}



		void handleFullscreenApplication(const xdl::XdevLDelegateActionParameter& actionParameter) {
			if((actionParameter.state == xdl::BUTTON_PRESSED) &&
			    (actionParameter.mod & xdl::KEY_MOD_LCTRL) &&
			    (actionParameter.mod & xdl::KEY_MOD_LALT)) {
				static xdl::xdl_bool fullscreen = xdl::xdl_false;
				fullscreen = !fullscreen;

				getWindow()->setFullscreen(fullscreen);
			}
		}

		void handleAxisX(const xdl::XdevLDelegateAxisActionParameter& actionParamter) {
			if(m_leftMouseButton) {
				m_ry += static_cast<float>(actionParamter.rel) * 0.01f;
			}
		}

		void handleAxisY(const xdl::XdevLDelegateAxisActionParameter& actionParamter) {
			if(m_leftMouseButton) {
				m_rx += static_cast<float>(-actionParamter.rel) * 0.01f;
			}
		}

		//
		// Initialize the framebuffer.
		//
		xdl::xdl_int initFramebuffer() {

			// Create a new framebuffer instance.
			m_frameBuffer = getRAI()->createFrameBuffer();

			// Initialize the framebuffer so that it is 8 times smaller then the specified window.
			auto result = m_frameBuffer->init(getWindow()->getWidth()/8, getWindow()->getHeight()/8);

			// Add a color render target to the framebuffer. We need that because we want to render colors right :D.
			m_frameBuffer->addColorTarget(0	, xdl::XdevLFrameBufferColorFormat::RGBA8);

			// Set how the color render target texture is filtered.
			auto texture = m_frameBuffer->getTexture(0);
			texture->lock();
			xdl::XdevLTextureSamplerFilterStage filter {};
			filter.magFilter = xdl::XdevLTextureFilterMag::LINEAR;
			filter.minFilter = xdl::XdevLTextureFilterMin::NEAREST;
			texture->setTextureFilter(filter);
			texture->unlock();

			// Add Depth target to the framebuffer. Without it we can't use depth test.
			m_frameBuffer->addDepthTarget(xdl::XdevLFrameBufferDepthFormat::COMPONENT24);

			createScreenVertexArray(getWindow());

			auto vertexShaderfile = getArchive()->open(xdl::XdevLOpenForReadOnly(),xdl::XdevLFileName("frameBuffer_vs.glsl"));
			auto vertexFragmentfile = getArchive()->open(xdl::XdevLOpenForReadOnly(),xdl::XdevLFileName("frameBuffer_fs.glsl"));

			m_frameBufferSP = getRAI()->createShaderProgram();
			m_frameBufferVS = getRAI()->createVertexShader();
			m_frameBufferFS = getRAI()->createFragmentShader();

			m_frameBufferVS->addShaderCode(vertexShaderfile);
			m_frameBufferFS->addShaderCode(vertexFragmentfile);

			m_frameBufferVS->compile();
			m_frameBufferFS->compile();

			result = m_frameBufferSP->attach(m_frameBufferVS);
			if(xdl::RET_SUCCESS != result) {
				return result;
			}

			result = m_frameBufferSP->attach(m_frameBufferFS);
			if(xdl::RET_SUCCESS != result) {
				return result;
			}

			result = m_frameBufferSP->link();
			if(xdl::RET_SUCCESS != result) {
				return result;
			}

			m_frameBufferProjectionMatrix = m_frameBufferSP->getUniformLocation("projMatrix");
			m_frameBufferTexture = m_frameBufferSP->getUniformLocation("texture0");

			return xdl::RET_SUCCESS;
		}

		//
		// Initialize the rendering assets.
		//
		xdl::xdl_int initRenderAssets() {

			m_vd = getRAI()->createVertexDeclaration();
			m_vd->add(3, xdl::XdevLBufferElementTypes::FLOAT, 0);		// Position
			m_vd->add(4, xdl::XdevLBufferElementTypes::FLOAT, 1);		// Color
			m_vd->add(3, xdl::XdevLBufferElementTypes::FLOAT, 2);		// Normal

			std::vector<xdl::xdl_uint8*> list;
			list.push_back((xdl::xdl_uint8*)g_vertex_buffer_data);
			list.push_back((xdl::xdl_uint8*)g_color_buffer_data);
			list.push_back((xdl::xdl_uint8*)g_normal_buffer_data);


			m_va = getRAI()->createVertexArray();
			auto result = m_va->init(list.size(), list.data(), 36, m_vd);
			if(xdl::RET_SUCCESS != result) {
				return result;
			}

			// Create the shader program.
			m_sp = getRAI()->createShaderProgram();
			m_vs = getRAI()->createVertexShader();
			m_fs = getRAI()->createFragmentShader();

			auto vertexShaderfile = getArchive()->open(xdl::XdevLOpenForReadOnly(),xdl::XdevLFileName("vs1.vs"));
			auto vertexFragmentfile = getArchive()->open(xdl::XdevLOpenForReadOnly(),xdl::XdevLFileName("fs1.fs"));

			m_vs->addShaderCode(vertexShaderfile);
			m_fs->addShaderCode(vertexFragmentfile);

			result = m_sp->attach(m_vs);
			if(xdl::RET_SUCCESS != result) {
				return result;
			}

			result = m_sp->attach(m_fs);
			if(xdl::RET_SUCCESS != result) {
				return result;
			}

			m_vs->compile();
			m_fs->compile();

			result = m_sp->link();
			if(xdl::RET_SUCCESS != result) {
				return result;
			}


			// Get the vertex attributes for the array.

			m_modelMatrix		= m_sp->getUniformLocation("modelMatrix");
			m_projViewMatrix	= m_sp->getUniformLocation("projViewMatrix");

			//
			// Initialize font system.
			//

			xdl::XdevLFontSystemCreateParameter fontSystemCreateParameter {};
			fontSystemCreateParameter.rai = getRAI();
			fontSystem->create(fontSystemCreateParameter);

			auto fontFile = getArchive()->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName("fonts/default_info.txt"));
			auto font = fontSystem->createFromFontFile(fontFile);
			if(nullptr == font) {
				return xdl::RET_FAILED;
			}

			xdl::XdevLTextLayoutCreateParameter textLayoutCreateParameter;
			textLayoutCreateParameter.rai = getRAI();
			textLayoutCreateParameter.window = getWindow();
			textLayoutSystem->create(textLayoutCreateParameter);
			textLayoutSystem->setScale(1.0f);
			textLayoutSystem->setDFT(0);
			textLayoutSystem->setEffect(0);
			textLayoutSystem->useFont(font);

			return xdl::RET_SUCCESS;
		}


		void createScreenVertexArray(xdl::IPXdevLWindow window) {

			xdl::xdl_float screen_vertex [] = {
				0.0f, 0.0f,
				(xdl::xdl_float)window->getWidth(), 0.0f,
				(xdl::xdl_float)window->getWidth(), (xdl::xdl_float)window->getHeight(),

				(xdl::xdl_float)window->getWidth(), (xdl::xdl_float)window->getHeight(),
				0.0f, (xdl::xdl_float)window->getHeight(),
				0.0f, 0.0f
			};

			xdl::xdl_float screen_uv [] = {
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,

				1.0f, 1.0f,
				0.0f, 1.0f,
				0.0f, 0.0f
			};

			m_vertexDeclaration = getRAI()->createVertexDeclaration();
			m_vertexDeclaration->add(2, xdl::XdevLBufferElementTypes::FLOAT, 0);
			m_vertexDeclaration->add(2, xdl::XdevLBufferElementTypes::FLOAT, 9);

			std::vector<xdl::xdl_uint8*> list2;
			list2.push_back((xdl::xdl_uint8*)screen_vertex);
			list2.push_back((xdl::xdl_uint8*)screen_uv);

			m_frameBufferArray = getRAI()->createVertexArray();
			m_frameBufferArray->init(list2.size(), list2.data(), 6, m_vertexDeclaration);
		}

	private:

		xdl::IPXdevLFrameBuffer m_frameBuffer;
		xdl::IPXdevLVertexDeclaration m_vertexDeclaration;
		xdl::IPXdevLVertexArray m_frameBufferArray;
		xdl::IPXdevLVertexShader m_frameBufferVS;
		xdl::IPXdevLFragmentShader m_frameBufferFS;
		xdl::IPXdevLShaderProgram m_frameBufferSP;
		xdl::IPXdevLFontSystem fontSystem;
		xdl::IPXdevLTextLayout textLayoutSystem;
		xdl::IPXdevLVertexArray m_va;
		xdl::IPXdevLVertexDeclaration m_vd;
		xdl::IPXdevLVertexShader m_vs;
		xdl::IPXdevLFragmentShader m_fs;
		xdl::IPXdevLShaderProgram m_sp;
		xdl::xdl_int m_modelMatrix;
		xdl::xdl_int m_projViewMatrix;
		xdl::xdl_int m_frameBufferProjectionMatrix;
		xdl::xdl_int m_frameBufferTexture;
		xdl::xdl_bool m_leftMouseButton;
		xdl::xdl_float m_rx;
		xdl::xdl_float m_ry;

};


XdevLStartMainXML(MyGLSLApplication, xdl::XdevLWindowTitle(), xdl::XdevLFileName(TEXT("glsl_demo.xml")))
