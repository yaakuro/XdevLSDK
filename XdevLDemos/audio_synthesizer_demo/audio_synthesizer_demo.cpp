/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLAudio/XdevLAudio.h>

#include <signal.h>
#include <cmath>
#include <map>

xdl::IPXdevLCore core = nullptr;
xdl::audio::XdevLAudioCapture* alsa_capture = nullptr;
xdl::audio::XdevLAudioPlayback* alsa_playback = nullptr;


/**
 * @class NotePitch
 * @brief Holds the pitch of a note.
 */
struct NotePitch {
	NotePitch(xdl::xdl_double p) : pitch(p) {}
	xdl::xdl_double pitch;

	operator xdl::xdl_double() {
		return pitch;
	}
};

/**
 * @class NoteVelocity
 * @brief Holds the velocity of a note.
 */
struct NoteVelocity {
	NoteVelocity(xdl::xdl_double v) : velocity(v) {}
	xdl::xdl_double velocity;

	operator xdl::xdl_double() {
		return velocity;
	}
};

/**
 * @class Note
 * @brief Represents a note.
 */
class Note {
	public:
		Note()
			: pitch(440.0)
			, velocity(0.1) {}

		Note(NotePitch& p)
			: pitch(p)
			, velocity(0.1) {}

		Note(NotePitch& p, NoteVelocity& v)
			: pitch(p)
			, velocity(v) {}



		NotePitch pitch;
		NoteVelocity velocity;
};

/**
 * @class Oszillator
 * @brief Interfarce for an Oszillator.
 */
class Oszillator {
	public:
		Oszillator()
			: samplingRate(44100.0f)
			, phase(0.0f) {}

		virtual xdl::xdl_float calculate(Note& note) = 0;

	protected:

		xdl::xdl_float samplingRate;
		xdl::xdl_float phase;
};

/**
 * @class OszillatorSine
 * @brief Implementation of a Sine Oszillator.
 */
class OszillatorSine : public Oszillator {
	public:
		xdl::xdl_float calculate(Note& note) override final {
			phase += (note.pitch * 2.0 * M_PI)/samplingRate;
			if(phase > 2 * M_PI) {
				phase = phase - (2.0 * M_PI);
			}
			return sin(phase) * note.velocity;
		}
};

/**
 * @class OszillatorSawtooth
 * @brief Implementation of a Sawtooth Oszillator.
 */
class OszillatorSawtooth : public Oszillator {
	public:
		xdl::xdl_float calculate(Note& note) override final {
			phase += (note.pitch * 2.0 * M_PI) / samplingRate;
			if(phase > 2 * M_PI) {
				phase = phase - (2.0 * M_PI);
			}
			return (1.0 - (1.0/M_PI) * phase) * note.velocity;
		}
};

/**
 * @class OszillatorTriangle
 * @brief Implementation of a Triangle Oszillator.
 */
class OszillatorTriangle : public Oszillator {
	public:
		xdl::xdl_float calculate(Note& note) override final {
			phase += (note.pitch * 2.0 * M_PI)/ samplingRate;
			xdl::xdl_float y = 0.0;
			if(phase < M_PI) {
				y = -1.0 + (2.0/M_PI)*phase;
			} else {
				y = 3.0 - (2.0/M_PI)*phase;
			}
			if(phase > 2.0 * M_PI) {
				phase = phase - (2.0 * M_PI) * note.velocity;
			}
			return y * note.velocity;
		}
};

/**
 * @class OszillatorSquare
 * @brief Implementation of a Square Oszillator.
 */
class OszillatorSquare : public Oszillator {
	public:
		xdl::xdl_float calculate(Note& note) override final {
			phase += (note.pitch * 2.0 * M_PI)/ samplingRate;
			xdl::xdl_float y = 0.0;
			if(phase < M_PI) {
				y = 1.0;
			} else {
				y = -1.0;
			}
			if(phase > 2.0 * M_PI) {
				phase = phase - (2.0 * M_PI);
			}
			return y * note.velocity;
		}
};

/**
 * @class OszillatorNoise
 * @brief Implementation of a Noise Oszillator.
 */
class OszillatorNoise : public Oszillator {
	public:
		xdl::xdl_float calculate(Note& note) override final {
			return (1.0f/(xdl::xdl_float)RAND_MAX) * (xdl::xdl_float)rand() * note.velocity;
		}
};


void exitHandle(int signal) {

	// Destroy the Core.
	xdl::destroyCore(core);

	exit(0);
}

Note kammerton;
OszillatorSine oszillatorSine;
OszillatorSawtooth oszillatorSawtooth;
OszillatorTriangle oszillatorTriangle;
OszillatorSquare oszillatorSquare;
OszillatorNoise oszillatorNoise;

Oszillator* oszilator = &oszillatorSquare;


NotePitch NP_A_0(440.0);
NotePitch NP_A_Sharp_0(500.0);
NotePitch NP_B_0(540.0);
NotePitch NP_C_1(580.0);
Note A_0(NP_A_0);
Note A_Sharp_0(NP_A_Sharp_0);
Note A_B_0(NP_B_0);
Note A_C_1(NP_C_1);
Note* currentNote = &A_0;

std::map<xdl::XdevLButtonId, Note> pitchMap {
	{xdl::KEY_A, A_0},
	{xdl::KEY_W, A_Sharp_0},
	{xdl::KEY_S, A_B_0},
	{xdl::KEY_D, A_C_1}
};

xdl::xdl_uint callback(xdl::xdl_uint8* buffer, xdl::xdl_uint samples, void* userData) {
	xdl::xdl_float* sampleBuffer = (xdl::xdl_float*)buffer;
	for(xdl::xdl_uint sample = 0; sample != samples; sample++) {
		sampleBuffer[sample] = oszilator->calculate(*currentNote) ;
	}
	return samples;
}

void callback2(xdl::audio::XdevLAudioPlayback* playback) {
	xdl::xdl_float sampleBuffer[1024];
	for(xdl::xdl_uint sample = 0; sample != 1024; sample++) {
		((xdl::xdl_uint16*)sampleBuffer)[sample] = (xdl::xdl_uint16)oszilator->calculate(kammerton) ;
	}
	playback->write((xdl::xdl_uint8*)sampleBuffer, 1024);
}

int main(int argc, char *argv[]) {
	// Let's register the CTRL + c signal handler.
	if(signal(SIGINT, exitHandle) == SIG_ERR) {
		std::cerr <<  "Failed to set SIGINT handler." << std::endl;
		return -1;
	}

	// Register termination handler.
	if(signal(SIGTERM, exitHandle) == SIG_ERR) {
		std::cerr <<  "Failed to set SIGTERM handler." << std::endl;
		return -1;
	}

	if(xdl::createCore(&core, argc, argv) != xdl::RET_SUCCESS) {
		return -1;
	}


	if(core->plug(xdl::XdevLPluginName("XdevLAudioAlsa"), xdl::XdevLVersion(0, 7, 0)) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return -1;
	}

	alsa_playback = xdl::createModule<xdl::audio::XdevLAudioPlayback*>(core, xdl::XdevLModuleName("XdevLAudioPlayback"), xdl::XdevLID("MyAlsaPlayback"));
	if(isModuleNotValid(alsa_playback)) {
		xdl::destroyCore(core);
		return -1;
	}

	xdl::audio::IPXdevLAudioBuffer buffer2 = alsa_playback->createAudioBuffer(xdl::audio::AUDIO_BUFFER_FORMAT_FLOAT, xdl::audio::AUDIO_SAMPLE_RATE_44100, 1, 0, nullptr);
	if(nullptr == buffer2) {
		xdl::destroyCore(core);
		return -1;
	}

	alsa_playback->setPlaybackCallbackFunction(callback, nullptr);

	for(;;) {
		core->update();
		alsa_playback->update2();
//		callback2(alsa_playback);
	}

	xdl::destroyCore(core);
}
