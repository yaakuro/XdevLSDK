#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 vi_position;
layout(location = 1) in vec3 vi_normal;
layout(location = 2) in vec4 vi_color;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec3 fragNormal;

layout (binding = 0) uniform UBO 
{
	mat4 projectionMatrix;
	mat4 viewMatrix;
	mat4 modelMatrix;
} ubo;

void main() {
    fragColor = vi_color;
	fragNormal = normalize(ubo.modelMatrix * vec4(vi_normal.xyz, 0.0)).xyz;

    gl_Position = ubo.projectionMatrix * ubo.viewMatrix * ubo.modelMatrix * vi_position;
}