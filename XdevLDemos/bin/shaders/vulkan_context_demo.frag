#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 if_color;
layout(location = 1) in vec3 if_normal;

layout(location = 0) out vec4 outColor;

vec3 lightDir = vec3(0.0, 0.0, -1.0);

void main() {
	float fs = max(0.0, dot(if_normal, -lightDir));
    outColor = if_color * fs;
}