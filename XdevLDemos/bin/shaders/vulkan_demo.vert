#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 iposition;
layout(location = 1) in vec4 icolor;

layout(location = 0) out vec4 ocolor;

layout (binding = 0) uniform UBO 
{
	mat4 projectionMatrix;
	mat4 modelMatrix;
	mat4 viewMatrix;
} ubo;

void main() {
	ocolor = icolor;
	gl_Position = ubo.projectionMatrix * ubo.viewMatrix * ubo.modelMatrix * iposition;
}