#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 icolor;
layout(location = 0) out vec4 ocolor;

void main() {
	ocolor = icolor;
}