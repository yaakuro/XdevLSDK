layout(location = 0) in vec3 iposition;
layout(location = 1) in vec3 inormal;
layout(location = 2) in vec3 itangent;
layout(location = 3) in vec3 ibitagent;
layout(location = 4) in vec4 icolor;
layout(location = 9) in vec2 itexCoord;

layout (std140) uniform VertexShaderUniformBuffer {
	mat4 proj;
	mat4 model[100];
	mat4 view;
	vec3 lightPosition;
	vec3 cameraPosition;
} inUB;

out vec4 color;
out vec3 normal;
out vec2 texCoord;
out vec3 worldPosition;
out vec3 cameraPosition;
out vec3 lightPosition;
out vec3 worldDirectionToLight;
out vec3 worldDirectionToCamera;
out mat3 normalMatrix;
out mat3 TBN;

void main() {

	normalMatrix = mat3(inUB.model[gl_InstanceID]);

	worldPosition = (inUB.model[gl_InstanceID] * vec4(iposition, 1.0f)).xyz;


	// -------------------------------------------------------------------------
	// Calculate TBN matrix that transforma tanget space into world space.
	//
	vec3 T = normalize(inUB.model[gl_InstanceID] * vec4(itangent, 0.0)).xyz;
	vec3 N = normalize(inUB.model[gl_InstanceID] * vec4(inormal, 0.0)).xyz;
	normal = N;

	// Gram Schmidt orthogonalize.
	T = normalize(T - dot(T, N) * N);
	vec3 B = cross(T, N);

	TBN = mat3(T, B, N);
	// -------------------------------------------------------------------------

	//
	// Calculate the lights and view direction in world space.
	//
	worldDirectionToLight = normalize(inUB.lightPosition - worldPosition);

	worldDirectionToCamera = inUB.cameraPosition - worldPosition;
	worldDirectionToCamera = normalize(inUB.view * vec4(worldDirectionToCamera, 1.0)).xzy;

	lightPosition = inUB.lightPosition;
	cameraPosition = inUB.cameraPosition;
	texCoord = itexCoord;
	color = icolor;

	// (Clip space) = (Transform into Clip space) * (Transform into Camera Space) * (Transform into World space) * (Local Space)
	gl_Position = inUB.proj * inUB.view * inUB.model[gl_InstanceID] * vec4(iposition, 1.0f);
}
