in vec4 color;
in vec3 normal;
in vec2 texCoord;
in vec3 worldDirectionToLight;
in vec3 worldDirectionToCamera;
in mat3 normalMatrix;
in mat3 TBN;
in vec3 worldPosition;
in vec3 cameraPosition;
in vec3 lightPosition;

#define NORMAL_MAPPING 1
#define PARALLAX_MAPPING 2


layout (std140) uniform FragmentShaderUniformBuffer {
	int mappingStates;
	float parallaxScale;
	float parallaxBias;
	float steepParallaxSteps;

	float specularComponent;
	int lit;
} inUB;

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform sampler2D heightTexture;

layout(location = 0) out vec4 ocolor;

vec4 ambientLighting = vec4(0.1, 0.1, 0.1, 1.0);
vec3 specularColor = vec3(0.5, 0.5, 0.5);

//
// Here we are calculating texture coordinate offsets to fake more depth perspective when viewing
// textures from specific angles. It is only an approximation.
//
vec2 calcParallaxTextureCoords(sampler2D heightMapTexture, vec3 V, mat3 TBN, vec2 texCoords, float heightScale, float bias) {

	// Get the height value from the texture.
	float height = heightScale * texture(heightMapTexture, texCoords).r - bias;

	// Transform the view vector into tangent space and multiplicate the height.
	vec2 offset = (TBN * V).xy * height;

	// Return new texture coordinates removing the parallax offset from it.
	return texCoords - offset;
}


vec2 calcSteepParallaxTextureCoords(sampler2D heightMapTexture, vec3 V, mat3 TBN, vec2 texCoords, float heightScale, float bias) {

	float numberOfLayers = inUB.steepParallaxSteps;
	float layerHeight = 1.0 / numberOfLayers;
	vec2 dtex = heightScale * ((V * TBN) .xy / numberOfLayers);

	float currentHeight = 0.0f;
	vec2 textureCoordinate = texCoords;

	float heightMapValue = texture(heightMapTexture, texCoords).r - bias;
	while(currentHeight < heightMapValue) {

		textureCoordinate -= dtex;

		heightMapValue = texture(heightMapTexture, texCoords).r - bias;

		currentHeight += layerHeight;

	}
	return textureCoordinate;
}

vec3 blinn_phong(float ndotl, float ndoth, vec3 specularColor, vec3 diffuse, float fSpecularExponent) {

	// Compute the diffuse term
	float diffuseCoefficient = ndotl ;
	vec3 cdiffuse = diffuseCoefficient * diffuse;


	float specularCoefficient = 0.0;
	if(diffuseCoefficient > 0.0) {
		specularCoefficient = pow(ndoth, fSpecularExponent);
	}

	// Compute the specular colour
	vec3 cspecular = specularCoefficient * specularColor;

	return (cdiffuse + cspecular);
}

void main(void) {

	if(inUB.lit == 1) {

		vec3 L = normalize(worldDirectionToLight);
		vec3 V = normalize(worldDirectionToCamera);
		vec3 H = normalize(L + V);
		vec3 N = normal;

		vec2 parallaxTextureCoordinates = texCoord;
		if( (inUB.mappingStates & PARALLAX_MAPPING) == PARALLAX_MAPPING) {
			mat3 TBNINV = transpose(TBN);
			parallaxTextureCoordinates = calcParallaxTextureCoords(heightTexture, V, TBNINV, texCoord, inUB.parallaxScale, inUB.parallaxBias);
	//		parallaxTextureCoordinates = calcSteepParallaxTextureCoords(heightTexture,  V, TBNINV, texCoord, inUB.parallaxScale, inUB.parallaxBias);
		}

		// Get the normal from the normal map and convert from color to normal space and trasform into model space.
		if((inUB.mappingStates & NORMAL_MAPPING) == NORMAL_MAPPING) {
			N = normalize(TBN * normalize(2.0*texture(normalTexture, parallaxTextureCoordinates).xyz - 1.0));
		}

		vec4 diffuse = texture(diffuseTexture, parallaxTextureCoordinates);

		float NdotL = max(0.0, dot(N, L));
		float NdotH = max(0.0, dot(N, H));

		vec4 fragment = vec4(blinn_phong(NdotL, NdotH, specularColor, diffuse.xyz, inUB.specularComponent), 1);
		ocolor = fragment;
	//	ocolor = vec4(worldPosition, 1.0f);
	//	float gama = 2.2f;
	//	ocolor.rgb = pow( fragment.rgb, vec3(1.0/gama) );
	//	ocolor.a = fragment.a;
	} else {
		ocolor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	}
}
