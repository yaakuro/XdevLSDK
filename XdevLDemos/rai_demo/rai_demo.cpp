/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of 
	this software and associated documentation files (the "Software"), to deal in the 
	Software without restriction, including without limitation the rights to use, copy, 
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
	and to permit persons to whom the Software is furnished to do so, subject to the 
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies 
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLFileSystem/XdevLArchive.h>

#include <XdevLWindow/XdevLWindow.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLUI/XdevLUI.h>

#include <XdevLInput/XdevLMouse/XdevLMouse.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>
#include <iomanip>

#include <tm/tm.h>
#include <XdevLOpenGLContext/GL/glew.h>

xdl::IPXdevLCore core = nullptr;
xdl::IPXdevLWindow window = nullptr;
xdl::IPXdevLCursor cursor = nullptr;
xdl::IPXdevLKeyboard keyboard = nullptr;
xdl::IPXdevLMouse mouse = nullptr;
xdl::IPXdevLRAI rai = nullptr;
xdl::IPXdevLArchive archive = nullptr;
xdl::ui::IPXdevLUI ui = nullptr;

std::string vertex_shader_source = {
	"\
layout(location = 0) in vec3 iposition;\
layout(location = 1) in vec4 icolor;\
layout(location = 2) in vec3 inormal;\
\
layout (std140) uniform VertexShaderUniformBuffer {\
mat4 model;\
mat4 proj;\
vec4 modulationColor;\
};\
\
out vec4 color;\
out vec3 normal;\
\
void main() {\
\
	color = icolor * modulationColor;\
\
	normal = normalize(model*vec4(inormal, 0.0)).xyz;\
\
    gl_Position = proj * model * vec4(iposition, 1.0);\
}"
};

std::string fragment_shader_source = {
	"\
in vec4 color;\
in vec3 normal;\
\
vec3 lightDir = vec3(0.0, 0.0, -1.0);\
\
layout(location = 0) out vec4 ocolor;\
\
void main() {\
\
	float fs = max(0.0, dot(normal, -lightDir));\
\
    ocolor = fs*color;\
}"
};


static const float g_vertex_buffer_data[] = {
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,

	1.0f, 1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,

	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,

	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,

	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,

	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,

	-1.0f, 1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,

	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,

	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f
};

static const float g_normal_buffer_data[] = {
	-1.0f, 0.0f,0.0f,
	-1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f,

	0.0f, 0.0f,-1.0f,
	0.0f, 0.0f,-1.0f,
	0.0f, 0.0f,-1.0f,

	0.0f, -1.0f, 0.0f,
	0.0f, -1.0f,0.0f,
	0.0f, -1.0f,0.0f,

	0.0f, 0.0f,-1.0f,
	0.0f, 0.0f,-1.0f,
	0.0f, 0.0f,-1.0f,

	-1.0f, 0.0f,0.0f,
	-1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f,0.0f,

	0.0f,-1.0f, 0.0f,
	0.0f,-1.0f, 0.0f,
	0.0f,-1.0f,0.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,

	1.0f, 0.0f, 0.0f,
	1.0f,0.0f,0.0f,
	1.0f, 0.0f,0.0f,

	1.0f,0.0f,0.0f,
	1.0f, 0.0f, 0.0f,
	1.0f,0.0f, 0.0f,

	0.0f, 1.0f, 0.0f,
	0.0f, 1.0f,0.0f,
	0.0f, 1.0f,0.0f,

	0.0f, 1.0f, 0.0f,
	0.0f, 1.0f,0.0f,
	0.0f, 1.0f, 0.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f
};

static const float g_color_buffer_data[] = {
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f
};

xdl::xdl_bool running = xdl::xdl_true;

void notify(xdl::XdevLEvent& event) {
	switch(event.type) {
		case xdl::XDEVL_CORE_EVENT: {
			if(event.core.type == xdl::XDEVL_CORE_SHUTDOWN) {
				running = xdl::xdl_false;
			}
		}
		break;
		case xdl::XDEVL_WINDOW_EVENT: {
			switch(event.window.event) {
				case xdl::XDEVL_WINDOW_MOVED:
				case xdl::XDEVL_WINDOW_RESIZED: {
					xdl::XdevLViewPort viewport {};
					viewport.x = 0.0f;
					viewport.y = 0.0f;
					viewport.width = event.window.width;
					viewport.height = event.window.height;
					rai->setViewport(viewport);
				}
				break;
			}
		}
		break;
	}
}

void buttonClickedCallback(const xdl::ui::XdevLUIClickedEvent& event) {
	std::cout << "Button Pressed" << std::endl;
	running = false;
}

void buttonHoverCallback(const xdl::ui::XdevLUIHoverEvent& event) {
	std::cout << "Button Hover" << std::endl;
}


void sliderMovedCallback(const xdl::ui::XdevLUISliderMotionEvent& event) {
	std::cout << "Slider moved: " << event.values[0] << ", " << event.values[1] << ", " << event.values[2] << ", " << event.values[3]<< std::endl;
}

void checkboxCallback(const xdl::ui::XdevLUICheckedEvent& event) {
	std::cout << "CheckBox checked: " << event.checked << std::endl;

	window->setFullscreen(event.checked);
}

void sliderAngleMovedCallback(const xdl::ui::XdevLUISliderMotionEvent& event) {
	std::cout << "SliderAngle moved: " << event.values[0] << std::endl;
}

void comboBoxCallback(const xdl::ui::XdevLUIComboBoxEvent& event) {
	std::cout << "ComboBox Item: " << event.item << " selected" << std::endl;
}

static xdl::xdl_float zoom = 1.0;
void radioButton1Callback(const xdl::ui::XdevLUIActivatedEvent& event) {
	std::cout << "RadioButton changed: " << event.state << std::endl;
	if(event.state == 0) {
		zoom = 1.0f;
	} else if(event.state == 1) {
		zoom = 1.5f;
	}
}

void textInputCallback(const xdl::ui::XdevLUITextInputEvent& event) {
	std::cout << "TextInput changed: " << event.text << std::endl;
}

int main(int argc, char* argv[]) {

	// Create the core system.
	if(xdl::createCore(&core, argc, argv, xdl::XdevLFileName("rai_demo.xml")) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	// Register the specified instance as observer.
	auto result = core->registerListener(notify);
	if(xdl::RET_SUCCESS != result) {
		return xdl::RET_FAILED;
	}

	// Create a window so that we can draw something.
	window = xdl::getModule<xdl::IPXdevLWindow>(core, xdl::XdevLID("MyWindow"));
	if(!window) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	result = window->create();
	if(xdl::RET_SUCCESS != result) {
		return xdl::RET_FAILED;
	}

	// Create a window so that we can draw something.
	cursor = xdl::getModule<xdl::IPXdevLCursor>(core, xdl::XdevLID("XdevLCursor"));
	if(!cursor) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Get the instance to the keyboard module.
	keyboard = xdl::getModule<xdl::IPXdevLKeyboard>(core, xdl::XdevLID("MyKeyboard"));
	if(!keyboard) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Get the instance to the keyboard module.
	mouse = xdl::getModule<xdl::IPXdevLMouse>(core, xdl::XdevLID("MyMouse"));
	if(!mouse) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	if(cursor->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Get the OpenGL Rendering System.
	rai = xdl::getModule<xdl::IPXdevLRAI>(core, xdl::XdevLID("MyRAI"));
	if(!rai) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Get Archive instance.
	archive = xdl::getModule<xdl::XdevLArchive*>(core, xdl::XdevLID("MyArchive"));
	if(!archive) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}
	// We tell the archive to load files relative to the apps position.
	archive->mount(xdl::XdevLFileName("."));

	ui = xdl::getModule<xdl::ui::XdevLUI*>(core, xdl::XdevLID("MyUI"));
	if(!ui) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Create the RAI system.
	if(rai->create(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Attach the keyboard to the window.
	if(keyboard->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Attach the mouse to the window.
	if(mouse->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	// Attach the mouse to the window.
	if(ui->create(window, rai, archive) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	auto fontConfigFileName = xdl::XdevLFileName("fonts/default_info.txt");
	if(archive->fileExists(fontConfigFileName) == xdl::xdl_false) {
		return xdl::RET_FAILED;
	}
	auto fontFile = archive->open(xdl::XdevLOpenForReadOnly(), fontConfigFileName);
	ui->setupFontFromFile(fontFile);
	fontFile->close();

	//
	// Create Buffers
	//

	auto vd = rai->createVertexDeclaration();
	vd->add(3, xdl::XdevLBufferElementTypes::FLOAT, 0);		// Position
	vd->add(4, xdl::XdevLBufferElementTypes::FLOAT, 1);		// Color
	vd->add(3, xdl::XdevLBufferElementTypes::FLOAT, 2);		// Normal

	std::vector<xdl::xdl_uint8*> list;
	list.push_back((xdl::xdl_uint8*)g_vertex_buffer_data);
	list.push_back((xdl::xdl_uint8*)g_color_buffer_data);
	list.push_back((xdl::xdl_uint8*)g_normal_buffer_data);

	auto va = rai->createVertexArray();
	va->init(list.size(), list.data(), 36, vd);

	// Create the shader object instances.
	auto vs = rai->createVertexShader();
	auto fs = rai->createFragmentShader();
	auto sp = rai->createShaderProgram();

	// Create the source of the shaders.
	vs->addShaderCode(vertex_shader_source.c_str());
	fs->addShaderCode(fragment_shader_source.c_str());

	// Compile the shaders.
	vs->compile();
	fs->compile();

	// Link vertex and fragment shader to one program.
	sp->attach(vs);
	sp->attach(fs);
	sp->link();

	//
	// Let's create a buffer we are going to use within the shaders.
	//
	struct vertexShaderUniformBuffer {
		tmath::mat4 model;
		tmath::mat4 proj;
		float modulationColor[4];
	};
	vertexShaderUniformBuffer uniformBuffer;

	auto ub = rai->createUniformBuffer();

	// The size will have the size of our structure from above.
	ub->init(sizeof(vertexShaderUniformBuffer));

	//
	// Bind this buffer to a binding point. We will use that for the
	// shader program too.
	ub->bind(0);

	//
	// Get the name of the structure name within the shader and assign
	// it to the bining point we used for the uniform buffer.
	//
	sp->bind(xdl::XdevLString("VertexShaderUniformBuffer"), 0);


	//
	// Setup some other stuff
	//

	// Get a valid button object for the escape key.
	xdl::IPXdevLButton esc = nullptr;
	keyboard->getButton(xdl::KEY_ESCAPE, &esc);

	xdl::XdevLButton* showPropertiesKey;
	keyboard->getButton(xdl::KEY_S, &showPropertiesKey);

	xdl::XdevLButton* toggleFullscreen = nullptr;
	keyboard->getButton(xdl::KEY_F, &toggleFullscreen);

	xdl::IPXdevLAxis x_axis = nullptr;
	xdl::IPXdevLAxis y_axis = nullptr;
	xdl::IPXdevLButton left_mouse_button = nullptr;
	mouse->getAxis(xdl::AXIS_0, &x_axis);
	mouse->getAxis(xdl::AXIS_1, &y_axis);
	mouse->getButton(xdl::BUTTON_0, &left_mouse_button);

	window->show();


	//
	// Create interactive surfaces.
	//
	xdl::ui::XdevLUIButton button(xdl::XdevLString("Quit"));
	button.bind(xdl::ui::XdevLDelegateClicked::Create<&buttonClickedCallback>());
	button.bind(xdl::ui::XdevLDelegateHover::Create<&buttonHoverCallback>());

	std::vector<xdl::ui::XdevLUISlider::Type> backgroundColorInitial {0.4f, 0.462f, 0.561f};
	xdl::ui::XdevLUISlider backgroundColor(xdl::XdevLString("Background Color"), backgroundColorInitial);
	backgroundColor.bind(xdl::ui::XdevLDelegateSliderMotion::Create<&sliderMovedCallback>());

	xdl::ui::XdevLUISliderAngle sliderAngle(xdl::XdevLString("Rotate the mesh"), -360.0f, 360.0f);
	sliderAngle.bind(xdl::ui::XdevLDelegateSliderMotion::Create<&sliderAngleMovedCallback>());

	std::vector<xdl::ui::XdevLUISliderAngle::Type> aspectRatioInitial {(xdl::xdl_float)window->getWidth()/window->getHeight()};
	xdl::ui::XdevLUISliderAngle aspectRatio(xdl::XdevLString("Aspect Ratio"), 0, 360.0f, aspectRatioInitial);
	aspectRatio.bind(xdl::ui::XdevLDelegateSliderMotion::Create<&sliderAngleMovedCallback>());

	xdl::ui::XdevLUICheckBox checkbox(xdl::XdevLString("Fullscreen"));
	checkbox.bind(xdl::ui::XdevLDelegateChecked::Create<&checkboxCallback>());

	xdl::ui::XdevLUIRadioButton radioButton1(xdl::XdevLString("Zoom 1.0"), 0);
	radioButton1.bind(xdl::ui::XdevLDelegateActivated::Create<&radioButton1Callback>());

	xdl::ui::XdevLUIRadioButton radioButton2(xdl::XdevLString("Zoom 0.5"), 1, radioButton1);
	radioButton2.bind(xdl::ui::XdevLDelegateActivated::Create<&radioButton1Callback>());

	std::vector<xdl::XdevLString> comboBoxItems {xdl::XdevLString("Color 1"), xdl::XdevLString("Color 2")};
	xdl::ui::XdevLUIComboBox comboBox(xdl::XdevLString("Mesh Color"), comboBoxItems);
	comboBox.bind(xdl::ui::XdevLDelegateItemSelected::Create<&comboBoxCallback>());

	xdl::ui::XdevLUIText fpsMessage(xdl::XdevLString("Hello, XdevL World"));

	xdl::ui::XdevLUITextInputField textInputField(xdl::XdevLString("Type a your message"));
	textInputField.bind(xdl::ui::XdevLDelegateTextInput::Create<&textInputCallback>());


	xdl::xdl_float modulationColorItem[2][4] = {
		{1.0f, 0.0f, 1.0f, 1.0f},
		{0.3, 1.0f, 0.7f, 1.0f}
	};


	//
	// Start main loop.
	//
	while(running) {
		core->update();

		if(esc->getClicked()) {
			break;
		}

		static xdl::xdl_bool showProperties = xdl::xdl_false;
		if(showPropertiesKey->getClicked()) {
			showProperties = !showProperties;
		}

		static xdl::xdl_bool fullscreen = xdl::xdl_false;
		if(toggleFullscreen->getClicked()) {
			fullscreen = !fullscreen;
			window->setFullscreen(fullscreen);
		}

		rai->setActiveDepthTest(xdl::xdl_true);
		rai->clearColorTargets(backgroundColor.getData()[0], backgroundColor.getData()[1], backgroundColor.getData()[2], backgroundColor.getData()[3]);
		rai->clearDepthTarget(1.0f);

		tmath::mat4 rotx, roty, trans;
		tmath::perspective(45.0f, aspectRatio.getData()[0], 1.0f, 110.0f, uniformBuffer.proj);

		tmath::rotate_y(sliderAngle.getData()[0] * 10.0f, roty);
		tmath::rotate_x(sliderAngle.getData()[0] * 10.0f, rotx);
		tmath::translate(0.0f, 0.0f, -3.0f * zoom, trans);

		uniformBuffer.model =  trans * rotx * roty;
		memcpy(uniformBuffer.modulationColor, modulationColorItem[comboBox.getSelectedItemIndex()], sizeof(xdl::xdl_float) * 4);

		ub->lock();
		ub->upload((xdl::xdl_uint8*)&uniformBuffer, sizeof(vertexShaderUniformBuffer));
		ub->unlock();

		rai->setActiveVertexArray(va);
		rai->setActiveShaderProgram(sp);

		rai->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLES, 36);

//		ui->lock(xdl::XdevLString("Rendering Statistics"));
//
//		std::stringstream tmp;
//		tmp << "OpenGL Demo using XdevLFont: " << ui->getJitter() << " ms/frame " << "FPS: " << ui->getFPS() << std::endl;
//		fpsMessage.setLabel(xdl::XdevLString(tmp.str()));
//		ui->add(fpsMessage);

//		ui->unlock();


		//
		// Start new window and add surfaces on to it.
		//
		ui->lock(xdl::XdevLString("Rendering Settings"));

		std::stringstream tmp;
		tmp << "OpenGL Demo using XdevLFont: " << ui->getJitter() << " ms/frame " << "FPS: " << ui->getFPS() << std::endl;
		fpsMessage.setLabel(xdl::XdevLString(tmp.str().c_str()));
		ui->add(fpsMessage);

		ui->add(button);
		ui->add(backgroundColor);
		ui->add(sliderAngle);
		ui->add(aspectRatio);
		ui->add(checkbox);
		ui->add(comboBox);
		ui->add(radioButton1);
		ui->add(radioButton2);
		ui->add(textInputField);

		ui->unlock();

//		ui->lock(xdl::XdevLString("Something Settings"));
//		ui->unlock();

		//
		// Finished adding surfaces to window.
		//

		ui->render();
		rai->swapBuffers();

	}

	xdl::destroyCore(core);

	return 0;
}
