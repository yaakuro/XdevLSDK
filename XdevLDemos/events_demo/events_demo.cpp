/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLLog.h>
#include <XdevLThread.h>
#include <XdevLWindow/XdevLWindow.h>
#include <map>

xdl::IPXdevLCore core = XDEVL_MODULE_NULLPTR;
xdl::IPXdevLWindow window1 = XDEVL_MODULE_NULLPTR;
xdl::IPXdevLWindow window2 = XDEVL_MODULE_NULLPTR;
xdl::xdl_bool run = xdl::xdl_true;

std::map<xdl::xdl_uint64, xdl::IPXdevLWindow> windowsMap;


//
// The following are the ID's for the events. They are fixed so you can create them always like this.
//
static const xdl::XdevLID ButtonPressed("XDEVL_BUTTON_PRESSED");
static const xdl::XdevLID ButtonReleased("XDEVL_BUTTON_RELEASED");
static const xdl::XdevLID MouseButtonPressed("XDEVL_MOUSE_BUTTON_PRESSED");
static const xdl::XdevLID MouseButtonReleased("XDEVL_MOUSE_BUTTON_RELEASED");
static const xdl::XdevLID MouseMouseMotion("XDEVL_MOUSE_MOTION");

struct Cursor {
	Cursor() : pressed(xdl::xdl_false), released(xdl::xdl_true), x(0), y(0) {}
	xdl::xdl_bool pressed;
	xdl::xdl_bool released;
	xdl::xdl_int32 x;
	xdl::xdl_int32 y;
};

Cursor currentCursorPosition;

class EventListener : public xdl::XdevLListener {
	public:
		xdl::xdl_int notify(xdl::XdevLEvent& event) {

			if(event.type == ButtonPressed.getHashCode()) {
				std::cout << "WindowID:" <<  event.key.windowid << " XDEVL_KEY_PRESSED: " <<  xdl::xdevLButtonIdToString((xdl::XdevLButtonId)event.key.keycode) << std::endl;
				if(event.key.keycode == xdl::KEY_ESCAPE) {
					run = xdl::xdl_false;
				}

			} else if(event.type == ButtonReleased.getHashCode()) {
				std::cout << "WindowID:" << event.key.windowid << " XDEVL_KEY_RELEASED: " << xdl::xdevLButtonIdToString((xdl::XdevLButtonId)event.key.keycode) << std::endl;
				if(event.key.keycode == xdl::KEY_ESCAPE) {
					run = xdl::xdl_false;
				}
			} else if(event.type == MouseButtonPressed.getHashCode()) {
				std::cout << "WindowID:" << event.key.windowid << " XDEVL_MOUSE_BUTTON_PRESSED: (" << event.button.x << "," << event.button.y << ")" << std::endl;
				currentCursorPosition.pressed = true;
				currentCursorPosition.released = false;
				currentCursorPosition.x = event.button.x;
				currentCursorPosition.y = event.button.y;
			} else if(event.type == MouseButtonReleased.getHashCode()) {
				std::cout << "WindowID:" << event.key.windowid << " XDEVL_MOUSE_BUTTON_RELEASED: (" << event.button.x << "," << event.button.y << ")" << std::endl;
				currentCursorPosition.pressed = false;
				currentCursorPosition.released = true;
				currentCursorPosition.x = event.button.x;
				currentCursorPosition.y = event.button.y;
			} else if(event.type == MouseMouseMotion.getHashCode()) {
				std::cout << "WindowID: " << event.motion.windowid << " -> XDEVL_MOUSE_MOTION ( " << event.motion.x << "," << event.motion.y << "," << event.motion.xrel << "," << event.motion.yrel << ")" << std::endl;
			}


			switch(event.type) {
				case xdl::XDEVL_WINDOW_EVENT: {
					switch(event.window.event) {
						case xdl::XDEVL_WINDOW_CREATE: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_CREATE" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_CLOSE: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_CLOSE" << std::endl;
							if(event.window.windowid == window1->getWindowID()) {
								run = xdl::xdl_false;
							}
						}
						break;
						case xdl::XDEVL_WINDOW_EXPOSED: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_EXPOSED" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_INPUT_FOCUS_GAINED: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_INPUT_FOCUS_GAINED" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_INPUT_FOCUS_LOST: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_INPUT_FOCUS_LOST" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_HIDDEN: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_HIDDEN" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_LEAVE: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_LEAVE" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_ENTER: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_ENTER" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_MAXIMIZED: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_MAXIMIZED" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_MINIMIZED: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_MINIMIZED" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_MOVED: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_MOVED (" << event.window.x << "," << event.window.y << ")" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_RESIZED: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_RESIZED (" << event.window.width << "," << event.window.height << ")" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_RESTORED: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_RESTORED" << std::endl;
						}
						break;
						case xdl::XDEVL_WINDOW_SHOWN: {
							std::cout << "WindowID:" << event.key.windowid << " XDEVL_WINDOW_SHOWN" << std::endl;
						}
						break;
					}
				}
				break;
			}
			return 0;
		}

		const xdl::XdevLID& getID() const {
			return m_id;
		}

		xdl::XdevLID m_id;
};

int main(int argc, char* argv[]) {

	// Create the core system.
	if(xdl::createCore(&core, argc, argv, xdl::XdevLFileName("events_demo.xml")) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	// Register a listener callback function to catch all events in the system.
	EventListener myEventListener;
	core->registerListener(&myEventListener);

	window1 = xdl::getModule<xdl::IPXdevLWindow>(core,  xdl::XdevLID("MyWindow1"));
	if(xdl::isModuleNotValid(window1)) {
		return xdl::RET_FAILED;
	}

	xdl::IPXdevLWindow window2 = xdl::getModule<xdl::IPXdevLWindow>(core,  xdl::XdevLID("MyWindow2"));
	if(xdl::isModuleNotValid(window2)) {
		return xdl::RET_FAILED;;
	}

	window1->create();
	window2->create();

	window1->show();
	window2->show();

	windowsMap[window1->getWindowID()] = window1;
	windowsMap[window2->getWindowID()] = window2;


	while(run) {
		core->update();
	}

	xdl::destroyCore(core);

	return xdl::RET_SUCCESS;
}
