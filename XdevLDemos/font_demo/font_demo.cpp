/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLApplication.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLFont/XdevLFont.h>
#include <XdevLFont/XdevLFontSystem.h>
#include <XdevLFont/XdevLTextLayout.h>
#include <XdevLFontGenerator/XdevLFontGenerator.h>

class FontTest : public xdl::XdevLApplication {
	public:


		FontTest(const xdl::XdevLWindowTitle& title, int argc, char** argv, const xdl::XdevLFileName& xml_filename) throw() :
			xdl::XdevLApplication(title, argc, argv, xml_filename),
			m_appRun(xdl::xdl_true),
			m_font(nullptr),
			m_textEngine(nullptr) {

		}

		~FontTest() {

		}

		virtual void preInit(const Arguments& argv) {


			initializeRenderSystem();

			m_mouseAxisDelegate = xdl::XdevLAxisDelegateType::Create<FontTest, &FontTest::mouse_axis_handle>(this);
			getMouse()->registerDelegate(m_mouseAxisDelegate);


			m_buttonDelegate = xdl::XdevLButtonIdDelegateType::Create<FontTest, &FontTest::escape_key_handle>(this);
			xdl::XdevLString key("KEY_ESCAPE");

			getKeyboard()->registerDelegate(key, m_buttonDelegate);


			getWindow()->show();
			getWindow()->setInputFocus();


		}

		void updateGraphics(xdl::xdl_double dT) override final {
			getCore()->update();

			get3DProcessor()->setActiveDepthTest(xdl::xdl_true);
			get3DProcessor()->clearColorTargets(1.0f, 1.0f, 1.0f, 0.0f);
			get3DProcessor()->clearDepthTarget(1.0f);

			m_textEngine->setColor(0, 0, 0, 255);
			m_textEngine->useFont(m_font);
			xdl::XdevLString tmp = xdl::XdevLString::ansiToString("The quick brown fox jumps over the lazy dog");
			m_textEngine->printText(tmp, xdl::XdevLPositionF(m_xaxis, m_yaxis));

			get3DProcessor()->swapBuffers();
			xdl::sleep(0.002);
		}

		xdl::IPXdevLRAI get3DProcessor() {
			return m_rai;
		}


		xdl::xdl_int initializeRenderSystem() {
			// Get the OpenGL context.
			m_rai = xdl::getModule<xdl::IPXdevLRAI>(getCore(), xdl::XdevLID("MyOpenGL"));
			if(xdl::isModuleNotValid(m_rai)) {
				return xdl::RET_FAILED;
			}

			// We must attach the OpenGL context to a render m_window.
			if(m_rai->create(getWindow()) != xdl::RET_SUCCESS) {
				return xdl::RET_FAILED;
			}

			// Get the FontSystem
			m_fontGenerator = xdl::getModule<xdl::IPXdevLFontGenerator>(getCore(), xdl::XdevLID("MyFontGenerator"));
			if(xdl::isModuleNotValid(m_fontGenerator)) {
				return xdl::RET_FAILED;
			}

			// Get the FontSystem
			m_fontSystem = xdl::getModule<xdl::IPXdevLFontSystem>(getCore(), xdl::XdevLID("MyFontSystem"));
			if(xdl::isModuleNotValid(m_fontSystem)) {
				return xdl::RET_FAILED;
			}

			// Get the Text Layout System.
			m_textEngine = xdl::getModule<xdl::IPXdevLTextLayout>(getCore(), xdl::XdevLID("MyTextLayout"));
			if(xdl::isModuleNotValid(m_textEngine)) {
				return xdl::RET_FAILED;
			}

			auto ttFontFileName = xdl::XdevLFileName("fonts/ProggyClean.ttf");
			if(getArchive()->fileExists(ttFontFileName) == xdl::xdl_false) {
				return xdl::RET_FAILED;
			}
			auto ttFile = getArchive()->open(xdl::XdevLOpenForReadOnly(), ttFontFileName);
			auto fontConfigFileName = xdl::XdevLFileName("fonts/default");
			m_fontGenerator->create(getArchive(), ttFile, fontConfigFileName, 32, 1024, xdl::XdevLFontSize(16), xdl::XdevLTextureSize(512, 512));


			// Initialize font system.
			xdl::XdevLFontSystemCreateParameter fontSystemCreateParameter;
			fontSystemCreateParameter.rai = get3DProcessor();
			m_fontSystem->create(fontSystemCreateParameter);

			fontConfigFileName = xdl::XdevLFileName("fonts/default_info.txt");
			if(getArchive()->fileExists(fontConfigFileName) == xdl::xdl_false) {
				return xdl::RET_FAILED;
			}
			auto fontFile = getArchive()->open(xdl::XdevLOpenForReadOnly(), fontConfigFileName);
			m_font = m_fontSystem->createFromFontFile(fontFile);
			if(nullptr == m_font) {
				return xdl::RET_FAILED;
			}
			fontFile->close();

			xdl::XdevLTextLayoutCreateParameter textLayoutCreateParameter;
			textLayoutCreateParameter.rai = get3DProcessor();
			textLayoutCreateParameter.window = getWindow();

			m_textEngine->create(textLayoutCreateParameter);
			m_textEngine->usePixelUnits(xdl::xdl_true);
			m_textEngine->setScale(1.0f);
			m_textEngine->setDFT(0);
			m_textEngine->setEffect(0);
			m_textEngine->useFont(m_font);

			return xdl::RET_SUCCESS;
		}

	private:

		void escape_key_handle(const xdl::XdevLDelegateButtonIdParameter& parameter) {
			if(parameter.state == xdl::BUTTON_RELEASED) {
				m_appRun = false;
			}
		}

		void mouse_axis_handle(const xdl::XdevLDelegateAxisParameter& parameter) {
			if(parameter.id == xdl::AXIS_0) {
				m_xaxis = parameter.value;
			} else if(parameter.id == xdl::AXIS_1) {
				m_yaxis = parameter.value;
			}
		}

	private:

		xdl::xdl_bool m_appRun;
		xdl::XdevLButtonIdDelegateType 	m_buttonDelegate;
		xdl::XdevLAxisDelegateType 		m_mouseAxisDelegate;
		xdl::IPXdevLRAI 				m_rai;

		xdl::IPXdevLFontGenerator	m_fontGenerator;
		xdl::IPXdevLFontSystem		m_fontSystem;
		xdl::IPXdevLFont			m_font;
		xdl::IPXdevLTextLayout		m_textEngine;
		xdl::xdl_float m_xaxis;
		xdl::xdl_float m_yaxis;

};


XdevLStartMainXML(FontTest, xdl::XdevLString("Font Demo"), xdl::XdevLFileName(TEXT("font_demo.xml")))
