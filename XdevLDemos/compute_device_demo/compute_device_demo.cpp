/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLComputeDevice/XdevLComputeDevice.h>

float data[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

int main(int argc, char** argv) {

	xdl::IPXdevLCore core = nullptr;
	if(xdl::createCore(&core, argc, argv) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	core->plug(xdl::XdevLPluginName("XdevLComputeDeviceCL"), xdl::XdevLVersion(0,7,0));
	core->plug(xdl::XdevLPluginName("XdevLFileSystem"), xdl::XdevLVersion(0,7,0));

	auto archive = xdl::createModule<xdl::IPXdevLArchive>(core, xdl::XdevLModuleName("XdevLArchive"), xdl::XdevLID("MyArchive"));
	archive->mount(xdl::XdevLFileName("."));

	auto computeDevice = xdl::createModule<xdl::IPXdevLComputeDevice>(core, xdl::XdevLModuleName("XdevLComputeDevice"), xdl::XdevLID("MyComputeDevice"));

	// We need a context.
	auto context = computeDevice->createContext();

	// We need a command queue to run commands.
	auto commandQueue = context->createCommandQueue();


	auto program = context->createProgram();
	auto inBuffer = context->createBuffer(xdl::XDEVL_COMPUTE_BUFFER_READ_ONLY, sizeof(float) * 10);
	auto outBuffer = context->createBuffer(xdl::XDEVL_COMPUTE_BUFFER_WRITE_ONLY, sizeof(float) * 10);

	//
	// Load and build the kernel.
	//
	auto file = archive->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName("compute_device_demo.cl"));
	auto kernel = program->compile(file, xdl::XdevLString("calculate_sqrt"));


	for(int a = 0; a < 100; a++) {
		kernel->setArgumentBuffer(0, inBuffer);
		kernel->setArgumentBuffer(1, outBuffer);
		kernel->setArgumentFloat(2, 2);

		inBuffer->upload(commandQueue.get(), sizeof(float) * 10, (xdl::xdl_uint8*)data);

		xdl::XdevLComputeExecuteParameter para(commandQueue.get(), kernel.get(), {32});
		program->execute(para);


//		std::cout << "Before: " << std::endl;
//		for(auto item : data) {
//			std::cout << item << " : ";
//		}
//		std::cout << std::endl;

		outBuffer->download(commandQueue.get(), sizeof(float) * 10, (xdl::xdl_uint8*)data);

		std::cout << "After: " << std::endl;
		for(auto item : data) {
			std::cout << item << " : ";
		}
		std::cout << std::endl;
	}
	xdl::destroyCore(core);
}
