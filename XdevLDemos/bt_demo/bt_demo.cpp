/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLBehaviorTree/XdevLBehaviorTree.h>


class MyTask1 : public xdl::bt::XdevLBTTaskNode {
		xdl::bt::XdevLBTStatus tick(xdl::xdl_float dT) override {
			std::cout << "MyTask1: Distance: " << getBlackboard()->getFloatValue(xdl::XdevLString("distance")) << std::endl;
			return xdl::bt::XdevLBTStatus::Failed;
		}
};

class MyTask2 : public xdl::bt::XdevLBTTaskNode {
		xdl::bt::XdevLBTStatus tick(xdl::xdl_float dT) override {
			std::cout << "MyTask2" << std::endl;
			return xdl::bt::XdevLBTStatus::Failed;
		}
};

class MyTask3 : public xdl::bt::XdevLBTTaskNode {
		xdl::bt::XdevLBTStatus tick(xdl::xdl_float dT) override {
			std::cout << "MyTask3" << std::endl;
			return xdl::bt::XdevLBTStatus::Success;
		}
};



int main(int argc, char** argv) {

	// Create composite nodes to play around with.
	xdl::bt::XdevLBTSelectorNode selector;
	xdl::bt::XdevLBTSequenceNode sequence;

	// Create some task nodes to play around with.
	MyTask1 task1;
	MyTask2 task2;
	MyTask3 task3;

	// Attach the nodes.
	selector.addChild(&task1);
	selector.addChild(&sequence);
	sequence.addChild(&task2);
	sequence.addChild(&task3);

	// Create a blackboard and add some key, values.
	auto blackboard = xdl::bt::XdevLBTBlackboard();
	blackboard.setFloatValue(xdl::XdevLString("distance"), 1.2f);

	// Create behavior tree.
	xdl::bt::XdevLBT behaviorTree(&blackboard);

	// Set the root node of the behavior tree.
	behaviorTree.setRoot(&selector);

	// Assign the blackboard to the behavior tree.
	behaviorTree.setBlackboard(&blackboard);

	// Start the behavior tree. Usually this would be inside a loop.
	behaviorTree.tick(0.0f);

	return 0;
}
