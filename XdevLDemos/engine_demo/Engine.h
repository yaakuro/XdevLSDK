/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef ENGINE_H
#define ENGINE_H

#include <XdevL.h>
#include <XdevLApplication.h>
#include <XdevLUI/XdevLUI.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLFont/XdevLFont.h>
#include <XdevLFont/XdevLFontSystem.h>
#include <XdevLFont/XdevLTextLayout.h>
#include <XdevLFileSystem/XdevLArchive.h>
#include <XdevLModel/XdevLModelServer.h>
#include <XdevLImage/XdevLImageServer.h>

#include <XdevLGraphics/3D/XdevLScene.h>
#include <XdevLGraphics/3D/XdevLTransformable.h>
#include <XdevLGraphics/3D/XdevLCamera.h>
#include <XdevLGraphics/3D/XdevLSphereTransformationGizmo.h>
#include <XdevLGraphics/3D/XdevLActor.h>
#include <XdevLGraphics/3D/XdevLLight.h>

#include <cmath>
#include <tm/tm.h>

struct vertexShaderUniformBuffer {
	tmath::mat4 proj;
	tmath::mat4 model[100];
	tmath::mat4 view;
	tmath::vec3 lightPosition;
	tmath::vec3 cameraPosition;
};

enum fragmentShaderStates {
	NORMAL_MAPPING = 1,
	PARALLAX_MAPPING = 2
};

struct fragmentShaderUniformBuffer {

	//
	// Normal, Parallax, Steep Parallax Mapping
	//
	xdl::xdl_uint32 mappingStates = NORMAL_MAPPING | PARALLAX_MAPPING;
	xdl::xdl_float parallaxScale = 0.05f;
	xdl::xdl_float parallaxBias = 0.043f;
	xdl::xdl_float steepParallaxSteps = 20.0f;

	// Lighting.
	xdl::xdl_float specularComponent = 32.0f;

	xdl::xdl_uint32 lit = 1;
};


inline xdl::xdl_bool isCheckBoxChecked(const fragmentShaderUniformBuffer& uniformBuffer, xdl::xdl_uint mappingMask) {
	return ((uniformBuffer.mappingStates & mappingMask) == mappingMask ? xdl::xdl_true : xdl::xdl_false);
}

class Engine : public xdl::XdevLApplication {
	public:

		using UpdateInputDelegate = xdl::XdevLDelegate<void, xdl::xdl_double>;


		Engine(const xdl::XdevLWindowTitle& title, int argc, char** argv, const xdl::XdevLFileName& xml_filename) throw();
		~Engine();

		void preInit(const Arguments& argv) override final;
		void tick(xdl::xdl_float dT) override final;
		void initInputActions(xdl::IPXdevLInputActionSystem& inputActionSystem) override final;

		xdl::xdl_int notify(xdl::XdevLEvent& event) override final;

		xdl::xdl_int initRenderDevice();
		xdl::xdl_int initInputHandling();
		xdl::xdl_int initFramebuffer();
		xdl::xdl_int initRenderAssets();
		xdl::xdl_int initUI();

		void updatePhysics(xdl::xdl_double time, xdl::xdl_double dT) override final;
		void updateGraphics(xdl::xdl_double dT) override final;
		void updateUI(xdl::xdl_double dT) override final;
		void handleUpdateInput(xdl::xdl_double dT);

		UpdateInputDelegate updateInput;

	private:

		xdl::ui::IPXdevLUI m_ui;

		xdl::IPXdevLFrameBuffer m_frameBuffer;
		xdl::IPXdevLVertexDeclaration m_vertexDeclaration;
		xdl::IPXdevLVertexDeclaration m_clickVertexDeclaration;
		xdl::IPXdevLVertexArray m_clickVertexArray;
		xdl::IPXdevLVertexBuffer m_clickVertexBuffer;

		xdl::IPXdevLFontSystem fontSystem;
		xdl::IPXdevLTextLayout textLayoutSystem;

		xdl::IPXdevLVertexShader m_vs;
		xdl::IPXdevLFragmentShader m_fs;
		xdl::IPXdevLShaderProgram m_sp;
		xdl::IPXdevLUniformBuffer m_uniformBuffer;
		xdl::IPXdevLUniformBuffer m_fragmentUniformBuffer;

		xdl::gfx::IPXdevLModelServer m_modelServer;
		xdl::IPXdevLImageServer m_imageServer;

		xdl::xdl_int m_frameBufferProjectionMatrix;
		xdl::xdl_int m_frameBufferTexture;

		xdl::xdl_int m_diffuseMapTextureID;
		xdl::xdl_int m_normalMapTextureID;
		xdl::xdl_int m_heightMapTextureID;

		xdl::IPXdevLTexture m_diffuseMap;
		xdl::IPXdevLTexture m_normalMap;
		xdl::IPXdevLTexture m_heightMap;

		xdl::IPXdevLAxis x_axis;
		xdl::IPXdevLAxis y_axis;

		xdl::IPXdevLButton m_control;
		xdl::IPXdevLButton m_left_mouse_button;
		xdl::IPXdevLButton m_rightMouseButton;

		xdl::IPXdevLButton m_W;
		xdl::IPXdevLButton m_S;
		xdl::IPXdevLButton m_A;
		xdl::IPXdevLButton m_D;
		xdl::IPXdevLButton m_Q;
		xdl::IPXdevLButton m_E;
		xdl::IPXdevLButton m_F1;

		xdl::xdl_bool m_appIsRunning;

		xdl::XdevLSphereTransformationGizmo sphereRotationGizmo;

		xdl::XdevLCamera* camera;
		xdl::XdevLLight* light;
		xdl::XdevLScene* m_scene;

		xdl::ui::XdevLUICheckBox* enableNormalMapping;
		xdl::ui::XdevLUICheckBox* enableParalaxMapping;
		xdl::ui::XdevLUISlider* parallaxScale;
		xdl::ui::XdevLUISlider* parallaxBias;
		xdl::ui::XdevLUISlider* steepParallaxSteps;
		xdl::ui::XdevLUISlider* specularComponent;

		void enableNormalMappingCheckBox(const xdl::ui::XdevLUICheckedEvent& event);
		void enableParallaxMappingCheckBox(const xdl::ui::XdevLUICheckedEvent& event);
		void parallaxScaleSlider(const xdl::ui::XdevLUISliderMotionEvent& event);
		void parallaxBiasSlider(const xdl::ui::XdevLUISliderMotionEvent& event);
		void steepParallaxStepsSlider(const xdl::ui::XdevLUISliderMotionEvent& event);
		void specularComponentSlider(const xdl::ui::XdevLUISliderMotionEvent& event);

		void mouseAxisCallback(const xdl::XdevLDelegateAxisParameter& parameter);
		void mouseButtonCallback(const xdl::XdevLDelegateActionParameter& actionParameter);

		xdl::xdl_double m_fps;
		
		struct ClickVertex {
			tmath::vec3 pos;
			xdl::xdl_float r, g, b, a;
		};
		std::vector<ClickVertex> m_vertices;
};

#endif
