/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "Engine.h"
#include <XdevLImage/XdevLImageUtils.h>
#include <XdevLGraphics/3D/XdevLRaycast.h>

vertexShaderUniformBuffer uniformBuffer;
fragmentShaderUniformBuffer fragmentUniformBuffer;

Engine::Engine(const xdl::XdevLWindowTitle& title, int argc, char** argv, const xdl::XdevLFileName& xml_filename) throw()
	: Super(title, argc, argv, xml_filename)
	, m_frameBuffer(nullptr)
	, m_appIsRunning(xdl::xdl_true) {
}

Engine::~Engine() {

}

void Engine::initInputActions(xdl::IPXdevLInputActionSystem& inputActionSystem) {
	Super::initInputActions(inputActionSystem);

//	xdl::XdevLAxisActionDelegateType mouseAxisX = xdl::XdevLAxisActionDelegateType::Create<MyGLSLApplication, &MyGLSLApplication::handleAxisX>(this);
//	xdl::XdevLAxisActionDelegateType mouseAxisY = xdl::XdevLAxisActionDelegateType::Create<MyGLSLApplication, &MyGLSLApplication::handleAxisY>(this);
//	xdl::XdevLActionDelegateType activateRotationDelegate = xdl::XdevLActionDelegateType::Create<MyGLSLApplication, &MyGLSLApplication::handleActivateRotation>(this);

//	inputActionSystem->registerAxisAction(xdl::XdevLString(TEXT("MouseAxisX")), xdl::AXIS_0, mouseAxisX);
//	inputActionSystem->registerAxisAction(xdl::XdevLString(TEXT("MouseAxisY")), xdl::AXIS_1, mouseAxisY);
//	inputActionSystem->registerButtonAction(xdl::XdevLString(TEXT("ActivateRotation")), xdl::BUTTON_0, activateRotationDelegate);

	inputActionSystem->getKeyboard()->getButton(xdl::KEY_LCTRL, &m_control);
	inputActionSystem->getKeyboard()->getButton(xdl::KEY_W, &m_W);
	inputActionSystem->getKeyboard()->getButton(xdl::KEY_S, &m_S);
	inputActionSystem->getKeyboard()->getButton(xdl::KEY_A, &m_A);
	inputActionSystem->getKeyboard()->getButton(xdl::KEY_D, &m_D);
	inputActionSystem->getKeyboard()->getButton(xdl::KEY_Q, &m_Q);
	inputActionSystem->getKeyboard()->getButton(xdl::KEY_E, &m_E);
	inputActionSystem->getKeyboard()->getButton(xdl::KEY_F1, &m_F1);

	inputActionSystem->getMouse()->getButton(xdl::BUTTON_LEFT, &m_left_mouse_button);
	inputActionSystem->getMouse()->getButton(xdl::BUTTON_RIGHT, &m_rightMouseButton);
	inputActionSystem->getMouse()->getAxis(xdl::AXIS_0, &x_axis);
	inputActionSystem->getMouse()->getAxis(xdl::AXIS_1, &y_axis);

	xdl::XdevLAxisDelegateType mouseAxisDelegate = xdl::XdevLAxisDelegateType::Create<Engine, &Engine::mouseAxisCallback>(this);
	xdl::XdevLActionDelegateType mouseButtonDelegate = xdl::XdevLActionDelegateType::Create<Engine, &Engine::mouseButtonCallback>(this);

	inputActionSystem->getMouse()->registerDelegate(mouseAxisDelegate);
	inputActionSystem->registerButtonAction(xdl::XdevLString(TEXT("ActivateRotation")), xdl::BUTTON_RIGHT, mouseButtonDelegate);
}

void Engine::preInit(const Arguments& argv)  {
	Super::preInit(argv);

	// Set the delegates.
	updateInput = UpdateInputDelegate::Create<Engine, &Engine::handleUpdateInput>(this);

	if(initRenderDevice() != xdl::RET_SUCCESS) {
		return;
	}

	if(initFramebuffer() != xdl::RET_SUCCESS) {
		return;
	}

	if(initRenderAssets() != xdl::RET_SUCCESS) {
		return;
	}

	if(initUI() != xdl::RET_SUCCESS) {
		return;
	}

	getWindow()->show();
}

void Engine::tick(xdl::xdl_float dT) {
	Super::tick(dT);

	updateInput(dT);
}

void Engine::updatePhysics(xdl::xdl_double time, xdl::xdl_double dT) {
	Super::updatePhysics(time, dT);

	//
	// Calculate transformation of camera.
	//
//	camera->setPosition(tmath::vec3(0.0f, 0.0f, 2.1f));
//	camera->startTrackObject(&sphereRotationGizmo);
//	camera->setTrackingProperties(0.0f, 0.0f, 4.0f, dT);

	uniformBuffer.cameraPosition = camera->getPosition();
	uniformBuffer.view = camera->getInverseTransformationMatrix();
	uniformBuffer.proj = camera->getProjectionMatrix();

	// -------------------------------------------------------------------------

	//
	// Calculate transformation of light.
	//
	static xdl::xdl_float lightSpeed = 2.0f; // :D, tyhahaaa.

	light->setPosition(tmath::vec3(sin((xdl::xdl_float)time * lightSpeed) * 1.0f, 0.0f, 1.5f));
	uniformBuffer.lightPosition = light->getPosition();
}

void Engine::handleUpdateInput(xdl::xdl_double dT) {

	if(m_left_mouse_button->getPressed() && m_control->getPressed()) {
		auto rotateAroundYAxis = static_cast<xdl::xdl_float>(-y_axis->getDeltaValue()) * static_cast<xdl::xdl_float>(dT) * 13.0f;
		auto rotateAroundXAxis = static_cast<xdl::xdl_float>(x_axis->getDeltaValue()) * static_cast<xdl::xdl_float>(dT) * 13.0f;

		//
		// Calculate transformation object.
		//
		sphereRotationGizmo.rotateSphere(rotateAroundYAxis, rotateAroundXAxis, x_axis->getValue(), y_axis->getValue());
		sphereRotationGizmo.setPosition(tmath::vec3(0.0f, 0.0f, 0.0f));
	}

	if(m_rightMouseButton->getPressed()) {
		auto yawRotation = static_cast<xdl::xdl_float>(y_axis->getDeltaValue()) * static_cast<xdl::xdl_float>(dT);
		auto pitchRotation = static_cast<xdl::xdl_float>(-x_axis->getDeltaValue()) * static_cast<xdl::xdl_float>(dT);
		camera->fpsView(yawRotation, pitchRotation, dT);
	}

	m_scene->getActorList()[0]->setOrientation(sphereRotationGizmo.getOrientation());
	m_scene->getActorList()[0]->setPosition(sphereRotationGizmo.getPosition());
	// -------------------------------------------------------------------------

	if(m_W->getPressed()) {
		camera->moveForward(static_cast<xdl::xdl_float>(dT));
	}
	if(m_S->getPressed()) {
		camera->moveForward(static_cast<xdl::xdl_float>(-dT));
	}
	if(m_A->getPressed()) {
		camera->moveSide(static_cast<xdl::xdl_float>(-dT));
	}
	if(m_D->getPressed()) {
		camera->moveSide(static_cast<xdl::xdl_float>(dT));
	}
	if(m_Q->getPressed()) {
		camera->moveUp(static_cast<xdl::xdl_float>(-dT));
	}
	if(m_E->getPressed()) {
		camera->moveUp(static_cast<xdl::xdl_float>(dT));
	}
	if(m_F1->getClicked()) {
		static xdl::xdl_bool f1Toggle = xdl::xdl_false;
		f1Toggle = !f1Toggle;
	}
}

void Engine::updateGraphics(xdl::xdl_double dT) {
	Super::updateGraphics(dT);

	//
	// Let's render stuff into the framebuffer object with low resolution.
	//
	m_frameBuffer->activate();
	m_frameBuffer->setActiveDepthTest(xdl::xdl_true);

	xdl::XdevLFrameBufferColorTargets list[] = {xdl::XdevLFrameBufferColorTargets::TARGET0};
	m_frameBuffer->activateColorTargets(1, list);

	m_frameBuffer->clearColorTargets(0.0f, 0.0f, 0.0f, 1.0f);
	m_frameBuffer->clearDepthTarget(1.0f);


	// -------------------------------------------------------------------------


	getRAI()->setActiveShaderProgram(m_sp);

	for(auto& actor : m_scene->getActorList()) {
		for(auto mesh : actor->getModel()->getMeshArray()) {

			uniformBuffer.model[0] = actor->getTransformationMatrix();
			fragmentUniformBuffer.lit = actor->isLit();

			m_uniformBuffer->lock();
			m_uniformBuffer->upload((xdl::xdl_uint8*)&uniformBuffer, sizeof(vertexShaderUniformBuffer));
			m_uniformBuffer->unlock();

			m_fragmentUniformBuffer->lock();
			m_fragmentUniformBuffer->upload((xdl::xdl_uint8*)&fragmentUniformBuffer, sizeof(fragmentShaderUniformBuffer));
			m_fragmentUniformBuffer->unlock();

			auto material = mesh->getMaterial();

			m_sp->activate();
			if(material->getUseDiffuseMap()) {
				m_sp->setUniformi(m_diffuseMapTextureID, 0);
				m_diffuseMap->activate(xdl::XdevLTextureStage::STAGE0);
			}
			if(material->getUseNormalMap()) {
				m_sp->setUniformi(m_normalMapTextureID, 1);
				m_normalMap->activate(xdl::XdevLTextureStage::STAGE1);
			}
			if(material->getUseDisplacementMap()) {
				m_sp->setUniformi(m_heightMapTextureID, 2);
				m_heightMap->activate(xdl::XdevLTextureStage::STAGE2);
			}
			m_sp->deactivate();

			getRAI()->setActiveVertexArray(mesh->getVertexArray());
			if(mesh->getVertexArray()->getIndexBuffer()) {
				getRAI()->drawInstancedVertexArray(mesh->getPrimitive(), mesh->getNumberOfFaces() * 3, 1);
			} else {
				getRAI()->drawInstancedVertexArray(mesh->getPrimitive(), mesh->getNumberOfVertices(), 1);
			}
		}
	}

	fragmentUniformBuffer.lit = 0;
	m_fragmentUniformBuffer->lock();
	m_fragmentUniformBuffer->upload((xdl::xdl_uint8*)&fragmentUniformBuffer, sizeof(fragmentShaderUniformBuffer));
	m_fragmentUniformBuffer->unlock();
	getRAI()->setPointSize(4.0f);
	m_clickVertexBuffer->lock();
	m_clickVertexBuffer->upload((xdl::xdl_uint8*)m_vertices.data(), m_vertices.size() * sizeof(tmath::vec3));
	m_clickVertexBuffer->unlock();

	getRAI()->setActiveVertexArray(m_clickVertexArray);
	getRAI()->drawVertexArray(xdl::XDEVL_PRIMITIVE_POINTS, m_vertices.size());

	//
	// Now stop rendering into the frambuffer object.
	//
	m_frameBuffer->deactivate();

	std::vector<xdl::XdevLFrameBufferRenderTarget> renderTargets {xdl::XdevLFrameBufferRenderTarget::COLOR};
	m_frameBuffer->blit(nullptr, renderTargets.size(), renderTargets.data());

	std::wstringstream tmp;
	tmp << L"Camera Pos: " << camera->getPosition().x << ", " << camera->getPosition().y << ", " << camera->getPosition().z;
	xdl::XdevLString pos = xdl::XdevLString::wideToString(tmp.str());

	tmp.str(L"");
	tmp.clear();
	tmp << L"XdevL Platform Engine (c) 2017 Cengiz Terzibas: FPS: " << (m_fps);
	xdl::XdevLString fps = xdl::XdevLString::wideToString(tmp.str());

	textLayoutSystem->setColor(255, 255, 255, 255);
	textLayoutSystem->printText(pos, xdl::XdevLPositionF(-1.0f, -0.9f));
	textLayoutSystem->printText(fps, xdl::XdevLPositionF(-1.0f, -0.96f));

}

void Engine::updateUI(xdl::xdl_double dT) {
	Super::updateUI(dT);

	m_ui->lock(xdl::XdevLString("Rendering Settings"));
	m_ui->add(*enableNormalMapping);
	m_ui->add(*enableParalaxMapping);
	m_ui->add(*parallaxScale);
	m_ui->add(*parallaxBias);
	m_ui->add(*steepParallaxSteps);
	m_ui->add(*specularComponent);
	m_ui->unlock();
	m_ui->render();
}

//
// Initialize the rendering device.
//
xdl::xdl_int Engine::initRenderDevice() {

	// Get the FontSystem
	fontSystem = xdl::getModule<xdl::XdevLFontSystem*>(getCore(), xdl::XdevLID("MyFontSystem"));
	if(!fontSystem) {
		return xdl::RET_FAILED;
	}

	// Get the Text Layout System.
	textLayoutSystem = xdl::getModule<xdl::XdevLTextLayout*>(getCore(), xdl::XdevLID("MyTextLayout"));
	if(!textLayoutSystem) {
		return xdl::RET_FAILED;
	}

	// Supports importing images.
	m_imageServer = xdl::getModule<xdl::IPXdevLImageServer>(getCore(), xdl::XdevLID("MyImageServer"));
	if(!m_imageServer) {
		return xdl::RET_FAILED;
	}
	if(m_imageServer->create(getRAI()) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	// Supports importing 3D assets.
	m_modelServer = xdl::getModule<xdl::gfx::IPXdevLModelServer>(getCore(), xdl::XdevLID("MyModelServer"));
	if(!m_modelServer) {
		return xdl::RET_FAILED;
	}

	if(m_modelServer->create(getRAI(), m_imageServer) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}



	// Supports basic UI.
	m_ui = xdl::getModule<xdl::ui::IPXdevLUI>(getCore(), xdl::XdevLID("MyUI"));
	if(!m_ui) {
		return xdl::RET_FAILED;
	}

	// Attach the mouse to the window.
	if(m_ui->create(getWindow(), getRAI(), getArchive()) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	//getCursor()->attach(getWindow());

	return xdl::RET_SUCCESS;
}

void Engine::mouseAxisCallback(const xdl::XdevLDelegateAxisParameter& parameter) {
	if(parameter.id == xdl::AXIS_2) {
		camera->moveForward(parameter.value * 0.01f);
	}
}

void Engine::mouseButtonCallback(const xdl::XdevLDelegateActionParameter& actionParameter) {
	if(actionParameter.state == xdl::BUTTON_PRESSED) {
		tmath::mat4 view = camera->getTransformationMatrix();
		tmath::mat4 proj = camera->getProjectionMatrix();
		auto worldPos = xdl::XdevLRaycast::screenToWorld(x_axis->getValue(), y_axis->getValue(), camera->getNearClipPlane(), xdl::xdl_true, getRAI()->getViewport(), proj, view);
		ClickVertex vertex { worldPos, 1.0f, 1.0f, 1.0f, 1.0f};
		m_vertices.push_back(vertex);
//		if(actionParameter.id == xdl::BUTTON_RIGHT) {
//			getCursor()->hide();
////			getCursor()->enableRelativeMotion();
//		}
	} else {
		getCursor()->show();
	}
}


//
// Initialize the framebuffer.
//
xdl::xdl_int Engine::initFramebuffer() {

	m_frameBuffer = getRAI()->createFrameBuffer();
	m_frameBuffer->init(getWindow()->getWidth(), getWindow()->getHeight());

	m_frameBuffer->addColorTarget(0	, xdl::XdevLFrameBufferColorFormat::RGBA8);
	m_frameBuffer->addDepthTarget(xdl::XdevLFrameBufferDepthFormat::COMPONENT16);

	auto texture = m_frameBuffer->getTexture(0);
	texture->lock();

	const xdl::XdevLTextureSamplerFilterStage filterStage {xdl::XdevLTextureFilterMin::NEAREST, xdl::XdevLTextureFilterMag::LINEAR};
	texture->setTextureFilter(filterStage);

	texture->unlock();

	return xdl::RET_SUCCESS;
}

xdl::xdl_int Engine::initUI() {
	enableNormalMapping = new xdl::ui::XdevLUICheckBox(xdl::XdevLString("Normal Mapping"), isCheckBoxChecked(fragmentUniformBuffer, NORMAL_MAPPING));
	enableNormalMapping->bind(xdl::ui::XdevLDelegateChecked::Create<Engine, &Engine::enableNormalMappingCheckBox>(this));

	enableParalaxMapping = new xdl::ui::XdevLUICheckBox(xdl::XdevLString("Parallax Mapping"), isCheckBoxChecked(fragmentUniformBuffer, PARALLAX_MAPPING));
	enableParalaxMapping->bind(xdl::ui::XdevLDelegateChecked::Create<Engine, &Engine::enableParallaxMappingCheckBox>(this));

	std::vector<xdl::ui::XdevLUISlider::Type> initialScale {fragmentUniformBuffer.parallaxScale};
	parallaxScale = new xdl::ui::XdevLUISlider(xdl::XdevLString("Parallax Scale"), initialScale);
	parallaxScale->bind(xdl::ui::XdevLDelegateSliderMotion::Create<Engine, &Engine::parallaxScaleSlider>(this));

	std::vector<xdl::ui::XdevLUISlider::Type> initialBias {fragmentUniformBuffer.parallaxBias};
	parallaxBias = new xdl::ui::XdevLUISlider(xdl::XdevLString("Parallax Bias"), initialBias);
	parallaxBias->bind(xdl::ui::XdevLDelegateSliderMotion::Create<Engine, &Engine::parallaxBiasSlider>(this));

	std::vector<xdl::ui::XdevLUISlider::Type> initialSteepParallaxSteps {fragmentUniformBuffer.steepParallaxSteps};
	steepParallaxSteps = new xdl::ui::XdevLUISlider(xdl::XdevLString("Steep Parallax Steps"), 1.0f, 32.0f, initialSteepParallaxSteps);
	steepParallaxSteps->bind(xdl::ui::XdevLDelegateSliderMotion::Create<Engine, &Engine::steepParallaxStepsSlider>(this));

	std::vector<xdl::ui::XdevLUISlider::Type> initialSpecularComponent {fragmentUniformBuffer.specularComponent};
	specularComponent = new xdl::ui::XdevLUISlider(xdl::XdevLString("Specular Component"), 0.0f, 128.0f, initialSpecularComponent);
	specularComponent->bind(xdl::ui::XdevLDelegateSliderMotion::Create<Engine, &Engine::specularComponentSlider>(this));

	return xdl::RET_SUCCESS;
}

void Engine::enableNormalMappingCheckBox(const xdl::ui::XdevLUICheckedEvent& event) {
	if(event.checked) {
		fragmentUniformBuffer.mappingStates |= NORMAL_MAPPING;
	} else {
		fragmentUniformBuffer.mappingStates &= ~NORMAL_MAPPING;
	}
}

void Engine::enableParallaxMappingCheckBox(const xdl::ui::XdevLUICheckedEvent& event) {
	if(event.checked) {
		fragmentUniformBuffer.mappingStates |= PARALLAX_MAPPING;
	} else {
		fragmentUniformBuffer.mappingStates &= ~PARALLAX_MAPPING;
	}
}

void Engine::parallaxScaleSlider(const xdl::ui::XdevLUISliderMotionEvent& event) {
	fragmentUniformBuffer.parallaxScale = event.values[0];
}

void Engine::parallaxBiasSlider(const xdl::ui::XdevLUISliderMotionEvent& event) {
	fragmentUniformBuffer.parallaxBias = event.values[0];
}

void Engine::steepParallaxStepsSlider(const xdl::ui::XdevLUISliderMotionEvent& event) {
	fragmentUniformBuffer.steepParallaxSteps = event.values[0];
}


void Engine::specularComponentSlider(const xdl::ui::XdevLUISliderMotionEvent& event) {
	fragmentUniformBuffer.specularComponent = event.values[0];
}

//
// Initialize the rendering assets.
//
xdl::xdl_int Engine::initRenderAssets() {
	xdl::XdevLSceneContructParameter sceneConstructParameter {};
	m_scene = new xdl::XdevLScene(sceneConstructParameter);

	//
	// Create model assets.
	//
	xdl::gfx::IPXdevLProceduralSystem proceduralSystem = m_modelServer->getProceduralSystem();

//	file = m_archive->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName("box.obj"));
//	auto boxModel = m_modelServer->import(file);

	auto boxModel = m_modelServer->createModel();//m_modelServer->import(file);
	auto boxMaterial = std::make_shared<xdl::gfx::XdevLMaterial>();

	auto mesh = proceduralSystem->createBox(xdl::XdevLString(TEXT("MyBox")), 1.0f, 1.0f, 1.0f);
	mesh->setParentModel(boxModel);
	mesh->setMaterial(boxMaterial);
	boxModel->add(mesh);

	xdl::XdevLActorContructParameter boxActorConstructParameter {};
	boxActorConstructParameter.name = xdl::XdevLString(TEXT("Box"));

	auto boxActor = new xdl::XdevLActor(boxActorConstructParameter);
	boxActor->setModel(boxModel);
	boxActor->setPosition(tmath::vec3(-1.0f, 2.0f, 3.0f));
	boxActor->setScale(tmath::vec3(1.0f, 1.0f, 1.0f));

	m_scene->addActor(boxActor);

	xdl::XdevLLightContructParameter lightConstructParameter {};
	lightConstructParameter.lit = xdl::xdl_false;
	lightConstructParameter.lightType = xdl::XdevLLightType::POINT;
	lightConstructParameter.name = xdl::XdevLString(TEXT("Light"));
	lightConstructParameter.model = boxModel;
	light = new xdl::XdevLLight(lightConstructParameter);
	light->setScale(tmath::vec3(0.05f, 0.05f, 0.05f));

	m_scene->addActor(light);


	xdl::XdevLCameraContructParameter cameraConstructParameter {};
	cameraConstructParameter.lit = xdl::xdl_false;
	camera = new xdl::XdevLCamera(cameraConstructParameter);
	camera->setForwardSpeed(20.0f);
	camera->setPosition(tmath::vec3(0.0f, 0.0f, 3.0f));
	camera->setProjection(xdl::XdevLCameraProjectionType::PERSPECTIVE);
	camera->setViewPort(getRAI()->getViewport());

	// -------------------------------------------------------------------------

	m_sp = getRAI()->createShaderProgram();
	m_sp->create(xdl::XdevLString("plane"));

	// Create the shader program.
	m_vs = getRAI()->createVertexShader();
	m_fs = getRAI()->createFragmentShader();

	auto vertexShaderfile = getArchive()->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName("shaders/engine_demo/default_vs.glsl"));
	auto vertexFragmentfile = getArchive()->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName("shaders/engine_demo/default_fs.glsl"));

	m_vs->addShaderCode(vertexShaderfile);
	m_fs->addShaderCode(vertexFragmentfile);

	m_vs->compile();
	m_fs->compile();


	auto result = m_sp->attach(m_vs);
	if(xdl::RET_SUCCESS != result) {
		return result;
	}

	result = m_sp->attach(m_fs);
	if(xdl::RET_SUCCESS != result) {
		return result;
	}

	result = m_sp->link();
	if(xdl::RET_SUCCESS != result) {
		return result;
	}


	m_diffuseMapTextureID = m_sp->getUniformLocation("diffuseTexture");
	m_normalMapTextureID = m_sp->getUniformLocation("normalTexture");
	m_heightMapTextureID = m_sp->getUniformLocation("heightTexture");

	m_uniformBuffer = getRAI()->createUniformBuffer();
	m_uniformBuffer->init(sizeof(vertexShaderUniformBuffer));
	m_uniformBuffer->bind(0);
	m_sp->bind(xdl::XdevLString("VertexShaderUniformBuffer"), 0);

	m_fragmentUniformBuffer = getRAI()->createUniformBuffer();
	m_fragmentUniformBuffer->init(sizeof(fragmentShaderUniformBuffer));
	m_fragmentUniformBuffer->bind(1);
	m_sp->bind(xdl::XdevLString("FragmentShaderUniformBuffer"), 1);


	//
	// Initialize font system.
	//

	xdl::XdevLFontSystemCreateParameter fontSystemCreateParameter;
	fontSystemCreateParameter.rai = getRAI();
	fontSystem->create(fontSystemCreateParameter);

	auto fontFile = getArchive()->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName("fonts/default_info.txt"));
	auto font = fontSystem->createFromFontFile(fontFile);
	if(nullptr == font) {
		return xdl::RET_FAILED;
	}

	xdl::XdevLTextLayoutCreateParameter textLayoutCreateParameter;
	textLayoutCreateParameter.rai = getRAI();
	textLayoutCreateParameter.window = getWindow();
	textLayoutSystem->create(textLayoutCreateParameter);
	textLayoutSystem->setScale(1.0f);
	textLayoutSystem->setDFT(0);
	textLayoutSystem->setEffect(0);
	textLayoutSystem->useFont(font);

	const xdl::XdevLFileName fileRootFolder(TEXT("images/"));
	const xdl::XdevLFileName filenameTypeAlbedo(TEXT("_albedo"));
	const xdl::XdevLFileName filenameTypeNormal(TEXT("_normal"));
	const xdl::XdevLFileName filenameTypeHeight(TEXT("_height"));
	const xdl::XdevLFileName filenameExtension(TEXT(".png"));

//	const xdl::XdevLFileName filenameBase(TEXT("mybricks1"));
	const xdl::XdevLFileName filenameBase(TEXT("redbricks2b"));
//	const xdl::XdevLFileName filenameBase(TEXT("blocksrough"));
//	const xdl::XdevLFileName filenameBase(TEXT("harshbricks"));


	//
	// Load albedo image.
	//
	const xdl::XdevLFileName fAlbedo = fileRootFolder + filenameBase + filenameTypeAlbedo + filenameExtension;
	auto diffuseMapFile = getArchive()->open(xdl::XdevLOpenForReadOnly(), fAlbedo);
	auto diffuseMapImageObjects = m_imageServer->import(diffuseMapFile);
	if(nullptr == diffuseMapImageObjects) {
		return xdl::RET_FAILED;
	}
	m_diffuseMap = xdl::convert(diffuseMapImageObjects, getRAI());

	diffuseMapImageObjects->destroy();

	//
	// Load normal image.
	//
	const xdl::XdevLFileName fnormal = fileRootFolder + filenameBase + filenameTypeNormal + filenameExtension;
	auto normalMapFile = getArchive()->open(xdl::XdevLOpenForReadOnly(), fnormal);
	auto normalMapImageObjects = m_imageServer->import(normalMapFile);
	if(nullptr == normalMapImageObjects) {
		return xdl::RET_FAILED;
	}
	m_normalMap = xdl::convert(normalMapImageObjects, getRAI());

	normalMapImageObjects->destroy();

	//
	// Load height image.
	//
	const xdl::XdevLFileName fheight = fileRootFolder + filenameBase + filenameTypeHeight + filenameExtension;
	auto heightMapFile = getArchive()->open(xdl::XdevLOpenForReadOnly(), fheight);
	auto heightMapImageObjects = m_imageServer->import(heightMapFile);
	if(nullptr == heightMapImageObjects) {
		return xdl::RET_FAILED;
	}
	m_heightMap = xdl::convert(heightMapImageObjects, getRAI());

	heightMapImageObjects->destroy();

	auto file = getArchive()->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName("plane.obj"));
	auto model = m_modelServer->import(file);

	auto material = std::make_shared<xdl::gfx::XdevLMaterial>();

	material->setTexture(xdl::gfx::XdeLMaterialTextures::DIFFUSE_MAP, m_diffuseMap);
	material->setTexture(xdl::gfx::XdeLMaterialTextures::NORMAL_MAP, m_normalMap);
	material->setTexture(xdl::gfx::XdeLMaterialTextures::DISPLACEMENT_MAP, m_heightMap);
	material->setUseDiffuseMap(xdl::xdl_true);
	material->setUseNormalMap(xdl::xdl_true);
	material->setUseDisplacementMap(xdl::xdl_true);


	model->getMesh(0)->setMaterial(material);

	xdl::XdevLActorContructParameter planeActorConstructParameter {};
	planeActorConstructParameter.name = xdl::XdevLString(TEXT("Plane"));

	auto planeActor = new xdl::XdevLActor(planeActorConstructParameter);
	planeActor->setModel(model);
	planeActor->setScale(tmath::vec3(2.0f, 2.0f, 2.0f));

	m_scene->addActor(planeActor);






	m_clickVertexDeclaration = getRAI()->createVertexDeclaration();
	m_clickVertexDeclaration->add(3, xdl::XdevLBufferElementTypes::FLOAT, 0);
	m_clickVertexDeclaration->add(4, xdl::XdevLBufferElementTypes::FLOAT, 4);
	m_clickVertexBuffer = getRAI()->createVertexBuffer();
	m_clickVertexBuffer->init();

	m_clickVertexArray = getRAI()->createVertexArray();
	m_clickVertexArray->init(m_clickVertexBuffer, m_clickVertexDeclaration);


	return xdl::RET_SUCCESS;
}

xdl::xdl_int Engine::notify(xdl::XdevLEvent& event) {
	switch(event.type) {
		case xdl::XDEVL_CORE_EVENT: {
			if(event.core.type == xdl::XDEVL_CORE_SHUTDOWN) {
				std::cout << "XDEVL_CORE_SHUTDOWN received.\n";
				m_appIsRunning = xdl::xdl_false;
			}
		}
		break;
		case xdl::XDEVL_WINDOW_EVENT: {
			switch(event.window.event) {
				case xdl::XDEVL_WINDOW_MOVED:
				case xdl::XDEVL_WINDOW_RESIZED: {
					initFramebuffer();
				}
				break;
			}
		}
		break;
	}
	return xdl::RET_SUCCESS;
}
