/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLFileSystem/XdevLArchive.h>
#include <XdevLVulkanContext/XdevLVulkanContext.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>

#include <tm/tm.h>
#include <array>

xdl::IPXdevLCore core;
xdl::IPXdevLWindow window;
xdl::IPXdevLArchive archive;
xdl::IPXdevLKeyboard keyboard;
xdl::IPXdevLButton esc;
xdl::IPXdevLVulkanContext vulkan_context;
xdl::IPXdevLVulkanInstance vulkan_instance;
xdl::IPXdevLVulkanDevice vulkan_device;
xdl::IPXdevLVulkanDescriptorLayout vulkan_descriptor_layout;
xdl::IPXdevLVulkanQueue vulkan_render_queue;
xdl::IPXdevLVulkanQueue vulkan_present_queue;
xdl::IPXdevLVulkanCommandPool vulkan_command_pool;
xdl::IPXdevLVulkanSurface vulkan_surface;
xdl::IPXdevLVulkanSwapChain vulkan_swapchain;
xdl::IPXdevLVulkanVertexBuffer vulkan_vertex_buffer;
xdl::IPXdevLVulkanIndexBuffer vulkan_index_buffer;
xdl::IPXdevLVulkanUniformBuffer vulkan_uniform_buffer;
xdl::IPXdevLVulkanShader vulkan_vertex_shader;
xdl::IPXdevLVulkanShader vulkan_fragment_shader;
xdl::xdl_bool running = xdl::xdl_true;

//
// Vulkan native handles.
//
VkDevice device = VK_NULL_HANDLE;

VkPipelineCache pipelineCache = VK_NULL_HANDLE;
VkPipeline graphicsPipeline = VK_NULL_HANDLE;

VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
VkDescriptorSet descriptorSet = VK_NULL_HANDLE;


std::vector<VkCommandBuffer> drawCommandBuffers	{VK_NULL_HANDLE};


struct Vertex {
	xdl::xdl_float position[4];
	xdl::xdl_float normal[3];
	xdl::xdl_float color[4];
};


struct UniformBufferForVertexShader {
	tmath::mat4 projectionMatrix;
	tmath::mat4 modelMatrix;
	tmath::mat4 viewMatrix;
};

UniformBufferForVertexShader uboVS;

void setupCommandBuffers() {

	// -------------------------------------------------------------------------
	// Create Command Buffers
	//

	drawCommandBuffers.resize(vulkan_swapchain->getSwapChainImages().size());
	drawCommandBuffers.reserve(vulkan_swapchain->getSwapChainImages().size());

	VkCommandBufferAllocateInfo commandBufferAllocateInfo {};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandPool = vulkan_command_pool->getNativeHandle();
	commandBufferAllocateInfo.commandBufferCount = drawCommandBuffers.size();

	vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, drawCommandBuffers.data());

	// -------------------------------------------------------------------------
}

void setupDescriptorPool() {

	// -------------------------------------------------------------------------
	// Create DescriptorPool
	//

	std::vector<VkDescriptorPoolSize> typeCounts;

	VkDescriptorPoolSize descriptorPoolSize;
	descriptorPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorPoolSize.descriptorCount = 1;

	typeCounts.push_back(descriptorPoolSize);

	VkDescriptorPoolCreateInfo descriptorPoolInfo {};
	descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	descriptorPoolInfo.poolSizeCount = (uint32_t)typeCounts.size();
	descriptorPoolInfo.pPoolSizes = typeCounts.data();
	descriptorPoolInfo.maxSets = typeCounts.size();

	VK_CHECK_RESULT(vkCreateDescriptorPool(device, &descriptorPoolInfo, nullptr, &descriptorPool));

	// -------------------------------------------------------------------------
}

void setupDescriptorSet() {

	// -------------------------------------------------------------------------
	// Create descriptor set.
	//

	VkDescriptorSetAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = vulkan_descriptor_layout->getNativeHandlePtr();

	VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &allocInfo, &descriptorSet));

	VkWriteDescriptorSet writeDescriptorSet {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.dstSet = descriptorSet;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

	auto desscriptorBufferInfo = vulkan_uniform_buffer->getDescriptorBufferInfo();
	writeDescriptorSet.pBufferInfo = &desscriptorBufferInfo;
	writeDescriptorSet.dstBinding = 0;

	vkUpdateDescriptorSets(device, 1, &writeDescriptorSet, 0, NULL);

	// -------------------------------------------------------------------------
}

void setupPipelineLayout() {

	// -------------------------------------------------------------------------
	// Create Pipeline Layout.
	//

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo {};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = vulkan_descriptor_layout->getNativeHandlePtr();

	VK_CHECK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

	// -------------------------------------------------------------------------
}

void setupGraphicsPipelines() {

	// -------------------------------------------------------------------------
	// Create Pipeline
	//

	VkPipelineRasterizationStateCreateInfo rasterizationState {};
	rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizationState.depthClampEnable = VK_FALSE;
	rasterizationState.rasterizerDiscardEnable = VK_FALSE;
	rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizationState.lineWidth = 1.0f;
	rasterizationState.cullMode = VK_CULL_MODE_NONE;
	rasterizationState.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizationState.depthBiasEnable = VK_FALSE;

	VkPipelineViewportStateCreateInfo viewportState {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.scissorCount = 1;

	std::vector<VkDynamicState> dynamicStateEnables {};
	dynamicStateEnables.push_back(VK_DYNAMIC_STATE_VIEWPORT);
	dynamicStateEnables.push_back(VK_DYNAMIC_STATE_SCISSOR);

	VkPipelineDynamicStateCreateInfo dynamicState {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.pDynamicStates = dynamicStateEnables.data();
	dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());

	VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo {};
	pipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	pipelineMultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	// Blending is not used in this example
	VkPipelineColorBlendAttachmentState blendAttachmentState {};
	blendAttachmentState.colorWriteMask = 0xf;
	blendAttachmentState.blendEnable = VK_FALSE;

	VkPipelineColorBlendStateCreateInfo colorBlendState = {};
	colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendState.attachmentCount = 1;
	colorBlendState.pAttachments = &blendAttachmentState;


	// -------------------------------------------------------------------------
	// Create and attach shaders.
	//
	xdl::XdevLFileName vertex_shader_filename("shaders/vulkan_context_demo.vert.spv");
	xdl::XdevLFileName fragment_shader_filename("shaders/vulkan_context_demo.frag.spv");

	if(archive->fileExists(vertex_shader_filename) == xdl::xdl_false) {
		XDEVL_ASSERT(false, "Vertex Shader file doesn't exists.");
	}
	if(archive->fileExists(fragment_shader_filename) == xdl::xdl_false) {
		XDEVL_ASSERT(false, "Fragment Shader file doesn't exists.");
	}
	vulkan_vertex_shader = vulkan_device->createVertexShader();
	vulkan_fragment_shader = vulkan_device->createFragmentShader();

	vulkan_vertex_shader->create();
	vulkan_fragment_shader->create();

	auto file = archive->open(xdl::XdevLOpenForReadOnly(), vertex_shader_filename);
	vulkan_vertex_shader->load(file);
	file->close();
	file =  archive->open(xdl::XdevLOpenForReadOnly(), fragment_shader_filename);
	vulkan_fragment_shader->load(file);

	std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages {};
	shaderStages[0] = vulkan_vertex_shader->getPipelineShaderStageCreateInfo();
	shaderStages[1] = vulkan_fragment_shader->getPipelineShaderStageCreateInfo();

	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// Set buffer binding information.
	//

	VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo = vulkan_vertex_buffer->getPipelineVertexInputStateCreateInfo();

	// -------------------------------------------------------------------------

	VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo {};
	pipelineDepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	pipelineDepthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
	pipelineDepthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
	pipelineDepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	pipelineDepthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
	pipelineDepthStencilStateCreateInfo.back.failOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.back.passOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
	pipelineDepthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;
	pipelineDepthStencilStateCreateInfo.front = pipelineDepthStencilStateCreateInfo.back;

	VkGraphicsPipelineCreateInfo pipelineCreateInfo {};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.flags = VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT;
	pipelineCreateInfo.pViewportState = &viewportState;
	pipelineCreateInfo.pColorBlendState = &colorBlendState;
	pipelineCreateInfo.pRasterizationState = &rasterizationState;
	pipelineCreateInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
	pipelineCreateInfo.pDepthStencilState = &pipelineDepthStencilStateCreateInfo;
	pipelineCreateInfo.pMultisampleState = &pipelineMultisampleStateCreateInfo;
	pipelineCreateInfo.pDynamicState = &dynamicState;
	pipelineCreateInfo.renderPass = vulkan_swapchain->getRenderPass();
	pipelineCreateInfo.layout = pipelineLayout;
	pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineCreateInfo.pStages = shaderStages.data();

	VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo {};
	pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	pipelineInputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	pipelineCreateInfo.pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo;


	VkPipelineCacheCreateInfo pipelineCacheCreateInfo {};
	pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
	VK_CHECK_RESULT(vkCreatePipelineCache(device, &pipelineCacheCreateInfo, nullptr, &pipelineCache));

	VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &graphicsPipeline));

	// -------------------------------------------------------------------------
}


void updateUniformBuffers() {

	// -------------------------------------------------------------------------
	// Update Uniform buffer.
	//

	xdl::xdl_float aspect = (xdl::xdl_float)window->getWidth()/(xdl::xdl_float)window->getHeight();
	xdl::xdl_float fov = 90.0f;

	tmath::perspective(fov, aspect, 0.5f, 100.0f, uboVS.projectionMatrix);
	tmath::translate(0.0f, 0.0f, -5.0f, uboVS.viewMatrix);

	static xdl::xdl_float angle = 0.0f;
	tmath::mat4 rx, ry;
	tmath::rotate_x(angle, rx);
	tmath::rotate_y(angle, ry);
	uboVS.modelMatrix = rx * ry;

	angle += core->getDT() * 90.0f;

	vulkan_uniform_buffer->lock();
	vulkan_uniform_buffer->upload((xdl::xdl_uint8*)&uboVS, sizeof(UniformBufferForVertexShader));
	vulkan_uniform_buffer->unlock();

	// -------------------------------------------------------------------------
}

void setupVertexIndexAndUniformBuffer() {

	// -------------------------------------------------------------------------
	// Setup vertices and indices.
	//

	std::vector<Vertex> vertexBuffer = {
		{-1.0f,-1.0f,-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f,-1.0f, 1.0f, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f,-1.0f, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f,-1.0f, 1.0f,  0.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f,-1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{-1.0f,-1.0f,-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f,-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f,-1.0f, 1.0f, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f, 1.0f, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f,-1.0f, 1.0f,  0.0f,-1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{-1.0f, 1.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f,-1.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f, 1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f,-1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f, 1.0f,-1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f,-1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f, 1.0f,-1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f,-1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f,-1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f},

		{ 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
		{ 1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f}
	};
	uint32_t vertexBufferSize = static_cast<uint32_t>(vertexBuffer.size()) * sizeof(Vertex);

	vulkan_vertex_buffer = vulkan_device->createVertexBuffer();
	vulkan_vertex_buffer->addElement(0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, offsetof(Vertex, position));
	vulkan_vertex_buffer->addElement(0, 1, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, normal));
	vulkan_vertex_buffer->addElement(0, 2, VK_FORMAT_R32G32B32A32_SFLOAT, offsetof(Vertex, color));
	vulkan_vertex_buffer->init((xdl::xdl_uint8*)vertexBuffer.data(), vertexBufferSize);


	//
	// Setup indices.
	//
	std::vector<uint32_t> indexBuffer = {
		0, 1, 2,
		3, 4, 5,
		6, 7, 8,
		9, 10, 11,
		12, 13, 14,
		15, 16, 17,
		18, 19, 20,
		21, 22, 23,
		24, 25, 26,
		27, 28, 29,
		30, 31, 32,
		33, 34, 35
	};
	uint32_t indexBufferSize = indexBuffer.size() * sizeof(uint32_t);

	vulkan_index_buffer = vulkan_device->createIndexBuffer();
	vulkan_index_buffer->init((xdl::xdl_uint8*)indexBuffer.data(), indexBufferSize, indexBuffer.size());

	//
	// Setup Uniform Buffer.
	//
	vulkan_uniform_buffer = vulkan_device->createUniformBuffer();
	vulkan_uniform_buffer->init(sizeof(UniformBufferForVertexShader));

	// -------------------------------------------------------------------------
}



void fillCommandBuffers() {

	// -------------------------------------------------------------------------
	// Fill the command buffer.
	//

	VkCommandBufferBeginInfo commandBufferBeginInfo {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

	std::array<VkClearValue, 2> clearValues {};
	clearValues[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
	clearValues[1].depthStencil = { 1.0f, 0 };

	VkRenderPassBeginInfo renderPassBeginInfo {};
	renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBeginInfo.renderPass = vulkan_swapchain->getRenderPass();
	renderPassBeginInfo.renderArea.offset.x = 0;
	renderPassBeginInfo.renderArea.offset.y = 0;
	renderPassBeginInfo.renderArea.extent = vulkan_swapchain->getExtent2D();
	renderPassBeginInfo.clearValueCount = clearValues.size();
	renderPassBeginInfo.pClearValues = clearValues.data();

	for(size_t i = 0; i < drawCommandBuffers.size(); ++i) {

		renderPassBeginInfo.framebuffer = vulkan_swapchain->getSwapChainFramebuffers()[i];

		vkBeginCommandBuffer(drawCommandBuffers[i], &commandBufferBeginInfo);
		{
			vkCmdBeginRenderPass(drawCommandBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
			{
				VkViewport viewport {};
				viewport.width = (xdl::xdl_float)window->getWidth();
				viewport.height = (xdl::xdl_float)window->getHeight();
				viewport.minDepth = (xdl::xdl_float) 0.0f;
				viewport.maxDepth = (xdl::xdl_float) 1.0f;
				vkCmdSetViewport(drawCommandBuffers[i], 0, 1, &viewport);

				VkRect2D scissor {};
				scissor.extent = vulkan_swapchain->getExtent2D();

				vkCmdSetScissor(drawCommandBuffers[i], 0, 1, &scissor);

				vkCmdBindDescriptorSets(drawCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);

				vkCmdBindPipeline(drawCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

				VkDeviceSize offsets[1] = { 0 };
				VkBuffer vertex_buffer = vulkan_vertex_buffer->getBufferHandle();
				vkCmdBindVertexBuffers(drawCommandBuffers[i], 0, 1, &vertex_buffer, offsets);

//				vkCmdDraw(drawCommandBuffers[i], 3, 1, 0, 0 );

				vkCmdBindIndexBuffer(drawCommandBuffers[i], vulkan_index_buffer->getBufferHandle(), 0, VK_INDEX_TYPE_UINT32);
				vkCmdDrawIndexed(drawCommandBuffers[i], vulkan_index_buffer->getNumberOfIndices(), 1, 0, 0, 1);
			}
			vkCmdEndRenderPass(drawCommandBuffers[i]);
		}

		vkEndCommandBuffer(drawCommandBuffers[i]);
	}

	// -------------------------------------------------------------------------
}

void notify(xdl::XdevLEvent& event) {
	switch(event.type) {
		case xdl::XDEVL_CORE_EVENT: {
			if(event.core.type == xdl::XDEVL_CORE_SHUTDOWN) {
				running = xdl::xdl_false;
			}
		}
		break;
		case xdl::XDEVL_WINDOW_EVENT: {
			switch(event.window.event) {
				case xdl::XDEVL_WINDOW_MOVED:
				case xdl::XDEVL_WINDOW_RESIZED:
					break;
			}
		}
		break;
	}
}

xdl::xdl_int initXdevL(int argc, char* argv[]) {

	// Create the core system.
	if(xdl::createCore(&core, argc, argv, xdl::XdevLFileName("vulkan_context_demo.xml")) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	// Register the specified instance as observer.
	auto ret = core->registerListener(notify);
	if(xdl::RET_SUCCESS != ret) {
		return xdl::RET_FAILED;
	}

	// Create a window so that we can draw something.
	window = xdl::getModule<xdl::IPXdevLWindow>(core, xdl::XdevLID("MyWindow"));
	if(!window) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	ret = window->create();
	if(xdl::RET_SUCCESS != ret) {
		return xdl::RET_FAILED;
	}
	window->show();

	// Get the instance to the keyboard module.
	keyboard = xdl::getModule<xdl::IPXdevLKeyboard>(core, xdl::XdevLID("MyKeyboard"));
	if(!keyboard) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}
	if(keyboard->attach(window) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	keyboard->getButton(xdl::KEY_ESCAPE, &esc);

	archive = xdl::getModule<xdl::IPXdevLArchive> (core,  xdl::XdevLID("MyArchive"));
	if(nullptr == archive) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}
	archive->mount(xdl::XdevLFileName("."));

	// Get the OpenGL Rendering System.
	vulkan_context = xdl::getModule<xdl::IPXdevLVulkanContext>(core, xdl::XdevLID("MyVulkanContext"));
	if(!vulkan_context) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	VkApplicationInfo applicationInfo {};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pApplicationName = "XdevLVulkanContext";
	applicationInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
	applicationInfo.pEngineName = "XdevLSDK";
	applicationInfo.engineVersion = VK_MAKE_VERSION(0, 1, 0);
	applicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 40);

	if(vulkan_context->createContext(window, applicationInfo) != xdl::RET_SUCCESS) {
		xdl::destroyCore(core);
		return xdl::RET_FAILED;
	}

	//
	// Get the needed Vulkan modules.
	//
	vulkan_instance = vulkan_context->getInstance();
	vulkan_device = vulkan_context->getDevice();
	vulkan_surface = vulkan_context->getSurface();
	vulkan_swapchain = vulkan_context->getSwapChain();
	vulkan_swapchain->createFramebuffers();

	vulkan_render_queue = vulkan_device->createQueue(xdl::XdevLVulkanQueueType::GRAPHICS);
	vulkan_present_queue = vulkan_device->createQueue(xdl::XdevLVulkanQueueType::PRESENT);
	vulkan_command_pool = vulkan_device->createCommandPool(xdl::XdevLVulkanCommandPoolType::GRAPHICS);

	vulkan_descriptor_layout = vulkan_device->createDescriptorLayout();
	vulkan_descriptor_layout->addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT);
	vulkan_descriptor_layout->create();

	//
	// Get native Vulkan handles.
	//
	device = (VkDevice)vulkan_device->getNativeHandle();

	return xdl::RET_SUCCESS;
}


//
// 1. Create Instance.
// 2. Create Device.
// 3. Create Surface.
// 4. Create SwapChain.
// All the steps above are done in the XdevLVulkanContext class when using
// XdevLVulkanContext::createContext. You may do all those steps manually.


int main(int argc, char* argv[]) {

	//
	//
	// Initialize all XdevL plugins and modules.
	//
	if(initXdevL(argc, argv) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	setupVertexIndexAndUniformBuffer();
	setupPipelineLayout();
	setupGraphicsPipelines();
	setupDescriptorPool();
	setupDescriptorSet();
	setupCommandBuffers();
	fillCommandBuffers();

	VkSemaphore presentSemaphore = VK_NULL_HANDLE;
	VkSemaphoreCreateInfo presentCompleteSemaphoreCreateInfo {};
	presentCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &presentCompleteSemaphoreCreateInfo, nullptr, &presentSemaphore);


	VkSemaphore renderSemaphore = VK_NULL_HANDLE;
	VkSemaphoreCreateInfo renderCompleteSemaphoreCreateInfo {};
	renderCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &renderCompleteSemaphoreCreateInfo, nullptr, &renderSemaphore);


	//
	// Start main loop.
	//
	while(running) {
		core->update();

		if(esc->getClicked()) {
			break;
		}

		updateUniformBuffers();

		xdl::xdl_uint32 currentBuffer;
		VkResult result = vkAcquireNextImageKHR(device, vulkan_swapchain->getNativeHandle(), std::numeric_limits<uint64_t>::max(), presentSemaphore, VK_NULL_HANDLE, &currentBuffer);
		if(VK_SUCCESS == result || VK_SUBOPTIMAL_KHR == result) {

			//
			// Submit the commands.
			//
			VkPipelineStageFlags waitStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			VkSubmitInfo submitInfo {};
			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submitInfo.pWaitDstStageMask = &waitStageMask;
			submitInfo.pWaitSemaphores = &presentSemaphore;
			submitInfo.waitSemaphoreCount = 1;
			submitInfo.pSignalSemaphores = &renderSemaphore;
			submitInfo.signalSemaphoreCount = 1;
			submitInfo.pCommandBuffers = &drawCommandBuffers[currentBuffer];
			submitInfo.commandBufferCount = 1;

			vkQueueSubmit(vulkan_render_queue->getNativeHandle(), 1, &submitInfo, VK_NULL_HANDLE);

			//
			// When ready present the framebuffer.
			//
			VkPresentInfoKHR present {};
			present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
			present.swapchainCount = 1;
			present.waitSemaphoreCount = 1;
			present.pWaitSemaphores = &renderSemaphore;
			present.pSwapchains = vulkan_swapchain->getNativeHandlePtr();
			present.pImageIndices = &currentBuffer;

			VkResult result = vkQueuePresentKHR(vulkan_present_queue->getNativeHandle(), &present);
			if(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR) {
				result = vkQueueWaitIdle(vulkan_present_queue->getNativeHandle());
			}
		}
	}

	vkDeviceWaitIdle(device);

	vkDestroySemaphore(device, presentSemaphore, nullptr);
	vkDestroySemaphore(device, renderSemaphore, nullptr);

	vkDestroyPipeline(device, graphicsPipeline, nullptr);
	vkDestroyPipelineCache(device, pipelineCache, nullptr);
	vkDestroyPipelineLayout(device, pipelineLayout, nullptr);

	vkFreeCommandBuffers(device, vulkan_command_pool->getNativeHandle(), drawCommandBuffers.size(), drawCommandBuffers.data());
	vkFreeDescriptorSets(device, descriptorPool, 1, &descriptorSet);

	vkDestroyDescriptorPool(device, descriptorPool, nullptr);


	vulkan_descriptor_layout = nullptr;
	vulkan_command_pool = nullptr;
	vulkan_present_queue = nullptr;
	vulkan_render_queue = nullptr;
	vulkan_uniform_buffer = nullptr;
	vulkan_vertex_buffer = nullptr;
	vulkan_index_buffer = nullptr;
	vulkan_vertex_shader = nullptr;
	vulkan_fragment_shader = nullptr;
	vulkan_swapchain = nullptr;
	vulkan_surface = nullptr;
	vulkan_device = nullptr;
	vulkan_instance = nullptr;

	xdl::destroyCore(core);

	return 0;
}
