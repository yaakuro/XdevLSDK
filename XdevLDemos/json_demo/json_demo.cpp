/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLFileSystem/XdevLArchive.h>

xdl::IPXdevLCore core = nullptr;

namespace xdl {
	namespace utils {
		
		struct XdevLJsonPair {
			XdevLString name;
			XdevLString value;
		};
		
		class XdevLJsonObject {
			public:
			std::map<XdevLString, XdevLString> pair;
		};
		
		class XdevLJson {
			public:
				xdl_int createFromFile(IPXdevLFile& file);
		};


		xdl_int XdevLJson::createFromFile(IPXdevLFile& file) {
			XdevLString jsonString;
			file->readString(jsonString);

			
			return RET_FAILED;
		}

		void XdevLJson::createJsonObject(XdevLString& str, XdevLJsonObject* parent) {
			auto objectBegin = str.find_first_of("{");
			if(objectBegin != XdevLString::npos) {
				if(parent == nullptr) {
					
				}
			}
		}
	}
}

void programExit() {
	xdl::destroyCore(core);
}

int main(int argc, char** argv) {

	atexit(programExit);

	if(xdl::createCore(&core, argc, argv, xdl::XdevLFileName("json_demo.xml")) != xdl::RET_SUCCESS) {
		return xdl::RET_FAILED;
	}

	xdl::IPXdevLArchive archive = xdl::getModule<xdl::IPXdevLArchive> (core,  xdl::XdevLID("MyArchive"));
	if(nullptr == archive) {
		return xdl::RET_FAILED;
	}
	archive->mount(xdl::XdevLFileName("."));

	xdl::XdevLFileName jsonFileName("json.txt");

	if(archive->fileExists(jsonFileName) == xdl::xdl_false) {
		return xdl::RET_FAILED;
	}

	auto file = archive->open(xdl::XdevLOpenForReadOnly(), jsonFileName);
	if(nullptr == file) {
		return xdl::RET_FAILED;
	}

	xdl::utils::XdevLJson jsonObject;
	jsonObject.createFromFile(file);

	return 0;
}
