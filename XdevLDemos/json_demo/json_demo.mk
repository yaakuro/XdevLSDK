##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug_Linux64
ProjectName            :=json_demo
ConfigurationName      :=Debug_Linux64
WorkspacePath          :=/home/yaakuro/Public/Projects/XdevLSDK/XdevLDemos
ProjectPath            :=/home/yaakuro/Public/Projects/XdevLSDK/XdevLDemos/xml_demo
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=yaakuro
Date                   :=11/05/18
CodeLitePath           :=/home/yaakuro/.codelite
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../bin/$(ProjectName)d
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="json_demo.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -m64
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(XDEVL_HOME)/include $(IncludeSwitch)$(XDEVL_PLUGINS) $(IncludeSwitch)$(XDEVL_EXTERNALS)/include $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)XdevLd $(LibrarySwitch)dl $(LibrarySwitch)pthread 
ArLibs                 :=  "XdevLd" "dl" "pthread" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(XDEVL_HOME)/lib $(LibraryPathSwitch)$(XDEVL_PLUGINS) $(LibraryPathSwitch)$(XDEVL_EXTERNALS)/lib 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS := -std=c++11 -g -m64 -std=c++11 $(Preprocessors)
CFLAGS   :=  -g -m64 $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
XDEVL_HOME:=/home/yaakuro/Public/Projects/XdevLSDK/XdevL
XDEVL_PLUGINS:=/home/yaakuro/Public/Projects/XdevLSDK/XdevLPlugins
XDEVL_EXTERNALS:=/home/yaakuro/Public/Projects/XdevLSDK/XdevLExternals
ENGINE_ROOT:=/home/yaakuro/Public/Projects/Taranome
VULKAN_SDK:=/home/yaakuro/Downloads/VulkanSDK/1.1.70.1/x86_64
LD_LIBRARY_PATH:=$VULKAN_SDK/lib:$LD_LIBRARY_PATH
VK_LAYER_PATH:=$VULKAN_SDK/x86_64/etc/explicit_layer.d
VK_INSTANCE_LAYERS:="VK_LAYER_LUNARG_core_validation:VK_LAYER_GOOGLE_threading:VK_LAYER_LUNARG_parameter_validation"
Objects0=$(IntermediateDirectory)/json_demo.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) $(Objects) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)


$(IntermediateDirectory)/.d:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/json_demo.cpp$(ObjectSuffix): json_demo.cpp $(IntermediateDirectory)/json_demo.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/yaakuro/Public/Projects/XdevLSDK/XdevLDemos/xml_demo/json_demo.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/json_demo.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/json_demo.cpp$(DependSuffix): json_demo.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/json_demo.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/json_demo.cpp$(DependSuffix) -MM json_demo.cpp

$(IntermediateDirectory)/json_demo.cpp$(PreprocessSuffix): json_demo.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/json_demo.cpp$(PreprocessSuffix) json_demo.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


