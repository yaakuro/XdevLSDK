/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef VECTOR3_H
#define VECTOR3_H

namespace tmath {

	/**
		@class vectorn<T,3>
		@brief Class that represents a 3-dimensional vector.
		@author Cengiz Terzibas
	*/
	template<typename T>
	class vectorn<T, 3> {
		public:

			typedef T value_type;

			union {
				T x, r, s;
			};

			union {
				T y, g, t;
			};

			union {
				T z, b, p;
			};

			vectorn<T,3>()
				: x(static_cast<T>(0.0))
				, y(static_cast<T>(0.0))
				, z(static_cast<T>(0.0)) {
			}

			vectorn<T,3>(T vx, T vy, T vz) : x(vx), y(vy), z(vz) {
			}

			vectorn<T,3>(const T* v) : x(v[0]), y(v[1]), z(v[2]) {
			}

			vectorn<T,3>(const vectorn<T,3>& v) : x(v.x), y(v.y), z(v.z) {
			}

			vectorn<T,3>(const vectorn<T,4>& v) : x(v.x), y(v.y), z(v.z) {
			}


			inline const vectorn<T,3>& operator+=(const vectorn<T,3>& v) {
				x += v.x;
				y += v.y;
				z += v.z;
				return *this;
			}

			inline const vectorn<T,3>& operator-=(const vectorn<T,3>& v) {
				x -= v.x;
				y -= v.y;
				z -= v.z;
				return *this;
			}

			inline const vectorn<T,3>& operator*=(const T& num) {
				x *= num;
				y *= num;
				z *= num;
				return *this;
			}

			inline const vectorn<T,3>& operator/=(const T& num) {
				const T r = static_cast<T>(1.0)/num;
				x *= r;
				y *= r;
				z *=r;
				return *this;
			}

			inline friend std::ostream& operator<<(std::ostream& out, const vectorn<T,3>& v)	{
				out << "[" << v.x << "," << v.y << "," << v.z << "]";
				return out;
			}

			inline bool operator == (const vectorn<T,3>& v) const {
				return (v.x==x && v.y==y && v.z==z);
			}

			inline bool operator != (const vectorn<T,3>& v) const {
				return !(v == *this);
			}

			inline const vectorn<T,3> operator - () const {
				return vectorn<T,3>(-x, -y, -z);
			}

			inline friend const vectorn<T,3> operator+(const vectorn<T,3>& v1, const vectorn<T,3>& v2) {
				return vectorn<T,3>(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
			}

			inline friend const vectorn<T,3> operator-(const vectorn<T,3>& v1, const vectorn<T,3>& v2) {
				return vectorn<T,3>(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
			}

			inline friend const T	operator*(const vectorn<T,3>& v1, const vectorn<T,3>& v2) {
				return T(v1.x * v2.x + v1.y *v2.y + v1.z * v2.z);
			}

			inline const vectorn<T,3> operator*(const T& num) const {
				return vectorn<T,3>(x * num, y * num, z * num);
			}

			friend inline const vectorn<T,3> operator * (const T& s, const vectorn<T,3>& v) {
				return v * s;
			}

			inline const vectorn<T,3> operator/(const T& num) const {
				const T r = static_cast<T>(1.0)/num;
				return vectorn<T,3>(x * r, y * r, z * r);
			}

			inline size_t size() const {
				return 3;
			}

			operator T*() {
				return &x;
			}

			operator const T*() const {
				return &x;
			}
	};

	/// Calculate the normal of a vector.
	template<typename T>
	inline T norm(const vectorn<T,3>& v) {
		return static_cast<T>(v.x*v.x + v.y*v.y + v.z*v.z);
	}

	/// Normalize a vector.
	template<typename T>
	vectorn<T,3>& normalize(vectorn<T,3>& v) {
		T value = static_cast<T>(1.0)/sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
		v.x *= value;
		v.y *= value;
		v.z *= value;
		return v;
	}

	/// Calculate the length of a vector.
	template<typename T>
	T length(const vectorn<T,3>& v) {
		return static_cast<T>(sqrt(v.x*v.x + v.y*v.y + v.z*v.z));
	}

	/// Calculates the cross product of two vector.
	template<typename T>
	vectorn<T,3> cross(const vectorn<T,3>& v1,const vectorn<T,3>& v2) {
		return tmath::vectorn<T,3>((v1.y * v2.z) - (v1.z * v2.y),
		                           (v1.z * v2.x) - (v1.x * v2.z),
		                           (v1.x * v2.y) - (v1.y * v2.x));

	}

	/// Calculates the cross product of two vector.
	template<typename T>
	void cross(const vectorn<T,3>& v1,const vectorn<T,3>& v2, vectorn<T,3>& rv) {
		rv.x = (v1.y * v2.z) - (v1.z * v2.y);
		rv.y = (v1.z * v2.x) - (v1.x * v2.z);
		rv.z = (v1.x * v2.y) - (v1.y * v2.x);
	}

	template<typename T>
	std::ifstream& operator >> (std::ifstream& in, vectorn<T,3> &v) {
		in >> v.x >>  v.y >> v.z;
		return in;
	}

	/// Convert vectorn<T,3> to vector.
	template<typename T>
	vectorn<T,4>& convert(const vectorn<T,3>& v1, vectorn<T,4>& v2) {
		v2.x = v1.x;
		v2.y = v1.y;
		v2.z = v1.z;
		v2.w = 0.0f;
		return v2;
	}
}

#endif
