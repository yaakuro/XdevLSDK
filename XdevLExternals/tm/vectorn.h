/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef VECTORN_H
#define VECTORN_H

#include <vector>

namespace tmath {

	/**
		@class vectorn
		@brief Class that represents a n-dimensional vector
		@author Cengiz Terzibas
	*/
	template<typename T, int NUM>
	class vectorn {
		public:
			typedef T value_type;
			T cmp[NUM];
			// constructors
			vectorn<T,NUM>() {
				for(int idx = 0; idx < NUM; ++idx)
					cmp[idx] = static_cast<T>(0.0);
			}
			vectorn<T,NUM>(const T* vector) {
				for(int idx = 0; idx < NUM; ++idx)
					cmp[idx] = vector[idx];
			}
			vectorn<T,NUM>(const vectorn<T,NUM>& v)  {
				for(int idx = 0; idx < NUM; ++idx)
					cmp[idx] = v.cmp[idx];
			}
			// assignment operations
			inline const vectorn<T,NUM> operator = (const vectorn<T,NUM>& v) {
				for(int idx = 0; idx < NUM; ++idx)
					cmp[idx] = v.cmp[idx];
				return *this;
			}
			inline const vectorn<T,NUM> operator+=(const vectorn<T,NUM>& v) {
				for(int idx = 0; idx < NUM; ++idx)
					cmp[idx] += v.cmp[idx];
				return *this;
			}
			inline const vectorn<T,NUM> operator-=(const vectorn<T,NUM>& v) {
				for(int idx = 0; idx < NUM; ++idx)
					cmp[idx] -= v.cmp[idx];
				return *this;
			}
			inline const vectorn<T,NUM> operator*=(const T& num) {
				for(int idx = 0; idx < NUM; ++idx)
					cmp[idx] *= num;
				return *this;
			}
			inline const vectorn<T,NUM> operator/=(const T& num) {
				const T r = static_cast<T>(1.0)/num;
				for(int idx = 0; idx < NUM; ++idx)
					cmp[idx] *= r;
				return *this;
			}
			// stream operations
			friend std::ostream& operator<<(std::ostream& out, const vectorn<T,NUM>& v) {
				out << "[";
				for(int idx = 0; idx < NUM; ++idx) {
					out << v.cmp[idx];
					if(idx + 1 <  NUM)
						out << ",";
				}
				out << "]";
				return out;
			}
			// comparison operations
			inline bool operator == (const vectorn<T,NUM>& v) const {
				bool res = true;
				for(int idx = 0; idx < NUM; ++idx)
					res &= v.cmp[idx] == cmp[idx];
				return res;
			}
			inline bool operator != (const vectorn<T,NUM>& v) const {
				return !(v == *this);
			}
			// unary operations
			inline const vectorn<T,NUM> operator - () const {
				vectorn<T,NUM> v;
				for(int idx = 0; idx < NUM; ++idx)
					v.cmp[idx] = -cmp[idx];
				return v;
			}
			// binary operations
			inline friend const vectorn<T,NUM> operator+(const vectorn<T,NUM>& v1,const vectorn<T,NUM>& v2) {
				vectorn<T,NUM> res;
				for(int idx = 0; idx < NUM; ++idx)
					res[idx] = v1.cmp[idx] + v2.cmp[idx];
				return res;
			}
			inline friend const vectorn<T,NUM> operator-(const vectorn<T,NUM>& v1, const vectorn<T,NUM>& v2) {
				vectorn<T,NUM> res;
				for(int idx = 0; idx < NUM; ++idx)
					res[idx] = v1.cmp[idx] - v2.cmp[idx];
				return res;
			}
			inline friend const T	operator*(const vectorn<T,NUM>& v1,const vectorn<T,NUM>& v2) {
				T res = static_cast<T>(0.0);
				for(int idx = 0; idx < NUM; ++idx)
					res += v1.cmp[idx] * v2.cmp[idx];
				return res;
			}
			inline const vectorn<T,NUM> operator*(const T& num) const {
				vectorn<T,NUM> v;
				for(int idx = 0; idx < NUM; ++idx)
					v.cmp[idx] = cmp[idx]*num;
				return v;
			}
			friend inline const vectorn<T,NUM> operator * (const T& s, const vectorn<T,NUM>& v) {
				return v * s;
			}
			inline const vectorn<T,NUM> operator/(const T& num) const {
				vectorn<T,NUM> v;
				const T r = static_cast<T>(1.0)/num;
				for(int idx = 0; idx < NUM; ++idx)
					v.cmp[idx] = cmp[idx]*r;
				return v;
			}
			size_t size() const {
				return NUM;
			}
			// cast operations
			operator T*() {
				return cmp;
			}
			operator const T*() const {
				return cmp;
			}
	};

}

#endif
