/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef VECTOR4_H
#define VECTOR4_H

namespace tmath {

	/**
		@class vectorn<T,4>
		@brief Class that represents a 4-dimensional vector.
		@author Cengiz Terzibas
	*/
	template<typename T>
	class vectorn<T,4> {
		public:

			typedef T value_type;

			T x, y, z, w;

			vectorn<T,4>()
				: x(static_cast<T>(0.0))
				, y(static_cast<T>(0.0))
				, z(static_cast<T>(0.0))
				, w(static_cast<T>(0.0)) {
			}

			vectorn<T,4>(T vx, T vy, T vz, T vw) : x(vx), y(vy), z(vz), w(vw) {
			}

			vectorn<T,4>(const T* v) : x(v[0]), y(v[1]), z(v[2]), w(v[3]) {
			}

			vectorn<T,4>(const vectorn<T,4>& v) : x(v.x), y(v.y), z(v.z), w(v.w) {
			}

			vectorn<T,4>(const vectorn<T,2>& v, T vz = 0.0, T vw = 0.0) : x(v.x), y(v.y), z(vz), w(vw) {
			}

			vectorn<T,4>(const vectorn<T,3>& v, T vw = 0.0) : x(v.x), y(v.y), z(v.z), w(vw) {
			}

			inline friend std::ostream& operator<<(std::ostream& out, const vectorn<T,4>& v) {
				out << "[" << v.x << "," << v.y << "," << v.z << "," << v.w << "]";
				return out;
			}

			inline const vectorn<T,4>& operator+=(const vectorn<T,4>& v) {
				x += v.x; y += v.y; z += v.z; w += v.w;
				return *this;
			}

			inline const vectorn<T,4>& operator-=(const vectorn<T,4>& v) {
				x -= v.x; y -= v.y; z -= v.z; w -= v.w;
				return *this;
			}

			inline const vectorn<T,4>& operator*=(const T& num) {
				x *= num; y *= num; z *= num; w *= num;
				return *this;
			}

			inline const vectorn<T,4>& operator/=(const T& num) {
				const T r = static_cast<T>(1.0)/num;
				x *= r; y *= r; z *=r; w *= r;
				return *this;
			}

			inline bool operator == (const vectorn<T,4>& v) const {
				return (v.x==x && v.y==y && v.z==z && v.w==w);
			}

			inline bool operator != (const vectorn<T,4>& v) const {
				return !(v == *this);
			}

			inline const vectorn<T,4> operator - () const {
				return vectorn<T,4>(-x, -y, -z, -w);
			}

			inline friend const vectorn<T,4> operator+(const vectorn<T,4>& v1, const vectorn<T,4>& v2)  {
				return vectorn(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z,v1.w + v2.w);
			}

			inline friend const vectorn<T,4> operator-(const vectorn<T,4>& v1, const vectorn<T,4>& v2) {
				return vectorn(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
			}

			inline friend const T	operator*(const vectorn<T,4>& v1,const vectorn<T,4>& v2) {
				return T(v1.x * v2.x + v1.y *v2.y + v1.z * v2.z + v1.w * v2.w);
			}

			inline const vectorn<T,4> operator*(const T& num) const {
				return vectorn(x * num, y * num, z * num, w * num);
			}

			friend inline const vectorn<T,4> operator * (const T& s, const vectorn<T,4>& v) {
				return v * s;
			}

			inline const vectorn<T,4> operator/(const T& num) const {
				const T r = (T)1.0/num;
				return vectorn(x * r, y * r, z * r, w * r);
			}

			inline size_t size() const {
				return 4;
			}

			operator T*() {
				return &x;
			}

			operator const T*() const {
				return &x;
			}
	};

	/// Calculate the normal of a vectorn
	template<typename T>
	T norm(const vectorn<T,4>& v) {
		return static_cast<T>(v.x*v.x + v.y*v.y + v.z*v.z + v.w*v.w);
	}

	/// Normalize a vectorn
	template<typename T>
	vectorn<T,4>& normalize(vectorn<T,4>& v) {
		T value = static_cast<T>(1.0)/sqrt(v.x*v.x + v.y*v.y + v.z*v.z + v.w*v.w);
		v.x *= value;
		v.y *= value;
		v.z *= value;
		v.w *= value;
		return v;
	}

	/// Calculate the length of a vectorn
	template<typename T>
	T length(const vectorn<T,4>& v) {
		return static_cast<T>(sqrt(v.x*v.x + v.y*v.y + v.z*v.z + v.w*v.w));
	}

	/// Convert vectorn to vectorn<T,3>
	template<typename T>
	vectorn<T,3>& convert(const vectorn<T,4>& v1, vectorn<T,3>& v2) {
		v2.x = v1.x;
		v2.y = v1.y;
		v2.z = v1.z;
		return v2;
	}

	template<typename T>
	std::ifstream& operator >> (std::ifstream& in, vectorn<T,4> &v) {
		in >> v.x >> v.y >> v.z >> v.w;
		return in;
	}
}

#endif
