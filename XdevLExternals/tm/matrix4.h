/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef MATRIX4_H
#define MATRIX4_H

#include <string.h>

namespace tmath {

	/**
		@class matrix<T,4,4>
		@brief Class that represents a 4 x 4 matrix.
		@author Cengiz Terzibas
	*/
	template<typename T>
	class matrix<T,4,4> {
		public:

			typedef T value_type;

			union { T xx, a0; };
			union { T xy, a1; };
			union { T xz, a2; };
			union { T xw, a3; };

			union { T yx, a4; };
			union { T yy, a5; };
			union { T yz, a6; };
			union { T yw, a7; };

			union { T zx, a8; };
			union { T zy, a9; };
			union { T zz, a10; };
			union { T zw, a11; };

			union { T wx, a12; };
			union { T wy, a13; };
			union { T wz, a14; };
			union { T ww, a15; };

			// constructors
			matrix<T,4,4>() {
				memset(&xx, 0, sizeof(T)*16);
			}

			matrix<T,4,4>(const T  _a0, const T  _a1, const T  _a2, const T  _a3,
			              const T  _a4, const T  _a5, const T  _a6, const T  _a7,
			              const T  _a8, const T  _a9, const T _a10, const T _a11,
			              const T _a12, const T _a13, const T _a14, const T _a15) :
				xx(_a0), xy(_a1), xz(_a2), xw(_a3),
				yx(_a4), yy(_a5), yz(_a6), yw(_a7),
				zx(_a8), zy(_a9), zz(_a10), zw(_a11),
				wx(_a12), wy(_a13), wz(_a14), ww(_a15) {}

			matrix<T,4,4>(const vectorn<T, 4> vx,
			              const vectorn<T, 4> vy,
			              const vectorn<T, 4> vz,
			              const vectorn<T, 4> vw) :
				xx(vx.x), xy(vx.y), xz(vx.z), xw(vx.w),
				yx(vy.x), yy(vy.y), yz(vy.z), yw(vy.w),
				zx(vz.x), zy(vz.y), zz(vz.z), zw(vz.w),
				wx(vw.x), wy(vw.y), wz(vw.z), ww(vw.w) {}

			// assignment operations
			inline matrix<T,4,4>& operator+=(const matrix<T,4,4>& m) {
				xx += m.xx; xy += m.xy; xz += m.xz; xw += m.xw;
				yx += m.yx; yy += m.yy; yz += m.yz; yw += m.yw;
				zx += m.zx; zy += m.zy; zz += m.zz; zw += m.zw;
				wx += m.wx; wy += m.wy; wz += m.wz; ww += m.ww;
				return *this;
			}

			inline matrix<T,4,4>& operator-=(const matrix<T,4,4>& m) {
				xx -= m.xx; xy -= m.xy; xz -= m.xz; xw -= m.xw;
				yx -= m.yx; yy -= m.yy; yz -= m.yz; yw -= m.yw;
				zx -= m.zx; zy -= m.zy; zz -= m.zz; zw -= m.zw;
				wx -= m.wx; wy -= m.wy; wz -= m.wz; ww -= m.ww;
				return *this;
			}

			inline matrix<T,4,4>& operator*=(const T& num) {
				xx *= num; xy *= num; xz *= num; xw *= num;
				yx *= num; yy *= num; yz *= num; yw *= num;
				zx *= num; zy *= num; zz *= num; zw *= num;
				wx *= num; wy *= num; wz *= num; ww *= num;
				return *this;
			}

			inline matrix<T,4,4>& operator/=(const T& num) {
				T tmp = T(1.0)/num;
				xx *= tmp; xy *= tmp; xz *= tmp; xw *= tmp;
				yx *= tmp; yy *= tmp; yz *= tmp; yw *= tmp;
				zx *= tmp; zy *= tmp; zz *= tmp; zw *= tmp;
				wx *= tmp; wy *= tmp; wz *= tmp; ww *= tmp;
				return *this;
			}

			// stream operations
			friend std::ostream& operator<<(std::ostream& out, const matrix<T,4,4>& m) {
				out << m.xx << " " << m.xy << " " << m.xz << " " << m.xw << std::endl;
				out << m.yx << " " << m.yy << " " << m.yz << " " << m.yw << std::endl;
				out << m.zx << " " << m.zy << " " << m.zz << " " << m.zw << std::endl;
				out << m.wx << " " << m.wy << " " << m.wz << " " << m.ww << std::endl;
				return out;
			}

			// comparison operations
			inline bool operator == (const matrix<T,4,4>& m) const {
				return (xx == m.xx && xy == m.xy && xz == m.xz && xw == m.xw &&
				        yx == m.yx && yy == m.yy && yz == m.yz && yw == m.yw &&
				        zx == m.zx && zy == m.zy && zz == m.zz && zw == m.zw &&
				        wx == m.wx && wy == m.wy && wz == m.wz && ww == m.ww);
			}

			inline bool operator != (const matrix<T,4,4>& m) const {
				return !(m == *this);
			}

			// unary operations
			inline const matrix<T,4,4> operator - () const {
				return matrix<T,4,4>(-xx, -xy, -xz, -xw,
				                     -yx, -yy, -yz, -yw,
				                     -zx, -zy, -zz, -zw,
				                     -wx, -wy, -wz, -ww);
			}

			// binary operations
			inline matrix<T,4,4> operator+(const matrix<T,4,4>& m) {
				return matrix<T,4,4>(xx + m.xx, xy + m.xy, xz + m.xz, xw + m.xw,
				                     yx + m.yx, yy + m.yy, yz + m.yz, yw + m.yw,
				                     zx + m.zx, zy + m.zy, zz + m.zz, zw + m.zw,
				                     wx + m.wx, wy + m.wy, wz + m.wz, ww + m.ww);
			}

			inline matrix<T,4,4> operator-(const matrix<T,4,4>& m) {
				return matrix<T,4,4>(xx - m.xx, xy - m.xy, xz - m.xz, xw - m.xw,
				                     yx - m.yx, yy - m.yy, yz - m.yz, yw - m.yw,
				                     zx - m.zx, zy - m.zy, zz - m.zz, zw - m.zw,
				                     wx - m.wx, wy - m.wy, wz - m.wz, ww - m.ww);
			}

			inline const matrix<T,4,4> operator*(const T& num) const {
				return matrix<T,4,4>(xx * num, xy * num, xz * num, xw * num,
				                     yx * num, yy * num, yz * num, yw * num,
				                     zx * num, zy * num, zz * num, zw * num,
				                     wx * num, wy * num, wz * num, ww * num);
			}

			friend inline const matrix<T,4,4> operator*(const T &s, const matrix<T,4,4> &m) {
				return m * s;
			}

			// Column-Row Multiplication.
			inline matrix<T,4,4> operator*(const matrix<T,4,4>& m) {
				return matrix<T,4,4>(xx * m.xx + yx * m.xy + zx * m.xz + wx * m.xw,
				                     xy * m.xx + yy * m.xy + zy * m.xz + wy * m.xw,
				                     xz * m.xx + yz * m.xy + zz * m.xz + wz * m.xw,
				                     xw * m.xx + yw * m.xy + zw * m.xz + ww * m.xw,

				                     xx * m.yx + yx * m.yy + xz * m.yz + wx * m.yw,
				                     xy * m.yx + yy * m.yy + zy * m.yz + wy * m.yw,
				                     xz * m.yx + yz * m.yy + zz * m.yz + wz * m.yw,
				                     xw * m.yx + yw * m.yy + zw * m.yz + ww * m.yw,

				                     xx * m.zx + yx * m.zy + zx * m.zz + wx * m.zw,
				                     xy * m.zx + yy * m.zy + zy * m.zz + wy * m.zw,
				                     xz * m.zx + yz * m.zy + zz * m.zz + wz * m.zw,
				                     xw * m.zx + yw * m.zy + zw * m.zz + ww * m.zw,

				                     xx * m.wx + yx * m.wy + zx * m.wz + wx * m.ww,
				                     xy * m.wx + yy * m.wy + zy * m.wz + wy * m.ww,
				                     xz * m.wx + yz * m.wy + zz * m.wz + wz * m.ww,
				                     xw * m.wx + yw * m.wy + zw * m.wz + ww * m.ww);

			}

			inline const vectorn<T,4> operator*(const vectorn<T,4>& v) const {
				return vectorn<T,4>(v.x*xx + v.y*yx + v.z*zx + v.w*wx,
				                    v.x*xy + v.y*yy + v.z*zy + v.w*wy,
				                    v.x*xz + v.y*yz + v.z*zz + v.w*wz,
				                    v.x*xw + v.y*yw + v.z*zw + v.w*ww);

			}

			inline const matrix<T,4,4> operator/(const T& num) const {
				T val = T(1.0)/num;
				return matrix<T,4,4>(xx * val, xy * val, xz * val, xw * val,
				                     yx * val, yy * val, yz * val, yw * val,
				                     zx * val, zy * val, zz * val, zw * val,
				                     wx * val, wy * val, wz * val, ww * val);
			}

			// indexing operations
			void row(unsigned int pRow, const vectorn<T,4>& pV) {
				unsigned idx = 4*pRow;
				(*this)[idx]			= pV.x;
				(*this)[idx + 1]	= pV.y;
				(*this)[idx + 2]	= pV.z;
				(*this)[idx + 3]	= pV.w;
			}

			vectorn<T,4> row(unsigned int pRow) {
				unsigned idx = 4*pRow;
				return vectorn<T,4>((*this)[idx],(*this)[idx+1],(*this)[idx+2],(*this)[idx+3]);
			}

			void column(unsigned int pColumn, const vectorn<T,4>& pV) {
				(*this)[pColumn]	= pV.x;
				(*this)[pColumn + 4]	= pV.y;
				(*this)[pColumn + 8]	= pV.z;
				(*this)[pColumn + 12]	= pV.w;
			}

			vectorn<T,4> column(unsigned int pColumn) {
				return vectorn<T,4>((*this)[pColumn],(*this)[pColumn+4],(*this)[pColumn+8],(*this)[pColumn+12]);
			}

			inline const vectorn<T,4> row(int idx) {
				return vectorn<T,4>(*(&xx + 4*idx),*(&xx + 4*idx +1),*(&xx + 4*idx +2),*(&xx + 4*idx +3));
			}

			inline const vectorn<T,4> column(int idx) {
				return vectorn<T,4>(*(&xx + idx),*(&xx + idx+ 4),*(&xx + idx + 8),*(&xx + idx + 12));
			}

			static matrix<T,4,4> identity() {
				matrix<T,4,4> mat;
				mat.xx = T(1.0); mat.xy 	= T(0.0); mat.xz 	= T(0.0); mat.xw 	= T(0.0);
				mat.yx = T(0.0); mat.yy 	= T(1.0); mat.yz 	= T(0.0); mat.yw 	= T(0.0);
				mat.zx = T(0.0); mat.zy 	= T(0.0); mat.zz 	= T(1.0); mat.zw 	= T(0.0);
				mat.wx = T(0.0); mat.wy 	= T(0.0); mat.wz 	= T(0.0); mat.ww 	= T(1.0);
				return mat;
			}

			// cast operations
			operator T*() {
				return &xx;
			}

			operator const T*() const {
				return &xx;
			}

			T& at(unsigned int row, unsigned int column) {
				return xx[row][column];
			}
	};

	/// Transpose a matrix.
	template<typename T>
	const matrix<T,4,4> transpose(const matrix<T,4,4>& m) {
		return matrix<T,4,4>(m.xx, m.yx, m.zx, m.wx,
		                     m.xy, m.yy, m.zy, m.wy,
		                     m.xz, m.yz, m.zz, m.wz,
		                     m.xw, m.yw, m.zw, m.ww);
	}

	/// Calculates the determinant of a matrix.
	template<typename T>
	T determinant(const matrix<T,4,4>& m) {
		T det;
		det = m[0] * m[5] * m[10];
		det += m[4] * m[9] * m[2];
		det += m[8] * m[1] * m[6];
		det -= m[8] * m[5] * m[2];
		det -= m[4] * m[1] * m[10];
		det -= m[0] * m[9] * m[6];
		return det;
	}

	/// Creates a translation matrix.
	template<typename T>
	inline matrix<T,4,4>& translate(const T x, const T y, const T z, matrix<T,4,4>& m) {
		m.xx = T(1.0); m.xy = T(0.0); m.xz = T(0.0); m.xw = T(0.0);
		m.yx = T(0.0); m.yy = T(1.0); m.yz = T(0.0); m.yw = T(0.0);
		m.zx = T(0.0); m.zy = T(0.0); m.zz = T(1.0); m.zw = T(0.0);
		m.wx = x;      m.wy = y;      m.wz = z;      m.ww = T(1.0);
		return m;
	}

	/// Creates a translation matrix.
	template<typename T>
	inline matrix<T,4,4>& translate(const vectorn<T,3>& v, matrix<T,4,4>& m) {
		return translate(v.x, v.y, v.z, m);
	}

	/// Creates a scale matrix.
	template<typename T>
	inline matrix<T,4,4>& scale(const T sx, const T sy, const T sz, matrix<T,4,4>& m) {
		m.xx = sx;   m.xy = T(0.0); m.xz = T(0.0); m.xw = T(0.0);
		m.yx = T(0.0); m.yy = sy;   m.yz = T(0.0); m.yw = T(0.0);
		m.zx = T(0.0); m.zy = T(0.0); m.zz = sz;   m.zw = T(0.0);
		m.wx = T(0.0); m.wy = T(0.0); m.wz = T(0.0); m.ww = T(1.0);
		return m;
	}

	/// Creates a scale matrix.
	template<typename T>
	inline matrix<T,4,4>& scale(const vectorn<T,3>& v, matrix<T,4,4>& m) {
		return scale(v.x, v.y, v.z, m);
	}

	/// Creates a matrix<T,4,4> that represents a rotation around the x axis.
	template<typename T>
	matrix<T,4,4>& rotate_x(const T& angle, matrix<T,4,4>& m) {
		T rad = d2r(angle);
		T c = cos(rad);
		T s = sin(rad);
		m[5] 	=  c;  m[6] = s;
		m[9] 	= -s;  m[10] = c;
		m[0] 	= T(1.0); m[15] = T(1.0);
		return m;
	}

	/// Creates a matrix<T,4,4> that represents a rotation around the y axis.
	template<typename T>
	matrix<T,4,4>& rotate_y(const T& angle, matrix<T,4,4>& m) {
		T rad = d2r(angle);
		T c = cos(rad);
		T s = sin(rad);
		m[0] = c;   m[2] = -s;
		m[8] = s;   m[10] = c;
		m[5] = T(1.0); m[15] = T(1.0);
		return m;
	}

	/// Creates a matrix<T,4,4> that represents a rotation around the z axis.
	template<typename T>
	matrix<T,4,4>& rotate_z(const T& angle, matrix<T,4,4>& m) {
		T rad = d2r(angle);
		T c = cos(rad);
		T s = sin(rad);
		m[0] =  c; m[1] = s;
		m[4] = -s; m[5] = c;
		m[10] = T(1.0); m[15] = T(1.0);
		return m;
	}

	/// Creates a perspective frustum matrix using left, right, bottom, top near and far.
	template<typename T>
	matrix<T,4,4>& frustum(T left_plane, T right_plane, T bottom_plane, T top_plane, T near_plane, T far_plane, matrix<T,4,4>& mat) {
		T d = (right_plane-left_plane);
		T c = (top_plane-bottom_plane);
		mat[0] = 2.0f*near_plane/d; mat[1] = T(0.0); mat[2] = T(0.0); mat[3] = T(0.0);
		mat[4] = T(0.0); mat[5] = 2.0f*near_plane/c; mat[6] = T(0.0); mat[7] = T(0.0);
		mat[8] =(right_plane+left_plane)/d; mat[9] = (top_plane+bottom_plane)/c; mat[10] = -(far_plane+near_plane)/(far_plane-near_plane); mat[11] = -T(1.0);
		mat[12] = T(0.0); mat[13] = T(0.0); mat[14] = -2.0f*far_plane*near_plane/(far_plane-near_plane); mat[15] = T(0.0);
		return mat;
	}

	/// Creates ortho matrix using left, right, bottom, top, near and far.
	template<typename T>
	matrix<T, 4, 4>& ortho(T left, T right, T bottom, T top, T near_plane_distance, T far_plane_distance, matrix<T, 4, 4>& mat) {
		T d = (right-left);
		T c = (top-bottom);
		mat[0] = 2.0f/d; mat[1] = T(0.0); mat[2] = T(0.0); mat[3] = T(0.0);
		mat[4] = T(0.0); mat[5] = 2.0f/c; mat[6] = T(0.0); mat[7] = T(0.0);
		mat[8] = T(0.0); mat[9] = T(0.0); mat[10] = -2.0f / (far_plane_distance - near_plane_distance); mat[11] = T(0.0);
		mat[12] = -(right + left) / d; mat[13] = -(top + bottom) / c; mat[14] = -(far_plane_distance + near_plane_distance) / (far_plane_distance - near_plane_distance); mat[15] = T(1.0);
		return mat;
	}

	/// Creates a perspective matrix using fov, aspect, near and far.
	template<typename T>
	matrix<T,4,4>& perspective_fov(T fov, T aspect, T near_plane_distance, T far_plane_distance, matrix<T,4,4>& mat) {
		T f = static_cast<T>(1.0)/tan(d2r(fov* static_cast<T>(0.5)));
		T d = (near_plane_distance - far_plane_distance);
		mat[0]  = f/aspect; mat[1]  = 0.0; mat[2]  = 0.0;           mat[3] = 0.0;
		mat[4]  = 0.0;      mat[5]  = f;    mat[6]  = 0.0;           mat[7] = 0.0;
		mat[8] = 0.0;       mat[9]  = 0.0; mat[10] = (far_plane_distance + near_plane_distance) / d; mat[11] = (2.0*far_plane_distance * near_plane_distance) / d;
		mat[12] = 0.0;      mat[13] = 0.0;  mat[14] = -1.0;           mat[15] = 0.0;
		return mat;
	}

	/// Creates a perspective matrix using fov, aspect, near and far.
	template<typename T>
	matrix<T,4,4>& perspective(T fovy, T aspect, T near_plane_distance, T far_plane_distance, matrix<T,4,4>& mat) {

		T range = tan(d2r(fovy / static_cast<T>(2.0))) * near_plane_distance;
		T left = -range * aspect;
		T right = range * aspect;
		T bottom = -range;
		T top = range;

		mat.xx = (static_cast<T>(2.0) * near_plane_distance) / (right - left);
		mat.yy = (static_cast<T>(2.0) * near_plane_distance) / (top - bottom);
		mat.zz = - (far_plane_distance + near_plane_distance) / (far_plane_distance - near_plane_distance);
		mat.zw = - static_cast<T>(1.0);
		mat.wz = - (static_cast<T>(2.0) * far_plane_distance * near_plane_distance) / (far_plane_distance - near_plane_distance);

		mat.xy = static_cast<T>(0.0);
		mat.xz = static_cast<T>(0.0);
		mat.xw = static_cast<T>(0.0);
		mat.yx = static_cast<T>(0.0);
		mat.yz = static_cast<T>(0.0);
		mat.yw = static_cast<T>(0.0);
		mat.zx = static_cast<T>(0.0);
		mat.zy = static_cast<T>(0.0);
		mat.wx = static_cast<T>(0.0);
		mat.wy = static_cast<T>(0.0);
		mat.ww = static_cast<T>(0.0);

		return mat;
	}

	/// Creates a rotation matrix around a free axis.
	template<typename T>
	matrix<T,4,4>& rotate(const T& angle, const vectorn<T,3>& axis, matrix<T,4,4>& mat) {
		T rad = d2r(angle);
		T c = static_cast<T>(cos(rad));
		T s = static_cast<T>(sin(rad));
		vectorn<T,3> v = axis;
		normalize(v);
		T xy = v.x * v.y;
		T yz = v.y * v.z;
		T zx = v.z * v.x;
		T xs = v.x * s;
		T ys = v.y * s;
		T zs = v.z * s;
		T tt = static_cast<T>(1.0) - c;

		mat[0]  = tt * v.x * v.x + c; mat[1]  = tt * xy + zs;       mat[2]  = tt * zx - ys;        mat[3]  = T(0.0);
		mat[4]  = tt * xy - zs;       mat[5]  = tt * v.y * v.y + c; mat[6]  = tt * yz + xs;        mat[7]  = T(0.0);
		mat[8]  = tt * zx + ys;       mat[9]  = tt * yz - xs;       mat[10] = tt * v.z * v.z + c;  mat[11] = T(0.0);
		mat[12] = T(0.0);             mat[13] = T(0.0);             mat[14] = T(0.0);              mat[15] = T(1.0);
		return mat;
	}


	/// Makes a zero matrix<T,4,4>
	template<typename T>
	matrix<T,4,4>& zero(matrix<T,4,4>& mat) {
		memset(mat, 0, sizeof(matrix<T,4,4>));
		return mat;
	}

	/// Make a identity matrix<T,4,4>
	template<typename T>
	matrix<T,4,4>& identity(matrix<T,4,4>& mat) {
		mat[0] 	= T(1.0); mat[1] 	= T(0.0); mat[2] 	= T(0.0); mat[3] 	= T(0.0);
		mat[4] 	= T(0.0); mat[5] 	= T(1.0); mat[6] 	= T(0.0); mat[7] 	= T(0.0);
		mat[8] 	= T(0.0); mat[9] 	= T(0.0); mat[10] = T(1.0); mat[11] = T(0.0);
		mat[12] = T(0.0); mat[13] = T(0.0); mat[14] = T(0.0); mat[15] = T(1.0);
		return mat;
	}

	/// Converts matrix into a quaterion.
	template<typename T>
	quaternion<T>& convert(const matrix<T,4,4>& mat, quaternion<T>& quat) {
		quat.w = (mat[0] + mat[5] + mat[10] + T(1.0)) / T(4.0);
		quat.x = (mat[0] - mat[5] - mat[10] + T(1.0)) / T(4.0);
		quat.y = (-mat[0] + mat[5] - mat[10] + T(1.0)) / T(4.0);
		quat.z = (-mat[0] - mat[5] + mat[10] + T(1.0)) / T(4.0);
		if(quat.w < T(0.0)) quat.w = T(0.0);
		if(quat.x < T(0.0)) quat.x = T(0.0);
		if(quat.y < T(0.0)) quat.y = T(0.0);
		if(quat.z < T(0.0)) quat.z = T(0.0);
		quat.w = sqrt(quat.w);
		quat.x = sqrt(quat.x);
		quat.y = sqrt(quat.y);
		quat.z = sqrt(quat.z);
		if(quat.w >= quat.x && quat.w >= quat.y && quat.w >= quat.z) {
			quat.w *= +T(1.0);
			quat.x *= sign(mat[6] - mat[9]);
			quat.y *= sign(mat[8] - mat[2]);
			quat.z *= sign(mat[1] - mat[4]);
		} else if(quat.x >= quat.w && quat.x >= quat.y && quat.x >= quat.z) {
			quat.w *= sign(mat[6] - mat[9]);
			quat.x *= +T(1.0);
			quat.y *= sign(mat[1] + mat[4]);
			quat.z *= sign(mat[8] + mat[2]);
		} else if(quat.y >= quat.w && quat.y >= quat.x && quat.y >= quat.z) {
			quat.w *= sign(mat[8] - mat[2]);
			quat.x *= sign(mat[1] + mat[4]);
			quat.y *= +T(1.0);
			quat.z *= sign(mat[6] + mat[9]);
		} else if(quat.z >= quat.w && quat.z >= quat.x && quat.z >= quat.y) {
			quat.w *= sign(mat[1] - mat[4]);
			quat.x *= sign(mat[2] + mat[8]);
			quat.y *= sign(mat[6] + mat[9]);
			quat.z *= +T(1.0);
		} else {
			// Some problems..
		}
		normalize(quat);
		return quat;
	}

	/// Calculates the inverse of a matrix<T,4,4>
	template<typename T>
	matrix<T,4,4> inverse(const matrix<T,4,4>& m) {
		tmath::matrix<T,4,4> mat;
		T tmp[12];
		matrix<T,4,4> src;

		// Transpose matrix.
		src = transpose(m);

		tmp[0] = src.zz * src.ww;
		tmp[1] = src.zw * src.wz;
		tmp[2] = src.zy * src.ww;
		tmp[3] = src.zw * src.wy;
		tmp[4] = src.zy * src.wz;
		tmp[5] = src.zz * src.wy;
		tmp[6] = src.zx * src.ww;
		tmp[7] = src.zw * src.wx;
		tmp[8] = src.zz * src.wz;
		tmp[9] = src.zz * src.wx;
		tmp[10] = src.zx * src.wy;
		tmp[11] = src.zy * src.wx;

		// calculate first 8 elements (cofactors)
		mat.xx = tmp[0]*src.yy + tmp[3]*src.yz + tmp[4]*src.yw;
		mat.xx -= tmp[1]*src.yy + tmp[2]*src.yz + tmp[5]*src.yw;
		mat.xy = tmp[1]*src.yx + tmp[6]*src.yz + tmp[9]*src.yw;
		mat.xy -= tmp[0]*src.yx + tmp[7]*src.yz + tmp[8]*src.yw;
		mat.xz = tmp[2]*src.yx + tmp[7]*src.yy + tmp[10]*src.yw;
		mat.xz -= tmp[3]*src.yx + tmp[6]*src.yy + tmp[11]*src.yw;
		mat.xw = tmp[5]*src.yx + tmp[8]*src.yy + tmp[11]*src.yz;
		mat.xw -= tmp[4]*src.yx + tmp[9]*src.yy + tmp[10]*src.yz;
		mat.yx = tmp[1]*src.xy + tmp[2]*src.xz + tmp[5]*src.xw;
		mat.yx -= tmp[0]*src.xy + tmp[3]*src.xz + tmp[4]*src.xw;
		mat.yy = tmp[0]*src[0] + tmp[7]*src.xz + tmp[8]*src.xw;
		mat.yy -= tmp[1]*src[0] + tmp[6]*src.xz + tmp[9]*src.xw;
		mat.yz = tmp[3]*src[0] + tmp[6]*src.xy + tmp[11]*src.xw;
		mat.yz -= tmp[2]*src[0] + tmp[7]*src.xy + tmp[10]*src.xw;
		mat.yw = tmp[4]*src[0] + tmp[9]*src.xy + tmp[10]*src.xz;
		mat.yw -= tmp[5]*src[0] + tmp[8]*src.xy + tmp[11]*src.xz;

		tmp[0] = src.xz * src.yw;
		tmp[1] = src.xw * src.yz;
		tmp[2] = src.xy * src.yw;
		tmp[3] = src.xw * src.yy;
		tmp[4] = src.xy * src.yz;
		tmp[5] = src.xz * src.yy;
		tmp[6] = src.xx * src.yw;
		tmp[7] = src.xw * src.yx;
		tmp[8] = src.xx * src.yz;
		tmp[9] = src.xz * src.yx;
		tmp[10] = src.xx * src.yy;
		tmp[11] = src.xy * src.yx;

		mat.zx = tmp[0]*src.wy + tmp[3]*src.wz + tmp[4]*src.ww;
		mat.zx -= tmp[1]*src.wy + tmp[2]*src.wz + tmp[5]*src.ww;
		mat.zy = tmp[1]*src.wx + tmp[6]*src.wz + tmp[9]*src.ww;
		mat.zy -= tmp[0]*src.wx + tmp[7]*src.wz + tmp[8]*src.ww;
		mat.zz = tmp[2]*src.wx + tmp[7]*src.wy + tmp[10]*src.ww;
		mat.zz-= tmp[3]*src.wx + tmp[6]*src.wy + tmp[11]*src.ww;
		mat.zw = tmp[5]*src.wx + tmp[8]*src.wy + tmp[11]*src.wz;
		mat.zw-= tmp[4]*src.wx + tmp[9]*src.wy + tmp[10]*src.wz;
		mat.wx = tmp[2]*src.zz + tmp[5]*src.zw + tmp[1]*src.zy;
		mat.wx-= tmp[4]*src.zw + tmp[0]*src.zy + tmp[3]*src.zz;
		mat.wy = tmp[8]*src.zw + tmp[0]*src.zx + tmp[7]*src.zz;
		mat.wy-= tmp[6]*src.zz + tmp[9]*src.zw + tmp[1]*src.zx;
		mat.wz = tmp[6]*src.zy + tmp[11]*src.zw + tmp[3]*src.zx;
		mat.wz-= tmp[10]*src.zw + tmp[2]*src.zx + tmp[7]*src.zy;
		mat.ww = tmp[10]*src.zz + tmp[4]*src.zx + tmp[9]*src.zy;
		mat.ww-= tmp[8]*src.zy + tmp[11]*src.zz + tmp[5]*src.zx;

		// calculate determinant
		T det = (src.xx * mat.xx) + (src.xy * mat.xy) + (src.xz * mat.xz) + (src.xw * mat.xw);
		if(fabs(det) < static_cast<T>(0.0005)) {
			return tmath::matrix<T,4,4>();
		}

		mat /= det;

		return mat;
	}

	/// Creates a look at matrix.
	template<typename T>
	matrix<T,4,4>& look_at(vectorn<T,3>& position, vectorn<T,3>& target, vectorn<T,3>& up, matrix<T,4,4>& m) {
		vectorn<T,3> forward, side;

		// Calculate the direction of the view.
		forward = target - position;
		tmath::normalize(forward);

		// Calculate the positive right coordinate.
		tmath::cross(forward, up, side);
		tmath::normalize(side);

		// Now we must recompute the up vector to adjust.
		// it to the new right vector.
		tmath::cross(side, forward, up);
		normalize(up);

		// Fill the right information into the matrix.
		m[0] = side.x; m[1] = side.y; m[2] = side.z; m[3] = 0.0;
		m[4] = up.x;		m[5] = up.y	;	 m[6] = up.z;		m[7] = 0.0;
		m[8] = forward.x; m[9] = forward.y; m[10] = forward.z; m[11] = 0.0;
		m[12] = position.x; m[13] = -position.y; m[14] = -position.z; m[15] = T(1.0);

		return m;
	}


	template<typename T>
	std::ifstream& operator>>(std::ifstream& in, matrix<T,4,4>& m) {
		in >> m.xx; in >> m.xy; in >> m.xz; in >> m.xw;
		in >> m.yx; in >> m.yy; in >> m.yz; in >> m.yw;
		in >> m.zx; in >> m.zy; in >> m.zz; in >> m.zw;
		in >> m.wx; in >> m.wy; in >> m.wz; in >> m.ww;
		return in;
	}

}

#endif
