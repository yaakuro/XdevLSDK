The latest version of Physics\+FS can be found at\+: {\tt https\+://icculus.\+org/physfs/}

Physics\+FS; a portable, flexible file i/o abstraction.

This A\+PI gives you access to a system file system in ways superior to the stdio or system i/o calls. The brief benefits\+:


\begin{DoxyItemize}
\item It\textquotesingle{}s portable.
\item It\textquotesingle{}s safe. No file access is permitted outside the specified dirs.
\item It\textquotesingle{}s flexible. Archives (.Z\+IP files) can be used transparently as directory structures.
\end{DoxyItemize}

With Physics\+FS, you have a single writing directory and multiple directories (the \char`\"{}search path\char`\"{}) for reading. You can think of this as a filesystem within a filesystem. If (on Windows) you were to set the writing directory to \char`\"{}\+C\+:\textbackslash{}\+My\+Game\textbackslash{}\+My\+Writing\+Directory\char`\"{}, then no P\+H\+Y\+S\+FS calls could touch anything above this directory, including the \char`\"{}\+C\+:\textbackslash{}\+My\+Game\char`\"{} and \char`\"{}\+C\+:\textbackslash{}\char`\"{} directories. This prevents an application\textquotesingle{}s internal scripting language from piddling over c\+:\textbackslash{}config.\+sys, for example. If you\textquotesingle{}d rather give P\+H\+Y\+S\+FS full access to the system\textquotesingle{}s R\+E\+AL file system, set the writing dir to \char`\"{}\+C\+:\textbackslash{}\char`\"{}, but that\textquotesingle{}s generally A Bad Thing for several reasons.

Drive letters are hidden in Physics\+FS once you set up your initial paths. The search path creates a single, hierarchical directory structure. Not only does this lend itself well to general abstraction with archives, it also gives better support to operating systems like Mac\+OS and Unix. Generally speaking, you shouldn\textquotesingle{}t ever hardcode a drive letter; not only does this hurt portability to non-\/\+Microsoft O\+Ses, but it limits your win32 users to a single drive, too. Use the Physics\+FS abstraction functions and allow user-\/defined configuration options, too. When opening a file, you specify it like it was on a Unix filesystem\+: if you want to write to \char`\"{}\+C\+:\textbackslash{}\+My\+Game\textbackslash{}\+My\+Config\+Files\textbackslash{}game.\+cfg\char`\"{}, then you might set the write dir to \char`\"{}\+C\+:\textbackslash{}\+My\+Game\char`\"{} and then open \char`\"{}\+My\+Config\+Files/game.\+cfg\char`\"{}. This gives an abstraction across all platforms. Specifying a file in this way is termed \char`\"{}platform-\/independent notation\char`\"{} in this documentation. Specifying a a filename in a form such as \char`\"{}\+C\+:\textbackslash{}mydir\textbackslash{}myfile\char`\"{} or \char`\"{}\+Mac\+O\+S hard drive\+:\+My Directory\+:\+My File\char`\"{} is termed \char`\"{}platform-\/dependent
 notation\char`\"{}. The only time you use platform-\/dependent notation is when setting up your write directory and search path; after that, all file access into those directories are done with platform-\/independent notation.

All files opened for writing are opened in relation to the write directory, which is the root of the writable filesystem. When opening a file for reading, Physics\+FS goes through the search path. This is N\+OT the same thing as the P\+A\+TH environment variable. An application using Physics\+FS specifies directories to be searched which may be actual directories, or archive files that contain files and subdirectories of their own. See the end of these docs for currently supported archive formats.

Once the search path is defined, you may open files for reading. If you\textquotesingle{}ve got the following search path defined (to use a win32 example again)\+:


\begin{DoxyItemize}
\item C\+:\textbackslash{}mygame
\item C\+:\textbackslash{}mygame\textbackslash{}myuserfiles
\item D\+:\textbackslash{}mygamescdromdatafiles
\item C\+:\textbackslash{}mygame\textbackslash{}installeddatafiles.\+zip
\end{DoxyItemize}

Then a call to P\+H\+Y\+S\+F\+S\+\_\+open\+Read(\char`\"{}textfiles/myfile.\+txt\char`\"{}) (note the directory separator, lack of drive letter, and lack of dir separator at the start of the string; this is platform-\/independent notation) will check for C\+:\textbackslash{}mygame\textbackslash{}textfiles\textbackslash{}myfile.\+txt, then C\+:\textbackslash{}mygame\textbackslash{}myuserfiles\textbackslash{}textfiles\textbackslash{}myfile.\+txt, then D\+:\textbackslash{}mygamescdromdatafiles\textbackslash{}textfiles\textbackslash{}myfile.\+txt, then, finally, for textfiles\textbackslash{}myfile.\+txt inside of C\+:\textbackslash{}mygame\textbackslash{}installeddatafiles.\+zip. Remember that most archive types and platform filesystems store their filenames in a case-\/sensitive manner, so you should be careful to specify it correctly.

Files opened through Physics\+FS may N\+OT contain \char`\"{}.\char`\"{} or \char`\"{}..\char`\"{} or \char`\"{}\+:\char`\"{} as dir elements. Not only are these meaningless on Mac\+OS Classic and/or Unix, they are a security hole. Also, symbolic links (which can be found in some archive types and directly in the filesystem on Unix platforms) are N\+OT followed until you call \doxyref{P\+H\+Y\+S\+F\+S\+\_\+permit\+Symbolic\+Links()}{p.}{physfs_8h_aad451d9b3f46f627a1be8caee2eef9b7}. That\textquotesingle{}s left to your own discretion, as following a symlink can allow for access outside the write dir and search paths. For portability, there is no mechanism for creating new symlinks in Physics\+FS.

The write dir is not included in the search path unless you specifically add it. While you C\+AN change the write dir as many times as you like, you should probably set it once and stick to it. Remember that your program will not have permission to write in every directory on Unix and NT systems.

All files are opened in binary mode; there is no endline conversion for textfiles. Other than that, Physics\+FS has some convenience functions for platform-\/independence. There is a function to tell you the current platform\textquotesingle{}s dir separator (\char`\"{}\textbackslash{}\textbackslash{}\char`\"{} on windows, \char`\"{}/\char`\"{} on Unix, \char`\"{}\+:\char`\"{} on Mac\+OS), which is needed only to set up your search/write paths. There is a function to tell you what C\+D-\/\+R\+OM drives contain accessible discs, and a function to recommend a good search path, etc.

A recommended order for the search path is the write dir, then the base dir, then the cdrom dir, then any archives discovered. Quake 3 does something like this, but moves the archives to the start of the search path. Build Engine games, like Duke Nukem 3D and Blood, place the archives last, and use the base dir for both searching and writing. There is a helper function (\doxyref{P\+H\+Y\+S\+F\+S\+\_\+set\+Sane\+Config()}{p.}{physfs_8h_afef8700f714b6800ff688372c540bfe2}) that puts together a basic configuration for you, based on a few parameters. Also see the comments on \doxyref{P\+H\+Y\+S\+F\+S\+\_\+get\+Base\+Dir()}{p.}{physfs_8h_a0034c327dfb381a1c95deb80878bcede}, and \doxyref{P\+H\+Y\+S\+F\+S\+\_\+get\+Pref\+Dir()}{p.}{physfs_8h_acd87392d234d070695303521bb8052a5} for info on what those are and how they can help you determine an optimal search path.

Physics\+FS 2.\+0 adds the concept of \char`\"{}mounting\char`\"{} archives to arbitrary points in the search path. If a zipfile contains \char`\"{}maps/level.\+map\char`\"{} and you mount that archive at \char`\"{}mods/mymod\char`\"{}, then you would have to open \char`\"{}mods/mymod/maps/level.\+map\char`\"{} to access the file, even though \char`\"{}mods/mymod\char`\"{} isn\textquotesingle{}t actually specified in the .zip file. Unlike the Unix mentality of mounting a filesystem, \char`\"{}mods/mymod\char`\"{} doesn\textquotesingle{}t actually have to exist when mounting the zipfile. It\textquotesingle{}s a \char`\"{}virtual\char`\"{} directory. The mounting mechanism allows the developer to seperate archives in the tree and avoid trampling over files when added new archives, such as including mod support in a game...keeping external content on a tight leash in this manner can be of utmost importance to some applications.

Physics\+FS is mostly thread safe. The error messages returned by \doxyref{P\+H\+Y\+S\+F\+S\+\_\+get\+Last\+Error()}{p.}{physfs_8h_a9f78e4ab560885fd10202988d2fff1b6} are unique by thread, and library-\/state-\/setting functions are mutex\textquotesingle{}d. For efficiency, individual file accesses are not locked, so you can not safely read/write/seek/close/etc the same file from two threads at the same time. Other race conditions are bugs that should be reported/patched.

While you C\+AN use stdio/syscall file access in a program that has P\+H\+Y\+S\+F\+S\+\_\+$\ast$ calls, doing so is not recommended, and you can not use system filehandles with Physics\+FS and vice versa.

Note that archives need not be named as such\+: if you have a Z\+IP file and rename it with a .P\+KG extension, the file will still be recognized as a Z\+IP archive by Physics\+FS; the file\textquotesingle{}s contents are used to determine its type where possible.

Currently supported archive types\+:
\begin{DoxyItemize}
\item .Z\+IP (pk\+Zip/\+Win\+Zip/\+Info-\/\+Z\+IP compatible)
\item .7Z (7zip archives)
\item .I\+SO (I\+S\+O9660 files, C\+D-\/\+R\+OM images)
\item .G\+RP (Build Engine groupfile archives)
\item .P\+AK (Quake I/\+II archive format)
\item .H\+OG (Descent I/\+II H\+OG file archives)
\item .M\+VL (Descent II movielib archives)
\item .W\+AD (D\+O\+OM engine archives)
\end{DoxyItemize}

String policy for Physics\+FS 2.\+0 and later\+:

Physics\+FS 1.\+0 could only deal with null-\/terminated A\+S\+C\+II strings. All high A\+S\+C\+II chars resulted in undefined behaviour, and there was no Unicode support at all. Physics\+FS 2.\+0 supports Unicode without breaking binary compatibility with the 1.\+0 A\+PI by using U\+T\+F-\/8 encoding of all strings passed in and out of the library.

All strings passed through Physics\+FS are in null-\/terminated U\+T\+F-\/8 format. This means that if all you care about is English (A\+S\+C\+II characters $<$= 127) then you just use regular C strings. If you care about Unicode (and you should!) then you need to figure out what your platform wants, needs, and offers. If you are on Windows before Win2000 and build with Unicode support, your T\+C\+H\+AR strings are two bytes per character (this is called \char`\"{}\+U\+C\+S-\/2 encoding\char`\"{}). Any modern Windows uses U\+T\+F-\/16, which is two bytes per character for most characters, but some characters are four. You should convert them to U\+T\+F-\/8 before handing them to Physics\+FS with \doxyref{P\+H\+Y\+S\+F\+S\+\_\+utf8\+From\+Utf16()}{p.}{physfs_8h_abad1e7ea73fb2108e136fc043d9c4b30}, which handles both U\+T\+F-\/16 and U\+C\+S-\/2. If you\textquotesingle{}re using Unix or Mac OS X, your wchar\+\_\+t strings are four bytes per character (\char`\"{}\+U\+C\+S-\/4 encoding\char`\"{}). Use \doxyref{P\+H\+Y\+S\+F\+S\+\_\+utf8\+From\+Ucs4()}{p.}{physfs_8h_ac18c9c45db1be6fd26be19d0f4d25a60}. Mac OS X can give you U\+T\+F-\/8 directly from a C\+F\+String or N\+S\+String, and many Unixes generally give you C strings in U\+T\+F-\/8 format everywhere. If you have a single-\/byte high A\+S\+C\+II charset, like so-\/many European \char`\"{}codepages\char`\"{} you may be out of luck. We\textquotesingle{}ll convert from \char`\"{}\+Latin1\char`\"{} to U\+T\+F-\/8 only, and never back to Latin1. If you\textquotesingle{}re above A\+S\+C\+II 127, all bets are off\+: move to Unicode or use your platform\textquotesingle{}s facilities. Passing a C string with high-\/\+A\+S\+C\+II data that isn\textquotesingle{}t U\+T\+F-\/8 encoded will N\+OT do what you expect!

Naturally, there\textquotesingle{}s also \doxyref{P\+H\+Y\+S\+F\+S\+\_\+utf8\+To\+Ucs2()}{p.}{physfs_8h_a0ab51c265f7ca4e8218d6c74b4f43401}, \doxyref{P\+H\+Y\+S\+F\+S\+\_\+utf8\+To\+Utf16()}{p.}{physfs_8h_adebf3f8244318c47e061f780ca8882f9}, and \doxyref{P\+H\+Y\+S\+F\+S\+\_\+utf8\+To\+Ucs4()}{p.}{physfs_8h_a3ff1045e71cd4c14607973da7bfc41e5} to get data back into a format you like. Behind the scenes, Physics\+FS will use Unicode where possible\+: the U\+T\+F-\/8 strings on Windows will be converted and used with the multibyte Windows A\+P\+Is, for example.

Physics\+FS offers basic encoding conversion support, but not a whole string library. Get your stuff into whatever format you can work with.

All platforms supported by Physics\+FS 2.\+1 and later fully support Unicode. We have dropped platforms that don\textquotesingle{}t (O\+S/2, Mac OS 9, Windows 95, etc), as even an OS that\textquotesingle{}s over a decade old should be expected to handle this well. If you absolutely must support one of these platforms, you should use an older release of Physics\+FS.

Many game-\/specific archivers are seriously unprepared for Unicode (the Descent H\+O\+G/\+M\+VL and Build Engine G\+RP archivers, for example, only offer a D\+OS 8.\+3 filename, for example). Nothing can be done for these, but they tend to be legacy formats for existing content that was all A\+S\+C\+II (and thus, valid U\+T\+F-\/8) anyhow. Other formats, like .Z\+IP, don\textquotesingle{}t explicitly offer Unicode support, but unofficially expect filenames to be U\+T\+F-\/8 encoded, and thus Just Work. Most everything does the right thing without bothering you, but it\textquotesingle{}s good to be aware of these nuances in case they don\textquotesingle{}t.

Other stuff\+:

Please see the file L\+I\+C\+E\+N\+S\+E.\+txt in the source\textquotesingle{}s root directory for licensing and redistribution rights.

Please see the file C\+R\+E\+D\+I\+T\+S.\+txt in the source\textquotesingle{}s \char`\"{}docs\char`\"{} directory for a more or less complete list of who\textquotesingle{}s responsible for this.

\begin{DoxyAuthor}{Author}
Ryan C. Gordon. 
\end{DoxyAuthor}
