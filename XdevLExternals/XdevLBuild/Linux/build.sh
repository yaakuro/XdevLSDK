#!/bin/sh

#
# (c) 2017 Cengiz Terzibas Script setups freetype for XdevL.
#

set -e

SCRIPT_DIR=`pwd`

# ------------------------------------------------------------------------------
# Setup build directory.
#
FREETYPE_FILE_NAME=freetype-2.7.1.tar.gz
FREETYPE_OUTPUT_FOLDER=freetype
DESTINATION_ROOT=$SCRIPT_DIR/../../$FREETYPE_OUTPUT_FOLDER

# ------------------------------------------------------------------------------
# Select the build configuration.
#
BUILD_TYPE=release
TARGET=x86_64
#TARGET=aarch64



# ------------------------------------------------------------------------------
# Unpack the freetype library.
#
Unpack() {

	mkdir -p $DESTINATION_ROOT
	tar -xf $SCRIPT_DIR/../../$FREETYPE_FILE_NAME -C $DESTINATION_ROOT --strip-components=1

}

# ------------------------------------------------------------------------------
# Patch the CMake project file for our needs.
#
CMakePatch() {

	sed -i -e '342a add_library(freetype_fpic ${PUBLIC_HEADERS} ${PUBLIC_CONFIG_HEADERS} ${PRIVATE_HEADERS} ${BASE_SRCS})' $DESTINATION_ROOT/CMakeLists.txt
	sed -i -e '343a set_property(TARGET freetype_fpic PROPERTY POSITION_INDEPENDENT_CODE TRUE)' $DESTINATION_ROOT/CMakeLists.txt

}

# ------------------------------------------------------------------------------
# Build the project.
#
Build() {

	mkdir -pv $DESTINATION_ROOT/$BUILD_TYPE/$TARGET
	cd $DESTINATION_ROOT/$BUILD_TYPE/$TARGET

	if [ $BUILD_TYPE = release ]
	then
		CMAKE_BUILD_TYPE=Release
	else
		CMAKE_BUILD_TYPE=Debug
	fi
	echo "BUILD_TYPE      : " $BUILD_TYPE
	echo "CMAKE_BUILD_TYPE: " $CMAKE_BUILD_TYPE

	cmake -DWITH_HarfBuzz=OFF -DWITH_BZip2=OFF -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE ../../
	make -j4

}

# ------------------------------------------------------------------------------
# Build the project.
#
CopyFiles() {

	#
	# Copy files to their needed directories.
	#
	mkdir -p $DESTINATION_ROOT/../lib/linux/$TARGET


	#
	# Copy to Binaries/ThirdParty/CEF3/Linux
	#
	cp --remove-destination $DESTINATION_ROOT/$BUILD_TYPE/$TARGET/*.a $DESTINATION_ROOT/../lib/linux/$TARGET


}

#Unpack
#CMakePatch
Build release
CopyFiles

set +e
