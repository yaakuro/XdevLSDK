/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_ERROR_H
#define XDEVL_ERROR_H

#include <XdevLTypes.h>
#include <iostream>

namespace xdl {

	/**
		@enum XdevLReturnCode
		@brief Error codes that can occur in the core system.
	*/
	enum XdevLReturnCode {

		/// No problem occurred.
		RET_SUCCESS = 0,

		/// Some unspecific error occurred.
		RET_FAILED,

		/// Plugin not found.
		RET_PLUGIN_NOT_FOUND,

		/// Plugin exist already.
		RET_PLUGIN_EXIST_ALREADY,

		/// Plugin doesn't work on this platform.
		RET_PLUGIN_WRONG_PLATFORM,

		/// No valid module found.
		RET_MODULE_NOT_FOUND,

		/// Module exist already.
		RET_MODULE_EXIST_ALREADY,

		/// Something unspecific, but fatal.
		RET_FATAL,

		/// Device is busy but the core may work correctly.
		RET_DEVICE_BUSY,

		/// The device does not work at all.
		RET_DEVICE_NOT_WORKING,

		/// No memory available.
		RET_OUT_OF_MEMORY,

		/// Input/Output operation failed.
		RET_IO,

		/// The user has forgotten to specify the main xml filename.
		RET_MAIN_XML_FILE_NOT_SPECIFIED,

		/// The main xml file could not be found.
		RET_MAIN_XML_FILE_NOT_FOUND,

		/// The main xml has a wrong format.
		RET_WRONG_MAIN_XML_FORMAT,

		/// The core plugin could not be found.
		RET_CORE_PLUGIN_NOT_FOUND,

		/// The core plugin could not be loaded.
		RET_CORE_PLUGIN_LOADING_FAILED,

		/// Critical plugin error. The address of a function could not be returned.
		RET_GET_FUNCTION_ADDRESS_FAILED,

		/// The core system could not be created.
		RET_CORE_CREATION_FAILED,

		/// The core system could not be initialized.
		RET_CORE_INITIALIZATION_FAILED,

		/// A buffer overflow happened.
		RET_BUFFER_OVERFLOW,

		/// A buffer underflow happened.
		RET_BUFFER_UNDERFLOW,

		/// The specified file was not found.
		RET_FILE_NOT_FOUND,

		/// Unknown error.
		RET_UNKNOWN

	};


	/**
		@class XdevLReturn
		@brief A XdevL error object.
	*/
	class XdevLReturn {
		public:

			XdevLReturn() : m_returnCode(RET_SUCCESS), m_description(TEXT("No description")), m_filename(__TEXT_FILE__), m_line(XDEVL_FUNC_LINE) {}
			XdevLReturn(XdevLReturnCode code) : m_returnCode(code), m_description(TEXT("No description")), m_filename(__TEXT_FILE__), m_line(XDEVL_FUNC_LINE) {}
			XdevLReturn(XdevLReturnCode code, const XdevLString& desc) : m_returnCode(code), m_description(desc), m_filename(__TEXT_FILE__), m_line(XDEVL_FUNC_LINE) {}
			XdevLReturn(XdevLReturnCode code, const XdevLString& desc, const XdevLString& filename, xdl_uint line = 0) : m_returnCode(code), m_description(desc), m_filename(filename), m_line(line) {}

			XdevLReturn(XdevLReturn& other) {
				m_returnCode = other.m_returnCode;
				m_description = other.m_description;
				m_filename = other.m_filename;
				m_line = other.m_line;
			}

			XdevLReturn(XdevLReturn&& other) :
				m_returnCode(std::move(other.m_returnCode)),
				m_description(std::move(other.m_description)),
				m_filename(std::move(other.m_filename)),
				m_line(other.m_line)
			{}

			/// Returns the description of the error.
			const XdevLString& getDescription() const {
				return m_description;
			}

			/// Returns the name of the file where the error occurred.
			const XdevLString& getFilename() const {
				return m_description;
			}

			/// Returns the error code.
			XdevLReturnCode getErrorCode() const {
				return m_returnCode;
			}

			/// Returns the line number in the file.
			xdl_int getLine() const {
				return m_line;
			}

			friend std::ostream& operator << (std::ostream& os, const XdevLReturn& err) {
				os << "[ Error Code: " << err.m_returnCode << ": " << err.m_filename << ":" << err.m_line << ": " << err.m_description << " ]";
				return os;
			}

			operator XdevLReturnCode() {
				return m_returnCode;
			}

		private:

			XdevLReturnCode	m_returnCode;
			XdevLString m_description;
			XdevLString m_filename;
			xdl_uint m_line;
	};

#define XDEVL_ERROR(CODE, DESC) xdl::XdevLReturn(CODE, DESC, xdl::XdevLString(__FILE__), XDEVL_FUNC_LINE)
}

#endif
