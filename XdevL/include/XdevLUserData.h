#ifndef XDEVL_USER_DATA_H
#define XDEVL_USER_DATA_H

#include <XdevLID.h>

namespace xdl {

	/**
		@class XdevLUserData
		@brief The iterface for user data.
	*/
	class XdevLUserData {
		public:
			XdevLID id;
			void* data;
	};

}

#endif
