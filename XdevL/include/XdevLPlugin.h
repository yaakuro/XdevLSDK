/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PLUGIN_H
#define XDEVL_PLUGIN_H

#include <XdevLUtils.h>
#include <XdevLVersion.h>
#include <XdevLDynLib.h>

namespace xdl {

#define XDEVL_MODULE_DEFAULT_VENDOR xdl::XdevLString("www.codeposer.net")
#define XDEVL_MODULE_DEFAULT_AUTHOR xdl::XdevLString("Cengiz Terzibas")
#define XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER xdl::XdevLString("(c) 2005 - 2018 Cengiz Terzibas.")

	class XdevLPluginCreateParameter;
	class XdevLModuleCreateParameter;
	class XdevLModule;
	class XdevLCoreMediator;
	class XdevLPluginDescriptor;

	//
	// Function pointer typedefs for creating/deleting modules.
	//
	using XdevLCreateModuleFunction = int(*)(XdevLModuleCreateParameter* parameter);
	using XdevLDeleteModuleFunction = void(*)(XdevLModule* obj);
	using XdevLGetPluginDescriptorFunction = XdevLPluginDescriptor*(*)();
	using XdevLPluginInitFunction = xdl_int(*)(XdevLPluginCreateParameter* parameter);
	using XdevLPluginShutdownFunction = xdl_int(*)();

#define XDEVL_MODULE_PARAMETER_NAME XDEVL_MODULE_PARAMETER->getModuleName()
#define XDEVL_PLUGIN_CREATE_PARAMETER_MEDIATOR XDEVL_PLUGIN_CREATE_PARAMETER->getMediator()
#define XDEVL_PLUGIN_CREATE_MODULE extern "C" XDEVL_EXPORT int _create(xdl::XdevLModuleCreateParameter* XDEVL_MODULE_PARAMETER)

#define XDEVL_MODULE_SET_MODULE_INSTACE(INSTANCE) XDEVL_MODULE_PARAMETER->setModuleInstance(INSTANCE)

#define XDEVL_PLUGIN_INIT extern "C" XDEVL_EXPORT xdl::xdl_int _init_plugin(xdl::XdevLPluginCreateParameter* XDEVL_PLUGIN_CREATE_PARAMETER)
#define XDEVL_PLUGIN_SHUTDOWN extern "C" XDEVL_EXPORT xdl::xdl_int _shutdown_plugin()

#define XDEVL_PLUGIN_INIT_DEFAULT extern "C" XDEVL_EXPORT xdl::xdl_int _init_plugin(xdl::XdevLPluginCreateParameter* XDEVL_PLUGIN_CREATE_PARAMETER) { return xdl::RET_SUCCESS;}
#define XDEVL_PLUGIN_SHUTDOWN_DEFAULT extern "C" XDEVL_EXPORT xdl::xdl_int _shutdown_plugin() { return xdl::RET_SUCCESS;}
#define XDEVL_PLUGIN_DELETE_MODULE_DEFAULT extern "C" XDEVL_EXPORT void _delete(xdl::XdevLModule* obj) { if(obj) delete obj; }
#define XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(PLUGIN_DESCRIPTOR) extern "C" XDEVL_EXPORT xdl::XdevLPluginDescriptor* _getDescriptor(){ return &PLUGIN_DESCRIPTOR; }

#define XDEVL_USE_MODULE(CLASS_NAME, MODULE) (MODULE)
#define XDEVL_NEW_MODULE(CLASS_NAME, PARAMETER) new CLASS_NAME(PARAMETER)
#define XDEVL_NEW_MODULE_DESCRIPTOR(CLASS_NAME, PARAMETER, DESCRIPTOR) new CLASS_NAME(PARAMETER, DESCRIPTOR)
#define XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(CLASS_NAME, MODULE_DESCRIPTOR) {if(MODULE_DESCRIPTOR.getName() == XDEVL_MODULE_PARAMETER->getModuleName()) {xdl::XdevLModule* tmp = new CLASS_NAME(XDEVL_MODULE_PARAMETER, MODULE_DESCRIPTOR); XDEVL_MODULE_PARAMETER->setModuleInstance(tmp); return xdl::RET_SUCCESS;}}
#define XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND return xdl::RET_MODULE_NOT_FOUND;

	/**
	 * @class XdevLPluginCreateParameter
	 * @brief This class is used as parameter when creating a plugin. 
	 */
	class XdevLPluginCreateParameter {
		public:
			XdevLPluginCreateParameter(XdevLCoreMediator* mediator)
				: m_mediator(mediator) {
			}

			/// Returns the mediator of the core system.
			/**
				A valid pointer to the current mediator. This is a part of the XdevLCore module.
			*/
			virtual XdevLCoreMediator* getMediator() {
				return m_mediator;
			}

		private:

			XdevLCoreMediator* m_mediator;
	};

	/**
		@class XdevLPluginDescriptor
		@brief Describes the plugin.

		Holds information like
		- The name of the plugin.
		- How many modules it contains.
		- Build for which architecture.
		- Build for which platform.
		- Version of the plugin.
	*/
	class XdevLPluginDescriptor {
		public:
			using XdevLModuleNames = std::vector<XdevLModuleName>;

			XdevLPluginDescriptor(const XdevLPluginName& name, const XdevLVersion& version) :
				m_name(name),
				m_version(version),
				m_platformName(XDEVL_CURRENT_PLATFORM_AS_STRING),
				m_architecture(XDEVL_CURRENT_ARCHITECTURE_AS_STRING) {}

			XdevLPluginDescriptor(const XdevLString& name,
			                      const std::vector<XdevLModuleName>& moduleNames,
			                      xdl_uint major,
			                      xdl_uint minor,
			                      xdl_uint patch) :
				m_name(name),
				m_version(major, minor, patch),
				m_platformName(XDEVL_CURRENT_PLATFORM_AS_STRING),
				m_architecture(XDEVL_CURRENT_ARCHITECTURE_AS_STRING) {
				for(auto& moduleName : moduleNames) {
					m_moduleNames.push_back(moduleName);
				}
			}

			virtual ~XdevLPluginDescriptor() {}

			/// Returns the name of the plugin.
			/**
				@return Returns a valid pointer to a string.
			*/
			virtual const XdevLPluginName& getName() const {
				return m_name;
			}

			/// Returns the number of modules in the plugin.
			/**
				@return Returns the number of supported modules in the plugin.
			*/
			virtual xdl_uint getNumModules() const {
				return static_cast<xdl_uint>(m_moduleNames.size());
			}

			/// Returns the list of module names.
			virtual XdevLModuleNames getModuleNames() const {
				return m_moduleNames;
			}

			/// Returns the name of the specified module.
			/**
				@param idx Index of the module. The index idx < getNumModules().
				@return Returns a valid pointer to a string which is the name of the module.
			*/
			virtual const XdevLModuleName& getModuleName(const xdl_uint& idx) const {
				XDEVL_ASSERT(idx < m_moduleNames.size(), "Parameter has invalid range.");
				return m_moduleNames[idx];
			}

			/// Returns the version of the plugins.
			/**
				@return Return the version of this plugin.
			*/
			virtual const XdevLVersion& getVersion() const {
				return m_version;
			}

			/// Returns the platform name of this plugin.
			virtual const XdevLPlatformName& getPlatformName() const {
				return m_platformName;
			}

			/// Returns the CPU architecture this plugin was compiled.
			virtual const XdevLArchitecture& getArchitecture() const {
				return m_architecture;
			}

		private:

			XdevLPluginName m_name;
			XdevLVersion m_version;
			XdevLPlatformName m_platformName;
			XdevLArchitecture m_architecture;
			XdevLModuleNames m_moduleNames;
	};


	/**
		@class XdevLPluginInfo
		@brief Class that creates/deletes modules.
	*/
	class XdevLPluginInfo {
		public:

			XdevLPluginInfo(XdevLCreateModuleFunction c,
			                XdevLDeleteModuleFunction d,
			                XdevLGetPluginDescriptorFunction desc,
			                XdevLSharedLibrary* dynlib) :
				m_initPlugin(nullptr),
				m_shutdownPlugin(nullptr),
				m_createModule(c),
				m_deleteModule(d),
				m_getPluginDescriptor(desc),
				m_version(desc()->getVersion()),
				m_sharedLibraryLoader(dynlib) {
			}

			XdevLPluginInfo(XdevLPluginInitFunction pif,
			                XdevLPluginShutdownFunction psf,
			                XdevLCreateModuleFunction c,
			                XdevLDeleteModuleFunction d,
			                XdevLGetPluginDescriptorFunction desc,
			                XdevLSharedLibrary* dynlib) :
				m_initPlugin(pif),
				m_shutdownPlugin(psf),
				m_createModule(c),
				m_deleteModule(d),
				m_getPluginDescriptor(desc),
				m_version(desc()->getVersion()),
				m_sharedLibraryLoader(dynlib) {
			}

			~XdevLPluginInfo() {
				delete m_sharedLibraryLoader;
			}

			/// Initialize a plugin.
			xdl_int initPlugin(XdevLPluginCreateParameter* parameter) {
				return m_initPlugin(parameter);
			}

			/// Shut down a plugin.
			xdl_int shutdownPlugin() {
				return m_shutdownPlugin();
			}

			/// Creates a module.
			xdl_int createModule(XdevLModuleCreateParameter* parameter) {
				return m_createModule(parameter);
			}

			/// Deletes a module.
			void deleteModule(XdevLModule* obj) {
				m_deleteModule(obj);
			}

			/// Returns the plugins descriptor.
			XdevLPluginDescriptor* getPluginDescriptor() {
				return m_getPluginDescriptor();
			}

			/// Returns the associated shared library loader object.
			XdevLSharedLibrary* getSharedLibraryLoader() {
				return m_sharedLibraryLoader;
			}

			/// Returns the version.
			const XdevLVersion& getVersion() const  {
				return m_version;
			}

			xdl_bool isInitPluginValid() {
				return (m_initPlugin != nullptr);
			}

			xdl_bool isShutdownPluginValid() {
				return (m_shutdownPlugin != nullptr);
			}

		private:

			// Hodls the function pointer to initialize a plugin.
			XdevLPluginInitFunction m_initPlugin;

			// Holds the function pointer to shutdown a plugin.
			XdevLPluginShutdownFunction m_shutdownPlugin;

			// Holds the functon pointer to create a module.
			XdevLCreateModuleFunction m_createModule;

			// Holds the function pointer to delete a module.
			XdevLDeleteModuleFunction m_deleteModule;

			// Holds the function pointer to get the plugins descriptor.
			XdevLGetPluginDescriptorFunction m_getPluginDescriptor;

			// Holds the version of the plugin.
			XdevLVersion m_version;

			// Holds the shared library that contains the modules.
			XdevLSharedLibrary* m_sharedLibraryLoader;

	};

}

#endif
