/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_STREAM_H
#define XDEVL_STREAM_H

#include <XdevLModule.h>

namespace xdl {

	/**
	 * @class XdevLStreamRead
	 * @brief Base class for reading from a stream.
	 */
	class XdevLStreamRead {
		public:
			virtual ~XdevLStreamRead() {}

			/// Read data from the stream.
			/**
			 * @brief Read data from the stream.  It can happen that not the whole size is read
			 * at once. Please make sure that you check the return value.
			 * @param dst The destination to copy to.
			 * @param size The number of bytes to read from the stream into the specified buffer.
			 * @return Returns -1 when an error occured else the number of bytes read
			 * from the stream.
			 */
			virtual xdl_int read(xdl_uint8* dst, xdl_int size) = 0;
	};

	/**
	 * @class XdevLStreamWrite
	 * @brief Base class for writing to a stream.
	 */
	class XdevLStreamWrite {
		public:
			virtual ~XdevLStreamWrite() {}

			/// Write data to the stream.
			/**
			 * @brief Write bytes to the stream. It can happen that not the whole size is written
			 * at once. Please make sure that you check the return value.
			 * @param src The buffer to read from.
			 * @param size The number of bytes to send to the stream from the specified buffer.
			 * @return Returns -1 when an error occured else the number of bytes wrote
			 * to the stream.
			 */
			virtual xdl_int write(xdl_uint8* src, xdl_int size) = 0;
	};

	/**
	 * @class XdevLStreamAccess
	 * @brief Base class for reading/writing operations to streams.
	 */
	class XdevLStreamAccess : public XdevLStreamRead, public XdevLStreamWrite {
		public:
			virtual ~XdevLStreamAccess() {}
	};

	/**
		@class XdevLStream
		@brief Interface class stream objects.
		@author Cengiz Terzibas

		This can be files, serial port, buses etc.

		@note Normal users shouldn't use this functions.
	*/
	class XdevLStream : public XdevLStreamAccess {
		public:
			enum class AccesType {
				XDEVL_STREAM_READ_ONLY = 1,
				XDEVL_STREAM_WRITE_ONLY = 2,
				XDEVL_STREAM_READ_WRITE = 4
			};

			virtual ~XdevLStream() {}

			using XdevLStreamRead::read;
			using XdevLStreamWrite::write;

			/// Open a connection using XML properties.
			/**
			 * @return Returns RET_SUCCESS when successful else RET_FAILED.
			 */
			virtual xdl_int open() = 0;

			/// Open a connection to the device with a specific name.
			virtual xdl_int open(const XdevLFileName& name) = 0;

			/// Close the connection.
			virtual xdl_int close() = 0;

			/// Flush input and output buffer.
			/**
				@return
					- @b RET_FAILED if it failed.
					- @b RET_SUCCESS if it was successful.
			*/
			virtual xdl_int flush() = 0;
	};

	using XdevLDeviceModes = std::vector<XdevLStream::AccesType>;
}

#endif
