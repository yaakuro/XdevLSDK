/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_H
#define XDEVL_H

/**
	@file XdevL.h
	@brief This file contains the main core create functions.
*/

#include <XdevLTypes.h>
#include <XdevLCore.h>
#include <XdevLListener.h>
#include <XdevLModule.h>
#include <XdevLPlugin.h>
#include <XdevLUtils.h>
#include <XdevLInput/XdevLInputSystem.h>
#include <XdevLTimer.h>
#include <XdevLXstring.h>
#include <XdevLXfstring.h>
#include <XdevLError.h>

#include <map>

#define XDEVL_MAJOR_VERSION 0
#define XDEVL_MINOR_VERSION 7
#define XDEVL_PATCH_VERSION 0

namespace xdl {

	class XdevLUserData;

	/// Create a module without using the XdevLCore plugin.
	/**
	 * @param pluginDescriptor A valid plugin descriptor that describes a existing plugin to use.
	 * @param moduleDescriptor A valid module descriptor that describes a existing module to use.
	 * @param pluginPath If specified the plugin will be searched in that folder.
	 */
	XDEVL_EXPORT XdevLModule* createModule(const XdevLPluginDescriptor& pluginDescriptor, XdevLModuleDescriptor& moduleDescriptor, const XdevLFileName& pluginPath = XdevLFileName());

	/// Plugs a new Plugin to the specified core.
	/**
	 * @param core A valid XdevLCore object.
	 * @param pluginName The name of the plugin to plug.
	 * @param version The version of the plugin to use.
	 * @return RET_SUCCESS when successful else RET_FAILED.
	 */
	XDEVL_INLINE xdl_int plug(XdevLCore* core, const XdevLPluginName& pluginName, const XdevLVersion& version = xdl::XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, 0), const XdevLPlatformName& platform = XDEVL_CURRENT_PLATFORM_AS_STRING) {
		return core->plug(pluginName, version, platform);
	}

	/// Unplug a plugin from the specified core.
	/**
	 * If a module is while you try to unplug the plugin where the module was created from
	 * this function might fail.
	 * @param core A valid XdevLCore object.
	 * @param pluginName The name of the plugin to unplug.
	 * @return RET_SUCCESS when successful else RET_FAILED.
	 */
	XDEVL_INLINE xdl_int unplug(XdevLCore* core, const XdevLPluginName& pluginName) {
		return core->unplug(pluginName);
	}

	/// Check if the specified module is valid.
	/**
	 * @param module The module to check.
	 * @return xdl_true if the specified module is valid else xdl_false.
	 */
	XDEVL_INLINE xdl_bool isModuleValid(xdl::XdevLModule* module) {
		return (nullptr != module) ? xdl_true : xdl_false;
	}

	/// Check if the specified module is not valid.
	/**
	 * @param module The module to check.
	 * @return xdl_true if the specified module is not valid else xdl_false.
	 */
	XDEVL_INLINE xdl_bool isModuleNotValid(xdl::XdevLModule* module) {
		return (nullptr == module) ? xdl_true : xdl_false;
	}

	/// Opens the specified filename and puts the content into a XdevLString.
	/**
	 * @param filename The filename of the XML file.
	 * @param xmlBuffer XdevLString object that will be filled with the XML file content.
	 * @return
		- RET_SUCCESS when successful loaded.
		- RET_FAILED if an error occurs.
	 */
	XDEVL_EXPORT xdl_int getXmlFile(const XdevLFileName& filename, XdevLString& xmlBuffer);

	/**
		@fn template<typename T> T getModule(XdevLCore* core, const XdevLID& id)
		@brief Returns a existing module from the XdevL core system.

		@param core A valid pointer to the XdevLCore object.
		@param id A null terminated string which identifies the created module.
		@return
		- Returns a valid pointer for the interface T.
		- nullptr if an error occurs.
	*/
	template<typename T>
	T getModule(XdevLCore* core, const XdevLID& id) {
		return static_cast<T>(core->getModule(id));
	}

	/**
		@fn template<typename T> T createModule(XdevLCore* core,
	               const XdevLModuleName& moduleName,
	               const XdevLID& id,
	               const XdevLPluginName& pluginName = XdevLPluginName(),
	               XdevLUserData* data = nullptr)
		@brief Creates a module which is in a plugin.
		@param core A valid pointer to the XdevLCore object.
		@param moduleName A null terminated string which represents the module name.
		@param id A null terminated string which will be used to identify the module in the XdevL core system.
		@param pluginName A null terminated string which is the name of the plugin.
		@param data Not used at the moment.
		@return
		- Returns a valid pointer for the interface T.
		- nullptr if an error occurs.

	*/
	template<typename T>
	T createModule(XdevLCore* core,
	               const XdevLModuleName& moduleName,
	               const XdevLID& id,
	               const XdevLPluginName& pluginName = XdevLPluginName(),
	               XdevLUserData* data = nullptr) {
		return static_cast<T>(core->createModule(moduleName, id, pluginName, data));
	}

	/**
	 * @brief Create a module without the XdevLCore system.
	 * @param pluginDescriptor The descriptor of the plugin.
	 * @param moduleDescriptor The descriptor of the module./
	 * @return A valid instance of the module if successful, nullptr if it fails.
	 */
	template<typename T>
	T createModule(const XdevLPluginDescriptor& pluginDescriptor, xdl::XdevLModuleDescriptor& moduleDescriptor, const xdl::XdevLFileName& path = xdl::XdevLFileName()) {
		return static_cast<T>(createModule(pluginDescriptor, moduleDescriptor, path));
	}

	/// Removes a module from the core system.
	/**
		@fn template<typename T> void destroyModule(XdevLCore* core, T* module)
		@brief Deletes a module from the Core system.

		@param core A valid pointer to the XdevLCore object.
		@param id The id of the module that should be deleted. You can get the XdevLModuleId
		for a module if you use the XdevLModule::getId() fuction.
		@return
		- @b RET_MODULE_NOT_FOUND
		The specified module doesn't exists int the system.
		- @b RET_SUCCESS
		If it was successful.
	*/
	template<typename T>
	XDEVL_INLINE xdl_int destroyModule(XdevLCore* core, T* module) {
		return core->deleteModule(module->getID());
	}

	/**
	 * @brief Create the XdevLCore module.
	 * @param core A pointer to a XdevLCore pointer that will hold the valid pointer to the instance.
	 * @param argc argc of the main function.
	 * @param argv argv of the main function.
	 * @param xmlBufferSize If a xml buffer is passed specify the length of it in bytes.
	 * @param xmlBuffer The buffer that holds the xml data.
	 * @param numberOfUserData The number of user data that should be passed.
	 * @param userDataList User data list to pass user information.
	 * @return Returns RET_SUCCESS on success else RET_FAILED;
	 */
	xdl_int createCore(XdevLCore** core,
	                   xdl_int argc,
	                   xdl_char* argv[],
	                   xdl_uint numberOfUserData = 0,
	                   XdevLUserData* userDataList[] = nullptr);

	xdl_int createCore(XdevLCore** core,
	                   xdl_int argc,
	                   xdl_char* argv[],
	                   xdl_int xmlBufferSize,
	                   xdl_uint8* xmlBuffer,
	                   xdl_uint numberOfUserData = 0,
	                   XdevLUserData* userDataList[] = nullptr);

	xdl_int createCore(XdevLCore** core,
	                   xdl_int argc,
	                   xdl_char* argv[],
	                   const XdevLString& xmlBuffer,
	                   xdl_uint numberOfUserData = 0,
	                   XdevLUserData* userDataList[] = nullptr);

	xdl_int createCore(XdevLCore** core,
	                   xdl_int argc,
	                   xdl_char* argv[],
	                   const XdevLFileName& fileName,
	                   xdl_uint numberOfUserData = 0,
	                   XdevLUserData* userDataList[] = nullptr);

	/**
		@fn xdl_int destroyCore(XdevLCore* core)
		@brief Destroys the XdevLCore system.

		@param core A valid pointer to the XdevLCore object.
		@return
			- RET_SUCCESS if it was successful.
			- or xdl::eXdevLErrorCodes if a failure occurred.
	*/
	xdl_int destroyCore(XdevLCore* core);

	/**
	 * @brief Sets the XdevLPlugins search path.
	 * @param path
	 *
	 * Use this function to specify in which folder XdevL should look by default for the plugins.
	 */
	void setPluginPath(const xdl::XdevLFileName& path);

#if 1
#define createModuleText(CORE, INTERFACE, ID, MODULE) xdl::createModule<xdl::INTERFACE*>(CORE, xdl::XdevLModuleName(#INTERFACE), xdl::XdevLID(ID))
#else
#define createModuleText(CORE, INTERFACE, ID, MODULE) new xdl::INTERFACE##MODULE
#endif

	using XdevLCreateModuleFunctionType = xdl::xdl_int(*)(xdl::XdevLModuleCreateParameter* parameter, std::shared_ptr<xdl::XdevLModule>& module);
	using XdevLDeleteModuleFunctionType = xdl::xdl_int(*)();

	class XdevLModuleNode {
		public:
			XdevLModuleNode() :
				create(nullptr),
				destroy(nullptr) {
			}

			XdevLModuleNode(XdevLCreateModuleFunctionType createFunction) :
				create(createFunction) {
			}

			XdevLModuleNode(XdevLCreateModuleFunctionType createFunction, XdevLDeleteModuleFunctionType destroyFunction) :
				create(createFunction),
				destroy(destroyFunction) {
			}

			XdevLCreateModuleFunctionType create;
			XdevLDeleteModuleFunctionType destroy;
	};

	/**
		@class XdevLMonolithic
		@brief Supports monolithic build of XdevL Modules.

		@page xdevlmonolithic Usage

		auto monolithic = xdl::createXdevLMonolithic();
		monolithic->plug(xdl::XdevLModuleName(TEXT("XdevLCore")), xdl::createXdevLCore);
		auto core = std::dynamic_pointer_cast<xdl::XdevLCore>(monolithic->create(xdl::XdevLModuleName(TEXT("XdevLCore")), xdl::XdevLID("MyCore")));

	*/
	class XdevLMonolithic {
		public:

			virtual xdl_int plug(const XdevLModuleName& moduleName, XdevLCreateModuleFunctionType createModuleFunctionType) = 0;
			virtual std::shared_ptr<XdevLModule> create(const XdevLModuleName& moduleName, const xdl::XdevLID& id, std::shared_ptr<xdl::XdevLCore> core = nullptr) = 0;
	};

	using IXdevLMonolithic = XdevLMonolithic;
	using IPXdevLMonolithic = std::shared_ptr<XdevLMonolithic>;

	/**
	 * @brief Create the monolithic system.
	 * 
	 * Using the monolithic instance gives you another way of linking the modules of the XdevL framework.
	 */
	XDEVL_EXPORT IPXdevLMonolithic createXdevLMonolithic();
}


#endif
