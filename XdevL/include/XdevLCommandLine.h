/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/
#ifndef XDEVL_COMMAND_LINE_H
#define XDEVL_COMMAND_LINE_H

#include <XdevLTypes.h>
#include <vector>
#include <string>
#include <map>
#include <sstream>

namespace xdl {

	/**
		@class XdevLParameter
		@brief A command line parameter.
	*/
	class XdevLParameter {
		public:

			enum XdevLParameterType {
			    BOOL_PARAMETER,
			    INT_PARAMETER,
			    STRING_PARAMETER
			};

			enum XdevLParameterArgument {
			    ARGUMENT_NEEDED,
			    ARGUMENT_NOT_NEEDED
			};

			XdevLParameter(const XdevLString& cm, XdevLParameterType type, XdevLParameterArgument argumentExpected = ARGUMENT_NOT_NEEDED) :
				m_command(cm),
				m_desc(TEXT("No description")),
				m_type(type),
				m_setFlag(xdl_false),
				m_argumentExpected(argumentExpected) {
			}

			XdevLParameter(const XdevLString& cm, const XdevLString& desc, XdevLParameterType type, XdevLParameterArgument argumentExpected = ARGUMENT_NOT_NEEDED) :
				m_command(cm),
				m_desc(desc),
				m_type(type),
				m_setFlag(xdl::xdl_false),
				m_argumentExpected(argumentExpected) {
			}

			const XdevLString& getCommand() {
				return m_command;
			}

			const XdevLString& getDescription() {
				return m_desc;
			}

			XdevLParameterType getType() {
				return m_type;
			}

			xdl::xdl_bool getSet() {
				return m_setFlag;
			}

			XdevLParameterArgument argumentExpected() const {
				return m_argumentExpected;
			}

		protected:

			XdevLString m_command;
			XdevLString	m_desc;
			XdevLParameterType 	m_type;
			xdl_bool m_setFlag;
			XdevLParameterArgument m_argumentExpected;
	};

	/**
	 * @class XdevLBoolParameter
	 * @brief Boolean implementation of the XdevLParameter.
	 */
	class XdevLBoolParameter : public XdevLParameter {
		public:
			XdevLBoolParameter(const XdevLString& cm, XdevLParameterArgument argumentExpected, xdl::xdl_bool* defaultValue) :
				XdevLParameter(cm, BOOL_PARAMETER, argumentExpected),
				m_state(defaultValue) {
			}
			XdevLBoolParameter(const XdevLString& cm, const XdevLString& desc, XdevLParameterArgument argumentExpected, xdl::xdl_bool* defaultValue) :
				XdevLParameter(cm, desc, BOOL_PARAMETER, argumentExpected),
				m_state(defaultValue) {
			}
			xdl::xdl_bool getValue() {
				return *m_state;
			}
			void setValue(xdl::xdl_bool state) {
				*m_state = state;
				m_setFlag = xdl::xdl_true;
			}
		private:
			xdl_bool* m_state;
	};

	/**
	 * @class XdevLIntParameter
	 * @brief Integer implementation of the XdevLParameter.
	 */
	class XdevLIntParameter: public XdevLParameter {
		public:
			XdevLIntParameter(const XdevLString& cm, XdevLParameterArgument argumentExpected, xdl::xdl_int* defaultValue) :
				XdevLParameter(cm, INT_PARAMETER, argumentExpected),
				m_state(defaultValue) {
			}

			XdevLIntParameter(const XdevLString& cm, const XdevLString& desc, XdevLParameterArgument argumentExpected, xdl::xdl_int* defaultValue) :
				XdevLParameter(cm, desc, INT_PARAMETER, argumentExpected),
				m_state(defaultValue) {
			}

			xdl::xdl_int getValue() {
				return *m_state;
			}

			void setValue(xdl::xdl_int state) {
				*m_state = state;
				m_setFlag = xdl::xdl_true;
			}
		private:
			xdl_int* m_state;
	};

	/**
	 * @class XdevLStringParameter
	 * @brief String implementation of the XdevLParameter.
	 */
	class XdevLStringParameter: public XdevLParameter {
		public:
			XdevLStringParameter(const XdevLString& cm, XdevLParameterArgument argumentExpected, XdevLString* defaultValue) :
				XdevLParameter(cm, STRING_PARAMETER, argumentExpected),
				m_state(defaultValue) {
			}

			XdevLStringParameter(const XdevLString& cm, const XdevLString& desc, XdevLParameterArgument argumentExpected, XdevLString* defaultValue) :
				XdevLParameter(cm, desc, STRING_PARAMETER, argumentExpected),
				m_state(defaultValue) {
			}

			const XdevLString& getValue() {
				return *m_state;
			}

			void setValue(const XdevLString& state) {
				*m_state = state;
				m_setFlag = xdl::xdl_true;
			}

		private:
			XdevLString* m_state;
	};

	/**
		@class XdevLCommandLineParser
		@brief Class to manage command line arguments.
	*/
	class XdevLCommandLineParser {
		public:

			/// Type for the arguments.
			using Arguments = std::vector<std::string>;

			XdevLCommandLineParser(xdl_int argc, xdl_char** argv) {
				if((argc != 0) || (argv != NULL)) {
					// Create strings out of the argument vector.
					this->m_args = Arguments(argv, argv+argc);
				}
			}

			void add(XdevLParameter* parameter) {
				assert(parameter && "XdevLCommandLine::add: Not a valid parameter!");
				if(m_parameters.find(parameter->getCommand()) != m_parameters.end()) {
					std::cerr << "XdevLCommandLine::add: Command '" + parameter->getCommand().toUTF8() + "' already defined!" << std::endl;
					exit(-1);
				}
				m_parameters[parameter->getCommand()] = parameter;
			}

			void parse() {

				// Skip if no arguments where passed.
				if(this->m_args.size() == 0) {
					return;
				}

				for(Arguments::iterator argument = this->m_args.begin() + 1; argument != this->m_args.end(); ++argument) {

					//
					// Get the correct registered parameter and assign the values to it.
					//
					XdevLString arg = XdevLString((*argument).c_str());
					arg = arg.substr(arg.find(XdevLString(TEXT("-"))) + 1, arg.size());

					//
					// Is it a help command?
					//
					if((arg == XdevLString(TEXT("h"))) || (arg == XdevLString(TEXT("help")))) {
						std::cout << "\n--- Help Menu ----------------------------------------------------------------\n" << std::endl;

						for(std::map<XdevLString, XdevLParameter*>::const_iterator parameter = m_parameters.begin();
						        parameter != m_parameters.end();
						        ++parameter) {
							std::cout << "-" << parameter->second->getCommand() << ": " << parameter->second->getDescription() << std::endl;
						}

						std::cout << "\n------------------------------------------------------------------------------\n" << std::endl;

						exit(0);
					}

					XdevLParameter* parameter = m_parameters[arg];
					if(parameter == NULL) {
						continue;
					}

					switch(parameter->getType()) {
						case XdevLParameter::BOOL_PARAMETER: {
							XdevLBoolParameter* sp = static_cast<XdevLBoolParameter*>(parameter);
							xdl::xdl_bool state = xdl_true;
							if(parameter->argumentExpected() == XdevLParameter::ARGUMENT_NEEDED) {
								argument++;
								std::stringstream ss(*argument);
								ss >> state;
							}
							sp->setValue(state);
						}
						break;
						case XdevLParameter::INT_PARAMETER: {
							XdevLIntParameter* sp = static_cast<XdevLIntParameter*>(parameter);
							argument++;
							std::stringstream ss(*argument);
							xdl::xdl_int state;
							ss >> state;
							sp->setValue(state);
						}
						break;
						case XdevLParameter::STRING_PARAMETER: {
							XdevLStringParameter* sp = static_cast<XdevLStringParameter*>(parameter);
							argument++;
							sp->setValue(XdevLString((*argument).c_str()));
						}
						break;
					}
				}
			}

			/// Returns the number of arguments.
			xdl_int getNumberOfArguments() {
				return static_cast<xdl_int>(this->m_args.size());
			}

			/// Returns a specific argument from the argument list.
			std::string& getArgument(xdl_uint idx) {
				return this->m_args[idx];
			}

			/// Returns the hole list of arguments.
			Arguments& getArguments() {
				return this->m_args;
			}



		private:

			// Holds a list of arguments.
			Arguments 	m_args;

			std::map<XdevLString, XdevLParameter*> m_parameters;

	};

}

#endif
