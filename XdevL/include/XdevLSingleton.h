/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SINGLETON_H
#define XDEVL_SINGLETON_H

#include <utility>

namespace xdl {

	/**
		@class XdevLSingleton
		@brief Helper template to implement Singleton pattern classes.
	*/
	template<class T>
	class XdevLSingleton {

			static T* m_instance;

		public:

			template <typename... Args>
			static T* Instance(Args...args) {
				if(nullptr == m_instance) {
					m_instance = new T(std::forward<Args>(args)...);
				}
				return m_instance;
			}

			static void Destroy() {
				if(nullptr != m_instance) {
					delete m_instance;
				}
				m_instance = nullptr;
			}
	};

	template<typename T>
	T* XdevLSingleton<T>::m_instance = nullptr;
}

#endif
