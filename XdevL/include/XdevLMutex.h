/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MUTEX_H
#define XDEVL_MUTEX_H

#include <XdevLPlatform.h>

#if defined (XDEVL_PLATFORM_UNIX)
#include <Unix/XdevLMutex.h>
#else
#include <Windows/XdevLMutex.h>
#endif

namespace xdl {
	namespace thread {

		/**
		 @class XdevLScopeLock
		 @brief Locks and unlocks a mutex automatically.
		*/
		class XdevLScopeLock {
			public:
				XdevLScopeLock(Mutex& mutex) : m_mutex(mutex) {
					m_mutex.Lock();
				}

				~XdevLScopeLock() {
					m_mutex.Unlock();
				}

				operator Mutex& () {
					return m_mutex;
				}

			private:
				/// Disable copy construction.
				XdevLScopeLock(const XdevLScopeLock& rhs);

				/// Disable assignment.
				XdevLScopeLock& operator =(const XdevLScopeLock& rhs);

				Mutex& m_mutex;
		};
	}
}
#endif /* MUTEX_H_ */
