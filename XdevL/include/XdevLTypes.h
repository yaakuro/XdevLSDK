/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_TYPES_H
#define XDEVL_TYPES_H

#include <XdevLPlatform.h>

#ifdef __GNUC__
#include <stdint.h>
#endif

#define XDEVL_MODULE_NULLPTR nullptr;

namespace xdl {

#ifdef _MSC_VER

	typedef __int8 				xdl_int8;
	typedef __int16 			xdl_int16;
	typedef __int32 			xdl_int32;
	typedef __int64 			xdl_int64;
	typedef unsigned __int8 	xdl_uint8;
	typedef unsigned __int16 	xdl_uint16;
	typedef unsigned __int32 	xdl_uint32;
	typedef unsigned __int64 	xdl_uint64;

// C99 related

	typedef __int8 				int8_t;
	typedef __int16 			int16_t;
	typedef __int32 			int32_t;
	typedef __int64 			int64_t;
	typedef unsigned __int8 	uint8_t;
	typedef unsigned __int16 	uint16_t;
	typedef unsigned __int32 	uint32_t;
	typedef unsigned __int64 	uint64_t;

	typedef int8_t    int_least8_t;
	typedef int16_t   int_least16_t;
	typedef int32_t   int_least32_t;
	typedef int64_t   int_least64_t;
	typedef uint8_t   uint_least8_t;
	typedef uint16_t  uint_least16_t;
	typedef uint32_t  uint_least32_t;
	typedef uint64_t  uint_least64_t;

	typedef int8_t    int_fast8_t;
	typedef int16_t   int_fast16_t;
	typedef int32_t   int_fast32_t;
	typedef int64_t   int_fast64_t;
	typedef uint8_t   uint_fast8_t;
	typedef uint16_t  uint_fast16_t;
	typedef uint32_t  uint_fast32_t;
	typedef uint64_t  uint_fast64_t;

#endif
#ifdef __GNUC__
	typedef int8_t 		xdl_int8;
	typedef int16_t 	xdl_int16;
	typedef int32_t		xdl_int32;
	typedef int64_t		xdl_int64;
	typedef uint8_t 	xdl_uint8;
	typedef uint16_t 	xdl_uint16;
	typedef uint32_t	xdl_uint32;
	typedef uint64_t	xdl_uint64;
#endif

	typedef bool			xdl_bool;
	typedef unsigned char 	xdl_ubyte;
	typedef char			xdl_byte;
	typedef unsigned char	xdl_uchar;
	typedef char			xdl_char;
	typedef xdl_uint32		xdl_uint;
	typedef xdl_int32		xdl_int;
	typedef xdl_int64		xdl_long;
	typedef xdl_uint64		xdl_ulong;
	typedef float			xdl_float;
	typedef double			xdl_double;

	const bool xdl_true		= 1;
	const bool xdl_false	= 0;
	const bool xdl_yes		= 1;
	const bool xdl_no		= 0;

#ifdef XDEVL_ARCHITECTURE_64_BIT
	using xdl_vptr = xdl_int64;
#elif defined(XDEVL_ARCHITECTURE_32_BIT)
	using xdl_vptr = xdl_int32;
#else
#error "Architecture not supported."
#endif

	class XdevLString;
	class XdevLModuleName;
	class XdevLPluginName;
	class XdevLInternalName;
	class XdevLFileName;

	/**
	@struct XdevSize
	*/
	template<typename T>
	struct XdevLSize {
		using type = T;

		XdevLSize() : width(0), height(0) {}
		XdevLSize(type w, type h) : width(w), height(h) {}

		const type& getWidth() const {
			return width;
		}
		const type& getHeight() const {
			return height;
		}

		friend bool operator==(const XdevLSize& s1, const XdevLSize& s2) {
			return ((s1.width == s2.width) and (s1.height == s2.height));
		}

		XdevLSize<T>& operator *= (T value) {
			width *= value;
			height *= value;
			return *this;
		}

		XdevLSize<T>& operator /= (T value) {
			width /= value;
			height /= value;
			return *this;
		}

		type width;
		type height;
	};

	using XdevLSizeUI = XdevLSize<xdl_uint>;
	using XdevLSizeI = XdevLSize<xdl_int>;
	using XdevLSizeF = XdevLSize<xdl_float>;
	using XdevLSizeD = XdevLSize<xdl_double>;

	/**
		@struct XdevLPosition
		@brief A structure that holds 2d position data.
	*/
	template<typename T>
	struct XdevLPosition {
		using type = T;

		XdevLPosition() : x(0), y(0) {}
		XdevLPosition(type _x, type _y) : x(_x), y(_y) {}

		/// Returns the x coordinate of the position.
		const type& getX() const {
			return x;
		}

		/// Returns the y coordinate of the position.
		const type& getY() const {
			return y;
		}

		/// Sets the x coordinate of the position.
		void setX(const type& value) {
			x = value;
		}

		/// Sets the y coordinate of the position.
		void setY(const type& value) {
			y = value;
		}

		/// X Coordinate of the position.
		type x;

		/// Y Coordinate of the position.
		type y;
	};

	using XdevLPositionI = XdevLPosition<xdl_int>;
	using XdevLPositionUI = XdevLPosition<xdl_uint>;
	using XdevLPositionF = XdevLPosition<xdl_float>;
	using XdevLPositionD = XdevLPosition<xdl_double>;

	using XdevLUTF32String = std::u32string;
	using XdevLUTF16String = std::u16string;

#if XDEVL_PLATFORM_WINDOWS
#undef TEXT
#endif

#define TEXT(MSG) MSG
#define __TEXT__(x) TEXT(x)
#define __TEXT_FILE__ __TEXT__(__FILE__)

	/**
		@struct XdevLString
		@brief A string structure that holds a string.

		This string class is working in progress. Not all parts will work out of
		the latin representation boundary.
	*/
	class XdevLString {
		public:
			using type = std::string;

			XdevLString() {}

			XdevLString(const type& str) : m_value(str) {}

			explicit XdevLString(const char* str) {
				this->m_value = ansiToString(str);
			}

			explicit XdevLString(const wchar_t* str) {
				this->m_value = wideToString(str);
			}

			explicit XdevLString(const char32_t* str) {
				this->m_value = utf32ToString(str);
			}

			XDEVL_INLINE static XdevLString ansiToString(const char* str) {
				XdevLString res;
				xdl_char tmp[7] {};
				while(*str) {
					xdl_char* dest = tmp;
					unicodeToUTF8(dest, (xdl_uint32)*str++);
					*dest = 0;
					res.m_value += tmp;
				}
				return res;
			}

			XDEVL_INLINE static XdevLString asciiToString(const char* str) {
				XdevLString res;
				xdl_char tmp[7] {};
				while(*str) {
					xdl_char* dest = tmp;
					asciiCharacterToUTF8(dest, (xdl_uint32)*str++);
					*dest = 0;
					res.m_value += tmp;
				}
				return res;
			}

			XDEVL_INLINE static XdevLString wideToString(const wchar_t* str) {
				XdevLString res;
				xdl_char tmp[7] {};
				while(*str) {
					xdl_char* dest = tmp;
					unicodeToUTF8(dest, (xdl_uint32)*str);
					str++;
					*dest = 0;
					res.m_value += tmp;
				}
				return res;
			}

			XDEVL_INLINE static XdevLString wideToString(const std::wstring& str) {
				return wideToString(str.data());
			}

			XDEVL_INLINE static XdevLString unicodeToString(const char32_t* str) {
				return utf32ToString(str);
			}

			XDEVL_INLINE static XdevLString utf32ToString(const char32_t* str) {
				XdevLString res;
				xdl_char tmp[7] {};
				while(*str) {
					xdl_char* dest = tmp;
					unicodeToUTF8(dest, (xdl_uint32)*str++);
					*dest = 0;
					res.m_value += tmp;
				}
				return res;
			}

			/// Convert UTF-16 to XdevLString.
			XDEVL_INLINE static XdevLString utf16ToString(const char16_t* str) {
				XdevLString res;
				xdl_char tmp[7] {};
				while(*str) {
					xdl_char* dest = tmp;
					auto size = utf16ToUTF8(dest, str);
					*dest = 0;
					res.m_value += tmp;
					str+= size;
				}
				return res;
			}

			XDEVL_INLINE std::string toString() const {
				return this->m_value;
			}

			XDEVL_INLINE std::string toUTF8() const {
				return this->m_value;
			}

			std::u32string toUnicode() {
				std::u32string tmp;

				const xdl_char* src = (xdl_char*)this->m_value.data();
				while(true) {
					char32_t unicode = (char32_t)utf8ToUnicode(src);
					if(unicode == 0) {
						break;
					}
					tmp += unicode;
				}
				return tmp;
			}

			std::wstring toWideString() {
				std::wstring tmp;

				const xdl_char* src = (xdl_char*)this->m_value.data();
				while(true) {
					wchar_t wc = (wchar_t)utf8ToUnicode(src);
					if(wc == 0) {
						break;
					}
					tmp += wc;
				}
				return tmp;
			}

			XdevLUTF32String toUTF32String() {
				XdevLUTF32String tmp;

				const xdl_char* src = (xdl_char*)this->m_value.data();
				while(true) {
					char32_t utf32 = (char32_t)utf8ToUnicode(src);
					if(utf32 == 0) {
						break;
					}
					tmp += utf32;
				}
				return tmp;
			}

			XDEVL_INLINE size_t characterSize() const {
				return sizeof(type::value_type);
			}

			XDEVL_INLINE void clear() {
				this->m_value.clear();
			}

			XDEVL_INLINE size_t length() const {
				return this->m_value.length();
			}

			size_t size() {
				xdl_uint ret = 0;

				const xdl_char* src = this->m_value.data();
				if(!src) {
					return ret;
				}

				const xdl_char* end = this->m_value.data() + this->m_value.size();

				while(src < end) {
					utf8ToUnicode(src);
					++ret;
				}

				return ret;
			}

			type substr(type::size_type pos,  type::size_type count) {
				// TODO Write this method for UTF-8.
				return this->m_value.substr(pos, count);
			}

			type::size_type find_last_of(const type& str, type::size_type pos = 0) const {
				// TODO Write this method for UTF-8.
				return this->m_value.find_last_of(str, pos);
			}

			type::size_type find_first_of(const type& str, type::size_type pos = 0) const {
				// TODO Write this method for UTF-8.
				return this->m_value.find_first_of(str, pos);
			}

			const char* begin() const {
				return (const char*)m_value.data();
			}

			const char* end() const {
				return (const xdl_char*)&(m_value.data())[m_value.length()];
			}

			/// Convert unicode string to UTF-16 string.
			XDEVL_INLINE static XdevLUTF16String unicodeToUTF16String(const char32_t* unicode) {
				XdevLUTF16String res;
				char16_t tmp[2] {};
				while(*unicode) {
					char16_t* dest = tmp;
					unicodeToUTF16(dest, *unicode++);
					*dest = 0;
					res+= tmp;
				}
				return res;
			}

			/// Convert ascii to UTF-8.
			/**
					This is quite easy because on the first 127 values are used.
					We can just copy the values 1&1.
			 */
			XDEVL_INLINE static void asciiCharacterToUTF8(xdl_char*& dst, char c) {
				if((c & 0x80) == 0) {
					*dst = c;
					dst++;
				} else {
					assert(0 && "Not a valid ASCII character.");
				}
			}

			/// Convert UTF16 to UTF8.
			XDEVL_INLINE static xdl_uint32 utf16ToUTF8(xdl_char*& dst, const char16_t* w) {
				char32_t codepoint {};
				xdl_uint32 size = 1;

				if(w[0] < 0xd800 || w[0] > 0xdfff) {
					codepoint = (char32_t)w[0];
				} else if(w[0] >= 0xd800 && w[0] < 0xdbff) {
					if(w[1] <= 0xdc00 && w[1] <= 0xdfff) {
						codepoint = ((char32_t)(w[0]) - 0xd800) * 0x400 + (w[1] - 0xdc00);
						codepoint *= 0x10000;
						size = 2;
					} else {
						assert(0 && "Invalid UTF string.");
					}
				} else {
					assert(0 && "Invalid UTF string.");
				}
				unicodeToUTF8(dst, codepoint);
				return size;
			}

			/// Encode single unicode codepoint into single UTF-8 representation.
			XDEVL_INLINE static void unicodeToUTF8(xdl_char*& dst, xdl_uint32 codepoint) {
				// U+0000 – U+007F code point.
				if(codepoint < 0x80) {
					*dst = codepoint;
					dst++;
				}
				// U+0080 – U+07FF code point.
				else if(codepoint < 0x800) {
					dst[0] = (xdl_char)(((codepoint >> 6) & 0x1f) | 0xc0);
					dst[1] = (xdl_char)(((codepoint) & 0x3f) | 0x80);
					dst += 2;
				}
				// U+0800 – U+FFFF code point.
				else if(codepoint < 0x10000) {
					dst[0] = (xdl_char)(((codepoint >> 12) & 0x0f) | 0xe0);
					dst[1] = (xdl_char)(((codepoint >>  6) & 0x3f) | 0x80);
					dst[2] = (xdl_char)(((codepoint) & 0x3f) | 0x80);
					dst += 3;
				}
				// U+10000 – U+1FFFFF code point.
				else if(codepoint < 0x200000) {
					dst[0] = (xdl_char)(((codepoint >> 18) & 0x70) | 0xf0);
					dst[1] = (xdl_char)(((codepoint >> 12) & 0x3f) | 0x80);
					dst[2] = (xdl_char)(((codepoint >>  6) & 0x3f) | 0x80);
					dst[3] = (xdl_char)(((codepoint) & 0x3f) | 0x80);
					dst += 4;
				}
				// U+200000 – U+3FFFFFF code point.
				else if(codepoint < 0x4000000) {
					dst[0] = (xdl_char)(((codepoint >> 24) & 0x30) | 0xf8);
					dst[1] = (xdl_char)(((codepoint >> 18) & 0x3f) | 0x80);
					dst[2] = (xdl_char)(((codepoint >> 12) & 0x3f) | 0x80);
					dst[3] = (xdl_char)(((codepoint >>  6) & 0x3f) | 0x80);
					dst[4] = (xdl_char)(((codepoint) & 0x3f) | 0x80);
					dst += 5;
				}
				// U+4000000 – U+7FFFFFFF code point.
				else {
					dst[0] = (xdl_char)(((codepoint >> 30) & 0x10) | 0xfc);
					dst[1] = (xdl_char)(((codepoint >> 24) & 0x3f) | 0x80);
					dst[2] = (xdl_char)(((codepoint >> 18) & 0x3f) | 0x80);
					dst[3] = (xdl_char)(((codepoint >> 12) & 0x3f) | 0x80);
					dst[4] = (xdl_char)(((codepoint >> 6) & 0x3f) | 0x80);
					dst[5] = (xdl_char)(((codepoint >> 0) & 0x3f) | 0x80);
					dst += 6;
				}
			}

			/// Encode single unicode codepoint into single UTF-16 representation.
			XDEVL_INLINE static void unicodeToUTF16(char16_t*& dst, char32_t codepoint) {
				// U+0000 to U+D7FF and U+E000 to U+FFFF
				if(codepoint < 0x10000) {
					dst[0] = codepoint;
					dst++;
				}
				// U+10000 to U+10FFFF
				else {
					char32_t udot = codepoint - 0x10000;
					char16_t high = char16_t(udot >> 10) + 0xd800;
					char16_t low = high + 0xdc00;
					dst[0] = low;
					dst[1] = high;
					dst +=2;
				}
			}

			/// Decode UTF-8 into unicode representation.
			XDEVL_INLINE static xdl_uint32 utf8ToUnicode(const xdl_char*& src) {
				// TODO There are no precise checks.
				if(nullptr == src) {
					return 0;
				}

				// Get the first byte from the list and proceed src to next.
				xdl_uchar byte1 = *src++;

				// U+0000 – U+007F code point.
				if(byte1 < 0x80) {
					return (xdl_uint32)byte1;
				}
				// U+0080 – U+07FF code point.
				else if(byte1 < 0xe0) {
					xdl_uchar byte2 = *src++;
					return (xdl_uint32)((byte2 & 0x3f) | ((byte1 & 0x1f) << 6));
				}
				// U+0800 – U+FFFF code point.
				else if(byte1 < 0xf0) {
					xdl_uchar byte2 = *src++;
					xdl_uchar byte3 = *src++;
					return (xdl_uint32)((byte3 & 0x3f) | (byte2 & 0x3f) << 6 | ((byte1 & 0x0f) << 12));
				}
				// U+10000 – U+1FFFFF code point.
				else if(byte1 < 0xf8) {
					xdl_uchar byte2 = *src++;
					xdl_uchar byte3 = *src++;
					xdl_uchar byte4 = *src++;
					return (xdl_uint32)((byte4 & 0x3f) | (byte3 & 0x3f) << 6 | (byte2 & 0x3f) << 12 | ((byte1 & 0x70) << 18));
				}
				// U+200000 – U+3FFFFFF code point.
				else if(byte1 < 0xfc) {
					xdl_uchar byte2 = *src++;
					xdl_uchar byte3 = *src++;
					xdl_uchar byte4 = *src++;
					xdl_uchar byte5 = *src++;
					return (xdl_uint32)((byte5 & 0x3f) | (byte4 & 0x3f) << 6  | (byte3 & 0x3f) << 12 | (byte2 & 0x3f) << 18 | ((byte1 & 0x30) << 24));
				}
				// U+4000000 – U+7FFFFFFF code point.
				else if(byte1 < 0xfe) {
					xdl_uchar byte2 = *src++;
					xdl_uchar byte3 = *src++;
					xdl_uchar byte4 = *src++;
					xdl_uchar byte5 = *src++;
					xdl_uchar byte6 = *src++;
					return (xdl_uint32)((byte6 & 0x3f) | (byte5 & 0x3f) << 6 | (byte4 & 0x3f) << 12 | (byte3 & 0x3f) << 18 | (byte2 & 0x3f) << 24 | ((byte1 & 0x10) << 30));
				} else {
					assert(0 && "Wrong UTF-8 encoding detected.");
				}
				return 0;
			}

			xdl_int toInt() {
				std::stringstream streamIn(this->toString());
				xdl_int ret;
				streamIn >> ret;
				return ret;
			}

			// TODO This needs to be done.
			XDEVL_INLINE static void printf(const char* format, ...) {
				va_list args;
				va_start(args, format);

				char buffer[256];
				vsnprintf(buffer,256,format, args);
				perror(buffer);
				va_end(args);
			}

			xdl_float toFloat() {
				std::stringstream streamIn(this->toString());
				xdl_float ret;
				streamIn >> ret;
				return ret;
			}

			friend std::ostream& operator << (std::ostream& os, const XdevLString& name) {
				os << name.m_value;
				return os;
			}

			XdevLString operator + (const XdevLString& str) {
				return XdevLString(str.m_value + m_value);
			}

			friend XdevLString operator + (const XdevLString& str1, const XdevLString& str2) {
				return XdevLString(str1.m_value + str2.m_value);
			}

			XdevLString& operator = (const xdl_char* str) {
				m_value = str;
				return *this;
			}

			XdevLString& operator += (const xdl_char str) {
				m_value += str;
				return *this;
			}

			XdevLString& operator = (const wchar_t* str) {
				clear();

				xdl_char tmp[7] {};
				while(*str) {
					xdl_char* dest = tmp;
					unicodeToUTF8(dest, (xdl_uint32)*str++);
					*dest = 0;
					this->m_value += tmp;
				}
				return *this;
			}

			XdevLString& operator += (const XdevLString& str) {
				m_value = m_value + str.m_value;
				return *this;
			}

			bool operator == (const type& s1) {
				return (m_value == s1);
			}

			friend bool operator == (const XdevLString& s1, const XdevLString& s2) {
				return (s1.m_value == s2.m_value);
			}

			friend bool operator != (const XdevLString& s1, const XdevLString& s2) {
				return !(s1.m_value == s2.m_value);
			}

			friend bool operator < (const XdevLString& s1, const XdevLString& s2) {
				return (s1.m_value < s2.m_value);
			}

			operator const char* () const {
				return (const char*)m_value.data();
			}

			void replaceAll(const XdevLString& from, const XdevLString& to) {
				if(from.m_value.empty()) {
					return;
				}
				size_t start_pos = 0;
				while((start_pos = m_value.find(from.m_value, start_pos)) != XdevLString::type::npos) {
					m_value.replace(start_pos, from.m_value.length(), to.m_value);
					start_pos += to.m_value.length();
				}
			}

			size_t find(const XdevLString& str) const {
				return m_value.find(str.m_value);
			}

			void trim() {
				const type& whitespace = TEXT(" \t\r");
				const auto strBegin = m_value.find_first_not_of(whitespace);

				if(strBegin == type::npos) {
					return ; 	// No content
				}

				const auto strEnd = m_value.find_last_not_of(whitespace);
				const auto strRange = strEnd - strBegin + 1;

				m_value = m_value.substr(strBegin, strRange);
			}

			size_t tokenize(std::vector<XdevLString>& tokens, XdevLString delimiters) {
				// Skip delimiters at beginning.
				XdevLString::type::size_type lastPos = m_value.find_first_not_of(delimiters.m_value, 0);
				// Find first "non-delimiter".
				XdevLString::type::size_type pos     = m_value.find_first_of(delimiters.m_value, lastPos);

				while(XdevLString::type::npos != pos || XdevLString::type::npos != lastPos) {
					// Found a token, add it to the vector.
					tokens.push_back(m_value.substr(lastPos, pos - lastPos));
					// Skip delimiters.  Note the "not_of"
					lastPos = m_value.find_first_not_of(delimiters.m_value, pos);
					// Find next "non-delimiter"
					pos = m_value.find_first_of(delimiters.m_value, lastPos);
				}

				return tokens.size();
			}

			xdl_bool empty() {
				return this->m_value.empty();
			}

			xdl_uint8* data() {
				return (xdl_uint8*)this->m_value.data();
			}

			type m_value;
	};


	/**
		@class XdevLPluginName
		@brief A class that holds the name of a plugin.

	*/
	class XdevLPluginName : public XdevLString {
		public:
			XdevLPluginName() : XdevLString() {}
			XdevLPluginName(const XdevLString& str) : XdevLString(str) {}

			explicit XdevLPluginName(const char* t) : XdevLString(t) {}

			XdevLPluginName& operator = (XdevLString::type t) {
				m_value = t;
				return *this;
			}

	};

	/**
		@class XdevLModuleName
		@brief A class that holds the name of a module.
	*/
	class XdevLModuleName : public XdevLString {
		public:
			XdevLModuleName() : XdevLString() {}

			explicit XdevLModuleName(const char* t) : XdevLString(t) {}

			XdevLModuleName& operator = (XdevLString::type t) {
				m_value = t;
				return *this;
			}
	};

	/**
		@class XdevLInternalName
		@brief A class that holds the name of an internal object within a module.
	*/
	class XdevLInternalName : public XdevLString {
		public:
			XdevLInternalName() : XdevLString() {}
			XdevLInternalName(const XdevLString::type& str) : XdevLString(str) {}
			explicit XdevLInternalName(const char* t) : XdevLString(t) {}

			XdevLInternalName& operator = (XdevLString::type t) {
				m_value = t;
				return *this;
			}

	};

	/**
		@class XdevLFileName
		@brief A class that holds the name of a file.

		Supports basic filename manipulation methods.
	*/
	class XdevLFileName : public XdevLString {
		public:
			XdevLFileName() : XdevLString() {}
			XdevLFileName(const XdevLString& str) : XdevLString(str) {}
			XdevLFileName(const XdevLString::type& str) : XdevLString(str) {}
			XdevLFileName(const XdevLPluginName& pluginName) : XdevLString(pluginName) {}

			explicit XdevLFileName(const char* str) : XdevLString(str) {}
			explicit XdevLFileName(const char32_t* str) : XdevLString(str) {}

			XdevLFileName& operator = (XdevLString::type t) {
				this->m_value = t;
				return *this;
			}

			XdevLString& operator = (const XdevLString& str) {
				this->m_value = str.m_value;
				return *this;
			}

			friend XdevLFileName operator + (const XdevLFileName& str1, const XdevLFileName& str2) {
				XdevLFileName tmp(str1);
				tmp += str2;
				return tmp;
			}

			friend XdevLFileName operator + (const XdevLString& str1, const XdevLFileName& str2) {
				XdevLFileName tmp(str1);
				tmp += str2;
				return tmp;
			}

			friend XdevLFileName operator + (const XdevLFileName& str1, const XdevLString& str2) {
				XdevLFileName tmp(str1);
				tmp += str2;
				return tmp;
			}

			void makePath() {
				replaceAll(XdevLString(TEXT("\"")), XdevLString(TEXT("")));
				replaceAll(XdevLString(TEXT("//")), XdevLString(TEXT("/")));
			}

			/// Returns the extension of the filename.
			/**
				If the filename does not have any extension a empty XdevLString will
				be returned.
			*/
			XdevLFileName getExtension() const {

				//
				// Extract extions of the filename.
				//
				size_t ext_pos = m_value.find_last_of(TEXT("."));
				if(ext_pos != XdevLString::type::npos) {
					// Extract the extension of the file.
					auto tmp = m_value.substr(ext_pos + 1, XdevLString::type::npos);
					return XdevLFileName(tmp);
				}
				return XdevLFileName();
			}

			/// Removes the file extension if it has one.
			XdevLFileName& removeExtension() {
				size_t pos = m_value.find_last_of(TEXT("."));
				if(pos == XdevLFileName::type::npos) {
					return *this;
				}
				m_value = m_value.substr(0, pos);
				return *this;
			}

			/// Removes the file extension if it has one.
			XdevLFileName getWithoutExtension() const {
				size_t pos = m_value.find_last_of(TEXT("."));
				if(pos == XdevLFileName::type::npos) {
					return m_value;
				}
				auto tmp = XdevLFileName(m_value.substr(0, pos));
				return tmp;
			}

			/// Returns the path of the filename.
			/**
				If the filename does not have a file, it will return the current path '.'.
			*/
			XdevLFileName getPath() const {

				//
				// Extract path of the filename.
				//
				size_t path_pos = m_value.find_last_of(TEXT("/\\"));
				if(path_pos != XdevLFileName::type::npos) {
					// Extract the path of the file.
					return XdevLFileName(m_value.substr(0, ++path_pos));
				}

				return XdevLFileName(TEXT("."));
			}

			/// Returns a filename without a path.
			XdevLFileName getWithoutPath() const {
				size_t pos = m_value.find_last_of(TEXT("/\\"));
				if(pos == XdevLFileName::type::npos) {
					return *this;
				}

				XdevLFileName tmp(m_value.substr(pos + 1, m_value.size()));
				return tmp;
			}

			/// Removes the path from the filename
			XdevLFileName& removePath() {
				size_t pos = m_value.find_last_of(TEXT("/\\"));
				if(pos == XdevLFileName::type::npos) {
					return *this;
				}

				m_value = m_value.substr(pos + 1, m_value.size());
				return *this;
			}

			/// Returns the filename without path and extension.
			/**
				This function will return a string that represents the filename
				without path and exntension information.
			*/
			XdevLFileName getFilename() {

				//
				// Extract extions of the filename.
				//
				size_t ext_pos = m_value.find_last_of(TEXT("."));
				if(ext_pos == std::string::npos) {
					// No extension found.
					ext_pos = m_value.size();
				}

				//
				// Extract path of the filename.
				//
				size_t path_pos = m_value.find_last_of(TEXT("/\\"));
				if(path_pos == std::string::npos) {
					// No path found.
					path_pos	= 0;
				}

				// Extract the filename.
				size_t number_to_read = ext_pos - path_pos ;

				return XdevLFileName(m_value.substr(path_pos, number_to_read));
			}
	};

	const XdevLFileName XdevLFileCurrentPath(TEXT("."));
	const XdevLFileName XdevLFileParentPath(TEXT(".."));


	/**
	 * @class XdevLDirectoryName
	 * @brief Basic directory name manipulation.
	 */
	class XdevLDirectoryName : public XdevLFileName {

	};

	/**
		@class XdevLPlatformName
		@brief A class that holds the name of the platform.

	*/
	class XdevLPlatformName : public XdevLString {
		public:
			XdevLPlatformName() : XdevLString() {}

			explicit XdevLPlatformName(const char* t) : XdevLString(t) {}

			XdevLPlatformName& operator = (XdevLString::type t) {
				m_value = t;
				return *this;
			}
	};

	/**
		@class XdevLArchitecture
		@brief A class that holds the name of CPU architecture.

	*/
	class XdevLArchitecture : public XdevLString {
		public:
			XdevLArchitecture() : XdevLString() {}

			explicit XdevLArchitecture(const char* t) : XdevLString(t) {}

			XdevLArchitecture& operator = (XdevLString::type t) {
				m_value = t;
				return *this;
			}
	};


	class XdevLTimeMilliseconds {
		public:
			XdevLTimeMilliseconds() = default;
			XdevLTimeMilliseconds(xdl_uint64 ms) : milliseconds(ms) {}

			xdl_uint64 milliseconds = 0;

			XdevLTimeMilliseconds operator - (const XdevLTimeMilliseconds& t1) {
				XdevLTimeMilliseconds tmp = *this;
				tmp.milliseconds -= t1.milliseconds;
				return tmp;
			}

			operator xdl_uint64() {
				return milliseconds;
			}
	};
}


// -----------------------------------------------------------------------------
// Needed classes and template specialization to be able to use unordered_map with XdevLString.
//
struct XdevLStringHash {
	std::size_t operator()(const xdl::XdevLString& str) const {
		return (std::hash<std::string>()(str.m_value));
	}
};

namespace std {
	template<>
	struct hash<xdl::XdevLString> {
		size_t operator()(const xdl::XdevLString& str) const {
			auto const h1(hash<string> {}(str.m_value));
			return h1;
		}
	};

}
// -----------------------------------------------------------------------------

#endif
