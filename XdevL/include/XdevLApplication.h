/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_APPLICATION_H
#define XDEVL_APPLICATION_H

#include <XdevL.h>
#include <XdevLFileSystem/XdevLArchive.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLInput/XdevLMouse/XdevLMouse.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>
#include <XdevLInput/XdevLJoystick/XdevLJoystick.h>
#include <XdevLInput/XdevLInputActionSystem/XdevLInputActionSystem.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLAudio/XdevLAudio.h>

#include <fstream>
#include <vector>
#include <string>
#include <atomic>

namespace xdl {

	class XdevLMouse;
	class XdevLKeyboard;
	class XdevLWindow;
	class XdevLJoystick;

	/**
		@class XdevLApplication
		@brief The main class for all XdevL applications.
	*/
	class XdevLApplication : public XdevLListener {
		public:
			using Super = xdl::XdevLApplication;

			/// Type for the arguments.
			using Arguments = std::vector<std::string>;

			/// Type for one argument.
			using Argument = std::string;

			XdevLApplication(const XdevLWindowTitle& title, int argc = 0, char** argv = nullptr, const XdevLFileName& xml_filename = XdevLFileName())
				: m_core(nullptr)
				, m_archive(nullptr)
				, m_window(nullptr)
				, m_cursor(nullptr)
				, m_inputActionSystem(nullptr)
				, m_rai(nullptr)
				, m_rdstream(nullptr)
				, m_argc(0)
				, m_argv(nullptr)
				, m_id("XdevLApplication")
				, m_appIsRunning(xdl_true) {

				this->m_argc = argc;
				this->m_argv = argv;

				// Create strings out of the argument vector.
				this->m_args = Arguments(argv, argv+argc);

				// Flag for redirecting the output stream.
				xdl_bool rds = xdl_false;

				xdl_int argcursor = -1;

				Arguments::iterator arg(this->m_args.begin());

				// Check argument list.
				while(arg != this->m_args.end()) {

					argcursor++;
					// Redirect stream only if we didn't do it already.
					if((*arg == "-rds") && (rds == xdl_false)) {
						rds = xdl_true;
						std::cout << "Redirecting stream requested." << std::endl;
						if(argcursor + 1 > argc) {
							std::cerr << "Please specify the argument for the -rds command." << std::endl;
							std::cerr << "Ignoring -rds" << std::endl;
						}
						continue;
					}

					// If the user wished to redirect the stream we should do so.
					if((rds == xdl_true) && (m_rdstream == nullptr)) {
						// Redirect the stdio to a file.
						m_rdstream = freopen((*arg).c_str(), "w", stdout);
						if(m_rdstream == nullptr) {
							std::cerr << "Couldn't redirect the output stream." << std::endl;
						} else {
							std::cout << "Stream redirected." << std::endl;
						}
						continue;
					}

					arg++;
				}


				// Create the core system.
				if(XdevLString() != xml_filename) {
					if(createCore(&m_core, argc, argv, xml_filename) != RET_SUCCESS) {
						XDEVL_ASSERT(0, "Could not find XdevLCore XML file");
					}
				} else {
					if(createCore(&m_core, argc, argv) != RET_SUCCESS) {
						XDEVL_ASSERT(0, "Could not find XdevLCore XML file");
					}
				}
				getCore()->plug(XdevLPluginName("XdevLWindowX11"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION), XdevLPlatformName("Linux"));
				getCore()->plug(XdevLPluginName("XdevLWindowWindows"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION), XdevLPlatformName("Windows"));
				getCore()->plug(XdevLPluginName("XdevLWindowCocoa"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION), XdevLPlatformName("Mac"));
				m_window = createModule<XdevLWindow*>(getCore(), XdevLModuleName("XdevLWindow"), XdevLID("XdevLApplicationWindow"));
				XDEVL_ASSERT(nullptr != m_window, "Could not find XdevLWindow");

				if(XdevLWindowTitle() != title) {
					XdevLWindowAttribute windowAttribute {};
					windowAttribute.title = title;
					if(m_window->create(windowAttribute) == xdl::RET_FAILED) {
						XDEVL_ASSERT(nullptr != m_window, "Could create XdevLWindow instance.");
					}
				} else {
					if(m_window->create() == xdl::RET_FAILED) {
						XDEVL_ASSERT(nullptr != m_window, "Could create XdevLWindow instance.");
					}
				}

				m_cursor = getModule<XdevLCursor*>(getCore(), XdevLID("XdevLCursor"));
				XDEVL_ASSERT(nullptr != m_cursor, "Could not find XdevLCursor.");

				m_cursor->attach(m_window);

				// ---------------------------------------------------------------------
				// Initialize the XdevLInputActionSystem.
				//
				getCore()->plug(XdevLPluginName("XdevLInputActionSystem"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION));
				m_inputActionSystem = createModule<IPXdevLInputActionSystem>(getCore(), XdevLModuleName("XdevLInputActionSystem"), XdevLID("XdevLApplicationInputActionSystem"));
				XDEVL_ASSERT(nullptr != m_inputActionSystem, "Could not find XdevLInputActionSystem.");

				xdl::XdevLInputActionSystemParameter parameter {};
				parameter.window = m_window;

				this->getInputActionSystem()->create(parameter);
				// ---------------------------------------------------------------------


				// ---------------------------------------------------------------------
				// Initialize the XdevLFileSystem.
				//
				getCore()->plug(XdevLPluginName("XdevLFileSystem"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION));
				m_archive = createModule<IPXdevLArchive>(getCore(), XdevLModuleName("XdevLArchive"), XdevLID("XdevLApplicationArchive"));
				XDEVL_ASSERT(nullptr != m_archive, "Could not find XdevLArchive.");

				m_archive->mount(xdl::XdevLFileName("."));
				m_archive->setWriteDir(xdl::XdevLFileName("."));
				// ---------------------------------------------------------------------

				// ---------------------------------------------------------------------
				// Initialize the RAI system.
				//
				getCore()->plug(XdevLPluginName("XdevLOpenGLContextGLX"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION), XdevLPlatformName("Linux"));
				getCore()->plug(XdevLPluginName("XdevLOpenGLContextWGL"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION), XdevLPlatformName("Windows"));
				getCore()->plug(XdevLPluginName("XdevLOpenGLContextGLX"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION), XdevLPlatformName("Mac"));
				getCore()->plug(XdevLPluginName("XdevLRAIGL"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION));
				m_rai = createModule<IPXdevLRAI>(getCore(), XdevLModuleName("XdevLRAIGL"), XdevLID("XdevLApplicationRAI"));
				XDEVL_ASSERT(nullptr != m_rai, "Could not find XdevLRAI.");

				if(m_rai->create(getWindow()) != xdl::RET_SUCCESS) {
					XDEVL_ASSERT(0, "Failed to create XdevLRAI");
				}
				// ---------------------------------------------------------------------

				// ---------------------------------------------------------------------
				// Initialize the audio system.
				//
				getCore()->plug(XdevLPluginName("XdevLAudioOpenAL"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION), XdevLPlatformName("Linux"));
				m_audio = createModule<audio::IPXdevLAudio>(getCore(), XdevLModuleName("XdevLAudioPlayback"), XdevLID("XdevLApplicationAudio"));
				XDEVL_ASSERT(nullptr != m_audio, "Could not find XdevLRAI.");

				// ---------------------------------------------------------------------

			}

			virtual ~XdevLApplication() {
				// If the user requested redirection of the output stream
				// we have to free the FILE resource we used to do that.
				if(m_rdstream != nullptr) {
					fclose(m_rdstream);
				}

				// Destroy the core.
				destroyCore(m_core);
			}

			/// Start the main function.
			void start() {

				// ---------------------------------------------------------------------
				// We are going to listen to some internal events using the notify method.
				//
				this->getCore()->registerListener(this);

				// ---------------------------------------------------------------------
				// Pre-Initialize the application.
				//
				this->preInit(this->m_args);

				// ---------------------------------------------------------------------
				// Initialize the input actions registration.
				//
				this->initInputActions(m_inputActionSystem);

				// ---------------------------------------------------------------------
				// Start main loop.
				//
				this->main();
			}

			// The main loop.
			void main() {
				auto accumulatorTime = 0.0;
				auto accumulatorDeltaTime = 0.01;

				xdl::XdevLTimer timer;
				timer.reset();
				auto currentTime = timer.getTime();
				auto proccessTime = 0.0;

				while(m_appIsRunning) {
					getCore()->update();
					tick(getCore()->getDT());

					auto newTime = timer.getTime();
					auto frameTime = newTime - currentTime;

					if(frameTime > 0.25) {
						frameTime = 0.25;
					}

					currentTime = newTime;
					accumulatorTime += frameTime;

					while(accumulatorTime >= accumulatorDeltaTime) {
						updatePhysics(proccessTime, accumulatorDeltaTime);

						proccessTime += accumulatorDeltaTime;
						accumulatorTime -= accumulatorDeltaTime;
					}

					{
						xdl::XdevLRAIRenderScope renderScope(getRAI(), getWindow());
						updateGraphics(accumulatorDeltaTime);
						updateUI(accumulatorDeltaTime);
					}

					m_fps = 1.0/frameTime;
				}
			}

			/// Will be called before the main loop starts and before the input actions are registered.
			virtual void preInit(const Arguments& argv) {}

			/// Override this method to implement your tick method.
			virtual void tick(xdl_float dT) {}

			/// Override this method to handle physics calculations.
			virtual void updatePhysics(xdl_double time, xdl_double dT) {}

			/// Override this method to handle your rendering.
			virtual void updateGraphics(xdl_double dT) {}

			/// Override this method to handle your UI.
			virtual void updateUI(xdl_double dT) {}

			/// Override this method to register your own event actions.
			virtual void initInputActions(IPXdevLInputActionSystem& inputActionsSystem) {

				// ---------------------------------------------------------------------
				// Register exit application and fullscreen mode when specific input
				// actions occur.
				//
				XdevLActionDelegateType fullscreenDelegate = XdevLActionDelegateType::Create<XdevLApplication, &XdevLApplication::handleFullscreenApplication>(this);
				XdevLActionDelegateType exitApplicationDelegate = XdevLActionDelegateType::Create<XdevLApplication, &XdevLApplication::handleExitApplication>(this);

				inputActionsSystem->registerButtonAction(XdevLString(TEXT("Fullscreen")), KEY_ENTER, fullscreenDelegate);
				inputActionsSystem->registerButtonAction(XdevLString(TEXT("ExitApplication")), KEY_ESCAPE, exitApplicationDelegate);
			}

			// Handle CTRL + ALT + Enter for fullscreen.
			void handleFullscreenApplication(const XdevLDelegateActionParameter& actionParameter) {
				if((actionParameter.state == BUTTON_PRESSED) &&
				    (actionParameter.mod & KEY_MOD_LCTRL) &&
				    (actionParameter.mod & KEY_MOD_LALT)) {
					static xdl_bool fullscreen = xdl_false;
					fullscreen = !fullscreen;

					getWindow()->setFullscreen(fullscreen);
				}
			}

			void handleExitApplication(const XdevLDelegateActionParameter& actionParameter) {
				if(actionParameter.state == BUTTON_PRESSED) {
					m_appIsRunning = xdl_false;
				}
			}

			virtual xdl_int notify(XdevLEvent& event) {
				switch(event.type) {
					case XDEVL_CORE_EVENT: {
						if(event.core.type == XDEVL_CORE_SHUTDOWN) {
							m_appIsRunning = xdl_false;
						}
					}
					break;
					case xdl::XDEVL_WINDOW_EVENT: {
						switch(event.window.event) {
							case xdl::XDEVL_WINDOW_MOVED:
							case xdl::XDEVL_WINDOW_RESIZED: {
								xdl::XdevLViewPort viewport {};
								viewport.x = 0.0f;
								viewport.y = 0.0f;
								viewport.width = event.window.width;
								viewport.height = event.window.height;
								getRAI()->setViewport(viewport);
							}
							break;
						}
					}
					break;
				}
				return RET_SUCCESS;
			}

			virtual XdevLModuleDescriptor* getDescriptor() const {
				return nullptr;
			}

			virtual const XdevLID& getID() const {
				return m_id;
			}

			/// Returns the pointer to the XdevLCore instance.
			XDEVL_INLINE IPXdevLCore getCore() {
				return m_core;
			}

			/// Returns the pointer to the XdevLArchive instance.
			XDEVL_INLINE IPXdevLArchive getArchive() {
				return m_archive;
			}

			/// Returns the pointer to the XdevLWindow instance.
			XDEVL_INLINE XdevLWindow* getWindow() {
				return m_window;
			}

			/// Returns the pointer to the XdevLCursor instance.
			XDEVL_INLINE XdevLCursor* getCursor() {
				return m_cursor;
			}

			/// Returns the pointer to the XdevLKeyboard instance.
			XDEVL_INLINE XdevLKeyboard* getKeyboard() {
				return m_inputActionSystem->getKeyboard();
			}

			/// Returns the pointer to the XdevLMouse instance.
			XDEVL_INLINE XdevLMouse* getMouse() {
				return m_inputActionSystem->getMouse();
			}

			/// Returns the pointer to the XdevLJoystick instance.
			XDEVL_INLINE XdevLJoystick* getJoystick() {
				return m_inputActionSystem->getJoystick();
			}

			/// Returns the pointer to the XdevLInputActionSystem instance.
			XDEVL_INLINE IPXdevLInputActionSystem getInputActionSystem() {
				return m_inputActionSystem;
			}

			/// Returns the pointer to the XdevLRAI instance.
			XDEVL_INLINE IPXdevLRAI getRAI() {
				return m_rai;
			}

			/// Returns the pointer to the XdevLRAI instance.
			XDEVL_INLINE audio::IPXdevLAudio getAudio() {
				return m_audio;
			}

			/// Returns the current FPS (Frames Per Second).
			XDEVL_INLINE xdl_float getFPS() {
				return m_fps;
			}

		private:

			IPXdevLCore m_core;
			IPXdevLArchive m_archive;
			IPXdevLWindow m_window;
			IPXdevLCursor m_cursor;
			IPXdevLInputActionSystem m_inputActionSystem;
			IPXdevLRAI m_rai;
			audio::IPXdevLAudio m_audio;

			// Holds the redirected stream file pointer.
			FILE* m_rdstream;

			// Holds a list of arguments.
			Arguments m_args;

			// Holds the argc argument of the main function.
			xdl_int m_argc;

			// Holds the argv arguments of the main function.
			xdl_char** m_argv;

			XdevLID m_id;

			std::atomic<xdl_bool> m_appIsRunning;

			xdl_double m_fps;
	};

#define XdevLStartMainXML(APP_NAME, WINDOW_TITLE, XML_FILENAME)	int main(int argc, char* argv[]) {try {APP_NAME app(WINDOW_TITLE, argc, argv, XML_FILENAME); app.start();} catch(...) {std::cerr << "Application: Error occurred.\n";} return 0;}
#define XdevLStartMain(APP_NAME, WINDOW_TITLE)	int main(int argc, char* argv[]) {try {APP_NAME app(WINDOW_TITLE, argc, argv); app.start();} catch(...) {std::cerr << "Application: Error occurred.\n";} return 0;}
#define XDEVL_APP_DEFAULT_BODY_XML(APP_NAME) APP_NAME(const xdl::XdevLWindowTitle& title, int argc, char** argv, const xdl::XdevLFileName& xml_filename) : Super(title, argc, argv, xml_filename) {}
#define XDEVL_APP_BODY_XML(APP_NAME) APP_NAME(const xdl::XdevLWindowTitle& title, int argc, char** argv, const xdl::XdevLFileName& xml_filename) 
}

#endif
