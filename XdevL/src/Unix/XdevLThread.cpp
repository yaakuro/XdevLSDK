/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLThread.h>
#include <XdevLMutex.h>

#include <XdevLUtils.h>
#include <XdevLError.h>

#include <iostream>
#include <cassert>

namespace xdl {

	namespace thread {

		pthread_t Thread::m_mainThreadID;

		Thread::Thread() :
			m_thread(0),
			m_arg(NULL) {
		}

		Thread::Thread(thread_callback_function func) {
			XDEVL_ASSERT(func != nullptr,"Invalid callback function");
			callbackFunction = func;
			StartUsingFuncPointer();
		}

		Thread::~Thread() {

		}

		xdl_int Thread::Start(ThreadArgument* arg) {
			Arguments(arg);
			pthread_attr_t type;
			pthread_attr_init(&type);
			pthread_attr_setdetachstate(&type, PTHREAD_CREATE_JOINABLE);
			if(pthread_create(&m_thread, &type, Thread::ThreadProc, this) != 0) {
				XDEVL_MODULEX_ERROR(Thread, "Could not create thread.\n");
				return RET_FAILED;
			}
			m_isInterrupted = false;
			return RET_SUCCESS;
		}

		void Thread::Stop() {
			m_isInterrupted = true;
			if(m_thread) {
				pthread_join(m_thread, 0);
			}
		}

		bool Thread::IsInterrupted() {
			return m_isInterrupted;
		}

		xdl_int Thread::StartUsingFuncPointer() {
			if(pthread_create(&m_thread, NULL, Thread::ThreadProc2, this) != 0) {
				XDEVL_MODULEX_ERROR(Thread, "Could not create thread.\n");
				return RET_FAILED;
			}
			return RET_SUCCESS;
		}


		xdl_int Thread::Join() {
			if(m_thread == 0) {
				return RET_FAILED;
			}

			xdl_int ret = pthread_join(m_thread, NULL);
			if(0 != ret) {
				return RET_FAILED;
			}

			return RET_SUCCESS;
		}

		void Thread::Exit(xdl_int exitCode) {
			xdl_int tmp = exitCode;
			pthread_exit(&tmp);
		}

		void* Thread::ThreadProc(void* p_this) {
			Thread* ptr = static_cast<Thread*>(p_this);
			return reinterpret_cast<void*>(ptr->RunThread(ptr->Arguments()));

		}

		void* Thread::ThreadProc2(void* p_this) {
			Thread* ptr = static_cast<Thread*>(p_this);
			return reinterpret_cast<void*>(ptr->callbackFunction());

		}

		xdl_int Thread::RunThread(ThreadArgument* p_arg) {
			return 0;
		}

		ThreadArgument* Thread::Arguments() {
			return m_arg;
		}

		void Thread::Arguments(ThreadArgument* p_arg) {
			m_arg = p_arg;
		}

		void Thread::SetMainThread() {
			m_mainThreadID = GetCurrentThreadID();
		}

		pthread_t Thread::GetCurrentThreadID() {
			return pthread_self();
		}

		bool Thread::IsMainThread() {
			return GetCurrentThreadID() == m_mainThreadID;
		}
	}
}
