/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLDynLib.h>
#include <XdevLXfstring.h>
#include <XdevLUtils.h>
#include <XdevLError.h>
#include <string>
#include <iostream>

#define TOSTRING(x) #x

namespace xdl {

#ifdef XDEVL_PLATFORM_ANDROID
	XdevLString XdevLSharedLibrary::extension = XdevLString("so");
#else
	XdevLString XdevLSharedLibrary::extension = XdevLString("xdp");
#endif

	XdevLSharedLibrary::XdevLSharedLibrary()
		: m_module(0) {
	}

	XdevLSharedLibrary::~XdevLSharedLibrary() {
	}

	xdl_bool XdevLSharedLibrary::isOpen() const {
		return (0 != m_module);
	}

	XdevLFileName XdevLSharedLibrary::createFileName(const xdl_char* filename, xdl_bool attachDefault) {
		XdevLFileName modifiedFilename(filename);
		auto path = modifiedFilename.getPath();
		auto pathless = modifiedFilename.getWithoutPath();

		XdevLFileName extless;
		// Check if the file has a valid extension.
		if(hasValidExtension(modifiedFilename.toString()) == false) {
			extless = pathless;
		} else {
			extless = pathless.getWithoutExtension();
		}
#if defined (XDEVL_PLATFORM_UNIX)
		modifiedFilename.clear();
		if(path.size() == 0)
			modifiedFilename = XdevLFileName(TEXT("./"));
		else
			modifiedFilename = path + XdevLFileName(TEXT("/"));

		if(attachDefault) {
			modifiedFilename += extless + XdevLFileName(TEXT(".")) + extension;
		} else {
			modifiedFilename += extless;
		}
#elif defined (XDEVL_PLATFORM_WINDOWS)
		// Hint: If you have GetLastError() == 126, Make sure you have the correct DLL in your bin folder.
		// Your DLL might depend on other DLL.

		// Ok, this is because LoadLibrary can not handle "./xxx.dll" properly.
		if(path == xstd::current_path_id)
			modifiedFilename = extless;
		else
			modifiedFilename = path + XdevLFileName(TEXT("/")) + extless;

		xstd::make_dos_file(filename);

		if(attachDefault) {
			modifiedFilename += XdevLFileName(TEXT(".")) + extension();
		}
#elif defined(XDEVL_PLATFORM_MINGW)
		if(path == xstd::current_path_id)
			modifiedFilename = extless + XdevLFileName(TEXT(".")) + extension;
		else
			modifiedFilename = path + XdevLFileName(TEXT("/")) + extless + XdevLFileName(TEXT(".")) + extension;
#else
#error XDEVL_UNKNOWN_PLATFORM
#endif
		return modifiedFilename;
	}

	xdl_int XdevLSharedLibrary::open(const xdl_char* pName, xdl_bool attachDefault) {

		if(m_module) {
			if(close() != 0) {
				// TODO Do we have to do something here?
			}
		}

		// ATTN: The file extension must not be written, otherwise it will fail.
		auto filename = createFileName(pName, attachDefault);
#if defined (XDEVL_PLATFORM_UNIX)
		m_module = dlopen(filename.toString().c_str(), RTLD_LAZY);
		if(m_module == NULL) {
			XDEVL_MODULEX_ERROR(XdevLSharedLibrary, "dlopen failed:" << dlerror() << std::endl);
			return RET_FAILED;
		}
#elif defined (XDEVL_PLATFORM_WINDOWS)
		m_module = LoadLibraryA(filename.c_str());
		if(nullptr == m_module) {
			XDEVL_MODULEX_ERROR(XdevLSharedLibrary, "LoadLibraryA failed:"  << GetLastError() << std::endl);
			return RET_FAILED;
		}
#elif defined(XDEVL_PLATFORM_MINGW)
		m_module = LoadLibraryA(filename.c_str());
		if(m_module == NULL) {
			XDEVL_MODULEX_ERROR(XdevLSharedLibrary, "LoadLibraryA failed:"  << GetLastError() << std::endl);
			return RET_FAILED;
		}
#else
#error XDEVL_UNKNOWN_PLATFORM
#endif

		return RET_SUCCESS;
	}

	xdl_int XdevLSharedLibrary::close() {

		// Already desytroyed?
		if(m_module == 0) {
			return RET_FAILED;
		}
#if defined (XDEVL_PLATFORM_UNIX)
		if(dlclose(m_module) != 0) {
			XDEVL_MODULEX_ERROR(XdevLSharedLibrary, "dlclose failed:"  << dlerror() << std::endl);
			return RET_FAILED;
		}
#elif defined (XDEVL_PLATFORM_WINDOWS) ||  defined(XDEVL_PLATFORM_MINGW)
		if(FreeLibrary(static_cast<HMODULE>(m_module)) == 0) {
			XDEVL_MODULEX_ERROR(XdevLSharedLibrary, "FreeLibrary failed:"  << GetLastError() << std::endl);
			return RET_FAILED;
		}
#else
#error XDEVL_UNKNOWN_PLATFORM
#endif
		m_module = 0;
		return RET_SUCCESS;
	}

	SHARED_FUNCTION XdevLSharedLibrary::getFunctionAddress(const xdl_char* pName) {

		SHARED_FUNCTION lProc = 0;

#if defined (XDEVL_PLATFORM_UNIX)
		lProc = (SHARED_FUNCTION)dlsym(m_module, pName);
		if(lProc == 0) {
			XDEVL_MODULEX_ERROR(XdevLSharedLibrary, "dlsym failed:"  << dlerror() << std::endl);
			return lProc;
		}
#elif defined (XDEVL_PLATFORM_WINDOWS) ||  defined(XDEVL_PLATFORM_MINGW)
		lProc = (SHARED_FUNCTION)(GetProcAddress(static_cast<HMODULE>(m_module), pName));
		if(lProc == 0) {
			XDEVL_MODULEX_ERROR(XdevLSharedLibrary, "GetProcAddress failed:"  << GetLastError() << std::endl);
			return lProc;
		}
#else
#error XDEVL_UNKNOWN_PLATFORM
#endif
		return lProc;
	}

	bool XdevLSharedLibrary::hasValidExtension(const XdevLFileName& str) {
		auto ext = str.getExtension();
		if(XdevLSharedLibrary::extension == ext) {
			return true;
		}

		return false;
	}
}
