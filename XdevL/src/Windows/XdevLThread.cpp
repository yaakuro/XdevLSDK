/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLThread.h>
#include <XdevLMutex.h>

#include <XdevLUtils.h>
#include <XdevLError.h>

#include <iostream>
#include <cassert>

namespace xdl {

	namespace thread {

		DWORD Thread::m_mainThreadID = 0;

		Thread::Thread() :
			m_thread(nullptr),
			m_arg(NULL) {
		}

		Thread::Thread(thread_callback_function func) {
			assert(func && "Thread::Thread: Invalid callback function");
			callbackFunction = func;
			StartUsingFuncPointer();
		}

		Thread::~Thread() {
			if (m_thread) {
				if (CloseHandle(m_thread) == 0) {
					std::cerr << "Thread::Can't close thread handle.\n";
				}
				else
					m_thread = NULL;
			}
		}

		int Thread::Start(ThreadArgument* arg) {
			Arguments(arg);
			m_thread = CreateThread(0, 0, Thread::ThreadProc, this, 0, &m_threadId);
			if (m_thread == NULL) {
				return 1;
			}
			m_isInterrupted = false;
			return 0;
		}

		void Thread::Stop() {
			m_isInterrupted = true;
		}

		bool Thread::IsInterrupted() {
			return m_isInterrupted;
		}

		int Thread::StartUsingFuncPointer() {
			m_thread = CreateThread(0, 0, Thread::ThreadProc2, this, 0, &m_threadId);
			if (m_thread == NULL) {
				return 1;
			}
			return 0;
		}


		int Thread::Join() {
			if (m_thread == 0)
				return 1;

			int ret = 0;
			if (WaitForSingleObject(m_thread, INFINITE) == WAIT_FAILED)
				ret = 1;
			return ret;
		}

		void Thread::Exit(int exitCode) {
			ExitThread((DWORD)exitCode);
		}

		unsigned long __stdcall Thread::ThreadProc(void* p_this) {
			Thread* ptr = static_cast<Thread*>(p_this);
			return static_cast<unsigned long>(ptr->RunThread(ptr->Arguments()));
		}

		unsigned long __stdcall Thread::ThreadProc2(void* p_this) {
			Thread* ptr = static_cast<Thread*>(p_this);
			return static_cast<unsigned long>(ptr->callbackFunction());
		}

		int Thread::RunThread(ThreadArgument* p_arg) {
			return 0;
		}

		ThreadArgument* Thread::Arguments() {
			return m_arg;
		}

		void Thread::Arguments(ThreadArgument* p_arg) {
			m_arg = p_arg;
		}

		void Thread::SetMainThread() {
			m_mainThreadID = GetCurrentThreadID();
		}

		DWORD Thread::GetCurrentThreadID() {
			return GetCurrentThreadId();
		}

		bool Thread::IsMainThread() {
			return GetCurrentThreadID() == m_mainThreadID;
		}
	}
}