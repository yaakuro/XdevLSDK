/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <tinyxml.h>
#include "XdevL.h"
#include "XdevLCore.h"
#include "XdevLDynLib.h"
#include "XdevLUtils.h"
#include "XdevLCommandLine.h"
#include "XdevLError.h"
#include "XdevLUserData.h"
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

//
// The procedure of XdevL looking for the main XdevLCore plugin.
//
// 1. If use specified a

namespace xdl {

	XdevLVersion XdevLVersion::zero {0,0,0};

	// Typedefs for creating and deleting the XdevLCore object.
	using DELETE_XDEVL_CORE = void (*)(XdevLModule* core);

	// Function pointer to the create function of the XdevLCore module.
	XdevLCreateModuleFunction create_xdevl_core = nullptr;

	// Function pointer to the delete function of the XdevLCore module.
	DELETE_XDEVL_CORE deleteXdevLCore = nullptr;

	// Holds the main XdevLCore XML filename.
	static XdevLFileName xmlFileName;

	// Holds the XdevLCore plugins version number.
	static XdevLFileName versionNumber;

	// Holds the XdevLCore plugins filename.
	static XdevLFileName pluginFilename;

	// Holds the path to the XdevLCore plugin.
	static XdevLFileName pluginPath(TEXT("."));

	static xdl_bool verbose = false;

	// Is used to load shared objects dynamically.
	XdevLSharedLibrary dynamicLibraryLoader;

	void setPluginPath(const xdl::XdevLFileName& path) {
		pluginPath = path;
	}


	//
	// Check the local path for the XdevLCore plugin.
	//
	xdl_int checkLocal(XdevLFileName& pluginFilename, XdevLSharedLibrary* sharedLibrary) {


		// Make platform dependent fileformat.
		pluginFilename.makePath();
		pluginPath.makePath();

		if(verbose) {
			std::cout << ">> XdevLCore plugins search information: " << std::endl;
			std::cout << ">> Version   : " << versionNumber << std::endl;
			std::cout << ">> Directory : " << pluginPath << std::endl;
			std::cout << ">> Trying to load ... ";
		}
		// Let's look if the XdevLCore plugin is in the local folder or the specified
		// by the user using command line or xml file.


		XDEVL_MODULEX_INFO(XdevL, "Checking if the plugin: " << pluginFilename << " is in the local folder.\n");
		std::ifstream fs;
		fs.open(pluginFilename.toString().c_str());
		if(fs.is_open()) {
			fs.close();
			if(sharedLibrary->open(pluginFilename.toString().c_str()) == RET_FAILED) {
				std::cerr << "Could not open library : " << pluginFilename << std::endl;
				exit(-1);
			} else {
				if(verbose) {
					std::cout << "Found and loaded.\n";
				}
			}
		} else {
			// It was not at the specified path. Let's check if the
			// 'XDEVL_PLUGINS' path is set as a environment variable.
			if(verbose) {
				XDEVL_MODULEX_INFO(XdevL, "not found.\n");

				std::cout << ">> Checking if XDEVL_PLUGINS environment variable is set ... ";
				XDEVL_MODULEX_INFO(XdevL, ">> Checking if XDEVL_PLUGINS environment variable is set ... \n");
			}

			if(getenv("XDEVL_PLUGINS") == nullptr) {
				XDEVL_MODULEX_WARNING(XdevL, "XDEVL_PLUGINS environment variable not set. Cant find plugin: " << pluginFilename << std::endl);
				exit(-1);
			}

			auto evnironment_variable = XdevLFileName(getenv("XDEVL_PLUGINS"));
			if(evnironment_variable.size() == 0) {
				std::cerr << "It is not set. Can not find: " <<  pluginFilename << std::endl;
				exit(-1);
			} else {
				if(verbose) {
					std::cout << "It is set." << std::endl;
				}
			}
			evnironment_variable.makePath();

			//
			// OK, the XDEVL_PLUGINS environment variable is set.
			// Let's check if the XdevLCore plugins is there.
			//
			if(verbose) {
				std::cout << ">> Trying to load from there ... ";
			}
			pluginPath = evnironment_variable + XdevLFileName(TEXT("/lib/"));
			pluginFilename = pluginPath + pluginFilename.getWithoutPath();

			fs.open(pluginFilename.toString().c_str());
			if(fs.is_open()) {
				fs.close();
				// File found in the local path. Than lets load it.
				if(sharedLibrary->open(pluginFilename.toString().c_str()) == RET_FAILED) {
					std::cerr << "Could not open plugin: '" << pluginFilename << "'... Abord program." << std::endl;
					exit(-1);
				} else {
					if(verbose) {
						std::cout << "Found and loaded." << std::endl;
					}
				}
			} else {
				std::cerr << "Not found  ... Abord program." << std::endl;
				exit(-1);
			}
		}
		return RET_SUCCESS;
	}

	//
	// Check the local path for the XdevLCore plugin.
	//
	xdl_int checkLocal(XdevLFileName& pluginFilename) {
		return checkLocal(pluginFilename, &dynamicLibraryLoader);
	}

	//
	// Creates the XdevLCore filename.
	//
	void createXdevLCoreFilename(const XdevLFileName& path, const XdevLFileName& version, XdevLFileName& result) {
		result = path + XdevLFileName(TEXT("/"));
#if XDEVL_PLATFORM_ANDROID
		result += XdevLFileName(TEXT("libXdevLCore-"));
#else
		result += XdevLFileName(TEXT("XdevLCore-"));
#endif
		result += version;
#ifdef XDEVL_DEBUG
		result += XdevLFileName(TEXT("d"));
#endif
		result += XdevLFileName(TEXT(".")) + XdevLSharedLibrary::extension;
	}

//
// Create XdevLCore functions.
//


	XdevLCore* createCore(xdl_int argc,
	                      xdl_char* argv[],
	                      xdl_int xmlBufferSize,
	                      xdl_uint8* xmlBuffer,
	                      xdl_uint numberOfUserData,
	                      XdevLUserData* userDataList[]) {

		std::stringstream vn;
		vn << XDEVL_MAJOR_VERSION << "." << XDEVL_MINOR_VERSION << "." << XDEVL_PATCH_VERSION;
		versionNumber = XdevLFileName(vn.str().c_str());

		//
		// All command line specified values will override values specified in the
		// xml file or default values. The following values can be overwritten.
		//
		//	- XML filename
		//	- XdevLPlugins plugins path
		// 	- XdevLCore plugins version number
		//

		xdl::XdevLCommandLineParser cmdParser(argc, argv);

		//
		// Set default parameters.
		//
		xdl::XdevLStringParameter versionNumberParameter(XdevLString(TEXT("v")), XdevLString(TEXT("The path to the plugins folder.")), xdl::XdevLParameter::ARGUMENT_NEEDED, &versionNumber);
		xdl::XdevLStringParameter pluginsPathParameter(XdevLString(TEXT("pp")), XdevLString(TEXT("The path to the plugins folder.")), xdl::XdevLParameter::ARGUMENT_NEEDED, &pluginPath);
		xdl::XdevLStringParameter fileNameParameter(XdevLString(TEXT("f")), XdevLString(TEXT("The filename of the XML file.")), xdl::XdevLParameter::ARGUMENT_NEEDED, &xmlFileName);

		// Parse the command line arguments.
		cmdParser.add(&versionNumberParameter);
		cmdParser.add(&pluginsPathParameter);
		cmdParser.add(&fileNameParameter);
		cmdParser.parse();

		std::cout << "XdevLCore     : " << versionNumber << std::endl;
		std::cout << "Plugins Folder: " << pluginPath << std::endl;
		std::cout << "Main XML file : " << xmlFileName << std::endl;

		//
		// Check if there is a main XML file where the user may have specified.
		// XdevLCore related information.
		// TODO The not specified comparision must be changed.
		if(xmlBuffer != nullptr) {

			TiXmlDocument xmlDocument;
			if(!xmlDocument.Parse((char*)xmlBuffer)) {
				std::cerr << "## Couldn't parse the specified XML file." << std::endl;
				exit(-1);
			}

			TiXmlHandle docHandle(&xmlDocument);
			TiXmlElement* root = docHandle.FirstChild("XdevLCoreProperties").ToElement();
			if(!root) {
				std::cerr << "## XML format not correct." << std::endl;
				exit(-1);
			}

			//
			// Do we have a version forces to use by the user?
			//
			if(!versionNumberParameter.getSet()) {
				if(root->Attribute("version")) {
					// Use the one from the xml file.
					versionNumber = XdevLFileName(root->Attribute("version"));
					std::cout << ">> Using version specified in XML file." << std::endl;
				}
			}
			if(!pluginsPathParameter.getSet()) {
				if(root->Attribute("plugins_path")) {
					// Use the one from the xml file.
					pluginPath = XdevLFileName(root->Attribute("plugins_path"));
					std::cout << ">> Using plugins folder specified in XML file." << std::endl;
				}
			}
		}

		//
		// Create the filename.
		//
		createXdevLCoreFilename(pluginPath, versionNumber, pluginFilename);

		// Load the XdevLCore plugin.
		xdl_int ret;
		if((ret = checkLocal(pluginFilename)) != RET_SUCCESS) {
			std::cerr << "## Could not find XdevLCore plugin at all." << std::endl;
			exit(-1);
		}


		//
		// Get the plugins create and delte function pointer.
		//
		create_xdevl_core = (XdevLCreateModuleFunction)(dynamicLibraryLoader.getFunctionAddress("_create"));
		if(!create_xdevl_core) {
			std::cerr << "## XdevLCore plugin method format wrong: CREATE_XDEVL_CORE not found." <<  std::endl;
			exit(-1);
		}
		deleteXdevLCore = (DELETE_XDEVL_CORE)(dynamicLibraryLoader.getFunctionAddress("_delete"));
		if(!deleteXdevLCore) {
			std::cerr << "## XdevLCore plugin method format wrong: DELETE_XDEVL_CORE not found." << std::endl;
			exit(-1);
		}

		//
		// Create the XdevLCore object.
		//
		XdevLUserData userData;

		userData.id = XdevLID("XDEVL_COMMAND_LINE_PARSER");
		userData.data = (void*)&cmdParser;


		XdevLModuleCreateParameter parameter;
		XdevLID CoreID("XdevLCoreMain");
		parameter.setPluginName(xdl::XdevLPluginName("XdevLCore"));
		parameter.setModuleName(xdl::XdevLModuleName("XdevLCore"));
		parameter.setUserParameter(&userData);
		parameter.setModuleId(CoreID);

		if(create_xdevl_core(&parameter) != RET_SUCCESS) {
			std::cerr << "## Couldn't create XdevLCore object." << std::endl;
			exit(-1);
		}
		XdevLCore* xdevlCoreObject = static_cast<XdevLCore*>(parameter.getModuleInstance());


		XdevLEvent moduleInit;
		moduleInit.type = XDEVL_MODULE_EVENT;
		moduleInit.module.sender = CoreID.getHashCode();
		moduleInit.module.event = XDEVL_MODULE_INIT;
		xdevlCoreObject->notify(moduleInit);

		//
		// Initialize the XdevLCore system.
		//
		XdevLCoreParameters parameters;
		parameters.xmlBuffer = std::vector<xdl_uint8>(xmlBuffer, xmlBuffer + xmlBufferSize);
		parameters.pluginsPath 		= pluginPath;
		parameters.userDataList 	= userDataList;
		parameters.numberOfUserData = numberOfUserData;

		if(xdevlCoreObject->setParameters(parameters) != RET_SUCCESS) {
			std::cerr << "## Couldn't initialize XdevLCore object." << std::endl;
			exit(-1);
		}

		return xdevlCoreObject;
	}


	xdl_int createCore(XdevLCore** core,
	                   xdl_int argc,
	                   xdl_char* argv[],
	                   xdl_uint numberOfUserData,
	                   XdevLUserData* userDataList[]) {

		*core = createCore(argc, argv, 0, nullptr, 0, nullptr);
		if(*core == nullptr) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int createCore(XdevLCore** core,
	                   xdl_int argc,
	                   xdl_char* argv[],
	                   xdl_int xmlBufferSize,
	                   xdl_uint8* xmlBuffer,
	                   xdl_uint numberOfUserData,
	                   XdevLUserData* userDataList[]) {

		*core = createCore(argc, argv, xmlBufferSize, xmlBuffer, numberOfUserData, userDataList);
		if(*core == nullptr) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int createCore(XdevLCore** core,
	                   xdl_int argc,
	                   xdl_char* argv[],
	                   const XdevLString& xmlBuffer,
	                   xdl_uint numberOfUserData,
	                   XdevLUserData* userDataList[]) {

		*core = createCore(argc, argv, static_cast<xdl_int>(xmlBuffer.toString().length()), (xdl::xdl_uint8*)xmlBuffer.toString().data(), numberOfUserData, userDataList);
		if(*core == nullptr) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int createCore(XdevLCore** core,
	                   xdl_int argc,
	                   xdl_char* argv[],
	                   const XdevLFileName& filename,
	                   xdl_uint numberOfUserData,
	                   XdevLUserData* userDataList[]) {

		xdl::XdevLString xmlBuffer;
		xdl::getXmlFile(filename, xmlBuffer);

		*core = createCore(argc, argv, static_cast<xdl_int>(xmlBuffer.toString().length()), (xdl::xdl_uint8*)xmlBuffer.toString().data(), numberOfUserData, userDataList);
		if(*core == nullptr) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

//
//
//

	XdevLModule* createModule(const XdevLPluginDescriptor& pluginDescriptor, XdevLModuleDescriptor& moduleDescriptor, const XdevLFileName& pluginPath) {

		XdevLSharedLibrary* sharedLibrary = new XdevLSharedLibrary();
		XdevLFileName pluginName;

		// Did the user specify a plugin folder?
		if(pluginPath != XdevLFileName()) {
			pluginName = pluginPath;
			pluginName += XdevLFileName(TEXT("/"));
		}

#ifdef XDEVL_PLATFORM_ANDROID
		pluginName += xdl::XdevLFileName(TEXT("lib"));
#endif
		pluginName += pluginDescriptor.getName();
		pluginName += XdevLFileName(TEXT("-"));
		pluginName += pluginDescriptor.getVersion().toString();
#ifdef XDEVL_DEBUG
		pluginName += XdevLFileName(TEXT("d"));
#endif
		pluginName += XdevLFileName(".") + XdevLSharedLibrary::extension;

		xdl_int ret;
		if((ret = checkLocal(pluginName, sharedLibrary)) != RET_SUCCESS) {
			std::cerr << "## Could not find XdevLCore plugin at all." << std::endl;
			exit(-1);
		}

		//
		// Get the plugins create and delte function pointer.
		//
		moduleDescriptor.create = (CREATE_XDEVL_MODULE)(sharedLibrary->getFunctionAddress("_createModule"));
		if(!moduleDescriptor.create) {
			return nullptr;
		}
		moduleDescriptor.destroy = (DELETE_XDEVL_MODULE)(sharedLibrary->getFunctionAddress("_delete"));
		if(!moduleDescriptor.destroy) {
			return nullptr;
		}

		moduleDescriptor.setLibrary(sharedLibrary);

		return moduleDescriptor.create(pluginDescriptor, moduleDescriptor);

	}

	xdl_int destroyCore(XdevLCore* core) {
		XDEVL_ASSERT(nullptr != core, "## Invalid argument!");

		// Create the message.
		xdl::XdevLEvent moduleShutdown;
		moduleShutdown.type = XDEVL_MODULE_EVENT;
		moduleShutdown.module.sender = 0;

		// Send the message to the core.
		core->sendEventTo(core->getID().getHashCode(), moduleShutdown);

		deleteXdevLCore(core);

		return RET_SUCCESS;
	}

//
//
//

	class XdevLMonolithicImpl : public XdevLMonolithic {
		public:

			xdl_int plug(const XdevLModuleName& moduleName, XdevLCreateModuleFunctionType createModuleFunctionType) {
				auto moduleID = XdevLID(moduleName);

				auto node = m_moduleNodes.find(moduleID.getHashCode());
				if(node != m_moduleNodes.end()) {
					return RET_FAILED;
				}

				m_moduleNodes[moduleID.getHashCode()] = XdevLModuleNode(createModuleFunctionType);
				return RET_SUCCESS;
			}

			std::shared_ptr<XdevLModule> create(const XdevLModuleName& moduleName, const xdl::XdevLID& id, std::shared_ptr<xdl::XdevLCore> core = nullptr) {
				auto moduleID = XdevLID(moduleName);

				auto node = m_moduleNodes.find(moduleID.getHashCode());
				if(node == m_moduleNodes.end()) {
					return nullptr;
				}

				xdl::XdevLModuleCreateParameter parameter;
				parameter.setModuleId(id);
				if(core != nullptr) {
					parameter.setModuleMediator(core.get());
				}

				std::shared_ptr<xdl::XdevLModule> tmp;
				if(node->second.create(&parameter, tmp) != xdl::RET_SUCCESS) {
					return nullptr;
				}

				return tmp;
			}

		private:

			std::map<xdl_uint64, XdevLModuleNode> m_moduleNodes;
	};

	XDEVL_EXPORT IPXdevLMonolithic createXdevLMonolithic() {
		auto tmp =  std::make_shared<XdevLMonolithicImpl>();
		return tmp;
	}

	xdl_int getXmlFile(const XdevLFileName& filename, XdevLString& xmlBuffer) {
		std::ifstream file(filename.toString().c_str());
		if(!file.is_open()) {
			return RET_FAILED;
		}
		std::stringstream ss;
		ss << file.rdbuf();
		xmlBuffer = XdevLString(ss.str().c_str());
		return RET_SUCCESS;
	}
}
