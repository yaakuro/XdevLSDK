if(WIN32)
	find_path(OPENAL_INCLUDE_DIR AL/al.h
	  HINTS
	  PATH_SUFFIXES include
	  PATHS
	 ${XDEVL_EXTERNALS}/openal-soft/include
	)

	find_library(OPENAL_LIBRARY
	  NAMES libOpenAL32.a libOpenAL32.dll.a OpenAL32.lib OpenAL32.dll
	  HINTS
	  PATH_SUFFIXES Debug
	  PATHS
	  ${XDEVL_EXTERNALS}/openal-soft/Debug
	)
elseif(APPLE)
	find_path(OPENAL_INCLUDE_DIR al.h
	 HINTS
    ENV OPENALDIR
		PATH_SUFFIXES include/AL include/OpenAL include
		PATHS
		~/Library/Frameworks
		/Library/Frameworks
		/sw # Fink
		/opt/local # DarwinPorts
		/opt/csw # Blastwave
		/opt
		${XDEVL_EXTERNALS}/openal-soft/include
	)

	find_library(OPENAL_LIBRARY
	  NAMES OpenAL al openal OpenAL32
		HINTS
    ENV OPENALDIR
		PATH_SUFFIXES lib64 lib libs64 libs ${_OpenAL_ARCH_DIR}
		PATHS
		~/Library/Frameworks
		/Library/Frameworks
		/sw
		/opt/local
		/opt/csw
		/opt
	  ${XDEVL_EXTERNALS}/openal-soft/Debug
	)
elseif(UNIX AND NOT APPLE)
	find_path(OPENAL_INCLUDE_DIR al.h
	  HINTS
		ENV OPENALDIR
	  PATH_SUFFIXES include/AL include/OpenAL include
	  PATHS
	)

	if(CMAKE_SIZEOF_VOID_P EQUAL 8)
	  set(_OpenAL_ARCH_DIR libs/Win64)
	else()
	  set(_OpenAL_ARCH_DIR libs/Win32)
	endif()

	find_library(OPENAL_LIBRARY
	  NAMES OpenAL al openal OpenAL32
	  HINTS
		ENV OPENALDIR
	  PATH_SUFFIXES lib64 lib libs64 libs ${_OpenAL_ARCH_DIR}
	  PATHS
	)

	unset(_OpenAL_ARCH_DIR)
endif()

if (OPENAL_INCLUDE_DIR AND OPENAL_LIBRARY)
	SET(OPENAL_FOUND "YES")
endif(OPENAL_INCLUDE_DIR AND OPENAL_LIBRARY)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(OPENAL  DEFAULT_MSG  OPENAL_LIBRARY OPENAL_INCLUDE_DIR)
mark_as_advanced(OPENAL_LIBRARY OPENAL_INCLUDE_DIR)
