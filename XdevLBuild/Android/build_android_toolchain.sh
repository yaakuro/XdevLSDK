#! /bin/bash

# Small script that helps to build and copy the android toolchain files

XDEVL_EXTERNALS=$XDEVL_HOME/../XdevLExternals
NDK_ROOT=$NDKROOT

ANDROID_ARCH=$1
ANDROID_TOOLCHAIN_FOLDER=$XDEVL_EXTERNALS/android/$ANDROID_ARCH
ANDROID_PLATFORM=$2

mkdir -p $XDEVL_EXTERNALS/android
$NDK_ROOT/build/tools/make-standalone-toolchain.sh --platform=$ANDROID_PLATFORM --arch=$ANDROID_ARCH --ndk-dir=$NDK_ROOT --install-dir=$ANDROID_TOOLCHAIN_FOLDER
