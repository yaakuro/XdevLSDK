set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)

set(TOOLCHAIN_FOLDER $ENV{XDEVL_HOME}/../XdevLExternals/android/mips)
set(TOOLCHAIN_ARCH mipsel-linux-android)

set(CMAKE_C_COMPILER ${TOOLCHAIN_FOLDER}/bin/${TOOLCHAIN_ARCH}-gcc)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_FOLDER}/bin/${TOOLCHAIN_ARCH}-g++)

set(CMAKE_FIND_ROOT_PATH ${TOOLCHAIN_FOLDER}/bin ${TOOLCHAIN_FOLDER}/${TOOLCHAIN_ARCH} ${TOOLCHAIN_FOLDER}/sysroot)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(PLATFORM_ANDROID TRUE)
set(PLATFORM_ANDROID_ARCH mips)
add_definitions(-DXDEVL_PLATFORM_ANDROID=1)
