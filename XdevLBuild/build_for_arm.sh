#! /bin/bash
mkdir build-android_armv7a
cd build-android_armv7a
../XdevLBuild/Android/build_android_toolchain.sh arm android-21
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=XdevLBuild/Android-armv7a-toolchain.cmake ..
make -j4
