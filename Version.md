# XdevL Versions

Here you can see the development of the XdevLSDK.

## Version 0.6.0
	- Add XdevLUI plugin for Graphical User Interface using XdevLRAI.
	- Add XdevLArchive for I/O handling. This will include changes in different API's.
	- Add XdevLConfig a basic config file handler.

## Version 0.5.4
  - Add native XdevLAudio methods.

## Version 0.5.3
  - Add XdevLOpenGLES context creation.
  - Add XdevLDirectoryWatcher on MacOSX platforms.

## Version 0.5.2
  - Rename XdevLOpenGL into XdevLRAI because it will mainly abstract rendering 3D accelerated stuff. [finished]
  - Remove some methods from the XdevLAudio API to make it more slim. [finished]

## Version 0.5.1
  - Separate OpenGL context creation from the XdevLOpenGL plugin. [finished]
  - Create native OpenGL context creation plugins, XdevLOpenGLContextGLX, XdevLOpenGLContextCocoa, XdevLOpenGLContextWGL etc. [finished]

##Version 0.5.0
  - Replace all strings and xdl_char by XdevLString. [finished]
  