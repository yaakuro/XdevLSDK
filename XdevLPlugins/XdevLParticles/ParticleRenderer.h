/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLERENDERER_H
#define XDEVL_PARTICLERENDERER_H

#include <XdevLError.h>

#include <XdevLParticles/ParticleEmitter.h>

namespace xdl {

	namespace particles {

		class ParticleRenderer {

			public:

				ParticleRenderer() {}

				virtual ~ParticleRenderer() {}

				virtual xdl_int Create(ParticleEmitter* pEmitter) {
					return RET_FAILED;
				}

				virtual void Render(ParticleEmitter* pEmitter) {
				}

		};
		
		using IPParticleRenderer = std::shared_ptr<ParticleRenderer>;
	}
}
#endif
