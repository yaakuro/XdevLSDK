/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLE_EMITTER_IMPL_H
#define XDEVL_PARTICLE_EMITTER_IMPL_H

#include <XdevLParticles/Particle.h>
#include <XdevLParticles/ParticleDynamic.h>
#include <XdevLParticles/ParticleGenerator.h>
#include <XdevLParticles/ParticleEmitter.h>
#include <XdevLParticles/ParticleDynamicEuler.h>

#include <cassert>

namespace xdl {

	namespace particles {

		/**
			@class ParticleEmitterImpl
			@brief Implementation of the ParticleEmitter class.
		*/
		class ParticleEmitterImpl : public ParticleEmitter {
			public:
				ParticleEmitterImpl(xdl_uint pNumParticles = 100,
				                    const tmath::vec3 pPos = tmath::vec3(0.0f, 0.0f, -10.0f),
				                    const tmath::vec3 pDir = tmath::vec3(0.0f, 1.0f, 0.0f), ParticleGenerator* pPGenerator = NULL) :
					mNumParticles(pNumParticles)
					, mPos(pPos)
					, mDir(pDir)
					, mParticleArray(nullptr)
					, mParticleSize(100.0f)
					, mVisibleParticles(0)
					, mGForce(tmath::vec3(0.0f, -9.81f, 0.0f))
					, mGravitation(true)
					, mColPlaneNormal(tmath::vec3(0.0f, 0.0f, 1.0f))
					, mColPos(tmath::vec3(0.0f, 0.0f, 0.0f))
					, mPDynamic(nullptr)
					, mPGenerator(pPGenerator) {

					mPDynamic = new ParticleDynamicEuler();

					CreateParticles(pNumParticles);

				}

				virtual ~ParticleEmitterImpl() {
					if(nullptr != mPDynamic) {
						delete mPDynamic;
						mPDynamic = nullptr;
					}

					if(nullptr != mParticleArray) {
						delete [] mParticleArray;
						mParticleArray = NULL;
					}
				}

				/// Set the maximum size of the emitted particle
				void SetParticleSize(xdl_float pSize) override final {
					mParticleSize = pSize;
				}

				/// Retrieve the maximum size of the emitted particle
				xdl_float GetParticleSize() const override final {
					return mParticleSize;
				}

				/// Set the position of the emitter in space
				void SetPos(const tmath::vec3& pPos) override final {
					mPos = pPos;
				}

				/// Set the direction of the emitter in space
				void SetDir(const tmath::vec3& pDir) override final;

				/// Retrieve the position of the emitter
				const tmath::vec3& GetPos() const override final {
					return mPos;
				}

				/// Return the direction of the emitter
				const tmath::vec3& GetDir() const override final {
					return mDir;
				}

				/// Return the number of particles
				xdl_int GetNumParticles() const override final {
					return mNumParticles;
				}

				/// Enable/Disable gravitation influence
				void SetGravitationState(bool pState) override final {
					mGravitation = pState;
				}

				/// Retrieve the gravitation influence state
				bool GetGravitationState() const override final {
					return mGravitation;
				}

				/// Set the gravitation vector
				void SetGravitation(const tmath::vec3& pValue) override final {
					mGForce = pValue;
				}

				/// Set the dynamic calculator of the particles
				void SetParticleDynamic(ParticleDynamic* pParticleDynamic) override final;

				/// Set the particle generator
				void SetParticleGenerator(ParticleGenerator* pParticleGenerator) override final;

				/// Create the particles
				void CreateParticles(xdl_uint pNumCParticles) override final;

				/// Update states of all particles
				void Update(xdl_float dT) override final;

				/// Retrieves a pointer to the particle array
				const Particle* GetArray() const override final {
					return mParticleArray;
				}

				Particle& operator[](xdl_uint index) {
					assert(index < mNumParticles);
					return mParticleArray[index];
				}

				void ResetSystem() override final {
					CreateParticles(mNumParticles);
				}

				xdl_uint GetNumVisParticle() const override final {
					return mVisibleParticles;
				}

			protected:

				xdl_uint mNumParticles;
				tmath::vec3 mPos;
				tmath::vec3 mDir;
				Particle*	mParticleArray;

				xdl_float mParticleSize;
				xdl_uint mVisibleParticles;

				tmath::vec3 mGForce;
				bool mGravitation;

				tmath::vec3 mColPlaneNormal;
				tmath::vec3 mColPos;

				ParticleDynamic* mPDynamic;
				ParticleGenerator* mPGenerator;
		};

	}
}
#endif
