/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLE_SERVER_H
#define XDEVL_PARTICLE_SERVER_H

#include <map>

#include <XdevLUtils.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLParticles/ParticleRenderer.h>

namespace xdl {

	namespace particles {

		struct Particle;
		class ParticleServer;
		class ParticleEmitter;

		/**
			@class ParticleServer
			@brief Manages multiple particle emitters.
		*/
		/*
			Create a instance of the Particle server, after that create dynamic equation for a emitter.
			After that create a generator method for the emitter. After that you can create the emitter
			\code
			ParticleServer* pserver = ParticleServer::GetInstance();
			ParticleDynamicEuler* dyn = new ParticleDynamicEuler();

			ParticleGeneratorBox* gbox = new ParticleGeneratorBox();

			tmath::vector3<float> emitterpos = tmath::vector3<float>(0.0f, 0.0f, 0.0f);
			tmath::vector3<float> emitterdir = tmath::vector3<float>(0.0f, 1.0f, 0.0f);
			ParticleEmitter* emitter = new ParticleEmitter(10, emitterpos, emitterdir, gbox);

			emitter->SetGravitationState(false);
			emitter->SetCParticleSize(pointsize);
			emitter->SetCParticleGenerator(gbox);
			emitter->ResetSystem();
			pserver->AddEmitter(emitter);


			\endcode

			You can see that we create 10 Particles, with a specified position and direction of the emitter.
			Than we setting some other parameters of the emitter and register it to the Particleserver.

		*/
		class ParticleServer {
			public:
				virtual ~ParticleServer() {}

				virtual xdl_int	Create(xdl::IPXdevLRAI rai) = 0;

				virtual bool AddEmitter(ParticleEmitter* pEmitter) = 0;

				virtual void Update(xdl_float dT) = 0;

				virtual xdl_int Render() = 0;
		};

		using IPParticleServer = std::shared_ptr<ParticleServer>;

	}
}
#endif
