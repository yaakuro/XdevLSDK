/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLE_GENERATOR_H
#define XDEVL_PARTICLE_GENERATOR_H

#include <XdevLParticles/Particle.h>

namespace xdl {

	namespace particles {

		class ParticleEmitter;

		class ParticleGenerator {
			public:
				ParticleGenerator();
				virtual ~ParticleGenerator() {};

				virtual void createParticle(Particle* particle, ParticleEmitter* pEmitter) = 0;
				virtual void createParticles(Particle** particles, ParticleEmitter* emitter) = 0;

			protected:

				tmath::vec3 mPos;
				tmath::vec3 mClipSpace;

		};

		inline xdl_float GetRandomMinMax(xdl_float pMin, xdl_float pMax) {
			// Retrieves a random float between 0.0f and 1.0f;
			xdl_float ran = static_cast<xdl_float>(rand()/(xdl_float)RAND_MAX);
			xdl_float tlength = fabs(pMax - pMin);
			return (pMin + ran*tlength);
		}
	}
}
#endif
