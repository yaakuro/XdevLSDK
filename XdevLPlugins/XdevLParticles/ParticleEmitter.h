/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLEEMITTER_H
#define XDEVL_PARTICLEEMITTER_H

#include <XdevLParticles/Particle.h>
#include <XdevLParticles/ParticleDynamic.h>
#include <XdevLParticles/ParticleGenerator.h>

#include <cassert>

namespace xdl {

	namespace particles {

		/**
			@class ParticleEmitter
			@brief Emitts particles.

			The emitter is responsible for the creation and simulation of the particles. To catch that
			problem he need 2 classes that helps him. First is the CParticleGenerator. This class is
			used to create particles in different ways. Maybe you want create particles on a surface
			of an sphere or on the surface of an cube.
			To calculate the new physical states, like position, velocity etc. you have to use the
			CParticleDynamic class. This is used to update the states of all particles.
		*/
		class ParticleEmitter {
			public:

				virtual ~ParticleEmitter() {}

				/// Set the maximum size of the emitted particle
				virtual void SetParticleSize(xdl_float pSize) = 0;

				/// Retrieve the maximum size of the emitted particle
				virtual xdl_float GetParticleSize() const = 0;

				/// Set the position of the emitter in space
				virtual void SetPos(const tmath::vec3& pPos) = 0;

				/// Set the direction of the emitter in space
				virtual void SetDir(const tmath::vec3& pDir) = 0;

				/// Retrieve the position of the emitter
				virtual const tmath::vec3& GetPos() const = 0;

				/// Return the direction of the emitter
				virtual const tmath::vec3& GetDir() const = 0;

				/// Return the number of particles
				virtual xdl_int GetNumParticles() const = 0;

				/// Enable/Disable gravitation influence
				virtual void SetGravitationState(bool pState) = 0;

				/// Retrieve the gravitation influence state
				virtual bool GetGravitationState() const = 0;

				/// Set the gravitation vector
				virtual void SetGravitation(const tmath::vec3& pValue) = 0;

				/// Set the dynamic calculator of the particles
				virtual void SetParticleDynamic(ParticleDynamic* pParticleDynamic) = 0;

				/// Set the particle generator
				virtual void SetParticleGenerator(ParticleGenerator* pParticleGenerator) = 0;

				/// Create the particles
				virtual void CreateParticles(xdl_uint pNumCParticles) = 0;

				/// Update states of all particles
				virtual void Update(xdl_float dT) = 0;

				/// Retrieves a pointer to the particle array
				virtual const Particle* GetArray() const = 0;

				/// Resets the emitter.
				virtual void ResetSystem() = 0;

				/// Returns the number of visible particles.
				virtual xdl_uint GetNumVisParticle() const = 0;
		};

		using IPParticleEmitter = std::shared_ptr<ParticleEmitter>;

	}
}
#endif
