/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLParticles/ParticleServerImpl.h"
#include "XdevLParticlesImpl.h"
#include "XdevLParticles/ParticleEmitterImpl.h"

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::particles::pluginName,
	xdl::particles::moduleNames,
	XDEVLPARTICLES_PLUGIN_MAJOR_VERSION,
	XDEVLPARTICLES_PLUGIN_MINOR_VERSION,
	XDEVLPARTICLES_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::particles::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::particles::moduleDescription,
	XDEVLPARTICLES_MODULE_MAJOR_VERSION,
	XDEVLPARTICLES_MODULE_MINOR_VERSION,
	XDEVLPARTICLES_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::particles::XdevLParticlesImpl, moduleDescriptor);
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

namespace xdl {

	namespace particles {

		XdevLParticlesImpl::XdevLParticlesImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
			: XdevLModuleImpl<XdevLParticles> (parameter, descriptor) {
		}

		XdevLParticlesImpl::~XdevLParticlesImpl() {}

		xdl_int XdevLParticlesImpl::init() {
			return RET_SUCCESS;
		}

		xdl_int XdevLParticlesImpl::shutdown() {
			return RET_SUCCESS;
		}

		IPParticleServer XdevLParticlesImpl::createParticleServer() {
			auto tmp = std::make_shared<ParticleServerImpl>();
			return tmp;
		}

		IPParticleEmitter XdevLParticlesImpl::createParticleEmitter() {
			auto tmp = std::make_shared<ParticleEmitterImpl>();
			return tmp;
		}

	}
}
