/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLE_RENDERER_RAI_H
#define XDEVL_PARTICLE_RENDERER_RAI_H

#include <XdevLRAI/XdevLRAI.h>
#include <XdevLParticles/ParticleRenderer.h>

namespace xdl {

	namespace particles {

		struct ParticleVertex {
			xdl_float x,y,z;
			xdl_float u,v;
			xdl_float r,g,b,a;
		};

		class ParticleRendererRAI : public ParticleRenderer {
			public:
				ParticleRendererRAI(xdl::IPXdevLRAI pRd);
				virtual ~ParticleRendererRAI();
				virtual void Render(ParticleEmitter* emitter);
				virtual xdl_int Create(ParticleEmitter* emitter);
			protected:
				xdl::IPXdevLRAI m_rai;
				xdl::IPXdevLVertexArray m_vertexArray;
				xdl::IPXdevLVertexBuffer m_vertexBuffer;
				xdl::IPXdevLVertexDeclaration m_vertexDeclaration;
		};
	}
}
#endif
