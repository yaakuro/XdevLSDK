/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLE_H
#define XDEVL_PARTICLE_H

#include <XdevLTypes.h>

#include <tm/tm.h>

namespace xdl {

	namespace particles {

		/**
			@class Particle
			@brief Holds the particles information.
		*/
		struct Particle {

			Particle() :
				position(tmath::vec3(0.0f, 0.0f, 0.0f))
				, velocity(tmath::vec3(0.0f, 0.0f, 0.0f))
				, mass(1.001f)
				, lifetime(0.0f)
				, age(0.0f)
				, acceleration(tmath::vec3(0.0f, 0.0f, 0.0f))
			{}

			/**
				@param pos Is the initial position.
				@param vel Is the initial velocity.
				@param ltime Is the lifetime of the.
				@param initialAge Is the initial age.
			*/
			Particle(const tmath::vec3& pos,
			         const tmath::vec3& vel,
			         xdl_float ltime,
			         xdl_float initialAge)
				: position(pos)
				, velocity(vel)
				, lifetime(ltime)
				, age(initialAge) {
			}

			tmath::vec3 position;
			tmath::vec3 velocity;
			xdl_float r, g, b, a;
			xdl_float mass;
			xdl_float lifetime;
			xdl_float age;
			tmath::vec3 acceleration;
		};
	}
}

#endif
