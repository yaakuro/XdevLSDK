/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLEGENERATORSPHERE_H
#define XDEVL_PARTICLEGENERATORSPHERE_H

#include <XdevLParticles/Particle.h>
#include <XdevLParticles/ParticleGenerator.h>


namespace xdl {

	namespace particles {

		enum eEmitterSphereType {
		  SPHERE,
		  HULL
		};

		class ParticleGeneratorSphere : public ParticleGenerator {
			public:
				ParticleGeneratorSphere() :
					mRadius(1.0f)
					, m_MaxLifeTime(10.0f)
					, m_MinLifeTime(4.0f)
					, mType(HULL) {
				}

				ParticleGeneratorSphere(const ParticleGeneratorSphere& pSrc) {
					mRadius = pSrc.mRadius;
				}

				virtual ~ParticleGeneratorSphere() {};
				void createParticle(Particle* particle, ParticleEmitter* pEmitter) override final;
				void createParticles(Particle** particles, ParticleEmitter* emitter) override final;

				void SetType(eEmitterSphereType pType) {
					mType = pType;
				}
				void SetSphereRadius(xdl_float pRadius) {
					mRadius = pRadius;
				}
				xdl_float GetSphereRadius() const {
					return mRadius;
				}
				void SetMaxLifeTime(xdl_float pMaxLifeTime) {
					m_MaxLifeTime = pMaxLifeTime;
				}
				void SetMinLifeTime(xdl_float pMinLifeTime) {
					m_MinLifeTime = pMinLifeTime;
				}

			protected:

				xdl_float mRadius;
				xdl_float m_MaxLifeTime;
				xdl_float m_MinLifeTime;
				eEmitterSphereType mType;
		};
	}
}
#endif
