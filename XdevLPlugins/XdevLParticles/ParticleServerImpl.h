/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLE_SERVER_IMPL_H
#define XDEVL_PARTICLE_SERVER_IMPL_H

#include <map>

#include <XdevLTypes.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLParticles/ParticleServer.h>

namespace xdl {

	namespace particles {

		/**
			@class ParticleServerImpl
			@brief Manages multiple particle emitters.
		*/
		class ParticleServerImpl : public ParticleServer {
			public:
				ParticleServerImpl();
				virtual ~ParticleServerImpl();

				xdl_int	Create(xdl::IPXdevLRAI rai);

				virtual bool AddEmitter(ParticleEmitter* pEmitter);

				virtual void Update(xdl_float dT);

				virtual xdl_int Render();

				using ParticleEmitterMap = std::map<xdl_int, ParticleEmitter*>;

			protected:

				bool AddEmitter(xdl_int pID, ParticleEmitter* pEmitter);

				ParticleEmitterMap					mEmitterMap;
				ParticleRenderer*					m_Renderer;

		};

	}
}
#endif
