/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLParticles/ParticleRendererRAI.h>

namespace xdl {

	namespace particles {

		ParticleRendererRAI::ParticleRendererRAI(xdl::IPXdevLRAI rai) : m_rai(rai) {
		}

		ParticleRendererRAI::~ParticleRendererRAI() {
		}

		xdl_int ParticleRendererRAI::Create(ParticleEmitter* emitter) {


			m_vertexArray = m_rai->createVertexArray();
			m_vertexBuffer = m_rai->createVertexBuffer();
			m_vertexBuffer->init(nullptr, sizeof(ParticleVertex) * emitter->GetNumParticles() * 6);

			m_vertexDeclaration = m_rai->createVertexDeclaration();

			m_vertexDeclaration->add(3, XdevLBufferElementTypes::FLOAT, 0);
			m_vertexDeclaration->add(4, XdevLBufferElementTypes::FLOAT, 4);
			m_vertexDeclaration->add(2, XdevLBufferElementTypes::FLOAT, 9);

			m_vertexArray->init(m_vertexBuffer, m_vertexDeclaration);

			return RET_SUCCESS;
		}
		void ParticleRendererRAI::Render(ParticleEmitter* emitter) {
//		m_RD->SetMatrixMode(xgw::PROJECTIONMATRIX);
//		m_RD->PushMatrix();
//		m_RD->SetMatrixMode(xgw::MODELVIEWMATRIX);
//		m_RD->PushMatrix();
//		m_RD->SetDepthState(false);
//		m_RD->SetBlendState(true);
//	//	m_RD->SetBlendFunc(xgw::BLEND_ONE_MINUS_DST_COLOR, xgw::BLEND_ONE_MINUS_SRC_COLOR);
//		m_RD->SetBlendFunc(xgw::BLEND_SRC_ALPHA, xgw::BLEND_ONE);

//			ParticleVertex* buffer = (ParticleVertex*)m_vertexBuffer->map(xdl::XDEVL_BUFFER_WRITE_ONLY);

			xdl_float mat[16] {};
			const Particle* particles = emitter->GetArray();
			tmath::vec3 vRight(mat[0], mat[4], mat[8]);
			tmath::vec3 vUp(mat[1], mat[5], mat[9]);
			tmath::vec3 vPoint0;
			tmath::vec3 vPoint1;
			tmath::vec3 vPoint2;
			tmath::vec3 vPoint3;
			tmath::vec3 vCenter;

			xdl_float fAdjustedSize = emitter->GetParticleSize()/300.0f;
			int idx = 0;

			std::vector<ParticleVertex> vertices(emitter->GetNumParticles() * 6);
			for(int a = 0; a < emitter->GetNumParticles(); a++) {

				vCenter.x = particles[a].position.x;
				vCenter.y = particles[a].position.y;
				vCenter.z = particles[a].position.z;

				vPoint0 = vCenter + ((-vRight - vUp) * fAdjustedSize);
				vPoint1 = vCenter + ((vRight - vUp) * fAdjustedSize);
				vPoint2 = vCenter + ((vRight + vUp) * fAdjustedSize);
				vPoint3 = vCenter + ((-vRight + vUp) * fAdjustedSize);

				vertices[idx + 0].x = vPoint0.x;
				vertices[idx + 0].y = vPoint0.y;
				vertices[idx + 0].z = vPoint0.z;
				vertices[idx + 0].u = 0.0f;
				vertices[idx + 0].v = 0.0f;
				vertices[idx + 0].r = particles[a].r;
				vertices[idx + 0].g = particles[a].g;
				vertices[idx + 0].b = particles[a].b;
				vertices[idx + 0].a = particles[a].a;


				vertices[idx + 1].x = vPoint1.x;
				vertices[idx + 1].y = vPoint1.y;
				vertices[idx + 1].z = vPoint1.z;
				vertices[idx + 1].u = 1.0f;
				vertices[idx + 1].v = 0.0f;
				vertices[idx + 1].r = particles[a].r;
				vertices[idx + 1].g = particles[a].g;
				vertices[idx + 1].b = particles[a].b;
				vertices[idx + 1].a = particles[a].a;

				vertices[idx + 2].x = vPoint2.x;
				vertices[idx + 2].y = vPoint2.y;
				vertices[idx + 2].z = vPoint2.z;
				vertices[idx + 2].u = 1.0f;
				vertices[idx + 2].v = 1.0f;
				vertices[idx + 2].r = particles[a].r;
				vertices[idx + 2].g = particles[a].g;
				vertices[idx + 2].b = particles[a].b;
				vertices[idx + 2].a = particles[a].a;


				vertices[idx + 3].x = vPoint2.x;
				vertices[idx + 3].y = vPoint2.y;
				vertices[idx + 3].z = vPoint2.z;
				vertices[idx + 3].u = 1.0f;
				vertices[idx + 3].v = 1.0f;
				vertices[idx + 3].r = particles[a].r;
				vertices[idx + 3].g = particles[a].g;
				vertices[idx + 3].b = particles[a].b;
				vertices[idx + 3].a = particles[a].a;

				vertices[idx + 4].x = vPoint3.x;
				vertices[idx + 4].y = vPoint3.y;
				vertices[idx + 4].z = vPoint3.z;
				vertices[idx + 4].u = 0.0f;
				vertices[idx + 4].v = 1.0f;
				vertices[idx + 4].r = particles[a].r;
				vertices[idx + 4].g = particles[a].g;
				vertices[idx + 4].b = particles[a].b;
				vertices[idx + 4].a = particles[a].a;

				vertices[idx + 5].x = vPoint0.x;
				vertices[idx + 5].y = vPoint0.y;
				vertices[idx + 5].z = vPoint0.z;
				vertices[idx + 5].u = 0.0f;
				vertices[idx + 5].v = 0.0f;
				vertices[idx + 5].r = particles[a].r;
				vertices[idx + 5].g = particles[a].g;
				vertices[idx + 5].b = particles[a].b;
				vertices[idx + 5].a = particles[a].a;

				idx+=6;

			}
			m_vertexBuffer->lock();
			m_vertexBuffer->upload((xdl_uint8*)vertices.data(), vertices.size() * 6 * sizeof(ParticleVertex));
			m_vertexBuffer->unlock();

			m_rai->setActiveVertexArray(m_vertexArray);
			m_rai->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLES, emitter->GetNumParticles() * 6);

		}

	}
}
