/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLEGENERATORBOX_H
#define XDEVL_PARTICLEGENERATORBOX_H

#include <XdevLParticles/Particle.h>
#include <XdevLParticles/ParticleGenerator.h>

namespace xdl {

	namespace particles {

		class ParticleGeneratorBox : public ParticleGenerator {
			public:
				ParticleGeneratorBox() {};
				ParticleGeneratorBox(const ParticleGeneratorBox& pSrc) {
					mSize = pSrc.mSize;
				}

				virtual ~ParticleGeneratorBox() {};

				virtual void createParticle(Particle& particle, ParticleEmitter* pEmitter) {
					tmath::vec3 v;
					xdl_float s = mSize*0.5f;

					v.x = GetRandomMinMax(-s, s);
					v.y = GetRandomMinMax(-s, s);
					v.z = GetRandomMinMax(-s, s);

					particle->pos 			= pEmitter->GetPos() + v;
					//	particle->Vel 			= tmath::vector3<float>(0.0f, 0.0f, 0.0f);//mDir* tmath::GetRandomMinMax(0.1f, 4.0f);
					particle->velocity 			= pEmitter->GetDir()*GetRandomMinMax(0.1f, 4.0f);
					particle->lifetime = GetRandomMinMax(2.0f, 10.0f);
					particle->age = 0.0f;
					particle->r = 0.3f;
					particle->g = 0.8f;
					particle->b = 0.3f;
					particle->a = 1.0f;
					particle->mass = GetRandomMinMax(1.0f, 1.0f);
				}
				
				void createParticles(Particle** particles, ParticleEmitter* emitter) override final {
					
				}


				void SetBoxSize(xdl_float pSize) {
					mSize = pSize;
				}

				xdl_float GetBoxSize() const {
					return mSize;
				}

			protected:
				xdl_float mSize;

		};
	}
}
#endif
