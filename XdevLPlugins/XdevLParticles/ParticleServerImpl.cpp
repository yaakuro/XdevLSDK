/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLParticles/ParticleServer.h>
#include <XdevLParticles/ParticleEmitter.h>
#include <XdevLParticles/ParticleRendererRAI.h>
#include <XdevLParticles/ParticleServerImpl.h>

namespace xdl {

	namespace particles {

		ParticleServerImpl::ParticleServerImpl() 
		: m_Renderer(nullptr) {

		}

		ParticleServerImpl::~ParticleServerImpl() {
			mEmitterMap.clear();
		}

		int	ParticleServerImpl::Create(xdl::IPXdevLRAI rai) {
			m_Renderer = new ParticleRendererRAI(rai);
			return 1;
		}

		void ParticleServerImpl::Update(xdl_float dT) {
			for(auto emitter : mEmitterMap) {
				emitter.second->Update(dT);
			}
		}

		bool ParticleServerImpl::AddEmitter(ParticleEmitter* emitter) {
			auto size = mEmitterMap.size();
			if(AddEmitter(size, emitter) == false) {
				XDEVL_MODULEX_ERROR(XdevLParticleServerImpl, "Could not add specified particle emitter.\n");
				return false;
			}
			return true;
		}

		bool ParticleServerImpl::AddEmitter(xdl_int pID, ParticleEmitter* emitter) {
			mEmitterMap.insert(ParticleEmitterMap::value_type(pID, emitter));
			if(m_Renderer) {
				m_Renderer->Create(emitter);
			}
			return true;
		}


		xdl_int ParticleServerImpl::Render() {

			for(auto emitter : mEmitterMap) {
				m_Renderer->Render(emitter.second);
			}
			return 0;
		}

	}
}
