/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PARTICLE_DYNAMIC_H
#define XDEVL_PARTICLE_DYNAMIC_H

#include <XdevLParticles/Particle.h>

namespace xdl {

	namespace particles {

		/**
				@class ParticleDynamic
				@brief Calculates the new state of a particle.

				@code
				class MyDyn : public ParticleDynamic
				{
					MyDyn() {}
					~MyDyn() {}
					virtual void UpdateParticle(Particle& particle, float dT)
					{
						Particle.mCurrtime += dT;
						particle.mPos.x = particle.mPos.x + particle.mVel.x*dT;
						particle.mPos.y = particle.mPos.y + particle.mVel.y*dT;
						particle.mPos.z = particle.mPos.z + particle.mVel.z*dT;
						particle.mA = 1.0 - (1.0f/particle.mLifetime)*particle.mCurrtime ;
					}
				};
				...

				MyDyn dyn;

				@endcode
		*/
		class ParticleDynamic {
			public:
				ParticleDynamic();
				virtual ~ParticleDynamic();

				/// Updates a particle.
				virtual void UpdateParticle(Particle& particle, xdl_float dT);

			protected:
		};

	}
}

#endif
