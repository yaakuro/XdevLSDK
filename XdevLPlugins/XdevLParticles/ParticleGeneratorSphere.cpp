/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLParticles/ParticleGeneratorSphere.h>
#include <XdevLParticles/ParticleEmitter.h>

namespace xdl {

	namespace particles {

		void ParticleGeneratorSphere::createParticle(Particle* particle, ParticleEmitter* emitter) {
			tmath::vec3 v;
			switch(mType) {
				case HULL: {
					xdl_float phi = GetRandomMinMax((xdl_float)-static_cast<float>(M_PI), static_cast<float>(M_PI));
					xdl_float theta = GetRandomMinMax((xdl_float)-static_cast<float>(M_PI)*0.5f,(xdl_float) static_cast<float>(M_PI)*0.5f);

					v.x = mRadius*cos(phi)*cos(theta);
					v.y = mRadius*sin(phi)*cos(theta);
					v.z = mRadius*sin(theta);
				}
				break;
				case SPHERE: {

					xdl_float phi = GetRandomMinMax((xdl_float)-M_PI, (xdl_float)M_PI);
					xdl_float theta = GetRandomMinMax((xdl_float)-M_PI*0.5f,(xdl_float) M_PI*0.5f);
					xdl_float radius = GetRandomMinMax(0.01f, mRadius);

					v.x = radius*cos(phi)*cos(theta);
					v.y = radius*sin(phi)*cos(theta);
					v.z = radius*sin(theta);

				}
				break;
			}

			particle->position = emitter->GetPos() + v;
			tmath::normalize(v);
			particle->velocity = v*0.1f;
			particle->lifetime = GetRandomMinMax(m_MinLifeTime ,m_MaxLifeTime);
			particle->age = 0.0f;
			particle->r = 0.7f;
			particle->g = 0.4f;
			particle->b = 0.2f;
			particle->a = 1.1f;
			particle->mass = 1.1f;

		}

		void ParticleGeneratorSphere::createParticles(Particle** particles, ParticleEmitter* emitter) {

		}
	}
}
