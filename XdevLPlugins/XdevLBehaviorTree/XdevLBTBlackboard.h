/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BT_BLACKBOARD_H
#define XDEVL_BT_BLACKBOARD_H

#include <unordered_map>

namespace xdl {

	namespace bt {

		class XdevLBTBlackboard {
			public:

				XDEVL_INLINE void setBoolValue(const XdevLString& key, xdl_bool value) {
					m_boolValues[key] = value;
				}

				XDEVL_INLINE xdl_bool getBoolValue(const XdevLString& key) {
					auto iterator = m_boolValues.find(key);
					if(iterator == m_boolValues.end()) {
						iterator->second = false;
					}
					return iterator->second;
				}

				XDEVL_INLINE void setIntValue(const XdevLString& key, xdl_int value) {
					m_intValues[key] = value;
				}

				XDEVL_INLINE xdl_int getIntValue(const XdevLString& key) {
					auto iterator = m_intValues.find(key);
					if(iterator == m_intValues.end()) {
						iterator->second = false;
					}
					return iterator->second;
				}

				void setFloatValue(const XdevLString& key, xdl_float value) {
					m_floatValues[key] = value;
				}

				XDEVL_INLINE xdl_float getFloatValue(const XdevLString& key) {
					auto iterator = m_floatValues.find(key);
					if(iterator == m_floatValues.end()) {
						iterator->second = 0.0f;
					}
					return iterator->second;
				}

				XDEVL_INLINE void setStringValue(const XdevLString& key, const XdevLString& value) {
					m_stringValues[key] = value;
				}

				XDEVL_INLINE XdevLString getStringValue(const XdevLString& key) {
					auto iterator = m_stringValues.find(key);
					if(iterator == m_stringValues.end()) {
						iterator->second = "";
					}
					return iterator->second;
				}

				XDEVL_INLINE void setObjectValue(const XdevLString& key, void* value) {
					m_objectValues[key] = value;
				}

				XDEVL_INLINE void* getObjectValue(const XdevLString& key) {
					auto iterator = m_objectValues.find(key);
					if(iterator == m_objectValues.end()) {
						iterator->second = nullptr;
					}
					return iterator->second;
				}

			private:

				std::unordered_map<XdevLString, xdl_bool> m_boolValues;
				std::unordered_map<XdevLString, xdl_int> m_intValues;
				std::unordered_map<XdevLString, xdl_float> m_floatValues;
				std::unordered_map<XdevLString, XdevLString> m_stringValues;
				std::unordered_map<XdevLString, void*> m_objectValues;
		};

	}
}

#endif
