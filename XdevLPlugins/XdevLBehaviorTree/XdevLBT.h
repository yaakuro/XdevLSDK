/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BT_H
#define XDEVL_BT_H

#include <XdevLBehaviorTree/XdevLBTNode.h>

namespace xdl {

	namespace bt {

		/**
		 * @class XdevLBT
		 * @brief The main class for a behavior tree.
		 */
		class XdevLBT {
			public:

				XdevLBT(XdevLBTBlackboard* blackboard)
					: m_root(nullptr)
					, m_blackboard(blackboard) {
				}

				/// Sets the root node of this behavior tree.
				void setRoot(XdevLBTNode* node) {
					XDEVL_ASSERT(node, "Specified root node is not valid.");
					XDEVL_ASSERT(m_blackboard, "No blackboard assigned to the behavior tree.");

					m_root = node;
					m_root->setBlackboard(getBlackboard());
				}

				/// Starts this behavior tree.
				XdevLBTStatus tick(xdl_float dT) {
					XDEVL_ASSERT(m_root, "No root node assigned.");

					return m_root->tick(dT);
				}

				/// Returns the blackboard assigned to the behavior tree.
				XDEVL_INLINE XdevLBTBlackboard* getBlackboard() {
					XDEVL_ASSERT(m_blackboard, "No blackboard assigned to the behavior tree.");

					return m_blackboard;
				}

				/// Sets the blackboard for this behavior tree.
				XDEVL_INLINE void setBlackboard(XdevLBTBlackboard* blackboard) {
					m_blackboard = blackboard;
				}

			private:

				XdevLBTNode* m_root;

				XdevLBTBlackboard* m_blackboard;
		};
	}
}
#endif
