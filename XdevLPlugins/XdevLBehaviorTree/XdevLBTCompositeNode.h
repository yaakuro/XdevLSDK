/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BT_COMPOSITE_NODE_H
#define XDEVL_BT_COMPOSITE_NODE_H

#include <XdevLBehaviorTree/XdevLBTNode.h>

namespace xdl {

	namespace bt {
		/**
		 * @class XdevLBTCompositeNode
		 * @brief Base class for Composite nodes.
		 */
		class XdevLBTCompositeNode : public XdevLBTNode {
			public:
				XdevLBTCompositeNode()
					: XdevLBTNode() {

				}

				using Super = XdevLBTNode;

				/// Adds a child to this node.
				void addChild(XdevLBTNode* node) {
					auto iterator = std::find(m_childNodes.begin(), m_childNodes.end(), node);
					if(m_childNodes.end() != iterator) {
						return;
					}
					node->setParent(this);
					m_childNodes.push_back(node);
				}

				/// Removes a child from this node.
				void removeChild(XdevLBTNode* node) {
					auto iterator = std::find(m_childNodes.begin(), m_childNodes.end(), node);
					if(m_childNodes.end() != iterator) {
						return;
					}
					(*iterator)->setParent(nullptr);
					m_childNodes.erase(iterator);
				}

				/// Returns the list of childs.
				XDEVL_INLINE std::vector<XdevLBTNode*>& getChildNodesList() {
					return m_childNodes;
				}

			protected:

				void setBlackboard(XdevLBTBlackboard* blackboard) override {
					Super::setBlackboard(blackboard);

					for(auto& child : getChildNodesList()) {
						child->setBlackboard(blackboard);
					}
				}

			private:

				std::vector<XdevLBTNode*> m_childNodes;
		};

	}
}

#endif
