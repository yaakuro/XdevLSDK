/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BT_DECORATOR_NODE_H
#define XDEVL_BT_DECORATOR_NODE_H

#include <XdevLBehaviorTree/XdevLBTNode.h>

namespace xdl {

	namespace bt {

		/**
		 * @class XdevLBTDecoratorNode
		 * @brief A decorator class for the behavior tree.
		 */
		class XdevLBTDecoratorNode : public XdevLBTNode {
			public:
				XdevLBTDecoratorNode()
					: m_child(nullptr) {

				}

				using Super = XdevLBTNode;

				/// Sets the child of this node.
				void setChild(XdevLBTNode* child) {
					m_child = child;
				}

				/// Returns if this decorator has a child or not.
				xdl_bool hasChild() {
					return (nullptr != m_child);
				}

			protected:

				void setBlackboard(XdevLBTBlackboard* blackboard) override {
					Super::setBlackboard(blackboard);

					if(m_child) {
						m_child->setBlackboard(blackboard);
					}
				}

				XdevLBTStatus tick(xdl_float dT) override {
					if(m_child) {
						return m_child->tick(dT);
					}
					return XdevLBTStatus::Failed;
				}

			private:

				XdevLBTNode* m_child;
		};









	}
}

#endif
