/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BT_SELECTOR_NODE_H
#define XDEVL_BT_SELECTOR_NODE_H

#include <XdevLBehaviorTree/XdevLBTCompositeNode.h>

namespace xdl {

	namespace bt {

		/**
		 * @class XdevLBTSelectorNode
		 * @brief The Selector Node of the Behavior Tree.
		 */
		class XdevLBTSelectorNode : public XdevLBTCompositeNode {
			public:

				using Super = XdevLBTCompositeNode;

			private:

				XdevLBTStatus tick(xdl_float dT) override {

					// Go through all childs until one succeeds.
					for(auto& child : getChildNodesList()) {
						auto result = child->tick(dT);
						if( (XdevLBTStatus::Success == result) || (XdevLBTStatus::Running == result) ) {
							return result;
						}
					}
					return XdevLBTStatus::Failed;
				}
		};



	}
}

#endif
