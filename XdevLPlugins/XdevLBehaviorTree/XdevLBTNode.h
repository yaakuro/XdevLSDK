/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BT_NODE_H
#define XDEVL_BT_NODE_H

#include <XdevLBehaviorTree/XdevLBT.h>
#include <XdevLBehaviorTree/XdevLBTBlackboard.h>

#include <unordered_map>
#include <algorithm>

namespace xdl {

	namespace bt {

		class XdevLBT;

		/**
			* @enum XdevLBTNodeStatus
			* @brief Status of an Behavior task, decorator.
			*/
		enum class XdevLBTStatus {
			Invaild,
			Success,
			Failed,
			Running
		};

		/**
		 * @class XdevLBTNode
		 * @brief Base node class for Behavior Trees
		 */
		class XdevLBTNode {
			public:
				friend class XdevLBT;
				friend class XdevLBTCompositeNode;
				friend class XdevLBTDecoratorNode;
				friend class XdevLBTSelectorNode;
				friend class XdevLBTSequenceNode;
				friend class XdevLBTTaskNode;

				using Super = XdevLBTNode;

				XdevLBTNode()
					: m_parentNode(nullptr)
					, m_blackboard(nullptr) {

				}

				/// Sets the parent of this node.
				void setParent(XdevLBTNode* node) {
					m_parentNode = node;
				}

				/// Returns the name of the node.
				XDEVL_INLINE XdevLString getName() {
					return m_name;
				}

				/// Sets the name of the node.
				XDEVL_INLINE void setName(const XdevLString& name) {
					m_name = name;
				}

				/// Returns the blackboard assigned to the node.
				XdevLBTBlackboard* getBlackboard() {
					return m_blackboard;
				}

			protected:

				/// Sets the blackboard for this node.
				virtual void setBlackboard(XdevLBTBlackboard* blackboard) {
					XDEVL_ASSERT(blackboard, "Invalid blackboard object");

					m_blackboard = blackboard;
				}

				/// Removes the assigned blackboard from this node.
				XDEVL_INLINE void removeBlackboard() {
					m_blackboard = nullptr;
				}

				/// Ticks this node.
				virtual XdevLBTStatus tick(xdl_float dT) {
					return XdevLBTStatus::Success;
				}

			private:

				XdevLBTNode* m_parentNode;
				XdevLBTBlackboard* m_blackboard;

				XdevLString m_name;
		};
	}
}

#endif
