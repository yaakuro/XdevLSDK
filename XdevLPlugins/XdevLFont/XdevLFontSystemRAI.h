/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of 
	this software and associated documentation files (the "Software"), to deal in the 
	Software without restriction, including without limitation the rights to use, copy, 
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
	and to permit persons to whom the Software is furnished to do so, subject to the 
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies 
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_FONT_SYSTEM_RAI_H
#define XDEVL_FONT_SYSTEM_RAI_H

#include <XdevLPluginImpl.h>
#include <XdevLUtils.h>
#include <XdevLFont/XdevLFontSystem.h>
#include <XdevLRAI/XdevLRAI.h>

#include <list>

namespace xdl {

	class XdevLFontRAI;

	class XdevLFontSystemRAI : public XdevLModuleImpl<XdevLFontSystem> {
		public:

			XdevLFontSystemRAI(XdevLModuleCreateParameter* parameter,  const XdevLModuleDescriptor& descriptor);

			virtual ~XdevLFontSystemRAI();
			virtual xdl_int create(const XdevLFontSystemCreateParameter& parameter) override;
			virtual xdl_int shutdown() override;
			virtual IPXdevLFont createFromFontFile(IPXdevLFile& file) override;
			virtual IPXdevLFont createFontFromTexture(IPXdevLFile& file, IPXdevLTexture texture) override;
			virtual void setCreateTextureCallback(XdevLFontSystem::createTextureFromFileCallbackFunction function) override;
			virtual xdl_uint getScreenWidth() const override;
			virtual xdl_uint getScreenHeight() const override;

		private:
			XdevLGlyphMetric& readLine(IPXdevLFile& file, XdevLGlyphMetric& gp);
			void calculateGlyphInformation(XdevLFontRAI* font, IPXdevLFile& file);
		private:
			xdl_uint screenWidth;
			xdl_uint screenHeight;
			IPXdevLRAI m_rai;
			XdevLFontSystem::createTextureFromFileCallbackFunction createTextureFromFile;
			std::list<IPXdevLFont> m_fonts;
	};

}

#endif // XDEVL_FONT_SYSTEM_H
