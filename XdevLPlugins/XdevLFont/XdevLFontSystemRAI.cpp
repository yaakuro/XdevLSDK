/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLFont/XdevLFontSystemRAI.h>
#include <XdevLFileSystem/XdevLArchive.h>

#include "XdevLFontRAI.h"
#include "lodepng.h"

#include <iostream>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cassert>

namespace xdl {

	XdevLFontSystemRAI::XdevLFontSystemRAI(XdevLModuleCreateParameter* parameter,  const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLFontSystem>(parameter,  descriptor),
		screenWidth(0),
		screenHeight(0),
		m_rai(nullptr),
		createTextureFromFile(nullptr) {
	}

	XdevLFontSystemRAI::~XdevLFontSystemRAI() {
	}

	xdl_uint XdevLFontSystemRAI::getScreenWidth() const {
		return screenWidth;
	}

	xdl_uint XdevLFontSystemRAI::getScreenHeight() const {
		return screenHeight;
	}


	xdl_int XdevLFontSystemRAI::create(const XdevLFontSystemCreateParameter& parameter) {
		m_rai = parameter.rai;
		m_rai->getDescriptor().registerDependency(this);
		return RET_SUCCESS;
	}

	xdl_int XdevLFontSystemRAI::shutdown() {
		return RET_SUCCESS;
	}


	void XdevLFontSystemRAI::setCreateTextureCallback(XdevLFontSystem::createTextureFromFileCallbackFunction function) {
		assert(function && " XdevLFontImpl::setCreateTextureCallback: Parameter not valid.");
		createTextureFromFile = function;
	}

	IPXdevLFont XdevLFontSystemRAI::createFromFontFile(IPXdevLFile& file) {
		assert(m_rai && " XdevLFontImpl::createFromFontFile: XdevLFontSystem not initialized.");

		std::vector<std::string> fileNames;
		xdl_int numberOfTextures = 1;
		xdl_uint fontSize = 0;

		XdevLString tmp;
		file->readStringLine(tmp);
		file->readStringLine(tmp);

		// Get font size.
		file->readStringLine(tmp);
		std::stringstream ss(tmp.toString());
		ss >> fontSize;

		// Create the font instance.
		auto font = new XdevLFontRAI();
		font->setFontSize(fontSize);

		// Reset stringstream.
		ss.str("");
		ss.clear();

		file->readStringLine(tmp);
		ss << tmp;
		ss >> numberOfTextures;


		IPXdevLTexture texture = nullptr;

		auto archive = file->getArchive();

		for(auto id = 0; id < numberOfTextures; id++) {
			XdevLFileName filename;
			file->readStringLine(filename);
			filename.trim();
		
			// Did the user specify a external functions to create a texture out of an image file?
			if(createTextureFromFile) {
				auto file = archive->open(XdevLOpenForReadOnly(), XdevLFileName(filename));
				// Yes, they use that to create the texture.
				texture = createTextureFromFile(file.get());

			} else {
				// No, let's use the lodepng project import PNG files.
				std::vector<xdl_uint8> imageIn, imageOut;
				auto newFile = archive->open(XdevLOpenForReadOnly(), filename);
				if(nullptr == newFile) {
					XDEVL_MODULEX_ERROR(XdevLFontSystem, "Could not find: " << filename.toString() << std::endl);
					return nullptr;
				}
				imageIn.reserve((size_t)newFile->length());
				imageIn.resize((size_t)newFile->length());

				newFile->read(imageIn.data(), newFile->length());
				newFile->close();

				xdl_uint width, height;

				xdl_int error = lodepng::decode(imageOut, width, height, imageIn);
				if(error) {
					std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
					return std::shared_ptr<XdevLFontRAI>();
				}

				// This flipping is neccessary because the library flips the picture up side down.
				// Method is brute force and needs unessarly memory. It creates a vector the same size
				// and starts copying which is not effective at all.
//				std::vector<xdl_uint8> vflipedimage(imageOut);
//				for(xdl_uint y = 0; y != height; y++) {
//					for(xdl_uint x = 0; x != width*4; x++) {
//						vflipedimage[x + y*width*4] = imageOut[x + (height - 1 - y)*width*4];
//					}
//				}

				texture = m_rai->createTexture();

				texture->init(width, height, XdevLTextureFormat::RGBA8, XdevLTexturePixelFormat::RGBA, imageOut.data());
				imageOut.clear();
				imageOut.clear();

			}
			if(texture == nullptr) {
				XDEVL_MODULEX_ERROR(XdevLFontSystemRAI, "XdevLFontImpl::importFromFontFile: Could not create texture: " << filename << std::endl);
				assert(texture && "FontTexture not created.");
			}

			const XdevLTextureSamplerFilterStage filterStage {XdevLTextureFilterMin::NEAREST, XdevLTextureFilterMag::NEAREST};
			const XdevLTextureSamplerWrap wrap {XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE};

			texture->lock();
			texture->setTextureFilter(filterStage);
			texture->setTextureWrap(wrap);
			texture->unlock();

			font->addFontTexture(texture);

		}

		calculateGlyphInformation(font, file);

		return std::shared_ptr<XdevLFontRAI>(font);
	}


	IPXdevLFont XdevLFontSystemRAI::createFontFromTexture(IPXdevLFile& file, IPXdevLTexture texture) {
		assert(m_rai && " XdevLFontImpl::createFontFromTexture: XdevLFontSystem not initialized.");

		auto font = std::shared_ptr<XdevLFontRAI>(new XdevLFontRAI());

		IPXdevLTexture tx = texture;
		font->addFontTexture(texture);

		const XdevLTextureSamplerFilterStage filterStage {XdevLTextureFilterMin::NEAREST, XdevLTextureFilterMag::NEAREST};
		const XdevLTextureSamplerWrap wrap {XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE};

		tx->lock();
		tx->setTextureFilter(filterStage);
		tx->setTextureWrap(wrap);
		tx->unlock();


		// One unit on the screen for x,y [-1 , 1]
		font->setUnitX(2.0f/(xdl_float)screenWidth);
		font->setUnitY(2.0f/(xdl_float)screenHeight);

		xdl_uint numberOfTextures = 1;
		xdl_uint fontSize = 0;


		XdevLString tmp;
		file->readStringLine(tmp);
		file->readStringLine(tmp);

		file->readStringLine(tmp);
		std::stringstream ss(tmp.toString());
		ss >> numberOfTextures;
		ss.str("");
		ss.clear();

		// Get font size.
		file->readStringLine(tmp);
		ss << tmp;
		ss >> fontSize;

		font->setFontSize(fontSize);

		xdl_float numCols 	= (xdl_float)tx->getWidth()/(xdl_float)fontSize;

		// TODO Using maps to handle id of the glyphs? At the moment it is just a hack.
		while(file->tell() != file->length()) {

			XdevLGlyphMetric gp;
			readLine(file, gp);

			//
			// TODO This part to find the position of the glyph in the texture is some sort of hack.
			// Make it so that all of the information is the the fontInfo.txt file.
			//
			xdl_uint idx = gp.unicode - 32;

			xdl_float pos_x = static_cast<xdl_float>(idx % (xdl_int)numCols);
			xdl_float pos_y = static_cast<xdl_float>(idx / (xdl_int)numCols);

			xdl_float ds = 1.0f/(xdl_float)tx->getWidth()*gp.width;
			xdl_float dt = 1.0f/(xdl_float)tx->getWidth()*gp.height;

			xdl_float s = 1.0f/numCols*pos_x ;
			xdl_float t = 1.0f - 1.0f/numCols*pos_y - (1.0f/(xdl_float)tx->getHeight())*((fontSize - gp.height - gp.top));

			//
			// Add an offset of x,y pixel offset to the s,t coordinates.
			//
			xdl_float offset_x = 0.0f/(xdl_float)tx->getWidth();
			xdl_float offset_y = 0.0f/(xdl_float)tx->getHeight();


			gp.vertices[0].x = gp.brearing_x;
			gp.vertices[0].y = (gp.height - gp.brearing_y);
			gp.vertices[0].s = s - offset_x;
			gp.vertices[0].t = t - dt - offset_y;

			gp.vertices[1].x = gp.brearing_x;
			gp.vertices[1].y = gp.brearing_y;
			gp.vertices[1].s = s - offset_x;
			gp.vertices[1].t = t + offset_y;

			gp.vertices[2].x = gp.brearing_x + gp.width;
			gp.vertices[2].y = gp.brearing_y;
			gp.vertices[2].s = s + ds + offset_x;
			gp.vertices[2].t = t + offset_y;

			gp.vertices[3].x = gp.brearing_x + gp.width;
			gp.vertices[3].y = (gp.height - gp.brearing_y);
			gp.vertices[3].s = s + ds + offset_x;
			gp.vertices[3].t = t - dt - offset_y;

			//
			// Find maximum value for the new line.
			//
			font->setNewLineSize(std::max(font->getNewLineSize(), gp.height));

			font->addGlyph(gp);

		}
		return font;

	}

	XdevLGlyphMetric& XdevLFontSystemRAI::readLine(IPXdevLFile& file, XdevLGlyphMetric& gp) {
		XdevLString tmp;
		file->readStringLine(tmp);

		// TODO Maybe not use stringstreams?
		std::stringstream ss(tmp.toString());

		ss >> gp.texture_id
		   >> gp.unicode
		   >> gp.left
		   >> gp.top
		   >> gp.width
		   >> gp.height
		   >> gp.advance_h
		   >> gp.advance_v
		   >> gp.brearing_x
		   >> gp.brearing_y
		   >> gp.vertices[0].s
		   >> gp.vertices[0].t
		   >> gp.vertices[3].s
		   >> gp.vertices[3].t;

		return gp;
	}

	void XdevLFontSystemRAI::calculateGlyphInformation(XdevLFontRAI* font, IPXdevLFile& file) {

		xdl_uint	count = 0;

		// TODO Using maps to handle id of the glyphs? At the moment it is just a hack.
		while(file->tell() != file->length()) {

			XdevLGlyphMetric gp;
			readLine(file, gp);

			//
			// Get the info for the glyph.
			//

			IPXdevLTexture tx = font->getTexture(gp.texture_id);

			//
			// Add an offset of x,y pixel offset to the x,y coordinates.
			// What this doesn is expanding the size of the quad in the same amound
			//
			xdl_float offset_sx = 0.0;
			xdl_float offset_sy = 0.0;

			//
			// Convert the pixel units into texture coordinate units
			//
			xdl_float s1 = (1.0f/(xdl_float)tx->getWidth())*gp.vertices[0].s;
			xdl_float t1 = (1.0f/(xdl_float)tx->getWidth())*gp.vertices[0].t;

			xdl_float s2 = (1.0f/(xdl_float)tx->getWidth())*gp.vertices[3].s;
			xdl_float t2 = (1.0f/(xdl_float)tx->getWidth())*gp.vertices[3].t;


			gp.vertices[0].x = gp.brearing_x - offset_sx;
			gp.vertices[0].y = (gp.height - gp.brearing_y) - offset_sy;
			gp.vertices[0].s = s1;
			gp.vertices[0].t = t2;

			gp.vertices[1].x = gp.brearing_x - offset_sx;
			gp.vertices[1].y = gp.brearing_y + offset_sy;
			gp.vertices[1].s = s1;
			gp.vertices[1].t = t1;

			gp.vertices[2].x = gp.brearing_x + gp.width + offset_sx;
			gp.vertices[2].y = gp.brearing_y + offset_sy;
			gp.vertices[2].s = s2;
			gp.vertices[2].t = t1;

			gp.vertices[3].x = gp.brearing_x + gp.width + offset_sx;
			gp.vertices[3].y = (gp.height - gp.brearing_y) - offset_sy;
			gp.vertices[3].s = s2;
			gp.vertices[3].t = t2;


			//
			// Find maximum value for the new line.
			//
			font->setNewLineSize(std::max(font->getNewLineSize(), gp.height));

			//
			// Store that glyph in the map
			//
			font->addGlyph(gp);

			count++;

		}
	}

}
