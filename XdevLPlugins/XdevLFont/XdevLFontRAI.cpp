/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLFontRAI.h"
#include "XdevLFontSystemRAI.h"
#include "XdevLTextLayoutRAI.h"
#include "lodepng.h"
#include <iostream>
#include <fstream>
#include <sstream>

namespace xdl {


	XdevLFontRAI::XdevLFontRAI() :
		m_fontSize(0.0f),
		m_newLine(0.0f),
		m_unitX(1.0f),
		m_unitY(1.0f) {

		//
		// Create dummy glyph for unknown glyphs. This will be returned if the method getGlyphMetric()
		// fails.
		//
		m_dummyGlyph.width		= 50;
		m_dummyGlyph.height		= 50;
		m_dummyGlyph.advance_h	= m_dummyGlyph.width;
		m_dummyGlyph.advance_v	= m_dummyGlyph.height;

		m_dummyGlyph.vertices[0].x = 0;
		m_dummyGlyph.vertices[0].y = (m_dummyGlyph.height - m_dummyGlyph.brearing_y);
		m_dummyGlyph.vertices[0].s = 0.0f;
		m_dummyGlyph.vertices[0].t = 0.0f;

		m_dummyGlyph.vertices[1].x = m_dummyGlyph.brearing_x;
		m_dummyGlyph.vertices[1].y = m_dummyGlyph.brearing_y;
		m_dummyGlyph.vertices[1].s = 0.0f;
		m_dummyGlyph.vertices[1].t = 1.0f;

		m_dummyGlyph.vertices[2].x = m_dummyGlyph.brearing_x + m_dummyGlyph.width;
		m_dummyGlyph.vertices[2].y = m_dummyGlyph.brearing_y;
		m_dummyGlyph.vertices[2].s = 1.0f;
		m_dummyGlyph.vertices[2].t = 1.0f;

		m_dummyGlyph.vertices[3].x = m_dummyGlyph.brearing_x + m_dummyGlyph.width;
		m_dummyGlyph.vertices[3].y = (m_dummyGlyph.height - m_dummyGlyph.brearing_y);
		m_dummyGlyph.vertices[3].s = 1.0f;
		m_dummyGlyph.vertices[3].t = 0.0f;


	}

	XdevLFontRAI::~XdevLFontRAI() {

	}

	XdevLGlyphMetric& XdevLFontRAI::getGlyphMetric(xdl_uint32 unicode) {

		auto ib = m_glyphMap.find(unicode);
		if(ib == m_glyphMap.end()) {
			return m_dummyGlyph;
		}

		return ib->second;
	}

	xdl_float  XdevLFontRAI::getFontSize() const {
		return m_fontSize;
	}

	xdl_float XdevLFontRAI::getNewLineSize() const {
		return m_newLine;
	}

	IPXdevLTexture XdevLFontRAI::getTexture(xdl_uint idx) {
		assert((idx < m_textureList.size()) && "XdevLFontImpl::getTexture: Specified index out of range.");
		return m_textureList[idx];
	}

	xdl_uint XdevLFontRAI::getNumberOfTextures() const {
		return m_textureList.size();
	}

}
