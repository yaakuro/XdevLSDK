/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLFontRAI.h"
#include "XdevLFontSystemRAI.h"
#include "XdevLTextLayoutRAI.h"
#include "lodepng.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

const xdl::XdevLString pluginName {
	"XdevLFont"
};

const xdl::XdevLString description {
	"This plugin helps rendering fonts."
};

std::vector<xdl::XdevLModuleName> moduleNames {
	xdl::XdevLModuleName("XdevLFontSystem"),
	xdl::XdevLModuleName("XdevLTextLayout")
};

xdl::XdevLPluginDescriptor pluginDescriptor {
	pluginName,
	moduleNames,
	XDEVLFONT_MAJOR_VERSION,
	XDEVLFONT_MINOR_VERSION,
	XDEVLFONT_PATCH_VERSION
};

xdl::XdevLModuleDescriptor fontSystemModuleDesc {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	description,
	XDEVLFONTSYSTEM_MODULE_MAJOR_VERSION,
	XDEVLFONTSYSTEM_MODULE_MINOR_VERSION,
	XDEVLFONTSYSTEM_MODULE_PATCH_VERSION
};

xdl::XdevLModuleDescriptor textLayoutModuleDesc {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	moduleNames[1],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	description,
	XDEVLTEXTLAYOUT_MODULE_MAJOR_VERSION,
	XDEVLTEXTLAYOUT_MODULE_MINOR_VERSION,
	XDEVLTEXTLAYOUT_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLFontSystemRAI, fontSystemModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLTextLayoutRAI, textLayoutModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLFontSystem, xdl::XdevLFontSystemRAI, fontSystemModuleDesc)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLTextLayout, xdl::XdevLTextLayoutRAI, textLayoutModuleDesc)