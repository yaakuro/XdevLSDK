/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/
#ifndef XDEVL_UI_WIDGETS_H
#define XDEVL_UI_WIDGETS_H

#include <XdevLUI/XdevLUI.h>
#include <memory>

namespace xdl {

	namespace ui {

		class XdevLUIPosition;
		class XdevLUISize;
		class XdevLUISurface;
		class XdevLUIButton;
		class XdevLUIText;
		class XdevLUICheckBox;
		class XdevLUIRadioButton;
		class XdevLUITextInputField;
		class XdevLUIVerticalBox;

		enum class XdevLCommandControl {
			BACKSPACE,
			LEFT,
			RIGHT,
			UP,
			DOWN,
			ENTER
		};

		/**
		 * @class XdevLUIRenderTraverser
		 * @brief Traverser to render ui widgets.
		 */
		class XdevLUIRenderTraverser {
			public:
				virtual ~XdevLUIRenderTraverser() {}
				virtual xdl_int create(IPXdevLWindow window, IPXdevLRAI rai, IPXdevLTextLayout textLayout) = 0;
				virtual void render(XdevLUIButton* button) = 0;
				virtual void render(XdevLUIText* text) = 0;
				virtual void render(XdevLUICheckBox* checkbox) = 0;
				virtual void render(XdevLUIRadioButton* radioButton) = 0;
				virtual void render(XdevLUITextInputField* textInputField) = 0;
				virtual void render(XdevLUIVerticalBox* verticalBox) = 0;

				virtual void preRender() {};
				virtual void postRender() {};
		};

		using IXXdevLUIRenderTraverser = XdevLUIRenderTraverser;
		using IPXdevLUIRenderTraverser = XdevLUIRenderTraverser*;

		enum XdevLUIState {
			NORMAL = (1 << 0),
			HOVER = (1 << 1),
			PRESS = (1 << 2),
			RELEASE = (1 << 3),
			CLICK = (1 << 4),
			ENTER = (1 << 5),
			LEAVE = (1 << 6),
		};

		enum class XdevLUIWidgetType {
			SURFACE,
			VBOX,
			HBOX,
			BUTTON,
			CHECKBOX,
			RADIOBUTTON,
			TEXT,
			SLIDER,
			SLIDERANGLE,
			COMBOBOX,
			TEXTINPUT
		};

		enum class XdevLUIAlignment {
			TOP_LEFT,
			TOP_CENTER,
			TOP_RIGHT,
			CENTER_LEFT,
			CENTER_CENTER,
			CENTER_RIGHT,
			BOTTOM_LEFT,
			BOTTOM_CENTER,
			BOTTOM_RIGHT
		};

		enum class XdevLUIPivot {
			TOP_LEFT,
			TOP_CENTER,
			TOP_RIGHT,
			CENTER_LEFT,
			CENTER_CENTER,
			CENTER_RIGHT,
			BOTTOM_LEFT,
			BOTTOM_CENTER,
			BOTTOM_RIGHT
		};

		struct XdevLUIPadding {
			xdl_float left, right, top, bottom;
		};

		/**
		* @struct XdevLUIColor
		* @brief Structure that holds color information.
		*/
		struct XdevLUIColor {
			public:
				XdevLUIColor() : r(0.0f), g(0.0f), b(0.0f), a(0.0f) {}
				XdevLUIColor(float red, float green, float blue, float alpha) : r(red), g(green), b(blue), a(alpha) {}
				union {
					xdl_float r, c;
				};
				union  {
					xdl_float g, m;
				};
				union  {
					xdl_float b, y;
				};
				union  {
					xdl_float a, k;
				};

				bool operator == (const XdevLUIColor& color) const {
					return ((color.r == r) && (color.g == g) && (color.b == b) && (color.a == a));
				}
				bool operator != (const XdevLUIColor& color) const {
					return !((color.r == r) && (color.g == g) && (color.b == b) && (color.a == a));
				}

				XdevLUIColor getCMYK() {
					XdevLUIColor color(r, g, b, a);
					color.covertToCMYK();
					return color;
				}

				void covertToCMYK() {
					float K = 1.0f - std::max(std::max(r, g), b);
					float C = (1.0f - r - K) / (1.0f - K);
					float M = (1.0f - g - K) / (1.0f - K);
					float Y = (1.0f - b - K) / (1.0f - K);
					c = C;
					y = Y;
					m = M;
					k = K;
				}

		};

		/**
		 * @class XdevLUIPosition
		 * @brief Structure that holds position information for UI elements.
		 */
		struct XdevLUIPosition : public XdevLPosition<float> {
			XdevLUIPosition() : XdevLPosition<float>() {}
			XdevLUIPosition(xdl_float x, xdl_float y) : XdevLPosition<float>(x, y) {}

			const XdevLUIPosition& operator += (float value) {
				this->x += value;
				this->y += value;
				return *this;
			}

			XDEVL_INLINE XdevLUIPosition operator+=(const XdevLUIPosition& pos) {
				x += pos.x; y += pos.y;
				return *this;
			}

			XDEVL_INLINE friend XdevLUIPosition operator+(const XdevLUIPosition& pos, const float value) {
				return XdevLUIPosition(pos.x + value, pos.y + value);
			}

			XDEVL_INLINE friend XdevLUIPosition operator * (const XdevLUIPosition& position, xdl_float value) {
				return XdevLUIPosition(position.x * value, position.y * value);
			}

			XDEVL_INLINE friend XdevLUIPosition operator + (const XdevLUIPosition& p1, const XdevLUIPosition& p2) {
				return XdevLUIPosition(p1.x + p2.x, p1.y + p2.y);
			}

		};

		/**
		 * @class XdevLUISize
		 * @brief Structure that holds size information for UI elements.
		 */
		struct XdevLUISize : public XdevLSize<float> {
			XdevLUISize() : XdevLSize<float>() {}
			XdevLUISize(xdl_float w, xdl_float h) : XdevLSize<float>(w, h)  {}

			friend XdevLUISize operator * (const XdevLUISize& size, xdl_float value) {
				return XdevLUISize(size.width * value, size.height * value);
			}
		};


		XDEVL_INLINE XdevLUIPosition operator + (const XdevLUIPosition& position, const XdevLUISize& size) {
			return XdevLUIPosition(position.x + size.width, position.y + size.height);
		}

		/**
		 * @class XdevLUIProperties
		 * @brief Properties of an UI surface.
		 */
		struct XdevLUIProperties {
			XdevLID id;
			XdevLString text;
			XdevLUISize prefSize = {0, 0};
			XdevLUISize maxSize = {0, 0};
			XdevLUISize minSize = {0, 0};
			XdevLUIAlignment alignment = XdevLUIAlignment::TOP_LEFT;
			xdl_bool disable = false;
			xdl_float opacity = 1.0f;
			XdevLUIPadding padding {0.0f, 0.0f, 0.0f, 0.0f};
			xdl_bool fillWidth = false;
		};


		/**
		 * @class XdevLUIBaseEvent
		 * @brief Base structure for all UI events.
		 */
		struct XdevLUIBaseEvent {
			xdl_uint64 timestamp;
		};

		/**
		 * @class XdevLUIClickedEvent
		 * @brief Structure for click events.
		 */
		struct XdevLUIClickedEvent : public XdevLUIBaseEvent {

		};

		/**
		 * @class XdevLUIEnterEvent
		 * @brief Structure for enter events.
		 */
		struct XdevLUIEnterEvent : public XdevLUIBaseEvent {

		};

		/**
		 * @class XdevLUILeaveEvent
		 * @brief Structure for leave events.
		 */
		struct XdevLUILeaveEvent : public XdevLUIBaseEvent {

		};

		/**
		 * @class XdevLUISliderMotionEvent
		 * @brief Structure for slider motion events.
		 */
		struct XdevLUISliderMotionEvent : public XdevLUIBaseEvent  {
			using Type = xdl_float;
			Type values[4];
		};

		/**
		 * @class XdevLUICheckedEvent
		 * @brief Structure for check events like check boxes.
		 */
		struct XdevLUICheckedEvent : public XdevLUIBaseEvent  {
			using Type = xdl_bool;
			Type checked;
		};

		/**
		 * @class XdevLUIActivatedEvent
		 * @brief Structure for activation events. Means when
		 * something gets activated like radio buttons.
		 */
		struct XdevLUIActivatedEvent : public XdevLUIBaseEvent {
			using Type = xdl_int;
			Type state;
		};

		/**
		 * @class XdevLUIComboBoxEvent
		 * @brief Structure for combox box item selection events.
		 * This will be used when and item in a combo box is selected.
		 */
		struct XdevLUIComboBoxEvent : public XdevLUIBaseEvent  {
			using Type = xdl_int;
			Type item;
		};

		/**
		 * @class XdevLUITextInputEvent
		 * @brief Structure for text change events. For example when
		 * the text inside a TextInputField gets changed.
		 */
		struct XdevLUITextInputEvent : public XdevLUIBaseEvent  {
			using Type = XdevLString;
			Type text;
		};

		/**
		 * @class XdevLUIHoverEvent
		 * @brief
		 */
		struct XdevLUIHoverEvent : public XdevLUIBaseEvent  {
			using Type = xdl_bool;
			Type state;
		};


		// Typedefs for UI delegates.
		using XdevLDelegateActivated = XdevLDelegate<void, const XdevLUIActivatedEvent&>;
		using XdevLDelegateClicked = XdevLDelegate<void, const XdevLUIClickedEvent&>;
		using XdevLDelegateEnter = XdevLDelegate<void, const XdevLUIEnterEvent&>;
		using XdevLDelegateLeave = XdevLDelegate<void, const XdevLUILeaveEvent&>;
		using XdevLDelegateSliderMotion = XdevLDelegate<void, const XdevLUISliderMotionEvent&>;
		using XdevLDelegateChecked = XdevLDelegate<void, const XdevLUICheckedEvent&>;
		using XdevLDelegateItemSelected = XdevLDelegate<void, const XdevLUIComboBoxEvent&>;
		using XdevLDelegateTextInput = XdevLDelegate<void, const XdevLUITextInputEvent&>;
		using XdevLDelegateHover = XdevLDelegate<void, const XdevLUIHoverEvent&>;


		/**
		 * @class XdevLUISurface
		 * @brief Base class for the base class of widgets :D.
		 */
		class XdevLUISurface {
			public:
				XdevLUISurface(XdevLUIWidgetType widgetType, const XdevLUISize& size = XdevLUISize(), const XdevLUIPosition& position = XdevLUIPosition())
					: m_position(position)
					, m_size(size)
					, m_pivot(XdevLUIPivot::TOP_LEFT)
					, m_widgetType(widgetType)
					, m_parent(nullptr)
					, m_state(0)
					, m_hasFocus(xdl_false) {

				}

				XdevLUISurface operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}

				const XdevLID& getID() const {
					return m_properties.id;
				}

				void setID(const XdevLID& id) {
					m_properties.id = id;
				}

				/// Returns the position.
				XdevLUIPosition getPosition() {
					auto position = m_position;
					if(m_parent) {
						position += m_parent->getPosition();
					}
					return position;
				}

				/// Returns the size.
				const XdevLUISize& getSize() const {
					return m_size;
				}

				XdevLUIWidgetType getWigetType() const {
					return m_widgetType;
				}

				void setPivot(const XdevLUIPivot& pivot) {
					m_pivot = pivot;
				}

				XdevLUIPosition getPivotPosition() const {
					XdevLUIPosition pos = m_position;
					switch(m_pivot) {
						case XdevLUIPivot::TOP_LEFT: break;
						case XdevLUIPivot::TOP_CENTER: pos.x = (m_position.x - m_size.width*0.5f); break;
						case XdevLUIPivot::TOP_RIGHT: pos.x = (m_position.x - m_size.width); break;
						case XdevLUIPivot::CENTER_LEFT: pos.y = (m_position.y - m_size.height * 0.5f); break;
						case XdevLUIPivot::CENTER_CENTER: pos.x = (m_position.x - m_size.width*0.5f); pos.y = (m_position.y - m_size.height * 0.5f); break;
						case XdevLUIPivot::CENTER_RIGHT: pos.x = (m_position.x - m_size.width); pos.y = (m_position.y - m_size.height * 0.5f); break;
						case XdevLUIPivot::BOTTOM_LEFT: pos.y = (m_position.y - m_size.height); break;
						case XdevLUIPivot::BOTTOM_CENTER: pos.x = (m_position.x - m_size.width*0.5f); pos.y = (m_position.y - m_size.height); break;
						case XdevLUIPivot::BOTTOM_RIGHT: pos.x = (m_position.x - m_size.width); pos.y = (m_position.y - m_size.height); break;
						default:break;
					}
					if(m_parent) {
						pos += m_parent->getPivotPosition();
					}
					return pos;
				}

				void setPosition(const XdevLUIPosition& pos) {
					m_position = pos;
				}

				// TODO This method is temp. Needs to be removed later.
				void setSize(const XdevLUISize& size) {
					m_size = size;
				}

				void bind(const XdevLDelegateHover& delegate, const XdevLString& group = xdl::XdevLString("Global")) {
					m_hoverDelegates.push_back(std::move(delegate));
				}

				/// Bind a delegate to this button.
				void bind(const XdevLDelegateClicked& delegate, const XdevLString& group = xdl::XdevLString("Global")) {
					m_clickedDelegates.push_back(std::move(delegate));
				}

				void hover() {
					m_state |= (xdl_uint)XdevLUIState::HOVER;
					for(auto& child : m_surfaces) {
						child->hover();
					}
					for(auto& delegate : m_hoverDelegates) {
						XdevLUIHoverEvent event;
						event.state = xdl_true;
						delegate(event);
					}
				}

				void press() {
					m_state |= (xdl_uint)XdevLUIState::PRESS;
				}

				void release() {
					m_state = m_state & ~(xdl_uint)XdevLUIState::PRESS;
				}

				void clicked() {
					m_state = (xdl_uint)XdevLUIState::CLICK;
					for(auto& delegate : m_clickedDelegates) {
						XdevLUIClickedEvent event;
						delegate(event);
					}
				}

				void enter() {

				}

				void leave() {
					resetState();
				}

				virtual void key(xdl::XdevLEvent& event) {

				}

				/// Add a surface to this vertical box.
				void addChild(XdevLUISurface* surface) {
					surface->setParent(this);
					m_surfaces.push_back(surface);
				}

				/// Returns all surfaces added to this vertical box.
				const std::vector<XdevLUISurface*>& getSurfaces() const {
					return m_surfaces;
				}

				void setParent(XdevLUISurface* parent) {
					m_parent = parent;
				}

				XdevLUISurface const* getParent() const {
					return m_parent;
				}

				/// Sets the traverser for this surface.
				virtual void render(XdevLUIRenderTraverser* traverser) {
					XDEVL_ASSERT(traverser != nullptr, "Parameter invalid.");
				}

				/// Returns the current state.
				XDEVL_INLINE XdevLUIState getState() {
					return (XdevLUIState)m_state;
				}

				void resetState() {
					m_state = (xdl_uint)XdevLUIState::NORMAL;
				}

				xdl_bool hasFocus() {
					return m_hasFocus;
				}

				virtual void onControlCommand(XdevLCommandControl cmd) {
				}

			private:

				XdevLUIPosition m_position;
				XdevLUISize m_size;
				XdevLUIPivot m_pivot;
				const XdevLUIWidgetType m_widgetType;

				std::vector<XdevLDelegateHover> m_hoverDelegates;
				std::vector<XdevLDelegateClicked> m_clickedDelegates;

			protected:

				XdevLUIProperties m_properties;

				XdevLUISurface* m_parent;

				xdl_uint m_state;
				xdl_bool m_hasFocus;

				std::vector<XdevLUISurface*> m_surfaces;
		};

		/**
		 * @class XdevLUIVerticalBox
		 * @brief Vertical box that aligned surfaces vertically.
		 */
		class XdevLUIVerticalBox : public XdevLUISurface {
			public:

				using Super = XdevLUISurface;

				XdevLUIVerticalBox(const XdevLUISize& size = XdevLUISize()) :
					XdevLUISurface(XdevLUIWidgetType::VBOX, size) {
				}

				XdevLUIVerticalBox operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}

			private:

				void render(XdevLUIRenderTraverser* traverser) override {
					Super::render(traverser);

					traverser->render(this);
				}
		};

		/**
		 * @class XdevLUIHorizontalBox
		 * @brief Vertical box that aligned surfaces vertically.
		 */
		class XdevLUIHorizontalBox : public XdevLUISurface {
			public:
				XdevLUIHorizontalBox(const XdevLUISize& size = XdevLUISize()) :
					XdevLUISurface(XdevLUIWidgetType::HBOX, size) {
				}

				XdevLUIHorizontalBox operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}
		};


		/**
		 * @class XdevLUIWidget
		 * @brief Base class for widgets.
		 */
		class XdevLUIWidget : public XdevLUISurface {
			public:
				XdevLUIWidget(XdevLUIWidgetType widgetType, const XdevLString& label, const XdevLUISize& size = XdevLUISize())
					: XdevLUISurface(widgetType, size)
					, m_foregroundColor(XdevLUIColor(1.0f, 1.0f, 1.0f, 1.0f))
					, m_backgroundColor(XdevLUIColor(0.5f, 0.5f, 0.5f, 1.0f))
					, m_hoverColor(XdevLUIColor(1.0f, 0.0f, 0.0f, 1.0f))
					, m_pressColor(XdevLUIColor(0.0f, 1.0f, 0.0f, 1.0f)) {
					setLabel(label);
				}

				/// Returns the label.
				const XdevLString& getLabel() const {
					return m_properties.text;
				}

				/// Set the text of the label.
				void setLabel(const XdevLString& text) {
					m_properties.text = text;
				}

				// TODO This is used for compress a warning when passing no arguments to function with variadic parameters.
				va_list& getArgumentList() {
					return m_args;
				}

				XDEVL_INLINE void setForgroundColor(const XdevLUIColor& color) {
					m_foregroundColor = color;
				}

				XDEVL_INLINE XdevLUIColor getForegroundColor() {
					return m_foregroundColor;
				}

				XDEVL_INLINE void setBackgroundColor(const XdevLUIColor& color) {
					m_backgroundColor = color;
				}

				XDEVL_INLINE XdevLUIColor getBackgroundColor() {
					return m_backgroundColor;
				}

				XDEVL_INLINE void setHoverColor(const XdevLUIColor& color) {
					m_hoverColor = color;
				}

				XDEVL_INLINE XdevLUIColor getHoverColor() {
					return m_hoverColor;
				}


				XDEVL_INLINE XdevLUIColor getPressColor() {
					return m_pressColor;
				}

				XDEVL_INLINE void setPressColor(const XdevLUIColor& color) {
					m_pressColor = color;
				}

			private:

				va_list m_args;

				XdevLUIColor m_foregroundColor;
				XdevLUIColor m_backgroundColor;
				XdevLUIColor m_hoverColor;
				XdevLUIColor m_pressColor;

		};

		/**
		 * @class XdevLUIText
		 * @brief A text widget.
		 */
		class XdevLUIText : public XdevLUIWidget {
			public:

				using Super = XdevLUIWidget;

				XdevLUIText(const XdevLString& label, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::TEXT, label, size) {
				}

				XdevLUIText operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}

				void render(XdevLUIRenderTraverser* traverser) override {
					Super::render(traverser);

					traverser->render(this);
				}
		};


		/**
		 * @class XdevLUICheckBox
		 * @brief A check box widget.
		 */
		class XdevLUICheckBox : public XdevLUIWidget {
			public:

				using Super = XdevLUIWidget;

				XdevLUICheckBox(const XdevLString& label, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::CHECKBOX, label, size),
					m_checked(xdl_false) {
					Super::bind(XdevLDelegateClicked::Create<XdevLUICheckBox, &XdevLUICheckBox::clicked>(this));
				}

				XdevLUICheckBox(const XdevLString& label, xdl_bool isChecked, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::CHECKBOX, label, size),
					m_checked(isChecked) {
					Super::bind(XdevLDelegateClicked::Create<XdevLUICheckBox, &XdevLUICheckBox::clicked>(this));
				}

				void clicked(const XdevLUIClickedEvent& event) {
					m_checked = !m_checked;
					checked();
				}

				void bind(const XdevLDelegateChecked& delegate, const XdevLString& group = xdl::XdevLString("Global")) {
					m_checkedDelegates.push_back(std::move(delegate));
				}

				void setChecked(xdl_bool isChecked) {
					m_checked = isChecked;
				}

				/// Returns the check state.
				xdl_bool isChecked() {
					return m_checked;
				}

				xdl_bool* getData() {
					return &m_checked;
				}

				void checked() {
					for(auto& delegate : m_checkedDelegates) {
						XdevLUICheckedEvent event;
						event.checked = m_checked;
						delegate(event);
					}
				}

				void checked(XdevLUICheckedEvent::Type value) {
					m_checked = value;

					for(auto& delegate : m_checkedDelegates) {
						XdevLUICheckedEvent event;
						event.checked = value;
						delegate(event);
					}
				}

				XdevLUICheckBox operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}

			private:

				void render(XdevLUIRenderTraverser* traverser) override {
					Super::render(traverser);

					traverser->render(this);
				}

			private:

				xdl_bool m_checked;
				std::vector<XdevLDelegateChecked> m_checkedDelegates;
		};


		/**
		 * @class XdevLUIRadioButton
		 * @brief A radio button widget.
		 */
		class XdevLUIRadioButton : public XdevLUIWidget {
			public:

				using Super = XdevLUIWidget;

				XdevLUIRadioButton(const XdevLString& label, xdl_int id, const XdevLUISize& size = XdevLUISize())
					: XdevLUIWidget(XdevLUIWidgetType::RADIOBUTTON, label, size)
					, m_id(id) {
					m_activated = std::make_shared<xdl_int>();
					Super::bind(XdevLDelegateClicked::Create<XdevLUIRadioButton, &XdevLUIRadioButton::clicked>(this));
				}

				XdevLUIRadioButton(const XdevLString& label, xdl_int id, XdevLUIRadioButton& radioButton, const XdevLUISize& size = XdevLUISize())
					: XdevLUIWidget(XdevLUIWidgetType::RADIOBUTTON, label, size)
					, m_id(id) {
					m_activated = radioButton.getSharedData();
					Super::bind(XdevLDelegateClicked::Create<XdevLUIRadioButton, &XdevLUIRadioButton::clicked>(this));
				}

				virtual ~XdevLUIRadioButton() {

				}

				void clicked(const XdevLUIClickedEvent& event) {
					(*m_activated.get()) = m_id;
					changed();
				}

				void bind(const XdevLDelegateActivated& delegate, const XdevLString& group = xdl::XdevLString("Global")) {
					m_activatedDelegates.push_back(std::move(delegate));
				}

				xdl_uint getID() {
					return m_id;
				}

				xdl_int* getData() {
					return m_activated.get();
				}

				std::shared_ptr<xdl_int> getSharedData() {
					return m_activated;
				}

				void changed() {
					for(auto& delegate : m_activatedDelegates) {
						XdevLUIActivatedEvent event;
						event.state = *m_activated;
						delegate(event);
					}
				}

				xdl_bool isActive() {
					return ((*m_activated.get()) == m_id);
				}

			private:

				void render(XdevLUIRenderTraverser* traverser) override {
					Super::render(traverser);

					traverser->render(this);
				}

			private:

				xdl_int m_id;
				std::shared_ptr<xdl_int> m_activated;
				std::vector<XdevLDelegateActivated> m_activatedDelegates;
		};

		/**
		 * @class XdevLUIButton
		 * @brief A button widget.
		 */
		class XdevLUIButton : public XdevLUIWidget {
			public:

				using Super = XdevLUIWidget;

				XdevLUIButton(const XdevLString& label = XdevLString(), const XdevLUISize& size = XdevLUISize(64,32))
					: XdevLUIWidget(XdevLUIWidgetType::BUTTON, label, size) {
				}

				XdevLUIButton operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}

			private:

				void render(XdevLUIRenderTraverser* traverser) override {
					Super::render(traverser);

					traverser->render(this);
				}

			private:

		};

		/**
		 * @class XdevLUISlider
		 * @brief A slider widget.
		 */
		class XdevLUISlider : public XdevLUIWidget {
			public:
				using Type = XdevLUISliderMotionEvent::Type;

				XdevLUISlider(const XdevLString& label, xdl_int numberOfSliders = 1, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::SLIDER, label, size),
					m_min(0.0f),
					m_max(1.0f) {
					m_values.reserve(numberOfSliders);
					m_values.resize(numberOfSliders);
				}

				XdevLUISlider(const XdevLString& label, std::vector<xdl_float>& initialize, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::SLIDER, label, size),
					m_min(0.0f),
					m_max(1.0f) {
					m_values = std::move(initialize);
				}

				XdevLUISlider(const XdevLString& label, xdl_float min, xdl_float max, xdl_int numberOfSliders = 1, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::SLIDER, label, size),
					m_min(min),
					m_max(max) {
					m_values.reserve(numberOfSliders);
					m_values.resize(numberOfSliders);
				}

				XdevLUISlider(const XdevLString& label, xdl_float min, xdl_float max, std::vector<xdl_float>& initialize, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::SLIDER, label, size),
					m_min(min),
					m_max(max) {
					m_values = std::move(initialize);
				}

				void bind(const XdevLDelegateSliderMotion& delegate, const XdevLString& group = xdl::XdevLString("Global")) {
					m_sliderDelegates.push_back(std::move(delegate));
				}

				Type* getData() {
					return m_values.data();
				}

				xdl_int getSize() {
					return m_values.size();
				}

				xdl_float getMin() {
					return m_min;
				}

				xdl_float getMax() {
					return m_max;
				}

				/// This will be called by an emitter.
				void sliderMoved() {
					if(m_sliderDelegates.size() == 0) {
						return;
					}

					XdevLUISliderMotionEvent event;
					std::copy(m_values.begin(), m_values.end(), event.values);

					for(auto& delegate : m_sliderDelegates) {
						delegate(event);
					}
				}

				XdevLUISlider operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}

			private:

				xdl_float m_min;
				xdl_float m_max;
				std::vector<Type> m_values;
				std::vector<XdevLDelegateSliderMotion> m_sliderDelegates;
		};

		/**
		 * @class XdevLUISliderAngle
		 * @brief A slider that represents an angle widget.
		 */
		class XdevLUISliderAngle : public XdevLUISlider {
			public:
				using Type = XdevLUISliderMotionEvent::Type;

				XdevLUISliderAngle(const XdevLString& label, const XdevLUISize& size = XdevLUISize()) :
					XdevLUISlider(label, 1, size) {

				}
				XdevLUISliderAngle(const XdevLString& label, xdl_float min, xdl_float max, const XdevLUISize& size = XdevLUISize()) :
					XdevLUISlider(label, min, max, 1, size) {

				}
				XdevLUISliderAngle(const XdevLString& label, xdl_float min, xdl_float max, std::vector<Type>& initialize, const XdevLUISize& size = XdevLUISize()) :
					XdevLUISlider(label, min, max, initialize, size)	{

				}

				XdevLUISliderAngle operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}
			private:

				const XdevLUIWidgetType m_widgetType = XdevLUIWidgetType::SLIDERANGLE;
		};

		/**
		 * @class XdevLUIComboBox
		 * @brief A combo box widget.
		 */
		class XdevLUIComboBox : public XdevLUIWidget {
			public:
				XdevLUIComboBox(const XdevLString& label, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::COMBOBOX, label, size),
					m_selectedItem(0) {

				}

				XdevLUIComboBox(const XdevLString& label, std::vector<XdevLString>& items, const XdevLUISize& size = XdevLUISize()) :
					XdevLUIWidget(XdevLUIWidgetType::COMBOBOX, label, size),
					m_selectedItem(0) {
					m_items = std::move(items);
				}

				void bind(const XdevLDelegateItemSelected& delegate, const XdevLString& group = xdl::XdevLString("Global")) {
					m_comboBoxDelegates.push_back(std::move(delegate));
				}

				std::vector<XdevLString> & getItems() {
					return m_items;
				}

				XdevLUIComboBoxEvent::Type* getSelectedItemData() {
					return &m_selectedItem;
				}

				XdevLUIComboBoxEvent::Type getSelectedItemIndex() {
					return m_selectedItem;
				}

				void selected() {
					if(m_comboBoxDelegates.size() == 0) {
						return;
					}

					XdevLUIComboBoxEvent event;
					event.item = m_selectedItem;

					for(auto& delegate : m_comboBoxDelegates) {
						delegate(event);
					}
				}

				XdevLUIComboBox operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}

			private:

				XdevLUIComboBoxEvent::Type m_selectedItem;
				std::vector<XdevLString> m_items;
				std::vector<XdevLDelegateItemSelected> m_comboBoxDelegates;
		};

		/**
		 * @class XdevLUITextInputField
		 * @brief A text input field widget.
		 */
		class XdevLUITextInputField : public XdevLUIWidget {
			public:

				using Super = XdevLUIWidget;

				XdevLUITextInputField(const XdevLString& label, const XdevLUISize& size = XdevLUISize())
					: XdevLUIWidget(XdevLUIWidgetType::TEXTINPUT, label, size) {
					Super::bind(XdevLDelegateClicked::Create<XdevLUITextInputField, &XdevLUITextInputField::clicked>(this));
				}

				void bind(const XdevLDelegateTextInput& delegate, const XdevLString& group = xdl::XdevLString("Global")) {
					m_textInputDelegates.push_back(std::move(delegate));
				}

				void clicked(const XdevLUIClickedEvent& event) {
					m_hasFocus = xdl_true;
				}

				const XdevLString& getText() const {
					return m_inputField;
				}

				xdl_uint8* getData() {
					return m_inputField.data();
				}

				xdl_int getBufferSize() {
					return m_inputField.size();
				}

				void key(xdl::XdevLEvent& event) override final {
					m_inputField += XdevLString::ansiToString(event.textinput.text);
//					m_inputField = "Hello";
//					std::cout << m_inputField.toString() << std::endl;
				}

				void changed() {
					if(m_textInputDelegates.size() == 0) {
						return;
					}

					XdevLUITextInputEvent event;
					event.text = m_inputField;

					for(auto& delegate : m_textInputDelegates) {
						delegate(event);
					}
				}

				void onControlCommand(XdevLCommandControl cmd) override final {
					switch(cmd) {
						case XdevLCommandControl::BACKSPACE: {
							m_inputField = m_inputField.substr(0, m_inputField.size() - 1);
							std::cout << m_inputField.size() << std::endl;
						} break;
						default: break;
					}
				}

				XdevLUITextInputField operator = (XdevLUIProperties& properties) {
					m_properties = properties;
					return *this;
				}

			private:

				void render(XdevLUIRenderTraverser* traverser) override {
					Super::render(traverser);

					traverser->render(this);
				}

			private:

				XdevLString m_inputField;
				std::vector<XdevLDelegateTextInput> m_textInputDelegates;

		};

	}
}

#endif
