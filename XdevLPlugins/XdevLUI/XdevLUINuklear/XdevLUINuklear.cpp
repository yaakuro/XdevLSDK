/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#include <nuklear.h>

#include <XdevL.h>
#include <XdevLInput/XdevLInputSystem.h>

#include "XdevLUINuklear.h"

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::ui::pluginName,
	xdl::ui::moduleNames,
	XDEVLGUI_PLUGIN_MAJOR_VERSION,
	XDEVLGUI_PLUGIN_MINOR_VERSION,
	XDEVLGUI_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDesc {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::ui::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::ui::description,
	XDEVLGUI_MODULE_MAJOR_VERSION,
	XDEVLGUI_MODULE_MINOR_VERSION,
	XDEVLGUI_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::ui::XdevLUINuklear, moduleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

namespace xdl {

	namespace ui {
		static const xdl::XdevLID ButtonPressed("XDEVL_BUTTON_PRESSED");
		static const xdl::XdevLID ButtonReleased("XDEVL_BUTTON_RELEASED");
		static const xdl::XdevLID MouseButtonPressed("XDEVL_MOUSE_BUTTON_PRESSED");
		static const xdl::XdevLID MouseButtonReleased("XDEVL_MOUSE_BUTTON_RELEASED");
		static const xdl::XdevLID MouseMouseMotion("XDEVL_MOUSE_MOTION");
		static const XdevLID TextInputEvent("XDEVL_TEXT_INPUT");

		static xdl::IPXdevLRAI RAI;
		static xdl::IPXdevLShaderProgram shaderProgram;
		static xdl::IPXdevLVertexArray vertexArray;
		static xdl::IPXdevLVertexBuffer vertexBuffer;
		static xdl::IPXdevLIndexBuffer indexBuffer;
		static xdl::IPXdevLTexture texture;

		static int g_AttribLocationTex = 0, g_AttribLocationProjMtx = 0;
		static int g_AttribLocationPosition = 0, g_AttribLocationUV = 0, g_AttribLocationColor = 0;

//		static std::map<int, int> xdevLToNuklear;

//
// XdevLShaders
//

		const xdl_char *vertexShader =
		    "layout(location = 0) in vec2 iposition;\n"
		    "layout(location = 1) in vec2 iuv;\n"
		    "layout(location = 2) in vec4 icolor;\n"
		    "out vec2 fuv;\n"
		    "out vec4 fcolor;\n"
		    "uniform mat4 projMatrix;\n"
		    "void main()\n"
		    "{\n"
		    "	fuv = iuv;\n"
		    "	fcolor = icolor;\n"
		    "	gl_Position = projMatrix * vec4(iposition.xy,0,1);\n"
		    "}\n";

		const xdl_char* fragmentShader =
		    "uniform sampler2D tex;\n"
		    "in vec2 fuv;\n"
		    "in vec4 fcolor;\n"
		    "out vec4 ocolor;\n"
		    "void main()\n"
		    "{\n"
		    "	ocolor = fcolor * texture( tex, fuv.st);\n"
		    "}\n";


		XdevLUINuklear::XdevLUINuklear(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
			XdevLModuleImpl<XdevLUI>(parameter, descriptor),
			m_window(nullptr),
			m_rai(nullptr),
			m_cursor(nullptr),
			m_lock(xdl_false) {

		}

		XdevLUINuklear::~XdevLUINuklear() {

		}

		xdl_int XdevLUINuklear::init() {
			nk_init_default(&m_ctx, nullptr);
			nk_buffer_init_default(&m_cmds);
			m_ctx.clip.copy = nullptr;
			m_ctx.clip.paste = nullptr;
			m_ctx.clip.userdata = nk_handle_ptr(0);
			nk_buffer_init_default(&m_cmds);

//			xdevLToNuklear[KEY_TAB] = NK_KEY_TAB;
//			xdevLToNuklear[KEY_LEFT] = NK_KEY_LEFT;
//			xdevLToNuklear[KEY_RIGHT] = NK_KEY_RIGHT;
//			xdevLToNuklear[KEY_UP] = NK_KEY_UP;
//			xdevLToNuklear[KEY_DOWN] = NK_KEY_DOWN;
//			xdevLToNuklear[KEY_PAGEUP] = NK_KEY_SCROLL_UP;
//			xdevLToNuklear[KEY_PAGEDOWN] = NK_KEY_SCROLL_DOWN;
//			xdevLToNuklear[KEY_HOME] = KEY_HOME;
//			xdevLToNuklear[KEY_END] = KEY_END;
//			xdevLToNuklear[KEY_DELETE] = NK_KEY_DEL;
//			xdevLToNuklear[KEY_BACKSPACE] = NK_KEY_BACKSPACE;
//			xdevLToNuklear[KEY_ENTER] = NK_KEY_ENTER;
//			xdevLToNuklear[KEY_ESCAPE] = KEY_ESCAPE;
//			xdevLToNuklear[KEY_C] = NK_KEY_COPY;
//			xdevLToNuklear[KEY_V] = NK_KEY_PASTE;
//			xdevLToNuklear[KEY_X] = NK_KEY_CUT;
//			xdevLToNuklear[KEY_R] = NK_KEY_TEXT_REDO;
//			xdevLToNuklear[KEY_Z] = NK_KEY_TEXT_UNDO;

			XDEVL_MODULEX_SUCCESS(XdevLUINuklear, "ImGUI created successful.\n");
			return RET_SUCCESS;
		}

		xdl_int XdevLUINuklear::shutdown() {
//			nk_font_atlas_clear(m_atlas);
			nk_free(&m_ctx);
			nk_buffer_free(&m_cmds);
			return RET_SUCCESS;
		}

		xdl_int XdevLUINuklear::create(IPXdevLWindow window, IPXdevLRAI rai) {
			m_window = window;
			m_rai = rai;
			RAI = rai;

			// Create a window so that we can draw something.
			m_cursor = static_cast<XdevLCursor*>(getMediator()->createModule(XdevLModuleName("XdevLCursor"), XdevLID("XdevLUINuklearCursor")));
			if(!m_cursor) {
				XDEVL_MODULEX_ERROR(XdevLUINuklear, "Creation of the module: 'XdevLCursor' failed.\n");
				return xdl::RET_FAILED;
			}

			if(m_cursor->attach(m_window) != xdl::RET_SUCCESS) {
				return xdl::RET_FAILED;
			}


			m_vertexShader = m_rai->createVertexShader();
			m_fragmentShader = m_rai->createFragmentShader();
			m_shaderProgram = m_rai->createShaderProgram();

			m_vertexShader->addShaderCode(vertexShader);
			m_fragmentShader->addShaderCode(fragmentShader);

			m_vertexShader->compile();
			m_fragmentShader->compile();

			m_shaderProgram->attach(m_vertexShader);
			m_shaderProgram->attach(m_fragmentShader);
			m_shaderProgram->link();

			shaderProgram = m_shaderProgram;

			g_AttribLocationTex = m_shaderProgram->getUniformLocation("tex");
			g_AttribLocationProjMtx = m_shaderProgram->getUniformLocation("projMatrix");
			g_AttribLocationPosition = m_shaderProgram->getAttribLocation("iposition");
			g_AttribLocationUV = m_shaderProgram->getAttribLocation("iuv");
			g_AttribLocationColor = m_shaderProgram->getAttribLocation("icolor");



			auto vd = rai->createVertexDeclaration();
			vd->add(2, XdevLBufferElementTypes::FLOAT, g_AttribLocationPosition);
			vd->add(2, XdevLBufferElementTypes::FLOAT, g_AttribLocationUV);
			vd->add(4, XdevLBufferElementTypes::UNSIGNED_BYTE,  g_AttribLocationColor, xdl_true);

			vertexBuffer = m_rai->createVertexBuffer();
			vertexBuffer->init();

			indexBuffer = m_rai->createIndexBuffer();
			indexBuffer->init(XdevLBufferElementTypes::UNSIGNED_SHORT);

			m_vertexArray = m_rai->createVertexArray();
			m_vertexArray->init(vertexBuffer, indexBuffer, vd);
			vertexArray = m_vertexArray;

			//
			// Create Texture.
			//
			m_texture = m_rai->createTexture();

			//
			// Build texture atlas
			//
			nk_font_atlas_init_default(&m_atlas);
			nk_font_atlas_begin(&m_atlas);


			const void *image; int w, h;
			image = nk_font_atlas_bake(&m_atlas, &w, &h, NK_FONT_ATLAS_RGBA32);

			const XdevLTextureSamplerFilterStage filterStage {xdl::XdevLTextureFilterMin::NEAREST, xdl::XdevLTextureFilterMag::NEAREST};
			const XdevLTextureSamplerWrap wrap {XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE};

			m_texture->init(w, h, XdevLTextureFormat::RGBA8, XdevLTexturePixelFormat::RGBA, (xdl_uint8*)image);
			m_texture->lock();
			m_texture->setTextureFilter(filterStage);
			m_texture->setTextureWrap(wrap);
			m_texture->unlock();

			texture = m_texture;

			nk_font_atlas_end(&m_atlas, nk_handle_id((int)m_texture->id()), &m_null);
			if(m_atlas.default_font) {
				nk_style_set_font(&m_ctx, &m_atlas.default_font->handle);
			}

			return RET_SUCCESS;
		}

		xdl_int XdevLUINuklear::lock(const XdevLString& title) {
			XDEVL_ASSERT(m_lock != xdl_true, "You must unlock before your can lock.\n");

			m_lock = true;

			// Setup display size (every frame to accommodate for window resizing)
			xdl_int w = m_window->getWidth();
			xdl_int h = m_window->getHeight();

			// TODO This is obviously a hack.
			m_displayWidth = w;
			m_displayHeight = h;

			if(nk_begin(&m_ctx,
			            title.toString().c_str(),
			            nk_rect(50, 50, 230, 250),
			            NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE| NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE)) {

				nk_layout_row_static(&m_ctx, 30, 80, 1);
				if(nk_button_label(&m_ctx, "button"))
					fprintf(stdout, "button pressed\n");
			}
			return RET_SUCCESS;
		}

		xdl_int XdevLUINuklear::unlock() {
			XDEVL_ASSERT(m_lock == xdl_true, "You must unlock before your can lock.\n");
			nk_end(&m_ctx);
			m_lock = xdl_false;
			return RET_SUCCESS;
		}

		xdl_int XdevLUINuklear::render() {
			xdl_float windowX = (xdl_float)m_window->getWidth();
			xdl_float windowY = (xdl_float)m_window->getHeight();

			m_fbScaleX = windowX > 0 ? ((xdl_float)m_displayWidth / windowX) : 0;
			m_fbScaleY = windowY > 0 ? ((xdl_float)m_displayHeight / windowY) : 0;

			RAI->setActiveBlendMode(xdl_true);
			RAI->setBlendMode(XDEVL_BLEND_SRC_ALPHA, XDEVL_BLEND_ONE_MINUS_SRC_ALPHA);
			RAI->setBlendFunc(XDEVL_FUNC_ADD);
			RAI->setActiveDepthTest(xdl_false);
			RAI->setActiveScissorTest(xdl_true);

			xdl::XdevLViewPort viewport {};
			viewport.x = 0.0f;
			viewport.y = 0.0f;
			viewport.width = windowX;
			viewport.height = windowY;
			RAI->setViewport(viewport);

			const xdl_float ortho_projection[4][4] = {
				{ 2.0f/m_displayWidth, 0.0f,                   0.0f, 0.0f },
				{ 0.0f,                  2.0f/-m_displayHeight, 0.0f, 0.0f },
				{ 0.0f,                  0.0f,                  -1.0f, 0.0f },
				{-1.0f,                  1.0f,                   0.0f, 1.0f },
			};

			shaderProgram->activate();
			shaderProgram->setUniformMatrix4(g_AttribLocationProjMtx, 1, &ortho_projection[0][0]);
			shaderProgram->setUniformi(g_AttribLocationTex, 0);
			shaderProgram->deactivate();

			RAI->setActiveVertexArray(vertexArray);
			RAI->setActiveShaderProgram(shaderProgram);
			texture->activate(XdevLTextureStage::STAGE0);



			const nk_draw_command* cmd;
			xdl_int idx_buffer_offset = 0;
			nk_draw_foreach(cmd, &m_ctx, &m_cmds) {
				if(!cmd->elem_count) {
					continue;
				}

				RAI->setScissor((cmd->clip_rect.x * m_fbScaleX),
				                ((windowY - (cmd->clip_rect.y + cmd->clip_rect.h)) * m_fbScaleY),
				                (cmd->clip_rect.w * m_fbScaleX),
				                (cmd->clip_rect.h * m_fbScaleY));

				RAI->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLES, cmd->elem_count, idx_buffer_offset);
				idx_buffer_offset += cmd->elem_count;
			}
			return RET_SUCCESS;
		}

		xdl_int XdevLUINuklear::notify(xdl::XdevLEvent& event) {

//			if(m_ctx.input.mouse.grab) {
////	        SDL_SetRelativeMouseMode(SDL_TRUE);
//				m_ctx.input.mouse.grab = 0;
//			} else if(m_ctx.input.mouse.ungrab) {
//				int x = (int)m_ctx.input.mouse.prev.x;
//				int y = (int)m_ctx.input.mouse.prev.y;
//				m_cursor->setPosition(x, y);
////	        SDL_SetRelativeMouseMode(SDL_FALSE);
////	        SDL_WarpMouseInWindow(sdl.win, x, y);
//				m_ctx.input.mouse.ungrab = 0;
//			}
//			if(event.type == TextInputEvent.getHashCode()) {
//				nk_glyph glyph;
//				memcpy(glyph, event.textinput.text, NK_UTF_SIZE);
//				nk_input_glyph(&m_ctx, glyph);
//			} else if(event.type == ButtonPressed.getHashCode()) {
//
//				if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 1);
//				} else if((event.key.keycode == KEY_LCTRL) || (event.key.keycode == KEY_RCTRL)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 1);
//				} else if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 1);
//				} else if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 1);
//				}
//
//			} else if(event.type == ButtonReleased.getHashCode()) {
//
//				if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 0);
//				} else if((event.key.keycode == KEY_LCTRL) || (event.key.keycode == KEY_RCTRL)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 0);
//				} else if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 0);
//				} else if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 0);
//				}
//
//			} else if(event.type == MouseButtonPressed.getHashCode()) {
//				if(event.button.button == BUTTON_LEFT) {
//					nk_input_button(&m_ctx, NK_BUTTON_LEFT, event.button.x, event.button.y, 1);
//				} else if(event.button.button == BUTTON_RIGHT) {
//					nk_input_button(&m_ctx, NK_BUTTON_RIGHT, event.button.x, event.button.y, 1);
//				} else if(event.button.button == BUTTON_MIDDLE) {
//					nk_input_button(&m_ctx, NK_BUTTON_MIDDLE, event.button.x, event.button.y, 1);
//				}
//			} else if(event.type == MouseButtonReleased.getHashCode()) {
//				if(event.button.button == BUTTON_LEFT) {
//					nk_input_button(&m_ctx, NK_BUTTON_LEFT, event.button.x, event.button.y, 0);
//				} else if(event.button.button == BUTTON_RIGHT) {
//					nk_input_button(&m_ctx, NK_BUTTON_RIGHT, event.button.x, event.button.y, 0);
//				} else if(event.button.button == BUTTON_MIDDLE) {
//					nk_input_button(&m_ctx, NK_BUTTON_MIDDLE, event.button.x, event.button.y, 0);
//				}
//			} else if(event.type == MouseMouseMotion.getHashCode()) {
//				float sx = (float)(32768 + event.motion.x)/65536*m_window->getWidth();
//				float sy = (float)m_window->getHeight() - (float)(32768 + event.motion.y)/65536*m_window->getHeight();
//				if(m_ctx.input.mouse.grabbed) {
//					int x = (int)m_ctx.input.mouse.prev.x;
//					int y = (int)m_ctx.input.mouse.prev.y;
//					nk_input_motion(&m_ctx, x + event.motion.xrel, y + event.motion.yrel);
//				} else {
//					nk_input_motion(&m_ctx, sx, sy);
//				}
//			}


			return XdevLModuleImpl<XdevLUI>::notify(event);
		}

		xdl_float XdevLUINuklear::getFPS() {
			return 0.0f;
		}

		xdl_float XdevLUINuklear::getJitter() {
			return 0.0f;
		}

		void XdevLUINuklear::add(XdevLUIText& text) {
			//	ImGui::TextV((const char*)text.getLabel().toString().c_str(), text.getArgumentList());
		}


		void XdevLUINuklear::add(XdevLUICheckBox& checkbox) {
			// if(ImGui::Checkbox(checkbox.getLabel().toString().c_str(), checkbox.getData())) {
			// 	checkbox.checked();
			// }
		}

		void XdevLUINuklear::add(XdevLUIRadioButton& radiobutton) {
			// if(ImGui::RadioButton(radiobutton.getLabel().toString().c_str(), radiobutton.getData(), radiobutton.getID())) {
			// 	radiobutton.changed();
			// }
		}

		void XdevLUINuklear::add(XdevLUISlider& slider) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

			// if(slider.getSize() == 1) {
			// 	if(ImGui::SliderFloat(slider.getLabel().toString().c_str(), slider.getData(), slider.getMin(), slider.getMax(), "%.3f", 1.0)) {
			// 		slider.sliderMoved();
			// 	}
			//
			// } else if(slider.getSize() == 2) {
			// 	if(ImGui::SliderFloat2(slider.getLabel().toString().c_str(), slider.getData(), slider.getMin(), slider.getMax(), "%.3f", 1.0)) {
			// 		slider.sliderMoved();
			// 	}
			//
			// } else if(slider.getSize() == 3) {
			// 	if(ImGui::SliderFloat3(slider.getLabel().toString().c_str(), slider.getData(), slider.getMin(), slider.getMax(), "%.3f", 1.0)) {
			// 		slider.sliderMoved();
			// 	}
			//
			// } else if(slider.getSize() == 4) {
			// 	if(ImGui::SliderFloat4(slider.getLabel().toString().c_str(), slider.getData(), slider.getMin(), slider.getMax(), "%.3f", 1.0)) {
			// 		slider.sliderMoved();
			// 	}
			// }

		}

		void XdevLUINuklear::add(XdevLUISliderAngle& sliderAngle) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

//			std::cout << "MinMax: " << sliderAngle.getMin() << "," << sliderAngle.getMax() << std::endl;

			// if(ImGui::SliderAngle(sliderAngle.getLabel().toString().c_str(), sliderAngle.getData(), sliderAngle.getMin(), sliderAngle.getMax())) {
			// 	sliderAngle.sliderMoved();
			// }
		}


		void XdevLUINuklear::add(XdevLUIButton& button) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

//			auto size = button.getSize();
			// if(ImGui::Button(button.getLabel().toString().c_str(), ImVec2(size.width, size.height))) {
			// 	button.clicked();
			// }
//			nk_layout_row_static(&m_ctx, size.width, size.height, 1);
//			if(nk_button_label(&m_ctx, button.getLabel().toString().c_str())) {
//				button.clicked();
//			}
		}

		void XdevLUINuklear::add(XdevLUIComboBox& comboBox) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

			// std::vector<const xdl_char*> tmp;
			// for(auto& item : comboBox.getItems()) {
			// 	tmp.push_back(item.toString().c_str());
			// }
			//
			// if(ImGui::Combo(comboBox.getLabel().toString().c_str(), comboBox.getSelectedItemData(), tmp.data(), tmp.size(), -1)) {
			// 	comboBox.selected();
			// }
		}

		void XdevLUINuklear::add(XdevLUITextInputField& textInputField) {
			// TODO The cast to (xdl_char*) might cause issues later.
			// if(ImGui::InputText(textInputField.getLabel().toString().c_str(), (xdl_char*)textInputField.getData(), textInputField.getBufferSize())) {
			// 	textInputField.changed();
			// }
		}

	}
}
