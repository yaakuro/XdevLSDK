/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/


#include <XdevL.h>
#include <XdevLInput/XdevLInputSystem.h>
#include <XdevLRAI/XdevLRAI.h>

#include "XdevLUIRAI.h"

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::ui::pluginName,
	xdl::ui::moduleNames,
	XDEVLGUI_PLUGIN_MAJOR_VERSION,
	XDEVLGUI_PLUGIN_MINOR_VERSION,
	XDEVLGUI_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDesc {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::ui::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::ui::description,
	XDEVLGUI_MODULE_MAJOR_VERSION,
	XDEVLGUI_MODULE_MINOR_VERSION,
	XDEVLGUI_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::ui::XdevLUIRAI, moduleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

namespace xdl {

	namespace ui {
		static const xdl::XdevLID ButtonPressed("XDEVL_BUTTON_PRESSED");
		static const xdl::XdevLID ButtonReleased("XDEVL_BUTTON_RELEASED");
		static const xdl::XdevLID MouseButtonPressed("XDEVL_MOUSE_BUTTON_PRESSED");
		static const xdl::XdevLID MouseButtonReleased("XDEVL_MOUSE_BUTTON_RELEASED");
		static const xdl::XdevLID MouseMouseMotion("XDEVL_MOUSE_MOTION");
		static const XdevLID TextInputEvent("XDEVL_TEXT_INPUT");

		XdevLUIRAI::XdevLUIRAI(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
			: XdevLModuleAutoImpl<XdevLUI>(parameter, descriptor)
			, m_window(nullptr)
			, m_rai(nullptr)
			, m_cursor(nullptr)
			, m_font(nullptr)
			, m_fontSystem(nullptr)
			, m_textLayoutSystem(nullptr)
			, m_renderTraverser(nullptr)
			, m_lock(xdl_false) {

		}

		XdevLUIRAI::~XdevLUIRAI() {

		}

		xdl_int XdevLUIRAI::init() {
			return RET_SUCCESS;
		}

		xdl_int XdevLUIRAI::shutdown() {

			if(nullptr != m_renderTraverser) {
				delete m_renderTraverser;
				m_renderTraverser = nullptr;
			}

			return RET_SUCCESS;
		}

		xdl_int XdevLUIRAI::create(IPXdevLWindow window, IPXdevLRAI rai, IPXdevLArchive archive) {
			m_window = window;
			m_rai = rai;

			m_fontSystem = static_cast<IPXdevLFontSystem>(getMediator()->createModule(XdevLModuleName("XdevLFontSystem"), XdevLID("XdevLUIRAIFontSystem")));
			if(isModuleNotValid(m_fontSystem)) {
				return RET_FAILED;
			}

			XdevLFontSystemCreateParameter fontSystemCreateParameter;
			fontSystemCreateParameter.rai = rai;
			m_fontSystem->create(fontSystemCreateParameter);

			m_textLayoutSystem = static_cast<IPXdevLTextLayout>(getMediator()->createModule(XdevLModuleName("XdevLTextLayout"), XdevLID("XdevLUIRAITextLayout")));
			if(isModuleNotValid(m_textLayoutSystem)) {
				return RET_FAILED;
			}

			m_renderTraverser = new XdevLUIRenderTraverserRAI();
			m_renderTraverser->create(window, rai, m_textLayoutSystem);

			// Create a window so that we can draw something.
			m_cursor = static_cast<XdevLCursor*>(getMediator()->createModule(XdevLModuleName("XdevLCursor"), XdevLID("XdevLUIRAICursor")));
			if(!m_cursor) {
				XDEVL_MODULEX_ERROR(XdevLUIRAI, "Creation of the module: 'XdevLCursor' failed.\n");
				return xdl::RET_FAILED;
			}

			if(m_cursor->attach(m_window) != xdl::RET_SUCCESS) {
				return xdl::RET_FAILED;
			}

			return RET_SUCCESS;
		}

		xdl_int XdevLUIRAI::setupFontFromFile(std::shared_ptr<XdevLFile>& fontFile) {
			m_font = m_fontSystem->createFromFontFile(fontFile);
			if(nullptr == m_font) {
				return RET_FAILED;
			}

			XdevLTextLayoutCreateParameter textLayoutCreateParameter;
			textLayoutCreateParameter.rai = m_rai;
			textLayoutCreateParameter.window = m_window;

			m_textLayoutSystem->create(textLayoutCreateParameter);
			m_textLayoutSystem->usePixelUnits(xdl_true);
			m_textLayoutSystem->setScale(1.0f);
			m_textLayoutSystem->setDFT(0);
			m_textLayoutSystem->setEffect(0);
			m_textLayoutSystem->useFont(m_font);

			return RET_SUCCESS;
		}

		xdl_int XdevLUIRAI::setupFont(IPXdevLFont	font) {
			m_font = font;

			XdevLTextLayoutCreateParameter textLayoutCreateParameter;
			textLayoutCreateParameter.rai = m_rai;
			textLayoutCreateParameter.window = m_window;

			m_textLayoutSystem->create(textLayoutCreateParameter);
			m_textLayoutSystem->usePixelUnits(xdl_true);
			m_textLayoutSystem->setScale(1.0f);
			m_textLayoutSystem->setDFT(0);
			m_textLayoutSystem->setEffect(0);
			m_textLayoutSystem->useFont(m_font);

			return RET_SUCCESS;
		}


		void XdevLUIRAI::setFontSize(const XdevLUISize& size) {
			XDEVL_ASSERT(m_textLayoutSystem != nullptr, "XdevLTextLayout not created.");

			m_textLayoutSystem->setScale(size.width);
		}

		xdl_int XdevLUIRAI::lock(const XdevLString& title) {
			XDEVL_ASSERT(m_lock != xdl_true, "You must unlock before your can lock.\n");

			m_lock = true;

			return RET_SUCCESS;
		}

		xdl_int XdevLUIRAI::unlock() {
			XDEVL_ASSERT(m_lock == xdl_true, "You must unlock before your can lock.\n");

			m_lock = xdl_false;
			return RET_SUCCESS;
		}

		xdl_int XdevLUIRAI::render() {
			m_renderTraverser->preRender();

			for(const auto& surface : m_surfaces) {
				surface->render(m_renderTraverser);
			}
			m_renderTraverser->postRender();

			return RET_SUCCESS;
		}

		xdl_int XdevLUIRAI::update() {
			return XdevLModuleAutoImpl<XdevLUI>::update();
		}

		xdl_int XdevLUIRAI::notify(xdl::XdevLEvent& event) {


			if(event.type == TextInputEvent.getHashCode()) {
				if(8 == event.textinput.text[0]) {
					onBackspace();
				} else {
					onTextInput(event);
				}
			} else if(event.type == ButtonPressed.getHashCode()) {
//				if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 1);
//				} else if((event.key.keycode == KEY_LCTRL) || (event.key.keycode == KEY_RCTRL)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 1);
//				} else if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 1);
//				} else if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 1);
//				}
//
			} else if(event.type == ButtonReleased.getHashCode()) {

//				if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 0);
//				} else if((event.key.keycode == KEY_LCTRL) || (event.key.keycode == KEY_RCTRL)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 0);
//				} else if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 0);
//				} else if((event.key.keycode == KEY_LSHIFT) || (event.key.keycode == KEY_RSHIFT)) {
//					nk_input_key(&m_ctx, NK_KEY_SHIFT, 0);
//				}
//
			} else if(event.type == MouseButtonPressed.getHashCode()) {
				m_previousButtonEvent = m_currentButtonEvent;
				m_currentButtonEvent = *(XdevLMouseButtonEvent*)(&event);
				const auto position = convert(XdevLUIPosition(m_currentButtonEvent.x, m_currentButtonEvent.y));
				onPress(position);
//				if(event.button.button == BUTTON_LEFT) {
//					nk_input_button(&m_ctx, NK_BUTTON_LEFT, event.button.x, event.button.y, 1);
//				} else if(event.button.button == BUTTON_RIGHT) {
//					nk_input_button(&m_ctx, NK_BUTTON_RIGHT, event.button.x, event.button.y, 1);
//				} else if(event.button.button == BUTTON_MIDDLE) {
//					nk_input_button(&m_ctx, NK_BUTTON_MIDDLE, event.button.x, event.button.y, 1);
//				}
			} else if(event.type == MouseButtonReleased.getHashCode()) {
				m_previousButtonEvent = m_currentButtonEvent;
				m_currentButtonEvent = *(XdevLMouseButtonEvent*)(&event);

				const auto position = convert(XdevLUIPosition(m_currentButtonEvent.x, m_currentButtonEvent.y));
				onRelease(position);

				if(m_currentButtonEvent.timestamp - m_previousButtonEvent.timestamp < 300 * 1000) {
					const auto position = convert(XdevLUIPosition(m_currentButtonEvent.x, m_currentButtonEvent.y));
					onClicked(position);
				}

//				if(event.button.button == BUTTON_LEFT) {
//					nk_input_button(&m_ctx, NK_BUTTON_LEFT, event.button.x, event.button.y, 0);
//				} else if(event.button.button == BUTTON_RIGHT) {
//					nk_input_button(&m_ctx, NK_BUTTON_RIGHT, event.button.x, event.button.y, 0);
//				} else if(event.button.button == BUTTON_MIDDLE) {
//					nk_input_button(&m_ctx, NK_BUTTON_MIDDLE, event.button.x, event.button.y, 0);
//				}
			} else if(event.type == MouseMouseMotion.getHashCode()) {
				auto buttonMotionEvent = *(XdevLMouseMotionEvent*)(&event);
				const auto position = convert(XdevLUIPosition(buttonMotionEvent.x, buttonMotionEvent.y));
				onHovered(position);
//				float sx = (float)(32768 + event.motion.x)/65536*m_window->getWidth();
//				float sy = (float)m_window->getHeight() - (float)(32768 + event.motion.y)/65536*m_window->getHeight();
//				if(m_ctx.input.mouse.grabbed) {
//					int x = (int)m_ctx.input.mouse.prev.x;
//					int y = (int)m_ctx.input.mouse.prev.y;
//					nk_input_motion(&m_ctx, x + event.motion.xrel, y + event.motion.yrel);
//				} else {
//					nk_input_motion(&m_ctx, sx, sy);
//				}
			}

			return XdevLModuleAutoImpl<XdevLUI>::notify(event);
		}

		XdevLUIPosition XdevLUIRAI::convert(XdevLUIPosition cursorPositionRaw) {
			xdl_float x = (cursorPositionRaw.x + 32768)/65535.0f * m_window->getWidth();
//			xdl_float y = (65535 - (cursorPositionRaw.y + 32768))/65535.0f * m_window->getHeight();
			xdl_float y = ((cursorPositionRaw.y + 32768))/65535.0f * m_window->getHeight();
			return XdevLUIPosition(x, y);
		}

		void XdevLUIRAI::onPress(const XdevLUIPosition& cursorPosition) {
			for(const auto& surface : m_surfaces) {
				if(isPointInside(cursorPosition, *surface)) {
					surface->press();
				}
			}
		}

		void XdevLUIRAI::onRelease(const XdevLUIPosition& cursorPosition) {
			for(const auto& surface : m_surfaces) {
				if(isPointInside(cursorPosition, *surface)) {
					surface->release();
				}
			}
		}

		void XdevLUIRAI::onClicked(const XdevLUIPosition& cursorPosition) {
			for(const auto& surface : m_surfaces) {
				if(isPointInside(cursorPosition, *surface)) {
					surface->clicked();
				}
			}
		}

		void XdevLUIRAI::onHovered(const XdevLUIPosition& cursorPosition) {
			for(const auto& surface : m_surfaces) {
				if(isPointInside(cursorPosition, *surface)) {
					surface->hover();
				} else {
					surface->resetState();
				}
			}
		}

		void XdevLUIRAI::onTextInput(xdl::XdevLEvent& event) {
			for(const auto& surface : m_surfaces) {
				if(surface->hasFocus()) {
					surface->key(event);
					break;
				}
			}
		}

		void XdevLUIRAI::onBackspace() {
			for(const auto& surface : m_surfaces) {
				if(surface->hasFocus()) {
					surface->onControlCommand(XdevLCommandControl::BACKSPACE);
				}
			}
		}

		xdl_float XdevLUIRAI::getFPS() {
			return 0.0f;
		}

		xdl_float XdevLUIRAI::getJitter() {
			return 0.0f;
		}

		void XdevLUIRAI::add(XdevLUIText& text) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&text);
		}

		void XdevLUIRAI::add(XdevLUICheckBox& checkbox) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&checkbox);
		}

		void XdevLUIRAI::add(XdevLUIRadioButton& radiobutton) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&radiobutton);
		}

		void XdevLUIRAI::add(XdevLUISlider& slider) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&slider);
		}

		void XdevLUIRAI::add(XdevLUISliderAngle& sliderAngle) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&sliderAngle);
		}

		void XdevLUIRAI::add(XdevLUIButton& button) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&button);
		}

		void XdevLUIRAI::add(XdevLUIComboBox& comboBox) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&comboBox);
		}

		void XdevLUIRAI::add(XdevLUITextInputField& textInputField) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&textInputField);
		}

		void XdevLUIRAI::add(XdevLUIVerticalBox& verticalBox) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			m_surfaces.push_back(&verticalBox);
		}
	}
}
