/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/


#include <XdevL.h>
#include <XdevLInput/XdevLInputSystem.h>
#include <XdevLRAI/XdevLRAI.h>

#include "XdevLUIRAI.h"
#include <cmath>

namespace xdl {

	namespace ui {

		template<typename T>
		xdl_float* ortho(T left, T right, T bottom, T top, T near_plane_distance, T far_plane_distance, xdl_float* mat) {
			T d = (right-left);
			T c = (top-bottom);
			mat[0] = 2.0f/d;
			mat[1] = 0.0f;
			mat[2] = 0.0f;
			mat[3] = 0.0f;
			mat[4] = 0.0f;
			mat[5] = 2.0f/c;
			mat[6] = 0.0f;
			mat[7] = 0.0f;
			mat[8] = 0.0f;
			mat[9] = 0.0f;
			mat[10] = -2.0f / (far_plane_distance - near_plane_distance);
			mat[11] = 0.0f;
			mat[12] = -(right + left) / d;
			mat[13] = -(top + bottom) / c;
			mat[14] = -(far_plane_distance + near_plane_distance) / (far_plane_distance - near_plane_distance);
			mat[15] = 1.0f;


			return mat;
		}


//
// XdevLShaders
//

		const xdl_char *vertexShader =
		  "layout(location = 0) in vec2 iposition;\n"
		  "layout(location = 1) in vec2 iuv;\n"
		  "layout(location = 2) in vec4 icolor;\n"
		  "out vec2 fuv;\n"
		  "out vec4 fcolor;\n"
		  "uniform mat4 projMatrix;\n"
		  "void main()\n"
		  "{\n"
		  "	fuv = iuv;\n"
		  "	fcolor = icolor;\n"
		  "	gl_Position = projMatrix * vec4(iposition.xy,0,1);\n"
		  "}\n";

		const xdl_char* fragmentShader =
		  "uniform sampler2D tex;\n"
		  "in vec2 fuv;\n"
		  "in vec4 fcolor;\n"
		  "out vec4 ocolor;\n"
		  "void main()\n"
		  "{\n"
		  "	ocolor = fcolor;\n"
		  "}\n";

		xdl_int XdevLUIRenderTraverserRAI::create(IPXdevLWindow window, IPXdevLRAI rai, IPXdevLTextLayout textLayout) {
			m_window = window;
			m_rai = rai;
			m_textLayoutSystem = textLayout;

			m_vertexShader = m_rai->createVertexShader();
			m_fragmentShader = m_rai->createFragmentShader();
			m_shaderProgram = m_rai->createShaderProgram();

			m_vertexShader->addShaderCode(vertexShader);
			m_fragmentShader->addShaderCode(fragmentShader);

			m_vertexShader->compile();
			m_fragmentShader->compile();

			m_shaderProgram->attach(m_vertexShader);
			m_shaderProgram->attach(m_fragmentShader);
			m_shaderProgram->link();

			m_attribLocationProjMtx = m_shaderProgram->getUniformLocation("projMatrix");
			m_attribLocationPosition = m_shaderProgram->getAttribLocation("iposition");
			m_attribLocationUV = m_shaderProgram->getAttribLocation("iuv");
			m_attribLocationColor = m_shaderProgram->getAttribLocation("icolor");

			m_indexBuffer = m_rai->createIndexBuffer();
			m_indexBuffer->init(XdevLBufferElementTypes::UNSIGNED_SHORT);


			m_linesStripVertexDeclaration = rai->createVertexDeclaration();
			m_linesStripVertexDeclaration->add(2, XdevLBufferElementTypes::FLOAT, m_attribLocationPosition);
			m_linesStripVertexDeclaration->add(4, XdevLBufferElementTypes::FLOAT, m_attribLocationColor);

			m_linesStripVertexBuffer = m_rai->createVertexBuffer();
			m_linesStripVertexBuffer->init();

			m_linesStripVertexArray = m_rai->createVertexArray();
			m_linesStripVertexArray->init(m_linesStripVertexBuffer, m_linesStripVertexDeclaration);


			m_rectangleVertexDeclaration = rai->createVertexDeclaration();
			m_rectangleVertexDeclaration->add(2, XdevLBufferElementTypes::FLOAT, m_attribLocationPosition);
			m_rectangleVertexDeclaration->add(4, XdevLBufferElementTypes::FLOAT, m_attribLocationColor);

			m_rectangleVertexBuffer = m_rai->createVertexBuffer();
			m_rectangleVertexBuffer->init();

			m_rectangleVertexArray = m_rai->createVertexArray();
			m_rectangleVertexArray->init(m_rectangleVertexBuffer, m_rectangleVertexDeclaration);


			//
			// Vertex Buffer, Array and Decleration for rectangle lines.
			//
			m_rectangleLineVertexDeclaration = rai->createVertexDeclaration();
			m_rectangleLineVertexDeclaration->add(2, XdevLBufferElementTypes::FLOAT, m_attribLocationPosition);
			m_rectangleLineVertexDeclaration->add(4, XdevLBufferElementTypes::FLOAT, m_attribLocationColor);
			m_rectangleLineVertexBuffer = m_rai->createVertexBuffer();
			m_rectangleLineVertexBuffer->init();
			m_rectangleLineVertexArray = m_rai->createVertexArray();
			m_rectangleLineVertexArray->init(m_rectangleLineVertexBuffer, m_rectangleLineVertexDeclaration);


			m_circleVertexDeclaration = rai->createVertexDeclaration();
			m_circleVertexDeclaration->add(2, XdevLBufferElementTypes::FLOAT, m_attribLocationPosition);
			m_circleVertexDeclaration->add(4, XdevLBufferElementTypes::FLOAT, m_attribLocationColor);
			m_circleVertexBuffer = m_rai->createVertexBuffer();
			m_circleVertexBuffer->init();
			m_circleVertexArray = m_rai->createVertexArray();
			m_circleVertexArray->init(m_circleVertexBuffer, m_circleVertexDeclaration);


			return RET_SUCCESS;
		}

		void XdevLUIRenderTraverserRAI::preRender() {
		}

		void XdevLUIRenderTraverserRAI::postRender() {
			xdl_float windowX = (xdl_float)m_window->getWidth();
			xdl_float windowY = (xdl_float)m_window->getHeight();

			m_rai->setActiveBlendMode(xdl_true);
			m_rai->setBlendMode(XDEVL_BLEND_SRC_ALPHA, XDEVL_BLEND_ONE_MINUS_SRC_ALPHA);
			m_rai->setBlendFunc(XDEVL_FUNC_ADD);
			m_rai->setActiveDepthTest(xdl_false);
			m_rai->setActiveScissorTest(xdl_true);

			// Setup viewport, orthographic projection matrix
			XdevLViewPort viewport {};
			viewport.x = 0.0f;
			viewport.y = 0.0f;
			viewport.width = windowX;
			viewport.height = windowY;
			m_rai->setViewport(viewport);

			xdl_float projectionMatrix[16];
			ortho<xdl_float>(0.0f,
			                 (xdl::xdl_float)windowX,
			                 0.0f,
			                 (xdl::xdl_float)windowY,
			                 -1.0f,
			                 1.0f, projectionMatrix);

			m_shaderProgram->activate();
			m_shaderProgram->setUniformMatrix4(m_attribLocationProjMtx, 1, projectionMatrix);
			m_shaderProgram->deactivate();

			m_rai->setActiveShaderProgram(m_shaderProgram);

			//
			// Draw all rectangles.
			//
			if(m_rectangleVertexList.size() > 0) {
				m_rectangleVertexBuffer->lock();
				m_rectangleVertexBuffer->upload((xdl::xdl_uint8*)m_rectangleVertexList.data(), m_rectangleVertexDeclaration->vertexSize() * m_rectangleVertexList.size());
				m_rectangleVertexBuffer->unlock();

				m_rai->setActiveVertexArray(m_rectangleVertexArray);

				m_rai->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLES, m_rectangleVertexList.size());
				m_rectangleVertexList.clear();
			}

			//
			// Draw all lined rectangles.
			//
			if(m_rectangleLineVertexList.size() > 0) {
				m_rectangleLineVertexBuffer->lock();
				m_rectangleLineVertexBuffer->upload((xdl::xdl_uint8*)m_rectangleLineVertexList.data(), m_rectangleLineVertexDeclaration->vertexSize() * m_rectangleLineVertexList.size());
				m_rectangleLineVertexBuffer->unlock();

				m_rai->setActiveVertexArray(m_rectangleLineVertexArray);

				m_rai->drawVertexArray(xdl::XDEVL_PRIMITIVE_LINES, m_rectangleLineVertexList.size());
				m_rectangleLineVertexList.clear();
			}


			//
			// Draw all lines.
			//
			if(m_linesStripVertexList.size() > 0) {
				m_linesStripVertexBuffer->lock();
				m_linesStripVertexBuffer->upload((xdl::xdl_uint8*)m_linesStripVertexList.data(), m_linesStripVertexDeclaration->vertexSize() * m_linesStripVertexList.size());
				m_linesStripVertexBuffer->unlock();

				m_rai->setActiveVertexArray(m_linesStripVertexArray);

				m_rai->drawVertexArray(xdl::XDEVL_PRIMITIVE_LINES, m_linesStripVertexList.size());
				m_linesStripVertexList.clear();
			}

			if(m_circleVertexList.size() > 0) {
				for(auto& vertices : m_circleVertexList) {
					m_circleVertexBuffer->lock();
					m_circleVertexBuffer->upload((xdl::xdl_uint8*)vertices.data(), m_circleVertexDeclaration->vertexSize() * vertices.size());
					m_circleVertexBuffer->unlock();

					m_rai->setActiveVertexArray(m_circleVertexArray);

					m_rai->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLE_FAN, vertices.size());
				}
				m_circleVertexList.clear();
			}

			//
			// Draw all text
			//
			if(m_textList.size() > 0) {
				for(auto& text : m_textList) {
					m_textLayoutSystem->setColor(text.color.r, text.color.g, text.color.b, text.color.a);
					m_textLayoutSystem->printText(text.text, XdevLPositionF(text.x, text.y), text.textPivot);
				}
				m_textList.clear();
			}
		}

		void XdevLUIRenderTraverserRAI::drawLine(xdl::xdl_int x1, xdl::xdl_int y1, xdl::xdl_int x2, xdl::xdl_int y2) {
			XdevLUIVertex v1, v2;
			v1.x = x1;
			v1.y = y1;
			v1.color = m_currentColor;

			v2.x = x2;
			v2.y = y2;
			v2.color = m_currentColor;

			m_linesStripVertexList.push_back(v1);
			m_linesStripVertexList.push_back(v2);

		}

		void XdevLUIRenderTraverserRAI::drawRectLine(const XdevLUIPosition& position, const XdevLUISize& size) {
			XdevLUIVertex v1, v2, v3, v4;

			v1.x = position.x;
			v1.y = position.y;
			v1.color = m_currentColor;

			v2.x = position.x;
			v2.y = position.y + size.height;
			v2.color = m_currentColor;

			v3.x = position.x + size.width;
			v3.y = position.y + size.height;
			v3.color = m_currentColor;

			v4.x = position.x + size.width;
			v4.y = position.y;
			v4.color = m_currentColor;

			m_rectangleLineVertexList.push_back(v1);
			m_rectangleLineVertexList.push_back(v2);

			m_rectangleLineVertexList.push_back(v2);
			m_rectangleLineVertexList.push_back(v3);

			m_rectangleLineVertexList.push_back(v3);
			m_rectangleLineVertexList.push_back(v4);

			m_rectangleLineVertexList.push_back(v4);
			m_rectangleLineVertexList.push_back(v1);

		}


		void XdevLUIRenderTraverserRAI::drawRect(const XdevLUIPosition& position, const XdevLUISize& size) {
			XdevLUIVertex v1, v2, v3, v4;

			v1.x = position.x;
			v1.y = position.y;
			v1.color = m_currentColor;

			v2.x = position.x;
			v2.y = position.y + size.height;
			v2.color = m_currentColor;

			v3.x = position.x + size.width;
			v3.y = position.y + size.height;
			v3.color = m_currentColor;

			v4.x = position.x + size.width;
			v4.y = position.y;
			v4.color = m_currentColor;

			m_rectangleVertexList.push_back(v1);
			m_rectangleVertexList.push_back(v2);
			m_rectangleVertexList.push_back(v3);

			m_rectangleVertexList.push_back(v1);
			m_rectangleVertexList.push_back(v3);
			m_rectangleVertexList.push_back(v4);

		}

		void XdevLUIRenderTraverserRAI::drawCircle(const XdevLUIPosition& position, xdl_float radius) {

			std::vector<XdevLUIVertex> vertices;

			// Add the center of the triangle fan.
			XdevLUIVertex center {};
			center.x = position.x;
			center.y = position.y;
			center.color = m_currentColor;
			vertices.push_back(center);

			// Add triangle fan vertices edging the circle.
			xdl_float seg = 24.0;
			auto anglePerSegment = (2 * M_PI)/seg;

			for(xdl_float angle = 0.0f; angle <= 2 * M_PI + anglePerSegment; angle += anglePerSegment) {
				XdevLUIVertex v {};
				v.x = position.x + radius * sin(angle);
				v.y = position.y + radius * cos(angle);
				v.color = m_currentColor;
				vertices.push_back(v);
			}
			m_circleVertexList.push_back(vertices);
		}

		void XdevLUIRenderTraverserRAI::drawText(const XdevLString& text, const XdevLUIPosition& position, XdevLTextPivot pivot) {
			XdevLUITextInfo textInfo;
			textInfo.text = text;
			textInfo.x = position.x;
			textInfo.y = position.y;
			textInfo.color = m_currentColor;
			textInfo.textPivot = pivot;

			m_textList.push_back(textInfo);
		}

		void XdevLUIRAI::parse(XdevLUISurface const* root) {
			if(root == nullptr) {
				return;
			}

			switch(root->getWigetType()) {
				case XdevLUIWidgetType::SURFACE: {
					XdevLUISurface* surface = (XdevLUISurface*)root;
					m_surfaces.push_back(surface);
				} break;
				case XdevLUIWidgetType::VBOX: {
					XdevLUIVerticalBox* vbox = (XdevLUIVerticalBox*)root;
					m_surfaces.push_back(vbox);
				} break;
				case XdevLUIWidgetType::HBOX: {
					XdevLUIHorizontalBox* hbox = (XdevLUIHorizontalBox*)root;
					m_surfaces.push_back(hbox);
				} break;
				case XdevLUIWidgetType::BUTTON: {
					XdevLUIButton* button = (XdevLUIButton*)root;
					m_surfaces.push_back(button);
				} break;
				case XdevLUIWidgetType::CHECKBOX: {
					XdevLUICheckBox* checkbox = (XdevLUICheckBox*)root;
					m_surfaces.push_back(checkbox);
				} break;
				case XdevLUIWidgetType::RADIOBUTTON: {
					XdevLUIRadioButton* radiobutton = (XdevLUIRadioButton*)root;
					m_surfaces.push_back(radiobutton);
				} break;
				case XdevLUIWidgetType::TEXT: {
					XdevLUIText* text = (XdevLUIText*)root;
					m_surfaces.push_back(text);
				} break;
				case XdevLUIWidgetType::SLIDER: {
					XdevLUISlider* slider = (XdevLUISlider*)root;
					m_surfaces.push_back(slider);
				} break;
				case XdevLUIWidgetType::SLIDERANGLE: {
					XdevLUISliderAngle* sliderAngle = (XdevLUISliderAngle*)root;
					m_surfaces.push_back(sliderAngle);
				} break;
				case XdevLUIWidgetType::COMBOBOX: {
					XdevLUIComboBox* combobox = (XdevLUIComboBox*)root;
					m_surfaces.push_back(combobox);
				} break;
				case XdevLUIWidgetType::TEXTINPUT: {
					XdevLUITextInputField* textinput = (XdevLUITextInputField*)root;
					m_surfaces.push_back(textinput);
				} break;
				default: break;
			}

			for(auto& surface : root->getSurfaces()) {
				parse(surface);
			}
		}

		XdevLUIColor XdevLUIRenderTraverserRAI::getStateColor(XdevLUIWidget* widget) {
			if((widget->getState() & XdevLUIState::HOVER) && !(widget->getState() & XdevLUIState::PRESS)) {
				return widget->getHoverColor();
			} else if(widget->getState() & XdevLUIState::PRESS) {
				return widget->getPressColor();
			} else {
				return widget->getBackgroundColor();
			}
			return widget->getBackgroundColor();
		}

		//
		// Draw XdevLUIButton
		//
		void XdevLUIRenderTraverserRAI::render(XdevLUIButton* button) {
			auto position = button->getPivotPosition();
			if(button->getParent()) {
				button->getParent()->getPivotPosition();
			}

			//
			// Draw border.
			//
			m_currentColor = XdevLUIColor(0.7f, 0.7f, 0.7f, 1.0f);
			drawRectLine(button->getPivotPosition(), button->getSize());

			m_currentColor = getStateColor(button);
			drawRect(button->getPivotPosition(), button->getSize());

			m_currentColor = button->getForegroundColor();

			position.x += button->getSize().width * 0.5f;
			position.y += button->getSize().height * 0.5f;

			drawText(button->getLabel(), position);
		}

		//
		// Draw XdevLUIText
		//
		void XdevLUIRenderTraverserRAI::render(XdevLUIText* text) {
			m_currentColor = text->getForegroundColor();
			auto position = text->getPivotPosition();
			position.x += text->getSize().width * 0.5f;
			position.y += text->getSize().height * 0.5f;
			drawText(text->getLabel(), position);
		}

		//
		// Draw XdevLUICheckBox
		//
		void XdevLUIRenderTraverserRAI::render(XdevLUICheckBox* checkbox) {
			//
			// Draw border.
			//
			m_currentColor = XdevLUIColor(0.7f, 0.7f, 0.7f, 1.0f);
			drawRectLine(checkbox->getPivotPosition(), checkbox->getSize());

			m_currentColor = getStateColor(checkbox);
			drawRect(checkbox->getPivotPosition(), checkbox->getSize());

			if(checkbox->isChecked()) {
				m_currentColor = checkbox->getForegroundColor();
				auto size = checkbox->getSize() * 0.5f;
				auto position = checkbox->getPivotPosition() + size * 0.5f;
				drawRect(position, size);
			}

			m_currentColor = checkbox->getForegroundColor();
			auto size = checkbox->getSize();
			auto position = checkbox->getPivotPosition();
			position.x += size.width + 5.0f;
			position.y += size.height * 0.5f;
			drawText(checkbox->getLabel(), position, XdevLTextPivot::CENTER_LEFT);
		}

		//
		// Draw XdevLUIRadioButton
		//
		void XdevLUIRenderTraverserRAI::render(XdevLUIRadioButton* radioButton) {
			auto position = radioButton->getPivotPosition() + radioButton->getSize()*0.5f;
			auto size = radioButton->getSize();
			auto radius = sqrt(size.width * size.width + size.height * size.height) * 0.5f;
			m_currentColor = getStateColor(radioButton);
			drawCircle(position, radius);
			if(radioButton->isActive()) {
				m_currentColor = radioButton->getForegroundColor();
				drawCircle(position, radius * 0.5f);
			}


			m_currentColor = radioButton->getForegroundColor();
			auto textPosition = radioButton->getPivotPosition();
			textPosition.x += size.width + 10.0f;
			textPosition.y += size.height * 0.5f;
			drawText(radioButton->getLabel(), textPosition, XdevLTextPivot::CENTER_LEFT);
		}

		//
		// Draw XdevLUITextInputField
		//
		void XdevLUIRenderTraverserRAI::render(XdevLUITextInputField* textInputField) {
			//
			// Draw border.
			//
			m_currentColor = XdevLUIColor(0.7f, 0.7f, 0.7f, 1.0f);
			drawRectLine(textInputField->getPivotPosition(), textInputField->getSize());

			m_currentColor = getStateColor(textInputField);
			drawRect(textInputField->getPivotPosition(), textInputField->getSize());

			auto time = m_timer.getTime();

			// Do the blinking.
			if(textInputField->hasFocus()) {
				if(time < 1.0f) {
					m_currentColor = XdevLUIColor(1.0f, 1.0f, 1.0f, 1.0f);
					drawRect(textInputField->getPivotPosition(), XdevLUISize(2.0f, textInputField->getSize().height));
				} else if(time > 2.0f) {
					m_timer.reset();
				}
			}
			auto position = textInputField->getPivotPosition();
			position.y += textInputField->getSize().height * 0.5f;

			m_currentColor = textInputField->getForegroundColor();
			drawText(textInputField->getText(), position, XdevLTextPivot::CENTER_LEFT);


			m_currentColor = textInputField->getForegroundColor();
			auto size = textInputField->getSize();
			auto textPosition = textInputField->getPivotPosition();
			textPosition.x += size.width + 10.0f;
			textPosition.y += size.height * 0.5f;
			drawText(textInputField->getLabel(), textPosition, XdevLTextPivot::CENTER_LEFT);
		}

		void XdevLUIRenderTraverserRAI::render(XdevLUIVerticalBox* verticalBox) {
			for(auto& surface : verticalBox->getSurfaces()) {
				surface->render(this);
			}
		}

	}
}
