/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_UI_RENDER_TRAVERSER_RAI_H
#define XDEVL_UI_RENDER_TRAVERSER_RAI_H

#include <XdevLUI/XdevLUI.h>

#include <XdevLPluginImpl.h>

namespace xdl {

	namespace ui {

		struct XdevLUIVertex {
			XdevLUIVertex() :
				x(0.0f),
				y(0.0f),
				color(0.0f, 0.0f, 0.0f, 1.0f) {

			}
			xdl_float x;
			xdl_float y;

			XdevLUIColor color;
		};

		struct XdevLUITextInfo {
			XdevLUITextInfo()
				: x(0)
				, y(0)
				, color(0.0f, 0.0f, 0.0f, 1.0f)
				, textPivot(XdevLTextPivot::CENTER_CENTER)
			{}

			XdevLString text;
			xdl_float x;
			xdl_float y;
			XdevLUIColor color;
			XdevLTextPivot textPivot;
		};

		class XdevLUIRenderTraverserRAI : public XdevLUIRenderTraverser {
			public:
				XdevLUIRenderTraverserRAI()
					: m_window(nullptr)
					, m_rai(nullptr)
					, m_currentColor(XdevLUIColor(255, 255, 255, 255)) {

				}
				virtual ~XdevLUIRenderTraverserRAI() {}

				xdl_int create(IPXdevLWindow window, IPXdevLRAI rai, IPXdevLTextLayout textLayout);

				void render(XdevLUIButton* button) override final;
				void render(XdevLUIText* text) override final;
				void render(XdevLUICheckBox* checkbox) override final;
				void render(XdevLUIRadioButton* radioButton) override final;
				void render(XdevLUITextInputField* textInputField) override final;
				void render(XdevLUIVerticalBox* verticalBox) override final;

				void preRender() override final;
				void postRender() override final;

			private:

				void drawLine(xdl_int x1, xdl_int y1, xdl_int x2, xdl_int y2);
				void drawRectLine(const XdevLUIPosition& position, const XdevLUISize& size);
				void drawRect(const XdevLUIPosition& position, const XdevLUISize& size);
				void drawCircle(const XdevLUIPosition& position, xdl_float radius);
				void drawText(const XdevLString& text, const XdevLUIPosition& position, XdevLTextPivot pivot = XdevLTextPivot::CENTER_CENTER);

				XdevLUIColor getStateColor(XdevLUIWidget* widget);

			private:

				IPXdevLWindow m_window;
				IPXdevLRAI m_rai;

				IPXdevLFont	m_font;
				IPXdevLFontSystem	m_fontSystem;
				IPXdevLTextLayout m_textLayoutSystem;

				IPXdevLVertexShader m_vertexShader;
				IPXdevLFragmentShader m_fragmentShader;
				IPXdevLShaderProgram m_shaderProgram;

				IPXdevLIndexBuffer m_indexBuffer;

				IPXdevLVertexArray			m_linesStripVertexArray;
				IPXdevLVertexBuffer		m_linesStripVertexBuffer;
				IPXdevLVertexDeclaration	m_linesStripVertexDeclaration;

				IPXdevLVertexArray			m_rectangleLineVertexArray;
				IPXdevLVertexBuffer		m_rectangleLineVertexBuffer;
				IPXdevLVertexDeclaration	m_rectangleLineVertexDeclaration;

				IPXdevLVertexArray			m_rectangleVertexArray;
				IPXdevLVertexBuffer		m_rectangleVertexBuffer;
				IPXdevLVertexDeclaration	m_rectangleVertexDeclaration;

				IPXdevLVertexArray			m_circleVertexArray;
				IPXdevLVertexBuffer		m_circleVertexBuffer;
				IPXdevLVertexDeclaration	m_circleVertexDeclaration;


				xdl_int m_attribLocationProjMtx;
				xdl_int m_attribLocationPosition;
				xdl_int m_attribLocationUV;
				xdl_int m_attribLocationColor;

				XdevLUIColor m_currentColor;
				XdevLTimer m_timer;

				std::vector<XdevLUIVertex> 	m_linesStripVertexList;
				std::vector<XdevLUIVertex> 	m_rectangleLineVertexList;
				std::vector<XdevLUIVertex> 	m_rectangleVertexList;
				std::vector<std::vector<XdevLUIVertex>> 	m_circleVertexList;
				std::vector<XdevLUITextInfo> m_textList;
		};
	}
}
#endif
