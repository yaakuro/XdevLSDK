/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_UI_RAI_H
#define XDEVL_UI_RAI_H

#include <XdevLUI/XdevLUI.h>
#include "XdevLUIRenderTraverserRAI.h"
#include <XdevLPluginImpl.h>

namespace xdl {

	namespace ui {

		static const XdevLString pluginName {
			"XdevLUI"
		};

		static const XdevLString description {
			"This plugins supports UI rendering using RAI."
		};

		static std::vector<XdevLModuleName>	moduleNames {
			XdevLModuleName("XdevLUI")
		};

		/**
		 * @class XdevLUINuklear
		 * @brief Implementation of the XdevLUI class using ImGUI and XdevLRAI.
		 */
		class XdevLUIRAI : public XdevLModuleAutoImpl<XdevLUI> {
			public:

				XdevLUIRAI(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

				virtual ~XdevLUIRAI();

				xdl_int init() override final;
				xdl_int shutdown() override final;
				xdl_int update() override final;

				xdl_int notify(XdevLEvent& event) override final;

				xdl_int create(IPXdevLWindow window, IPXdevLRAI rai, IPXdevLArchive archive) override final;
				xdl_int setupFontFromFile(std::shared_ptr<XdevLFile>& fontFile) override final;
				xdl_int setupFont(IPXdevLFont	font) override final;

				xdl_int lock(const XdevLString& title) override final;
				xdl_int unlock() override final;
				xdl_int render() override final;

				xdl_float getFPS() override final;
				xdl_float getJitter() override final;

				void add(XdevLUIText& text) override final;
				void add(XdevLUIButton& button) override final;
				void add(XdevLUICheckBox& checkbox) override final;
				void add(XdevLUIRadioButton& checkbox) override final;
				void add(XdevLUISlider& slider) override final;
				void add(XdevLUISliderAngle& sliderAngle) override final;
				void add(XdevLUIComboBox& comboBox) override final;
				void add(XdevLUITextInputField& textInputField) override final;
				void add(XdevLUIVerticalBox& verticalBox) override final;

				void parse(XdevLUISurface const* root) override final;
				void setFontSize(const XdevLUISize& size) override final;

			private:

				void onPress(const XdevLUIPosition& cursorPosition);
				void onRelease(const XdevLUIPosition& cursorPosition);
				void onClicked(const XdevLUIPosition& cursorPosition);
				void onHovered(const XdevLUIPosition& cursorPosition);
				void onTextInput(xdl::XdevLEvent& event);
				void onBackspace();

				XdevLUIPosition convert(XdevLUIPosition cursorPositionRaw);

				inline xdl_bool isPointInside(const XdevLUIPosition& cursorPosition, const XdevLUISurface& surface) {
					if(surface.getSurfaces().size() > 0) {
						for(auto& childSurface : surface.getSurfaces()) {
							if(isPointInside(cursorPosition, *childSurface)) {
								return true;
							}
						}
						return false;
					}

					auto surfacePosition = surface.getPivotPosition();
					auto surfaceSize = surface.getSize();
					return ((cursorPosition.x >= surfacePosition.x) &&
					        (cursorPosition.x <= surfacePosition.x + surfaceSize.width) &&
					        (cursorPosition.y >= surfacePosition.y) &&
					        (cursorPosition.y <= surfacePosition.y + surfaceSize.height)
					       );
				}

			private:

				IPXdevLWindow m_window;
				IPXdevLRAI m_rai;
				IPXdevLCursor m_cursor;

				IPXdevLFont	m_font;
				IPXdevLFontSystem	m_fontSystem;
				IPXdevLTextLayout m_textLayoutSystem;

				std::vector<XdevLUISurface*> m_surfaces;

				XdevLMouseButtonEvent m_previousButtonEvent;
				XdevLMouseButtonEvent m_currentButtonEvent;

				IPXdevLUIRenderTraverser m_renderTraverser;
				xdl_bool m_lock;
		};
	}
}
#endif
