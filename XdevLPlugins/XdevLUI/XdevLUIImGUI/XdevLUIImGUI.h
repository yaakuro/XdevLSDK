/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_UI_IMGUI_H
#define XDEVL_UI_IMGUI_H

#include <XdevLUI/XdevLUI.h>
#include <XdevLPluginImpl.h>

#include <imgui.h>

namespace xdl {

	namespace ui {
		static const XdevLString pluginName {
			"XdevLUI"
		};

		static const XdevLString description {
			"This plugins supports UI rendering using RAI."
		};

		static std::vector<XdevLModuleName>	moduleNames {
			XdevLModuleName("XdevLUI")
		};

		/**
		 * @class XdevLUIImGUI
		 * @brief Implementation of the XdevLUI class using ImGUI and XdevLRAI.
		 */
		class XdevLUIImGUI : public XdevLModuleImpl<XdevLUI> {
			public:

				XdevLUIImGUI(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

				virtual ~XdevLUIImGUI();

				xdl_int init() override final;
				xdl_int shutdown() override final;
				xdl_int notify(xdl::XdevLEvent& event) override final;

				xdl_int create(IPXdevLWindow window, IPXdevLRAI rai, IPXdevLArchive archive) override final;
				xdl_int setupFontFromFile(std::shared_ptr<XdevLFile>& fontFile) override final;
				xdl_int setupFont(IPXdevLFont	font) override final;

				xdl_int lock(const XdevLString& title) override final;
				xdl_int unlock() override final;
				xdl_int render() override final;

				xdl_float getFPS() override final;
				xdl_float getJitter() override final;

				void add(XdevLUIText& text) override final;
				void add(XdevLUIButton& button) override final;
				void add(XdevLUICheckBox& checkbox) override final;
				void add(XdevLUIRadioButton& checkbox) override final;
				void add(XdevLUISlider& slider) override final;
				void add(XdevLUISliderAngle& sliderAngle) override final;
				void add(XdevLUIComboBox& comboBox) override final;
				void add(XdevLUITextInputField& textInputField) override final;
				void add(XdevLUIVerticalBox& verticalBox) override final;

				void setFontSize(const XdevLUISize& size) override final;

			private:

				IPXdevLWindow m_window;
				IPXdevLRAI m_rai;
				IPXdevLVertexShader m_vertexShader;
				IPXdevLFragmentShader m_fragmentShader;
				IPXdevLShaderProgram m_shaderProgram;
				IPXdevLTexture m_texture;
				IPXdevLVertexArray m_vertexArray;

				IPXdevLCursor m_cursor;
				xdl_bool m_lock;
				xdl_bool m_initialized;

		};

	}
}
#endif
