/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLInput/XdevLInputSystem.h>

#include "XdevLUIImGUI.h"

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::ui::pluginName,
	xdl::ui::moduleNames,
	XDEVLGUI_PLUGIN_MAJOR_VERSION,
	XDEVLGUI_PLUGIN_MINOR_VERSION,
	XDEVLGUI_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor imGUIModuleDesc {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::ui::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::ui::description,
	XDEVLGUI_MODULE_MAJOR_VERSION,
	XDEVLGUI_MODULE_MINOR_VERSION,
	XDEVLGUI_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::ui::XdevLUIImGUI, imGUIModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

namespace xdl {

	namespace ui {
		static const xdl::XdevLID ButtonPressed("XDEVL_BUTTON_PRESSED");
		static const xdl::XdevLID ButtonReleased("XDEVL_BUTTON_RELEASED");
		static const xdl::XdevLID MouseButtonPressed("XDEVL_MOUSE_BUTTON_PRESSED");
		static const xdl::XdevLID MouseButtonReleased("XDEVL_MOUSE_BUTTON_RELEASED");
		static const xdl::XdevLID MouseMouseMotion("XDEVL_MOUSE_MOTION");
		static const XdevLID TextInputEvent("XDEVL_TEXT_INPUT");

		static xdl::IPXdevLRAI RAI;
		static xdl::IPXdevLShaderProgram shaderProgram;
		static xdl::IPXdevLVertexArray vertexArray;
		static xdl::IPXdevLVertexBuffer vertexBuffer;
		static xdl::IPXdevLIndexBuffer indexBuffer;
		static xdl::IPXdevLTexture texture;

		static int g_AttribLocationTex = 0, g_AttribLocationProjMtx = 0;
		static int g_AttribLocationPosition = 0, g_AttribLocationUV = 0, g_AttribLocationColor = 0;


//
// XdevLShaders
//

		const xdl_char *vertexShader =
		  "layout(location = 0) in vec2 iposition;\n"
		  "layout(location = 1) in vec2 iuv;\n"
		  "layout(location = 2) in vec4 icolor;\n"
		  "out vec2 fuv;\n"
		  "out vec4 fcolor;\n"
		  "uniform mat4 projMatrix;\n"
		  "void main()\n"
		  "{\n"
		  "	fuv = iuv;\n"
		  "	fcolor = icolor;\n"
		  "	gl_Position = projMatrix * vec4(iposition.xy,0,1);\n"
		  "}\n";

		const xdl_char* fragmentShader =
		  "uniform sampler2D tex;\n"
		  "in vec2 fuv;\n"
		  "in vec4 fcolor;\n"
		  "out vec4 ocolor;\n"
		  "void main()\n"
		  "{\n"
		  "	ocolor = fcolor * texture( tex, fuv.st);\n"
		  "}\n";

		void ImGUIRenderDrawLists(ImDrawData* draw_data) {
			// Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
			ImGuiIO& io = ImGui::GetIO();
			xdl_int fb_width = (xdl_int)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
			xdl_int fb_height = (xdl_int)(io.DisplaySize.y * io.DisplayFramebufferScale.y);
			if(fb_width == 0 || fb_height == 0) {
				return;
			}

			draw_data->ScaleClipRects(io.DisplayFramebufferScale);

			// Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
			RAI->setActiveBlendMode(xdl_true);
			RAI->setBlendMode(XDEVL_BLEND_SRC_ALPHA, XDEVL_BLEND_ONE_MINUS_SRC_ALPHA);
			RAI->setBlendFunc(XDEVL_FUNC_ADD);
			RAI->setActiveDepthTest(xdl_false);
			RAI->setActiveScissorTest(xdl_true);

			// Setup viewport, orthographic projection matrix
			XdevLViewPort viewPort {};
			viewPort.x = 0.0f;
			viewPort.y = 0.0f;
			viewPort.width = static_cast<xdl_float>(fb_width);
			viewPort.height = static_cast<xdl_float>(fb_height);

			RAI->setViewport(viewPort);
			const float ortho_projection[4][4] = {
				{ 2.0f/io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
				{ 0.0f,                  2.0f/-io.DisplaySize.y, 0.0f, 0.0f },
				{ 0.0f,                  0.0f,                  -1.0f, 0.0f },
				{-1.0f,                  1.0f,                   0.0f, 1.0f },
			};

			shaderProgram->activate();
			shaderProgram->setUniformMatrix4(g_AttribLocationProjMtx, 1, &ortho_projection[0][0]);
			shaderProgram->setUniformi(g_AttribLocationTex, 0);
			shaderProgram->deactivate();

			RAI->setActiveVertexArray(vertexArray);
			RAI->setActiveShaderProgram(shaderProgram);
			texture->activate(XdevLTextureStage::STAGE0);


			for(int n = 0; n < draw_data->CmdListsCount; n++) {
				const ImDrawList* cmd_list = draw_data->CmdLists[n];
				xdl_uint64 idx_buffer_offset = 0;

				vertexBuffer->lock();
				vertexBuffer->upload((xdl_uint8*)cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
				vertexBuffer->unlock();

				indexBuffer->lock();
				indexBuffer->upload((xdl_uint8*)cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
				indexBuffer->unlock();

				for(int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++) {
					const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
					if(pcmd->UserCallback) {
						pcmd->UserCallback(cmd_list, pcmd);
					} else {
						RAI->setScissor(pcmd->ClipRect.x, fb_height - pcmd->ClipRect.w, pcmd->ClipRect.z - pcmd->ClipRect.x, pcmd->ClipRect.w - pcmd->ClipRect.y);
						RAI->drawVertexArray(xdl::XDEVL_PRIMITIVE_TRIANGLES, pcmd->ElemCount, idx_buffer_offset);
					}
					idx_buffer_offset += pcmd->ElemCount;
				}

			}

		}


		static const char* ImGUIGetClipboardText(void* data) {
			return nullptr;
		}

		static void ImGUISetClipboardText(void* data, const char* text) {

		}



		XdevLUIImGUI::XdevLUIImGUI(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
			XdevLModuleImpl<XdevLUI>(parameter, descriptor),
			m_window(nullptr),
			m_rai(nullptr),
			m_cursor(nullptr),
			m_lock(xdl_false),
			m_initialized(xdl_false) {

		}

		XdevLUIImGUI::~XdevLUIImGUI() {

		}

		xdl_int XdevLUIImGUI::init() {
			ImGui::CreateContext(nullptr);
			ImGuiIO& io = ImGui::GetIO();
			io.KeyMap[ImGuiKey_Tab] = KEY_TAB;
			io.KeyMap[ImGuiKey_LeftArrow] = KEY_LEFT;
			io.KeyMap[ImGuiKey_RightArrow] = KEY_RIGHT;
			io.KeyMap[ImGuiKey_UpArrow] = KEY_UP;
			io.KeyMap[ImGuiKey_DownArrow] = KEY_DOWN;
			io.KeyMap[ImGuiKey_PageUp] = KEY_PAGEUP;
			io.KeyMap[ImGuiKey_PageDown] = KEY_PAGEDOWN;
			io.KeyMap[ImGuiKey_Home] = KEY_HOME;
			io.KeyMap[ImGuiKey_End] = KEY_END;
			io.KeyMap[ImGuiKey_Delete] = KEY_DELETE;
			io.KeyMap[ImGuiKey_Backspace] = KEY_BACKSPACE;
			io.KeyMap[ImGuiKey_Enter] = KEY_ENTER;
			io.KeyMap[ImGuiKey_Escape] = KEY_ESCAPE;
			io.KeyMap[ImGuiKey_A] = KEY_A;
			io.KeyMap[ImGuiKey_C] = KEY_C;
			io.KeyMap[ImGuiKey_V] = KEY_V;
			io.KeyMap[ImGuiKey_X] = KEY_X;
			io.KeyMap[ImGuiKey_Y] = KEY_Y;
			io.KeyMap[ImGuiKey_Z] = KEY_Z;

			io.RenderDrawListsFn = ImGUIRenderDrawLists;
			io.SetClipboardTextFn = ImGUISetClipboardText;
			io.GetClipboardTextFn = ImGUIGetClipboardText;
#ifdef _WIN32
			// io.ImeWindowHandle = Add here window handle.
#endif
			io.MouseDrawCursor = true;
			m_initialized = xdl_true;

			XDEVL_MODULEX_SUCCESS(XdevLUIImGUI, "ImGUI created successful.\n");
			return RET_SUCCESS;
		}

		xdl_int XdevLUIImGUI::shutdown() {
			//ImGui::Shutdown();
			m_initialized = xdl_false;
			return RET_SUCCESS;
		}

		xdl_int XdevLUIImGUI::create(IPXdevLWindow window, IPXdevLRAI rai, IPXdevLArchive archive) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");

			m_window = window;
			m_rai = rai;
			RAI = rai;

			// Create a window so that we can draw something.
			m_cursor = static_cast<XdevLCursor*>(getMediator()->createModule(XdevLModuleName("XdevLCursor"), XdevLID("XdevLUIImGUICursor")));
			if(!m_cursor) {
				XDEVL_MODULEX_ERROR(XdevLUIImGUI, "Creation of the module: 'XdevLCursor' failed.\n");
				return xdl::RET_FAILED;
			}

			if(m_cursor->attach(m_window) != xdl::RET_SUCCESS) {
				return xdl::RET_FAILED;
			}


			m_vertexShader = m_rai->createVertexShader();
			m_fragmentShader = m_rai->createFragmentShader();
			m_shaderProgram = m_rai->createShaderProgram();

			m_vertexShader->addShaderCode(vertexShader);
			m_fragmentShader->addShaderCode(fragmentShader);

			m_vertexShader->compile();
			m_fragmentShader->compile();

			m_shaderProgram->attach(m_vertexShader);
			m_shaderProgram->attach(m_fragmentShader);
			m_shaderProgram->link();

			shaderProgram = m_shaderProgram;

			g_AttribLocationTex = m_shaderProgram->getUniformLocation("tex");
			g_AttribLocationProjMtx = m_shaderProgram->getUniformLocation("projMatrix");
			g_AttribLocationPosition = m_shaderProgram->getAttribLocation("iposition");
			g_AttribLocationUV = m_shaderProgram->getAttribLocation("iuv");
			g_AttribLocationColor = m_shaderProgram->getAttribLocation("icolor");



			auto vd = rai->createVertexDeclaration();
			vd->add(2, XdevLBufferElementTypes::FLOAT, g_AttribLocationPosition);
			vd->add(2, XdevLBufferElementTypes::FLOAT, g_AttribLocationUV);
			vd->add(4, XdevLBufferElementTypes::UNSIGNED_BYTE,  g_AttribLocationColor, xdl_true);

			vertexBuffer = m_rai->createVertexBuffer();
			vertexBuffer->init();

			indexBuffer = m_rai->createIndexBuffer();
			indexBuffer->init(XdevLBufferElementTypes::UNSIGNED_SHORT);

			m_vertexArray = m_rai->createVertexArray();
			m_vertexArray->init(vertexBuffer, indexBuffer, vd);
			vertexArray = m_vertexArray;

			//
			// Create Texture.
			//
			m_texture = m_rai->createTexture();

			// Build texture atlas
			ImGuiIO& io = ImGui::GetIO();
			unsigned char* pixels;
			int width, height;
			io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);   // Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.

			m_texture->init(width, height, XdevLTextureFormat::RGBA8, XdevLTexturePixelFormat::RGBA, pixels);
			m_texture->lock();
			const XdevLTextureSamplerFilterStage filterStage {xdl::XdevLTextureFilterMin::NEAREST, xdl::XdevLTextureFilterMag::NEAREST};
			const XdevLTextureSamplerWrap wrap {XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE};
			m_texture->setTextureFilter(filterStage);
			m_texture->setTextureWrap(wrap);
			m_texture->unlock();

			texture = m_texture;

			return RET_SUCCESS;
		}

		xdl_int XdevLUIImGUI::setupFontFromFile(std::shared_ptr<XdevLFile>& fontFile) {
			return RET_SUCCESS;
		}

		xdl_int XdevLUIImGUI::setupFont(IPXdevLFont	font) {
			return RET_SUCCESS;
		}

		xdl_int XdevLUIImGUI::lock(const XdevLString& title) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			XDEVL_ASSERT(m_lock != xdl_true, "You must unlock before your can lock.\n");

			m_lock = true;

			// Setup display size (every frame to accommodate for window resizing)
			xdl_int w = m_window->getWidth();
			xdl_int h = m_window->getHeight();

			xdl_int display_w = w;
			xdl_int display_h = h;

			ImGuiIO& io = ImGui::GetIO();
			io.DisplaySize = ImVec2((float)w, (float)h);
			io.DisplayFramebufferScale = ImVec2(w > 0 ? ((float)display_w / w) : 0, h > 0 ? ((float)display_h / h) : 0);

			// Set the delta time.
			io.DeltaTime = static_cast<xdl_float>(getMediator()->getDT());

			// Hide OS mouse cursor if ImGui is drawing it.
			io.MouseDrawCursor ? m_cursor->hide() : m_cursor->show();

			// Start the frame
			ImGui::NewFrame();


			ImGui::Begin(title.toString().c_str());

			return RET_SUCCESS;
		}

		xdl_int XdevLUIImGUI::unlock() {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			XDEVL_ASSERT(m_lock == xdl_true, "You must unlock before your can lock.\n");

			ImGui::End();
			m_lock = xdl_false;
			return RET_SUCCESS;
		}

		xdl_int XdevLUIImGUI::render() {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			ImGui::Render();
			return RET_SUCCESS;
		}

		xdl_int XdevLUIImGUI::notify(xdl::XdevLEvent& event) {

			if(xdl::isModuleValid(m_window) && (xdl_true == m_initialized)) {
				ImGuiIO& io = ImGui::GetIO();

				if(event.type == TextInputEvent.getHashCode()) {
					io.AddInputCharactersUTF8(event.textinput.text);
				} else if(event.type == ButtonPressed.getHashCode()) {

					io.KeysDown[event.key.keycode] = true;

					io.KeyCtrl = io.KeysDown[KEY_LCTRL] || io.KeysDown[KEY_RCTRL];
					io.KeyShift = io.KeysDown[KEY_LSHIFT] || io.KeysDown[KEY_RSHIFT];
					io.KeyAlt = io.KeysDown[KEY_LALT] || io.KeysDown[KEY_RALT];
					io.KeySuper = io.KeysDown[KEY_LGUI] || io.KeysDown[KEY_RGUI];

				} else if(event.type == ButtonReleased.getHashCode()) {
					io.KeysDown[event.key.keycode] = false;

				} else if(event.type == MouseButtonPressed.getHashCode()) {

					if(event.button.button == BUTTON_LEFT) {
						io.MouseDown[0] = true;
					}
					if(event.button.button == BUTTON_RIGHT) {
						io.MouseDown[1] = true;
					}
					if(event.button.button == BUTTON_MIDDLE) {
						io.MouseDown[2] = true;
					}
				} else if(event.type == MouseButtonReleased.getHashCode()) {

					if(event.button.button == BUTTON_LEFT) {
						io.MouseDown[0] = false;
					}
					if(event.button.button == BUTTON_RIGHT) {
						io.MouseDown[1] = false;
					}
					if(event.button.button == BUTTON_MIDDLE) {
						io.MouseDown[2] = false;
					}
				} else if(event.type == MouseMouseMotion.getHashCode()) {
					io.MousePos = ImVec2((float)(32768 + event.motion.x)/65536*m_window->getWidth(), (float)m_window->getHeight() - (float)(32768 + event.motion.y)/65536*m_window->getHeight());
				}
			}

			return XdevLModuleImpl<XdevLUI>::notify(event);
		}

		xdl_float XdevLUIImGUI::getFPS() {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			return ImGui::GetIO().Framerate;
		}

		xdl_float XdevLUIImGUI::getJitter() {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			return (1000.0f/ImGui::GetIO().Framerate);
		}

		void XdevLUIImGUI::setFontSize(const XdevLUISize& size) {
		}

		void XdevLUIImGUI::add(XdevLUIText& text) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			ImGui::TextV((const char*)text.getLabel().toString().c_str(), text.getArgumentList());

			if(ImGui::IsItemHovered()) {
				XdevLUIPosition pos();
				text.hover();
			}
		}


		void XdevLUIImGUI::add(XdevLUICheckBox& checkbox) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			if(ImGui::Checkbox(checkbox.getLabel().toString().c_str(), checkbox.getData())) {
				checkbox.checked();
			}

			if(ImGui::IsItemHovered()) {
				checkbox.hover();
			}
		}

		void XdevLUIImGUI::add(XdevLUIRadioButton& radiobutton) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			if(ImGui::RadioButton(radiobutton.getLabel().toString().c_str(), radiobutton.getData(), radiobutton.getID())) {
				radiobutton.changed();
			}

			if(ImGui::IsItemHovered()) {
				radiobutton.hover();
			}
		}

		void XdevLUIImGUI::add(XdevLUISlider& slider) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

			if(slider.getSize() == 1) {
				if(ImGui::SliderFloat(slider.getLabel().toString().c_str(), slider.getData(), slider.getMin(), slider.getMax(), "%.3f", 1.0)) {
					slider.sliderMoved();
				}

			} else if(slider.getSize() == 2) {
				if(ImGui::SliderFloat2(slider.getLabel().toString().c_str(), slider.getData(), slider.getMin(), slider.getMax(), "%.3f", 1.0)) {
					slider.sliderMoved();
				}

			} else if(slider.getSize() == 3) {
				if(ImGui::SliderFloat3(slider.getLabel().toString().c_str(), slider.getData(), slider.getMin(), slider.getMax(), "%.3f", 1.0)) {
					slider.sliderMoved();
				}

			} else if(slider.getSize() == 4) {
				if(ImGui::SliderFloat4(slider.getLabel().toString().c_str(), slider.getData(), slider.getMin(), slider.getMax(), "%.3f", 1.0)) {
					slider.sliderMoved();
				}
			}

			if(ImGui::IsItemHovered()) {
				slider.hover();
			}
		}

		void XdevLUIImGUI::add(XdevLUISliderAngle& sliderAngle) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

//			std::cout << "MinMax: " << sliderAngle.getMin() << "," << sliderAngle.getMax() << std::endl;

			if(ImGui::SliderAngle(sliderAngle.getLabel().toString().c_str(), sliderAngle.getData(), sliderAngle.getMin(), sliderAngle.getMax())) {
				sliderAngle.sliderMoved();
			}

			if(ImGui::IsItemHovered()) {
				sliderAngle.hover();
			}
		}


		void XdevLUIImGUI::add(XdevLUIButton& button) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

			auto size = button.getSize();
			if(ImGui::Button(button.getLabel().toString().c_str(), ImVec2(size.width, size.height))) {
				button.clicked();
			}
			if(ImGui::IsItemHovered()) {
				button.hover();
			}
		}

		void XdevLUIImGUI::add(XdevLUIComboBox& comboBox) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

			std::vector<const xdl_char*> tmp;
			for(auto& item : comboBox.getItems()) {
				tmp.push_back((const xdl_char*)item.data());
			}

			if(ImGui::Combo(comboBox.getLabel().toString().c_str(), comboBox.getSelectedItemData(), tmp.data(), tmp.size(), -1)) {
				comboBox.selected();
			}

			if(ImGui::IsItemHovered()) {
				comboBox.hover();
			}
		}

		void XdevLUIImGUI::add(XdevLUITextInputField& textInputField) {
			XDEVL_ASSERT(m_initialized == xdl_true, "XdevLUIImGUI is not initialized.\n");
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");
			// TODO The cast to (xdl_char*) might cause issues later.
			if(ImGui::InputText(textInputField.getLabel().toString().c_str(), (xdl_char*)textInputField.getData(), textInputField.getBufferSize())) {
				textInputField.changed();
			}
			if(ImGui::IsItemHovered()) {
				textInputField.hover();
			}
		}

		void XdevLUIImGUI::add(XdevLUIVerticalBox& verticalBox) {
			XDEVL_ASSERT(m_lock == xdl_true, "You must lock before your use this function.\n");

		}

	}
}
