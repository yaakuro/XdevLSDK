/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/
#ifndef XDEVL_UI_H
#define XDEVL_UI_H

#include <XdevLModule.h>
#include <XdevLFileSystem/XdevLArchive.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLFont/XdevLFont.h>
#include <XdevLFont/XdevLFontSystem.h>
#include <XdevLFont/XdevLTextLayout.h>
#include <XdevLUI/XdevLUIWidgets.h>

namespace xdl {

	namespace ui {

		/**
			@class XdevLUI
			@brief Base class for UI support.
		*/
		class XdevLUI : public XdevLModule {
			public:
				virtual ~XdevLUI() {}

				/// Creates the UI.
				virtual xdl_int create(IPXdevLWindow window, IPXdevLRAI rai, IPXdevLArchive archive) = 0;

				virtual xdl_int setupFontFromFile(std::shared_ptr<XdevLFile>& fontFile) = 0;
				virtual xdl_int setupFont(IPXdevLFont	font) = 0;

				/// Lock to add new surfaces to window.
				virtual xdl_int lock(const XdevLString& title) = 0;

				/// Unlock when you finished adding.
				virtual xdl_int unlock() = 0;

				/// Render all surfaces.
				virtual xdl_int render() = 0;

				/// Returns the frame rate.
				virtual xdl_float getFPS() = 0;
				virtual xdl_float getJitter() = 0;

				/// Add surfaces to the locked window.
				virtual void add(XdevLUIText& text) = 0;
				virtual void add(XdevLUIButton& button) = 0;
				virtual void add(XdevLUICheckBox& checkbox) = 0;
				virtual void add(XdevLUIRadioButton& checkbox) = 0;
				virtual void add(XdevLUISlider& slider) = 0;
				virtual void add(XdevLUISliderAngle& sliderAngle) = 0;
				virtual void add(XdevLUIComboBox& comboBox) = 0;
				virtual void add(XdevLUITextInputField& textInputField) = 0;
				virtual void add(XdevLUIVerticalBox& verticalBox) = 0;

				virtual void parse(XdevLUISurface const* root) {};
				virtual void setFontSize(const XdevLUISize& size) = 0;
		};

		using IXdevLUI = XdevLUI;
		using IPXdevLUI = XdevLUI*;

	}
}

#endif
