/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/
#ifndef XDEVL_UI_FXML_PARSER_H
#define XDEVL_UI_FXML_PARSER_H

#include <XdevLUI/XdevLUIWidgets.h>

namespace xdl {

	namespace ui {

		class XdevLUIFXMLParser {
			public:
				XdevLUIFXMLParser()
					: m_rootSurface(nullptr) {

				}

				XdevLUISurface const* getRoot() const {
					return m_rootSurface;
				}

				xdl_int parse(const XdevLFileName& filename) {
					xdl::XdevLString fxmlBuffer;
					xdl::getXmlFile(filename, fxmlBuffer);
					return parse(fxmlBuffer.data());
				}

				xdl_int parse(xdl_uint8* fxmlBuffer) {
					TiXmlDocument xmlDocument;
					if(!xmlDocument.Parse((char*)fxmlBuffer)) {
						XDEVL_MODULEX_ERROR(XdevLUIFXMLParser, "Could not parse xml file.\n");
						return RET_FAILED;
					}

					TiXmlHandle fxmlHandle(&xmlDocument);
					TiXmlElement* root = fxmlHandle.FirstChildElement().ToElement();
					if(nullptr == root) {
						XDEVL_MODULEX_ERROR(XdevLUIFXMLParser, "No elements to parse.\n");
						return RET_SUCCESS;
					}

					readWidget(root, m_rootSurface);

					return RET_SUCCESS;
				}

				void readWidget(TiXmlElement* root, XdevLUISurface*& rootSurface) {

					XdevLUIProperties properties;
					readProperties(root, properties);


					XdevLUISurface* surface = nullptr;
					if(XdevLString(root->Value()) == XdevLString("VBox")) {
						auto tmp = new XdevLUIVerticalBox(properties.prefSize);
						*tmp = properties;
						surface = tmp;
					} else if(XdevLString(root->Value()) == XdevLString("HBox")) {
						auto tmp = new XdevLUIHorizontalBox(properties.prefSize);
						*tmp = properties;
						surface = tmp;
					} else if(XdevLString(root->Value()) == XdevLString("Button")) {
						auto tmp = new XdevLUIButton(properties.id, properties.prefSize);
						*tmp = properties;
						surface = tmp;
					} else if(XdevLString(root->Value()) == XdevLString("CheckBox")) {
						auto tmp = new XdevLUICheckBox(properties.text, properties.prefSize);
						*tmp = properties;
						surface = tmp;
					} else if(XdevLString(root->Value()) == XdevLString("ComboBox")) {
						auto tmp = new XdevLUIComboBox(properties.text, properties.prefSize);
						*tmp = properties;
						surface = tmp;
					} else if(XdevLString(root->Value()) == XdevLString("Label")) {
						auto tmp = new XdevLUIText(properties.text, properties.prefSize);
						*tmp = properties;
						surface = tmp;
					} else if(XdevLString(root->Value()) == XdevLString("TextField")) {
						auto tmp = new XdevLUITextInputField(properties.text, properties.prefSize);
						*tmp = properties;
						surface = tmp;
					}

					if(nullptr != rootSurface) {
						surface->setParent(rootSurface);
						rootSurface->addChild(surface);
					} else {
						rootSurface = surface;
					}

					TiXmlElement* childrenElement = root->FirstChildElement("children");
					if(nullptr == childrenElement) {
						return;
					}

					TiXmlElement* childElement = childrenElement->FirstChildElement();
					while(childElement != nullptr) {
						readWidget(childElement, surface);
						if(!childElement->NextSibling()) {
							break;
						}
						childElement = childElement->NextSibling()->ToElement();
					}

				}

				void readProperties(TiXmlElement* root, XdevLUIProperties& properties) {
					if(root->Attribute("fx:id")) {
						properties.id = XdevLID(root->Attribute("fx:id"));
					}
					if(root->Attribute("prefHeight")) {
						properties.prefSize.height = xstd::from_string<xdl_int>(root->Attribute("prefHeight"));
					}
					if(root->Attribute("prefWidth")) {
						properties.prefSize.width = xstd::from_string<xdl_int>(root->Attribute("prefWidth"));
					}
					if(root->Attribute("maxHeight")) {
						properties.maxSize.height = xstd::from_string<xdl_int>(root->Attribute("maxHeight"));
					}
					if(root->Attribute("maxWidth")) {
						properties.maxSize.width = xstd::from_string<xdl_int>(root->Attribute("maxWidth"));
					}
					if(root->Attribute("minHeight")) {
						properties.minSize.height = xstd::from_string<xdl_int>(root->Attribute("minHeight"));
					}
					if(root->Attribute("minWidth")) {
						properties.minSize.width = xstd::from_string<xdl_int>(root->Attribute("minWidth"));
					}
					if(root->Attribute("alignment")) {
						properties.alignment = convert(XdevLString(root->Attribute("alignment")));
					}
					if(root->Attribute("disable")) {
						properties.disable = xstd::from_string<xdl_bool>(root->Attribute("disable"));
					}
					if(root->Attribute("opacity")) {
						properties.opacity = xstd::from_string<xdl_float>(root->Attribute("opacity"));
					}
					if(root->Attribute("text")) {
						properties.text = XdevLString(root->Attribute("text"));
					}
					if(root->Attribute("fillWidth")) {
						properties.fillWidth = XdevLString(root->Attribute("fillWidth"));
					}

					TiXmlElement* paddingElement = root->FirstChildElement("padding");
					if(nullptr != paddingElement) {
						TiXmlElement* insets = paddingElement->FirstChildElement("Insets")->ToElement();
						if(nullptr != insets) {
							if(insets->Attribute("left")) {
								properties.padding.left = xstd::from_string<xdl_float>(insets->Attribute("left"));
							}
							if(insets->Attribute("right")) {
								properties.padding.right = xstd::from_string<xdl_float>(insets->Attribute("right"));
							}
							if(insets->Attribute("top")) {
								properties.padding.top = xstd::from_string<xdl_float>(insets->Attribute("top"));
							}
							if(insets->Attribute("bottom")) {
								properties.padding.bottom = xstd::from_string<xdl_float>(insets->Attribute("bottom"));
							}
						}
					}
				}


				XdevLUIAlignment convert(XdevLString str) {
					if(str == XdevLString(TEXT("TOP_LEFT"))) {
						return XdevLUIAlignment::TOP_LEFT;
					} else if(str == XdevLString(TEXT("TOP_CENTER"))) {
						return XdevLUIAlignment::TOP_CENTER;
					} else if(str == XdevLString(TEXT("TOP_RIGHT"))) {
						return XdevLUIAlignment::TOP_RIGHT;
					} else if(str == XdevLString(TEXT("CENTER_LEFT"))) {
						return XdevLUIAlignment::CENTER_LEFT;
					} else if(str == XdevLString(TEXT("CENTER"))) {
						return XdevLUIAlignment::CENTER_CENTER;
					} else if(str == XdevLString(TEXT("CENTER_RIGHT"))) {
						return XdevLUIAlignment::CENTER_RIGHT;
					} else if(str == XdevLString(TEXT("BOTTOM_LEFT"))) {
						return XdevLUIAlignment::BOTTOM_LEFT;
					} else if(str == XdevLString(TEXT("BOTTOM_CENTER"))) {
						return XdevLUIAlignment::BOTTOM_CENTER;
					} else if(str == XdevLString(TEXT("BOTTOM_RIGHT"))) {
						return XdevLUIAlignment::BOTTOM_RIGHT;
					}
				}

				void setPosition(XdevLUISurface* parent) {
					if(nullptr == parent) {
						return;
					}

					switch(parent->getWigetType()) {
						case XdevLUIWidgetType::VBOX: {
						} break;
						case XdevLUIWidgetType::HBOX: {
						} break;
						default: break;
					}
				}

				xdl_float getChildrenVerticalSize(XdevLUISurface* parent) {
					if(nullptr == parent) {
						return 0.0f;
					}

					xdl_float sizeY = 0.0f;
					for(auto& surface : parent->getSurfaces()) {
						sizeY += getChildrenVerticalSize(surface);
					}

					return parent->getSize().height;
				}

			private:

				XdevLUISurface* m_rootSurface;
		};

	}
}

#endif
