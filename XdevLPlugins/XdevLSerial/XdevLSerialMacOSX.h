/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SERIAL_MACOSX_H
#define XDEVL_SERIAL_MACOSX_H

#include <XdevLSerialBase.h>

#include <IOKit/IOKitLib.h>
#include <IOKit/serial/IOSerialKeys.h>
#include <IOKit/IOBSD.h>

namespace xdl {

	static const std::vector<XdevLModuleName>	moduleNames	{
		XdevLModuleName("XdevLSerial")
	};

	/**
		@class XdevLSerialMacOSX
		@brief Implementation of the XdevLSerial interface for MacOSX OS.
	*/
	class XdevLSerialMacOSX : public XdevLSerialBase {
		public:
			XdevLSerialMacOSX(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLSerialMacOSX() {}

			xdl_int shutdown() override final;

			xdl_int open() override final;
			xdl_int open(const XdevLFileName& name) override final;
			xdl_int close() override final;
			xdl_int write(xdl_uint8* src, xdl_int size) override final;
			xdl_int read(xdl_uint8* dst, xdl_int size) override final;
			xdl_int setStates(xdl_int baudrate,
			                  XdevLSerialByteSize bytesize,
			                  XdevLSerialParity parity,
			                  XdevLSerialStopBits stopbits,
			                  XdevLSerialFlowControl flowcontrol,
			                  xdl_int timeout) override final;
			xdl_int setStates(const XdevLSerialPortParameters& parameters) override final;
			xdl_int waiting() override final;
			xdl_int flush() override final;
			xdl_uint64 length();

		protected:

			xdl_int open(const xdl_char* name, const XdevLDeviceModes& mode);
			xdl::xdl_int _open();
			xdl_int wrapBaudrate(xdl_int baudrate);
			void dump();

		private:

			xdl_int m_fd;
			xdl_bool m_dirtyFlag;
			timespec* m_timeoutSpec;
			xdl_bool m_connected;
	};

}

#endif
