/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SERIAL_BASE_H
#define XDEVL_SERIAL_BASE_H

#include <XdevLSerial/XdevLSerial.h>
#include <XdevLPluginImpl.h>
#include <XdevLUtils.h>

#include <map>
#include <string>

namespace xdl {

	/**
		@class XdevLSerialBase
		@brief Base class for all XdevLSerial classes.
	*/
	class XdevLSerialBase : public XdevLModuleImpl<XdevLSerial> {
		public:

			using Super = XdevLSerialBase;

			XdevLSerialBase(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
				: XdevLModuleImpl<XdevLSerial>(parameter, descriptor)
				, m_deviceMode(XdevLStream::AccesType::XDEVL_STREAM_READ_ONLY)
				, m_baudrate(9600)
				, m_byteSize(SERIAL_BSIZE_8)
				, m_parity(SERIAL_NO_PARITY)
				, m_stopBits(SERIAL_SB_1)
				, m_flowControl(SERIAL_FLOW_CONTROL_NONE)
				, m_timeout(0)
				, m_dirty(xdl_false)
				, m_xon(0x11)
				, m_xoff(0x13) {

				m_deviceModeMap["XDEVL_DEVICE_READ_ONLY"] 	= XdevLStream::AccesType::XDEVL_STREAM_READ_ONLY;
				m_deviceModeMap["XDEVL_DEVICE_WRITE_ONLY"] 	= XdevLStream::AccesType::XDEVL_STREAM_WRITE_ONLY;
				m_deviceModeMap["XDEVL_DEVICE_READ_WRITE"] 	= XdevLStream::AccesType::XDEVL_STREAM_READ_WRITE;

				m_byteSizeMap["SERIAL_BSIZE_5"] = SERIAL_BSIZE_5;
				m_byteSizeMap["SERIAL_BSIZE_6"] = SERIAL_BSIZE_6;
				m_byteSizeMap["SERIAL_BSIZE_7"] = SERIAL_BSIZE_7;
				m_byteSizeMap["SERIAL_BSIZE_8"] = SERIAL_BSIZE_8;

				m_parityMap["SERIAL_NO_PARITY"] 	= SERIAL_NO_PARITY;
				m_parityMap["SERIAL_EVEN_PARITY"] 	= SERIAL_EVEN_PARITY;
				m_parityMap["SERIAL_ODD_PARITY"] 	= SERIAL_ODD_PARITY;
				m_parityMap["SERIAL_SPACE_PARITY"] 	= SERIAL_SPACE_PARITY;

				m_stopBitsMap["SERIAL_SB_1"] = SERIAL_SB_1;
				m_stopBitsMap["SERIAL_SB_2"] = SERIAL_SB_2;

				m_flowControlMap["SERIAL_FLOW_CONTROL_NONE"] 		= SERIAL_FLOW_CONTROL_NONE;
				m_flowControlMap["SERIAL_FLOW_CONTROL_HARDWARE"] 	= SERIAL_FLOW_CONTROL_HARDWARE;
				m_flowControlMap["SERIAL_FLOW_CONTROL_SOFTWARE"] 	= SERIAL_FLOW_CONTROL_SOFTWARE;

			}

			virtual xdl_int setStates(xdl_int baudrate,
			                          XdevLSerialByteSize bytesize,
			                          XdevLSerialParity parity,
			                          XdevLSerialStopBits stopbits,
			                          XdevLSerialFlowControl flowcontrol,
			                          xdl_int timeout = -1) override {
				this->m_baudrate 		= baudrate;
				this->m_byteSize 		= bytesize;
				this->m_parity			= parity;
				this->m_stopBits		= stopbits;
				this->m_flowControl	= flowcontrol;
				this->m_timeout			= timeout;
				return RET_SUCCESS;
			}

			virtual xdl_int setStates(const XdevLSerialPortParameters& parameters) override {
				this->m_baudrate 		= parameters.baudrate;
				this->m_byteSize 		= parameters.bytesize;
				this->m_parity			= parameters.parity;
				this->m_stopBits		= parameters.stopbits;
				this->m_flowControl	= parameters.flowcontrol;
				this->m_timeout			= parameters.timeout;
				return RET_SUCCESS;
			}

			xdl_int readInfoFromXMLFile() {
				auto coreParameter = getMediator()->getCoreParameter();
				if(coreParameter.xmlBuffer.size() == 0) {
					return RET_FAILED;
				}

				TiXmlDocument xmlDocument;
				if(!xmlDocument.Parse((xdl_char*)coreParameter.xmlBuffer.data())) {
					XDEVL_MODULE_ERROR("Could not parse xml buffer.\n ");
					return RET_FAILED;
				}

				TiXmlHandle docHandle(&xmlDocument);
				TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toString().c_str()).FirstChildElement("XdevLSerial").ToElement();
				if(!root) {
					XDEVL_MODULE_WARNING("<XdevLSerial> section not found. Using default values for the device.\n");
					return RET_SUCCESS;
				}

				while(root != NULL) {
					if(root->Attribute("id")) {
						XdevLID id(root->Attribute("id"));
						if(getID() == id) {
							if(root->Attribute("device")) {
								m_deviceName = XdevLFileName(root->Attribute("device"));
							}
							if(root->Attribute("mode")) {
								m_deviceMode = m_deviceModeMap[root->Attribute("mode")];
							}
							if(root->Attribute("baudrate")) {
								std::istringstream ss(root->Attribute("baudrate"));
								ss >> m_baudrate;
							}
							if(root->Attribute("bytesize")) {
								m_byteSize = m_byteSizeMap[root->Attribute("bytesize")];
							}
							if(root->Attribute("stopbits")) {
								m_stopBits = m_stopBitsMap[root->Attribute("stopbits")];
							}
							if(root->Attribute("parity")) {
								m_parity = m_parityMap[root->Attribute("parity")];
							}
							if(root->Attribute("flowcontrol")) {
								m_flowControl = m_flowControlMap[root->Attribute("flowcontrol")];
							}
							if(root->Attribute("timeout")) {
								std::istringstream ss(root->Attribute("timeout"));
								ss >> m_timeout;
							}
						}
					} else
						XDEVL_MODULEX_ERROR(XdevLSerial, "No 'id' attribute specified. Using default values for the device\n");

					root = root->NextSiblingElement();
				}
				return RET_SUCCESS;
			}

		protected:

			XdevLFileName m_deviceName;
			XdevLStream::AccesType m_deviceMode;
			xdl::xdl_int m_baudrate;
			XdevLSerialByteSize m_byteSize;
			XdevLSerialParity m_parity;
			XdevLSerialStopBits m_stopBits;
			XdevLSerialFlowControl m_flowControl;
			xdl_int m_timeout;
			xdl_bool m_dirty;
			xdl_int m_xon;
			xdl_int m_xoff;

			std::map<std::string, XdevLStream::AccesType> m_deviceModeMap;
			std::map<std::string, XdevLSerialByteSize> m_byteSizeMap;
			std::map<std::string, XdevLSerialParity> m_parityMap;
			std::map<std::string, XdevLSerialStopBits> m_stopBitsMap;
			std::map<std::string, XdevLSerialFlowControl> m_flowControlMap;
	};

}

#endif
