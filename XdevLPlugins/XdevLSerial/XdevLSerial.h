/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SERIAL_H
#define XDEVL_SERIAL_H

#include <XdevLStream.h>

namespace xdl {

	/**
		@enum XdevLSerialStopBits
		@brief The stop bits of the serial port.
	*/
	enum XdevLSerialStopBits {
		SERIAL_SB_1,
		SERIAL_SB_2
	};

	XDEVL_INLINE XdevLString XdevLStopBitsToString(xdl_int stopBits) {
		if(stopBits == SERIAL_SB_1) {
			return XdevLString("SERIAL_SB_1");
		} else if(stopBits == SERIAL_SB_2) {
			return XdevLString("SERIAL_SB_2");
		}
		return XdevLString("SERIAL_SB_UNKOWN");
	}

	/**
		@enum XdevLSerialByteSize
		@brief The size of the byte transfered on the serial port.
	*/
	enum XdevLSerialByteSize {
		SERIAL_BSIZE_5,
		SERIAL_BSIZE_6,
		SERIAL_BSIZE_7,
		SERIAL_BSIZE_8
	};

	XDEVL_INLINE XdevLString xdevLByteSizeToString(xdl_int byteSize) {
		if(byteSize == SERIAL_BSIZE_5) {
			return XdevLString("SERIAL_BSIZE_5");
		} else if(byteSize == SERIAL_BSIZE_6) {
			return XdevLString("SERIAL_BSIZE_6");
		} else if(byteSize == SERIAL_BSIZE_7) {
			return XdevLString("SERIAL_BSIZE_7");
		} else if(byteSize == SERIAL_BSIZE_8) {
			return XdevLString("SERIAL_BSIZE_8");
		}
		return XdevLString("SERIAL_BSIZE_UNKOWN");
	}

	/**
		@enum XdevLSerialBaudRate
		@brief The baud rate of the serial port.
	*/
	enum XdevLSerialBaudRate {
		SERIAL_BAUD_0 = 0,
		SERIAL_BAUD_50 = 50,
		SERIAL_BAUD_75 = 75,
		SERIAL_BAUD_110 = 110,
		SERIAL_BAUD_134 = 134,
		SERIAL_BAUD_150 = 150,
		SERIAL_BAUD_200 = 200,
		SERIAL_BAUD_300 = 300,
		SERIAL_BAUD_600 = 600,
		SERIAL_BAUD_1200 = 1200,
		SERIAL_BAUD_1800 = 1800,
		SERIAL_BAUD_2400 = 2400,
		SERIAL_BAUD_4800 = 4800,
		SERIAL_BAUD_9600 = 9600,
		SERIAL_BAUD_19200 = 19200,
		SERIAL_BAUD_38400 = 38400,
		SERIAL_BAUD_57600 = 57600,
		SERIAL_BAUD_115200 = 115200,
		SERIAL_BAUD_230400 = 230400,
		SERIAL_BAUD_460800 = 460800,
		SERIAL_BAUD_500000 = 500000,
		SERIAL_BAUD_576000 = 576000,
		SERIAL_BAUD_921600 = 921600,
		SERIAL_BAUD_1000000 = 1000000,
		SERIAL_BAUD_1152000 = 1152000,
		SERIAL_BAUD_1382400 = 1382400,
		SERIAL_BAUD_1500000 = 1500000,
		SERIAL_BAUD_2000000 = 2000000,
		SERIAL_BAUD_2500000 = 2500000,
		SERIAL_BAUD_3000000 = 3000000,
		SERIAL_BAUD_3500000 = 3500000,
		SERIAL_BAUD_4000000 = 4000000
	};

	/**
		@enum XdevLSerialParity
		@brief The parity of the serial port.
	*/
	enum XdevLSerialParity {
		SERIAL_EVEN_PARITY,
		SERIAL_MARK_PARITY,
		SERIAL_NO_PARITY,
		SERIAL_ODD_PARITY,
		SERIAL_SPACE_PARITY
	};

	XDEVL_INLINE XdevLString xdevLParityToString(xdl_int parity) {
		if(parity == SERIAL_EVEN_PARITY) {
			return XdevLString("SERIAL_EVEN_PARITY");
		} else if(parity == SERIAL_MARK_PARITY) {
			return XdevLString("SERIAL_MARK_PARITY");
		} else if(parity == SERIAL_NO_PARITY) {
			return XdevLString("SERIAL_NO_PARITY");
		} else if(parity == SERIAL_ODD_PARITY) {
			return XdevLString("SERIAL_ODD_PARITY");
		} else if(parity == SERIAL_SPACE_PARITY) {
			return XdevLString("SERIAL_SPACE_PARITY");
		}
		return XdevLString("SERIAL_PARITY_UNKOWN");
	}

	/**
		@enum XdevLSerialFlowControl
		@brief The flow control mode of the serial port.
	*/
	enum XdevLSerialFlowControl {
		SERIAL_FLOW_CONTROL_NONE,
		SERIAL_FLOW_CONTROL_HARDWARE,
		SERIAL_FLOW_CONTROL_SOFTWARE
	};

	XDEVL_INLINE XdevLString xdevLFlowControlToString(xdl_int flowControl) {
		if(flowControl == SERIAL_FLOW_CONTROL_NONE) {
			return XdevLString("SERIAL_FLOW_CONTROL_NONE");
		} else if(flowControl == SERIAL_FLOW_CONTROL_HARDWARE) {
			return XdevLString("SERIAL_FLOW_CONTROL_HARDWARE");
		} else if(flowControl == SERIAL_FLOW_CONTROL_SOFTWARE) {
			return XdevLString("SERIAL_FLOW_CONTROL_SOFTWARE");
		}
		return XdevLString("SERIAL_FLOW_CONTROL_UNKOWN");
	}

	/**
		@class XdevLSerialPortParameters
		@brief Used to set the parameters of the serial port.
	*/
	struct XdevLSerialPortParameters {
		XdevLSerialBaudRate baudrate = SERIAL_BAUD_9600;
		XdevLSerialByteSize bytesize = SERIAL_BSIZE_8;
		XdevLSerialParity parity = SERIAL_NO_PARITY;
		XdevLSerialStopBits stopbits = SERIAL_SB_1;
		XdevLSerialFlowControl flowcontrol = SERIAL_FLOW_CONTROL_NONE;
		xdl_int timeout = 0;
	};

	/**
		@class XdevLSerial
		@brief Interface for Serial Port devices.
	*/
	class XdevLSerial : public XdevLStream, public XdevLModule {
		public:
			virtual ~XdevLSerial() {};

			using XdevLStream::open;
			using XdevLStream::close;
			using XdevLStream::read;
			using XdevLStream::write;

			/// Set the serial port communication properties.
			/**
				@param baudrate The baudrate of the serial port.
				@param bytesize The byte size of the data on the serial port.
				@param parity The parity of the serial port.
				@param stopbits The stop bits of the serial port.
				@param timeout Sets the timeout of the reading and writting bytes on the serial port.
				@return
					- @b RET_FAILED if it failed.
					- @b RET_SUCCESS if it was successful.
			*/
			XDEVL_DEPRECATED("This method will be not used in XdevLSDK 0.8 anymore. Use setState(const XdevLSerialPortParameters& parameters instead.")
			virtual xdl_int setStates(xdl_int baudrate,
			                          XdevLSerialByteSize bytesize,
			                          XdevLSerialParity parity,
			                          XdevLSerialStopBits stopbits,
			                          XdevLSerialFlowControl flowcontrol,
			                          xdl_int timeout = -1) = 0;
			virtual xdl_int setStates(const XdevLSerialPortParameters& parameters) = 0;

			/// Returns the number of bytes waiting in the buffer.
			/**
				@return The number of bytes waiting in the buffer.
			*/
			virtual xdl_int waiting() = 0;
	};

	using IXdevLSerial = XdevLSerial;
	using IPXdevLSerial = XdevLSerial*;

	XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLSerial)
}

#endif
