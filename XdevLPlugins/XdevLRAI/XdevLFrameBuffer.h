/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_FRAMEBUFFER_H
#define XDEVL_FRAMEBUFFER_H

#include <XdevLRAI/XdevLRAIFWD.h>

namespace xdl {

	class XdevLTexture;

	enum class XdevLFrameBufferRenderTarget {
		NONE = 0,
		COLOR = 1,
		DEPTH = 2,
		STENCIL = 4
	};

	enum class XdevLFrameBufferColorTargets {
		TARGET_NONE = 0,
		TARGET0 = 0x8CE0,
		TARGET1 = 0x8CE1,
		TARGET2 = 0x8CE2,
		TARGET3 = 0x8CE3,
		TARGET4 = 0x8CE4,
		TARGET5 = 0x8CE5,
		TARGET6 = 0x8CE6,
		TARGET7 = 0x8CE7,
		TARGET8 = 0x8CE8,
		TARGET9 = 0x8CE9,
		TARGET10 = 0x8CEA,
		TARGET11 = 0x8CEB,
		TARGET12 = 0x8CEC,
		TARGET13 = 0x8CED,
		TARGET14 = 0x8CEE,
		TARGET15 = 0x8CEF
	};

	enum class XdevLFrameBufferColorFormat {

		R8								= 0x8229,
		RG8								= 0x822B,
		RGB8							= 0x8051,
		RGBA8							= 0x8058,

		R8I								= 0x8231,
		RG8I							= 0x8237,
		RGB8I							= 0x8D8F,
		RGBA8I							= 0x8D8E,

		R8UI							= 0x8232,
		RG8UI							= 0x8238,
		RGB8UI							= 0x8D7D,
		RGBA8UI							= 0x8D7C,

		R16								= 0x822A,
		RG16							= 0x822C,
		RGB16							= 0x8054,
		RGBA16							= 0x805B,

		R16I							= 0x8233,
		RG16I							= 0x8239,
		RGB16I							= 0x8D89,
		RGBA16I							= 0x8D88,

		R16UI							= 0x8234,
		RG16UI							= 0x823A,
		RGB16UI							= 0x8D77,
		RGBA16UI						= 0x8D76,

		R16F							= 0x822D,
		RG16F							= 0x822F,
		RGB16F							= 0x881B,
		RGBA16F							= 0x881A,

		R32I							= 0x8235,
		RG32I							= 0x823B,
		RGB32I							= 0x8D83,
		RGBA32I							= 0x8D82,

		R32UI							= 0x8236,
		RG32UI							= 0x823C,
		RGB32UI							= 0x8D71,
		RGBA32UI						= 0x8D70,

		R32F							= 0x822E,
		RG32F							= 0x8230,
		RGB32F							= 0x8815,
		RGBA32F							= 0x8814,

		R11F_G11F_B10F					= 0x8C3A,
		RGB9_E5							= 0x8C3D,
		SRGB							= 0x8C40,
		SRGB8							= 0x8C41,
		SRGB_ALPHA						= 0x8C42,
		SRGB8_ALPHA8					= 0x8C43,

		R8_SNORM						= 0x8F94,
		RG8_SNORM						= 0x8F95,
		RGB8_SNORM						= 0x8F96,
		RGBA8_SNORM						= 0x8F97,

		R16_SNORM						= 0x8F98,
		RG16_SNORM						= 0x8F99,
		RGB16_SNORM						= 0x8F9A,
		RGBA16_SNORM					= 0x8F9B,

		COMPRESSED_ALPHA 				= 0x84E9,
		COMPRESSED_LUMINANCE 			= 0x84EA,
		COMPRESSED_LUMINANCE_ALPHA 		= 0x84EB,
		COMPRESSED_INTENSITY 			= 0x84EC,
		COMPRESSED_RGB 					= 0x84ED,
		COMPRESSED_RGBA 				= 0x84EE,
		COMPRESSED_SRGB					= 0x8C48,
		COMPRESSED_SRGB_ALPHA			= 0x8C49,
		COMPRESSED_SLUMINANCE			= 0x8C4A,
		COMPRESSED_SLUMINANCE_ALPHA		= 0x8C4B,
		COMPRESSED_SRGB_S3TC_DXT1		= 0x8C4C,
		COMPRESSED_SRGB_ALPHA_S3TC_DXT1	= 0x8C4D,
		COMPRESSED_SRGB_ALPHA_S3TC_DXT3	= 0x8C4E,
		COMPRESSED_SRGB_ALPHA_S3TC_DXT5	= 0x8C4F
	};

	enum class XdevLFrameBufferDepthFormat {

		COMPONENT		= 0x1902,
		COMPONENT16		= 0x81A5,
		COMPONENT24		= 0x81A6,
		COMPONENT32		= 0x81A7,
		COMPONENT32F	= 0x8CAC

	};

	enum class XdevLFrameBufferDepthStencilFormat {

		DEPTH_STENCIL		= 0x84F9,
		DEPTH24_STENCIL8	= 0x88F0,
		DEPTH32F_STENCIL8	= 0x8CAD

	};

	/**
		@class XdevLFrameBuffer
		@brief Allows user-defined Framebuffers.
		@author Cengiz Terzibas
	 */
	class XdevLFrameBuffer {
		public:
			virtual ~XdevLFrameBuffer() {}

			/// Initialize the framebuffer.
			/**
				@param width The width of the framebuffer
				@param height The height of the framebuffer
				@param number_of_color_buffers The number of color buffers.
				@param depth_buffer Use of a depth buffer.
					- 'xdl_true' for yes use a depth buffer.
					- 'xdl_false' do not use a depth buffer.
				@param stencil_buffer Use of a stencil buffer.
					- 'xdl_true' for yes use a stencil buffer.
					- 'xdl_false' do not use a stencil buffer.
			*/
			virtual xdl_int init(xdl_uint width, xdl_uint height) = 0;

			/// Add a color target to the framebuffer.
			virtual xdl_int addColorTarget(xdl_uint target_index, XdevLFrameBufferColorFormat internal_format) = 0;
			virtual xdl_int addColorTarget(xdl_uint target_index, IPXdevLTexture texture) = 0;
			virtual xdl_int addColorTarget(xdl_uint target_index, IPXdevLTextureCube textureCube) = 0;

			/// Add a depth target to the framebuffer.
			virtual xdl_int addDepthTarget(XdevLFrameBufferDepthFormat internal_format) = 0;
			virtual xdl_int addDepthTarget(IPXdevLTexture texture) = 0;

			/// Add a depth stencil target to the framebuffer.
			virtual xdl_int addDepthStencilTarget(XdevLFrameBufferDepthStencilFormat internal_format) = 0;

			/// Activate the frame buffer.
			virtual xdl_int activate() = 0;

			/// Deactivate the frame buffer.
			virtual xdl_int deactivate() = 0;

			/// Activates and/or deactivates a list of Color targets.
			virtual xdl_int activateColorTargets(xdl_uint numberOfTargets, XdevLFrameBufferColorTargets* targetList) = 0;

			/// Activates a position of a cube map.
			virtual xdl_int activateColorTargetCubePosition(xdl_uint target_index, XdevLCubemapPosition cubemapPosition) = 0;

			/// Activates or deactivates the Depth target.
			virtual xdl_int setActiveDepthTest(xdl_bool state) = 0;

			/// Activates or deactivates the Stencil target.
			virtual xdl_int activateStencilTarget(xdl_bool state) = 0;

			/// Clears the Color Targets.
			virtual xdl_int clearColorTargets(xdl_float r, xdl_float g, xdl_float b, xdl_float a) = 0;

			/// Clears the Depth Target.
			virtual xdl_int clearDepthTarget(xdl_float clear_value) = 0;

			/// Clears the Stencil Target.
			virtual xdl_int clearStencilTarget(xdl_int clear_value) = 0;

			/// Returns the width of the framebuffer.
			virtual xdl_uint getWidth() = 0;

			/// Returns the height of the framebuffer.
			virtual xdl_uint getHeight() = 0;

			/// Returns the color target of the framebuffer.
			/**
				The default returned texture is 0.
			*/
			virtual IPXdevLTexture getTexture(xdl_uint idx = 0) = 0;

			/// Returns the depth texture.
			virtual IPXdevLTexture getDepthTexture() = 0;

			/// Returns the number of color targets used.
			virtual xdl_uint getNumColorTextures() = 0;

			/// Returns the id of the framebuffer.
			virtual xdl_uint id() = 0;

			virtual void blit(XdevLFrameBuffer* framebuffer, xdl_uint32 numberOfRenderTargets, XdevLFrameBufferRenderTarget renderTargets[]) = 0;
	};
}

#endif
