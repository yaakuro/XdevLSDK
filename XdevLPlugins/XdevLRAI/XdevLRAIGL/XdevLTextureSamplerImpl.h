/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_TEXTURE_SAMPLER_IMPL_H
#define XDEVL_TEXTURE_SAMPLER_IMPL_H

#include <XdevLOpenGLUtils.h>
#include <XdevLRAI/XdevLTextureSampler.h>

namespace xdl {

	class XdevLTextureSamplerImpl : public XdevLTextureSampler {
		public:
			XdevLTextureSamplerImpl();
			virtual ~XdevLTextureSamplerImpl();

			xdl_int setTextureWrap(const XdevLTextureSamplerWrap& wrap) override final;
			xdl_int setTextureFilter(const XdevLTextureSamplerFilterStage& filterStage) override final;
			xdl_int setLOD(const XdevLTextureSamplerLOD& lod) override final;
			xdl_int setTextureCompare(const XdevLTextureSamplerCompare& compare) override final;
			xdl_int setTextureAnisotropy(const XdevLTextureSamplerAnisotropic& anisotropic) override final;
			xdl_int setBorderColor(XdevLTextureSamplerBorderColor borderColor) override final;

			void activate(XdevLTextureStage stage) override final;
			void deactivate() override final;
			void lock() override final;
			void unlock() override final;

		private:

			GLuint m_id;
			GLint m_stage;
			xdl_bool m_activate;
			xdl_bool m_locked;
	};

}


#endif
