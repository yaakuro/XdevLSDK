/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_TEXTURE_IMPL_H
#define XDEVL_TEXTURE_IMPL_H

#include <XdevLOpenGLUtils.h>
#include "XdevLTextureSamplerImpl.h"

namespace xdl {

	class XdevLTextureImpl: public XdevLTexture {
		public:
			XdevLTextureImpl()
				: m_id(0)
				, m_initialized(xdl_false)
				, m_textureStage(XdevLTextureStage::STAGE0)
				, m_lock(xdl_false)
				, m_width(0)
				, m_height(0)
				, m_isRenderBuffer(xdl_false)
				, m_textureSampler(nullptr)
			{}

			XdevLTextureImpl(xdl_uint id, xdl_uint width, xdl_uint height, xdl_bool isRenderBuffer = xdl_false)
				: m_id(id)
				, m_initialized(xdl_true)
				, m_textureStage(XdevLTextureStage::STAGE0)
				, m_lock(xdl_false)
				, m_width(width)
				, m_height(height)
				, m_isRenderBuffer(isRenderBuffer)
				, m_textureSampler(nullptr)
			{}

			virtual ~XdevLTextureImpl() {
				if(m_initialized) {
					if(nullptr != m_textureSampler) {
						delete m_textureSampler;
						m_textureSampler = nullptr;
					}
					if(m_isRenderBuffer) {
						glDeleteRenderbuffers(1, &m_id);
					} else {
						glDeleteTextures(1, &m_id);
					}
				}
			}

			xdl_int init(xdl_uint width, xdl_uint height, XdevLTextureFormat format, XdevLTexturePixelFormat texturePixelFormat, xdl_uint8* data) override final {
				m_textureSampler = new XdevLTextureSamplerImpl();

				glGetError();

				glGenTextures(1, &m_id);
				glBindTexture(GL_TEXTURE_2D, m_id);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

				glTexImage2D(GL_TEXTURE_2D,
				             0,
				             static_cast<GLint>(format),
				             width,
				             height,
				             0,
				             static_cast<GLenum>(texturePixelFormat),
				             xdevLTextureFormatToGLType(format),
				             (GLvoid*)data);
				auto result = glGetError();
				if(GL_NO_ERROR != result) {
					XDEVL_MODULEX_ERROR(XdevLTextureGL, "glTexImage2D failed: " << glGetErrorAsString(result) << std::endl);
					return RET_FAILED;
				}
				glBindTexture(GL_TEXTURE_2D, 0);
				m_internalFormat = static_cast<GLint>(format);
				m_initialized = xdl_true;
				m_width = width;
				m_height = height;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int upload(XdevLTexture* texture) override final {
				return RET_FAILED;
			}

			xdl_int upload(xdl_uint8* buffer, xdl_uint64 size) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");

				glCopyTexImage2D(GL_TEXTURE_2D, 0, m_internalFormat, 0, 0, m_width, m_height, 0);

				GL_CHECK_ERROR()

				return RET_FAILED;
			}

			xdl_int setTextureFilter(XdevLTextureFilterStage filterType, XdevLTextureFilter filter) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");
				if(XdevLTextureFilterStage::MAG_FILTER == filterType) {
					XDEVL_ASSERT(XdevLTextureFilter::LINEAR == filter || XdevLTextureFilter::NEAREST == filter, "XdevLTextureImpl::lock: For MAG_FILTER you can either use LINEAR or NEAREST .");
				}
				glTexParameteri(GL_TEXTURE_2D, static_cast<GLenum>(filterType), static_cast<GLint>(filter));

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int setTextureFilter(const XdevLTextureSamplerFilterStage& filterStage) override final {
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<GLint>(filterStage.magFilter));
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<GLint>(filterStage.minFilter));

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int setTextureWrap(XdevLTextureCoord texCoord, XdevLTextureWrap textureWrap) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");

				glTexParameteri(GL_TEXTURE_2D, static_cast<GLenum>(texCoord), static_cast<GLint>(textureWrap));

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int setTextureWrap(const XdevLTextureSamplerWrap& wrap) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, static_cast<GLint>(wrap.addressModeU));
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, static_cast<GLint>(wrap.addressModeV));
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, static_cast<GLint>(wrap.addressModeW));

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int setLOD(const XdevLTextureSamplerLOD& lod) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, lod.minLod);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, lod.maxLod);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, lod.lodBias);

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int setTextureCompare(const XdevLTextureSamplerCompare& compare) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");

				if(compare.enable) {
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, static_cast<GLint>(compare.compareOp));
				} else {
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
				}

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int setTextureMaxAnisotropy(xdl_float value) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");

				GLfloat largest;
				glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest);
				if(value < largest) {
					largest = value;
				}
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest);

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int setTextureAnisotropy(const XdevLTextureSamplerAnisotropic& anisotropic) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");

				// Value of 1.0f means to switch of anisotropic filtering.
				GLfloat maxValue = 1.0f;
				if(anisotropic.anisotropyEnable) {
					glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxValue);
					if(anisotropic.maxAnisotropy < maxValue) {
						maxValue = anisotropic.maxAnisotropy;
					}
				}
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxValue);

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int setBorderColor(XdevLTextureSamplerBorderColor borderColor) override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");
				switch(borderColor) {
					case XdevLTextureSamplerBorderColor::FLOAT_TRANSPARENT_BLACK: {
						const GLfloat color[] {0.0f, 0.0f, 0.0f, 0.0f};
						glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
					} break;
					case XdevLTextureSamplerBorderColor::INT_TRANSPARENT_BLACK: {
						const GLint color[] {0, 0, 0, 0};
						glTexParameterIiv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
					} break;
					case XdevLTextureSamplerBorderColor::FLOAT_OPAQUE_BLACK: {
						const GLfloat color[] {0.0f, 0.0f, 0.0f, 1.0f};
						glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
					} break;
					case XdevLTextureSamplerBorderColor::INT_OPAQUE_BLACK: {
						const GLint color[] {0, 0, 0, 255};
						glTexParameterIiv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
					} break;
					case XdevLTextureSamplerBorderColor::FLOAT_OPAQUE_WHITE: {
						const GLfloat color[] {1.0f, 1.0f, 1.0f, 1.0f};
						glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
					} break;
					case XdevLTextureSamplerBorderColor::INT_OPAQUE_WHITE: {
						const GLint color[] {255, 255, 255, 255};
						glTexParameterIiv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
					} break;
					default: break;
				}

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int generateMipMap() override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl:: Texture was not locked.");

				glGenerateMipmap(GL_TEXTURE_2D);

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int activate(XdevLTextureStage stage) override final {
				XDEVL_ASSERT(m_initialized == xdl_true, "XdevLTextureImpl:: Texture was not initialized with init().");

				glActiveTexture(GL_TEXTURE0 + static_cast<xdl_int>(stage));
				glBindTexture(GL_TEXTURE_2D, m_id);
				m_textureStage = stage;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int deactivate() override final {
				XDEVL_ASSERT(m_initialized == xdl_true, "XdevLTextureImpl:: Texture was not initialized with init().");

				glActiveTexture(GL_TEXTURE0 + static_cast<xdl_int>(m_textureStage));
				glBindTexture(GL_TEXTURE_2D, 0);

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int lock() override final {
				XDEVL_ASSERT(m_lock == xdl_false, "XdevLTextureImpl:: Texture was already locked.");

				glBindTexture(GL_TEXTURE_2D, m_id);
				m_lock = xdl_true;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int unlock() override final {
				XDEVL_ASSERT(m_lock == xdl_true, "XdevLTextureImpl::lock: Texture was not locked.");

				glBindTexture(GL_TEXTURE_2D, 0);
				m_lock = xdl_false;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_uint id() override final {
				return m_id;
			};

			void id(xdl_uint id) {
				m_id = id;
			}

			xdl_uint getWidth() override final {
				return m_width;
			}

			xdl_uint getHeight() override final {
				return m_height;
			}

			void setInitialized(xdl_bool state) {
				m_initialized = state;
			}

			GLuint m_id;
			xdl_bool m_initialized;
			XdevLTextureStage m_textureStage;
			xdl_bool m_lock;
			xdl_uint m_width;
			xdl_uint m_height;
			GLint m_internalFormat;
			xdl_bool m_isRenderBuffer;
			XdevLTextureSamplerImpl* m_textureSampler;
	};
}

#endif
