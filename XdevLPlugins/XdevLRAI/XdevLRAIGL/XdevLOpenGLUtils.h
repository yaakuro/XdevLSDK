/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_OPENGL_UTILS_H
#define XDEVL_OPENGL_UTILS_H

#include <XdevLUtils.h>
#include <XdevLRAI/XdevLTexture.h>
#include <XdevLOpenGLContext/XdevLOpenGLContext.h>
#include <GL/glew.h>

namespace xdl {


	void openGLDebugOutput(GLenum source, GLenum type, GLuint id, GLenum serverity, GLsizei length, const GLchar* message, const void* userParam);
	const char* glGetErrorAsString(GLint error);
	void shaderLog(xdl_uint shaderId);
	void checkOpenGLError(const char* funcName);
	void glExitOnVertexArrayBound(const xdl_char* message);
	void glFlushErrorQueue();


#define BUFFER_OFFSET(i) ((char *)NULL + (i))

#define glExitAndPrintError(ERROR) {std::cout << "###### " <<  XDEVL_FUNC_NAME << ":" << XDEVL_FUNC_LINE << ": " << glGetErrorAsString(ERROR) << std::endl; exit(0);}

#define GL_CHECK_ERROR() { \
		{ \
			GLuint ret = glGetError(); \
			if(ret != GL_NO_ERROR) { \
				glExitAndPrintError(ret); \
			} \
		} \
	}

	/**
	 * @class XdevLOpenGLExtensions
	 * @brief Holds supported extensions.
	 */
	class XdevLOpenGLExtensionsImpl : public XdevLOpenGLExtensions {
		public:
			enum ExtensionTypes {
				ARB_vertex_attrib_binding
				, NVX_gpu_memory_info
				, ARB_framebuffer_sRGB
				, ARB_texture_storage
				, ARB_copy_image
				, ARB_pixel_buffer_object
				, ARB_get_program_binary
				, GLEXT_LAST
			};

			XdevLOpenGLExtensionsImpl()
				: m_extensions {xdl_false} {
			}

			void init() {
				std::vector<std::string> extensionsNames;
			
				std::string tmp((char*)glGetString(GL_EXTENSIONS));
				xstd::tokenize(tmp, extensionsNames, " ");

				std::size_t pos = tmp.find("ARB_vertex_attrib_binding");
				if(pos != std::string::npos) {
					m_extensions[ARB_vertex_attrib_binding] = xdl_true;
				}

				pos = tmp.find("GL_NVX_gpu_memory_info");
				if(pos != std::string::npos) {
					m_extensions[NVX_gpu_memory_info] = xdl_true;
				}

				pos = tmp.find("ARB_framebuffer_sRGB");
				if(pos != std::string::npos) {
					m_extensions[ARB_framebuffer_sRGB] = xdl_true;
				}

				pos = tmp.find("ARB_texture_storage");
				if(pos != std::string::npos) {
					m_extensions[ARB_texture_storage] = xdl_true;
				}

				pos = tmp.find("ARB_copy_image");
				if(pos != std::string::npos) {
					m_extensions[ARB_copy_image] = xdl_true;
				}

				pos = tmp.find("ARB_pixel_buffer_object");
				if(pos != std::string::npos) {
					m_extensions[ARB_pixel_buffer_object] = xdl_true;
				}

				pos = tmp.find("ARB_get_program_binary");
				if(pos != std::string::npos) {
					m_extensions[ARB_get_program_binary] = xdl_true;
				}

			}

			xdl_bool supports(xdl_int extension) override final {
				return m_extensions[extension];
			}

		private:
			xdl_bool m_extensions[GLEXT_LAST];
	};

	using IPXdevLOpenGLExtensionsImpl = std::shared_ptr<XdevLOpenGLExtensionsImpl>;

#ifdef XDEVL_RAIGL_CHECK_VAO_BOUND
#define GL_CHECK_VAO_BOUND(MESSAGE) glExitOnVertexArrayBound(MESSAGE)
#else
#define GL_CHECK_VAO_BOUND(MESSAGE)
#endif

#if defined(XDEVL_PLATFORM_ANDROID)
#define XDEVL_GL_CLEAR_DEPTH(VALUE) glClearDepthf(VALUE);
#else
#define XDEVL_GL_CLEAR_DEPTH(VALUE) glClearDepth(VALUE);
#endif

	GLenum xdevLTextureFormatToGLType(XdevLTextureFormat format);

}

#endif
