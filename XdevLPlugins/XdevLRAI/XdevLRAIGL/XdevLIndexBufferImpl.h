/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_INDEXBUFFER_IMPL_H
#define XDEVL_INDEXBUFFER_IMPL_H

#include <XdevLRAI/XdevLIndexBuffer.h>
#include "XdevLOpenGLUtils.h"

namespace xdl {

	class XdevLIndexBufferImpl : public XdevLBufferBaseImpl<XdevLIndexBuffer, GL_ELEMENT_ARRAY_BUFFER> {
		public:
			XdevLIndexBufferImpl() : m_elementType(XdevLBufferElementTypes::UNSIGNED_INT) {}

			using XdevLBufferBaseImpl<XdevLIndexBuffer, GL_ELEMENT_ARRAY_BUFFER>::init;

			xdl_int init(XdevLBufferElementTypes indexBufferType) override final {
				m_elementType = indexBufferType;
				return XdevLBufferBaseImpl::init();
			}

			xdl_int init(XdevLBufferElementTypes indexBufferType, xdl_uint8* src, xdl_uint numberOfIndices) override final {

				// Create and empty VBO name and bind it.
				glGenBuffers(1, &m_id);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_id);
				if(!glIsBuffer(m_id)) {
					return RET_FAILED;
				}

				// Created and initialize the data store of the IBO.
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, bufferElementTypeSizeInBytes(indexBufferType)*numberOfIndices, src, GL_STREAM_DRAW);

				// Unbind to prevent override in other OpenGL code fragments.
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				m_size = numberOfIndices;
				m_elementType = indexBufferType;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			XdevLBufferElementTypes getElementType() override final {
				return m_elementType;
			}

			xdl_uint getSizeOfElementType() override final {
				return bufferElementTypeSizeInBytes(m_elementType);
			}

			XdevLBufferElementTypes			m_elementType;
	};


}


#endif
