/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_RAI_GL_H
#define XDEVL_RAI_GL_H

#include <tinyxml.h>

#include <XdevLPlatform.h>
#include <XdevLListener.h>
#include <XdevLModule.h>
#include <XdevLPluginImpl.h>

#include <XdevLRAI.h>
#include <XdevLOpenGLContext/XdevLOpenGLContext.h>

#include <GL/glew.h>

namespace xdl {

	static const std::vector<XdevLModuleName>	moduleNames	{
		XdevLModuleName("XdevLRAIGL")
	};

	/**
		@class XdevLRAIGL
		@brief OpenGL implementation of XdevLRAI.
		@author Cengiz Terzibas
	*/
	class XdevLRAIGL : public XdevLModuleImpl<XdevLRAI> {
		public:
			XdevLRAIGL(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLRAIGL();

			xdl_int create(IPXdevLWindow window, IPXdevLArchive archive) override final;
			xdl_int init() override final;

			const xdl_char* getVersion();
			const xdl_char* getVendor();
			xdl_int getNumExtensions() const ;
			const xdl_char* getExtension(xdl_int idx);
			xdl_bool extensionSupported(const xdl_char* extensionName);
			const xdl_char* getShaderVersion();


			XdevLFrameBuffer* getDefaultFrameBuffer() override final;
			XdevLFrameBuffer* get2DFrameBuffer() override final;

			xdl_int setActiveInternalFrameBuffer(xdl_bool state) override final;
			xdl_int setActive2DFrameBuffer(xdl_bool state) override final;
			xdl_int setActiveRenderWindow(XdevLWindow* window) override final;
			void setActiveDepthTest(xdl_bool enableDepthTest) override final;
			
			void setActiveScissorTest(xdl_bool enableScissorTest) override final;
			void setScissor(xdl_float x, xdl_float y, xdl_float width, xdl_float height) override final;

			void setActiveBlendMode(xdl_bool enableBlendMode) override final;
			xdl_int setBlendMode(XdevLBlendModes src, XdevLBlendModes dst, XdevLBlendModes alphaSrc = XDEVL_BLEND_ONE, XdevLBlendModes alphaDst = XDEVL_BLEND_ZERO) override final;
			xdl_int setBlendFunc(XdevLBlendFunc func) override final;

			void setPointSize(xdl_float size) override final;
			void setLineSize(xdl_float size) override final;

			xdl_int clearColorTargets(xdl_float r, xdl_float g, xdl_float b, xdl_float a) override final;
			xdl_int clearDepthTarget(xdl_float clear_value) override final;
			xdl_int setViewport(xdl_float x, xdl_float y, xdl_float width, xdl_float height) override final;
			void setViewport(const XdevLViewPort& viewPort) override final;
			XdevLViewPort getViewport() override final;

			xdl_int swapBuffers() override final;

			IPXdevLVertexDeclaration createVertexDeclaration() override final;
			IPXdevLVertexShader createVertexShader() override final;
			IPXdevLFragmentShader createFragmentShader() override final;
			IPXdevLGeometryShader createGeometryShader() override final;
			IPXdevLTessellationControlShader createTessellationControlShader() override final;
			IPXdevLTessellationEvaluationShader createTessellationEvaluatioinShader() override final;
			IPXdevLShaderProgram createShaderProgram() override final;
			IPXdevLTextureSampler createTextureSampler() override final;
			IPXdevLTexture createTexture() override final;
			IPXdevLTextureCube createTextureCube() override final;
			IPXdevLTexture3D createTexture3D() override final;
			IPXdevLFrameBuffer createFrameBuffer() override final;
			IPXdevLVertexBuffer createVertexBuffer() override final;
			IPXdevLIndexBuffer createIndexBuffer() override final;
			IPXdevLUniformBuffer createUniformBuffer() override final;
			IPXdevLVertexArray createVertexArray() override final;


			xdl_int setActiveTextureSampler(XdevLTextureStage textureStage, IPXdevLTextureSampler textureSampler) override final;
			xdl_int setActiveFrameBuffer(IPXdevLFrameBuffer frambuffer) override final;
			xdl_int setActiveVertexArray(IPXdevLVertexArray vertexArray) override final;
			xdl_int setActiveShaderProgram(IPXdevLShaderProgram shaderProgram) override final;

			xdl_int drawVertexArray(XdevLPrimitiveType primitiveType, xdl_uint numberOfElements, xdl_uint64 indexOffset = 0) override final;
			xdl_int drawInstancedVertexArray(XdevLPrimitiveType primitiveType, xdl_uint numberOfElements, xdl_uint numberOfInstances) override final;

			IPXdevLArchive getArchive() const override final;

			xdl_int initGLEW();
			void shaderLog(xdl_uint shaderID);
			xdl_int initExtensions();
			xdl_int createScreenVertexArray();
			xdl_int renderFrameBufferPlane();
			IPXdevLOpenGLContext getOpenGLContext();

			xdl::xdl_int m_frameBufferTextureShaderId;
			XDEVL_INLINE XdevLWindow* getWindow() {
				return m_window;
			}

		protected:
			XdevLWindow* m_window;
			IPXdevLArchive m_archive;
			XdevLOpenGLContext* m_gl_context;
			/// Holds the stencil depth value.
			xdl_int m_StencilDepth;
			/// Holds the color depth value.
			xdl_int m_ColorDepth;
			/// Holds the z-depth value.
			xdl_int m_ZDepth;
			/// Holds the vsync state.
			xdl_bool m_VSync;
			/// Holds the FSAA state.
			xdl_int m_fsaa;
			/// Holds the number of extensions supported by the graphics card.
			xdl_int	m_numExtensions;
			/// Holds the supported exstension names.
			std::vector<std::string> m_extensions;
			/// Holds the vendor name of the OpenGL driver.
			std::string m_vendor;
			/// Holds the version of the OpenGL driver.
			std::string m_version;

			/// Holds the context version number.
			xdl_int m_major;
			xdl_int m_minor;
			xdl_bool mXDEVL_DEBUG;
			std::string m_profile;
		protected:
			// Gets OpenGL information from the drivers.
			void getOpenGLInfos();
			/// Reads module information from a XML file.
			xdl_int readModuleInformation(TiXmlDocument* document);
			// Supported vertex shader
			xdl_int m_vertex_shader_version;
			// Supported fragment shader
			xdl_int m_fragment_shader_version;
			// Supported geometry shader
			xdl_int m_geometry_shader_version;
			xdl_bool m_useInternalBuffer;
			IPXdevLFrameBuffer m_defaultFrameBuffer;
			IPXdevLFrameBuffer	m_activeFrameBuffer;
			IPXdevLVertexArray 	m_activeVertexArray;
			IPXdevLShaderProgram	m_activeShaderProgram;

			xdl_bool m_use2DFrameBuffer;
			IPXdevLFrameBuffer m_2DFrameBuffer;
			IPXdevLVertexDeclaration m_vertexDeclaration;
			IPXdevLVertexArray m_frameBufferArray;
			IPXdevLShaderProgram m_frameBufferShaderProgram;
			IPXdevLVertexShader m_frameBufferVertexShader;
			IPXdevLFragmentShader m_frameBufferFragmentShader;

			GLboolean m_antialiasedLinesEnabled;
			GLint m_supportedAliasedLineWithRange[2];
			GLint m_supportedSmoothedLineWithRange[2];
			XdevLViewPort m_viewPort;
	};

}

#endif
