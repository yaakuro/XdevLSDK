/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SHADER_IMPL_H
#define XDEVL_SHADER_IMPL_H

#include <XdevLRAI/XdevLShader.h>
#include "XdevLOpenGLUtils.h"
#include <sstream>
#include <fstream>

namespace xdl {

	template<class Base>
	class XdevLShaderBase : public Base {
		public:
			XdevLShaderBase(XdevLShaderType type) :
				m_id(0),
				m_shaderVersion(330),
				m_shaderType(type),
				m_dirtyFlag(xdl_true) {

				// Create the shader.
				m_id = glCreateShader(static_cast<GLenum>(m_shaderType));
				if(m_id == 0) {
					shaderLog(m_id);
					assert(0 && "XdevLShaderBase::XdevLShaderBase: OpenGL 'glCreateShader' failed.");
				}

				// Make the version of the shader to 330.
				std::stringstream ss;
				ss << m_shaderVersion;

				// Add shader version into the first line.
				m_shaderVersionDefinition = "#version " + ss.str() + " core" + "\n";
				m_shaderSource.push_back(m_shaderVersionDefinition);

				GL_CHECK_ERROR()

			}
			virtual ~XdevLShaderBase() {
				if(0 != m_id) {
					glDeleteShader(m_id);
				}
			}

			void addShaderCode(const xdl_char* shadercode) override final {
				m_shaderSource.push_back(std::string(shadercode));
				m_dirtyFlag = xdl_true;
			}

			xdl_int addShaderCode(IPXdevLFile& file) override final {

				std::string shader;
				shader.reserve(static_cast<size_t>(file->length()));
				shader.resize(static_cast<size_t>(file->length()));

				// TODO Possible that not all of the file will be read.
				if(file->read((xdl_uint8*)shader.data(), static_cast<xdl_int>(file->length())) == -1) {
					XDEVL_MODULEX_ERROR(XdevLShaderImpl, "Reading from file: " << file->getFileName() << " failed.\n");
					return RET_FAILED;
				}

				//	m_shaderSource.push_back("#line XDEVL_FUNC_LINE\n");
				m_shaderSource.push_back(shader);
				m_dirtyFlag = xdl_true;
				return RET_SUCCESS;
			}

			xdl_int compile(XdevLFile* file) override final {

				if(file != nullptr) {
					std::string shader;
					shader.reserve(static_cast<size_t>(file->length()));
					shader.resize(static_cast<size_t>(file->length()));

					file->read((xdl_uint8*)shader.data(), static_cast<xdl_int>(file->length()));
					m_shaderSource.push_back(shader);
				}

				if(compile() != RET_SUCCESS) {
					std::cerr << "XdevLShaderBase::compileFromFile: Shader file: " << file->getFileName() << std::endl;
					return RET_FAILED;
				}
				m_dirtyFlag = xdl_false;
				return RET_SUCCESS;
			}

			xdl_int compile() {

				// We have to prepare the number of source code fragments in the source
				// list and the pointers to them.
				std::vector<GLint> lengths;
				std::vector<const xdl::xdl_char*> pointers;

				std::vector<std::string>::iterator ib(m_shaderSource.begin());
				while(ib != m_shaderSource.end()) {
					lengths.push_back((*ib).size());
					pointers.push_back((*ib).data());
					ib++;
				}

				// Tell OpenGL the source we have to far.
				glShaderSource(m_id, m_shaderSource.size(), pointers.data(), lengths.data());

				// Compile and check for errors.
				glCompileShader(m_id);
				GLint status = GL_FALSE;
				glGetShaderiv(m_id, GL_COMPILE_STATUS, &status);
				if(status == GL_FALSE) {
					shaderLog(m_id);
					glDeleteShader(m_id);
					return RET_FAILED;
				}
				m_dirtyFlag = xdl_false;

				GL_CHECK_ERROR()

				return RET_SUCCESS;

			}

			xdl_int id() override final {
				return m_id;
			}

			XdevLShaderType getShaderType() override final {
				return m_shaderType;
			}

			xdl_bool isCompiled() override final {
				return (m_dirtyFlag == xdl_false);
			}

			GLuint m_id;
			xdl_uint m_shaderVersion;
			XdevLShaderType m_shaderType;
			std::vector<std::string> m_shaderSource;
			std::string m_shaderVersionDefinition;
			xdl_bool m_dirtyFlag;
	};

	class XdevLOpenGLVertexShaderImpl : public XdevLShaderBase<XdevLVertexShader> {
		public:
			XdevLOpenGLVertexShaderImpl() : XdevLShaderBase(XdevLShaderType::VERTEX_SHADER) {}

			virtual ~XdevLOpenGLVertexShaderImpl() {}
	};

	class XdevLOpenGLFragmentShaderImpl : public XdevLShaderBase<XdevLFragmentShader> {
		public:
			XdevLOpenGLFragmentShaderImpl() : XdevLShaderBase(XdevLShaderType::FRAGMENT_SHADER) {}

			virtual ~XdevLOpenGLFragmentShaderImpl() {}
	};

	class XdevLOpenGLGeometryShaderImpl : public XdevLShaderBase<XdevLGeometryShader> {
		public:
			XdevLOpenGLGeometryShaderImpl() : XdevLShaderBase(XdevLShaderType::GEOMETRY_SHADER) {}

			virtual ~XdevLOpenGLGeometryShaderImpl() {}
	};

	class XdevLOpenGLTessellationEvaluationShaderImpl : public XdevLShaderBase<XdevLTessellationEvaluationShader> {
		public:
			XdevLOpenGLTessellationEvaluationShaderImpl() : XdevLShaderBase(XdevLShaderType::TESS_EVALUATION_SHADER) {}

			virtual ~XdevLOpenGLTessellationEvaluationShaderImpl() {}
	};

	class XdevLOpenGLTessellationControlShaderImpl : public XdevLShaderBase<XdevLTessellationControlShader> {
		public:
			XdevLOpenGLTessellationControlShaderImpl() : XdevLShaderBase(XdevLShaderType::TESS_CONTROL_SHADER) {}

			virtual ~XdevLOpenGLTessellationControlShaderImpl() {}
	};
}

#endif
