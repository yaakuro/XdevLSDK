/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SHADER_PROGRAM_IMPL_H
#define XDEVL_SHADER_PROGRAM_IMPL_H

#include <XdevLTypes.h>
#include <XdevLRAI/XdevLRAIFWD.h>
#include <XdevLRAI/XdevLShaderProgram.h>

namespace xdl {

	class XdevLOpenGLProgramImpl: public XdevLShaderProgram {
		public:
			XdevLOpenGLProgramImpl(XdevLRAIGL* rai);
			virtual ~XdevLOpenGLProgramImpl();

			xdl_int create(const XdevLString& name) override final;

			xdl_int attach(IPXdevLShader shader) override final;
			xdl_int link() override final;

			void activate() override final;
			void deactivate() override final;

			xdl_int getUniformLocation(const char* id) override final;
			xdl_int getAttribLocation(const char* id) override final;

			void setUniformMatrix4(xdl_uint id, xdl_uint count, const xdl_float* v1) override final;
			void setUniformMatrix3(xdl_uint id, xdl_uint count, const xdl_float* v1) override final;
			void setUniformMatrix2(xdl_uint id, xdl_uint count, const xdl_float* v1) override final;

			void setUniformi(xdl_uint id, xdl_int v1) override final;
			void setUniformi(xdl_uint id, xdl_int v1, xdl_int v2) override final;
			void setUniformi(xdl_uint id, xdl_int v1, xdl_int v2, xdl_int v3) override final;
			void setUniformi(xdl_uint id, xdl_int v1, xdl_int v2, xdl_int v3, xdl_int v4) override final;

			void setUniformui(xdl_uint id, xdl_uint v1) override final;
			void setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2) override final;
			void setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2, xdl_uint v3) override final;
			void setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2, xdl_uint v3, xdl_uint v4) override final;

			void setUniform1uiv(xdl_uint id, xdl_uint count, xdl_uint* v1) override final;
			void setUniform2uiv(xdl_uint id, xdl_uint count, xdl_uint* v1) override final;
			void setUniform3uiv(xdl_uint id, xdl_uint count, xdl_uint* v1) override final;
			void setUniform4uiv(xdl_uint id, xdl_uint count, xdl_uint* v1) override final;

			void setUniform(xdl_uint id, xdl_float v1) override final;
			void setUniform(xdl_uint id, xdl_float v1, xdl_float v2) override final;
			void setUniform(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3) override final;
			void setUniform(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3, xdl_float v4) override final;

			void setUniform1v(xdl_uint id, xdl_uint number, xdl_float* array) override final;
			void setUniform2v(xdl_uint id, xdl_uint number, xdl_float* array) override final;
			void setUniform3v(xdl_uint id, xdl_uint number, xdl_float* array) override final;
			void setUniform4v(xdl_uint id, xdl_uint number, xdl_float* array) override final;

			void setVertexAttrib(xdl_uint id, xdl_float v1) override final;
			void setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2) override final;
			void setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3) override final;
			void setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3, xdl_float v4) override final;

			xdl_int bind(const xdl::XdevLString blockName, xdl_int bindingPoint) override final;

			xdl_uint id() override final;
			void setGLid(GLuint id);

			xdl_int save(IPXdevLFile file) override final;
			xdl_int load(IPXdevLFile file) override final;
			const XdevLString& getName() const override final;
			xdl_int cache() override final;
			xdl_int uncache() override final;

		private:

			XdevLRAIGL* m_rai;
			XdevLString m_name;
			IPXdevLOpenGLExtensions m_extensions;
			GLuint m_id;
			xdl_bool m_inUse;
			xdl_bool m_cacheShader;
			std::vector<IPXdevLShader> m_attachedShaders;
	};

}

#endif
