/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_UNIFORM_BUFFER_IMPL_H
#define XDEVL_UNIFORM_BUFFER_IMPL_H

#include <XdevLRAI/XdevLUniformBuffer.h>
#include "XdevLOpenGLUtils.h"
#include <cassert>
#include <string.h>
#include <iostream>

namespace xdl {

	class XdevLUniformBufferImpl :  public XdevLBufferBaseImpl<XdevLUniformBuffer, GL_ELEMENT_ARRAY_BUFFER>  {

			xdl_int bind(xdl_int bindingPoint) override final {
				glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, m_id);
				auto result = glGetError();
				if(GL_NO_ERROR != result) {
					XDEVL_MODULEX_ERROR(XdevLUniformBuffer, "glBindBufferBase failed: " << glGetErrorAsString(result) << std::endl);
					return RET_FAILED;
				}

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int bindRange(xdl_int bindingPoint, xdl_int offset, xdl_int size) override final {
				glBindBufferRange(GL_UNIFORM_BUFFER, bindingPoint, m_id, offset, size);
				auto result = glGetError();
				if(GL_NO_ERROR != result) {
					XDEVL_MODULEX_ERROR(XdevLUniformBuffer, "glBindBufferRange failed: " << glGetErrorAsString(result) << std::endl);
					return RET_FAILED;
				}

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}
	};


}


#endif
