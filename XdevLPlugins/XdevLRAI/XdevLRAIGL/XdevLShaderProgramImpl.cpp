/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "GL/glew.h"
#include "XdevLRAIGL.h"
#include "XdevLOpenGLUtils.h"
#include "XdevLShaderProgramImpl.h"
#include "XdevLOpenGLUtils.h"
#include <XdevLError.h>
#include <cassert>
#include <iostream>

namespace xdl {


	XdevLOpenGLProgramImpl::XdevLOpenGLProgramImpl(XdevLRAIGL* rai)
		: m_rai(rai)
		, m_id(0)
		, m_inUse(xdl_false)
		, m_cacheShader(xdl_true) {
		XDEVL_ASSERT(nullptr != m_rai, "Invalid IPXdevLRAI specified.");

		m_id = glCreateProgram();
		XDEVL_ASSERT(0 != m_id, "glCreateProgram failed.");
		m_extensions = m_rai->getOpenGLContext()->getExtensions();
	}

	XdevLOpenGLProgramImpl::~XdevLOpenGLProgramImpl() {
		if(m_id != 0) {
			glDeleteProgram(m_id);
			m_id = 0;
		}
	}

	xdl_int XdevLOpenGLProgramImpl::create(const XdevLString& name) {
		m_name = name;
		return RET_SUCCESS;
	}

	const XdevLString& XdevLOpenGLProgramImpl::getName() const {
		return m_name;
	}

	xdl_int XdevLOpenGLProgramImpl::attach(IPXdevLShader shader) {
		// According to OpenGL spec.
		// Quote: Shader objects may be attached to program objects before source code has
		// been loaded into the shader object, or before the shader object has been compiled.

		glAttachShader(m_id, shader->id());
		m_attachedShaders.push_back(shader);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLProgramImpl::link() {

		// According to the OpenGL specs
		if(m_attachedShaders.size() == 0) {
			XDEVL_MODULEX_ERROR(XdevLRAI, "No shader objects are attached to program.\n");
			return RET_FAILED;
		}

		// If you check the comment in the attach method you will see that according to OpenGL specs
		// the shader objects has to be compiled. Let's make that sure here.
		for(auto& shader : m_attachedShaders) {
			if(shader->isCompiled() == xdl_false) {
				XDEVL_MODULEX_ERROR(XdevLRAI, "At least one of the attached shaders are not compiled.\n");
				return RET_FAILED;
			}
		}

		glLinkProgram(m_id);

		GLint LinkStatus;
		glGetProgramiv(m_id, GL_LINK_STATUS, &LinkStatus);
		if(LinkStatus == GL_FALSE) {
			XDEVL_MODULEX_ERROR(XdevLRAI, "Couldn't link shader objects.\n");
			return RET_FAILED;
		}

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLProgramImpl::getUniformLocation(const char* id) {
		GLint tmp =  glGetUniformLocation(this->m_id, id);
		if(tmp == -1) {
			checkOpenGLError("glGetUniformLocation");
		}

		GL_CHECK_ERROR()

		return tmp;
	}

	xdl_int XdevLOpenGLProgramImpl::getAttribLocation(const char* id) {
		GLint tmp = glGetAttribLocation(this->m_id, id);
		if(tmp == -1) {
			checkOpenGLError("glGetAttribLocation");
		}

		GL_CHECK_ERROR()

		return tmp;
	}

	void XdevLOpenGLProgramImpl::activate() {
		glUseProgram(m_id);
		m_inUse = true;

		GL_CHECK_ERROR()
	}
	void XdevLOpenGLProgramImpl::deactivate() {
		glUseProgram(0);
		m_inUse = false;

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformMatrix4(xdl_uint id, xdl_uint count, const xdl_float* v1) {
		assert(m_inUse && "activate() not used.\n");
		glUniformMatrix4fv(id, count, 0, v1);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformMatrix3(xdl_uint id, xdl_uint count, const xdl_float* v1) {
		assert(m_inUse && "activate() not used.\n");
		glUniformMatrix3fv(id, count, 0, v1);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformMatrix2(xdl_uint id, xdl_uint count, const xdl_float* v1) {
		assert(m_inUse && "activate() not used.\n");
		glUniformMatrix2fv(id, count, 0, v1);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformi(xdl_uint id, xdl_int v1) {
		assert(m_inUse && "activate() not used.\n");
		glUniform1i(id, v1);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformi(xdl_uint id, xdl_int v1, xdl_int v2) {
		assert(m_inUse && "activate() not used.\n");
		glUniform2i(id, v1, v2);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformi(xdl_uint id, xdl_int v1, xdl_int v2, xdl_int v3) {
		assert(m_inUse && "activate() not used.\n");
		glUniform3i(id, v1, v2, v3);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformi(xdl_uint id, xdl_int v1, xdl_int v2, xdl_int v3, xdl_int v4) {
		assert(m_inUse && "activate() not used.\n");
		glUniform4i(id, v1, v2, v3, v4);

		GL_CHECK_ERROR()
	}


	void XdevLOpenGLProgramImpl::setUniformui(xdl_uint id, xdl_uint v1) {
		assert(m_inUse && "activate() not used.\n");
		glUniform1ui(id, v1);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2) {
		assert(m_inUse && "activate() not used.\n");
		glUniform2ui(id, v1, v2);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2, xdl_uint v3) {
		assert(m_inUse && "activate() not used.\n");
		glUniform3ui(id, v1, v2, v3);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2, xdl_uint v3, xdl_uint v4) {
		assert(m_inUse && "activate() not used.\n");
		glUniform4ui(id, v1, v2, v3, v4);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform1uiv(xdl_uint id, xdl_uint count, xdl_uint* v) {
		assert(m_inUse && "activate() not used.\n");
		glUniform1uiv(id, count, v);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform2uiv(xdl_uint id, xdl_uint count, xdl_uint* v) {
		assert(m_inUse && "activate() not used.\n");
		glUniform2uiv(id, count, v);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform3uiv(xdl_uint id, xdl_uint count, xdl_uint* v) {
		assert(m_inUse && "activate() not used.\n");
		glUniform4uiv(id, count, v);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform4uiv(xdl_uint id, xdl_uint count, xdl_uint* v) {
		assert(m_inUse && "activate() not used.\n");
		glUniform4uiv(id, count, v);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform(xdl_uint id, xdl_float v1) {
		assert(m_inUse && "activate() not used.\n");
		glUniform1f(id, v1);

		GL_CHECK_ERROR()
	}
	void XdevLOpenGLProgramImpl::setUniform(xdl_uint id, xdl_float v1, xdl_float v2) {
		assert(m_inUse && "activate() not used.\n");
		glUniform2f(id, v1, v2);

		GL_CHECK_ERROR()
	}
	void XdevLOpenGLProgramImpl::setUniform(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3) {
		assert(m_inUse && "activate() not used.\n");
		glUniform3f(id, v1, v2, v3);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3, xdl_float v4) {
		assert(m_inUse && "activate() not used.\n");
		glUniform4f(id, v1, v2, v3, v4);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform1v(xdl_uint id, xdl_uint number, xdl_float* array) {
		assert(m_inUse && "activate() not used.\n");
		glUniform1fv(id, number, array);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform2v(xdl_uint id, xdl_uint number, xdl_float* array) {
		assert(m_inUse && "activate() not used.\n");
		glUniform2fv(id, number, array);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform3v(xdl_uint id, xdl_uint number, xdl_float* array) {
		assert(m_inUse && "activate() not used.\n");
		glUniform3fv(id, number, array);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setUniform4v(xdl_uint id, xdl_uint number, xdl_float* array) {
		assert(m_inUse && "activate() not used.\n");
		glUniform4fv(id, number, array);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setVertexAttrib(xdl_uint id, xdl_float v1) {
		assert(m_inUse && "begin() not used.\n");
		glVertexAttrib1f(id, v1);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2) {
		assert(m_inUse && "begin() not used.\n");
		glVertexAttrib2f(id, v1, v2);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3) {
		assert(m_inUse && "begin() not used.\n");
		glVertexAttrib3f(id, v1, v2, v3);

		GL_CHECK_ERROR()
	}

	void XdevLOpenGLProgramImpl::setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3, xdl_float v4) {
		assert(m_inUse && "begin() not used.\n");
		glVertexAttrib4f(id, v1, v2, v3, v4);

		GL_CHECK_ERROR()
	}

	xdl_uint XdevLOpenGLProgramImpl::id() {
		return m_id;
	}
	void XdevLOpenGLProgramImpl::setGLid(GLuint id) {
		m_id = id;
	}

	xdl_int XdevLOpenGLProgramImpl::bind(const XdevLString blockName, xdl_int bindingPoint) {
		GLuint returnedIndex = glGetUniformBlockIndex(m_id, blockName.toString().c_str());
		if(GL_INVALID_INDEX == returnedIndex) {
			XDEVL_MODULEX_ERROR(XdevLRAI, "glGetUniformBlockIndex for '" << blockName << "' failed.\n");
			return RET_FAILED;
		}

		glUniformBlockBinding(m_id, returnedIndex, bindingPoint);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLProgramImpl::cache() {
		if(m_cacheShader && m_extensions->supports(XdevLOpenGLExtensionsImpl::ARB_get_program_binary) && m_rai && m_rai->getArchive()) {

			const auto shaderProgramFileName = XdevLFileName(m_name) + XdevLFileName(TEXT(".spb"));
			auto shaderFile = m_rai->getArchive()->open(XdevLOpenForWriteOnly(), shaderProgramFileName);
			GLint formats = 0;
			glGetIntegerv(GL_NUM_PROGRAM_BINARY_FORMATS, &formats);

			GLint *binaryFormats = new GLint[formats];
			glGetIntegerv(GL_PROGRAM_BINARY_FORMATS, binaryFormats);

			GLint length = 0;
			glGetProgramiv(m_id, GL_PROGRAM_BINARY_LENGTH, &length);

			static std::vector<xdl_uint8> buffer;
			if(length != 0) {
				if((GLint)buffer.size() < length) {
					buffer.reserve(length);
					buffer.resize(length);
				}
				GLenum binaryFormat = 0;
				glGetProgramBinary(m_id, length, nullptr, &binaryFormat, buffer.data());

				//
				// Write the binary format of this shader program.
				//
				shaderFile->writeU64(binaryFormat);

				//
				// Write the binary data of this shader program.
				//
				shaderFile->write(buffer.data(), length);
				shaderFile->close();
			}

			GL_CHECK_ERROR()

			return RET_SUCCESS;
		}
		return RET_FAILED;
	}

	xdl_int XdevLOpenGLProgramImpl::uncache() {
		if(m_cacheShader && m_extensions->supports(XdevLOpenGLExtensionsImpl::ARB_get_program_binary) && m_rai && m_rai->getArchive()) {
			const auto shaderProgramFileName = XdevLFileName(m_name) + XdevLFileName(TEXT(".spb"));
			if(m_rai->getArchive()->fileExists(shaderProgramFileName)) {
				auto shaderFile = m_rai->getArchive()->open(xdl::XdevLOpenForReadOnly(), shaderProgramFileName);

				// Read the binary format.
				xdl_uint64 format = 0;
				shaderFile->readU64(format);


				//
				// Read the binary shader program data.
				//
				size_t length = shaderFile->length() - sizeof(xdl_uint64);

				static std::vector<xdl_uint8> buffer;
				if(buffer.size() < length) {
					buffer.reserve(length);
					buffer.resize(length);
				}

				//
				// Create new shader program.
				//
				shaderFile->read(buffer.data(), length);
				if(0 != m_id) {
					glDeleteProgram(m_id);
					m_id = 0;
				}

				m_id = glCreateProgram();
				glProgramBinary(m_id, format, (const void*)buffer.data(), length);

				GLint LinkStatus;
				glGetProgramiv(m_id, GL_LINK_STATUS, &LinkStatus);
				if(LinkStatus == GL_FALSE) {
					XDEVL_MODULEX_ERROR(XdevLRAI, "Couldn't link shader objects.\n");
					return RET_FAILED;
				}

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}
		}
		return RET_FAILED;
	}

	xdl_int XdevLOpenGLProgramImpl::save(IPXdevLFile file)  {
		XDEVL_ASSERT(nullptr != file, "Invalid IPXdevLFile specified.");
		GL_CHECK_ERROR()
		return RET_FAILED;
	}

	xdl_int XdevLOpenGLProgramImpl::load(IPXdevLFile file) {
		XDEVL_ASSERT(nullptr != file, "Invalid IPXdevLFile specified.");
		GL_CHECK_ERROR()
		return RET_FAILED;
	}

}
