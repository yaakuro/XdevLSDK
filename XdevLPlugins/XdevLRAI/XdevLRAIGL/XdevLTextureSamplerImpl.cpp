/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLTextureSamplerImpl.h"

namespace xdl {

	XdevLTextureSamplerImpl::XdevLTextureSamplerImpl()
		: m_id(0)
		, m_stage(GL_TEXTURE0)
		, m_activate(xdl_false)
		, m_locked(xdl_false) {
		glGenSamplers(1, &m_id);
	}

	XdevLTextureSamplerImpl::~XdevLTextureSamplerImpl() {
		if(0 != m_id) {
			glDeleteSamplers(1, &m_id);
			m_id = 0;
		}
	}

	void XdevLTextureSamplerImpl::activate(XdevLTextureStage stage) {
		m_stage = static_cast<GLint>(stage);
		glBindSampler(static_cast<GLint>(stage), m_id);

		GL_CHECK_ERROR()

		m_activate = xdl_true;
	}

	void XdevLTextureSamplerImpl::deactivate() {
		glBindSampler(m_stage, 0);
		m_activate = xdl_false;

		GL_CHECK_ERROR()

	}

	void XdevLTextureSamplerImpl::lock() {
		m_locked = xdl_true;
		glBindSampler(m_stage, m_id);

		GL_CHECK_ERROR()

	}

	void XdevLTextureSamplerImpl::unlock() {
		m_locked = xdl_false;
		glBindSampler(m_stage, 0);

		GL_CHECK_ERROR()

	}

	xdl_int XdevLTextureSamplerImpl::setTextureWrap(const XdevLTextureSamplerWrap& wrap) {
		XDEVL_ASSERT(m_activate == xdl_true, "XdevLTextureSampler::lock: Texture was not locked.");

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, static_cast<GLint>(wrap.addressModeU));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, static_cast<GLint>(wrap.addressModeV));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, static_cast<GLint>(wrap.addressModeW));

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLTextureSamplerImpl::setTextureFilter(const XdevLTextureSamplerFilterStage& filterStage) {
		XDEVL_ASSERT(m_activate == xdl_true, "XdevLTextureSampler::lock: Texture was not locked.");

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<GLint>(filterStage.magFilter));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<GLint>(filterStage.minFilter));

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLTextureSamplerImpl::setLOD(const XdevLTextureSamplerLOD& lod) {
		XDEVL_ASSERT(m_activate == xdl_true, "XdevLTextureSampler::lock: Texture was not locked.");

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, lod.minLod);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, lod.maxLod);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, lod.lodBias);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLTextureSamplerImpl::setTextureCompare(const XdevLTextureSamplerCompare& compare) {
		XDEVL_ASSERT(m_activate == xdl_true, "XdevLTextureSampler::lock: Texture was not locked.");

		if(compare.enable) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, static_cast<GLint>(compare.compareOp));
		} else {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
		}

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLTextureSamplerImpl::setTextureAnisotropy(const XdevLTextureSamplerAnisotropic& anisotropic) {
		XDEVL_ASSERT(m_activate == xdl_true, "XdevLTextureSampler::lock: Texture was not locked.");

		// Value of 1.0f means to switch of anisotropic filtering.
		GLfloat maxValue = 1.0f;
		if(anisotropic.anisotropyEnable) {
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxValue);
			if(anisotropic.maxAnisotropy < maxValue) {
				maxValue = anisotropic.maxAnisotropy;
			}
		}
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxValue);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLTextureSamplerImpl::setBorderColor(XdevLTextureSamplerBorderColor borderColor) {
		XDEVL_ASSERT(m_activate == xdl_true, "XdevLTextureSampler::lock: Texture was not locked.");

		switch(borderColor) {
			case XdevLTextureSamplerBorderColor::FLOAT_TRANSPARENT_BLACK: {
				const GLfloat color[] {0.0f, 0.0f, 0.0f, 0.0f};
				glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
			} break;
			case XdevLTextureSamplerBorderColor::INT_TRANSPARENT_BLACK: {
				const GLint color[] {0, 0, 0, 0};
				glTexParameterIiv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
			} break;
			case XdevLTextureSamplerBorderColor::FLOAT_OPAQUE_BLACK: {
				const GLfloat color[] {0.0f, 0.0f, 0.0f, 1.0f};
				glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
			} break;
			case XdevLTextureSamplerBorderColor::INT_OPAQUE_BLACK: {
				const GLint color[] {0, 0, 0, 255};
				glTexParameterIiv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
			} break;
			case XdevLTextureSamplerBorderColor::FLOAT_OPAQUE_WHITE: {
				const GLfloat color[] {1.0f, 1.0f, 1.0f, 1.0f};
				glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
			} break;
			case XdevLTextureSamplerBorderColor::INT_OPAQUE_WHITE: {
				const GLint color[] {255, 255, 255, 255};
				glTexParameterIiv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &color[0]);
			} break;
			default: break;
		}

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}
}
