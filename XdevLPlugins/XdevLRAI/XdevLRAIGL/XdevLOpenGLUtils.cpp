/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "GL/glew.h"
#include "XdevLOpenGLUtils.h"

namespace xdl {

	void openGLDebugOutput(GLenum source, GLenum type, GLuint id, GLenum serverity, GLsizei length, const GLchar* message, const void* userParam) {
		std::cout << message << std::endl;
	}

	const char* glGetErrorAsString(GLint error) {
		switch(error) {
			case GL_NO_ERROR:
				return "GL_NO_ERROR";
			case GL_INVALID_ENUM:
				return "GL_INVALID_ENUM";
			case GL_INVALID_VALUE:
				return "GL_INVALID_VALUE";
			case GL_INVALID_OPERATION:
				return "GL_INVALID_OPERATION";
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				return "GL_INVALID_FRAMEBUFFER_OPERATION";
			case GL_OUT_OF_MEMORY:
				return "GL_OUT_OF_MEMORY";
			case GL_STACK_UNDERFLOW:
				return "GL_STACK_UNDERFLOW";
			case GL_STACK_OVERFLOW:
				return "GL_STACK_OVERFLOW";
			default:
				break;
		}
		return "GL_UNKOWN_ERROR";
	}

	void checkOpenGLError(const char* funcName) {
		GLenum error = glGetError();
		if(GL_NO_ERROR != error) {
			switch(error) {
				case GL_INVALID_ENUM: {
					std::cerr << funcName << ":: Error: " << "GL_INVALID_ENUM" <<std::endl;
				}
				break;
				case GL_INVALID_VALUE: {
					std::cerr << funcName << ":: Error: " << "GL_INVALID_VALUE" <<std::endl;
				}
				break;
				case GL_INVALID_OPERATION: {
					std::cerr << funcName << ":: Error: " << "GL_INVALID_OPERATION" <<std::endl;
				}
				break;
				case GL_INVALID_FRAMEBUFFER_OPERATION: {
					std::cerr << funcName << ":: Error: " << "GL_INVALID_FRAMEBUFFER_OPERATION" <<std::endl;
				}
				break;
				case GL_OUT_OF_MEMORY: {
					std::cerr << funcName << ":: Error: " << "GL_OUT_OF_MEMORY" <<std::endl;
				}
				break;
				case GL_STACK_UNDERFLOW: {
					std::cerr << funcName << ":: Error: " << "GL_STACK_UNDERFLOW" <<std::endl;
				}
				break;
				case GL_STACK_OVERFLOW: {
					std::cerr << funcName << ":: Error: " << "GL_STACK_OVERFLOW" <<std::endl;
				}
				break;
			}
		}

	}

	void shaderLog(xdl_uint shaderId) {
		// Get the length of the message.
		GLint Len;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &Len);

		GLsizei ActualLen;
		GLchar *Log = new GLchar[Len];
		glGetShaderInfoLog(shaderId, Len, &ActualLen, Log);
		std::cout << "Shader ID: " << shaderId << ", Message: " << Log << "\n";
		delete [] Log;
	}

	void glExitOnVertexArrayBound(const xdl_char* message) {
		GLint vao = 0;
		glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &vao);
		if(0 != vao) {
			XDEVL_ASSERT(0, message);
		}
	}

	void glFlushErrorQueue() {
		for(;;) {
			GLint ret = glGetError();
			if(GL_NO_ERROR == ret) {
				break;
			}
		}
	}

	GLenum xdevLTextureFormatToGLType(XdevLTextureFormat format) {
		switch(format) {
			case XdevLTextureFormat::R8: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG8: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB8: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA8: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R8I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG8I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB8I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA8I: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R8UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG8UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB8UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA8UI: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R16: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG16: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB16: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA16: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R16I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG16I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB16I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA16I: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R16UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG16UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB16UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA16UI: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R16F: return GL_FLOAT;
			case XdevLTextureFormat::RG16F: return GL_FLOAT;
			case XdevLTextureFormat::RGB16F: return GL_FLOAT;
			case XdevLTextureFormat::RGBA16F: return GL_FLOAT;

			case XdevLTextureFormat::R32I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG32I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB32I: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA32I: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R32UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG32UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB32UI: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA32UI: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R32F: return GL_FLOAT;
			case XdevLTextureFormat::RG32F: return GL_FLOAT;
			case XdevLTextureFormat::RGB32F: return GL_FLOAT;
			case XdevLTextureFormat::RGBA32F: return GL_FLOAT;

			case XdevLTextureFormat::R11F_G11F_B10F: return GL_UNSIGNED_INT_10F_11F_11F_REV_EXT;
			case XdevLTextureFormat::RGB9_E5: return GL_UNSIGNED_INT_5_9_9_9_REV_EXT;
			case XdevLTextureFormat::SRGB: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::SRGB8: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::SRGB_ALPHA: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::SRGB8_ALPHA8: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R8_SNORM: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG8_SNORM: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB8_SNORM: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA8_SNORM: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::R16_SNORM: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RG16_SNORM: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGB16_SNORM: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::RGBA16_SNORM: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::COMPRESSED_ALPHA: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_LUMINANCE: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_LUMINANCE_ALPHA: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_INTENSITY: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_RGB: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_RGBA: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_SRGB: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_SRGB_ALPHA: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_SLUMINANCE: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_SLUMINANCE_ALPHA: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_SRGB_S3TC_DXT1: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_SRGB_ALPHA_S3TC_DXT1: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_SRGB_ALPHA_S3TC_DXT3: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::COMPRESSED_SRGB_ALPHA_S3TC_DXT5: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::DEPTH_COMPONENT: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::DEPTH_COMPONENT16: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::DEPTH_COMPONENT24: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::DEPTH_COMPONENT32: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::DEPTH_COMPONENT32F: return GL_UNSIGNED_BYTE;

			case XdevLTextureFormat::DEPTH_STENCIL: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::DEPTH24_STENCIL8: return GL_UNSIGNED_BYTE;
			case XdevLTextureFormat::DEPTH32F_STENCIL8: return GL_UNSIGNED_BYTE;
			default: break;
		}
		return GL_UNSIGNED_BYTE;
	}
}
