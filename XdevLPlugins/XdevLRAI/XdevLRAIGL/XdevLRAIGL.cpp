/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <stdexcept>
#include <fstream>
#include <cmath>

#include <XdevLCoreMediator.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLXstring.h>

#include "XdevLFrameBufferImpl.h"
#include "XdevLRAIGL.h"
#include "XdevLVertexBufferImpl.h"
#include "XdevLIndexBufferImpl.h"
#include "XdevLUniformBufferImpl.h"
#include "XdevLVertexArrayImpl.h"
#include "XdevLShaderProgramImpl.h"
#include "XdevLShaderImpl.h"
#include "XdevLTextureSamplerImpl.h"
#include "XdevLTextureCubeImpl.h"
#include "XdevLTexture3DImpl.h"
#include "XdevLOpenGLUtils.h"

class m_activeVertexArray;
class vertexArray;
class wrappPrimitiveType;

namespace xdl {

	std::string vertex_shader_source = {
		"\
layout(location = 0) in vec3 iposition;\
layout(location = 1) in vec2 itexcoord;\
\
out vec2 texcoord;\
\
void main() {\
 texcoord = itexcoord;\
 gl_Position = vec4(iposition * 0.5, 1.0);\
}"
	};

	std::string fragment_shader_source = {
		"\
layout(location = 0) out vec4 ocolor;\
in vec2 texcoord;\
uniform sampler2D tex;\
\
void main() {\
\
 ocolor = texture(tex, texcoord);\
}"
	};

	GLenum wrapToGLPrimitive(const XdevLPrimitiveType& type) {
		switch(type) {
			case XDEVL_PRIMITIVE_POINTS:
				return GL_POINTS;
			case XDEVL_PRIMITIVE_LINES:
				return GL_LINES;
			case XDEVL_PRIMITIVE_LINE_LOOP:
				return GL_LINE_LOOP;
			case XDEVL_PRIMITIVE_LINE_STRIP:
				return GL_LINE_STRIP;
			case XDEVL_PRIMITIVE_TRIANGLES:
				return GL_TRIANGLES;
			case XDEVL_PRIMITIVE_TRIANGLE_STRIP:
				return GL_TRIANGLE_STRIP;
			case XDEVL_PRIMITIVE_TRIANGLE_FAN:
				return GL_TRIANGLE_FAN;
			default:
				break;
		}
		XDEVL_ASSERT(0, "Not supported XdevLPrimitiveType.");
		return XDEVL_PRIMITIVE_UNKNOWN;
	}

	GLenum wrapToGLElementType(const XdevLBufferElementTypes& elementType) {
		switch(elementType) {
			case XdevLBufferElementTypes::UNSIGNED_BYTE:
				return GL_UNSIGNED_BYTE;
			case XdevLBufferElementTypes::UNSIGNED_SHORT:
				return GL_UNSIGNED_SHORT;
			case XdevLBufferElementTypes::UNSIGNED_INT:
				return GL_UNSIGNED_INT;
			default:
				break;
		}
		XDEVL_ASSERT(0, "Not supported XdevLBufferElementTypes");
		return 0;
	}

	XdevLRAIGL::XdevLRAIGL(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) : XdevLModuleImpl<XdevLRAI>(parameter, descriptor),
		m_window(nullptr),
		m_archive(nullptr),
		m_gl_context(nullptr),
		m_StencilDepth(8),
		m_ColorDepth(32),
		m_ZDepth(24),
		m_VSync(false),
		m_fsaa(false),
		m_major(3),
		m_minor(2),
		mXDEVL_DEBUG(false),
		m_profile("core"),
		m_vertex_shader_version(330),
		m_fragment_shader_version(330),
		m_geometry_shader_version(330),
		m_useInternalBuffer(xdl_false),
		m_defaultFrameBuffer(nullptr),
		m_activeFrameBuffer(nullptr),
		m_activeVertexArray(nullptr),
		m_activeShaderProgram(nullptr),
		m_use2DFrameBuffer(xdl_false),
		m_2DFrameBuffer(nullptr),
		m_vertexDeclaration(nullptr),
		m_frameBufferArray(nullptr),
		m_frameBufferShaderProgram(nullptr),
		m_frameBufferVertexShader(nullptr),
		m_frameBufferFragmentShader(nullptr) {
	}

	XdevLRAIGL::~XdevLRAIGL() {
	}

	xdl_int XdevLRAIGL::init() {
		auto coreParameter = getMediator()->getCoreParameter();
		if(coreParameter.xmlBuffer.size() != 0) {
			TiXmlDocument xmlDocument;
			if(!xmlDocument.Parse((xdl_char*)coreParameter.xmlBuffer.data())) {
				XDEVL_MODULE_ERROR("Could not parse xml buffer.\n ");
				return RET_FAILED;
			}

			if(readModuleInformation(&xmlDocument) != RET_SUCCESS) {
				return RET_FAILED;
			}
		}
		return RET_SUCCESS;
	}

	IPXdevLArchive XdevLRAIGL::getArchive() const {
		return m_archive;
	}

	int XdevLRAIGL::create(IPXdevLWindow window, IPXdevLArchive archive) {
		XdevLString name(window->getTitle() + XdevLString("XdevLRAIOpenGLContext"));
		XdevLID id(name);
		m_archive = archive;

		m_gl_context = static_cast<XdevLOpenGLContext*>(getMediator()->createModule(XdevLModuleName("XdevLOpenGLContext"), id));
		if(nullptr == m_gl_context) {
			XDEVL_MODULE_WARNING("Could not create OpenGL context. Assuming you're using a manually created one.\n");
		} else {
			// Create OpenGL context.
			if(m_gl_context->create(window) != RET_SUCCESS) {
				return RET_FAILED;
			}
			m_gl_context->makeCurrent(window);
		}

		XDEVL_MODULE_INFO(glGetString(GL_VENDOR) << std::endl);
		XDEVL_MODULE_INFO(glGetString(GL_RENDERER) << std::endl);
		XDEVL_MODULE_INFO(glGetString(GL_VERSION) << std::endl);


		if(initGLEW() != RET_SUCCESS) {
			return RET_FAILED;
		}

		//
		// Initialize the default framebuffer.
		//
		m_defaultFrameBuffer = createFrameBuffer();
		m_defaultFrameBuffer->init(320, 200);
		if(m_defaultFrameBuffer->addColorTarget(0, XdevLFrameBufferColorFormat::RGBA16F) != RET_SUCCESS) {
			XDEVL_MODULE_ERROR("Could't create color target for default framebuffer.\n")
		} else {
			auto texture = m_defaultFrameBuffer->getTexture(0);

			const XdevLTextureSamplerFilterStage filterStage {XdevLTextureFilterMin::LINEAR, XdevLTextureFilterMag::LINEAR};
			const XdevLTextureSamplerWrap wrap {XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE};

			texture->lock();
			texture->setTextureFilter(filterStage);
			texture->setTextureWrap(wrap);
			texture->unlock();

			if(m_defaultFrameBuffer->addDepthTarget(XdevLFrameBufferDepthFormat::COMPONENT24) != RET_SUCCESS) {
				XDEVL_MODULE_ERROR("Could't create depth target for default framebuffer.\n")
			}

			// Assign the new framebuffer as the active one.
			m_activeFrameBuffer = m_defaultFrameBuffer;
		}

		//
		// Initialize the default framebuffer.
		//
		m_2DFrameBuffer = createFrameBuffer();
		m_2DFrameBuffer->init(window->getWidth(), window->getHeight());
		if(m_2DFrameBuffer->addColorTarget(0, XdevLFrameBufferColorFormat::RGBA16F) != RET_SUCCESS) {
			XDEVL_MODULE_ERROR("Could't create color target for default framebuffer.\n")
		} else {
			auto texture = m_2DFrameBuffer->getTexture(0);

			const XdevLTextureSamplerFilterStage filterStage {XdevLTextureFilterMin::LINEAR, XdevLTextureFilterMag::LINEAR};
			const XdevLTextureSamplerWrap wrap {XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE, XdevLTextureWrap::CLAMP_TO_EDGE};

			texture->lock();
			texture->setTextureFilter(filterStage);
			texture->setTextureWrap(wrap);
			texture->unlock();
		}
		if(m_2DFrameBuffer->addDepthTarget(XdevLFrameBufferDepthFormat::COMPONENT24) != RET_SUCCESS) {
			XDEVL_MODULE_ERROR("Could't create depth target for default framebuffer.\n")
		}
		m_window = window;

		const auto windowSize = window->getSize();

		XdevLViewPort viewPort {};
		viewPort.width = windowSize.width;
		viewPort.height = windowSize.height;
		setViewport(viewPort);

		createScreenVertexArray();

		glGetIntegerv(GL_ALIASED_LINE_WIDTH_RANGE, m_supportedAliasedLineWithRange);
		glGetIntegerv(GL_SMOOTH_LINE_WIDTH_RANGE, m_supportedSmoothedLineWithRange);

		glDisable(GL_LINE_SMOOTH);


		XDEVL_MODULE_SUCCESS("OpenGL successfully initialized.\n")

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::setActiveInternalFrameBuffer(xdl_bool state) {
		m_useInternalBuffer = state;
		if(state) {
			m_defaultFrameBuffer->activate();

			XdevLFrameBufferColorTargets list[] = {XdevLFrameBufferColorTargets::TARGET0};
			m_defaultFrameBuffer->activateColorTargets(1, list);
		} else {
			m_defaultFrameBuffer->deactivate();
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::setActive2DFrameBuffer(xdl_bool state) {
		m_use2DFrameBuffer = state;

		if(state) {
			m_defaultFrameBuffer->deactivate();
			m_2DFrameBuffer->activate();

			XdevLFrameBufferColorTargets list[] = {XdevLFrameBufferColorTargets::TARGET0};
			m_2DFrameBuffer->activateColorTargets(1, list);
		} else {
			m_2DFrameBuffer->deactivate();
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::readModuleInformation(TiXmlDocument* document) {
		TiXmlHandle docHandle(document);
		TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toString().c_str()).FirstChildElement("XdevLOpenGL").ToElement();

		if(!root) {
			XDEVL_MODULE_INFO("<XdevLOpenGL> section not found, skipping proccess.\n");
			return RET_SUCCESS;
		}
		while(root != NULL) {
			if(root->Attribute("id")) {
				XdevLID id(root->Attribute("id"));
				if(getID() == id) {
					if(root->Attribute("framebuffer_depth")) {
						m_ColorDepth = xstd::from_string<xdl_int>(root->Attribute("framebuffer_depth"));
						XDEVL_MODULE_INFO("Framebuffer depth request: " << m_ColorDepth << std::endl);
					}
					if(root->Attribute("z_buffer_depth")) {
						m_ZDepth = xstd::from_string<xdl_int>(root->Attribute("z_buffer_depth"));
						XDEVL_MODULE_INFO("Z-Buffer depth request: " << m_ZDepth << std::endl);
					}
					if(root->Attribute("stencil_buffer_depth")) {
						m_StencilDepth = xstd::from_string<xdl_int>(root->Attribute("stencil_buffer_depth"));
						XDEVL_MODULE_INFO("Stencilbuffer depth request: " << m_StencilDepth << std::endl);
					}
					if(root->Attribute("vsync"))
						m_VSync = xstd::from_string<xdl_bool>(root->Attribute("vsync"));
					if(root->Attribute("fsaa"))
						m_fsaa = xstd::from_string<xdl_int>(root->Attribute("fsaa"));
					if(root->Attribute("major"))
						m_major = xstd::from_string<xdl_int>(root->Attribute("major"));
					if(root->Attribute("minor"))
						m_minor = xstd::from_string<xdl_int>(root->Attribute("minor"));
					if(root->Attribute("debug"))
						mXDEVL_DEBUG = xstd::from_string<xdl_bool>(root->Attribute("debug"));
					if(root->Attribute("profile"))
						m_profile = root->Attribute("profile");
				}
			} else
				XDEVL_MODULE_ERROR("No 'id' attribute specified. Using default values for the device\n");

			root = root->NextSiblingElement();
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::initGLEW() {
		glewExperimental= GL_TRUE;
		GLenum err = glewInit();
		if(GLEW_OK != err) {
			XDEVL_MODULE_ERROR(glewGetErrorString(err) << std::endl);
			return RET_FAILED;
		}
		// Get rid of an anoying GLEW bug.
		glGetError();

		// Only use if the extenstion is available. In some cases this is not
		// supported so we will use this as a hack.
		if(GLEW_ARB_debug_output) {
			glDebugMessageCallback((GLDEBUGPROC)(openGLDebugOutput), nullptr);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		}

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}


	void XdevLRAIGL::getOpenGLInfos() {
		std::string tmp;

		// The WGL_CONTEXT_CORE_PROFILE_BIT_ARB context profile doesn't support
		// glGetString(GL_EXTENSIONS) anymore. We have to do it in the propper way.
		if(m_profile == "core") {
			glGetIntegerv(GL_NUM_EXTENSIONS, &m_numExtensions);
			for(xdl_int idx = 0; idx < m_numExtensions; ++idx) {
				xdl_char* str_buffer = (xdl_char*)glGetStringi(GL_EXTENSIONS, idx);
				if(str_buffer != NULL)
					m_extensions.push_back((xdl_char*)glGetStringi(GL_EXTENSIONS, idx));
				else
					XDEVL_MODULE_INFO("GL_EXTENSIONS ERROR: " << idx << std::endl);


			}
		} else if(m_profile == "compatibility") {
			tmp = (xdl_char*)glGetString(GL_EXTENSIONS);
			m_numExtensions = xstd::tokenize(tmp, m_extensions, " ");
		}

		m_version 			= (xdl_char*)glGetString(GL_VERSION);
		m_vendor			=(xdl_char*)glGetString(GL_VENDOR);
	}

	const xdl_char* XdevLRAIGL::getVersion() {
		return m_version.c_str();
	}

	const xdl_char* XdevLRAIGL::getVendor() {
		return m_vendor.c_str();
	}

	xdl_int XdevLRAIGL::getNumExtensions() const {
		return m_numExtensions;
	}

	const xdl_char* XdevLRAIGL::getExtension(xdl_int idx) {
		const char* tmp = "";
		try {
			return m_extensions.at(idx).c_str();
		} catch(std::out_of_range& e) {
			XDEVL_MODULE_ERROR(e.what());
			return tmp;
		}
	}

	xdl_bool XdevLRAIGL::extensionSupported(const xdl_char* extensionName) {
		for(xdl_int a = 0; a < getNumExtensions(); ++a) {
			if(m_extensions[a] == extensionName)
				return true;
		}
		return false;
	}

	const xdl_char* XdevLRAIGL::getShaderVersion() {
		return (xdl_char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
	}

	XdevLFrameBuffer* XdevLRAIGL::getDefaultFrameBuffer() {
		return m_defaultFrameBuffer.get();
	}

	XdevLFrameBuffer* XdevLRAIGL::get2DFrameBuffer() {
		return m_2DFrameBuffer.get();
	}

	void XdevLRAIGL::setPointSize(xdl_float size) {
#if !defined(XDEVL_PLATFORM_ANDROID)
		glPointSize(size);
#endif
	}

	void XdevLRAIGL::setLineSize(xdl_float size) {

		glGetBooleanv(GL_LINE_SMOOTH, &m_antialiasedLinesEnabled);
		if(m_antialiasedLinesEnabled == GL_TRUE) {
			if(size < m_supportedAliasedLineWithRange[0] || size > m_supportedAliasedLineWithRange[0]) {
				XDEVL_MODULE_INFO("Line Width: " << size << " not supported by OpenGL driver." << std::endl);
				return;
			}
		} else {
			if(size < m_supportedSmoothedLineWithRange[0] || size > m_supportedSmoothedLineWithRange[0]) {
				XDEVL_MODULE_INFO("Line Width: " << size << " not supported by OpenGL driver." << std::endl);
				return;
			}
		}

		glLineWidth(size);

		GL_CHECK_ERROR()

	}

	void XdevLRAIGL::setActiveDepthTest(xdl_bool enableDepthTest) {
		enableDepthTest ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);

		GL_CHECK_ERROR()

	}

	void XdevLRAIGL::setActiveScissorTest(xdl_bool enableScissorTest) {
		enableScissorTest ? glEnable(GL_SCISSOR_TEST) : glDisable(GL_SCISSOR_TEST);

		GL_CHECK_ERROR()

	}

	void XdevLRAIGL::setScissor(xdl_float x, xdl_float y, xdl_float width, xdl_float height) {
		glScissor((GLint)x, (GLint)y, (GLsizei)width, (GLsizei)height);

		GL_CHECK_ERROR()

	}


	void XdevLRAIGL::setActiveBlendMode(xdl_bool enableBlendMode) {
		enableBlendMode ? glEnable(GL_BLEND) : glDisable(GL_BLEND);

		GL_CHECK_ERROR()

	}

	xdl_int XdevLRAIGL::setBlendFunc(XdevLBlendFunc func) {
		GLint f;
		switch(func) {
			case XDEVL_FUNC_ADD:
				f = GL_FUNC_ADD;
				break;
			case XDEVL_FUNC_SUBTRACT:
				f = GL_FUNC_SUBTRACT;
				break;
			case XDEVL_FUNC_REVERSE_SUBTRACT:
				f = GL_FUNC_REVERSE_SUBTRACT;
				break;
			default:
				f = GL_FUNC_ADD;
				break;
		}

		glBlendEquation(f);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}


	GLint XdevLBlendModeToGLBlendMode(XdevLBlendModes blendMode) {
		switch(blendMode) {
			case XDEVL_BLEND_ZERO:
				return GL_ZERO;
			case XDEVL_BLEND_ONE:
				return GL_ONE;
			case XDEVL_BLEND_SRC_COLOR:
				return GL_SRC_COLOR;
			case XDEVL_BLEND_ONE_MINUS_SRC_COLOR:
				return GL_ONE_MINUS_SRC_COLOR;
			case XDEVL_BLEND_DST_COLOR:
				return GL_DST_COLOR;
			case XDEVL_BLEND_ONE_MINUS_DST_COLOR:
				return GL_ONE_MINUS_DST_COLOR;
			case XDEVL_BLEND_SRC_ALPHA:
				return GL_SRC_ALPHA;
			case XDEVL_BLEND_ONE_MINUS_SRC_ALPHA:
				return GL_ONE_MINUS_SRC_ALPHA;
			case XDEVL_BLEND_DST_ALPHA:
				return GL_DST_ALPHA;
			case XDEVL_BLEND_ONE_MINUS_DST_ALPHA:
				return GL_ONE_MINUS_DST_ALPHA;
			case XDEVL_BLEND_CONSTANT_COLOR:
				return GL_CONSTANT_COLOR;
			case XDEVL_BLEND_ONE_MINUS_CONSTANT_COLOR:
				return GL_ONE_MINUS_CONSTANT_COLOR;
			case XDEVL_BLEND_CONSTANT_ALPHA:
				return GL_CONSTANT_ALPHA;
			case XDEVL_BLEND_ONE_MINUS_CONSTANT_ALPHA:
				return GL_ONE_MINUS_CONSTANT_ALPHA;
			case XDEVL_BLEND_SRC_ALPHA_SATURATE:
				return GL_SRC_ALPHA_SATURATE;
			case XDEVL_BLEND_SRC1_COLOR:
				return GL_SRC1_COLOR;
			case XDEVL_BLEND_ONE_MINUS_SRC1_COLOR:
				return GL_ONE_MINUS_SRC1_COLOR;
			case XDEVL_BLEND_SRC1_ALPHA:
				return GL_SRC1_ALPHA;
			case XDEVL_BLEND_ONE_MINUS_SRC1_ALPHA:
				return GL_ONE_MINUS_SRC1_ALPHA;
			default:
				return GL_ZERO;
		};
	}

	xdl_int XdevLRAIGL::setBlendMode(XdevLBlendModes src, XdevLBlendModes dst, XdevLBlendModes alphaSrc, XdevLBlendModes alphaDst) {
		GLint blend_src = XdevLBlendModeToGLBlendMode(src);
		GLint blend_dst = XdevLBlendModeToGLBlendMode(dst);
		GLint blendAlphaSrc = XdevLBlendModeToGLBlendMode(alphaSrc);
		GLint blendAlpahDst = XdevLBlendModeToGLBlendMode(alphaDst);
		glBlendFuncSeparate(blend_src, blend_dst, blendAlphaSrc, blendAlpahDst);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::clearColorTargets(xdl_float r, xdl_float g, xdl_float b, xdl_float a) {
		glClearColor(r,g, b, a);
		glClear(GL_COLOR_BUFFER_BIT);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::clearDepthTarget(xdl_float clear_value) {
		XDEVL_GL_CLEAR_DEPTH(clear_value);
		glClear(GL_DEPTH_BUFFER_BIT);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::setViewport(xdl_float x, xdl_float y, xdl_float width, xdl_float height) {
		m_viewPort.x = x;
		m_viewPort.y = y;
		m_viewPort.width = width;
		m_viewPort.height = height;

		glViewport((GLint)m_viewPort.x, (GLint)m_viewPort.y, (GLsizei)m_viewPort.width, (GLsizei)m_viewPort.height);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	void XdevLRAIGL::setViewport(const XdevLViewPort& viewPort) {
		m_viewPort = viewPort;
		glViewport((GLint)m_viewPort.x, (GLint)m_viewPort.y, (GLsizei)m_viewPort.width, (GLsizei)m_viewPort.height);

		GL_CHECK_ERROR()
	}

	XdevLViewPort XdevLRAIGL::getViewport() {
		return m_viewPort;
	}

	xdl_int XdevLRAIGL::setActiveRenderWindow(IPXdevLWindow window) {
		if(m_gl_context) {
			m_gl_context->makeCurrent(window);
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::swapBuffers() {
		if(m_useInternalBuffer) {
			glBindFramebuffer(GL_READ_FRAMEBUFFER, m_defaultFrameBuffer->id());
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

#if defined(XDEVL_PLATFORM_ANDROID)
			GLenum buffers[] = {GL_BACK};
			glDrawBuffers(sizeof(buffers), buffers);
#else
			glDrawBuffer(GL_BACK);
#endif
			glBlitFramebuffer(0, 0, m_defaultFrameBuffer->getWidth(), m_defaultFrameBuffer->getHeight(),
			                  0, 0, m_window->getWidth(), m_window->getHeight(),
			                  GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
		}

		if(m_use2DFrameBuffer) {
			// Activate the windows default framebuffer (context one).
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			// Render the stuff.
			renderFrameBufferPlane();

//			glBindFramebuffer(GL_READ_FRAMEBUFFER, m_2DFrameBuffer->id());
//			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

//			glDrawBuffer(GL_BACK);
//			glBlitFramebuffer(0, 0, m_2DFrameBuffer->getWidth(), m_2DFrameBuffer->getHeight(),
//			                  0, 0, m_window->getWidth(), m_window->getHeight(),
//			                  GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
		}

		if(m_gl_context) {
			m_gl_context->swapBuffers();
		}

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	IPXdevLVertexDeclaration XdevLRAIGL::createVertexDeclaration() {
		auto tmp = std::shared_ptr<XdevLVertexDeclaration>(new XdevLVertexDeclaration());
		return tmp;
	}

	IPXdevLVertexShader XdevLRAIGL::createVertexShader() {
		auto tmp = std::shared_ptr<XdevLOpenGLVertexShaderImpl>(new XdevLOpenGLVertexShaderImpl());
		return tmp;
	}

	IPXdevLFragmentShader XdevLRAIGL::createFragmentShader() {
		auto tmp = std::shared_ptr<XdevLOpenGLFragmentShaderImpl>(new XdevLOpenGLFragmentShaderImpl());
		return tmp;
	}

	IPXdevLGeometryShader XdevLRAIGL::createGeometryShader() {
		auto tmp = std::shared_ptr<XdevLOpenGLGeometryShaderImpl>(new XdevLOpenGLGeometryShaderImpl());
		return tmp;
	}

	IPXdevLTessellationControlShader  XdevLRAIGL::createTessellationControlShader() {
		auto tmp = std::shared_ptr<XdevLOpenGLTessellationControlShaderImpl>(new XdevLOpenGLTessellationControlShaderImpl());
		return tmp;
	}

	IPXdevLTessellationEvaluationShader  XdevLRAIGL::createTessellationEvaluatioinShader() {
		auto tmp = std::shared_ptr<XdevLOpenGLTessellationEvaluationShaderImpl>(new XdevLOpenGLTessellationEvaluationShaderImpl());
		return tmp;
	}

	IPXdevLShaderProgram XdevLRAIGL::createShaderProgram() {
		auto tmp = std::shared_ptr<XdevLOpenGLProgramImpl>(new XdevLOpenGLProgramImpl(this));
		return tmp;
	}

	IPXdevLTextureSampler XdevLRAIGL::createTextureSampler() {
		auto tmp = std::shared_ptr<XdevLTextureSamplerImpl>(new XdevLTextureSamplerImpl());
		return tmp;
	}

	IPXdevLTexture XdevLRAIGL::createTexture() {
		auto tmp = std::shared_ptr<XdevLTextureImpl>(new XdevLTextureImpl());
		return tmp;
	}

	IPXdevLTextureCube XdevLRAIGL::createTextureCube() {
		auto tmp = std::shared_ptr<XdevLTextureCubeImpl>(new XdevLTextureCubeImpl());
		return tmp;
	}

	IPXdevLTexture3D XdevLRAIGL::createTexture3D() {
		auto tmp = std::shared_ptr<XdevLTexture3DImpl>(new XdevLTexture3DImpl());
		return tmp;
	}

	IPXdevLFrameBuffer XdevLRAIGL::createFrameBuffer() {
		auto tmp = std::shared_ptr<XdevLFrameBufferImpl>(new XdevLFrameBufferImpl(this));
		return tmp;
	}

	IPXdevLVertexArray XdevLRAIGL::createVertexArray() {
		return std::shared_ptr<XdevLVertexArrayImpl>(new XdevLVertexArrayImpl(this));
	}

	IPXdevLVertexBuffer XdevLRAIGL::createVertexBuffer() {
		auto tmp = std::shared_ptr<XdevLVertexBufferImpl>(new XdevLVertexBufferImpl());
		return tmp;
	}

	IPXdevLIndexBuffer XdevLRAIGL::createIndexBuffer() {
		auto tmp = std::shared_ptr<XdevLIndexBufferImpl>(new XdevLIndexBufferImpl());
		return tmp;
	}

	IPXdevLUniformBuffer XdevLRAIGL::createUniformBuffer() {
		auto tmp = std::shared_ptr<XdevLUniformBufferImpl>(new XdevLUniformBufferImpl());
		return tmp;
	}

	xdl_int XdevLRAIGL::setActiveTextureSampler(XdevLTextureStage textureStage, IPXdevLTextureSampler textureSampler) {
		XDEVL_ASSERT(nullptr != textureSampler, "Invalid IPXdevLTextureSampler specified.");

		textureSampler->activate(textureStage);
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::setActiveFrameBuffer(IPXdevLFrameBuffer frambuffer) {
		assert(frambuffer && "XdevLOpenGLImpl::setActiveFrameBuffer: No valid Framebuffer specified.");
		m_activeFrameBuffer = frambuffer;
		return RET_SUCCESS;
	}


	xdl_int XdevLRAIGL::setActiveVertexArray(IPXdevLVertexArray vertexArray) {
		assert(vertexArray && "XdevLOpenGLImpl::setActiveVertexArray: No valid Vertex Array specified.");
		m_activeVertexArray = vertexArray;
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::setActiveShaderProgram(IPXdevLShaderProgram shaderProgram) {
		assert(shaderProgram && "XdevLOpenGLImpl::setActiveShaderProgram: No valid Shader Program specified.");
		m_activeShaderProgram = shaderProgram;
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::drawVertexArray(XdevLPrimitiveType primitiveType, xdl_uint numberOfElements, xdl_uint64 indexOffset) {
		glBindVertexArray(m_activeVertexArray->id());
		glUseProgram(m_activeShaderProgram->id());

		GLenum mode = wrapToGLPrimitive(primitiveType);
		GLsizei count = numberOfElements;

		if(m_activeVertexArray->getIndexBuffer()) {
			GLenum type = wrapToGLElementType(m_activeVertexArray->getIndexBuffer()->getElementType());
			const GLvoid * indices = (const GLvoid*)(m_activeVertexArray->getIndexBuffer()->getSizeOfElementType() * indexOffset);
			glDrawElements(mode, count, type, indices);
		} else {
			glDrawArrays(mode, indexOffset, count);
		}

		glBindVertexArray(0);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}


	xdl_int XdevLRAIGL::drawInstancedVertexArray(XdevLPrimitiveType primitiveType, xdl_uint numberOfElements, xdl_uint numberOfInstances) {

		glBindVertexArray(m_activeVertexArray->id());
		glUseProgram(m_activeShaderProgram->id());

		GLenum mode = wrapToGLPrimitive(primitiveType);
		GLsizei count = numberOfElements;
		GLsizei primcount = numberOfInstances;

		if(m_activeVertexArray->getIndexBuffer()) {
			GLenum type = wrapToGLElementType(m_activeVertexArray->getIndexBuffer()->getElementType());
			glDrawElementsInstanced(mode, count, type, nullptr, primcount);
		} else {
			glDrawArraysInstanced(mode, 0, count, primcount);
		}

		glBindVertexArray(0);

		GL_CHECK_ERROR()

		return RET_SUCCESS;

	}

	IPXdevLOpenGLContext XdevLRAIGL::getOpenGLContext() {
		return m_gl_context;
	}

	xdl_int XdevLRAIGL::initExtensions() {
//		glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)m_gl_context->getProcAddress("glGenVertexArrays");
//		glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC)m_gl_context->getProcAddress("glDeleteVertexArrays");
//		glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)m_gl_context->getProcAddress("glBindVertexArray");
//		glIsVertexArray = (PFNGLISVERTEXARRAYPROC)m_gl_context->getProcAddress("glIsVertexArray");
//
//		glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYARBPROC)m_gl_context->getProcAddress("glEnableVertexAttribArray");
//		glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERARBPROC)m_gl_context->getProcAddress("glVertexAttribPointer");
//
//
//
//		glGenBuffers = (PFNGLGENBUFFERSARBPROC)m_gl_context->getProcAddress("glGenBuffers");
//		glDeleteBuffers = (PFNGLDELETEBUFFERSPROC)m_gl_context->getProcAddress("glDeleteBuffers");
//		glBindBuffer = (PFNGLBINDBUFFERARBPROC)m_gl_context->getProcAddress("glBindBuffer");
//		glBufferData = (PFNGLBUFFERDATAPROC)m_gl_context->getProcAddress("glBindBuffer");
//
//		glGetShaderiv = (PFNGLGETSHADERIVPROC)m_gl_context->getProcAddress("glGetShaderiv");
//		glCreateProgram = (PFNGLCREATEPROGRAMPROC)m_gl_context->getProcAddress("glCreateProgram");
//		glLinkProgram = (PFNGLLINKPROGRAMARBPROC)m_gl_context->getProcAddress("glLinkProgram");
//		glUseProgram = (PFNGLUSEPROGRAMPROC)m_gl_context->getProcAddress("glUseProgram");
//
//		glBindProgramARB = (PFNGLBINDPROGRAMARBPROC)m_gl_context->getProcAddress("glBindProgram");
//		glGetProgramiv = (PFNGLGETPROGRAMIVPROC)m_gl_context->getProcAddress("glGetProgramiv");
//		glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)m_gl_context->getProcAddress("glGetUniformLocation");
//
//		glUniformMatrix2fv = (PFNGLUNIFORMMATRIX2FVARBPROC)m_gl_context->getProcAddress("glUniformMatrix2fv");
//		glUniformMatrix3fv = (PFNGLUNIFORMMATRIX3FVARBPROC)m_gl_context->getProcAddress("glUniformMatrix3fv");
//		glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVARBPROC)m_gl_context->getProcAddress("glUniformMatrix4fv");
//
//		glCreateShader = (PFNGLCREATESHADERPROC)m_gl_context->getProcAddress("glCreateShader");
//		glDeleteShader = (PFNGLDELETESHADERPROC)m_gl_context->getProcAddress("glDeleteShader");
//		glCompileShader = (PFNGLCOMPILESHADERARBPROC)m_gl_context->getProcAddress("glCompileShader");
//		glShaderSource = (PFNGLSHADERSOURCEPROC)m_gl_context->getProcAddress("glShaderSource");
//		glAttachShader = (PFNGLATTACHSHADERPROC)m_gl_context->getProcAddress("glAttachShader");
//		glShaderSource = (PFNGLSHADERSOURCEPROC)m_gl_context->getProcAddress("glShaderSource");
//
//		glDrawArraysEXT = (PFNGLDRAWARRAYSEXTPROC)m_gl_context->getProcAddress("glDrawArraysEXT");

		return RET_SUCCESS;
	}

	void XdevLRAIGL::shaderLog(xdl_uint shaderId) {
		// Get the length of the message.
		GLint Len;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &Len);

		GLsizei ActualLen;
		GLchar *Log = new GLchar[Len];
		glGetShaderInfoLog(shaderId, Len, &ActualLen, Log);
		XDEVL_MODULE_ERROR(Log << "\n");
		delete [] Log;
	}

	xdl_int XdevLRAIGL::createScreenVertexArray() {

		xdl_float screen_vertex [] = {
			-1.0f, -1.0f,
			-1.0f, 1.0f,
			1.0f, 1.0f,

			1.0f, 1.0f,
			1.0f, -1.0f,
			-1.0f, -1.0f
		};

		xdl_float screen_uv [] = {
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,

			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f
		};

		m_vertexDeclaration = createVertexDeclaration();
		m_vertexDeclaration->add(2, XdevLBufferElementTypes::FLOAT, 0);
		m_vertexDeclaration->add(2, XdevLBufferElementTypes::FLOAT, 9);

		std::vector<xdl_uint8*> list;
		list.push_back((xdl_uint8*)screen_vertex);
		list.push_back((xdl_uint8*)screen_uv);

		m_frameBufferArray = createVertexArray();
		m_frameBufferArray->init((xdl_uint8)list.size(), list.data(), 6, m_vertexDeclaration);

		// Create the shader program.
		m_frameBufferShaderProgram = createShaderProgram();
		m_frameBufferVertexShader = createVertexShader();
		m_frameBufferFragmentShader = createFragmentShader();
		m_frameBufferVertexShader->addShaderCode(vertex_shader_source.c_str());
		m_frameBufferFragmentShader->addShaderCode(fragment_shader_source.c_str());
		m_frameBufferVertexShader->compile();
		m_frameBufferFragmentShader->compile();

		m_frameBufferShaderProgram->attach(m_frameBufferVertexShader);
		m_frameBufferShaderProgram->attach(m_frameBufferFragmentShader);
		m_frameBufferShaderProgram->link();

		m_frameBufferTextureShaderId = m_frameBufferShaderProgram->getUniformLocation("tex");

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLRAIGL::renderFrameBufferPlane() {

		setActiveBlendMode(xdl_true);
		setBlendMode(XDEVL_BLEND_SRC_ALPHA, XDEVL_BLEND_ONE_MINUS_SRC_ALPHA);
		setActiveDepthTest(xdl_false);

		m_frameBufferShaderProgram->activate();
		m_frameBufferShaderProgram->setUniformi(m_frameBufferTextureShaderId, 0);
		m_2DFrameBuffer->getTexture(0)->activate(XdevLTextureStage::STAGE0);
		m_frameBufferShaderProgram->deactivate();

//		auto vd = m_vertexDeclaration;
//
//		glBindVertexArray(m_frameBufferArray->id());
//		glUseProgram(m_frameBufferShaderProgram->id());
//
//		for(xdl_uint idx = 0; idx < vd->getNumber(); idx++) {
//			GLuint shader_attrib = vd->get(idx)->shaderAttribute;
//			glEnableVertexAttribArray(shader_attrib);
//		}
//
//		glDrawArrays(XDEVL_PRIMITIVE_TRIANGLES, 0, 6);
//
//		for(xdl_uint idx = 0; idx < vd->getNumber(); idx++) {
//			glDisableVertexAttribArray(vd->get(idx)->shaderAttribute);
//		}


		setActiveVertexArray(m_frameBufferArray);
		setActiveShaderProgram(m_frameBufferShaderProgram);

		drawVertexArray(XDEVL_PRIMITIVE_TRIANGLES, 6);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}
}
