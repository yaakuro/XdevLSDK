/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BUFFER_BASE_IMPL_H
#define XDEVL_BUFFER_BASE_IMPL_H

#include "XdevLOpenGLUtils.h"

namespace xdl {

	template<typename T, GLenum TARGET>
	class XdevLBufferBaseImpl : public T {
		public:
			XdevLBufferBaseImpl()
				: m_id(0)
				, m_size(0)
				, m_locked(xdl_false)
				, m_mapped(xdl_false)
			{}

			virtual ~XdevLBufferBaseImpl() {
				if(0 != m_id) {
					glDeleteBuffers(1, &m_id);
				}
			}

			using T::init;

			xdl_int init() override final {
				glGenBuffers(1, &m_id);
				if(m_id == 0) {
					return RET_FAILED;
				}

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int init(xdl_uint size) override final {

				GLint64 blockSize = 0;
				glGetInteger64v(GL_MAX_UNIFORM_BLOCK_SIZE, &blockSize);
				XDEVL_ASSERT(size < blockSize, "The size specified is not supported by your OpenGL hardware.");

				glGenBuffers(1, &m_id);
				if(m_id == 0) {
					return RET_FAILED;
				}
				glBindBuffer(TARGET, m_id);
				if(!glIsBuffer(m_id)) {
					return RET_FAILED;
				}
				// Created and initialize the data store of the VBO.
				glBufferData(TARGET, size, nullptr, GL_STATIC_DRAW);

				// Unbind to prevent override in other OpenGL code fragments.
				glBindBuffer(TARGET, 0);
				m_size = size;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int init(xdl_uint8* src, xdl_uint size) override final {

				// Create and empty VBO name and bind it.
				glGenBuffers(1, &m_id);
				glBindBuffer(TARGET, m_id);
				if(!glIsBuffer(m_id)) {
					XDEVL_MODULEX_INFO(XdevLBufferBase, "glIsBuffer failed. Not a buffer.\n");
					return RET_FAILED;
				}

				// Created and initialize the data store of the VBO.
				glBufferData(TARGET, size, src, GL_STATIC_DRAW);

				// Unbind to prevent override in other OpenGL code fragments.
				glBindBuffer(TARGET, 0);
				m_size = size;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int lock() override final {
				XDEVL_ASSERT(m_id != 0, "XdevLBufferBase: You must initialze the uniform buffer.");
				XDEVL_ASSERT(xdl_false == m_locked, "XdevLBufferBase::lock: Was locked already.");

				GL_CHECK_VAO_BOUND("A VAO is bound. Locking this VBO will cause to be bound to the VAO.");

				glBindBuffer(TARGET, m_id);
				if(!glIsBuffer(m_id)) {
					return RET_FAILED;
				}

				m_locked = xdl_true;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int unlock() override final {
				XDEVL_ASSERT(m_id != 0, "XdevLBufferBase: You must initialze the uniform buffer.");
				XDEVL_ASSERT(xdl_false != m_locked,"XdevLBufferBase::unlock: Was not locked.");

				GL_CHECK_VAO_BOUND("A VAO is bound. Unlocking this VBO will cause to be unbound from the VAO.");

				glBindBuffer(TARGET, 0);
				xdl_int ret = RET_SUCCESS;
				if(glIsBuffer(m_id)) {
					ret = RET_FAILED;
				}

				m_locked = xdl_false;

				GL_CHECK_ERROR()

				return ret;
			}

			xdl_uint8* map(XdevLBufferAccessType bufferAccessType) override final {
				XDEVL_ASSERT(xdl_true == m_locked, "XdevLBufferBase::Buffer was not locked.");

				xdl_uint8* memory = (xdl_uint8*)glMapBuffer(TARGET, static_cast<GLenum>(bufferAccessType));
				if(nullptr == memory) {
					XDEVL_MODULEX_ERROR(XdevLBufferBase, "glMapBuffer failed.\n");
					return nullptr;
				}

				m_mapped = xdl_true;

				GL_CHECK_ERROR()

				return memory;
			}

			xdl_int unmap() override final {
				XDEVL_ASSERT(xdl_false != m_mapped, "XdevLBufferBase::Buffer was not mapped.");

				if(glUnmapBuffer(TARGET) != GL_TRUE) {
					return RET_FAILED;
				}
				m_mapped = xdl_false;

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_int upload(xdl_uint8* src, xdl_uint size, xdl_uint64 offset = 0) override final {
				XDEVL_ASSERT(xdl_true == m_locked, "XdevLBufferBase::Buffer was not locked.");

				// What do we here? Only glBufferData is creating the buffer. Only
				// recreate a new one if the specified size is bigger than this buffer
				// can hold.
				// TODO GL_DYNAMIC_DRAW is it neccessary?
				if(m_size < size) {
					m_size = size;
					XDEVL_MODULEX_INFO(XdevLUniformBuffer, "Resizing Vertex Buffer to: " << m_size << std::endl);
					glBufferData(TARGET, size, src, GL_DYNAMIC_DRAW);
				} else {
					XDEVL_ASSERT((offset + size) <= m_size, "XdevLBufferBase::upload size and offset combined exceeds the size of the buffer.");
					glBufferSubData(TARGET, (GLintptr)offset, size, src);
				}
				auto result = glGetError();
				if(GL_NO_ERROR != result) {
					XDEVL_MODULEX_ERROR(XdevLUniformBuffer, "glBufferData/glBufferSubData failed: " << glGetErrorAsString(result) << std::endl);
					return RET_FAILED;
				}

				GL_CHECK_ERROR()

				return RET_SUCCESS;
			}

			xdl_uint id() override final {
				return m_id;
			}

			xdl_uint getSize() override final {
				return m_size;
			}

			GLuint m_id;
			xdl_uint m_size;
			xdl_bool m_locked;
			xdl_bool m_mapped;
	};


}


#endif
