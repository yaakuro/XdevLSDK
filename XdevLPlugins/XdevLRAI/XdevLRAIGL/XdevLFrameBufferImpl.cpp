/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLFrameBufferImpl.h"
#include "XdevLOpenGLUtils.h"
#include <XdevLWindow/XdevLWindow.h>

namespace xdl {

	XdevLFrameBufferImpl::XdevLFrameBufferImpl(XdevLRAIGL* rai)
		: m_rai(rai),
		  m_id(0),
		  m_width(0),
		  m_height(0),
		  m_size(0),
		  m_inUse(xdl_false),
		  m_useMultisamples(xdl_false),
		  m_numSamples(4),
		  m_useRenderBuffers(xdl_false) {
		m_colorTargetTextures.reserve(4);
		m_colorTargetTextures.resize(4);
		m_extensions = rai->getOpenGLContext()->getExtensions();
	}

	XdevLFrameBufferImpl::~XdevLFrameBufferImpl() {
		if(glIsFramebuffer(m_id)) {
			glDeleteFramebuffers(1, &m_id);
		}
	}

	xdl_int XdevLFrameBufferImpl::init(xdl_uint width, xdl_uint height) {
		m_width 	= width;
		m_height 	= height;

		// Create the framebuffer object and make it current.
		glGenFramebuffers(1, &m_id);
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);
		if(!glIsFramebuffer(m_id)) {
			XDEVL_MODULEX_ERROR(XdevLFrameBufferImpl, "glBindFramebuffer failed.\n");
			return RET_FAILED;
		}

		// Switch off all color targets.
		std::vector<GLint> colorTargets = {GL_NONE, GL_NONE, GL_NONE, GL_NONE };
		glDrawBuffers(static_cast<GLsizei>(colorTargets.size()), (const GLenum*)colorTargets.data());

		// Unbind this framebuffer object to make sure that nothing will be done accidentally.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// Save previous viewport.
		glGetIntegerv(GL_VIEWPORT, m_previousViewport);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::addColorTarget(xdl_uint target_index, XdevLFrameBufferColorFormat format) {

		// Bind the framebuffer.
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);
		if(!glIsFramebuffer(m_id)) {
			XDEVL_MODULEX_ERROR(XdevLFrameBufferImpl, "glBindFramebuffer failed.\n");
			return RET_FAILED;
		}

		// Create the texture that is going to be used a color target.
		GLuint id;
		glGenTextures(1, &id);

		GLint textureTarget = m_useMultisamples ? GL_TEXTURE_2D_MULTISAMPLE :GL_TEXTURE_2D;
		GLint internalFormat = static_cast<GLint>(format);

		glBindTexture(textureTarget, id);

		// Use texture storage if possible.
		if(m_extensions->supports(XdevLOpenGLExtensionsImpl::ARB_texture_storage)) {
			if(m_useMultisamples) {
				glTexStorage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, m_numSamples, internalFormat, m_width, m_height, GL_TRUE);
			} else {
				glTexStorage2D(GL_TEXTURE_2D, 1, internalFormat, m_width, m_height);
			}
		} else {
			if(m_useMultisamples) {
				glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, m_numSamples, internalFormat, m_width, m_height, GL_TRUE);
			} else {
				glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, m_width, m_height, 0, GL_RED,  GL_UNSIGNED_BYTE, nullptr);
			}
		}

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + target_index, textureTarget, id, 0);

		// Check if the framebuffer was build correct.
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(GL_FRAMEBUFFER_COMPLETE != status) {
			framebufferErrorAsString(status);
			return RET_FAILED;
		}

		// Unbind framebuffer and texture object to make sure that nothing will be done accidentally.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(textureTarget, 0);

		m_colorTargetTextures[target_index] = std::shared_ptr<XdevLTextureImpl>(new XdevLTextureImpl(id, m_width, m_height));

		m_size++;

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int  XdevLFrameBufferImpl::addColorTarget(xdl_uint target_index, IPXdevLTexture texture) {

		// Bind the framebuffer.
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);
		if(!glIsFramebuffer(m_id)) {
			XDEVL_MODULEX_ERROR(XdevLFrameBufferImpl, "glBindFramebuffer failed.\n");
			return RET_FAILED;
		}

		GLint textureTarget = m_useMultisamples ? GL_TEXTURE_2D_MULTISAMPLE :GL_TEXTURE_2D;

		// We use glFramebufferTexture2D wich means we don't have to bind the texture object itself.
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + target_index, textureTarget, texture->id(), 0);

		// Check if the framebuffer was build correct.
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(GL_FRAMEBUFFER_COMPLETE != status) {
			framebufferErrorAsString(status);
			return RET_FAILED;
		}

		// Unbind framebuffer object to make sure that nothing will be done accidentally.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		m_colorTargetTextures[target_index] = texture;

		m_size++;

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int  XdevLFrameBufferImpl::addColorTarget(xdl_uint target_index, IPXdevLTextureCube textureCube) {

		// Bind the framebuffer.
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);

		// We use glFramebufferTexture2D wich means we don't have to bind the texture object itself.
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + target_index, GL_TEXTURE_CUBE_MAP_POSITIVE_X, textureCube->id(), 0);

		// Check if the framebuffer was build correct.
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(GL_FRAMEBUFFER_COMPLETE != status) {
			framebufferErrorAsString(status);
			return RET_FAILED;
		}

		// Unbind framebuffer object to make sure that nothing will be done accidentally.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		m_textureCube = textureCube;

		m_size++;

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::addDepthTarget(XdevLFrameBufferDepthFormat depthFormat) {

		// Bind the framebuffer.
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);
		if(!glIsFramebuffer(m_id)) {
			XDEVL_MODULEX_ERROR(XdevLFrameBufferImpl, "glBindFramebuffer failed.\n");
			return RET_FAILED;
		}

		GLint textureTarget = m_useMultisamples ? GL_TEXTURE_2D_MULTISAMPLE :GL_TEXTURE_2D;
		GLint internalFormat = static_cast<GLint>(depthFormat);
		GLuint id;
		if(m_useRenderBuffers) {
			glGenRenderbuffers(1, &id);
			glBindRenderbuffer(GL_RENDERBUFFER, id);
			if(m_useMultisamples) {
				glRenderbufferStorageMultisample(GL_RENDERBUFFER, m_numSamples, internalFormat, m_width, m_height);
			} else {
				glRenderbufferStorage(GL_RENDERBUFFER, internalFormat, m_width, m_height);
			}
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, id);
		} else {
			// Create depth texture.
			glGenTextures(1, &id);
			glBindTexture(textureTarget, id);

			if(m_useMultisamples) {
				glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, m_numSamples, internalFormat, m_width, m_height, GL_TRUE);
			} else {
				glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, m_width, m_height, 0, GL_DEPTH_COMPONENT,  GL_UNSIGNED_BYTE, nullptr);
			}
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, textureTarget, id, 0);
		}

		// Check if the framebuffer was build correct.
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(GL_FRAMEBUFFER_COMPLETE != status) {
			framebufferErrorAsString(status);
			return RET_FAILED;
		}

		// Unbind framebuffer and texture object to make sure that nothing will be done accidentally.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(textureTarget, 0);

		m_depthTexture = std::shared_ptr<XdevLTextureImpl>(new XdevLTextureImpl(id, m_width, m_height, m_useRenderBuffers));

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::addDepthTarget(IPXdevLTexture texture) {

		// Bind the framebuffer.
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture->id(), 0);

		// Check if the framebuffer was build correct.
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(GL_FRAMEBUFFER_COMPLETE != status) {
			framebufferErrorAsString(status);
			return RET_FAILED;
		}

		// Unbind framebuffer object to make sure that nothing will be done accidentally.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		m_depthTexture = std::shared_ptr<XdevLTextureImpl>(new XdevLTextureImpl(texture->id(), texture->getWidth(), texture->getHeight()));

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::addDepthStencilTarget(XdevLFrameBufferDepthStencilFormat depthStencilFormat) {

		// Bind the framebuffer.
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);
		GLint textureTarget = m_useMultisamples ? GL_TEXTURE_2D_MULTISAMPLE :GL_TEXTURE_2D;
		GLint internalFormat = static_cast<GLint>(depthStencilFormat);
		GLuint id;
		if(m_useRenderBuffers) {
			glGenRenderbuffers(1, &id);
			glBindRenderbuffer(GL_RENDERBUFFER, id);
			if(m_useMultisamples) {
				glRenderbufferStorageMultisample(GL_RENDERBUFFER, m_numSamples, internalFormat, m_width, m_height);
			} else {
				glRenderbufferStorage(GL_RENDERBUFFER, internalFormat, m_width, m_height);
			}
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, id);
		} else {

			// Create the depth/stencil texture.
			glGenTextures(1, &id);
			glBindTexture(textureTarget, id);
			if(m_useMultisamples) {
				glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, m_numSamples, internalFormat, m_width, m_height, GL_TRUE);
			} else {
				glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, m_width, m_height, 0, GL_DEPTH_COMPONENT,  GL_UNSIGNED_BYTE, nullptr);
			}

			// Attach them to the framebuffer.
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 		id, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, 	id, 0);
		}

		// Check if the framebuffer was build correct.
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(GL_FRAMEBUFFER_COMPLETE != status) {
			framebufferErrorAsString(status);
			return RET_FAILED;
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(textureTarget, 0);

		m_depthTexture = std::shared_ptr<XdevLTextureImpl>(new XdevLTextureImpl(id, m_width, m_height, m_useRenderBuffers));

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::activateColorTargets(xdl_uint numberOfTargets, XdevLFrameBufferColorTargets* targetList) {
		assert(m_inUse && "XdevLFrameBufferImpl::draw: Frame Buffer is not activated.");
		glDrawBuffers(numberOfTargets, reinterpret_cast<const GLenum*>(targetList));

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int  XdevLFrameBufferImpl::setActiveDepthTest(xdl_bool state) {
		if(state == xdl_true) {
			glEnable(GL_DEPTH_TEST);
		} else {
			glDisable(GL_DEPTH_TEST);
		}

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::activateColorTargetCubePosition(xdl_uint target_index, XdevLCubemapPosition cubemapPosition) {
		assert(m_inUse && "XdevLFrameBufferImpl::draw: Framebuffer not activated.");
		assert(m_textureCube && "XdevLFrameBufferImpl::activateColorTargetCube: Cube Texture not added.");

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + target_index, static_cast<GLenum>(cubemapPosition), m_textureCube->id(), 0);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int  XdevLFrameBufferImpl::clearColorTargets(xdl_float r, xdl_float g, xdl_float b, xdl_float a) {
		glClearColor(r,g,b,a);
		glClear(GL_COLOR_BUFFER_BIT);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int  XdevLFrameBufferImpl::clearDepthTarget(xdl_float clear_value) {
		XDEVL_GL_CLEAR_DEPTH(clear_value);
		glClear(GL_DEPTH_BUFFER_BIT);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::activateStencilTarget(xdl_bool state) {
		if(state == xdl_true) {
			glEnable(GL_STENCIL_TEST);
		} else {
			glDisable(GL_STENCIL_TEST);
		}

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::clearStencilTarget(xdl_int clear_value) {
		glClearStencil(clear_value);
		glClear(GL_STENCIL_BUFFER_BIT);

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::activate()  {
		if(m_useMultisamples) {
			glEnable(GL_MULTISAMPLE_ARB);
		}
//		glEnable(GL_FRAMEBUFFER_SRGB);

		// Save previous viewport.
		glGetIntegerv(GL_VIEWPORT, m_previousViewport);

		// Activate for read and write.
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);
		glViewport(0, 0, m_width, m_height);

		m_inUse = xdl_true;

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_int XdevLFrameBufferImpl::deactivate()  {
		if(m_useMultisamples) {
			glDisable(GL_MULTISAMPLE_ARB);
		}

		glViewport(m_previousViewport[0], m_previousViewport[1], m_previousViewport[2], m_previousViewport[3]);

		std::vector<GLint> colorTargets = {GL_NONE, GL_NONE, GL_NONE, GL_NONE };
		glDrawBuffers(static_cast<GLsizei>(colorTargets.size()), (const GLenum*)colorTargets.data());

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		m_inUse = xdl_false;

		GL_CHECK_ERROR()

		return RET_SUCCESS;
	}

	xdl_uint XdevLFrameBufferImpl::getWidth() {
		return m_width;
	}

	xdl_uint XdevLFrameBufferImpl::getHeight() {
		return m_height;
	}

	xdl_uint XdevLFrameBufferImpl::getNumColorTextures() {
		return m_size;
	}

	xdl_uint XdevLFrameBufferImpl::id() {
		return m_id;
	}

	IPXdevLTexture XdevLFrameBufferImpl::getTexture(xdl_uint idx) {
		auto texture = m_colorTargetTextures.at(idx);
		return texture;
	}

	IPXdevLTexture XdevLFrameBufferImpl::getDepthTexture() {
		return m_depthTexture;
	}

	void XdevLFrameBufferImpl::blit(XdevLFrameBuffer* framebuffer, xdl_uint32 numberOfRenderTargets, XdevLFrameBufferRenderTarget renderTargets[]) {

		// We are going to read data from the following framebuffer. ..
		glBindFramebuffer(GL_READ_FRAMEBUFFER, m_id);
		// When reading from the source framebuffer, use target ...
		glReadBuffer(GL_COLOR_ATTACHMENT0);

		GLuint destinationWidth = m_rai->getWindow()->getWidth();
		GLuint destinationHeight = m_rai->getWindow()->getHeight();

		if(nullptr != framebuffer) {
			// Set the destination framebuffer size.
			destinationWidth =  framebuffer->getWidth();
			destinationHeight = framebuffer->getHeight();

			// We are going to write into the following framebuffer.
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer->id());

			// When drawing to the destination framebuffer, use target ...
			glDrawBuffer(GL_COLOR_ATTACHMENT0);
		} else {

			// We are going to write into the main framebuffer of the screen.
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

			// When drawing to the destination framebuffer, use the backbuffer.
			glDrawBuffer(GL_BACK);
		}

		GLint glTarget = GL_COLOR_BUFFER_BIT;
		for(xdl_uint32 idx = 0; idx < numberOfRenderTargets; idx++) {
			auto renderTarget = renderTargets[idx];
			if(renderTarget == XdevLFrameBufferRenderTarget::COLOR) {
				glTarget |= GL_COLOR_BUFFER_BIT;
			} else if(renderTarget == XdevLFrameBufferRenderTarget::DEPTH) {
				glTarget |= GL_DEPTH_BUFFER_BIT;
			} else  if(renderTarget == XdevLFrameBufferRenderTarget::STENCIL) {
				glTarget |= GL_STENCIL_BUFFER_BIT;
			}
		}

		glBlitFramebuffer(0, 0, m_width, m_height, 0, 0, destinationWidth, destinationHeight, glTarget, GL_LINEAR);
		GLint ret = glGetError();
		if(ret != GL_NO_ERROR) {
			XDEVL_MODULEX_ERROR(XdevLFrameBufferImpl, "OpenGL error: " << glGetErrorAsString(ret) << std::endl);
		}

		GL_CHECK_ERROR()
	}

	void XdevLFrameBufferImpl::framebufferErrorAsString(GLenum status) {
		switch(status) {
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: {
				XDEVL_MODULEX_ERROR(XdevLFrameBufferImpl, "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: Not all framebuffer attachment points are framebuffer attachment complete" << std::endl);
			}
			break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: {
				XDEVL_MODULEX_ERROR(XdevLFrameBufferImpl, "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: No images are attached to the framebuffer." << std::endl);
			}
			break;
			case GL_FRAMEBUFFER_UNSUPPORTED: {
				XDEVL_MODULEX_ERROR(XdevLFrameBufferImpl, "GL_FRAMEBUFFER_UNSUPPORTED: The combination of internal formats of the attached images violates an implementation-dependent set of restrictions." << std::endl);
			}
			break;
		}
	}
}
