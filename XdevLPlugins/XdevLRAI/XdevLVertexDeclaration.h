/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VERTEX_DECLARATION_H
#define XDEVL_VERTEX_DECLARATION_H

#include <XdevLTypes.h>
#include <XdevLRAI/XdevLStreamBuffer.h>
#include <vector>
#include <cassert>

namespace xdl {


	struct XdevLVertexDeclarationItem {
		XdevLVertexDeclarationItem(xdl_uint n, XdevLBufferElementTypes s, xdl_uint sa, xdl_uint etsib, xdl_bool norm=xdl_false) : elementType(s),
			numberOfComponents(n), shaderAttribute(sa), elementTypeSizeInBytes(etsib), normalized(norm) {}
		/// The type of the item.
		XdevLBufferElementTypes	elementType;
		xdl_uint numberOfComponents;
		xdl_uint shaderAttribute;
		xdl_uint elementTypeSizeInBytes;
		xdl_bool normalized;
	};

	class XdevLVertexDeclaration {
		public:
			~XdevLVertexDeclaration() {
				for(auto& vertexDeclerationitem : m_list) {
					delete vertexDeclerationitem;
				}
			}

			void add(xdl_uint number, XdevLBufferElementTypes size, xdl_uint shader_attribute, xdl_bool normalized = xdl_false) {
				m_list.push_back(new XdevLVertexDeclarationItem(number, size, shader_attribute, bufferElementTypeSizeInBytes(size), normalized));
			}

			/// Returns the number of vertex elements within this declaration.
			xdl_uint getNumber() {
				return static_cast<xdl_uint>(m_list.size());
			}

			XdevLVertexDeclarationItem* get(xdl_uint idx) {
				return m_list[idx];
			}

			xdl_uint vertexStride() {
				if(m_list.size() == 1) {
					return 0;
				}
				return vertexSize();
			}

			xdl_uint vertexSize() {
				xdl_uint tmp = 0;
				std::vector<XdevLVertexDeclarationItem*>::iterator ib(m_list.begin());
				while(ib != m_list.end()) {
					tmp += bufferElementTypeSizeInBytes((*ib)->elementType) * (*ib)->numberOfComponents;
					ib++;
				}
				return tmp;
			}

		private:

			std::vector<XdevLVertexDeclarationItem*> m_list;
	};

	using IXdevLVertexDeclaration = XdevLVertexDeclaration ;
	using IPXdevLVertexDeclaration = std::shared_ptr<XdevLVertexDeclaration>;

}

#endif
