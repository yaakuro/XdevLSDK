/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_TEXTURE_SAMPLER_H
#define XDEVL_TEXTURE_SAMPLER_H

#include <XdevLRAI/XdevLRAIFWD.h>

namespace xdl {

	/**
		@enum XdevLTextureWrap
		@brief 	Definitions of the possible addressing methods of a texture
	*/
	enum class XdevLTextureWrap {
		UNKNOWN					= 0,
		CLAMP					= 0x2900,
		REPEAT					= 0x2901,
		CLAMP_TO_BORDER			= 0x812D,
		CLAMP_TO_EDGE			= 0x812F,
		MIRROR_CLAMP_TO_EDGE	= 0x8743
	};

	/**
	@enum 	XdevLTextureFilterMag
	@brief 	Filter modes for textures
	*/
	enum class XdevLTextureFilterMag {
		UNKNOWN = 0,
		NEAREST					= 0x2600,
		LINEAR 					= 0x2601,
	};


	/**
		@enum 	XdevLTextureFilterMin
		@brief 	Filter modes for textures
	*/
	enum class XdevLTextureFilterMin {
		UNKNOWN = 0,
		NEAREST					= 0x2600,
		LINEAR 					= 0x2601,
		NEAREST_MIPMAP_NEAREST	= 0x2700,
		LINEAR_MIPMAP_NEAREST	= 0x2701,
		NEAREST_MIPMAP_LINEAR	= 0x2702,
		LINEAR_MIPMAP_LINEAR	= 0x2703
	};


	/**
		@enum 	XdevLTextureCoord
		@brief 	Texture coordinate components
	*/
	enum class XdevLTextureCoord {
		UNKNOWN = 0,
		U = 0x2802,
		V = 0x2803,
		W = 0x8072
	};

	/**
		@enum 	XdevLTextureFilterStage
		@brief 	Filter for textures
	*/
	enum class XdevLTextureFilterStage {
		UNKNOWN = 0,
		MAG_FILTER = 0x2800,
		MIN_FILTER = 0x2801
	};

	/**
		@enum 	XdevLTextureFilter
		@brief 	Filter modes for textures
	*/
	enum class XdevLTextureFilter {
		UNKNOWN = 0,
		NEAREST					= 0x2600,
		LINEAR 					= 0x2601,
		NEAREST_MIPMAP_NEAREST	= 0x2700,
		LINEAR_MIPMAP_NEAREST	= 0x2701,
		NEAREST_MIPMAP_LINEAR	= 0x2702,
		LINEAR_MIPMAP_LINEAR	= 0x2703
	};

	/**
		@enum 	XdevLTextureStage
		@brief 	Texture stages
	*/
	enum class XdevLTextureStage {
		STAGE0 = 0,
		STAGE1,
		STAGE2,
		STAGE3,
		STAGE4,
		STAGE5,
		STAGE6,
		STAGE7,
		STAGE8,
		STAGE9,
		STAGE10,
		STAGE11,
		STAGE12,
		STAGE13,
		STAGE14,
		STAGE15,
		STAGE16,
		STAGE17,
		STAGE18,
		STAGE19,
		STAGE20,
		STAGE21,
		STAGE22,
		STAGE23,
		STAGE24,
		STAGE25,
		STAGE26,
		STAGE27,
		STAGE28,
		STAGE29,
		STAGE30,
		STAGE31
	};

	enum class XdevLTextureSamplerCompareOperator {
		NEVER		= 0x0200,
		LESS		= 0x0201,
		EQUAL		= 0x0202,
		LEQUAL		= 0x0203,
		GREATER		= 0x0204,
		NOTEQUAL	= 0x0205,
		GEQUAL		= 0x0206,
		ALWAYS		= 0x0207
	};

	enum class XdevLTextureSamplerBorderColor {
		FLOAT_TRANSPARENT_BLACK = 0,
		INT_TRANSPARENT_BLACK = 1,
		FLOAT_OPAQUE_BLACK = 2,
		INT_OPAQUE_BLACK = 3,
		FLOAT_OPAQUE_WHITE = 4,
		INT_OPAQUE_WHITE = 5,
	};

	struct XdevLTextureSamplerFilterStage {
		XdevLTextureFilterMin minFilter;
		XdevLTextureFilterMag magFilter;
	};

	struct XdevLTextureSamplerWrap {
		XdevLTextureSamplerWrap()
			: addressModeU(XdevLTextureWrap::CLAMP_TO_BORDER)
			, addressModeV(XdevLTextureWrap::CLAMP_TO_BORDER)
			, addressModeW(XdevLTextureWrap::CLAMP_TO_BORDER)
		{}

		XdevLTextureSamplerWrap(const XdevLTextureWrap& U, const XdevLTextureWrap& V)
			: addressModeU(U)
			, addressModeV(V)
			, addressModeW(XdevLTextureWrap::CLAMP_TO_BORDER)
		{}

		XdevLTextureSamplerWrap(const XdevLTextureWrap& U, const XdevLTextureWrap& V, const XdevLTextureWrap& W)
			: addressModeU(U)
			, addressModeV(V)
			, addressModeW(W)
		{}


		XdevLTextureWrap addressModeU;
		XdevLTextureWrap addressModeV;
		XdevLTextureWrap addressModeW;
	};

	struct XdevLTextureSamplerLOD {
		xdl_float minLod;
		xdl_float maxLod;
		xdl_float lodBias;
	};

	struct XdevLTextureSamplerCompare {
		xdl_bool enable;
		XdevLTextureSamplerCompareOperator compareOp;
	};

	struct XdevLTextureSamplerAnisotropic {
		xdl_bool anisotropyEnable;
		xdl_float maxAnisotropy;
	};

	struct XdevLTextureSamplerStage {
		XdevLTextureSamplerWrap addressMode;
		XdevLTextureSamplerFilterStage filter;
		XdevLTextureSamplerLOD lod;
		XdevLTextureSamplerCompare compare;
		XdevLTextureSamplerAnisotropic anisotropic;
		XdevLTextureSamplerBorderColor borderColor;
	};

	class XdevLTextureSampler {
		public:
			virtual ~XdevLTextureSampler() {};

			/// Sets the address mode of the texture.
			virtual xdl_int setTextureWrap(const XdevLTextureSamplerWrap& wrap) = 0;

			/// Sets the filter type of the texture.
			virtual xdl_int setTextureFilter(const XdevLTextureSamplerFilterStage& filterStage) = 0;

			/// Sets the MinLOD, MaxLOD and LOD bias state.
			virtual xdl_int setLOD(const XdevLTextureSamplerLOD& lod) = 0;

			/// Sets the texture comparison.
			virtual xdl_int setTextureCompare(const XdevLTextureSamplerCompare& compare) = 0;

			/// Sets the anisotropy filter.
			virtual xdl_int setTextureAnisotropy(const XdevLTextureSamplerAnisotropic& anisotropic) = 0;

			/// Sets the border color.
			virtual xdl_int setBorderColor(XdevLTextureSamplerBorderColor borderColor) = 0;

			virtual void activate(XdevLTextureStage stage) = 0;
			virtual void deactivate() = 0;
			virtual void lock() = 0;
			virtual void unlock() = 0;
	};

}


#endif
