/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_STREAM_BUFFER_H
#define XDEVL_STREAM_BUFFER_H

#include <XdevLUtils.h>

namespace xdl {

	/**
		@enum XdevLBufferAccessType
		@brief Access type for the buffer.
	*/
	enum class XdevLBufferAccessType {
		UNKNOWN,
		READ_ONLY	= 0x88B8,
		WRITE_ONLY	= 0x88B9,
		READ_WRITE	= 0x88BA
	};

	/**
		@enum XdevLBufferElementTypes
		@brief Element type in a buffer.
	*/
	enum class XdevLBufferElementTypes {
		UNKNOWN,
		BYTE 			= 0x1400,
		UNSIGNED_BYTE 	= 0x1401,
		SHORT 			= 0x1402,
		UNSIGNED_SHORT	= 0x1403,
		INT 			= 0x1404,
		UNSIGNED_INT	= 0x1405,
		FLOAT 			= 0x1406
	};

	/// Calculate the size of a buffer element.
	XDEVL_INLINE xdl_uint bufferElementTypeSizeInBytes(XdevLBufferElementTypes type) {
		switch(type) {
			case XdevLBufferElementTypes::BYTE:
			case XdevLBufferElementTypes::UNSIGNED_BYTE:
				return 1;
			case XdevLBufferElementTypes::SHORT:
			case XdevLBufferElementTypes::UNSIGNED_SHORT:
				return 2;
			case XdevLBufferElementTypes::INT:
			case XdevLBufferElementTypes::UNSIGNED_INT:
			case XdevLBufferElementTypes::FLOAT:
				return 4;
			default:
				break;
		}
		XDEVL_ASSERT(type == XdevLBufferElementTypes::UNKNOWN, "Unknown buffer element type.");
		return 0;
	}

	/**
	 * @class XdevLStreamBuffer
	 * @brief Base Interface for all buffers that can be streamed.
	 */
	class XdevLStreamBuffer {
		public:
			virtual ~XdevLStreamBuffer() {}

			/// Initialize an empty buffer.
			/**
			 * Will initialize the buffer without allocating memory nor setting its size.
			 * Using the upload will set its size and allocate memory for it.
			 * @return RET_SUCCESS if successful else RET_FAILED.
			 */
			virtual xdl_int init() = 0;

			/// Initialize an buffer with a specific size.
			virtual xdl_int init(xdl_uint size) = 0;

			/// Initialize a buffer with a specified size.
			/**
				@param src The source data to copy from.
				@param size The number of bytes to copy form the src.
			*/
			virtual xdl_int init(xdl_uint8* src, xdl_uint size) = 0;

			/// Lock the buffer for modification.
			/**
				Locks the buffer for modification. Before one can upload/change data
				the buffer needs to be locked. After finishing the job unlock the buffer.
			*/
			virtual xdl_int lock() = 0;

			/// Unlock the buffer after modification.
			virtual xdl_int unlock() = 0;

			/// Maps the buffer.
			/*
				Works only if the buffer was locked before.
			*/
			virtual xdl_uint8* map(XdevLBufferAccessType bufferAccessType) = 0;

			/// Unmaps the buffer.
			virtual xdl_int unmap() = 0;

			/// Upload streaming data to the buffer.
			/**
				The buffer maybe be rezied dependend on the size value.

				@param src The pointer to the data array to upload.
				@param size The the number of bytes to upload from the 'src'.
			*/
			virtual xdl_int upload(xdl_uint8* src, xdl_uint size, xdl_uint64 offset = 0) = 0;


			/// Returns the identification code.
			virtual xdl_uint id() = 0;

			/// Returns the size of the buffer in bytes.
			virtual xdl_uint getSize() = 0;
	};

}

#endif
