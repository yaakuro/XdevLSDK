/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_RAI_FWD_H
#define XDEVL_RAI_FWD_H

#include <XdevLTypes.h>

namespace xdl {

	class XdevLRAI;
	class XdevLFrameBuffer;
	class XdevLTexture;
	class XdevLVertexDeclaration;
	class XdevLVertexBuffer;
	class XdevLVertexArray;
	class XdevLUniformBuffer;
	class XdevLTextureCube;
	class XdevLTexture3D;
	class XdevLTexture;
	class XdevLStreamBuffer;
	class XdevLShaderProgram;
	class XdevLIndexBuffer;
	class XdevLShader;
	class XdevLVertexShader;
	class XdevLFragmentShader;
	class XdevLGeometryShader;
	class XdevLTessellationEvaluationShader;
	class XdevLTessellationControlShader;
	class XdevLStreamBuffer;
	class XdevLTextureSampler;
	struct XdevLTextureSamplerFilterStage;

	using IXdevLRAI = XdevLRAI;
	using IPXdevLRAI = XdevLRAI*;
	using IXdevLFrameBuffer = XdevLFrameBuffer;
	using IPXdevLFrameBuffer = std::shared_ptr<XdevLFrameBuffer>;
	using IXdevLVertexDeclaration = XdevLVertexDeclaration ;
	using IPXdevLVertexDeclaration = std::shared_ptr<XdevLVertexDeclaration>;
	using IXdevLTextureCube = XdevLTextureCube;
	using IPXdevLTextureCube = std::shared_ptr<XdevLTextureCube>;
	using IXdevLTexture3D = XdevLTexture3D;
	using IPXdevLTexture3D = std::shared_ptr<XdevLTexture3D>;
	using IXdevLTexture = XdevLTexture;
	using IPXdevLTexture = std::shared_ptr<XdevLTexture>;
	using IXdevLStreamBuffer = XdevLStreamBuffer;
	using IPXdevLStreamBuffer = XdevLStreamBuffer*;

	using IXdevLShaderProgram = XdevLShaderProgram;
	using IPXdevLShaderProgram = std::shared_ptr<XdevLShaderProgram>;
	using IXdevLShader = XdevLShader;
	using IPXdevLShader = std::shared_ptr<XdevLShader>;
	using IXdevLVertexShader = XdevLVertexShader;
	using IPXdevLVertexShader = std::shared_ptr<XdevLVertexShader>;
	using IXdevLFragmentShader = XdevLFragmentShader;
	using IPXdevLFragmentShader = std::shared_ptr<XdevLFragmentShader>;
	using IXdevLGeometryShader = XdevLGeometryShader;
	using IPXdevLGeometryShader = std::shared_ptr<XdevLGeometryShader>;
	using IXdevLTessellationEvaluationShader = XdevLTessellationEvaluationShader;
	using IPXdevLTessellationEvaluationShader = std::shared_ptr<XdevLTessellationEvaluationShader>;
	using IXdevLTessellationControlShader = XdevLTessellationControlShader;
	using IPXdevLTessellationControlShader = std::shared_ptr<XdevLTessellationControlShader>;

	using IXdevLVertexArray = XdevLVertexArray;
	using IPXdevLVertexArray = std::shared_ptr<XdevLVertexArray>;
	using IXdevLVertexBuffer = XdevLVertexBuffer;
	using IPXdevLVertexBuffer = std::shared_ptr<XdevLVertexBuffer>;
	using IXdevLIndexBuffer = XdevLIndexBuffer;
	using IPXdevLIndexBuffer = std::shared_ptr<XdevLIndexBuffer>;
	using IXdevLUniformBuffer = XdevLUniformBuffer;
	using IPXdevLUniformBuffer = std::shared_ptr<XdevLUniformBuffer>;
	using IXdevLTextureSampler = XdevLTextureSampler;
	using IPXdevLTextureSampler = std::shared_ptr<XdevLTextureSampler>;
	
	using IXdevLStreamBuffer = XdevLStreamBuffer;
	using IPXdevLStreamBuffer = XdevLStreamBuffer*;
}

#endif
