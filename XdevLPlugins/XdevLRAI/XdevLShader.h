/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SHADER_H
#define XDEVL_SHADER_H

#include <XdevLRAI/XdevLRAIFWD.h>
#include <XdevLFileSystem/XdevLFileSystem.h>

namespace xdl {

	enum class XdevLShaderType {
		UNKNOWN,
		VERTEX_SHADER			= 0x8B31,
		FRAGMENT_SHADER 		= 0x8B30,
		GEOMETRY_SHADER			= 0x8DD9,
		TESS_EVALUATION_SHADER	= 0x8E87,
		TESS_CONTROL_SHADER		= 0x8E88
	};

	/**
		@class XdevLShader
		@brief A Shader.

		The id() method return the OpenGL shader id.
	*/
	class XdevLShader {
		public:

			virtual ~XdevLShader() {}

			/// Adds shader source code to the current source code from a buffer.
			/**
				This function can help you to reuse code. Just add source fragments to this shader source code.
				@param shadercode A null terminated string that represents code fragments.
			*/
			virtual void addShaderCode(const xdl_char* shaderCode) = 0;


			/// Adds shader source code from a XdevLFile object.
			/**
				This method is the same like addShaderCode but instead of specifying a string buffer
				you have to specify a XdevLFile object.
				@param file A valid file object.
			*/
			virtual xdl_int addShaderCode(IPXdevLFile& file) = 0;

			/// Adds and compiles at the same time a whole shader source code.
			virtual xdl_int compile(XdevLFile* file = nullptr) = 0;

			/// Returns the identification code of this shader.
			virtual xdl_int id() = 0;

			/// Returns the type of the shader.
			virtual XdevLShaderType getShaderType() = 0;

			/// Checks if the shader source got compiled.
			virtual xdl_bool isCompiled() = 0;

			/// Returns the native handle.
			virtual void* getNativeHandle() {
				return nullptr;
			}
	};

	class XdevLVertexShader : public XdevLShader {
		public:
			virtual ~XdevLVertexShader() {}
	};

	class XdevLFragmentShader : public XdevLShader {
		public:
			virtual ~XdevLFragmentShader() {}
	};

	class XdevLGeometryShader : public XdevLShader {
		public:
			virtual ~XdevLGeometryShader() {}
	};

	class XdevLTessellationEvaluationShader : public XdevLShader {
		public:
			virtual ~XdevLTessellationEvaluationShader() {}
	};

	class XdevLTessellationControlShader : public XdevLShader {
		public:
			virtual ~XdevLTessellationControlShader() {}
	};
}

#endif
