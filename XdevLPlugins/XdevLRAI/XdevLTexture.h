/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_TEXTURE_H
#define XDEVL_TEXTURE_H

#include <XdevLRAI/XdevLTextureSampler.h>

namespace xdl {

	/**
		@enum 	XdevLTextureType
		@brief 	This defines, the 6 image IDs for the cubemap texture. You can access only with these IDs the wanted image.
	*/
	enum class XdevLTextureType {
		T_1D        = 0x0DE0,
		T_2D        = 0x0DE1,
		T_3D        = 0x806F,
		T_CUBE_MAP  = 0x8513
	};

	/**
		@enum 	XdevLCubemapPosition
		@brief 	This defines, the 6 image IDs for the cubemap texture. You can access only with these IDs the wanted image.
	*/
	enum class XdevLCubemapPosition {
		POSITIVE_X = 0x8515,
		NEGATIVE_X = 0x8516,
		POSITIVE_Y = 0x8517,
		NEGATIVE_Y = 0x8518,
		POSITIVE_Z = 0x8519,
		NEGATIVE_Z = 0x851A
	};

	/**
		Unsigned normalized integer means when shader access texture it will return values from [0.0,1.0].
		Signed normalized integer means when shader access texture it will return values from [-1.0,1.0].
	*/
	enum class XdevLTextureFormat {
		R8								= 0x8229,
		RG8								= 0x822B,
		RGB8							= 0x8051,
		RGBA8							= 0x8058,

		//
		// When shaders accessing textures having the following formats, actual integer values are returned.
		//
		R8I								= 0x8231,
		RG8I							= 0x8237,
		RGB8I							= 0x8D8F,
		RGBA8I							= 0x8D8E,

		R8UI							= 0x8232,
		RG8UI							= 0x8238,
		RGB8UI							= 0x8D7D,
		RGBA8UI							= 0x8D7C,
		// ---------------------------------------------------------------------

		R16								= 0x822A,
		RG16							= 0x822C,
		RGB16							= 0x8054,
		RGBA16							= 0x805B,

		//
		// When shaders accessing textures having the following formats, actual integer values are returned.
		//
		R16I							= 0x8233,
		RG16I							= 0x8239,
		RGB16I							= 0x8D89,
		RGBA16I							= 0x8D88,

		R16UI							= 0x8234,
		RG16UI							= 0x823A,
		RGB16UI							= 0x8D77,
		RGBA16UI						= 0x8D76,
		// ---------------------------------------------------------------------

		R16F							= 0x822D,
		RG16F							= 0x822F,
		RGB16F							= 0x881B,
		RGBA16F							= 0x881A,

		//
		// When shaders accessing textures having the following formats, actual integer values are returned.
		//
		R32I							= 0x8235,
		RG32I							= 0x823B,
		RGB32I							= 0x8D83,
		RGBA32I							= 0x8D82,

		R32UI							= 0x8236,
		RG32UI							= 0x823C,
		RGB32UI							= 0x8D71,
		RGBA32UI						= 0x8D70,
		// ---------------------------------------------------------------------

		R32F							= 0x822E,
		RG32F							= 0x8230,
		RGB32F							= 0x8815,
		RGBA32F							= 0x8814,

		R11F_G11F_B10F					= 0x8C3A,
		RGB9_E5							= 0x8C3D,
		SRGB							= 0x8C40,
		SRGB8							= 0x8C41,
		SRGB_ALPHA						= 0x8C42,
		SRGB8_ALPHA8					= 0x8C43,

		//
		// Signed normalized integer.
		//
		R8_SNORM						= 0x8F94,
		RG8_SNORM						= 0x8F95,
		RGB8_SNORM						= 0x8F96,
		RGBA8_SNORM						= 0x8F97,

		R16_SNORM						= 0x8F98,
		RG16_SNORM						= 0x8F99,
		RGB16_SNORM						= 0x8F9A,
		RGBA16_SNORM					= 0x8F9B,
		// ---------------------------------------------------------------------

		COMPRESSED_ALPHA 				= 0x84E9,
		COMPRESSED_LUMINANCE 			= 0x84EA,
		COMPRESSED_LUMINANCE_ALPHA 		= 0x84EB,
		COMPRESSED_INTENSITY 			= 0x84EC,
		COMPRESSED_RGB 					= 0x84ED,
		COMPRESSED_RGBA 				= 0x84EE,
		COMPRESSED_SRGB					= 0x8C48,
		COMPRESSED_SRGB_ALPHA			= 0x8C49,
		COMPRESSED_SLUMINANCE			= 0x8C4A,
		COMPRESSED_SLUMINANCE_ALPHA		= 0x8C4B,
		COMPRESSED_SRGB_S3TC_DXT1		= 0x8C4C,
		COMPRESSED_RGBA_S3TC_DXT1		= 0x83F1,
		COMPRESSED_RGBA_S3TC_DXT3		= 0x83F2,
		COMPRESSED_RGBA_S3TC_DXT5		= 0x83F3,
		COMPRESSED_SRGB_ALPHA_S3TC_DXT1	= 0x8C4D,
		COMPRESSED_SRGB_ALPHA_S3TC_DXT3	= 0x8C4E,
		COMPRESSED_SRGB_ALPHA_S3TC_DXT5	= 0x8C4F,

		DEPTH_COMPONENT					= 0x1902,
		DEPTH_COMPONENT16				= 0x81A5,
		DEPTH_COMPONENT24				= 0x81A6,
		DEPTH_COMPONENT32				= 0x81A7,
		DEPTH_COMPONENT32F				= 0x8CAC,

		DEPTH_STENCIL					= 0x84F9,
		DEPTH24_STENCIL8				= 0x88F0,
		DEPTH32F_STENCIL8				= 0x8CAD
	};

	enum class XdevLTexturePixelFormat {
		DEPTH_COMPONENT					= 0x1902,
		DEPTH_STENCIL					= 0x84F9,
		RED								= 0x1903,
		GREEN							= 0x1904,
		BLUE							= 0x1905,
		ALPHA							= 0x1906,
		RG								= 0x8227,
		RGB								= 0x1907,
		RGBA							= 0x1908,
		BGR								= 0x80E0,
		BGRA							= 0x80E1,
		LUMINANCE						= 0x1909,
		LUMINANCE_ALPHA					= 0x190A,
		RED_INTEGER						= 0x8D94,
		GREEN_INTEGER					= 0x8D95,
		BLUE_INTEGER					= 0x8D96,
		ALPHA_INTEGER					= 0x8D97,
		RG_INTEGER						= 0x8228,
		RGB_INTEGER						= 0x8D98,
		RGBA_INTEGER					= 0x8D99,
		BGR_INTEGER						= 0x8D9A,
		BGRA_INTEGER					= 0x8D9B
	};

	enum class XdevLTextureFormatType {
		DEPTH_COMPONENT = 0x1902,
		DEPTH_STENCIL	= 0x84F9,
		RGB 			= 0x1907,
		RGBA 			= 0x1908,
		RG 				= 0x8227

	};

	enum class XdevLTextureDataTypes {
		BYTE 			= 0x1400,
		UNSIGNED_BYTE	= 0x1401,
		SHORT			= 0x1402,
		UNSIGNED_SHORT	= 0x1403,
		INT				= 0x1404,
		UNSIGNED_INT	= 0x1405,
		FLOAT			= 0x1406,
		BYTES_2			= 0x1407,
		BYTES_3			= 0x1408,
		BYTES_4			= 0x1409,
		DOUBLE			= 0x140A
	};

	/**
		@class XdevLTexture
		@brief A texture.
	*/
	class XdevLTexture {
		public:

			virtual ~XdevLTexture() {}

			/// Initialize a basic texture from external source.
			virtual xdl_int init(xdl_uint width, xdl_uint height, XdevLTextureFormat format, XdevLTexturePixelFormat texturePixelFormat, xdl_uint8* data = nullptr) = 0;

			/// Lock the texture to modify.
			virtual xdl_int lock() = 0;

			/// Unlock the texture after modification.
			virtual xdl_int unlock() = 0;

			/// Copy from one texture to another texture.
			virtual xdl_int upload(XdevLTexture* texture) = 0;

			/// Upload data to texture.
			virtual xdl_int upload(xdl_uint8* buffer, xdl_uint64 size) = 0;

			/// Sets the filter type of the texture.
			XDEVL_DEPRECATED("Use setTextureFilter(const XdevLTextureSamplerFilterStage& filterStage) instead.") virtual xdl_int setTextureFilter(XdevLTextureFilterStage filterType, XdevLTextureFilter filter) = 0;
			virtual xdl_int setTextureFilter(const XdevLTextureSamplerFilterStage& filterStage) = 0;

			/// Sets the wrapping mode of the texture.
			/**
				@param texCoord
				@param textureWrap
			*/
			XDEVL_DEPRECATED("Use setTextureWrap(const XdevLTextureSamplerWrap& wrap) instead.") virtual xdl_int setTextureWrap(XdevLTextureCoord textureCoord, XdevLTextureWrap wrap) = 0;
			virtual xdl_int setTextureWrap(const XdevLTextureSamplerWrap& wrap) = 0;

			/// Sets the MinLOD, MaxLOD and LOD bias state.
			virtual xdl_int setLOD(const XdevLTextureSamplerLOD& lod) = 0;

			/// Sets the texture comparison.
			virtual xdl_int setTextureCompare(const XdevLTextureSamplerCompare& compare) = 0;

			/// Sets the anisotropy filter.
			/**
				@param pValue The anisotropy value that should be used
			*/
			virtual xdl_int setTextureMaxAnisotropy(xdl_float value) = 0;

			/// Sets the anisotropy filter.
			virtual xdl_int setTextureAnisotropy(const XdevLTextureSamplerAnisotropic& anisotropic) = 0;

			/// Sets the border color.
			virtual xdl_int setBorderColor(XdevLTextureSamplerBorderColor borderColor) = 0;

			/// Generates mip map automatically.
			virtual xdl_int generateMipMap() = 0;

			/// Activate the texture.
			/**
				@param stage The texture stage number to activate.
			*/
			virtual xdl_int activate(XdevLTextureStage stage) = 0;

			/// Deactivate the texture.
			virtual xdl_int deactivate() = 0;

			/// Return the identification code.
			virtual xdl_uint id() = 0;

			/// Returns the width of the texture.
			virtual xdl_uint getWidth() = 0;

			/// Returns the height of the texture.
			virtual xdl_uint getHeight() = 0;
	};
}


#endif
