/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_RAI_VULKAN_H
#define XDEVL_RAI_VULKAN_H

#include <XdevLPlatform.h>
#include <XdevLListener.h>
#include <XdevLModule.h>
#include <XdevLPluginImpl.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLVulkanContext/XdevLVulkanContext/XdevLVulkanInstanceImpl.h>
#include <XdevLVulkanContext/XdevLVulkanContext/XdevLVulkanDeviceImpl.h>

#include "XdevLSwapChainVulkan.h"

namespace xdl {

	static const std::vector<XdevLModuleName> raiVulkanModuleNames {
		XdevLModuleName("XdevLRAIVulkan")
	};	
	
	/**
		@class XdevLRAIVulkan
		@brief Interface class for 3D rendering support using Vulkan.
		@author Cengiz Terzibas
	*/
	class XdevLRAIVulkan : public XdevLModuleImpl<XdevLRAI> {
		public:
			XdevLRAIVulkan(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLRAIVulkan();

			xdl_int create(IPXdevLWindow window, IPXdevLArchive archive) override;
			XdevLFrameBuffer* getDefaultFrameBuffer() override final;
			XdevLFrameBuffer* get2DFrameBuffer() override final;

			xdl_int setActiveInternalFrameBuffer(xdl_bool state) override final;
			xdl_int setActive2DFrameBuffer(xdl_bool state) override final;
			xdl_int setActiveRenderWindow(XdevLWindow* window) override final;
			void setActiveDepthTest(xdl_bool enableDepthTest) override final;
			
			void setActiveScissorTest(xdl_bool enableScissorTest) override final;
			void setScissor(xdl_float x, xdl_float y, xdl_float width, xdl_float height) override final;

			void setActiveBlendMode(xdl_bool enableBlendMode) override final;
			xdl_int setBlendMode(XdevLBlendModes src, XdevLBlendModes dst, XdevLBlendModes alphaSrc = XDEVL_BLEND_ONE, XdevLBlendModes alphaDst = XDEVL_BLEND_ZERO) override final;
			xdl_int setBlendFunc(XdevLBlendFunc func) override final;

			void setPointSize(xdl_float size) override final;
			void setLineSize(xdl_float size) override final;

			xdl_int clearColorTargets(xdl_float r, xdl_float g, xdl_float b, xdl_float a) override final;
			xdl_int clearDepthTarget(xdl_float clear_value) override final;
			xdl_int setViewport(xdl_float x, xdl_float y, xdl_float width, xdl_float height) override final;
			void setViewport(const XdevLViewPort& viewPort) override final;
			XdevLViewPort getViewport();

			xdl_int swapBuffers() override final;

			IPXdevLVertexDeclaration createVertexDeclaration() override final;
			IPXdevLVertexShader createVertexShader() override final;
			IPXdevLFragmentShader createFragmentShader() override final;
			IPXdevLGeometryShader createGeometryShader() override final;
			IPXdevLTessellationControlShader createTessellationControlShader() override final;
			IPXdevLTessellationEvaluationShader createTessellationEvaluatioinShader() override final;
			IPXdevLShaderProgram createShaderProgram() override final;
			IPXdevLTextureSampler createTextureSampler() override final;
			IPXdevLTexture createTexture() override final;
			IPXdevLTextureCube createTextureCube() override final;
			IPXdevLTexture3D createTexture3D() override final;
			IPXdevLFrameBuffer createFrameBuffer() override final;
			IPXdevLVertexBuffer createVertexBuffer() override final;
			IPXdevLIndexBuffer createIndexBuffer() override final;
			IPXdevLUniformBuffer createUniformBuffer() override final;
			IPXdevLVertexArray createVertexArray() override final;

			xdl_int setActiveTextureSampler(XdevLTextureStage textureStage, IPXdevLTextureSampler textureSampler) override final;
			xdl_int setActiveFrameBuffer(IPXdevLFrameBuffer frambuffer) override final;
			xdl_int setActiveVertexArray(IPXdevLVertexArray vertexArray) override final;
			xdl_int setActiveShaderProgram(IPXdevLShaderProgram shaderProgram) override final;

			xdl_int drawVertexArray(XdevLPrimitiveType primitiveType, xdl_uint numberOfElements, xdl_uint64 indexOffset = 0) override final;
			xdl_int drawInstancedVertexArray(XdevLPrimitiveType primitiveType, xdl_uint numberOfElements, xdl_uint number) override final;

			IPXdevLArchive getArchive() const override final;

			VkInstance getVulkanInstance();
			VkPhysicalDevice getVulkanPhysicalDevice();
			VkDevice getVulkanDevice();

		private:
			IPXdevLArchive m_archive;
			IPXdevLVulkanContext m_vulkanContext;
			IPXdevLFrameBuffer m_activeFrameBuffer;
			IPXdevLVertexArray m_activeVertexArray;
			IPXdevLShaderProgram m_activeShaderProgram;
			XdevLFrameBuffer* m_defaultFrameBuffer;
			XdevLFrameBuffer* m_default2DFrameBuffer;
			IPXdevLWindow m_activeWindow;

			XdevLVulkanInstanceImpl* m_instanceImpl;
			VkInstance m_instance;
			VkPhysicalDevice m_physicalDevice;
			XdevLVulkanDeviceImpl* m_deviceImpl;
			VkDevice m_device;
			XdevLSwapChainVulkan m_swapChain;
	};

	XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLRAI)
}


#endif
