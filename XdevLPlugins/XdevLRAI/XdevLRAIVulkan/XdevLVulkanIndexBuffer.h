/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_RAI_VULKAN_INDEX_BUFFER_H
#define XDEVL_RAI_VULKAN_INDEX_BUFFER_H

#include <XdevLTypes.h>
#include <XdevLVulkanContext/XdevLVulkanContext.h>
#include <XdevLVulkanContext/XdevLVulkanContext/XdevLVulkanIndexBufferImpl.h>
#include <XdevLRAI/XdevLIndexBuffer.h>

namespace xdl {

	class XdevLRAIVulkanIndexBuffer : public XdevLIndexBuffer, public XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT> {
		public:
			XdevLRAIVulkanIndexBuffer(VkDevice device, VkPhysicalDeviceMemoryProperties memoryProperties)
				: XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>(device, memoryProperties)
				, m_elementType(XdevLBufferElementTypes::UNSIGNED_INT) {
			}

			virtual ~XdevLRAIVulkanIndexBuffer() {};

			xdl_int init() override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::init();
			}

			xdl_int init(xdl_uint size) override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::init(size);
			}

			xdl_int init(xdl_uint8* src, xdl_uint size) override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::init(src, size);
			}

			xdl_int init(XdevLBufferElementTypes indexBufferType) override final {
				m_elementType = indexBufferType;
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::init();
			}

			xdl_int init(XdevLBufferElementTypes indexBufferType, xdl_uint8* src, xdl_uint numberOfIndices) override final {
				return RET_FAILED;
			}

			XdevLBufferElementTypes getElementType() override final {
				return m_elementType;
			}

			xdl_uint getSizeOfElementType() override final {
				return bufferElementTypeSizeInBytes(m_elementType);
			}

			xdl_int lock() override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::lock();
			}

			xdl_int unlock() override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::unlock();
			}

			xdl_uint8* map(XdevLBufferAccessType bufferAccessType) override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::map(0);
			}

			xdl_int unmap() override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::unmap();
			}

			xdl_int upload(xdl_uint8* src, xdl_uint size, xdl_uint64 offset = 0) override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::upload(src, size, offset);
			}

			xdl_uint getSize() override final {
				return XdevLVulkanBufferBaseImpl<XdevLVulkanBufferBase, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::getSize();
			}

			xdl_uint id() override final {
				return 0;
			}

			XdevLBufferElementTypes m_elementType;
	};

}

#endif
