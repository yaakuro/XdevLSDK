/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLCommandBuffersVulkan.h"
#include "XdevLUtilsVulkan.h"

#include <iostream>

namespace xdl {

	CommandBuffer::CommandBuffer() :
		m_device(VK_NULL_HANDLE),
		m_commandPool(VK_NULL_HANDLE),
		m_cmdBuffer(VK_NULL_HANDLE) {

	}

	int CommandBuffer::init(VkDevice device, XdevLCommandPoolVulkan& commandPool) {
		m_device = device;
		m_commandPool = commandPool.getCommandPool();

		VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
		commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		commandBufferAllocateInfo.commandPool = m_commandPool;
		commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		commandBufferAllocateInfo.commandBufferCount = 1;

		VkResult result = vkAllocateCommandBuffers(m_device, &commandBufferAllocateInfo, &m_cmdBuffer);
		if(result != VK_SUCCESS) {
			XDEVL_MODULEX_ERROR(CommandBuffer, "vkAllocateCommandBuffers failed: " << vkVkResultToString(result) << std::endl);
			return 1;
		}
		return 0;
	}

	int CommandBuffer::destroy() {
		vkFreeCommandBuffers(m_device, m_commandPool, 1, &m_cmdBuffer);
		return 0;
	}

int CommandBuffer::begin() {
		VkCommandBufferBeginInfo cmdBufInfo = {};
		cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

		VkResult result = vkBeginCommandBuffer(m_cmdBuffer, &cmdBufInfo);
		assert(!result);

		return 0;
	}

	int CommandBuffer::end(VkQueue queue) {
		if(m_cmdBuffer == VK_NULL_HANDLE) {
			return 1;
		}

		VkResult result = vkEndCommandBuffer(m_cmdBuffer);
		assert(!result);

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &m_cmdBuffer;

		result = vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
		assert(!result);

		result = vkQueueWaitIdle(queue);
		assert(!result);
        return 0;
	}

	//
	// Command buffers.
	//


	CommandBuffers::CommandBuffers() :
		m_device(VK_NULL_HANDLE),
		m_commandPool(VK_NULL_HANDLE),
		m_postPresentCmdBuffer(VK_NULL_HANDLE) {

	}

	int CommandBuffers::init(VkDevice device, XdevLCommandPoolVulkan& commandPool, const XdevLSwapChainVulkan& swapChain) {
		m_device = device;
		m_commandPool = commandPool.getCommandPool();
		m_CmdBuffers.resize(swapChain.getImageCount());

		VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
		commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		commandBufferAllocateInfo.commandPool = commandPool;
		commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		commandBufferAllocateInfo.commandBufferCount = m_CmdBuffers.size();

		VkResult result = vkAllocateCommandBuffers(m_device, &commandBufferAllocateInfo, m_CmdBuffers.data());
		if(result != VK_SUCCESS) {
			XDEVL_MODULEX_ERROR(CommandBuffer, "vkAllocateCommandBuffers failed: " << vkVkResultToString(result) << std::endl);
			return 1;
		}

		// Create one command buffer for submitting the
		// post present image memory barrier
		commandBufferAllocateInfo.commandBufferCount = 1;

		result = vkAllocateCommandBuffers(m_device, &commandBufferAllocateInfo, &m_postPresentCmdBuffer);
		if(result != VK_SUCCESS) {
			XDEVL_MODULEX_ERROR(CommandBuffer, "vkAllocateCommandBuffers failed: " << vkVkResultToString(result) << std::endl);
			return 1;
		}
		return 0;
	}

	int CommandBuffers::destroy() {
		vkFreeCommandBuffers(m_device, m_commandPool, (uint32_t)m_CmdBuffers.size(), m_CmdBuffers.data());
		return 0;
	}

}
