/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLVulkanShaderProgram.h"
#include "XdevLVulkanShader.h"
#include <XdevLError.h>


namespace xdl {

	XdevLVulkanShaderProgram::XdevLVulkanShaderProgram()
		: m_inUse(xdl_false)
		, m_program(nullptr) {
		glslang::InitializeProcess();
		m_program = new glslang::TProgram;
	}

	XdevLVulkanShaderProgram::~XdevLVulkanShaderProgram() {
		glslang::FinalizeProcess();
	}

	xdl_int XdevLVulkanShaderProgram::create(const XdevLString& name) {
		m_name = name;
		return RET_SUCCESS;
	}

	const XdevLString& XdevLVulkanShaderProgram::getName() const {
		return m_name;
	}

	xdl_int XdevLVulkanShaderProgram::attach(IPXdevLShader shader) {
		glslang::TShader* glslangShader = (glslang::TShader*)(shader)->getNativeHandle();
		XDEVL_ASSERT(shader->isCompiled(), "Can't attach shader that is not compiled first.");

		m_program->addShader(glslangShader);
		m_attachedShaders.push_back(shader);
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanShaderProgram::link() {

		// According to the OpenGL specs
		if(m_attachedShaders.size() == 0) {
			XDEVL_MODULEX_ERROR(XdevLRAI, "No shader objects are attached to program.\n");
			return RET_FAILED;
		}

		// If you check the comment in the attach method you will see that according to OpenGL specs
		// the shader objects has to be compiled. Let's make that sure here.
		for(auto& shader : m_attachedShaders) {
			if(shader->isCompiled() == xdl_false) {
				XDEVL_MODULEX_ERROR(XdevLRAI, "At least one of the attached shaders are not compiled.\n");
				return RET_FAILED;
			}
		}

		EShMessages messages = EShMsgDefault;
		if(!m_program->link(messages)) {
			XDEVL_MODULEX_ERROR(XdevLRAI, "Linking failed.\n");
			std::cout << m_program->getInfoLog() << std::endl;
			std::cout << m_program->getInfoDebugLog() << std::endl;
			return RET_FAILED;
		}


		for(int stage = 0; stage < EShLangCount; ++stage) {
			if(m_program->getIntermediate((EShLanguage)stage)) {
				std::vector<unsigned int> spirv;
				std::string warningsErrors;
				spv::SpvBuildLogger logger;
				glslang::SpvOptions spvOptions;
				spvOptions.generateDebugInfo = true;

				glslang::GlslangToSpv(*m_program->getIntermediate((EShLanguage)stage), spirv, &logger, &spvOptions);
				glslang::OutputSpvBin(spirv, "test.spv");
			}
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanShaderProgram::getUniformLocation(const char* id) {
		return -1;
	}

	xdl_int XdevLVulkanShaderProgram::getAttribLocation(const char* id) {
		return -1;
	}

	void XdevLVulkanShaderProgram::activate() {
		m_inUse = true;
	}
	void XdevLVulkanShaderProgram::deactivate() {
		m_inUse = false;
	}

	void XdevLVulkanShaderProgram::setUniformMatrix4(xdl_uint id, xdl_uint count, const xdl_float* v1) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformMatrix3(xdl_uint id, xdl_uint count, const xdl_float* v1) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformMatrix2(xdl_uint id, xdl_uint count, const xdl_float* v1) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformi(xdl_uint id, xdl_int v1) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformi(xdl_uint id, xdl_int v1, xdl_int v2) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformi(xdl_uint id, xdl_int v1, xdl_int v2, xdl_int v3) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformi(xdl_uint id, xdl_int v1, xdl_int v2, xdl_int v3, xdl_int v4) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformui(xdl_uint id, xdl_uint v1) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2, xdl_uint v3) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniformui(xdl_uint id, xdl_uint v1, xdl_uint v2, xdl_uint v3, xdl_uint v4) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform1uiv(xdl_uint id, xdl_uint count, xdl_uint* v) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform2uiv(xdl_uint id, xdl_uint count, xdl_uint* v) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform3uiv(xdl_uint id, xdl_uint count, xdl_uint* v) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform4uiv(xdl_uint id, xdl_uint count, xdl_uint* v) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform(xdl_uint id, xdl_float v1) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform(xdl_uint id, xdl_float v1, xdl_float v2) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3, xdl_float v4) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform1v(xdl_uint id, xdl_uint number, xdl_float* array) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform2v(xdl_uint id, xdl_uint number, xdl_float* array) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform3v(xdl_uint id, xdl_uint number, xdl_float* array) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setUniform4v(xdl_uint id, xdl_uint number, xdl_float* array) {
		assert(m_inUse && "activate() not used.\n");
	}

	void XdevLVulkanShaderProgram::setVertexAttrib(xdl_uint id, xdl_float v1) {
		assert(m_inUse && "begin() not used.\n");
	}

	void XdevLVulkanShaderProgram::setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2) {
		assert(m_inUse && "begin() not used.\n");
	}

	void XdevLVulkanShaderProgram::setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3) {
		assert(m_inUse && "begin() not used.\n");
	}

	void XdevLVulkanShaderProgram::setVertexAttrib(xdl_uint id, xdl_float v1, xdl_float v2, xdl_float v3, xdl_float v4) {
		assert(m_inUse && "begin() not used.\n");
	}

	xdl_uint XdevLVulkanShaderProgram::id() {
		return 0;
	}

	xdl_int XdevLVulkanShaderProgram::bind(const xdl::XdevLString blockName, xdl_int bindingPoint) {
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanShaderProgram::save(IPXdevLFile file) {
		return RET_FAILED;
	}

	xdl_int XdevLVulkanShaderProgram::load(IPXdevLFile file) {
		return RET_FAILED;
	}

	xdl_int XdevLVulkanShaderProgram::cache() {
		return RET_FAILED;
	}
	xdl_int XdevLVulkanShaderProgram::uncache() {
		return RET_FAILED;
	}
}
