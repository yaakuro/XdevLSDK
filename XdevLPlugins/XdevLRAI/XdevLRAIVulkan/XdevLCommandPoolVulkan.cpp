/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLCommandPoolVulkan.h"
#include "XdevLUtilsVulkan.h"

#include <iostream>

namespace xdl {

	XdevLCommandPoolVulkan::XdevLCommandPoolVulkan() :
		m_device(VK_NULL_HANDLE),
		m_cmdPool(VK_NULL_HANDLE) {

	}

	int XdevLCommandPoolVulkan::init(VkDevice device, const XdevLSwapChainVulkan& swapChain) {
		m_device = device;

		//
		// Create command pool.
		//

		VkCommandPoolCreateInfo cmdPoolInfo = {};
		cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		cmdPoolInfo.queueFamilyIndex = swapChain.getQueueNodeIndex();
		cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		VkResult result = vkCreateCommandPool(m_device, &cmdPoolInfo, nullptr, &m_cmdPool);
		if(result != VK_SUCCESS) {
			XDEVL_MODULEX_ERROR(XdevLCommandPoolVulkan, "vkCreateCommandPool failed: " << vkVkResultToString(result) << std::endl);
			return 1;
		}

		return 0;
	}

	int XdevLCommandPoolVulkan::destroy() {
		if(VK_NULL_HANDLE != m_cmdPool) {
			vkDestroyCommandPool(m_device, m_cmdPool, nullptr);
		}
		return 0;
	}


}
