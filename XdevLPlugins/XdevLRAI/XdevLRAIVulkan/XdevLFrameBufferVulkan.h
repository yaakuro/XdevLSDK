/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_FRAME_BUFFER_VULKAN_H
#define XDEVL_FRAME_BUFFER_VULKAN_H

#include "XdevLSwapChainVulkan.h"

#include <vulkan/vulkan.h>

namespace xdl {

	class XdevLFrameBufferVulkan {
		public:
			XdevLFrameBufferVulkan(VkPhysicalDeviceMemoryProperties deviceMemoryProperties, VkCommandBuffer setupCmdBuffer);

			struct DepthStencil {
				VkImage image;
				VkDeviceMemory mem;
				VkImageView view;
			};

			int init(VkDevice device, VkRenderPass renderPass, XdevLSwapChainVulkan& swapChain, uint32_t width, uint32_t height);
			int createDepthStencil(VkFormat depthFormat);

		private:

			VkBool32 getMemoryType(VkPhysicalDeviceMemoryProperties deviceMemoryProperties, uint32_t typeBits, VkFlags properties, uint32_t * typeIndex);
			void setImageLayout(VkCommandBuffer cmdbuffer,
			                    VkImage image,
			                    VkImageAspectFlags aspectMask,
			                    VkImageLayout oldImageLayout,
			                    VkImageLayout newImageLayout);
			VkBool32 getSupportedDepthFormat(VkPhysicalDevice physicalDevice, VkFormat *depthFormat);

		private:
			VkDevice m_device;
			VkPhysicalDeviceMemoryProperties m_deviceMemoryProperties;
			VkCommandBuffer m_commandBuffer;
			std::vector<VkFramebuffer> m_frameBuffers;
			DepthStencil m_depthStencil;
			uint32_t m_width;
			uint32_t m_height;
			VkFormat m_depthFormat;
	};
}

#endif
