/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <vector>
#include <cassert>
#include <stdlib.h>

#include <XdevLError.h>
#include <XdevLVulkanContext/XdevLVulkanUtils.h>
#include <XdevLVulkanContext/XdevLVulkanContext/XdevLVulkanBufferBaseImpl.h>

#include "XdevLVertexArrayVulkan.h"
#include "XdevLVulkanVertexBuffer.h"
#include "XdevLVulkanIndexBuffer.h"

#include <iostream>

namespace xdl {

	XdevLVertexArrayVulkan::XdevLVertexArrayVulkan() 
		: m_device(VK_NULL_HANDLE)
		, m_physicalDevice(VK_NULL_HANDLE)
		, m_vd(nullptr)
		, m_indexBuffer(nullptr)
		, m_activated(xdl_false) {
	}

	XdevLVertexArrayVulkan::~XdevLVertexArrayVulkan() {

	}

	xdl_int XdevLVertexArrayVulkan::init() {
		return RET_SUCCESS;
	}

	xdl_int XdevLVertexArrayVulkan::activate() {
		assert(!m_activated && "XdevLVertexArrayVulkan::activate: Array initialized already.");

		m_activated = xdl_true;

		return RET_SUCCESS;
	}

	xdl_int  XdevLVertexArrayVulkan::deactivate() {
		assert(m_activated && "XdevLVertexArrayVulkan::activate: Not activated.");

		m_activated = xdl_false;
		return RET_SUCCESS;
	}

	xdl_int XdevLVertexArrayVulkan::setVertexStreamBuffer(xdl_uint shaderAttribute, xdl_uint numberOfComponents, XdevLBufferElementTypes itemSizeType, IPXdevLVertexBuffer vertexBuffer) {
		assert(m_activated && "XdevLVertexArrayVulkan::activate: Not activated.");
		assert(vertexBuffer && "XdevLVertexArrayVulkan::activate: No valid Vertex Buffer specified.");

		if(m_vd == NULL) {
			m_vd = std::make_shared<XdevLVertexDeclaration>();
		}

		m_vd->add(numberOfComponents, itemSizeType, shaderAttribute);

//		// Bind the Vertex Buffer to the array object.
//		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->id());
//
//		glEnableVertexAttribArray(shaderAttribute);
//		glVertexAttribPointer(shaderAttribute, numberOfComponents, itemSizeType, GL_FALSE, 0, (void*)(0));
//
//		glBindBuffer(GL_ARRAY_BUFFER, 0);

		m_vertexBufferList.push_back(vertexBuffer);
		return RET_SUCCESS;
	}

	xdl_int XdevLVertexArrayVulkan::setIndexBuffer(IPXdevLIndexBuffer indexBuffer) {
		assert(m_activated && "XdevLVertexArrayVulkan::activate: Not activated.");
		assert(indexBuffer && "XdevLVertexArrayVulkan::activate: No valid Index Buffer specified.");

		m_indexBuffer = indexBuffer;

		// Bind the Index Buffer to the array object.
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer->id());

		return RET_SUCCESS;
	}

	xdl_int XdevLVertexArrayVulkan::init(xdl_uint8 numberOfStreamBuffers,
	                                   xdl_uint8* srcOfSreamBuffers[],
	                                   xdl_uint numberOfVertex,
	                                   IPXdevLVertexDeclaration vd) {
		m_vd = vd;
//
//		glGenVertexArrays(1, &m_id);
//		glBindVertexArray(m_id);
//
//		m_vertexBufferList.reserve(numberOfStreamBuffers);
//		m_vertexBufferList.resize(numberOfStreamBuffers);
//
//		for(xdl_uint a = 0; a < numberOfStreamBuffers; a++) {
//			IPXdevLVertexBuffer vb = std::shared_ptr<XdevLVertexBufferImpl>(new XdevLVertexBufferImpl());
//			m_vertexBufferList[a] = vb;
//		}
//
//		for(xdl_uint idx = 0; idx < numberOfStreamBuffers; idx++) {
//
//			GLuint shaderAttribute 	= m_vd->get(idx)->shaderAttribute;
//
//			m_vertexBufferList[idx]->init(srcOfSreamBuffers[idx],  m_vd->vertexSize()*numberOfVertex);
//
//			glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferList[idx]->id());
//			glEnableVertexAttribArray(shaderAttribute);
//			glVertexAttribPointer(shaderAttribute, m_vd->get(idx)->numberOfComponents, m_vd->get(idx)->elementType, GL_FALSE, 0, (void*)(0));
//		}
//
//		glBindVertexArray(0);

		return RET_SUCCESS;
	}

	xdl_int XdevLVertexArrayVulkan::init(xdl_uint8* src, xdl_uint numberOfVertex, IPXdevLVertexDeclaration vd) {
		m_vd = vd;
//
//		// Create array object.
//		glGenVertexArrays(1, &m_id);
//		glBindVertexArray(m_id);
//
//		// Create vertex buffer object.
//		m_vertexBufferList.reserve(1);
//		m_vertexBufferList.resize(1);
//
//		IPXdevLVertexBuffer vb = std::shared_ptr<XdevLVertexBufferImpl>(new XdevLVertexBufferImpl());
//		vb->init(src,  vd->vertexSize()*numberOfVertex);
//		m_vertexBufferList.push_back(vb);
//		glBindBuffer(GL_ARRAY_BUFFER, vb->id());
//
//		xdl_uint64 pos = 0;
//		for(xdl_uint idx = 0; idx < m_vd->getNumber(); idx++) {
//
//			GLuint shaderAttribute = m_vd->get(idx)->shaderAttribute;
//			glEnableVertexAttribArray(shaderAttribute);
//			glVertexAttribPointer(shaderAttribute,
//			                      m_vd->get(idx)->numberOfComponents,
//			                      m_vd->get(idx)->elementType,
//			                      GL_FALSE,
//			                      m_vd->vertexStride(),
//			                      BUFFER_OFFSET(pos));
//
//			pos += m_vd->get(idx)->numberOfComponents*m_vd->get(idx)->elementTypeSizeInBytes;
//		}
//
//		glBindVertexArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);

		return RET_SUCCESS;
	}

	xdl_int  XdevLVertexArrayVulkan::init(IPXdevLVertexBuffer vertexBuffer, IPXdevLVertexDeclaration vd) {
		m_vd = vd;
//
//		// Create array object.
//		glGenVertexArrays(1, &m_id);
//		glBindVertexArray(m_id);
//
//		// Create vertex buffer object.
//		m_vertexBufferList.reserve(1);
//		m_vertexBufferList.resize(1);
//
//		m_vertexBufferList.push_back(vertexBuffer);
//		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->id());
//
//		xdl_uint64 pos = 0;
//		for(xdl_uint idx = 0; idx < m_vd->getNumber(); idx++) {
//
//			GLuint shaderAttribute = m_vd->get(idx)->shaderAttribute;
//			glEnableVertexAttribArray(shaderAttribute);
//			glVertexAttribPointer(shaderAttribute,
//			                      m_vd->get(idx)->numberOfComponents,
//			                      m_vd->get(idx)->elementType,
//			                      GL_FALSE,
//			                      m_vd->vertexStride(),
//			                      BUFFER_OFFSET(pos));
//
//			pos += m_vd->get(idx)->numberOfComponents*m_vd->get(idx)->elementTypeSizeInBytes;
//		}
//
//		glBindVertexArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);

		return RET_SUCCESS;
	}

	xdl_int XdevLVertexArrayVulkan::init(IPXdevLVertexBuffer vertexBuffer, IPXdevLIndexBuffer indexBuffer, IPXdevLVertexDeclaration vd) {
		m_vd = vd;
		m_indexBuffer = indexBuffer;
//
//		// Create array object.
//		glGenVertexArrays(1, &m_id);
//		glBindVertexArray(m_id);
//		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->id());
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer->id());
//
//		// Create vertex buffer object.
//		m_vertexBufferList.push_back(vertexBuffer);
//
//		xdl_uint64 pos = 0;
//		for(xdl_uint idx = 0; idx < m_vd->getNumber(); idx++) {
//
//			GLuint shaderAttribute = m_vd->get(idx)->shaderAttribute;
//			glEnableVertexAttribArray(shaderAttribute);
//			glVertexAttribPointer(shaderAttribute,
//			                      m_vd->get(idx)->numberOfComponents,
//			                      m_vd->get(idx)->elementType,
//			                      m_vd->get(idx)->normalized ? GL_TRUE : GL_FALSE,
//			                      m_vd->vertexStride(),
//			                      BUFFER_OFFSET(pos));
//
//			pos += m_vd->get(idx)->numberOfComponents*m_vd->get(idx)->elementTypeSizeInBytes;
//		}
//
//		glBindVertexArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		return RET_SUCCESS;
	}

	xdl_int XdevLVertexArrayVulkan::init(xdl_uint32 numberIndices,
	                                   xdl_uint8* srcOfIndices,
	                                   xdl_uint8 numberOfStreamBuffers,
	                                   xdl_uint8* srcOfSreamBuffers[],
	                                   xdl_uint numberOfVertex,
	                                   IPXdevLVertexDeclaration vd) {



		m_vd = vd;

//		m_indexBuffer = std::shared_ptr<XdevLRAIVulkanIndexBuffer>(new XdevLRAIVulkanIndexBuffer());
//		m_indexBuffer->init();

		m_indexBuffer->lock();
		m_indexBuffer->upload(srcOfIndices, sizeof(xdl_uint)*numberIndices);
		m_indexBuffer->unlock();

//		glGenVertexArrays(1, &m_id);
//		glBindVertexArray(m_id);
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer->id());

		m_vertexBufferList.reserve(numberOfStreamBuffers);
		m_vertexBufferList.resize(numberOfStreamBuffers);

		for(xdl_uint a = 0; a < numberOfStreamBuffers; a++) {
//			IPXdevLVertexBuffer vb = std::make_shared<XdevLRAIVulkanVertexBuffer>(m_device, m_instance->getPhysicalDeviceMemoryProperties());
//			m_vertexBufferList[a] = vb;
		}


		for(xdl_uint idx = 0; idx < numberOfStreamBuffers; idx++) {

//			GLuint shaderAttribute 	= m_vd->get(idx)->shaderAttribute;

//			XdevLVertexDeclaration* vdecl = new XdevLVertexDeclaration();
//			vdecl->add(m_vd->get(idx)->numberOfComponents, m_vd->get(idx)->elementType, shaderAttribute);

//			m_vertexBufferList[idx]->init(srcOfSreamBuffers[idx], vdecl->vertexSize()*numberOfVertex);
//			glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferList[idx]->id());

//			glEnableVertexAttribArray(shaderAttribute);
//			glVertexAttribPointer(shaderAttribute, m_vd->get(idx)->numberOfComponents, m_vd->get(idx)->elementType, GL_FALSE, 0, (void*)(0));
		}

//		//
//		// Let's unbind all objects so that nothing can get messed up by other OpenGL code fragments.
//		//
//		glBindVertexArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		return RET_SUCCESS;
	}


	IPXdevLVertexDeclaration XdevLVertexArrayVulkan::getVertexDeclarationRef() {
		return m_vd;
	}

	XdevLVertexDeclaration* XdevLVertexArrayVulkan::getVertexDeclaration() {
		return m_vd.get();
	}

	xdl_uint XdevLVertexArrayVulkan::id() {
		return 0;
	}

	IPXdevLVertexBuffer XdevLVertexArrayVulkan::getVertexBufferRef(xdl_uint indexNumber) {
		return m_vertexBufferList[indexNumber];
	}

	IPXdevLIndexBuffer XdevLVertexArrayVulkan::getIndexBufferRef() {
		return m_indexBuffer;
	}

	XdevLVertexBuffer* XdevLVertexArrayVulkan::getVertexBuffer(xdl_uint indexNumber) {
		return m_vertexBufferList[indexNumber].get();
	}

	XdevLIndexBuffer* XdevLVertexArrayVulkan::getIndexBuffer() {
		return m_indexBuffer.get();
	}

	xdl_int32 XdevLVertexArrayVulkan::getNumberOfVertexBuffers() const {
		return m_vertexBufferList.size();
	}

	void XdevLVertexArrayVulkan::add(IPXdevLVertexBuffer vertexBuffer, xdl_uint number, XdevLBufferElementTypes size, xdl_uint shader_attribute) {
		m_vertexBufferList.push_back(vertexBuffer);

//		glBindVertexArray(m_id);
//		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->id());
//		glEnableVertexAttribArray(shader_attribute);
//		glVertexAttribPointer(shader_attribute, number, size, GL_FALSE, 0, (void*)(0));
//
//		//
//		// Let's unbind all objects so that nothing can get messed up by other OpenGL code fragments.
//		//
//		glBindVertexArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}
}
