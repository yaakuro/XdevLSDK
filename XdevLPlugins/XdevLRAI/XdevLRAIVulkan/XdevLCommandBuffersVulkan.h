/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_COMMAND_BUFFERS_VULKAN_H
#define XDEVL_COMMAND_BUFFERS_VULKAN_H

#include "XdevLCommandPoolVulkan.h"

#include <vulkan/vulkan.h>

namespace xdl {

	class CommandBuffer {
		public:
			CommandBuffer();
			int init(VkDevice device, XdevLCommandPoolVulkan& commandPool);
			int destroy();
			int begin();
			int end(VkQueue queue);

			VkCommandBuffer getCommandBuffer() {
				return m_cmdBuffer;
			}
			operator VkCommandBuffer() {
				return m_cmdBuffer;
			}
		public:
			VkDevice m_device;
			VkCommandPool m_commandPool;
			VkCommandBuffer m_cmdBuffer;
	};

	class CommandBuffers {
		public:
			CommandBuffers();
			int init(VkDevice device, XdevLCommandPoolVulkan& commandPool, const XdevLSwapChainVulkan& swapChain);
			int destroy();
			std::vector<VkCommandBuffer>& getCommandBuffers() {
				return m_CmdBuffers;
			}
		public:
			VkDevice m_device;
			VkCommandPool m_commandPool;
			VkCommandBuffer m_postPresentCmdBuffer;
			std::vector<VkCommandBuffer> m_CmdBuffers;
	};
}

#endif
