/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_RAI_VULKAN_FRAME_BUFFER_H
#define XDEVL_RAI_VULKAN_FRAME_BUFFER_H

#include <XdevLVulkanContext/XdevLVulkanContext/XdevLVulkanFrameBufferImpl.h>
#include <XdevLVulkanContext/XdevLVulkanContext/XdevLVulkanTextureImpl.h>
#include "XdevLVulkanTexture.h"

#include <vulkan/vulkan.h>

namespace xdl {

	VkFormat xdevLFrameBufferFormatToVulkanFormat(XdevLFrameBufferColorFormat format) {
		switch(format) {
			case XdevLFrameBufferColorFormat::RGBA8: return VK_FORMAT_R8G8B8A8_UNORM;
			default: break;
		}
		return VK_FORMAT_UNDEFINED;
	}

	VkFormat xdevLFrameBufferDepthStencilFormatToVulkanFormat(XdevLFrameBufferDepthStencilFormat format) {
		switch(format) {
			case XdevLFrameBufferDepthStencilFormat::DEPTH_STENCIL: return VK_FORMAT_D16_UNORM_S8_UINT;
			case XdevLFrameBufferDepthStencilFormat::DEPTH24_STENCIL8: return VK_FORMAT_D24_UNORM_S8_UINT;
			case XdevLFrameBufferDepthStencilFormat::DEPTH32F_STENCIL8: return VK_FORMAT_D32_SFLOAT_S8_UINT;
			default: break;
		}
		return VK_FORMAT_UNDEFINED;
	}

	class XdevLRAIVulkanFrameBuffer : public XdevLFrameBuffer, public XdevLVulkanFrameBufferImpl {
		public:
			XdevLRAIVulkanFrameBuffer(XdevLVulkanInstanceImpl* instanceImpl, XdevLVulkanDeviceImpl* deviceImpl)
				: XdevLVulkanFrameBufferImpl(instanceImpl, deviceImpl)
				, m_depthStencil(nullptr)
				, m_activated(xdl_false) {
				m_colorTargets.reserve(8);
				m_colorTargets.resize(8);
			}

			virtual ~XdevLRAIVulkanFrameBuffer() {}

			xdl_int init(xdl_uint width, xdl_uint height) override final {
				m_size = {width, height};

				return XdevLVulkanFrameBufferImpl::create(width, height);
			}

			xdl_int addColorTarget(xdl_uint target_index, XdevLFrameBufferColorFormat format) override final {
				auto texture = std::make_shared<XdevLRAIVulkanTexture>(m_instanceImpl, m_deviceImpl);
				texture->create(getWidth(), getHeight(), xdevLFrameBufferFormatToVulkanFormat(format));
				m_colorTargets[target_index] = std::move(texture);

				return XdevLVulkanFrameBufferImpl::addColorTarget(target_index, m_colorTargets[target_index]->getFormat(), m_colorTargets[target_index]->getVulkanImage());
			}

			xdl_int addColorTarget(xdl_uint target_index, IPXdevLTexture texture) override final {
				auto tmp = std::static_pointer_cast<XdevLRAIVulkanTexture>(texture);
				m_colorTargets[target_index] = std::move(tmp);

				return XdevLVulkanFrameBufferImpl::addColorTarget(target_index, m_colorTargets[target_index]->getFormat(), m_colorTargets[target_index]->getVulkanImage());
			}

			xdl_int addColorTarget(xdl_uint target_index, IPXdevLTextureCube textureCube) override final {
				return RET_FAILED;
			}

			xdl_int addDepthTarget(XdevLFrameBufferDepthFormat internal_format) override final {
				return RET_FAILED;
			}

			xdl_int addDepthTarget(IPXdevLTexture texture) override final {
				return RET_FAILED;
			}

			xdl_int addDepthStencilTarget(XdevLFrameBufferDepthStencilFormat format) override final {
				auto texture = std::make_shared<XdevLRAIVulkanTexture>(m_instanceImpl, m_deviceImpl);
				texture->create(getWidth(), getHeight(), xdevLFrameBufferDepthStencilFormatToVulkanFormat(format));
				m_depthStencil = std::move(texture);

				return XdevLVulkanFrameBufferImpl::addDepthStencil(m_depthStencil->getFormat(), m_depthStencil->getVulkanImage());
			}

			xdl_int activate() override final {
				m_activated = xdl_true;
				return RET_SUCCESS;
			}

			xdl_int deactivate() override final {
				m_activated = xdl_false;
				return RET_SUCCESS;
			}

			xdl_int activateColorTargets(xdl_uint numberOfTargets, XdevLFrameBufferColorTargets* targetList) override final {
				return RET_FAILED;
			}

			xdl_int activateColorTargetCubePosition(xdl_uint target_index, XdevLCubemapPosition cubemapPosition) override final {
				return RET_FAILED;
			}

			xdl_int setActiveDepthTest(xdl_bool state) override final {
				return RET_FAILED;
			}

			xdl_int activateStencilTarget(xdl_bool state) override final {
				return RET_FAILED;
			}

			xdl_int clearColorTargets(xdl_float r, xdl_float g, xdl_float b, xdl_float a) override final {
				return RET_FAILED;
			}

			xdl_int clearDepthTarget(xdl_float clear_value) override final {
				return RET_FAILED;
			}

			xdl_int clearStencilTarget(xdl_int clear_value) override final {
				return RET_FAILED;
			}

			xdl_uint getWidth() override final {
				return m_size.width;
			}

			xdl_uint getHeight() override final {
				return m_size.height;
			}

			IPXdevLTexture getTexture(xdl_uint idx = 0) override final {
				return m_colorTargets[idx];
			}

			IPXdevLTexture getDepthTexture() override final {
				return nullptr;
			}

			xdl_uint getNumColorTextures() override final {
				return (xdl_uint)m_colorTargets.size();
			}

			xdl_uint id() override final {
				return 0;
			}

			void blit(XdevLFrameBuffer* framebuffer, xdl_uint32 numberOfRenderTargets, XdevLFrameBufferRenderTarget renderTargets[]) override final {}

		private:

			std::vector<std::shared_ptr<XdevLRAIVulkanTexture>> m_colorTargets;
			std::shared_ptr<XdevLRAIVulkanTexture> m_depthStencil;
			xdl_bool m_activated;
			XdevLSizeUI m_size;
	};
}

#endif
