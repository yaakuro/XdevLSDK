/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_RAI_VULKAN_SHADER_H
#define XDEVL_RAI_VULKAN_SHADER_H

#include <vector>
#include <XdevLThread.h>
#include <XdevLVulkanContext/XdevLVulkanContext/XdevLVulkanShaderImpl.h>

#include <glslang/Include/ShHandle.h>
#include <glslang/Include/revision.h>
#include <glslang/Public/ShaderLang.h>
#include <SPIRV/GlslangToSpv.h>
#include <SPIRV/GLSL.std.450.h>
#include <SPIRV/doc.h>
#include <SPIRV/disassemble.h>

namespace xdl {

// Default include class for normal include convention of search backward
// through the stack of active include paths (for nested includes).
// Can be overridden to customize.
	class DirStackFileIncluder : public glslang::TShader::Includer {
		public:
			DirStackFileIncluder() : externalLocalDirectoryCount(0) { }

			virtual IncludeResult* includeLocal(const char* headerName,
			                                    const char* includerName,
			                                    size_t inclusionDepth) override {
				return readLocalPath(headerName, includerName, inclusionDepth);
			}

			virtual IncludeResult* includeSystem(const char* headerName,
			                                     const char* /*includerName*/,
			                                     size_t /*inclusionDepth*/) override {
				return readSystemPath(headerName);
			}

			// Externally set directories. E.g., from a command-line -I<dir>.
			//  - Most-recently pushed are checked first.
			//  - All these are checked after the parse-time stack of local directories
			//    is checked.
			//  - This only applies to the "local" form of #include.
			//  - Makes its own copy of the path.
			virtual void pushExternalLocalDirectory(const std::string& dir) {
				directoryStack.push_back(dir);
				externalLocalDirectoryCount = directoryStack.size();
			}

			virtual void releaseInclude(IncludeResult* result) override {
				if(result != nullptr) {
					delete [] static_cast<tUserDataElement*>(result->userData);
					delete result;
				}
			}

			virtual ~DirStackFileIncluder() override { }

		protected:
			typedef char tUserDataElement;
			std::vector<std::string> directoryStack;
			int externalLocalDirectoryCount;

			// Search for a valid "local" path based on combining the stack of include
			// directories and the nominal name of the header.
			virtual IncludeResult* readLocalPath(const char* headerName, const char* includerName, int depth) {
				// Discard popped include directories, and
				// initialize when at parse-time first level.
				directoryStack.resize(depth + externalLocalDirectoryCount);
				if(depth == 1)
					directoryStack.back() = getDirectory(includerName);

				// Find a directory that works, using a reverse search of the include stack.
				for(auto it = directoryStack.rbegin(); it != directoryStack.rend(); ++it) {
					std::string path = *it + '/' + headerName;
					std::replace(path.begin(), path.end(), '\\', '/');
					std::ifstream file(path, std::ios_base::binary | std::ios_base::ate);
					if(file) {
						directoryStack.push_back(getDirectory(path));
						return newIncludeResult(path, file, (int)file.tellg());
					}
				}

				return nullptr;
			}

			// Search for a valid <system> path.
			// Not implemented yet; returning nullptr signals failure to find.
			virtual IncludeResult* readSystemPath(const char* /*headerName*/) const {
				return nullptr;
			}

			// Do actual reading of the file, filling in a new include result.
			virtual IncludeResult* newIncludeResult(const std::string& path, std::ifstream& file, int length) const {
				char* content = new tUserDataElement [length];
				file.seekg(0, file.beg);
				file.read(content, length);
				return new IncludeResult(path, content, length, content);
			}

			// If no path markers, return current working directory.
			// Otherwise, strip file name and return path leading up to it.
			virtual std::string getDirectory(const std::string path) const {
				size_t last = path.find_last_of("/\\");
				return last == std::string::npos ? "." : path.substr(0, last);
			}
	};


	inline EShLanguage xdevLShaderTypeToLanguage(XdevLShaderType type) {
		switch(type) {
			case XdevLShaderType::VERTEX_SHADER: return EShLangVertex;
			case XdevLShaderType::FRAGMENT_SHADER: return EShLangFragment;
			case XdevLShaderType::GEOMETRY_SHADER: return EShLangGeometry;
			case XdevLShaderType::TESS_CONTROL_SHADER: return EShLangTessControl;
			case XdevLShaderType::TESS_EVALUATION_SHADER: return EShLangTessEvaluation;
			default: break;
		}
		// TODO Not sure about this.
		return EShLangCount;
	}

	template<class Base>
	class XdevLVulkanShaderBase : public XdevLVulkanShaderImpl, public Base {
		public:
			XdevLVulkanShaderBase(VkDevice device, VkShaderStageFlagBits shaderStageFalgBits, XdevLShaderType type)
				: XdevLVulkanShaderImpl(device, shaderStageFalgBits, XdevLString("main"))
				, m_shaderType(type)
				, m_shaderVersion(330) {
				// Make the version of the shader to 330.
				std::stringstream ss;
				ss << m_shaderVersion;

				// Add shader version into the first line.
				m_shaderVersionDefinition = "#version " + ss.str() + " core" + "\n";
				m_shaderSource.push_back(m_shaderVersionDefinition);
			}

			virtual ~XdevLVulkanShaderBase() {}

			void addShaderCode(const xdl_char* shaderCode) override final {
				m_shaderSource.push_back(std::string(shaderCode));
				m_dirtyFlag = xdl_true;
			}

			xdl_int addShaderCode(IPXdevLFile& file) override final {
				std::string shader;
				shader.reserve(static_cast<size_t>(file->length()));
				shader.resize(static_cast<size_t>(file->length()));

				// TODO Possible that not all of the file will be read.
				if(file->read((xdl_uint8*)shader.data(), static_cast<xdl_int>(file->length())) == -1) {
					XDEVL_MODULEX_ERROR(XdevLShaderImpl, "Reading from file: " << file->getFileName() << " failed.\n");
					return RET_FAILED;
				}

				//	m_shaderSource.push_back("#line __LINE__\n");
				m_shaderSource.push_back(shader);
				m_dirtyFlag = xdl_true;
				return RET_SUCCESS;

			}

			xdl_int compile(XdevLFile* file = nullptr) override final {
//				ShInitialize();
				glslang::InitializeProcess();
//				EShLanguage language = xdevLShaderTypeToLanguage(m_shaderType);
//				ShHandle compiler = ShConstructCompiler(language, EShMsgSpvRules);
//				if(compiler == 0) {
//					return RET_FAILED;
//				}

				if(nullptr != file) {
					auto length = file->length();
					std::vector<char> buffer;
					buffer.reserve(length);
					buffer.resize(length);

					file->read((xdl_uint8*)buffer.data(), buffer.size());

					m_shaderSource.push_back(buffer.data());
				}

//				compileShaderFromBuffer(compiler);
				auto result = compileShaderFromBuffer(0);
				if(RET_SUCCESS != result) {
					return RET_SUCCESS;
				}

//				ShDestruct(compiler);
				glslang::FinalizeProcess();
//				ShFinalize();

				return RET_SUCCESS;
			}

			xdl_int compileShaderFromBuffer(ShHandle compiler) {

				std::vector<int> lengths;
				std::vector<const char*> strings;
				for(auto& shaderSource : m_shaderSource) {
					lengths.push_back(shaderSource.size());
					strings.push_back(shaderSource.data());
				}

				xdl_uint messages = EShMsgDefault | EShMsgSpvRules;
				TBuiltInResource Resources {};

				EShLanguage language = xdevLShaderTypeToLanguage(m_shaderType);
				m_shader = new glslang::TShader(language);
				m_shader->setStringsWithLengths((const char* const*)strings.data(), lengths.data(), strings.size());
				m_shader->setAutoMapBindings(true);
				m_shader->setAutoMapLocations(true);
				m_shader->setEntryPoint(getEntryName().toString().c_str());
				m_shader->setSourceEntryPoint(getEntryName().toString().c_str());
				m_shader->setEnvInput(glslang::EShSourceGlsl, language, glslang::EShClientVulkan, 100);

				DirStackFileIncluder includer;
				std::string str;
				std::for_each(IncludeDirectoryList.rbegin(), IncludeDirectoryList.rend(), [&includer](const std::string& dir) {
					includer.pushExternalLocalDirectory(dir);
				});
				if(!m_shader->preprocess(&Resources, 100, ENoProfile, false, false, (EShMessages)messages, &str, includer)) {
					std::cout << m_shader->getInfoLog() << std::endl;
					std::cout << m_shader->getInfoDebugLog() << std::endl;
					return RET_FAILED;
				}
				if(!m_shader->parse(&Resources,100, ENoProfile, false, false, (EShMessages)messages, includer)) {
					std::cout << m_shader->getInfoLog() << std::endl;
					std::cout << m_shader->getInfoDebugLog() << std::endl;
					return RET_FAILED;
				}
				std::cout << str << std::endl;

				// Compile the shader.
//				auto result = ShCompile(compiler,
//										(const char* const*)strings.data(),
//										strings.size(),
//										lengths.data(),
//										EShOptNone,
//										&Resources,
//										0,
//										100,
//										false,
//										(EShMessages)messages);
//				if(0 == result) {
//					std::cerr << ShGetInfoLog(compiler) << std::endl;
//					return RET_FAILED;
//				}
//				auto executable = ShGetExecutable(compiler);
//				if(nullptr != executable) {
//
//				}
				m_dirtyFlag = xdl_false;

				return RET_SUCCESS;
			}

			xdl_int id() override final {
				return 0;
			}

			XdevLShaderType getShaderType() override final {
				return m_shaderType;
			}

			xdl_bool isCompiled() override final {
				return (m_dirtyFlag == xdl_false);
			}

			void* getNativeHandle() override final {
				return m_shader;
			}

		private:

			XdevLShaderType m_shaderType;
			std::vector<std::string> m_shaderSource;
			std::vector<xdl_uint8> m_shaderBinary;
			xdl_bool m_dirtyFlag;
			xdl_uint m_shaderVersion;
			TBuiltInResource Resources;
			std::string m_shaderVersionDefinition;
			std::vector<std::string> IncludeDirectoryList;
			glslang::TShader* m_shader;
	};

	class XdevLVulkanVertexShader : public XdevLVulkanShaderBase<XdevLVertexShader> {
		public:
			XdevLVulkanVertexShader(VkDevice device) : XdevLVulkanShaderBase(device, VK_SHADER_STAGE_VERTEX_BIT, XdevLShaderType::VERTEX_SHADER) {}

			virtual ~XdevLVulkanVertexShader() {}
	};

	class XdevLVulkanFragmentShader : public XdevLVulkanShaderBase<XdevLFragmentShader> {
		public:
			XdevLVulkanFragmentShader(VkDevice device) : XdevLVulkanShaderBase(device, VK_SHADER_STAGE_FRAGMENT_BIT, XdevLShaderType::FRAGMENT_SHADER) {}

			virtual ~XdevLVulkanFragmentShader() {}
	};

	class XdevLVulkanGeometryShader : public XdevLVulkanShaderBase<XdevLGeometryShader> {
		public:
			XdevLVulkanGeometryShader(VkDevice device) : XdevLVulkanShaderBase(device, VK_SHADER_STAGE_GEOMETRY_BIT, XdevLShaderType::GEOMETRY_SHADER) {}

			virtual ~XdevLVulkanGeometryShader() {}
	};

	class XdevLVulkanTessellationEvaluationShader : public XdevLVulkanShaderBase<XdevLTessellationEvaluationShader> {
		public:
			XdevLVulkanTessellationEvaluationShader(VkDevice device) : XdevLVulkanShaderBase(device, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, XdevLShaderType::TESS_EVALUATION_SHADER) {}

			virtual ~XdevLVulkanTessellationEvaluationShader() {}
	};

	class XdevLVulkanTessellationControlShader : public XdevLVulkanShaderBase<XdevLTessellationControlShader> {
		public:
			XdevLVulkanTessellationControlShader(VkDevice device) : XdevLVulkanShaderBase(device, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT, XdevLShaderType::TESS_CONTROL_SHADER) {}

			virtual ~XdevLVulkanTessellationControlShader() {}
	};
}
#endif
