/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLWindow/XdevLWindow.h>
#include <XdevLVulkanContext/XdevLVulkanUtils.h>

#include "XdevLRAIVulkan.h"
#include "XdevLVulkanVertexBuffer.h"
#include "XdevLVulkanIndexBuffer.h"
#include "XdevLVulkanUniformBuffer.h"
#include "XdevLVulkanShader.h"
#include "XdevLVulkanShaderProgram.h"
#include "XdevLVulkanFrameBuffer.h"
#include "XdevLVulkanTexture.h"
#include "XdevLVulkanVertexArray.h"

#include <array>

namespace xdl {

	VkBlendFactor xdevLBlendModeToVulkanBlendFactor(XdevLBlendModes mode) {
		switch(mode) {
			case XdevLBlendModes::XDEVL_BLEND_CONSTANT_ALPHA: return VK_BLEND_FACTOR_CONSTANT_ALPHA;
			case XdevLBlendModes::XDEVL_BLEND_CONSTANT_COLOR: return VK_BLEND_FACTOR_CONSTANT_COLOR;
			case XdevLBlendModes::XDEVL_BLEND_DST_ALPHA: return VK_BLEND_FACTOR_DST_ALPHA;
			case XdevLBlendModes::XDEVL_BLEND_DST_COLOR: return VK_BLEND_FACTOR_DST_COLOR;
			case XdevLBlendModes::XDEVL_BLEND_ONE: return VK_BLEND_FACTOR_ONE;
			case XdevLBlendModes::XDEVL_BLEND_ONE_MINUS_CONSTANT_ALPHA: return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
			case XdevLBlendModes::XDEVL_BLEND_ONE_MINUS_CONSTANT_COLOR: return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
			case XdevLBlendModes::XDEVL_BLEND_ONE_MINUS_DST_ALPHA: return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
			case XdevLBlendModes::XDEVL_BLEND_ONE_MINUS_DST_COLOR: return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
			case XdevLBlendModes::XDEVL_BLEND_ONE_MINUS_SRC_ALPHA: return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			case XdevLBlendModes::XDEVL_BLEND_ONE_MINUS_SRC_COLOR: return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
			case XdevLBlendModes::XDEVL_BLEND_SRC_ALPHA: return VK_BLEND_FACTOR_SRC_ALPHA;
			case XdevLBlendModes::XDEVL_BLEND_SRC_ALPHA_SATURATE: return VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
			case XdevLBlendModes::XDEVL_BLEND_SRC_COLOR: return VK_BLEND_FACTOR_SRC_COLOR;
			case XdevLBlendModes::XDEVL_BLEND_ZERO: return VK_BLEND_FACTOR_ZERO;
			case XdevLBlendModes::XDEVL_BLEND_SRC1_COLOR: return VK_BLEND_FACTOR_SRC1_COLOR;
			case XdevLBlendModes::XDEVL_BLEND_ONE_MINUS_SRC1_COLOR: return VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
			case XdevLBlendModes::XDEVL_BLEND_SRC1_ALPHA: return VK_BLEND_FACTOR_SRC1_ALPHA;
			case XdevLBlendModes::XDEVL_BLEND_ONE_MINUS_SRC1_ALPHA: return VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
			// TODO XDEVL_BLEND_NONE is not handled.
			default: break;
		}
		return VK_BLEND_FACTOR_ZERO;
	}

	VkCommandBuffer currentCommandBuffer = VK_NULL_HANDLE;

	VkRenderPassBeginInfo renderPassBeginInfo {};
	VkPipelineDepthStencilStateCreateInfo currentDepthStencil {};
	VkViewport currentViewport {};
	VkRect2D currentScissor {};
	std::array<VkClearValue, 2> clearValues {};

	//
	// Blending states.
	//
	VkPipelineColorBlendAttachmentState colorBlendAttachment {};
	VkPipelineColorBlendStateCreateInfo colorBlending {};

	XdevLRAIVulkan::XdevLRAIVulkan(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLModuleImpl<XdevLRAI>(parameter, descriptor),
		  m_archive(nullptr),
		  m_activeFrameBuffer(nullptr),
		  m_activeVertexArray(nullptr),
		  m_activeShaderProgram(nullptr),
		  m_defaultFrameBuffer(nullptr),
		  m_default2DFrameBuffer(nullptr),
		  m_instanceImpl(nullptr),
		  m_instance(VK_NULL_HANDLE),
		  m_physicalDevice(VK_NULL_HANDLE),
		  m_deviceImpl(nullptr),
		  m_device(VK_NULL_HANDLE) {
		currentDepthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	}

	XdevLRAIVulkan::~XdevLRAIVulkan() {
	}

	VkInstance XdevLRAIVulkan::getVulkanInstance() {
		return m_instance;
	}

	VkPhysicalDevice XdevLRAIVulkan::getVulkanPhysicalDevice() {
		return m_physicalDevice;
	}

	VkDevice XdevLRAIVulkan::getVulkanDevice() {
		return m_device;
	}

	IPXdevLArchive XdevLRAIVulkan::getArchive() const {
		return m_archive;
	}

	xdl_int XdevLRAIVulkan::create(IPXdevLWindow window, IPXdevLArchive archive) {

		XdevLString name(XdevLString("XdevLRAIVulkanContext"));
		XdevLID id(name);
		m_archive = archive;

		m_vulkanContext = static_cast<XdevLVulkanContext*>(getMediator()->createModule(XdevLModuleName("XdevLVulkanContext"), id));
		if(nullptr == m_vulkanContext) {
			XDEVL_MODULE_WARNING("Could not create OpenGL context. Assuming you're using a manually created one.\n");
		} else {
			// Create OpenGL context.
			if(m_vulkanContext->createContext(window) != RET_SUCCESS) {
				return RET_FAILED;
			}
		}
		m_instanceImpl = static_cast<XdevLVulkanInstanceImpl*>(m_vulkanContext->getInstance().get());
		m_deviceImpl =  static_cast<XdevLVulkanDeviceImpl*>(m_vulkanContext->getDevice().get());
		m_device = (VkDevice)m_vulkanContext->getDevice()->getNativeHandle();
		return RET_SUCCESS;
	}

	void XdevLRAIVulkan::setActiveScissorTest(xdl_bool enableScissorTest) {
		currentDepthStencil.stencilTestEnable = enableScissorTest ? VK_TRUE : VK_FALSE;
	}

	void XdevLRAIVulkan::setScissor(xdl_float x, xdl_float y, xdl_float width, xdl_float height) {
		currentScissor.offset.x = static_cast<int32_t>(x);
		currentScissor.offset.y = static_cast<int32_t>(y);
		currentScissor.extent.width = static_cast<int32_t>(width);
		currentScissor.extent.height = static_cast<int32_t>(height);
		vkCmdSetScissor(currentCommandBuffer, 0, 1, &currentScissor);
	}


	void XdevLRAIVulkan::setActiveBlendMode(xdl_bool enableBlendMode) {
		colorBlendAttachment.blendEnable = enableBlendMode ? VK_TRUE : VK_FALSE;
	}

	xdl_int XdevLRAIVulkan::setBlendMode(XdevLBlendModes src, XdevLBlendModes dst, XdevLBlendModes alphaSrc, XdevLBlendModes alphaDst) {
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachment.srcColorBlendFactor = xdevLBlendModeToVulkanBlendFactor(src);
		colorBlendAttachment.dstColorBlendFactor = xdevLBlendModeToVulkanBlendFactor(dst);
		colorBlendAttachment.srcAlphaBlendFactor = xdevLBlendModeToVulkanBlendFactor(alphaSrc);;
		colorBlendAttachment.dstAlphaBlendFactor = xdevLBlendModeToVulkanBlendFactor(alphaDst);
		return RET_FAILED;
	}

	xdl_int XdevLRAIVulkan::setBlendFunc(XdevLBlendFunc func) {
		switch(func) {
			case XdevLBlendFunc::XDEVL_FUNC_ADD: {
				colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
			} break;
			case XdevLBlendFunc::XDEVL_FUNC_SUBTRACT: {
				colorBlendAttachment.colorBlendOp = VK_BLEND_OP_SUBTRACT;
			} break;
			case XdevLBlendFunc::XDEVL_FUNC_REVERSE_SUBTRACT: {
				colorBlendAttachment.colorBlendOp = VK_BLEND_OP_REVERSE_SUBTRACT;
			} break;
			default: break;
		}
		return RET_FAILED;
	}

	XdevLFrameBuffer* XdevLRAIVulkan::getDefaultFrameBuffer() {
		return m_defaultFrameBuffer;
	}

	XdevLFrameBuffer* XdevLRAIVulkan::get2DFrameBuffer() {
		return m_default2DFrameBuffer;
	}

	xdl_int XdevLRAIVulkan::setActiveInternalFrameBuffer(xdl_bool state) {
		return RET_FAILED;
	}

	xdl_int XdevLRAIVulkan::setActive2DFrameBuffer(xdl_bool state) {
		return RET_FAILED;
	}

	void XdevLRAIVulkan::setPointSize(xdl_float size) {
	}

	void XdevLRAIVulkan::setLineSize(xdl_float size) {
	}

	void XdevLRAIVulkan::setActiveDepthTest(xdl_bool enableDepthTest) {
		currentDepthStencil.depthTestEnable = enableDepthTest ? VK_TRUE : VK_FALSE;
	}

	xdl_int XdevLRAIVulkan::clearColorTargets(xdl_float r, xdl_float g, xdl_float b, xdl_float a) {
		clearValues[0].color = {{r, g, b, a}};
		return RET_FAILED;
	}

	xdl_int XdevLRAIVulkan::clearDepthTarget(xdl_float clear_value) {
		clearValues[1].depthStencil.depth = clear_value;
		return RET_FAILED;
	}

	xdl_int XdevLRAIVulkan::setViewport(xdl_float x, xdl_float y, xdl_float width, xdl_float height) {
		currentViewport.x = x;
		currentViewport.y = y;
		currentViewport.width = width;
		currentViewport.height = height;
		currentViewport.minDepth = 0.0f;
		currentViewport.maxDepth = 1.0f;
		vkCmdSetViewport(currentCommandBuffer, 0, 1, &currentViewport);
		return RET_FAILED;
	}

	void XdevLRAIVulkan::setViewport(const XdevLViewPort& viewPort) {
		currentViewport.x = viewPort.x;
		currentViewport.y = viewPort.y;
		currentViewport.width = viewPort.width;
		currentViewport.height = viewPort.height;
		currentViewport.minDepth = 0.0f;
		currentViewport.maxDepth = 1.0f;
		vkCmdSetViewport(currentCommandBuffer, 0, 1, &currentViewport);
	}

	XdevLViewPort XdevLRAIVulkan::getViewport() {
		XdevLViewPort viewPort {};
		viewPort.x = currentViewport.x;
		viewPort.y = currentViewport.y;
		viewPort.width = currentViewport.width;
		viewPort.height = currentViewport.height;
		return viewPort;
	}

	xdl_int XdevLRAIVulkan::setActiveRenderWindow(IPXdevLWindow window) {
		m_activeWindow = window;
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIVulkan::swapBuffers() {
		return RET_FAILED;
	}

	IPXdevLVertexDeclaration XdevLRAIVulkan::createVertexDeclaration() {
		return std::make_shared<XdevLVertexDeclaration>();
	}

	IPXdevLVertexShader XdevLRAIVulkan::createVertexShader() {
		auto tmp = std::make_shared<XdevLVulkanVertexShader>(m_device);
		return tmp;
	}

	IPXdevLFragmentShader XdevLRAIVulkan::createFragmentShader() {
		auto tmp = std::make_shared<XdevLVulkanFragmentShader>(m_device);
		return tmp;
	}

	IPXdevLGeometryShader XdevLRAIVulkan::createGeometryShader() {
		auto tmp = std::make_shared<XdevLVulkanGeometryShader>(m_device);
		return tmp;
	}

	IPXdevLTessellationControlShader XdevLRAIVulkan::createTessellationControlShader() {
		auto tmp = std::make_shared<XdevLVulkanTessellationControlShader>(m_device);
		return tmp;
	}

	IPXdevLTessellationEvaluationShader XdevLRAIVulkan::createTessellationEvaluatioinShader() {
		auto tmp = std::make_shared<XdevLVulkanTessellationEvaluationShader>(m_device);
		return tmp;
	}

	IPXdevLShaderProgram XdevLRAIVulkan::createShaderProgram() {
		auto tmp = std::make_shared<XdevLVulkanShaderProgram>();
		return tmp;
	}

	IPXdevLTextureSampler XdevLRAIVulkan::createTextureSampler() {
		return nullptr;
	}

	IPXdevLTexture XdevLRAIVulkan::createTexture() {
		auto tmp = std::make_shared<XdevLRAIVulkanTexture>(m_instanceImpl, m_deviceImpl);
		return tmp;
	}

	IPXdevLTextureCube XdevLRAIVulkan::createTextureCube() {
		return nullptr;
	}

	IPXdevLTexture3D XdevLRAIVulkan::createTexture3D() {
		return nullptr;
	}

	IPXdevLFrameBuffer XdevLRAIVulkan::createFrameBuffer() {
		auto tmp = std::make_shared<XdevLRAIVulkanFrameBuffer>(m_instanceImpl, m_deviceImpl);
		return tmp;
	}

	IPXdevLVertexBuffer XdevLRAIVulkan::createVertexBuffer() {
		return std::make_shared<XdevLRAIVulkanVertexBuffer>(m_device, m_instanceImpl->getPhysicalDeviceMemoryProperties());
	}

	IPXdevLIndexBuffer XdevLRAIVulkan::createIndexBuffer() {
		return std::make_shared<XdevLRAIVulkanIndexBuffer>(m_device, m_instanceImpl->getPhysicalDeviceMemoryProperties());
	}

	IPXdevLUniformBuffer XdevLRAIVulkan::createUniformBuffer() {
		return std::make_shared<XdevLRAIVulkanUniformBuffer>(m_device, m_instanceImpl->getPhysicalDeviceMemoryProperties());
	}

	IPXdevLVertexArray XdevLRAIVulkan::createVertexArray() {
		auto tmp = std::make_shared<XdevLRAIVulkanVertexArray>();
		return tmp;
	}

	xdl_int XdevLRAIVulkan::setActiveTextureSampler(XdevLTextureStage textureStage, IPXdevLTextureSampler textureSampler) {
		return RET_FAILED;
	}

	xdl_int XdevLRAIVulkan::setActiveFrameBuffer(IPXdevLFrameBuffer framebuffer) {
		m_activeFrameBuffer = framebuffer;
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIVulkan::setActiveVertexArray(IPXdevLVertexArray vertexArray) {
		m_activeVertexArray = vertexArray;
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIVulkan::setActiveShaderProgram(IPXdevLShaderProgram shaderProgram) {
		m_activeShaderProgram = shaderProgram;
		return RET_SUCCESS;
	}

	xdl_int XdevLRAIVulkan::drawVertexArray(XdevLPrimitiveType primitiveType, xdl_uint numberOfElements, xdl_uint64 indexOffset) {
		//
		// Setup pipeline.
		//

		colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlending.logicOpEnable = VK_FALSE;
		colorBlending.logicOp = VK_LOGIC_OP_COPY;
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f;
		colorBlending.blendConstants[1] = 0.0f;
		colorBlending.blendConstants[2] = 0.0f;
		colorBlending.blendConstants[3] = 0.0f;
		return RET_FAILED;
	}

	xdl_int XdevLRAIVulkan::drawInstancedVertexArray(XdevLPrimitiveType primitiveType, xdl_uint numberOfElements, xdl_uint number) {
		return RET_FAILED;
	}

}
