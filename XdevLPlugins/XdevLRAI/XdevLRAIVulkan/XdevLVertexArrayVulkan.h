/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VERTEX_ARRAY_VULKAN_H
#define XDEVL_VERTEX_ARRAY_VULKAN_H

#include <XdevLVertexArray.h>

namespace xdl {

	class XdevLVertexArrayVulkan : public XdevLVertexArray {
		public:
			XdevLVertexArrayVulkan();
			virtual ~XdevLVertexArrayVulkan();
			xdl_int init() override final;
			xdl_int init(xdl_uint8* src, xdl_uint numberOfVertex, IPXdevLVertexDeclaration vd) override final;
			xdl_int init(IPXdevLVertexBuffer vertexBuffer, IPXdevLVertexDeclaration vd) override final;
			xdl_int init(IPXdevLVertexBuffer vertexBuffer, IPXdevLIndexBuffer indexBuffer, IPXdevLVertexDeclaration vd) override final;
			xdl_int init(xdl_uint8 numberOfStreamBuffers,
			                     xdl_uint8* srcOfSreamBuffers[],
			                     xdl_uint numberOfVertex,
			                     IPXdevLVertexDeclaration vd) override final;
			xdl_int init(xdl_uint32 numberIndices,
			                     xdl_uint8* srcOfIndices,
			                     xdl_uint8 numberOfStreamBuffers,
			                     xdl_uint8* srcOfSreamBuffers[],
			                     xdl_uint numberOfVertex,
			                     IPXdevLVertexDeclaration vd) override final;
			void add(IPXdevLVertexBuffer vertexBuffer, xdl_uint number, XdevLBufferElementTypes size, xdl_uint shader_attribute) override final;
			xdl_int activate() override final;
			xdl_int deactivate() override final;
			xdl_int setVertexStreamBuffer(xdl_uint shaderAttribute,
			                                      xdl_uint numberOfComponents,
			                                      XdevLBufferElementTypes itemSizeType,
			                                      IPXdevLVertexBuffer vertexBuffer) override final;
			xdl_int setIndexBuffer(IPXdevLIndexBuffer indexBuffer) override final;
			IPXdevLVertexBuffer getVertexBufferRef(xdl_uint indexNumber) override final;
			IPXdevLIndexBuffer getIndexBufferRef() override final;
			XdevLVertexBuffer* getVertexBuffer(xdl_uint indexNumber) override final;
			XdevLIndexBuffer* getIndexBuffer() override final;
			IPXdevLVertexDeclaration getVertexDeclarationRef() override final;
			XdevLVertexDeclaration* getVertexDeclaration() override final;
			xdl_int32 getNumberOfVertexBuffers() const override final;
			xdl_uint id() override final;

		private:
			VkDevice m_device;
			VkPhysicalDevice m_physicalDevice;
			IPXdevLVertexDeclaration m_vd;
			std::vector<IPXdevLVertexBuffer> m_vertexBufferList;
			IPXdevLIndexBuffer m_indexBuffer;
			xdl::xdl_bool m_activated;

	};

}

#endif
