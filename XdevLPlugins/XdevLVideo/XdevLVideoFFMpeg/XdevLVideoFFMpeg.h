/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef DEVICE_VIDEO_FFMPEG_H
#define DEVICE_VIDEO_FFMPEG_H

#include <XdevLPluginImpl.h>
#include <XdevLVideo/XdevLVideo.h>

#include <libavutil/attributes.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>

namespace xdl {

	static const XdevLString pluginName {
		"XdevLVideoFFMPEG"
	};

	static const XdevLString description {
		"Support for Video Playback using FFMpeg"
	};

	static std::vector<XdevLModuleName>	moduleNames {
		XdevLModuleName("XdevLVideo")
	};

	class XdevLVideoFFmpeg : public XdevLModuleImpl<XdevLVideo> {
		public:
			XdevLVideoFFmpeg(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLVideoFFmpeg();

			xdl_int init();
			xdl_int shutdown();
			xdl_int update();


			int open(const char* filename) override final;
			int close() override final;
			int getFrameWidth() override final;
			int getFrameHeight() override final;
			void* getFrameData() override final;
			void setFPS(float fps) override final;

		protected:

			void saveFrame(AVFrame *pFrame, int width, int height, int iFrame);

		protected:

			AVFormatContext* 	m_formatCtx;
			AVCodecContext* 	m_codecCtx;
			AVCodec*			m_codec;
			AVFrame*			m_frame;
			AVFrame*			m_frameRGB;
			SwsContext*			m_img_convert_ctx;
			uint8_t*			m_buffer;

			int videoStream;
			xdl::XdevLTimer		m_timer;
			double				m_updateFrequency;

			bool m_initialized;
			bool m_fileOpen;

	};

}

#endif
