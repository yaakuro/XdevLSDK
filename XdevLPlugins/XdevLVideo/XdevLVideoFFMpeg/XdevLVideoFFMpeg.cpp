#include "XdevLVideoFFMpeg.h"
#include <XdevLTimer.h>

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::pluginName,
	xdl::moduleNames,
	XDEVLFFMPEG_PLUGIN_MAJOR_VERSION,
	XDEVLFFMPEG_PLUGIN_MINOR_VERSION,
	XDEVLFFMPEG_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDesc {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::description,
	XDEVLFFMPEG_MODULE_MAJOR_VERSION,
	XDEVLFFMPEG_MODULE_MINOR_VERSION,
	XDEVLFFMPEG_MODULE_PATCH_VERSION,
	xdl::XDEVL_MODULE_STATE_DISABLE_AUTO_DESTROY
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLVideoFFmpeg, moduleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

namespace xdl {

	XdevLVideoFFmpeg::XdevLVideoFFmpeg(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLModuleImpl<XdevLVideo>(parameter, descriptor)
		, m_formatCtx(nullptr)
		, m_codecCtx(nullptr)
		, m_codec(nullptr)
		, m_frame(nullptr)
		, m_frameRGB(nullptr)
		, m_buffer(nullptr)
		, m_updateFrequency(30.0)
		, m_initialized(false)
		, m_fileOpen(false) {
	}

	XdevLVideoFFmpeg::~XdevLVideoFFmpeg() {}


	void XdevLVideoFFmpeg::setFPS(float fps) {
		m_updateFrequency = fps;
	}

	xdl_int XdevLVideoFFmpeg::init() {
		avcodec_register_all();
		av_register_all();

		videoStream = -1;
		m_initialized = true;
		return RET_SUCCESS;
	}
	xdl_int XdevLVideoFFmpeg::shutdown() {
		if(m_buffer) {
			av_free(m_buffer);
		}

		if(m_frame) {
			av_free(m_frame);
		}

		if(m_frameRGB) {
			av_free(m_frameRGB);
		}

		if(m_codecCtx) {
			avcodec_close(m_codecCtx);
		}
		if(m_formatCtx) {
			av_free(m_formatCtx);
		}

		m_initialized = false;
		m_fileOpen		= false;
		return RET_SUCCESS;
	}

	int XdevLVideoFFmpeg::open(const char* filename) {
		// Open video file
		if(avformat_open_input(&m_formatCtx, filename, nullptr, nullptr)!=0) {
			std::cerr << "Could not open video file: " << filename << std::endl;
			return xdl::RET_FAILED;
		}

		// Retrieve stream information
		if(avformat_find_stream_info(m_formatCtx, nullptr)<0)
			return xdl::RET_FAILED;

		for(unsigned int i=0; i<m_formatCtx->nb_streams; i++)
			if(m_formatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
				videoStream=i;
				break;
			}

		if(videoStream==-1) {
			std::cerr << "Videostream not found.\n";
			return xdl::RET_FAILED;
		}

		// Get a pointer to the codec context for the video stream
		m_codecCtx=m_formatCtx->streams[videoStream]->codec;
		// Find the decoder for the video stream
		m_codec=avcodec_find_decoder(m_codecCtx->codec_id);
		if(m_codec==nullptr) {
			std::cerr << "Unsupported codec!\n";
			return xdl::RET_FAILED; // Codec not found
		}

		// Open codec
		if(avcodec_open2(m_codecCtx, m_codec, nullptr)<0) {
			std::cerr << "Could not open coded.\n";
			return xdl::RET_FAILED;
		}


		// Allocate video frame
		m_frame = av_frame_alloc();
		m_frameRGB = av_frame_alloc();
		if(m_frameRGB==nullptr) {
			std::cerr << "Could not allocate codec frame memory.\n";
			return xdl::RET_FAILED;
		}

		int numBytes;
		// Determine required buffer size and allocate buffer
		numBytes=avpicture_get_size(PIX_FMT_RGB24, m_codecCtx->width, m_codecCtx->height);
		m_buffer=(uint8_t *)av_malloc(numBytes*sizeof(uint8_t));

		// Assign appropriate parts of buffer to image planes in m_frame
		// Note that m_frame is an AVFrame, but AVFrame is a superset
		// of AVPicture
		avpicture_fill((AVPicture *)m_frameRGB, m_buffer, PIX_FMT_RGB24, m_codecCtx->width, m_codecCtx->height);

		// Create image scale context to convert image into different formats.
		m_img_convert_ctx = sws_getContext(m_codecCtx->width, m_codecCtx->height,
		                                   m_codecCtx->pix_fmt,
		                                   m_codecCtx->width, m_codecCtx->height,
		                                   PIX_FMT_RGB24, SWS_BICUBIC, nullptr, nullptr, nullptr);

		m_fileOpen = true;
		m_timer.reset();
		return xdl::RET_SUCCESS;
	}

	int XdevLVideoFFmpeg::update() {

		if(!m_fileOpen)
			return RET_FAILED;

		if(m_timer.getTime() >= (1.0/m_updateFrequency)) {
//	if (true) {
			int frameFinished;
			AVPacket packet;

			av_read_frame(m_formatCtx, &packet);
			// Is this a packet from the video stream?
			if(packet.stream_index==videoStream) {
				// Decode video frame
				avcodec_decode_video2(m_codecCtx, m_frame, &frameFinished, &packet);

				// Did we get a video frame?
				if(frameFinished) {
					// Convert the image from its native format to RGB
//				img_convert((AVPicture *)m_frameRGB, PIX_FMT_RGB24, (AVPicture*)m_frame, m_codecCtx->pix_fmt, m_codecCtx->width, m_codecCtx->height);
					sws_scale(m_img_convert_ctx, m_frame->data, m_frame->linesize, 0, m_codecCtx->height, m_frameRGB->data, m_frameRGB->linesize);
					// Save the frame to disk
					//				saveFrame(m_frameRGB, m_codecCtx->width, m_codecCtx->height, 0);
				}
			}
			// Free the packet that was allocated by av_read_frame
			av_free_packet(&packet);
			m_timer.reset();
		}
		return xdl::RET_SUCCESS;
	}

	int XdevLVideoFFmpeg::close() {
		if(m_formatCtx) {
			av_free(m_formatCtx);
		}

		m_fileOpen = false;
		return RET_SUCCESS;
	}
	int XdevLVideoFFmpeg::getFrameWidth() {
		if(m_codecCtx)
			return m_codecCtx->width;
		return 0;
	}
	int XdevLVideoFFmpeg::getFrameHeight() {
		if(m_codecCtx)
			return m_codecCtx->height;
		return 0;
	}
	void* XdevLVideoFFmpeg::getFrameData() {
		return m_frameRGB->data[0];
	}

	void XdevLVideoFFmpeg::saveFrame(AVFrame *pFrame, int width, int height, int iFrame) {
		FILE *pFile;
		char szFilename[32];
		int  y;

		// Open file
		sprintf(szFilename, "frame%d.ppm", iFrame);
		pFile=fopen(szFilename, "wb");
		if(pFile==nullptr)
			return;

		// Write header
		fprintf(pFile, "P6\n%d %d\n255\n", width, height);

		// Write pixel data
		for(y=0; y<height; y++)
			fwrite(pFrame->data[0] + y * pFrame->linesize[0], 1, width*3, pFile);

		// Close file
		fclose(pFile);
	}


}
