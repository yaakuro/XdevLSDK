/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VIDEO_H
#define XDEVL_VIDEO_H

#include <XdevLModule.h>

namespace xdl {

	/**
	 * @class XdevLVideo
	 * @brief Interface for Video Capture and Playback.
	 */
	class XdevLVideo : public XdevLModule {
		public:
			virtual ~XdevLVideo() {}
			virtual int open(const char* file) = 0;
			virtual int close() = 0;
			virtual int getFrameWidth() = 0;
			virtual int getFrameHeight() = 0;
			virtual void* getFrameData() = 0;
			virtual void setFPS(float fps) = 0;
			virtual int update() = 0;
	};

	using IXdevLVideo = XdevLVideo;
	using IPXdevLVideo = XdevLVideo*;

	XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLVideo)
}

#endif
