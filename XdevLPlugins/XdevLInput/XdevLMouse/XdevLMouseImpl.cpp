/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLWindow/XdevLWindow.h>
#include "XdevLMouseImpl.h"

xdl::XdevLPluginDescriptor mousePluginDescriptor {
	xdl::mouse_pluginName,
	xdl::mouse_moduleNames,
	XDEVLMOUSE_MAJOR_VERSION,
	XDEVLMOUSE_MINOR_VERSION,
	XDEVLMOUSE_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleMouseDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::mouse_moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::mouseDescription,
	XDEVLMOUSE_MODULE_MAJOR_VERSION,
	XDEVLMOUSE_MODULE_MINOR_VERSION,
	XDEVLMOUSE_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(mousePluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLMouseImpl, moduleMouseDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLMouse, xdl::XdevLMouseImpl, moduleMouseDescriptor)

namespace xdl {

	static const XdevLID MouseButtonPressed("XDEVL_MOUSE_BUTTON_PRESSED");
	static const XdevLID MouseButtonReleased("XDEVL_MOUSE_BUTTON_RELEASED");
	static const XdevLID MouseMotion("XDEVL_MOUSE_MOTION");
	static const XdevLID MouseMotionRelative("XDEVL_MOUSE_MOTION_RELATIVE");
	static const XdevLID KeyButtonPressed("XDEVL_BUTTON_PRESSED");
	static const XdevLID KeyButtonReleased("XDEVL_BUTTON_RELEASED");

	XdevLMouseImpl::XdevLMouseImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLInputImpl<XdevLMouse>(parameter, descriptor)
		, m_mouse_curr_x(0)
		, m_mouse_curr_y(0)
		, m_mouse_old_x(0)
		, m_mouse_old_y(0)
		, m_mouse_button_down(xdl_false)
		, m_mouse_moved(xdl_false)
		, m_mouse_moved_old(xdl_false)
		, m_modState(KEY_MOD_NONE) {
	}

	XdevLMouseImpl::~XdevLMouseImpl() {
	}

	xdl_int XdevLMouseImpl::init() {

		if(MOUSE_MAX_AXES > 0) {
			m_axes.reserve(MOUSE_MAX_AXES);
			m_axes.resize(MOUSE_MAX_AXES);
			for(size_t a = 0; a < m_axes.size(); ++a) {
				m_axes[a] = new XdevLAxisImpl(&m_mutex);
			}
		}

		m_buttons[BUTTON_0] = new XdevLButtonImpl(&m_mutex);
		m_buttons[BUTTON_1] = new XdevLButtonImpl(&m_mutex);
		m_buttons[BUTTON_2] = new XdevLButtonImpl(&m_mutex);
		m_buttons[BUTTON_3] = new XdevLButtonImpl(&m_mutex);
		m_buttons[BUTTON_4] = new XdevLButtonImpl(&m_mutex);

		return Super::init();
	}

	xdl_int XdevLMouseImpl::update() {
		thread::XdevLScopeLock lock(m_mutex);

		m_mouse_moved_old = m_mouse_moved;
		m_mouse_moved = xdl_false;

		for(auto axe : m_axes) {
			axe->setDeltaValue(0.0f);
		}

		return Super::update();
	}


	xdl_int XdevLMouseImpl::attach(XdevLWindow* window) {

		Super::attach(window);

		m_windowSize = window->getSize();

		auto coreParameter = getMediator()->getCoreParameter();
		if(coreParameter.xmlBuffer.size() != 0) {
			TiXmlDocument xmlDocument;
			if(!xmlDocument.Parse((xdl_char*)coreParameter.xmlBuffer.data())) {
				XDEVL_MODULEX_ERROR(XdevLMouse, "Could not parse xml buffer.\n ");
				return RET_FAILED;
			}

			if(readMouseInfo(xmlDocument) != RET_SUCCESS) {
				return RET_FAILED;
			}
		}

		return RET_SUCCESS;
	}

	xdl_float XdevLMouseImpl::getValue(const xdl_uint axis) {
		return m_axes[axis]->getValue();
	}

	xdl_float XdevLMouseImpl::getDeltaValue(const xdl_uint axis) {
		return m_axes[axis]->getDeltaValue();
	}

	xdl_int XdevLMouseImpl::readMouseInfo(TiXmlDocument& document) {
		TiXmlHandle docHandle(&document);
		TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toString().c_str()).FirstChildElement(mouse_moduleNames[0].toString().c_str()).ToElement();
		if(!root) {
			XDEVL_MODULE_WARNING("<XdevLMouse> section not found. Using default values for the device.\n");
			return RET_SUCCESS;
		}

		while(root != NULL) {
			if(root->Attribute("id")) {
				XdevLID id(root->Attribute("id"));
				if(this->getID() == id) {
					// If there is a crt attribute, set this value for all buttons.
					if(root->Attribute("crt")) {
						XdevLTimeMilliseconds crt;
						crt.milliseconds = xstd::from_string<xdl_uint64>(root->Attribute("crt"));
						for(auto& button : m_buttons) {
							button.second->setClickResponseTime(crt);
						}
					}

					// If there is a min attribute, set this value for all axes.
					if(root->Attribute("min")) {
						xdl_float lmin = xstd::from_string<xdl_float>(root->Attribute("min"));
						for(size_t a = 0; a < m_axes.size(); ++a)
							m_axes[a]->setMin(lmin);
					}

					// If there is a max attribute, set this value for all axes.
					if(root->Attribute("max")) {
						xdl_float lmax = xstd::from_string<xdl_float>(root->Attribute("max"));
						for(size_t a = 0; a < m_axes.size(); ++a)
							m_axes[a]->setMax(lmax);
					}

					//FIXME Da die achsen erst spaeter erzeugt werden koennen die funktionen der achse nicht verwendet werden. Daher wird in m_axisMin und m_axisMax zwischengespeichert. das bitte aendern.
					TiXmlElement* child = 0;
					for(child = root->FirstChildElement(); child; child = child->NextSiblingElement()) {
						if(child->ValueTStr() == "Range") {
							if(child->Attribute("axis")) {
								auto axis = getAxisId(XdevLString(child->Attribute("axis")));
								float value = 0.0f;
								if(child->Attribute("min")) {
									value = xstd::from_string<float>(child->Attribute("min"));
									m_axes[axis]->setMin(value);
								}
								if(child->Attribute("max")) {
									value = xstd::from_string<float>(child->Attribute("max"));
									m_axes[axis]->setMax(value);
								}
							} else {
								XDEVL_MODULE_ERROR("No axis attribute in <Range> element found. Please specify an axis id.\n");
								return RET_FAILED;
							}
						}

						if(child->ValueTStr() == "ClickResponseTime") {
							if(child->Attribute("button")) {
								auto button = getButtonId(XdevLString(child->Attribute("button")));
								if (child->Attribute("value")) {
									XdevLTimeMilliseconds crt;
									crt.milliseconds = xstd::from_string<xdl_uint64>(child->Attribute("value"));
									m_buttons[static_cast<XdevLButtonId>(button)]->setClickResponseTime(crt);
								}
							} else {
								XDEVL_MODULE_ERROR("No button attribute in <ClickResponseTime> element found. Please specify a button id.\n");
								return RET_FAILED;
							}
						}
					}
				}
			}

			root = root->NextSiblingElement();
		}


		return RET_SUCCESS;
	}

	xdl_bool XdevLMouseImpl::isButtonSupported(const XdevLButtonId& buttonID) {
		return (buttonID == BUTTON_0 ||
		        buttonID == BUTTON_1 ||
		        buttonID == BUTTON_2 ||
		        buttonID == BUTTON_3 ||
		        buttonID == BUTTON_4);
	}

	xdl_int XdevLMouseImpl::notify(XdevLEvent& event) {
		if(isInitialized()) {

			XDEVL_ASSERT(m_buttons.size() > 0, "Seems no mouse buttons helper classes are allocated.");
			XDEVL_ASSERT(m_axes.size() > 0, "Seems no mouse axis helper classes are allocated.");

			//
			// Do this only if the user attached a window.
			//
			if(nullptr != m_window) {

				//
				// Because we allow normalized values we have to use the window size.
				// This might change in some cases and we need to check for it
				// and recalculate the normalization values.
				//
				if(event.type == XDEVL_WINDOW_EVENT) {
					//
					// Did the window got resized?
					//
					if(event.window.event == XDEVL_WINDOW_RESIZED) {
						if(m_window->getWindowID() == event.window.windowid) {
							m_windowSize.width = event.window.width;
							m_windowSize.height = event.window.height;
						}
					}
				} else if(event.type == KeyButtonPressed.getHashCode()) {
					m_modState = event.key.mod;
				} else if(event.type == KeyButtonReleased.getHashCode()) {
					m_modState = event.key.mod;
				} 
				else if(event.type == MouseButtonPressed.getHashCode()) {

					//
					// OK, it seems the user pressed a mouse button.
					//
					auto mouseButtonID = static_cast<XdevLButtonId>(event.button.button);

					// Do you support this button?
					if(isButtonSupported(mouseButtonID)) {
						if(!m_mouse_button_down) {
							m_buttons[mouseButtonID]->capturePressTime(event.common.timestamp);
						}

						m_mouse_button_down = xdl_true;
						m_buttons[mouseButtonID]->setState(xdl_true);

						XdevLDelegateButtonParameter parameter;
						parameter.id = covertIdxToXdevLButton(mouseButtonID);
						parameter.state = BUTTON_PRESSED;
						parameter.timestamp = event.button.timestamp;
						parameter.mod = m_modState;
						for(auto& delegate : m_buttonDelegates) {
							delegate(parameter);
						}

						for(auto delegateMultipMap :  m_buttonIdDelegates) {
							auto pp = delegateMultipMap.second.equal_range(mouseButtonID);
							XdevLDelegateButtonIdParameter parameterId;
							parameterId.state = BUTTON_PRESSED;
							parameterId.timestamp = event.key.timestamp;
							parameterId.mod = m_modState;
							for(auto it = pp.first; it != pp.second; ++it) {
								auto delegate = it->second;
								delegate(parameterId);
							}
						}
					} else {
						XDEVL_MODULEX_WARNING(XdevLMouse, "Unsupported mouse button pressed.\n")
					}

				} else if(event.type == MouseButtonReleased.getHashCode()) {

					//
					// OK, it seems the user released a mouse button.
					//
					auto mouseButtonID = static_cast<XdevLButtonId>(event.button.button);

					// Do you support this button?
					if(isButtonSupported(mouseButtonID)) {
						if(m_mouse_button_down) {
							m_buttons[mouseButtonID]->captureReleaseTime(event.common.timestamp);
						}

						m_mouse_button_down = xdl_false;
						m_buttons[mouseButtonID]->setState(xdl_false);

						XdevLDelegateButtonParameter parameter;
						parameter.id = covertIdxToXdevLButton(mouseButtonID);
						parameter.state = BUTTON_RELEASED;
						parameter.timestamp = event.button.timestamp;
						parameter.mod = m_modState;
						for(auto& delegate : m_buttonDelegates) {
							delegate(parameter);
						}

						for(auto delegateMultipMap :  m_buttonIdDelegates) {
							auto pp = delegateMultipMap.second.equal_range(mouseButtonID);
							XdevLDelegateButtonIdParameter parameterId;
							parameterId.state = BUTTON_RELEASED;
							parameterId.timestamp = event.key.timestamp;
							parameterId.mod = m_modState;
							for(auto it = pp.first; it != pp.second; ++it) {
								auto delegate = it->second;
								delegate(parameterId);
							}
						}
					}

				} else if(event.type == MouseMotion.getHashCode()) {

					//
					// OK, it seems the user moved the mouse.
					//
					xdl_int32 x = event.motion.x;
					xdl_int32 y = event.motion.y;
					xdl_int32 z = event.motion.z;

					m_mouse_old_x = m_mouse_curr_x;
					m_mouse_curr_x = x;
					m_mouse_old_y = m_mouse_curr_y;
					m_mouse_curr_y = y;
					m_mouse_old_z = m_mouse_curr_z;
					m_mouse_curr_z = z;
					m_mouse_moved = true;

					//
					// Why do I do x + 1? Because: 0 <= x < width. The same goes for y.
					//

					if(x >= m_windowSize.width - 1) {
						x = m_windowSize.width;
					}

					if(y >= m_windowSize.height - 1) {
						y = m_windowSize.height;
					}

					m_axes[AXIS_0]->setValue(m_mouse_curr_x/32768.0f);
					m_axes[AXIS_1]->setValue(m_mouse_curr_y/32768.0f);
					m_axes[AXIS_2]->setValue(m_mouse_curr_z/32768.0f);

					m_axes[AXIS_0]->setDeltaValue((xdl_float)(m_mouse_curr_x - m_mouse_old_x));
					m_axes[AXIS_1]->setDeltaValue((xdl_float)(m_mouse_curr_y - m_mouse_old_y));
					m_axes[AXIS_2]->setDeltaValue((xdl_float)(m_mouse_curr_z - m_mouse_old_z));

					//
					// Handle delegates that sends a axis id's and the value of the axis.
					//
					XdevLDelegateAxisParameter axisParameter1, axisParameter2, axisParameter3;
					axisParameter1.id = AXIS_0;
					axisParameter1.timestamp = event.motion.timestamp;
					axisParameter1.value = m_axes[AXIS_0]->getValue();
					axisParameter1.rel = m_axes[AXIS_0]->getDeltaValue();
					axisParameter1.mod = m_modState;

					axisParameter2.id = AXIS_1;
					axisParameter2.timestamp = event.motion.timestamp;
					axisParameter2.value = m_axes[AXIS_1]->getValue();
					axisParameter2.rel = m_axes[AXIS_1]->getDeltaValue();
					axisParameter2.mod = m_modState;

					axisParameter3.id = AXIS_2;
					axisParameter3.timestamp = event.motion.timestamp;
					axisParameter3.value = m_axes[AXIS_2]->getValue();
					axisParameter3.rel = m_axes[AXIS_2]->getDeltaValue();
					axisParameter3.mod = m_modState;


					for(auto& delegate : m_axisDelegates) {
						delegate(axisParameter1);
						delegate(axisParameter2);
						delegate(axisParameter3);
					}

					//
					// Handle delegates that registered only for one specific axis id.
					//
					auto pp1 = m_axisIdDelegates.equal_range(AXIS_0);
					XdevLDelegateAxisIdParameter axisIdParameter;
					axisIdParameter.timestamp = event.motion.timestamp;
					axisIdParameter.value = m_axes[AXIS_0]->getValue();
					axisIdParameter.mod = m_modState;

					for(auto it = pp1.first; it != pp1.second; ++it) {
						auto delegate = it->second;
						delegate(axisIdParameter);
					}

					axisIdParameter.value = m_axes[AXIS_1]->getValue();
					auto pp2 = m_axisIdDelegates.equal_range(AXIS_1);
					for(auto it = pp2.first; it != pp2.second; ++it) {
						auto delegate = it->second;
						delegate(axisIdParameter);
					}

					axisIdParameter.value = m_axes[AXIS_2]->getValue();
					auto pp3 = m_axisIdDelegates.equal_range(AXIS_2);
					for(auto it = pp3.first; it != pp3.second; ++it) {
						auto delegate = it->second;
						delegate(axisIdParameter);
					}


				} else if(event.type == MouseMotionRelative.getHashCode()) {

					m_axes[AXIS_0]->setDeltaValue(event.motion.xrel);
					m_axes[AXIS_1]->setDeltaValue(event.motion.yrel);
					m_axes[AXIS_2]->setDeltaValue(event.motion.zrel);

					//
					// Handle delegates that registered only for one specific axis id.
					//
					auto pp1 = m_axisIdDelegates.equal_range(AXIS_0);
					XdevLDelegateAxisIdParameter axisIdParameter;
					axisIdParameter.timestamp = event.motion.timestamp;
					axisIdParameter.value = m_axes[AXIS_0]->getValue();
					axisIdParameter.mod = m_modState;

					for(auto it = pp1.first; it != pp1.second; ++it) {
						auto delegate = it->second;
						delegate(axisIdParameter);
					}

					axisIdParameter.value = m_axes[AXIS_1]->getValue();
					auto pp2 = m_axisIdDelegates.equal_range(AXIS_1);
					for(auto it = pp2.first; it != pp2.second; ++it) {
						auto delegate = it->second;
						delegate(axisIdParameter);
					}

					axisIdParameter.value = m_axes[AXIS_2]->getValue();
					auto pp3 = m_axisIdDelegates.equal_range(AXIS_2);
					for(auto it = pp3 .first; it != pp3 .second; ++it) {
						auto delegate = it->second;
						delegate(axisIdParameter);
					}
				}
			}
		}

		return Super::notify(event);
	}

}
