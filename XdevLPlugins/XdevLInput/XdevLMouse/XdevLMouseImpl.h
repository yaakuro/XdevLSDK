/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MOUSE_IMPL_H
#define XDEVL_MOUSE_IMPL_H

#include <XdevLInput/XdevLInputImpl.h>
#include <XdevLInput/XdevLMouse/XdevLMouse.h>

namespace xdl {

	static const XdevLString mouse_pluginName {
		"XdevLMouse"
	};

	static const XdevLString mouseDescription {
		"Handles mouse related events."
	};

	static const std::vector<XdevLModuleName> mouse_moduleNames {
		XdevLModuleName("XdevLMouse")
	};

	const xdl_uint MOUSE_MAX_AXES = 3;

	class XdevLMouseImpl :  public XdevLInputImpl<XdevLMouse> {
		public:
			XdevLMouseImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLMouseImpl();

			//
			// XdevLListener methods.
			//
			xdl_int notify(XdevLEvent& event) override;

			//
			// XdevLModuleImpl methods.
			//
			xdl_int init() override;

			//
			// XdevLModuleAutoImpl methods.
			//
			xdl_int update() override;

			//
			// XdevLMouse methods.
			//
			xdl_int attach(XdevLWindow* window) override;
			xdl_float getValue(const xdl_uint axis) override;
			xdl_float getDeltaValue(const xdl_uint axis) override;

		private:

			xdl_int attach(XdevLWindow* window, const xdl_char* modulename);
			xdl_int readMouseInfo(TiXmlDocument& document);
			xdl_bool isButtonSupported(const XdevLButtonId& buttonID);

		protected:

			XdevLWindowSize m_windowSize;
			xdl_int m_mouse_curr_x;
			xdl_int m_mouse_curr_y;
			xdl_int m_mouse_curr_z;
			xdl_int m_mouse_old_x;
			xdl_int m_mouse_old_y;
			xdl_int m_mouse_old_z;

			xdl_bool m_mouse_button_down;
			xdl_bool m_mouse_moved;
			xdl_bool m_mouse_moved_old;
			xdl_uint16 m_modState;
	};

}

#endif
