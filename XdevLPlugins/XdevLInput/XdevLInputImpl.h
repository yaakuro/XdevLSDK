/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_INPUT_IMPL_H
#define XDEVL_INPUT_IMPL_H

#include <XdevLPluginImpl.h>
#include <XdevLInput/XdevLInputSystemUtils.h>

#include <vector>
#include <map>
#include <algorithm>

// TODO get/setClickResponseTime is converting between uint64 and double which I need to fix.

namespace xdl {

	class XdevLWindow;

	template<typename T>
	class XdevLInputImpl :  public XdevLModuleAutoImpl<T> {
		public:
			using Super = XdevLInputImpl;
			using ButtonIdDelegateType = std::multimap<XdevLButtonId, XdevLButtonIdDelegateType>;

			XdevLInputImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual	~XdevLInputImpl();

			//
			// XdevLListener methods.
			//
			xdl_int notify(XdevLEvent& event) override;

			//
			// XdevLModuleImpl methods.
			//
			xdl_int init() override;
			xdl_int shutdown() override;

			//
			// XdevLInputsystem methods.
			//
			xdl_int registerDelegate(const XdevLString& id, const XdevLButtonIdDelegateType& delegate, const XdevLString& group = xdl::XdevLString("Global")) override;
			xdl_int registerDelegate(const XdevLString& id, const XdevLAxisIdDelegateType& delegate, const XdevLString& group = xdl::XdevLString("Global")) override;
			xdl_int registerDelegate(const XdevLButtonDelegateType& delegate, const XdevLString& group = xdl::XdevLString("Global")) override;
			xdl_int registerDelegate(const XdevLAxisDelegateType& delegate, const XdevLString& group = xdl::XdevLString("Global")) override;
			xdl_int unregisterDelegate(const XdevLButtonDelegateType& delegate, const XdevLString& group = xdl::XdevLString("Global")) override;
			xdl_int unregisterDelegate(const XdevLButtonIdDelegateType& delegate, const XdevLString& group = xdl::XdevLString("Global")) override;
			xdl_int unregisterDelegate(const XdevLString& group = xdl::XdevLString("Global")) override;
			void unregisterAllDelegates() override;

			xdl_uint getNumButtons() const override;
			xdl_int getButton(const xdl_uint idx, XdevLButton** button) override;
			xdl_uint getNumAxis() const override;
			xdl_int getAxis(const xdl_uint idx, XdevLAxis** axis) const override;
			xdl_uint getNumPOV() const override;
			xdl_int getPOV(const xdl_uint idx, XdevLPOV** pov) const override;
			void setClickResponseTimeForAll(const XdevLTimeMilliseconds& crt) override;
			void setClickResponseTime(const xdl_uint key, const XdevLTimeMilliseconds& crt) override;
			XdevLTimeMilliseconds getClickResponseTime(const xdl_uint key) override;

			void setAxisRangeMinMax(const XdevLAxisId& axis, xdl_float min, xdl_float max) override;
			void setAxisRangeMin(const XdevLAxisId& axis, xdl_float min) override;
			void seAxisRangeMax(const XdevLAxisId& axis, xdl_float max) override;
			void getAxisRangeMinMax(const XdevLAxisId& axis, xdl_float* min, xdl_float* max) override;
			xdl_float getAxisRangeMin(const XdevLAxisId& axis) const override;
			xdl_float getAxisRangeMax(const XdevLAxisId& axis) const override;

			//
			// XdevLKeyboard methods
			//
			xdl_int attach(XdevLWindow* window) override;
			xdl_bool getPressed(const xdl_uint key) override;
			xdl_bool getClicked(const xdl_uint key) override;

		protected:

			xdl_bool isAttached();
			xdl_bool isInitialized();
			void setAttached(xdl_bool state);
			xdl_int reset();

			XDEVL_INLINE std::vector<XdevLAxisImpl*>& getAxes() const;
			XDEVL_INLINE std::map<XdevLButtonId, XdevLButtonImpl*> getButtons() const;

		protected:

			XdevLWindow* m_window;
			xdl_bool m_attached;
			xdl_bool m_initialized;
			thread::Mutex m_mutex;
			std::vector<XdevLButtonDelegateType> m_buttonDelegates;
			std::vector<XdevLAxisDelegateType> m_axisDelegates;

			std::map<xdl::XdevLString, ButtonIdDelegateType> m_buttonIdDelegates;
			std::multimap<XdevLAxisId, XdevLAxisIdDelegateType> m_axisIdDelegates;
			std::vector<XdevLAxisImpl*> m_axes;
			std::map<XdevLButtonId, XdevLButtonImpl*> m_buttons;

	};
}

#include "XdevLInputImpl.cpp"

#endif
