/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLJoystickImpl.h"

xdl::XdevLModuleDescriptor joystickModuleDesc {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::joystick_moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::joystick_description,
	XDEVLJOYSTICK_MODULE_MAJOR_VERSION,
	XDEVLJOYSTICK_MODULE_MINOR_VERSION,
	XDEVLJOYSTICK_MODULE_PATCH_VERSION
};

xdl::XdevLPluginDescriptor m_joystickPluginDescriptor {
	xdl::joystick_pluginName,
	xdl::joystick_moduleNames,
	XDEVLJOYSTICK_MAJOR_VERSION,
	XDEVLJOYSTICK_MINOR_VERSION,
	XDEVLJOYSTICK_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(m_joystickPluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLJoystickImpl, joystickModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLJoystick, xdl::XdevLJoystickImpl, joystickModuleDesc)

namespace xdl {

	static const XdevLID JoystickButtonPressed("XDEVL_JOYSTICK_BUTTON_PRESSED");
	static const XdevLID JoystickButtonReleased("XDEVL_JOYSTICK_BUTTON_RELEASED");
	static const XdevLID JoystickMotion("XDEVL_JOYSTICK_MOTION");
	static const XdevLID KeyButtonPressed("XDEVL_BUTTON_PRESSED");
	static const XdevLID KeyButtonReleased("XDEVL_BUTTON_RELEASED");

	XdevLJoystickImpl::XdevLJoystickImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLInputImpl<XdevLJoystick>(parameter, descriptor)
		, m_joystickServer(nullptr)
		, m_joy_button_down(false)
		, m_joy_moved(false)
		, m_joy_curr_x(0)
		, m_joy_curr_y(0)
		, m_joy_old_x(0)
		, m_joy_old_y(0)
		, m_relativeMode(xdl_false)
		, m_modState(KEY_MOD_NONE) {
		m_attached = xdl_true;
	}

	void XdevLJoystickImpl::initializeButtonsAndAxisArrays() {

		if(m_joystickDeviceInfo.numberOfAxes > 0) {
			// Delete previous one.
			for(auto& axis : m_axes) {
				delete axis;
			}

			// Reset and reserver new array of axis.
			m_axes.clear();
			m_axes.reserve(m_joystickDeviceInfo.numberOfAxes);
			m_axes.resize(m_joystickDeviceInfo.numberOfAxes);
			for(size_t a = 0; a < m_axes.size(); ++a) {
				m_axes[a] = new XdevLAxisImpl(&m_mutex);
				m_axes[a]->setMinMax(-1.0f, 1.0f);
			}
		}


		if(m_joystickDeviceInfo.numberOfButtons > 0) {
			// Delete previous one.
			for(auto& button : m_buttons) {
				delete button.second;
			}

			// Reset and reserver new array of buttons.
			m_buttons.clear();
			for(auto& button : m_buttons) {
				button.second = new XdevLButtonImpl(&m_mutex);
				button.second->setState(false);
			}
		}
	}

	xdl_int XdevLJoystickImpl::create(IPXdevLJoystickServer joystickServer, const XdevLJoystickId& joystickId) {

		if(nullptr == joystickServer) {
			return RET_FAILED;
		}

		m_joystickServer = joystickServer;

		return useJoystick(joystickId);

	}

	xdl_int XdevLJoystickImpl::useJoystick(const XdevLJoystickId& joystickId) {
		XDEVL_ASSERT(m_joystickServer, "Joystick not created. Use the create method first.");

		if(m_joystickServer->getJoystickInfo(joystickId, m_joystickDeviceInfo) == RET_FAILED) {
			return RET_FAILED;
		}

		initializeButtonsAndAxisArrays();

		return RET_SUCCESS;
	}


	xdl_int XdevLJoystickImpl::init() {
		Super::init();

		auto coreParameter = getMediator()->getCoreParameter();
		if(coreParameter.xmlBuffer.size() > 0) {
			TiXmlDocument xmlDocument;
			if(!xmlDocument.Parse((xdl_char*)coreParameter.xmlBuffer.data())) {
				XDEVL_MODULE_ERROR("Could not parse xml buffer.\n ");
				return RET_FAILED;
			}
		}



		return RET_SUCCESS;
	}

	xdl_int XdevLJoystickImpl::notify(XdevLEvent& event) {

		if(event.type == KeyButtonPressed.getHashCode()) {
			m_modState = event.key.mod;
		} else if(event.type == KeyButtonReleased.getHashCode()) {
			m_modState = event.key.mod;
		} else if(event.type == JoystickButtonPressed.getHashCode()) {
			auto buttonID = static_cast<XdevLButtonId>(event.jbutton.buttonid);
			if(!m_joy_button_down) {
				m_buttons[buttonID]->capturePressTime(event.common.timestamp);
			}
			m_joy_button_down = xdl_true;
			m_buttons[buttonID]->setState(xdl_true);

			//
			// Handle delegates that sends a button id's and the state of the button.
			//
			XdevLDelegateButtonParameter parameter;
			parameter.id = covertIdxToXdevLButton(buttonID);
			parameter.state = BUTTON_PRESSED;
			parameter.timestamp = event.jbutton.timestamp;
			parameter.mod = m_modState;
			for(auto& delegate : m_buttonDelegates) {
				delegate(parameter);
			}

			//
			// Handle delegates that registered only for one specific button id.
			//
			for(auto delegateMultipMap :  m_buttonIdDelegates) {
				auto pp = delegateMultipMap.second.equal_range(static_cast<XdevLButtonId>(event.key.keycode));
				XdevLDelegateButtonIdParameter parameterId;
				parameterId.state = BUTTON_PRESSED;
				parameterId.timestamp = event.key.timestamp;
				parameterId.mod = m_modState;
				for(auto it = pp.first; it != pp.second; ++it) {
					auto delegate = it->second;
					delegate(parameterId);
				}
			}

		} else if(event.type ==  JoystickButtonReleased.getHashCode()) {
			auto buttonID = static_cast<XdevLButtonId>(event.jbutton.buttonid);

			if(m_joy_button_down)
				m_buttons[buttonID]->captureReleaseTime(event.common.timestamp);

			m_joy_button_down = xdl_false;
			m_buttons[buttonID]->setState(xdl_false);

			//
			// Handle delegates that sends a button id's and the state of the button.
			//
			XdevLDelegateButtonParameter parameter;
			parameter.id = covertIdxToXdevLButton(buttonID);
			parameter.state = BUTTON_RELEASED;
			parameter.timestamp = event.jbutton.timestamp;
			parameter.mod = m_modState;
			for(auto& delegate : m_buttonDelegates) {
				delegate(parameter);
			}

			//
			// Handle delegates that registered only for one specific button id.
			//
			for(auto delegateMultipMap :  m_buttonIdDelegates) {
				auto pp = delegateMultipMap.second.equal_range(static_cast<XdevLButtonId>(event.key.keycode));
				XdevLDelegateButtonIdParameter parameterId;
				parameterId.state = BUTTON_RELEASED;
				parameterId.timestamp = event.key.timestamp;
				parameterId.mod = m_modState;
				for(auto it = pp.first; it != pp.second; ++it) {
					auto delegate = it->second;
					delegate(parameterId);
				}
			}

		} else if(event.type == JoystickMotion.getHashCode()) {
			xdl_float value = ((xdl_float)event.jaxis.value)/32768.0f;

			m_joy_old_x = m_joy_curr_x;
			m_joy_curr_x = static_cast<xdl_int>(value);
			m_axes[event.jaxis.axisid]->setValue(value);
			m_joy_moved = true;

			//
			// Handle delegates that sends a axis id's and the value of the axis.
			//
			XdevLDelegateAxisParameter axisParameter;
			axisParameter.id = (XdevLAxisId)event.jaxis.axisid;
			axisParameter.timestamp = event.jaxis.timestamp;
			axisParameter.value = m_axes[event.jaxis.axisid]->getValue();
			for(auto& delegate : m_axisDelegates) {
				delegate(axisParameter);
			}

			//
			// Handle delegates that registered only for one specific axis id.
			//
			auto pp = m_axisIdDelegates.equal_range((XdevLAxisId)event.jaxis.axisid);
			XdevLDelegateAxisIdParameter axisIdParameter;
			axisIdParameter.timestamp = event.jaxis.timestamp;
			axisIdParameter.value = m_axes[event.jaxis.axisid]->getValue();
			for(auto it = pp.first; it != pp.second; ++it) {
				auto delegate = it->second;
				delegate(axisIdParameter);
			}

		}

		return XdevLModuleImpl<XdevLJoystick>::notify(event);
	}


	xdl_int XdevLJoystickImpl::update() {
		thread::XdevLScopeLock lock(m_mutex);

		m_joy_moved = false;

		return Super::update();
	}

	xdl_int XdevLJoystickImpl::readJoystickXmlInfoPre(TiXmlDocument& document, const xdl_char* moduleName) {
		TiXmlHandle docHandle(&document);
		TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toString().c_str()).FirstChildElement(moduleName).ToElement();
		if(!root) {
			XDEVL_MODULE_WARNING("<XdevLJoystick> section not found. Using default values for the device.\n");
			return RET_SUCCESS;
		}
		if(root->Attribute("id")) {
			XdevLID id(root->Attribute("id"));
			if(this->getID() != id) {
				return RET_SUCCESS;
			}
		} else {
			XDEVL_MODULE_ERROR("No 'id' attribute specified.");
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLJoystickImpl::readJoystickXmlInfo(TiXmlDocument& document, const xdl_char* moduleName) {
		TiXmlHandle docHandle(&document);
		TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toUTF8().c_str()).FirstChildElement(moduleName).ToElement();
		if(!root) {
			XDEVL_MODULE_WARNING("<XdevLJoystick> section not found. Using default values for the device.\n");
			return RET_SUCCESS;
		}
		if(root->Attribute("id")) {
			XdevLID id(root->Attribute("id"));
			if(this->getID() != id) {
				return RET_SUCCESS;
			}
		} else {
			XDEVL_MODULE_ERROR("No 'id' attribute specified.");
			return RET_FAILED;
		}

		// If there is a crt attribute, set this value for all buttons.
		if(root->Attribute("crt")) {
			auto crt = xstd::from_string<xdl_uint64>(root->Attribute("crt"));
			for(auto& button : m_buttons)
				button.second->setClickResponseTime(crt);
		}

		// If there is a min attribute, set this value for all axes.
		if(root->Attribute("min")) {
			xdl_float min = xstd::from_string<xdl_float>(root->Attribute("min"));
			for(size_t a = 0; a < m_axes.size(); ++a)
				m_axes[a]->setMin(min);
		}

		// If there is a max attribute, set this value for all axes.
		if(root->Attribute("max")) {
			xdl_float max = xstd::from_string<xdl_float>(root->Attribute("max"));
			for(size_t a = 0; a < m_axes.size(); ++a)
				m_axes[a]->setMax(max);
		}

		TiXmlElement* child = 0;
		for(child = root->FirstChildElement(); child; child = child->NextSiblingElement()) {
			if(child->ValueTStr() == "Range") {
				xdl_int axis = 0;
				if(child->Attribute("axis")) {
					axis = getAxisId(XdevLString(child->Attribute("axis")));
					float value = 0.0f;
					if(child->Attribute("min")) {
						value = xstd::from_string<float>(child->Attribute("min"));
						if(NULL != m_axes[axis])
							m_axes[axis]->setMin(value);
						else {
							XDEVL_MODULE_WARNING("Axis: " << axis << " 'min' value can not be set.\n");
						}
					}
					if(child->Attribute("max")) {
						value = xstd::from_string<float>(child->Attribute("max"));
						if(NULL != m_axes[axis])
							m_axes[axis]->setMax(value);
						else {
							XDEVL_MODULE_WARNING("Axis: " << axis << " 'max' value can not be set.\n");
						}

					}
				} else {
					XDEVL_MODULE_ERROR("No axis attribute in <Range> element found. Please specify an axis id.\n");
					return RET_FAILED;
				}
			}

			if(child->ValueTStr() == "ClickResponseTime") {
				xdl_int button = 0;
				if(child->Attribute("button")) {
					button = getButtonId(XdevLString(child->Attribute("button")));
					if(child->Attribute("value"))
						m_buttons[static_cast<XdevLButtonId>(button)]->setClickResponseTime(xstd::from_string<xdl_uint64>(child->Attribute("value")));
				} else {
					XDEVL_MODULE_ERROR("No button attribute in <ClickResponseTime> element found. Please specify a button id.\n");
					return RET_FAILED;
				}
			}
		}

		return RET_SUCCESS;
	}

	xdl_float XdevLJoystickImpl::getValue(const xdl_uint axis) {
		return m_axes[axis]->getValue();
	}


}
