/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_JOYSTICK_IMPL_H
#define XDEVL_JOYSTICK_IMPL_H

#include <XdevLInput/XdevLInputImpl.h>
#include <XdevLInput/XdevLJoystick/XdevLJoystick.h>

namespace xdl {

	static const XdevLString joystick_pluginName {
		"XdevLJoystick"
	};

	static const XdevLString joystick_description {
		"Class that helps you to handle Controller events with ease."
	};

	static const std::vector<XdevLModuleName> joystick_moduleNames {
		XdevLModuleName("XdevLJoystick")
	};

	//
	// Joystick ID's for the event system.
	//

	class XdevLJoystickImpl : public XdevLInputImpl<XdevLJoystick> {
		public:
			XdevLJoystickImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

			virtual ~XdevLJoystickImpl() {}

			xdl_int init() override;

			xdl_int notify(XdevLEvent& event) override;

			xdl_int create(IPXdevLJoystickServer joystickServer, const XdevLJoystickId& joystickId = XdevLJoystickId::JOYSTICK_DEFAULT) override;
			xdl_int useJoystick(const XdevLJoystickId& joystickId) override;

			//
			// XdevLJoystick functions
			//
			xdl_float getValue(const xdl_uint button) override;

			xdl_int update() override final;

		protected:

			xdl_int readJoystickXmlInfoPre(TiXmlDocument& document, const xdl_char* moduleName);
			xdl_int readJoystickXmlInfo(TiXmlDocument& document, const xdl_char* moduleName);
			void initializeButtonsAndAxisArrays();

		protected:

			IPXdevLJoystickServer m_joystickServer;
			XdevLJoystickDeviceInfo m_joystickDeviceInfo;

			xdl_bool m_joy_button_down;
			xdl_bool m_joy_moved;
			xdl_int m_joy_curr_x;
			xdl_int m_joy_curr_y;
			xdl_int m_joy_old_x;
			xdl_int m_joy_old_y;
			xdl_bool m_relativeMode;
			xdl_uint16 m_modState;
	};

}


#endif
