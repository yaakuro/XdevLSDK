/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

namespace xdl {

	template<typename T>
	XdevLInputImpl<T>::XdevLInputImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleAutoImpl<T>(parameter, descriptor),
		m_window(nullptr),
		m_attached(xdl_false),
		m_initialized(xdl_false) {
	}

	template<typename T>
	XdevLInputImpl<T>::~XdevLInputImpl() {

	}
	
	template<typename T>
	xdl_bool XdevLInputImpl<T>::isInitialized() {
		return m_initialized;
	}

	template<typename T>
	std::vector<XdevLAxisImpl*>& XdevLInputImpl<T>::getAxes() const {
		return m_axes;
	}

	template<typename T>
	std::map<XdevLButtonId, XdevLButtonImpl*> XdevLInputImpl<T>::getButtons() const {
		return m_buttons;
	}

	template<typename T>
	xdl_uint XdevLInputImpl<T>::getNumButtons() const {
		return static_cast<xdl_uint>(m_buttons.size());
	}

	template<typename T>
	xdl_uint XdevLInputImpl<T>::getNumAxis() const {
		return static_cast<xdl_int>(m_axes.size());
	}

	template<typename T>
	xdl_uint XdevLInputImpl<T>::getNumPOV() const {
		return 0;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::getAxis(const xdl_uint idx, XdevLAxis** axis) const {
		XDEVL_ASSERT(m_attached, "Mouse Device not attached");

		if(m_axes.size() == 0) {
			*axis = NULL;
			return RET_FAILED;
		}
		if((idx >= m_axes.size())) {
			*axis = NULL;
			return RET_FAILED;
		}
		*axis = m_axes[idx];
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::getPOV(const xdl_uint, XdevLPOV** pov) const {
		*pov = nullptr;
		return RET_FAILED;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::getButton(const xdl_uint idx, XdevLButton** button)  {
		XDEVL_ASSERT(m_attached, "Instance not attached!");

		// If the key mapping does not exists, just create one.
		XdevLButtonImpl* tmp = m_buttons[static_cast<XdevLButtonId>(idx)];
		if(tmp == nullptr) {
			m_buttons[static_cast<XdevLButtonId>(idx)] =  new XdevLButtonImpl(&m_mutex);
			tmp = m_buttons[static_cast<XdevLButtonId>(idx)];
		}
		*button = tmp;
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_bool XdevLInputImpl<T>::getPressed(const xdl_uint idx) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
		return m_buttons[static_cast<XdevLButtonId>(idx)]->getPressed();
	}

	template<typename T>
	xdl_bool XdevLInputImpl<T>::getClicked(const xdl_uint idx) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
		return m_buttons[static_cast<XdevLButtonId>(idx)]->getClicked();
	}

	template<typename T>
	void XdevLInputImpl<T>::setClickResponseTimeForAll(const XdevLTimeMilliseconds& crt) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
		for(auto button : m_buttons) {
			button.second->setClickResponseTime(crt);
		}
	}

	template<typename T>
	void XdevLInputImpl<T>::setClickResponseTime(const xdl_uint idx, const XdevLTimeMilliseconds& crt) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
		m_buttons[static_cast<XdevLButtonId>(idx)]->setClickResponseTime(crt);
	}

	template<typename T>
	XdevLTimeMilliseconds XdevLInputImpl<T>::getClickResponseTime(const xdl_uint idx) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
		return m_buttons[static_cast<XdevLButtonId>(idx)]->getClickResponseTime();
	}

	template<typename T>
	void XdevLInputImpl<T>::setAxisRangeMinMax(const XdevLAxisId& axis, xdl_float min, xdl_float max) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
	}

	template<typename T>
	void XdevLInputImpl<T>::setAxisRangeMin(const XdevLAxisId& axis, xdl_float min) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
	}

	template<typename T>
	void XdevLInputImpl<T>::seAxisRangeMax(const XdevLAxisId& axis, xdl_float max) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
	}

	template<typename T>
	void XdevLInputImpl<T>::getAxisRangeMinMax(const XdevLAxisId& axis, xdl_float* min, xdl_float* max) {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
	}

	template<typename T>
	xdl_float XdevLInputImpl<T>::getAxisRangeMin(const XdevLAxisId& axis) const {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
		return 0.0f;
	}

	template<typename T>
	xdl_float XdevLInputImpl<T>::getAxisRangeMax(const XdevLAxisId& axis) const {
		XDEVL_ASSERT(m_attached, "Instance not attached!");
		return 0.0f;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::registerDelegate(const XdevLString& id, const XdevLButtonIdDelegateType& delegate, const XdevLString& group) {
		auto& delegatesMap = m_buttonIdDelegates[group];

		XdevLButtonId idType = getButtonId(id);
		auto tmp = delegatesMap.find(idType);
		if(delegatesMap.end() != tmp) {
			return RET_FAILED;
		}

		delegatesMap.insert(ButtonIdDelegateType::value_type(static_cast<XdevLButtonId>(idType), delegate));
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::registerDelegate(const XdevLButtonDelegateType& delegate, const XdevLString& group) {
		auto tmp = std::find(m_buttonDelegates.begin(), m_buttonDelegates.end(), delegate);
		if(m_buttonDelegates.end() != tmp) {
			return RET_FAILED;
		}
		m_buttonDelegates.push_back(delegate);
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::unregisterDelegate(const XdevLButtonDelegateType& delegate, const XdevLString& group) {
		auto tmp = std::find(m_buttonDelegates.begin(), m_buttonDelegates.end(), delegate);
		if(m_buttonDelegates.end() == tmp) {
			return RET_FAILED;
		}
		m_buttonDelegates.erase(tmp);
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::registerDelegate(const XdevLString& id, const XdevLAxisIdDelegateType& delegate, const XdevLString& group) {
		XdevLAxisId idType = getAxisId(id);
		auto tmp = m_axisIdDelegates.find(idType);
		if(m_axisIdDelegates.end() != tmp) {
			return RET_FAILED;
		}
		m_axisIdDelegates.insert(std::pair<const XdevLAxisId, const XdevLAxisIdDelegateType>(idType, delegate));
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::registerDelegate(const XdevLAxisDelegateType& delegate, const XdevLString& group) {
		auto tmp = std::find(m_axisDelegates.begin(), m_axisDelegates.end(), delegate);
		if(m_axisDelegates.end() != tmp) {
			return RET_FAILED;
		}
		m_axisDelegates.push_back(delegate);
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::unregisterDelegate(const XdevLButtonIdDelegateType& delegate, const XdevLString& group) {
		auto tmp = m_buttonIdDelegates.find(group);
		if(m_buttonIdDelegates.end() == tmp) {
			return RET_FAILED;
		}

		for(auto dlg : tmp->second) {
			if(delegate == dlg.second) {
				tmp->second.erase(dlg.first);
				break;
			}
		}
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::unregisterDelegate(const XdevLString& group) {
		auto tmp = m_buttonIdDelegates.find(group);
		if(m_buttonIdDelegates.end() == tmp) {
			return RET_FAILED;
		}
		m_buttonIdDelegates.erase(tmp);
		return RET_SUCCESS;
	}

	template<typename T>
	void XdevLInputImpl<T>::unregisterAllDelegates() {
		m_buttonDelegates.clear();
		m_buttonIdDelegates.clear();
	}

	template<typename T>
	xdl_bool XdevLInputImpl<T>::isAttached() {
		thread::XdevLScopeLock lock(m_mutex);

		xdl_bool tmp = m_attached;

		return tmp;
	}

	template<typename T>
	void XdevLInputImpl<T>::setAttached(xdl_bool state) {
		thread::XdevLScopeLock lock(m_mutex);

		m_attached = state;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::init() {
		m_initialized = xdl_true;
		reset();
		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::shutdown() {

		for(auto& button : m_buttons) {
			delete button.second;
		}

		for(auto& axis : m_axes) {
			delete axis;
		}

		m_buttons.clear();
		m_axes.clear();

		setAttached(xdl_false);
		m_initialized = xdl_false;

		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::reset() {

		for(auto& button : m_buttons) {
			button.second->setState(xdl_false);
		}

		for(size_t a = 0; a < m_axes.size(); ++a)
			m_axes[a]->setValue(0.0f);

		return RET_SUCCESS;
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::notify(XdevLEvent& event) {
		return XdevLModuleAutoImpl<T>::notify(event);
	}

	template<typename T>
	xdl_int XdevLInputImpl<T>::attach(XdevLWindow* window) {
		XDEVL_ASSERT(nullptr != window, "Invalid IPXdevLWindow specified.");

		m_window = window;

		if(!isInitialized()) {
			if(init() != RET_SUCCESS) {
				return RET_FAILED;
			}
		}

		setAttached(true);

		return RET_SUCCESS;
	}

}
