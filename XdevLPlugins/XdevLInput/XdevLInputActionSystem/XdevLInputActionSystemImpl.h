/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_INPUT_ACTION_SYSTEM_IMPL_H
#define XDEVL_INPUT_ACTION_SYSTEM_IMPL_H

#include <XdevLInput/XdevLMouse/XdevLMouse.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>
#include <XdevLInput/XdevLJoystick/XdevLJoystick.h>

#include "XdevLInputActionSystem.h"
#include <XdevLPluginImpl.h>

namespace xdl {

	static const XdevLString pluginName {
		"XdevLInputActionSystem"
	};

	static const XdevLString description {
		"Handles events in a better way."
	};

	static const std::vector<XdevLModuleName> moduleNames {
		XdevLModuleName("XdevLInputActionSystem")
	};

	struct XdevLActionInfo {
		XdevLActionDelegateType delegate;
		XdevLString actionName;
	};

	struct XdevLAxisActionInfo {
		XdevLAxisActionDelegateType delegate;
		XdevLString actionName;
	};

	class XdevLInputActionSystemImpl :  public XdevLModuleImpl<XdevLInputActionSystem> {
		public:
			XdevLInputActionSystemImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLInputActionSystemImpl();

			//
			// XdevLListener methods.
			//
			xdl_int notify(XdevLEvent& event) override final;

			//
			// XdevLModuleImpl methods.
			//
			xdl_int init() override final;

			//
			// XdevLInputActionSystem methods.
			//
			xdl_int create(const XdevLInputActionSystemParameter& parameter) override final;
			void registerButtonAction(const XdevLString& actionName, XdevLButtonId button, XdevLActionDelegateType delegate) override final;
			void registerAxisAction(const XdevLString& actionName, XdevLAxisId axis, XdevLAxisActionDelegateType delegate) override final;
			XdevLKeyboard* getKeyboard() override final {
				return m_keyboard;
			}

			XdevLMouse* getMouse() override final {
				return m_mouse;
			}

			XdevLJoystick* getJoystick() override final {
				return m_joystick;
			}
		private:

			xdl_int readInfo(TiXmlDocument& document);

			void handleButtonEvents(const XdevLDelegateButtonParameter& event);
			void handleAxisEvents(const XdevLDelegateAxisParameter& event);

		private:

			IPXdevLMouse m_mouse;
			IPXdevLKeyboard m_keyboard;
			IPXdevLJoystick m_joystick;
			IPXdevLJoystickServer m_joystickServer;
			std::map<XdevLButtonId, XdevLActionInfo> m_actionDelegates;
			std::map<XdevLAxisId, XdevLAxisActionInfo> m_axisActionDelegates;
	};

}

#endif
