/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_INPUT_ACTION_SYSTEM_H
#define XDEVL_INPUT_ACTION_SYSTEM_H

#include <XdevLModule.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLInput/XdevLInputSystem.h>

namespace xdl {

	/**
	 * @class XdevLDelegateActionParameter
	 */
	struct XdevLDelegateActionParameter {
		XdevLButtonState state; // Either BUTTON_PRESSED or BUTTON_RELEASED
		xdl_uint16 mod; // Binary AND combination of the KEY_MOD_XXX states.
	};

	/**
	 * @class XdevLDelegateAxisActionParameter
	 */
	struct XdevLDelegateAxisActionParameter {
		xdl_float value; // Absolute mouse movement value.
		xdl_float rel; // Relative mouse movement value.
		xdl_uint16 mod; // Binary AND combination of the KEY_MOD_XXX states.
	};

	struct XdevLInputActionSystemParameter {
		XdevLInputActionSystemParameter()
			: window(nullptr) {}

		XdevLID mouseModuleID;
		XdevLID keyboardModuleID;
		XdevLID joystickModuleID;
		IPXdevLWindow window;
	};

	using XdevLActionDelegateType = XdevLDelegate<void, const XdevLDelegateActionParameter&>;
	using XdevLAxisActionDelegateType = XdevLDelegate<void, const XdevLDelegateAxisActionParameter&>;

	class XdevLInputActionSystem : public XdevLModule {
		public:
			virtual xdl_int create(const XdevLInputActionSystemParameter& parameter) = 0;
			virtual void registerButtonAction(const XdevLString& actionName, XdevLButtonId button, XdevLActionDelegateType delegate) = 0;
			virtual void registerAxisAction(const XdevLString& actionName, XdevLAxisId axis, XdevLAxisActionDelegateType delegate) = 0;
			virtual XdevLKeyboard* getKeyboard() = 0;
			virtual XdevLMouse* getMouse() = 0;
			virtual XdevLJoystick* getJoystick() = 0;
	};

	using IXdevLInputActionSystem = XdevLInputActionSystem;
	using IPXdevLInputActionSystem = XdevLInputActionSystem*;

	XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLInputActionSystem)
}

#endif
