/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLInputActionSystemImpl.h"
#include <XdevL.h>

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::pluginName,
	xdl::moduleNames,
	XDEVL_INPUT_ACTION_SYSTEM_MAJOR_VERSION,
	XDEVL_INPUT_ACTION_SYSTEM_MINOR_VERSION,
	XDEVL_INPUT_ACTION_SYSTEM_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::description,
	XDEVL_INPUT_ACTION_SYSTEM_MODULE_MAJOR_VERSION,
	XDEVL_INPUT_ACTION_SYSTEM_MODULE_MINOR_VERSION,
	XDEVL_INPUT_ACTION_SYSTEM_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLInputActionSystemImpl, moduleDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLInputActionSystem, xdl::XdevLInputActionSystemImpl, moduleDescriptor)

namespace xdl {

	XdevLInputActionSystemImpl::XdevLInputActionSystemImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLModuleImpl<XdevLInputActionSystem>(parameter, descriptor)
		, m_mouse(nullptr)
		, m_keyboard(nullptr)
		, m_joystick(nullptr) 
		, m_joystickServer(nullptr) {
	}

	XdevLInputActionSystemImpl::~XdevLInputActionSystemImpl() {
	}

	xdl_int XdevLInputActionSystemImpl::create(const XdevLInputActionSystemParameter& parameter) {
		XdevLButtonDelegateType buttonDelegate = XdevLButtonDelegateType::Create<XdevLInputActionSystemImpl, &XdevLInputActionSystemImpl::handleButtonEvents>(this);
		XdevLAxisDelegateType axisDelegate = XdevLAxisDelegateType::Create<XdevLInputActionSystemImpl, &XdevLInputActionSystemImpl::handleAxisEvents>(this);

		// -------------------------------------------------------------------------
		// Initialize the mouse system.
		//
		if(XdevLID() != parameter.mouseModuleID) {
			m_mouse = static_cast<IPXdevLMouse>(getMediator()->getModule(parameter.mouseModuleID));
			if(nullptr == m_mouse) {
				XDEVL_MODULEX_ERROR(XdevLInputActionSystem, "Couldn't find module: " << parameter.mouseModuleID << std::endl);
				return RET_FAILED;
			}
		} else {
			XdevLModuleName moduleName(TEXT("XdevLMouse"));
			m_mouse = static_cast<IPXdevLMouse>(getMediator()->createModule(moduleName, XdevLID("XdevLInputActionSystemMouse")));
			if(nullptr == m_mouse) {
				XDEVL_MODULEX_ERROR(XdevLInputActionSystem, "Couldn't find module: " << moduleName << std::endl);
				return RET_FAILED;
			}
			XDEVL_ASSERT(parameter.window != nullptr, "Invalid window parameter.\n");
			m_mouse->attach(parameter.window);
		}
		m_mouse->registerDelegate(buttonDelegate);
		m_mouse->registerDelegate(axisDelegate);
		// -------------------------------------------------------------------------

		// -------------------------------------------------------------------------
		// Initialize the keybaord system.
		//
		if(XdevLID() != parameter.keyboardModuleID) {
			m_keyboard = static_cast<IPXdevLKeyboard>(getMediator()->getModule(parameter.keyboardModuleID));
			if(nullptr == m_keyboard) {
				XDEVL_MODULEX_ERROR(XdevLInputActionSystem, "Couldn't find module: " << parameter.keyboardModuleID << std::endl);
				return RET_FAILED;
			}
		} else {
			XdevLModuleName moduleName(TEXT("XdevLKeyboard"));
			m_keyboard = static_cast<IPXdevLKeyboard>(getMediator()->createModule(moduleName, XdevLID("XdevLInputActionSystemKeyboard")));
			if(nullptr == m_keyboard) {
				XDEVL_MODULEX_ERROR(XdevLInputActionSystem, "Couldn't find module: " << moduleName << std::endl);
				return RET_FAILED;
			}
			XDEVL_ASSERT(parameter.window != nullptr, "Invalid window parameter.\n");
			m_keyboard->attach(parameter.window);
		}
		m_keyboard->registerDelegate(buttonDelegate);
		m_keyboard->registerDelegate(axisDelegate);
		// -------------------------------------------------------------------------

		// -------------------------------------------------------------------------
		// Initialize the joystick system.
		//
		if(XdevLID() != parameter.joystickModuleID) {
			m_joystick = static_cast<IPXdevLJoystick>(getMediator()->getModule(parameter.keyboardModuleID));
			if(nullptr == m_joystick) {
				XDEVL_MODULEX_ERROR(XdevLInputActionSystem, "Couldn't find module: " << parameter.keyboardModuleID << std::endl);
				return RET_FAILED;
			}
		} else {
			XdevLModuleName moduleName(TEXT("XdevLJoystick"));
			m_joystick = static_cast<IPXdevLJoystick>(getMediator()->createModule(moduleName, XdevLID("XdevLInputActionSystemJoystick")));
			if(nullptr == m_joystick) {
				XDEVL_MODULEX_ERROR(XdevLInputActionSystem, "Couldn't find module: " << moduleName << std::endl);
				return RET_FAILED;
			}
			if(m_joystick) {
				if(m_joystickServer && m_joystickServer->getNumJoysticks() > 0) {
					m_joystick->create(m_joystickServer, xdl::XdevLJoystickId::JOYSTICK_DEFAULT);
				}
			}

			XDEVL_ASSERT(parameter.window != nullptr, "Invalid window parameter.\n");
			m_joystick->attach(parameter.window);
		}
		m_joystick->registerDelegate(buttonDelegate);
		m_joystick->registerDelegate(axisDelegate);
		// -------------------------------------------------------------------------


		return RET_SUCCESS;
	}

	xdl_int XdevLInputActionSystemImpl::init() {
		getMediator()->plug(XdevLPluginName("XdevLKeyboard"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION));
		getMediator()->plug(XdevLPluginName("XdevLMouse"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION));
		getMediator()->plug(XdevLPluginName("XdevLJoystick"), XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, XDEVL_PATCH_VERSION));

		return Super::init();
	}

	xdl_int XdevLInputActionSystemImpl::readInfo(TiXmlDocument& document) {
		TiXmlHandle docHandle(&document);
		TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toString().c_str()).FirstChildElement(moduleNames[0].toString().c_str()).ToElement();
		if(!root) {
			XDEVL_MODULE_WARNING("<XdevLInputActionSystem> section not found. Using default values for the device.\n");
			return RET_SUCCESS;
		}

		while(root != NULL) {
			if(root->Attribute("id")) {
				XdevLID id(root->Attribute("id"));
				if(this->getID() == id) {
				}
			}

			root = root->NextSiblingElement();
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLInputActionSystemImpl::notify(XdevLEvent& event) {
		return Super::notify(event);
	}

	void XdevLInputActionSystemImpl::registerButtonAction(const XdevLString& actionName, XdevLButtonId button, XdevLActionDelegateType delegate) {
		auto tmp = m_actionDelegates.find(button);
		if(tmp != m_actionDelegates.end()) {
			return;
		}

		XdevLActionInfo actionInfo {};
		actionInfo.delegate = delegate;
		actionInfo.actionName = actionName;

		m_actionDelegates[button] = actionInfo;
	}

	void XdevLInputActionSystemImpl::registerAxisAction(const XdevLString& actionName, XdevLAxisId axis, XdevLAxisActionDelegateType delegate) {
		auto tmp = m_axisActionDelegates.find(axis);
		if(tmp != m_axisActionDelegates.end()) {
			return;
		}

		XdevLAxisActionInfo actionInfo {};
		actionInfo.delegate = delegate;
		actionInfo.actionName = actionName;

		m_axisActionDelegates[axis] = actionInfo;
	}

	void XdevLInputActionSystemImpl::handleButtonEvents(const XdevLDelegateButtonParameter& event) {
		auto actionInfo = m_actionDelegates.find(event.id);
		if(actionInfo != m_actionDelegates.end()) {
			XdevLDelegateActionParameter actionParameter {};
			actionParameter.state = event.state;
			actionParameter.mod = event.mod;

			actionInfo->second.delegate(actionParameter);
		}
	}

	void XdevLInputActionSystemImpl::handleAxisEvents(const XdevLDelegateAxisParameter& event) {
		auto actionInfo = m_axisActionDelegates.find(event.id);
		if(actionInfo != m_axisActionDelegates.end()) {
			XdevLDelegateAxisActionParameter actionParameter {};
			actionParameter.value = event.value;
			actionParameter.rel = event.rel;
			actionParameter.mod = event.mod;

			actionInfo->second.delegate(actionParameter);
		}
	}
}
