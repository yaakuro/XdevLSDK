/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLKeyboardImpl.h"

xdl::XdevLPluginDescriptor keyboardPluginDescriptor {
	xdl::keyboard_pluginName,
	xdl::keyboard_moduleNames,
	XDEVLKEYBOARD_MAJOR_VERSION,
	XDEVLKEYBOARD_MINOR_VERSION,
	XDEVLKEYBOARD_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleKeyboardDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::keyboard_moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::keyboardDescription,
	XDEVLKEYBOARD_MODULE_MAJOR_VERSION,
	XDEVLKEYBOARD_MODULE_MINOR_VERSION,
	XDEVLKEYBOARD_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(keyboardPluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLKeyboardImpl, moduleKeyboardDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLKeyboard, xdl::XdevLKeyboardImpl, moduleKeyboardDescriptor)

namespace xdl {

	static const XdevLID KeyButtonPressed("XDEVL_BUTTON_PRESSED");
	static const XdevLID KeyButtonReleased("XDEVL_BUTTON_RELEASED");

	XdevLKeyboardImpl::XdevLKeyboardImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLInputImpl<XdevLKeyboard>(parameter, descriptor)
		, m_keyPressed(xdl_false)
		, m_modState(KEY_MOD_NONE) {

	}

	XdevLKeyboardImpl::~XdevLKeyboardImpl() {
		for(auto keybutton : m_buttons) {
			delete keybutton.second;
		}
		m_keyButtonId.clear();
	}


	xdl_int XdevLKeyboardImpl::init() {
		initKeyMap();
		return Super::init();
	}

	xdl_int XdevLKeyboardImpl::notify(XdevLEvent& event) {
		if(isInitialized()) {
			//
			// Did the user press a keyboard button?
			//
			if(event.type == KeyButtonPressed.getHashCode()) {
				m_modState = event.key.mod;

				XdevLButtonImpl* button = m_buttons[static_cast<XdevLButtonId>(event.key.keycode)];
				if(button == nullptr) {
					m_buttons[static_cast<XdevLButtonId>(event.key.keycode)] = new XdevLButtonImpl(&m_mutex);
					button = m_buttons[static_cast<XdevLButtonId>(event.key.keycode)];
				}

				if(!m_keyPressed) {
					button->capturePressTime(event.common.timestamp);
				}

				button->setState(true);
				m_keyPressed = true;

				XdevLDelegateButtonParameter parameter;
				parameter.id = static_cast<XdevLButtonId>(event.key.keycode);
				parameter.state = BUTTON_PRESSED;
				parameter.timestamp = event.key.timestamp;
				parameter.mod = m_modState;
				for(auto& delegate : m_buttonDelegates) {
					delegate(parameter);
				}

				for(auto delegateMultipMap :  m_buttonIdDelegates) {
					auto pp = delegateMultipMap.second.equal_range(static_cast<XdevLButtonId>(event.key.keycode));
					XdevLDelegateButtonIdParameter parameterId;
					parameterId.state = BUTTON_PRESSED;
					parameterId.timestamp = event.key.timestamp;
					parameterId.mod = m_modState;
					for(auto it = pp.first; it != pp.second; ++it) {
						auto delegate = it->second;
						delegate(parameterId);
					}
				}
			}
			//
			// Did the user released a keyboard button?
			//
			else if(event.type == KeyButtonReleased.getHashCode()) {
				m_modState = event.key.mod;

				XdevLButtonImpl* button = m_buttons[static_cast<XdevLButtonId>(event.key.keycode)];
				if(button == nullptr) {
					m_buttons[static_cast<XdevLButtonId>(event.key.keycode)] =  new XdevLButtonImpl(&m_mutex);
					button = m_buttons[static_cast<XdevLButtonId>(event.key.keycode)];
				}

				if(m_keyPressed) {
					button->captureReleaseTime(event.common.timestamp);
				}

				button->setState(false);
				m_keyPressed = false;

				XdevLDelegateButtonParameter parameter;
				parameter.id = static_cast<XdevLButtonId>(event.key.keycode);
				parameter.state = BUTTON_RELEASED;
				parameter.timestamp = event.key.timestamp;
				parameter.mod = m_modState;
				for(auto& delegate : m_buttonDelegates) {
					delegate(parameter);
				}


				for(auto delegateMultipMap :  m_buttonIdDelegates) {
					auto pp = delegateMultipMap.second.equal_range(static_cast<XdevLButtonId>(event.key.keycode));
					XdevLDelegateButtonIdParameter parameterId;
					parameterId.state = BUTTON_RELEASED;
					parameterId.timestamp = event.key.timestamp;
					parameterId.mod = m_modState;
					for(auto it = pp.first; it != pp.second; ++it) {
						auto delegate = it->second;
						delegate(parameterId);
					}
				}
			}
			//
			// Did the core sent a message?
			//
			else if(event.type == XDEVL_MODULE_EVENT) {
				// Initialize this module.
				if(event.module.event == XDEVL_MODULE_INIT) {
					return init();
				}
				// Shutdown this module.
				else if(event.module.event == XDEVL_MODULE_SHUTDOWN) {
					return shutdown();
				}
			}
		}
		return XdevLModuleImpl<XdevLKeyboard>::notify(event);
	}

	xdl_int XdevLKeyboardImpl::attach(XdevLWindow* window) {
		Super::attach(window);

		auto coreParameter = getMediator()->getCoreParameter();
		if(coreParameter.xmlBuffer.size() != 0) {
			TiXmlDocument xmlDocument;
			if(!xmlDocument.Parse((xdl_char*)coreParameter.xmlBuffer.data())) {
				XDEVL_MODULE_ERROR("Could not parse xml buffer.\n ");
				return RET_FAILED;
			}

			if(readKeyboardXmlInfo(&xmlDocument) != RET_SUCCESS) {
				return RET_FAILED;
			}
		}

		setAttached(true);

		return RET_SUCCESS;
	}

	xdl_int XdevLKeyboardImpl::readKeyboardXmlInfo(TiXmlDocument* document) {
		TiXmlHandle docHandle(document);
		TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toString().c_str()).FirstChildElement(keyboard_moduleNames[0].toString().c_str()).ToElement();
		if(!root) {
			return RET_SUCCESS;
		}

		if(root->Attribute("id")) {
			XdevLID id(root->Attribute("id"));
			if(this->getID() != id) {
				return RET_SUCCESS;
			}
		} else {
			XDEVL_MODULE_ERROR("No 'id' attribute specified.\n");
			return RET_FAILED;
		}

		// If there is a crt attribute, set this value for all buttons.
		if(root->Attribute("crt")) {
			XdevLTimeMilliseconds crt;
			crt.milliseconds = xstd::from_string<xdl_uint64>(root->Attribute("crt"));
			for(size_t a = 0; a < m_buttons.size(); ++a)
				m_buttons[static_cast<XdevLButtonId>(a)]->setClickResponseTime(crt);
		}

		TiXmlElement* child = 0;
		for(child = root->FirstChildElement(); child; child = child->NextSiblingElement()) {
			if(child->ValueTStr() == "ClickResponseTime") {
				if(child->Attribute("button")) {
					auto button = m_keyButtonId[XdevLString(child->Attribute("button"))];
					if(child->Attribute("value")) {
						XdevLTimeMilliseconds crt;
						crt.milliseconds = xstd::from_string<xdl_uint64>(child->Attribute("value"));
						m_buttons[static_cast<XdevLButtonId>(button)]->setClickResponseTime(crt);
					}
				} else {
					XDEVL_MODULE_ERROR("No button attribute in <ClickResponseTime> element found. Please specify a button id.\n");
					return RET_FAILED;
				}
			}
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLKeyboardImpl::initKeyMap() {

		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_ENTER")),KEY_ENTER));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_ESCAPE")),KEY_ESCAPE));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_BACKSPACE")),KEY_BACKSPACE));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_TAB")),KEY_TAB));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_SPACE")),KEY_SPACE));

		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_PLUS")),KEY_PLUS));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_COMMA")),KEY_COMMA));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_MINUS")),KEY_MINUS));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_PERIOD")),KEY_PERIOD));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_SLASH")),KEY_SLASH));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_BACKSLASH")),KEY_BACKSLASH));

		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_0")),KEY_0));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_1")),KEY_1));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_2")),KEY_2));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_3")),KEY_3));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_4")),KEY_4));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_5")),KEY_5));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_6")),KEY_6));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_7")),KEY_7));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_8")),KEY_8));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_9")),KEY_9));

		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_A")),KEY_A));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_B")),KEY_B));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_C")),KEY_C));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_D")),KEY_D));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_E")),KEY_E));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F")),KEY_F));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_G")),KEY_G));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_H")),KEY_H));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_I")),KEY_I));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_J")),KEY_J));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_K")),KEY_K));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_L")),KEY_L));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_M")),KEY_M));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_N")),KEY_N));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_O")),KEY_O));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_P")),KEY_P));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_Q")),KEY_Q));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_R")),KEY_R));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_S")),KEY_S));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_T")),KEY_T));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_U")),KEY_U));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_V")),KEY_V));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_W")),KEY_W));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_X")),KEY_X));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_Y")),KEY_Y));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_Z")),KEY_Z));

		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F1")),KEY_F1));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F2")),KEY_F2));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F3")),KEY_F3));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F4")),KEY_F4));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F5")),KEY_F5));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F6")),KEY_F6));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F7")),KEY_F7));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F8")),KEY_F8));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F9")),KEY_F9));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F10")),KEY_F10));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F11")),KEY_F11));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F12")),KEY_F12));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F13")),KEY_F13));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F14")),KEY_F14));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F15")),KEY_F15));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F16")),KEY_F16));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F17")),KEY_F17));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F18")),KEY_F18));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F19")),KEY_F19));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F20")),KEY_F20));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F21")),KEY_F21));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F22")),KEY_F22));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F23")),KEY_F23));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_F24")),KEY_F24));


		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_PRINTSCREEN")),KEY_PRINTSCREEN));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_SCROLLLOCK")),KEY_SCROLLLOCK));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_PAUSE")),KEY_PAUSE));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_INSERT")),KEY_INSERT));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_HOME")),KEY_HOME));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_PAGEUP")),KEY_PAGEUP));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_DELETE")),KEY_DELETE));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_END")),KEY_END));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_PAGEDOWN")),KEY_PAGEDOWN));

		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_RIGHT")),KEY_RIGHT));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_LEFT")),KEY_LEFT));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_DOWN")),KEY_DOWN));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_UP")),KEY_UP));

		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_CAPSLOCK")),KEY_CAPSLOCK));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_NUMLOCK")),KEY_NUMLOCK));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_DIVIDE")),KEY_KP_DIVIDE));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_MULTIPLY")),KEY_KP_MULTIPLY));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_MINUS")),KEY_KP_MINUS));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_PLUS")),KEY_KP_PLUS));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_ENTER")),KEY_KP_ENTER));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_0")),KEY_KP_0));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_1")),KEY_KP_1));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_2")),KEY_KP_2));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_3")),KEY_KP_3));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_4")),KEY_KP_4));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_5")),KEY_KP_5));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_6")),KEY_KP_6));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_7")),KEY_KP_7));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_8")),KEY_KP_8));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_9")),KEY_KP_9));
		m_keyButtonId.insert(KeyMapType::value_type(XdevLString(TEXT("KEY_KP_PERIOD")),KEY_KP_PERIOD));

		m_buttons[KEY_0] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_1] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_2] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_3] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_4] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_5] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_6] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_7] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_8] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_9] = new XdevLButtonImpl(&m_mutex);

		m_buttons[KEY_A] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_B] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_C] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_D] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_E] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_G] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_H] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_I] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_J] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_K] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_L] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_M] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_N] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_O] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_P] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_Q] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_R] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_S] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_T] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_U] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_V] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_W] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_X] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_Y] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_Z] = new XdevLButtonImpl(&m_mutex);

		m_buttons[KEY_F1] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F2] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F3] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F4] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F5] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F6] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F7] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F8] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F9] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F10] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F11] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F12] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F13] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F14] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F15] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F16] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F17] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F18] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F19] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F20] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F21] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F22] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F23] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_F24] = new XdevLButtonImpl(&m_mutex);

		m_buttons[KEY_HOME] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_END] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_PAGEUP] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_PAGEDOWN] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_DELETE] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_INSERT] = new XdevLButtonImpl(&m_mutex);

		m_buttons[KEY_UP] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_DOWN] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_LEFT] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_RIGHT] = new XdevLButtonImpl(&m_mutex);

		m_buttons[KEY_ESCAPE] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_ENTER] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_TAB] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_SPACE] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_BACKSPACE] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_SLASH] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_BACKSLASH] = new XdevLButtonImpl(&m_mutex);

		m_buttons[KEY_PLUS] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_MINUS] = new XdevLButtonImpl(&m_mutex);

		m_buttons[KEY_KP_MULTIPLY] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_DIVIDE] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_MINUS] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_PLUS] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_ENTER] = new XdevLButtonImpl(&m_mutex);

		m_buttons[KEY_KP_PERIOD] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_0] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_1] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_2] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_3] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_4] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_5] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_6] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_7] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_8] = new XdevLButtonImpl(&m_mutex);
		m_buttons[KEY_KP_9] = new XdevLButtonImpl(&m_mutex);

		return RET_SUCCESS;
	}

	xdl_uint16 XdevLKeyboardImpl::areModifiersPressed(std::vector<xdl_uint> modifierCombination) {
		xdl_bool tmp = xdl_true;
		for(auto& modifier : modifierCombination) {
			auto comp = (m_modState & modifier);
			tmp &= comp > 0;
		}
		return tmp;
	}
}
