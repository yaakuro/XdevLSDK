/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_KEYBOARD_IMPL_H
#define XDEVL_KEYBOARD_IMPL_H

#include <XdevLInput/XdevLInputImpl.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>

namespace xdl {

	static const XdevLString keyboard_pluginName {
		"XdevLKeyboard"
	};

	static const XdevLString keyboardDescription {
		"Module that helps handling keyboard events."
	};

	static const std::vector<XdevLModuleName> keyboard_moduleNames {
		XdevLModuleName("XdevLKeyboard")
	};

	class XdevLKeyboardImpl : public XdevLInputImpl<XdevLKeyboard> {
		public:
			using KeyMapType = std::map<const XdevLString, XdevLButtonId>;

			XdevLKeyboardImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual	~XdevLKeyboardImpl();

			//
			// XdevLListener methods.
			//
			xdl_int notify(XdevLEvent& event) override final;

			//
			// XdevLModuleImpl methods.
			//
			xdl_int init() override final;

			//
			// XdevLKeyboard methods
			//
			xdl_int attach(XdevLWindow* window) override final;
			xdl_uint16 areModifiersPressed(std::vector<xdl_uint> modifierCombination) override final;

		private:

			xdl_int initKeyMap();
			xdl_int readKeyboardXmlInfo(TiXmlDocument* document);

		protected:

			xdl_bool m_keyPressed;
			KeyMapType m_keyButtonId;
			xdl_uint16 m_modState;
	};

}

#endif
