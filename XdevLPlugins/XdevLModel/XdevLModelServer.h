/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MODEL_SERVER_H
#define XDEVL_MODEL_SERVER_H

#include <XdevL.h>
#include <XdevLModule.h>
#include <XdevLFileSystem/XdevLFileSystem.h>
#include <XdevLModel/XdevLModel.h>
#include <XdevLImage/XdevLImageServer.h>

namespace xdl {

	namespace gfx {

		/**
		 * @class XdevLModelImportPlugin
		 * @brief Interface for 3D model import plugin.
		 */
		class XdevLModelImportPlugin {
			public:
				virtual ~XdevLModelImportPlugin() {}

				virtual xdl_int init(IPXdevLRAI rai, IPXdevLImageServer imageserver) = 0;
				virtual IPXdevLModel import(IPXdevLFile& file) = 0;
				virtual XdevLString getExtension() = 0;
		};

		using IXdevLModelAssetImportPlugin = XdevLModelImportPlugin;
		using IPXdevLModelAssetImportPlugin = std::shared_ptr<XdevLModelImportPlugin>;

		/**
		 * @class XdevLModelImportPluginCreateParameter
		 * @brief Image importer plugin constructor parameter.
		 */
		class XdevLModelImportPluginCreateParameter {
			public:
				IPXdevLModelAssetImportPlugin pluginInstance;
				IPXdevLRAI rai;
		};

		using XdevLCreateModelImportPluginFunction = void(*)(const XdevLModelImportPluginCreateParameter& parameter);

		/**
		 * @class XdevLModelAssetImportPluginInfo
		 * @brief Info about a image imprter plugin.
		 */
		class XdevLModelAssetImportPluginInfo {
			public:
				XdevLID id;
				IPXdevLModelAssetImportPlugin modelAssetImportPlugin;
		};

		/**
		 * @class XdevLModelServer
		 * @brief The server that manages all image importer plugins.
		 */
		class XdevLModelServer : public XdevLModule {
			public:
				virtual ~XdevLModelServer() {}

				virtual xdl_int create(IPXdevLRAI rai, IPXdevLImageServer imageserver) = 0;

				/// Plug a new importer plugin to the system.
				virtual xdl_int plug(const XdevLPluginName& pluginName, const XdevLVersion& version = XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, 0)) = 0;

				/// Returns the importer for the specified extensions.
				virtual IPXdevLModelAssetImportPlugin getImporter(const XdevLString& extension) = 0;

				/// Loads the specified image an converts it into a RGB or RGBA image format.
				virtual IPXdevLModel import(IPXdevLFile& file) = 0;

				/// Creates empty model.
				virtual IPXdevLModel createModel() = 0;

				/// Returns the procedural system.
				virtual IPXdevLProceduralSystem getProceduralSystem() const = 0;

		};

		using IXdevLModelServer = XdevLModelServer;
		using IPXdevLModelServer = XdevLModelServer*;
	}
}

#endif
