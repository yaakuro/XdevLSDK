/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_COLOR_H
#define XDEVL_COLOR_H

#include <XdevLTypes.h>
#include <algorithm>

namespace xdl {

	namespace gfx {

		class XdevLColor {
			public:

				using value_type = xdl_float;

				XdevLColor()
					: r(0.0f)
					, g(0.0f)
					, b(0.0f)
					, a(0.0f)
				{}

				XdevLColor(xdl_float red, xdl_float green, xdl_float blue, xdl_float alpha)
					: r(red)
					, g(green)
					, b(blue)
					, a(alpha)
				{}

				XdevLColor getCMYK() {
					XdevLColor color(r, g, b, a);
					color.covertToCMYK();
					return color;
				}

				void covertToCMYK() {
					xdl_float K = 1.0f - std::max(std::max(r, g), b);
					xdl_float C = (1.0f - r - K) / (1.0f - K);
					xdl_float M = (1.0f - g - K) / (1.0f - K);
					xdl_float Y = (1.0f - b - K) / (1.0f - K);
					c = C;
					y = Y;
					m = M;
					k = K;
				}

				union {
					xdl_float c, r;
				};

				union {
					xdl_float m, g;
				};

				union {
					xdl_float y, b;
				};

				union {
					xdl_float k, a;
				};

				operator xdl_float* () {
					return &r;
				}

		};

		using IXdevLColor = XdevLColor;
		using IPXdevLColor = std::shared_ptr<XdevLColor>;

	}
}
#endif
