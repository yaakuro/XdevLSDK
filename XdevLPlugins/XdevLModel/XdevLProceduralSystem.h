/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_PROCEDURAL_SYSTEM_H
#define XDEVL_PROCEDURAL_SYSTEM_H

#include <XdevLRAI/XdevLRAI.h>
#include <XdevLModel/XdevLModelServerFWD.h>

namespace xdl {

	namespace gfx {

		class XdevLProceduralSystem {
			public:

				virtual ~XdevLProceduralSystem() {}

				struct ProceduralSystemGridVertex {
					xdl_float x,y,z;
					xdl_float r,g,b,a;
					xdl_float nx, ny, nz;
					xdl_float u, v;
				};

				struct XdevLTriangleFace {
					xdl_uint a, b, c;

					XDEVL_INLINE bool operator==(const XdevLTriangleFace& face) {
						return (a == face.a) && (b == face.b) && (c == face.c);
					}
				};


				virtual IPXdevLMesh createBox(const XdevLString& name,  xdl_float sx, xdl_float sy, xdl_float sz) = 0;
				virtual IPXdevLMesh createGrid(const XdevLString& name, xdl_float size, xdl_float resolution) = 0;

			public:

				virtual xdl_int init(IPXdevLRAI rai) = 0;
				virtual void shutdown() = 0;

		};

		using IXdevLProceduralSystem = XdevLProceduralSystem;
		using IPXdevLProceduralSystem = std::shared_ptr<XdevLProceduralSystem>;
	}
}

#endif
