/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLRAI/XdevLStreamBuffer.h>
#include <XdevLModel/XdevLProceduralSystemImpl.h>
#include <XdevLModel/XdevLMesh.h>

#include <cmath>

namespace xdl {

	namespace gfx {

		XdevLProceduralSystemImpl::XdevLProceduralSystemImpl()
			: m_rai(nullptr) {
		}


		xdl_int XdevLProceduralSystemImpl::init(IPXdevLRAI rai) {
			m_rai = rai;
			return RET_SUCCESS;
		}

		void XdevLProceduralSystemImpl::shutdown() {

		}

		IPXdevLMesh XdevLProceduralSystemImpl::createBox(const XdevLString& name, xdl_float sx, xdl_float sy, xdl_float sz) {

			xdl_float h2w = sx * 0.5f;
			xdl_float h2h = sy * 0.5f;
			xdl_float h2l = sz * 0.5f;

			const std::vector<tmath::vec4> vertexColor = {
				{1.0f, 1.0f, 1.0f, 1.0f},
				{1.0f, 1.0f, 1.0f, 1.0f},
				{1.0f, 1.0f, 1.0f, 1.0f},
				{1.0f, 1.0f, 1.0f, 1.0f},
				{1.0f, 1.0f, 1.0f, 1.0f},
				{1.0f, 1.0f, 1.0f, 1.0f},
				{1.0f, 1.0f, 1.0f, 1.0f},
				{1.0f, 1.0f, 1.0f, 1.0f}
			};

			const std::vector<tmath::vec3> vertexPosition = {
				{-h2w,-h2h, h2l},
				{-h2w, h2h, h2l},
				{ h2w, h2h, h2l},
				{ h2w,-h2h, h2l},
				{ h2w,-h2h,-h2l},
				{ h2w, h2h,-h2l},
				{-h2w, h2h,-h2l},
				{-h2w,-h2h,-h2l},
			};

			std::vector<XdevLTriangleFace> faces {
				// +Z
				{0, 1, 2},
				{2, 3, 0},
				// -Z
				{4, 5, 6},
				{6, 7, 4},
				// +X
				{3, 2, 5},
				{5, 4, 3},
				// -X
				{7, 6, 1},
				{1, 0, 7},
				// +Y
				{1, 6, 5},
				{5, 2, 1},
				// -Y
				{0, 7, 4},
				{4, 3, 0}
			};

			std::vector<tmath::vec3> vertexNormal;
			createNormals(vertexPosition, faces, vertexNormal);

			std::vector<xdl_uint8*> list;
			list.push_back((xdl_uint8*)vertexPosition.data());
			list.push_back((xdl_uint8*)vertexNormal.data());
			list.push_back((xdl_uint8*)vertexColor.data());

			auto mesh = std::make_shared<IXdevLMesh>(m_rai, XdevLID(name));
			mesh->add(3, XdevLBufferElementTypes::FLOAT, VERTEX_POSITION);
			mesh->add(3, XdevLBufferElementTypes::FLOAT, VERTEX_NORMAL);
			mesh->add(4, XdevLBufferElementTypes::FLOAT, VERTEX_COLOR);
			mesh->init(list, vertexPosition.size());

			auto indexBuffer = m_rai->createIndexBuffer();
			indexBuffer->init(XdevLBufferElementTypes::UNSIGNED_INT, (xdl_uint8*)faces.data(), faces.size() * bufferElementTypeSizeInBytes(XdevLBufferElementTypes::UNSIGNED_INT));
			mesh->setNumberOfFaces(faces.size());
			mesh->setNumberOfVertices(vertexPosition.size());
			mesh->init(indexBuffer);

			XdevLID id(name + XdevLString("Material"));
			auto material = std::make_shared<IXdevLMaterial>(id);
			mesh->setMaterial(material);
			return mesh;
		}

		IPXdevLMesh XdevLProceduralSystemImpl::createGrid(const XdevLString& name, xdl_float size, xdl_float resolution) {

			float min = -size;
			float max = size;
			float step = resolution;

			std::cout << "Vertices: \n";
			std::vector<ProceduralSystemGridVertex> vertices;
			for(float y = min; y <= max; y+= step) {
				for(float x = min; x <= max; x+= step) {
					ProceduralSystemGridVertex v1;

					// Add position information.
					v1.x = x;
					v1.y = y;
					v1.z = 0.0f;

					// Add normal information.
					v1.nx = 0.0f;
					v1.ny = 0.0f;
					v1.nz = 1.0f;

					// Add color information.
					v1.r = v1.g = v1.b = v1.a = 1.0f;

					std::cout << "(" << v1.x << "," << v1.y << ")" << std::endl;
					vertices.push_back(v1);
				}
			}

			std::cout << "Indices: \n";
			std::vector<xdl_uint16> indices;
			unsigned int width = static_cast<unsigned int>(std::abs((min - max)/step));
			for(unsigned int y = 0; y < width; y++) {
				for(unsigned int x = 0; x < width; x++) {
					unsigned int lp1 = x + y*(width + 1);
					unsigned int lp2 = x + width + 1 + y*(width+1);
					unsigned int lp3 = x + 1 + width + 1 + y*(width+1);
					unsigned int lp4 = x + 1 + y*(width+1);

					std::cout << "(" << lp1 << "," << lp2 << "," << lp3 << ")\n";
					std::cout << "(" << lp3 << "," << lp4 << "," << lp1 << ")\n";

					indices.push_back(lp1);
					indices.push_back(lp2);
					indices.push_back(lp3);

					indices.push_back(lp3);
					indices.push_back(lp4);
					indices.push_back(lp1);
				}
			}


			auto mesh = std::make_shared<IXdevLMesh>(m_rai, XdevLID(name));
			mesh->add(3, XdevLBufferElementTypes::FLOAT, VERTEX_POSITION);
			mesh->add(4, XdevLBufferElementTypes::FLOAT, VERTEX_COLOR);
			mesh->add(3, XdevLBufferElementTypes::FLOAT, VERTEX_NORMAL);
			mesh->add(2, XdevLBufferElementTypes::FLOAT, VERTEX_TEXTURE_COORD0);

			auto vb = m_rai->createVertexBuffer();
			if(vb->init((xdl_uint8*)vertices.data(), vertices.size() * sizeof(ProceduralSystemGridVertex)) != RET_SUCCESS) {
				return nullptr;
			}

			auto ib = m_rai->createIndexBuffer();
			if(ib->init(XdevLBufferElementTypes::UNSIGNED_INT, (xdl_uint8*)indices.data(), indices.size()) != RET_SUCCESS) {
				return nullptr;
			}

			mesh->init(vb, ib);

			XdevLID id(name + XdevLString("Material"));
			auto material = std::make_shared<IXdevLMaterial>(id);
			material->setDiffuse(1.0f, 1.0f, 1.0f, 1.0f);

			mesh->setMaterial(material);


			return mesh;
		}

		void XdevLProceduralSystemImpl::createNormals(const std::vector<tmath::vec3>& positions, const std::vector<XdevLTriangleFace>& faces, std::vector<tmath::vec3>& normals) {



			for(size_t idx = 0; idx < positions.size(); idx++) {

				//
				// Get all faces with same index.
				//
				std::vector<XdevLTriangleFace> sameFace;
				for(auto& face : faces) {
					if(face.a == idx || face.b == idx || face.c == idx) {
						sameFace.push_back(face);
					}
				}

				tmath::vec3 normal {};
				for(auto& face : sameFace) {
					auto& v1 = positions[face.a];
					auto& v2 = positions[face.b];
					auto& v3 = positions[face.c];

					auto d1 = v2 - v1;
					auto d2 = v3 - v1;
					normal += tmath::cross(d2, d1);
				}
				tmath::normalize(normal);
				normals.push_back(normal);

			}

		}
	}
}
