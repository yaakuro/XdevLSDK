/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <iostream>
#include <string>
#include <XdevLModule.h>
#include <XdevLCoreMediator.h>
#include <XdevLPlatform.h>

#include "XdevLModelServerBase.h"
#include "XdevLProceduralSystemImpl.h"

#include <fstream>


xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::gfx::pluginName,
	xdl::gfx::moduleNames,
	XDEVL_MODEL_SERVER_PLUGIN_MAJOR_VERSION,
	XDEVL_MODEL_SERVER_PLUGIN_MINOR_VERSION,
	XDEVL_MODEL_SERVER_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::gfx::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::gfx::description,
	XDEVL_MODEL_SERVER_MODULE_MAJOR_VERSION,
	XDEVL_MODEL_SERVER_MODULE_MINOR_VERSION,
	XDEVL_MODEL_SERVER_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::gfx::XdevLModelServerImpl, moduleDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

namespace xdl {

	namespace gfx {

		XdevLModelServerImpl::XdevLModelServerImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
			: XdevLModuleImpl<IXdevLModelServer>(parameter, descriptor)
			, m_rai(nullptr) {
		}

		XdevLModelServerImpl::~XdevLModelServerImpl() {
		}

		IPXdevLProceduralSystem XdevLModelServerImpl::getProceduralSystem() const {
			return m_proceduralSystem;
		}

		xdl_int  XdevLModelServerImpl::create(IPXdevLRAI rai, IPXdevLImageServer imageServer) {
			XDEVL_ASSERT(nullptr != rai, "Invaild IPXdevLRAI specified.");
			m_rai = rai;
			m_imageServer = imageServer;

			m_proceduralSystem = std::make_shared<XdevLProceduralSystemImpl>();
			auto result = m_proceduralSystem->init(m_rai);
			if(RET_SUCCESS != result) {
				return result;
			}

			result = plug(XdevLPluginName("XdevLModelImportPluginOBJ"));
			if(RET_SUCCESS != result) {
				XDEVL_MODULEX_ERROR(XdevLModelServer, "Faild to plug OBJ importer plugin.\n");
			}
			result = plug(XdevLPluginName("XdevLModelImportPluginFBX"));
			if(RET_SUCCESS != result) {
				XDEVL_MODULEX_ERROR(XdevLModelServer, "Faild to plug FBX importer plugin.\n");
			}

			return result;
		}

		xdl_int XdevLModelServerImpl::plug(const XdevLPluginName& pluginName, const XdevLVersion& version) {
			XDEVL_ASSERT(nullptr != m_rai, "You must initialize this instance using 'create' and specify a valid IPXdevLRAI.");

			XdevLSharedLibrary* modulesSharedLibrary = nullptr;
			try {
				modulesSharedLibrary = new XdevLSharedLibrary();
			} catch(std::bad_alloc& e) {
				XDEVL_MODULE_ERROR(e.what());
				return RET_FAILED;
			}

			XdevLFileName tmp = getMediator()->getCoreParameter().pluginsPath;
#ifdef XDEVL_PLATFORM_ANDROID
			tmp += XdevLFileName("lib");
#endif
			tmp += XdevLFileName(pluginName);
			tmp += XdevLFileName("-");
			tmp += version.toString();
#ifdef XDEVL_DEBUG
			tmp += XdevLFileName("d");
#endif
			tmp += XdevLFileName(".xmp");

			if(modulesSharedLibrary->open(tmp.toString().c_str(), xdl_false) == RET_FAILED) {
				delete modulesSharedLibrary;
				XDEVL_MODULE_ERROR("Could not open plugin: '" << tmp << "'.\n");
				return RET_FAILED;
			}

			XdevLCreateModelImportPluginFunction createModelImportPlugin = (XdevLCreateModelImportPluginFunction)(modulesSharedLibrary->getFunctionAddress("_createModelPlugin"));

			XdevLModelImportPluginCreateParameter importPluginCreateParameter {};
			createModelImportPlugin(importPluginCreateParameter);

			XdevLID id(importPluginCreateParameter.pluginInstance->getExtension());

			auto found = m_pluginMap.find(id.getHashCode());
			if(m_pluginMap.end() != found) {
				return RET_FAILED;
			}

			XdevLModelAssetImportPluginInfo importPluginInfo {};
			importPluginInfo.id = id;
			importPluginInfo.modelAssetImportPlugin = importPluginCreateParameter.pluginInstance;
			if(importPluginCreateParameter.pluginInstance->init(m_rai, m_imageServer) != RET_SUCCESS) {
				return RET_FAILED;
			}

			m_pluginMap[id.getHashCode()] = importPluginInfo;

			return RET_SUCCESS;
		}

		IPXdevLModelAssetImportPlugin XdevLModelServerImpl::getImporter(const XdevLString& extension) {
			XDEVL_ASSERT(nullptr != m_rai, "You must initialize this instance using 'create' and specify a valid IPXdevLRAI.");

			XdevLID id(extension);
			auto found = m_pluginMap.find(id.getHashCode());
			if(m_pluginMap.end() == found) {
				return nullptr;
			}
			return found->second.modelAssetImportPlugin;
		}

		IPXdevLModel XdevLModelServerImpl::import(IPXdevLFile& file) {
			XDEVL_ASSERT(nullptr != m_rai, "You must initialize this instance using 'create' and specify a valid IPXdevLRAI.");
			XDEVL_ASSERT(nullptr != file, "Specified file object is invalid.");

			auto importer = getImporter(file->getFileName().getExtension());
			if(nullptr != importer) {
				return importer->import(file);
			}
			return nullptr;
		}
		
		IPXdevLModel XdevLModelServerImpl::createModel() {
			auto tmp = std::make_shared<XdevLModel>(m_rai);
			return tmp;
		}
	}
}
