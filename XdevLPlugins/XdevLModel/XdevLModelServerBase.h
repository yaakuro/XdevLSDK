/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MODEL_SERVER_BASE_H
#define XDEVL_MODEL_SERVER_BASE_H

#include <XdevLModel/XdevLModelServer.h>
#include <XdevLRAI/XdevLRAI.h>

#include <XdevLPluginImpl.h>

namespace xdl {

	namespace gfx {

		static const XdevLString pluginName {
			"XdevLModelServer"
		};
		static const XdevLString description {
			"Creates 3D Model Import System."
		};
		static std::vector<XdevLModuleName>	moduleNames {
			XdevLModuleName("XdevLModelServer")
		};

		class XdevLModelServerImpl : public XdevLModuleImpl<IXdevLModelServer> {
			public:
				XdevLModelServerImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
				virtual ~XdevLModelServerImpl();

				xdl_int create(IPXdevLRAI rai, IPXdevLImageServer imageServer) override final;
				xdl_int plug(const XdevLPluginName& pluginName, const XdevLVersion& version = XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, 0)) override final;
				IPXdevLModelAssetImportPlugin getImporter(const XdevLString& extension) override final;
				IPXdevLModel import(IPXdevLFile& file) override final;
				IPXdevLModel createModel() override final;
				IPXdevLProceduralSystem getProceduralSystem() const override final;

			private:

				IPXdevLRAI m_rai;
				IPXdevLImageServer m_imageServer;
				IPXdevLProceduralSystem m_proceduralSystem;
				std::map<xdl_uint64, XdevLModelAssetImportPluginInfo> m_pluginMap;
		};
	}
}

#endif
