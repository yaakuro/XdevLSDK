/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MODEL_IMPORT_PLUGIN_OBJ_H
#define XDEVL_MODEL_IMPORT_PLUGIN_OBJ_H

#include <XdevLFileSystem/XdevLFileSystem.h>
#include <XdevLImage/XdevLImageServer.h>
#include <XdevLModel/XdevLModelImportUtils/XdevLAssimp.h>
#include <XdevLModel/XdevLModelServer.h>
#include <XdevLModel/XdevLModelServerFWD.h>

namespace xdl {

	namespace gfx {

		class XdevLModelImportPluginOBJ : public XdevLModelImportPlugin, public XdevLAssimp {

			public:

				XdevLModelImportPluginOBJ();

				virtual ~XdevLModelImportPluginOBJ();

				xdl_int init(IPXdevLRAI rai, IPXdevLImageServer imageserver) override final;
				IPXdevLModel import(IPXdevLFile& file) override final;
				XdevLString getExtension() override final;

		};
	}
}

#endif
