/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MODEL_IMPORT_PLUGIN_ASSIMP_H
#define XDEVL_MODEL_IMPORT_PLUGIN_ASSIMP_H

#include <XdevLFileSystem/XdevLFileSystem.h>
#include <XdevLModel/XdevLModelServer.h>
#include <XdevLModel/XdevLModelServerFWD.h>
#include <XdevLImage/XdevLImageServer.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace xdl {

	namespace gfx {


		XDEVL_INLINE tmath::vec3 convert(const aiVector3D& vec) {
			return tmath::vec3(vec.x, vec.y, vec.z);
		}

		/**
		 * @class XdevLAssimp
		 * @brief Helper class to handle Assimp importer.
		 */
		class XdevLAssimp {

			public:

				XdevLAssimp();

				virtual ~XdevLAssimp();

				/// Initialize Assimp.
				xdl_int init(IPXdevLRAI rai, IPXdevLImageServer imageserver);

				/// Shutsdown Assimp.
				xdl_int shutdown();

				/// Imports the scene of the asset to import.
				IPXdevLModel import(IPXdevLFile& file);

				/// Returns the Assimp scene.
				aiScene const* getScene() const;

			protected:

				/// Add Assimp mesh to XdevLModel.
				xdl_int addMesh(const aiMesh* aimesh, IPXdevLModel& model);


				/// Helper function for the getBoudingBox method.
				void getBoundingBoxNode(const aiNode* nd,
				                        tmath::vec3* min,
				                        tmath::vec3* max,
				                        aiMatrix4x4* trafo
				                       );

				/// Calculate the bounding box of the model.
				XdevLAABB getBoundingBox();

			private:

				/// Holds the Assimp scene.
				aiScene* m_aiScene;

				IPXdevLRAI m_rai;
				IPXdevLImageServer m_imageServer;
		};
	}
}

#endif
