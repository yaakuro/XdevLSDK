/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLAssimp.h"
#include <XdevLModel/XdevLMesh.h>
#include <XdevLModel/XdevLModel.h>
#include <XdevLArrayModifier.h>
#include <XdevLImage/XdevLImageUtils.h>

#include <fstream>


namespace xdl {

	namespace gfx {

		XdevLAssimp::XdevLAssimp()
			: m_aiScene(nullptr)
			, m_rai(nullptr) {
		}

		XdevLAssimp::~XdevLAssimp() {

		}

		aiScene const* XdevLAssimp::getScene() const {
			return m_aiScene;
		}


		xdl_int XdevLAssimp::init(IPXdevLRAI rai, IPXdevLImageServer imageserver) {
			XDEVL_ASSERT(nullptr != rai, "Invalid IPXdevLRAI specified.");
			m_rai = rai;
			m_imageServer = imageserver;
			return RET_SUCCESS;
		}

		xdl_int XdevLAssimp::shutdown() {
			if(nullptr != m_aiScene) {
				aiReleaseImport(m_aiScene);
				m_aiScene = nullptr;
			}
			return RET_SUCCESS;
		}

		IPXdevLModel XdevLAssimp::import(IPXdevLFile& file) {

			// First free the previous scene if we had one.
			shutdown();

			// Load the whole file into the memory.
			auto length = file->length();
			std::vector<xdl_uint8> buffer(length);
			buffer.reserve(length);
			buffer.resize(length);

			file->read(buffer.data(), buffer.size());

			// we are taking one of the postprocessing presets to avoid
			// spelling out 20+ single postprocessing flags here.
			auto filenameExtension = file->getFileName().getExtension();
			m_aiScene = (aiScene*)aiImportFileFromMemory((const char*)buffer.data(), buffer.size(), aiProcessPreset_TargetRealtime_Quality, filenameExtension.toUTF8().c_str());
			if(m_aiScene == nullptr) {
				XDEVL_MODULEX_ERROR(XdevLAssimp, "aiImportFileFromMemory failed.\n");
				return nullptr;
			}

			auto model = std::make_shared<IXdevLModel>(m_rai);

			// Read all neccessary mesh data from the model.
			for(unsigned int n = 0; n < getScene()->mNumMeshes; ++n) {
				const aiMesh* mesh = getScene()->mMeshes[n];

				addMesh(mesh, model);
			}

			return model;
		}

		xdl_int XdevLAssimp::addMesh(const aiMesh* aimesh, IPXdevLModel& model) {
			XDEVL_ASSERT(nullptr != m_rai, "You must initialize first.");

			// Create Vertex Declaration. This describes the strucure of a vertex.
			auto vd = m_rai->createVertexDeclaration();
			auto va = m_rai->createVertexArray();


			std::vector<xdl_uint8*> list;

			// Do we have position data?
			if(aimesh->HasPositions()) {
				list.push_back((xdl_uint8*)aimesh->mVertices);
				vd->add(3, XdevLBufferElementTypes::FLOAT, VERTEX_POSITION);
			}

			// Do we have normal data?
			if(aimesh->HasNormals()) {
				list.push_back((xdl_uint8*)aimesh->mNormals);
				vd->add(3, XdevLBufferElementTypes::FLOAT, VERTEX_NORMAL);
			}

			if(aimesh->HasTangentsAndBitangents()) {
				list.push_back((xdl_uint8*)aimesh->mTangents);
				vd->add(3, XdevLBufferElementTypes::FLOAT, VERTEX_TANGENT);

				list.push_back((xdl_uint8*)aimesh->mBitangents);
				vd->add(3, XdevLBufferElementTypes::FLOAT, VERTEX_BITANGENT);
			}

			xdl_float *texCoords0 = nullptr;
			if(aimesh->HasTextureCoords(0)) {
				vd->add(2, XdevLBufferElementTypes::FLOAT, VERTEX_TEXTURE_COORD0);
				texCoords0 = new xdl_float[sizeof(xdl_float)*2*aimesh->mNumVertices];
				for(unsigned int k = 0; k < aimesh->mNumVertices; ++k) {

					texCoords0[k*2]   = aimesh->mTextureCoords[0][k].x;
					texCoords0[k*2+1] = aimesh->mTextureCoords[0][k].y;

				}
				list.push_back((xdl_uint8*)texCoords0);
			}
			xdl_float *texCoords1 = nullptr;
			if(aimesh->HasTextureCoords(1)) {
				vd->add(2, XdevLBufferElementTypes::FLOAT, VERTEX_TEXTURE_COORD1);
				texCoords1 = new xdl_float[sizeof(xdl_float)*2*aimesh->mNumVertices];
				for(unsigned int k = 0; k < aimesh->mNumVertices; ++k) {

					texCoords1[k*2]   = aimesh->mTextureCoords[0][k].x;
					texCoords1[k*2+1] = aimesh->mTextureCoords[0][k].y;

				}
				list.push_back((xdl_uint8*)texCoords1);
			}


			auto faceArray = (xdl_uint *)new xdl_uint[aimesh->mNumFaces * 3];
			xdl_uint faceIndex = 0;

			for(xdl_uint t = 0; t < aimesh->mNumFaces; ++t) {
				const aiFace* face = &aimesh->mFaces[t];

				memcpy(&faceArray[faceIndex], face->mIndices,3 * sizeof(xdl_uint));
				faceIndex += 3;
			}


			va->init(aimesh->mNumFaces* 3,
			         (xdl_uint8*)faceArray,
			         list.size(),
			         list.data(),
			         aimesh->mNumVertices,
			         vd);

			if(texCoords0 != nullptr) {
				delete [] texCoords0;
			}
			if(texCoords1 != nullptr) {
				delete [] texCoords1;
			}
			if(faceArray != nullptr) {
				delete [] faceArray;
			}

			xdl_uint m_numFaces = aimesh->mNumFaces;
			xdl_uint m_numVertices = aimesh->mNumVertices;

			// TODO Give correct IDs
			auto material = std::make_shared<IXdevLMaterial>();

			//
			// Do Material stuff here.
			//
			aiMaterial *mtl = m_aiScene->mMaterials[aimesh->mMaterialIndex];
			aiColor4D color;
			if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_DIFFUSE, &color)) {
				material->setDiffuse(color.r, color.g, color.b, color.a);
			}
			if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_AMBIENT, &color)) {
				material->setAmbient(color.r, color.g, color.b, color.a);
			}
			if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_SPECULAR, &color)) {
				material->setSpecular(color.r, color.g, color.b, color.a);
			}

			float shininess = 0.0;
			float shininessStrength = 0.0;
			unsigned int max = 1;;
			if(AI_SUCCESS == aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, &max)) {
				max = 1;;
				if(AI_SUCCESS == aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS_STRENGTH, &shininessStrength, &max)) {
					// TODO This is a hack, using 128.0f as normalization between shineniness and roughness.
					material->setRoughness(1.0f - shininessStrength*shininess/320.0f);
				} else {
					material->setRoughness(1.0f - shininess/320.0f);
				}
			} else {
				material->setRoughness(1.0f);
				material->setSpecular(0.0f, 0.0f, 0.0f, 0.0f);
			}


			//
			// First set default texture for all meshes. This will ensure that we have a mesh da mo neee :D.
			//
			for(unsigned int idx = 0; idx < material->getNumTextures(); idx++) {
				if(material->getTexture(XdeLMaterialTextures::DIFFUSE_MAP) == nullptr) {
//					material->setTexture(XdeLMaterialTextures::DIFFUSE_MAP, soan::TextureServer::Inst()->getDefaultTexture());
//					material->getTexture(XdeLMaterialTextures::DIFFUSE_MAP)->lock();
//					material->getTexture(XdeLMaterialTextures::DIFFUSE_MAP)->setTextureFilter(XDEVL_TEXTURE_MAG_FILTER, XDEVL_NEAREST);
//					material->getTexture(XdeLMaterialTextures::DIFFUSE_MAP)->setTextureFilter(XDEVL_TEXTURE_MIN_FILTER, XDEVL_NEAREST);
//					material->getTexture(XdeLMaterialTextures::DIFFUSE_MAP)->setTextureWrap(XDEVL_TEXTURE_WRAP_S, XDEVL_REPEAT);
//					material->getTexture(XdeLMaterialTextures::DIFFUSE_MAP)->setTextureWrap(XDEVL_TEXTURE_WRAP_T, XDEVL_REPEAT);
//					material->getTexture(XdeLMaterialTextures::DIFFUSE_MAP)->generateMipMap();
//					material->getTexture(XdeLMaterialTextures::DIFFUSE_MAP)->unlock();
				}
			}

			//
			// Import all textures into the system and make references if neccessary.
			//
			for(unsigned int m=0; m<m_aiScene->mNumMaterials; ++m) {
				int texIndex = 0;
				aiString path;

				//
				// Import all diffuse textures.
				//
				aiReturn texFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
				while(texFound == AI_SUCCESS) {
					IPXdevLFile tmp = m_rai->getArchive()->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName(path.C_Str()));

					auto imageObject = m_imageServer->import(tmp);
					IPXdevLTexture diffuseMap = xdl::convert(imageObject, m_rai);

					const xdl::XdevLTextureSamplerFilterStage filterStage {xdl::XdevLTextureFilterMin::LINEAR_MIPMAP_LINEAR, xdl::XdevLTextureFilterMag::LINEAR};
					const xdl::XdevLTextureSamplerWrap wrap {xdl::XdevLTextureWrap::REPEAT, xdl::XdevLTextureWrap::REPEAT};

					diffuseMap->lock();
					diffuseMap->setTextureFilter(filterStage);
					diffuseMap->setTextureWrap(wrap);
					diffuseMap->generateMipMap();
					diffuseMap->unlock();

					imageObject->destroy();

					material->setTexture(XdeLMaterialTextures::DIFFUSE_MAP, diffuseMap);
					material->setUseDiffuseMap(xdl_true);

					texIndex++;
					texFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
				}
				texIndex = 0;

				//
				// Import all specular textures.
				//
				aiReturn heightlFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_SPECULAR, texIndex, &path);
				while(heightlFound == AI_SUCCESS) {
					// FIXME The import of the texture server can return nullptr. FIX THIS LINE HERE.
//					material->setTexture(Material::SPECULAR_MAP, soan::TextureServer::Inst()->import(path.data));
//					material->setUseSpecularMap(xdl_true);

					texIndex++;
					heightlFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_SPECULAR, texIndex, &path);
				}
				texIndex = 0;

				//
				// Import all normal textures.
				//
				aiReturn normalFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_HEIGHT, texIndex, &path);
				while(normalFound == AI_SUCCESS) {
					IPXdevLFile tmp = m_rai->getArchive()->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName(path.C_Str()));

					auto imageObject = m_imageServer->import(tmp);
					IPXdevLTexture normalMap = xdl::convert(imageObject, m_rai);

					const xdl::XdevLTextureSamplerFilterStage filterStage {xdl::XdevLTextureFilterMin::LINEAR_MIPMAP_LINEAR, xdl::XdevLTextureFilterMag::LINEAR};
					const xdl::XdevLTextureSamplerWrap wrap {xdl::XdevLTextureWrap::REPEAT, xdl::XdevLTextureWrap::REPEAT};

					normalMap->lock();
					normalMap->setTextureFilter(filterStage);
					normalMap->setTextureWrap(wrap);
					normalMap->generateMipMap();
					normalMap->generateMipMap();
					normalMap->unlock();

					imageObject->destroy();

					material->setTexture(XdeLMaterialTextures::NORMAL_MAP, normalMap);
					material->setUseNormalMap(xdl_true);

					texIndex++;
					normalFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_HEIGHT, texIndex, &path);
				}

				texIndex = 0;

				//
				// Import all displacement textures.
				//
				aiReturn displacementFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_DISPLACEMENT, texIndex, &path);
				while(displacementFound == AI_SUCCESS) {
					IPXdevLFile tmp = m_rai->getArchive()->open(xdl::XdevLOpenForReadOnly(), xdl::XdevLFileName(path.C_Str()));

					auto imageObject = m_imageServer->import(tmp);
					IPXdevLTexture displacementMap = xdl::convert(imageObject, m_rai);

					const xdl::XdevLTextureSamplerFilterStage filterStage {xdl::XdevLTextureFilterMin::LINEAR_MIPMAP_LINEAR, xdl::XdevLTextureFilterMag::LINEAR};
					const xdl::XdevLTextureSamplerWrap wrap {xdl::XdevLTextureWrap::REPEAT, xdl::XdevLTextureWrap::REPEAT};

					displacementMap->lock();
					displacementMap->setTextureFilter(filterStage);
					displacementMap->setTextureWrap(wrap);
					displacementMap->generateMipMap();
					displacementMap->generateMipMap();
					displacementMap->unlock();

					imageObject->destroy();

					material->setTexture(XdeLMaterialTextures::DISPLACEMENT_MAP, displacementMap);
					material->setUseDisplacementMap(xdl_true);

					texIndex++;
					displacementFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_DISPLACEMENT, texIndex, &path);
				}


//								//
//				// Import all normal textures.
//				//
//				aiReturn heightFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_HEIGHT, texIndex, &path);
//				while(heightFound == AI_SUCCESS) {
//					// FIXME The import of the texture server can return nullptr. FIX THIS LINE HERE.
//					IPXdevLTexture displacementMap =soan::TextureServer::Inst()->import(path.data);
//					displacementMap->lock();
//					displacementMap->setTextureFilter(XDEVL_TEXTURE_MAG_FILTER, XDEVL_LINEAR);
//					displacementMap->setTextureFilter(XDEVL_TEXTURE_MIN_FILTER, XDEVL_LINEAR_MIPMAP_LINEAR);
//					displacementMap->generateMipMap();
//					displacementMap->unlock();
//					material->setTexture(Material::DISPLACEMENT_MAP, displacementMap);
//					material->setUseDisplacementMap(xdl_true);
//
//					texIndex++;
//					heightFound = m_aiScene->mMaterials[aimesh->mMaterialIndex]->GetTexture(aiTextureType_HEIGHT, texIndex, &path);
//				}


			}


			auto mesh = std::make_shared<IXdevLMesh>(m_rai);
			mesh->setParentModel(model);
			mesh->setVertexArray(va);
			mesh->setMaterial(material);
			mesh->setNumberOfFaces(m_numFaces);
			mesh->setNumberOfVertices(m_numVertices);
			model->add(mesh);

			return RET_SUCCESS;
		}


		void XdevLAssimp::getBoundingBoxNode(const aiNode* nd,
		                                     tmath::vec3* min,
		                                     tmath::vec3* max,
		                                     aiMatrix4x4* trafo
		                                    ) {
			aiMatrix4x4 prev;
			unsigned int n = 0, t;

			prev = *trafo;
			aiMultiplyMatrix4(trafo,&nd->mTransformation);

			for(; n < nd->mNumMeshes; ++n) {
				const aiMesh* mesh = m_aiScene->mMeshes[nd->mMeshes[n]];
				for(t = 0; t < mesh->mNumVertices; ++t) {

					aiVector3D tmp = mesh->mVertices[t];
					aiTransformVecByMatrix4(&tmp,trafo);

					min->x = std::min(min->x,tmp.x);
					min->y = std::min(min->y,tmp.y);
					min->z = std::min(min->z,tmp.z);

					max->x = std::max(max->x,tmp.x);
					max->y = std::max(max->y,tmp.y);
					max->z = std::max(max->z,tmp.z);
				}
			}

			for(n = 0; n < nd->mNumChildren; ++n) {
				getBoundingBoxNode(nd->mChildren[n],min,max,trafo);
			}
			*trafo = prev;
		}

		XdevLAABB XdevLAssimp::getBoundingBox() {
			XDEVL_ASSERT(nullptr != m_rai, "You must initialize first.");
			aiMatrix4x4 trafo;
			aiIdentityMatrix4(&trafo);
			tmath::vec3 min, max;

			min.x = min.y = min.z =  1e10f;
			max.x = max.y = max.z = -1e10f;

			getBoundingBoxNode(m_aiScene->mRootNode, &min, &max, &trafo);

			XdevLAABB aabb {};
			aabb.setMin(min);
			aabb.setMax(max);

			return aabb;
		}
	}

}
