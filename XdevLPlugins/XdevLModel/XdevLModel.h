/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MODEL_H
#define XDEVL_MODEL_H

#include <XdevLModel/XdevLMesh.h>
#include <XdevLModel/XdevLMaterial.h>
#include <XdevLGraphics/3D/XdevLAABB.h>

namespace xdl {

	namespace gfx {

		/**
		 * @class XdevLModel
		 * @brief Represents a full 3D object.
		 */
		class XdevLModel {

			public:
				XdevLModel(IPXdevLRAI rai)
					: m_rai(rai) {
				}

				XdevLModel(IPXdevLRAI rai, const XdevLID& id)
					: m_rai(rai) {
				}

				IPXdevLMesh& add(IPXdevLMesh mesh) {
					m_meshes.push_back(mesh);
					return m_meshes.back();
				}

				std::vector<IPXdevLMesh>& getMeshArray() {
					return m_meshes;
				}

				IPXdevLMesh getMesh(xdl_uint idx) {
					return m_meshes[idx];
				}

				xdl::xdl_uint getNumberOfVertices() {
					xdl::xdl_uint tmp = 0;
					for(auto& mesh : m_meshes) {
						tmp += mesh->getNumberOfVertices();
					}
					return tmp;
				}

				xdl::xdl_uint getNumberOfFaces() {
					xdl::xdl_uint tmp = 0;
					for(auto& mesh : m_meshes) {
						tmp += mesh->getNumberOfFaces();
					}
					return tmp;
				}

				std::shared_ptr<XdevLModel> refCopy() {
					std::shared_ptr<XdevLModel> m(new XdevLModel(m_rai));

					for(auto& mesh : m_meshes) {
						//mesh->setMaterial(mesh->getMaterialRef());
						m->m_meshes.push_back(mesh);
					}

					m->m_aabb = m_aabb;

					return m;
				}

				std::shared_ptr<XdevLModel> copy() {
					auto model = std::make_shared<XdevLModel>(m_rai);

					for(auto& mesh : m_meshes) {
						auto newmaterial = std::shared_ptr<xdl::gfx::XdevLMaterial>(mesh->getMaterial()->copy());
						auto newmesh = std::shared_ptr<XdevLMesh>(mesh->refCopy());
						mesh->setMaterial(newmaterial);
						model->m_meshes.push_back(newmesh);
					}

					model->m_aabb = m_aabb;

					return model;
				}

//		xdl_int save(IPSerializer& serializer) {
//			//
//			// Write ID of this model.
//			// //
//			auto result = serializer->writeID(getID());
//
//			//
//			// Write number of meshes.
//			//
//			xdl_uint64 size = m_meshes.size();
//			serializer->writeU64(size);
//
//			//
//			// Write the meshes.
//			//
//			for(auto mesh: m_meshes) {
//				result += mesh->save(serializer);
//			}
//			return result;
//		}
//
//		xdl_int load(IPDeserializer& deserializer) {
//
//			//
//			// Clear all meshes we have so far.
//			//
//			m_meshes.clear();
//
//			//
//			// Read ID.
//			//
//			auto result = deserializer->readID(getID());
//
//			//
//			// Get the number of meshes.
//			//
//			xdl_uint64 size = 0;
//			deserializer->readU64(size);
//
//			//
//			// Create all meshes.
//			//
//			for(xdl_uint64 idx = 0; idx < size; idx++) {
//				auto mesh = std::make_shared<Mesh>();
//				mesh->load(deserializer);
//				m_meshes.push_back(mesh);
//			}
//
//			return result;
//		}

				xdl::xdl_float getBoundingBoxX() {
					return(m_aabb.getMax().x - m_aabb.getMin().x);
				}

				xdl::xdl_float getBoundingBoxY() {
					return(m_aabb.getMax().y - m_aabb.getMin().y);
				}

				xdl::xdl_float getBoundingBoxZ() {
					return(m_aabb.getMax().z - m_aabb.getMin().z);
				}

			private:
				IPXdevLRAI m_rai;
				std::vector<IPXdevLMesh> m_meshes;
				XdevLAABB m_aabb;

		};

		using IXdevLModel = XdevLModel;
		using IPXdevLModel = std::shared_ptr<XdevLModel>;

	}
}

#endif
