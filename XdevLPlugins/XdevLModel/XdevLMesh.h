/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MESH_H
#define XDEVL_MESH_H

#include <XdevLRAI/XdevLRAI.h>
#include <XdevLModel/XdevLMaterial.h>
#include <XdevLModel/XdevLProceduralSystem.h>
#include <XdevLGraphics/3D/XdevLAABB.h>

namespace xdl {

	namespace gfx {

		enum XdevLVertexItemBinding {
			VERTEX_POSITION=0,
			VERTEX_NORMAL=1,
			VERTEX_TANGENT=2,
			VERTEX_BITANGENT=3,
			VERTEX_COLOR=4,
			VERTEX_TEXTURE_COORD0=9,
			VERTEX_TEXTURE_COORD1=10,
		};

		/**
		 * @class XdevLMesh
		 * @brief A mesh holds vertex and material data.
		 */
		class XdevLMesh {
			public:
				XdevLMesh(const IPXdevLRAI& rai)
					: m_rai(rai)
					, m_vertexDeclaration(nullptr)
					, m_vertexArray(nullptr)
					, m_numberOfFaces(0)
					, m_numberOfVertices(0)
					, m_primitive(XDEVL_PRIMITIVE_TRIANGLES) {
					m_vertexDeclaration = m_rai->createVertexDeclaration();
					m_vertexArray = m_rai->createVertexArray();
					m_material = std::make_shared<XdevLMaterial>();
				}

				XdevLMesh(const IPXdevLRAI& rai, const XdevLID& id)
					: XdevLMesh(rai) {
				}

				XdevLMesh* refCopy() {
					auto m = new XdevLMesh(m_rai);
					m->m_parentModel = m_parentModel;
					m->m_vertexArray = m_vertexArray;
					m->m_material = m_material;
					m->m_numberOfFaces = m_numberOfFaces;
					m->m_numberOfVertices = m_numberOfVertices;
					m->m_primitive = m_primitive;
					return m;
				}

				void setVertexArray(IPXdevLVertexArray vertexArray) {
					m_vertexArray = vertexArray;
				}

				IPXdevLVertexArray getVertexArray() {
					return m_vertexArray;
				}

				IPXdevLMaterial& setMaterial(IPXdevLMaterial material) {
					m_material = material;
					return m_material;
				}

				IPXdevLMaterial& getMaterial() {
					return m_material;
				}

				void add(xdl_uint number, XdevLBufferElementTypes size, xdl_uint shader_attribute) {
					m_vertexDeclaration->add(number, size, shader_attribute);
				}

				void init(std::vector<xdl_uint8*>& list, xdl_uint numberOfVertices) {
					m_vertexArray->init(static_cast<xdl_uint8>(list.size()), list.data(), numberOfVertices, m_vertexDeclaration);
				}

				void init(IPXdevLVertexBuffer vb, IPXdevLIndexBuffer ib) {
					m_vertexArray->init(vb, ib, m_vertexDeclaration);
				}

				void init(IPXdevLIndexBuffer ib) {
					m_vertexArray->add(ib);
				}

				const IPXdevLVertexDeclaration& getVertexDeclaration() const {
					return m_vertexDeclaration;
				}

				xdl_uint getNumberOfFaces() {
					return m_numberOfFaces;
				}

				xdl_uint getNumberOfVertices() {
					return m_numberOfVertices;
				}

				XdevLPrimitiveType getPrimitive() {
					return m_primitive;
				}

				void setParentModel(const IPXdevLModel& model) {
					m_parentModel = model;
				}

				void setNumberOfFaces(xdl_uint numberOfFaces) {
					m_numberOfFaces = numberOfFaces;
				}

				void setNumberOfVertices(xdl_uint numberOfVertices) {
					m_numberOfVertices = numberOfVertices;
				}

//		xdl_int save(IPSerializer& serializer) {
//
//			//
//			// Read id ID of this mesh.
//			//
//			auto result = serializer->writeID(getID());
//
//			//
//			// Read AABB information.
//			//
//			result += serializer->writeAABB(getBoundingBox());
//
//			//
//			// Read the material information.
//			//
//			result += m_material->save(serializer);
//
//
//			//
//			// Write all vertex information.
//			//
//			serializer->writeVertexDeclaration(m_vertexArray->getVertexDeclarationRef());
//
//			auto numberOfVertexBuffers = m_vertexArray->getNumberOfVertexBuffers();
//			serializer->writeU64(m_vertexArray->getNumberOfVertexBuffers());
//
//			for(auto idx = 0; idx < numberOfVertexBuffers; idx++) {
//				auto vertexBuffer = m_vertexArray->getVertexBuffer(idx);
//				serializer->writeU64(vertexBuffer->getSize());
//
//				vertexBuffer->lock();
//				auto memory = vertexBuffer->map(XDEVL_BUFFER_READ_ONLY);
//				serializer->write(memory, vertexBuffer->getSize());
//				vertexBuffer->unmap();
//				vertexBuffer->unlock();
//			}
//
//			return result;
//		}
//
//		xdl_int load(IPDeserializer& deserializer) {
//
//			//
//			// Read ID.
//			//
//			auto result = deserializer->readID(getID());
//			if(-1 == result) {
//				XDEVL_MODULEX_ERROR(XdevLMesh, "Couldn't read ID info.\n");
//				return -1;
//			}
//
//			//
//			// Read AABB information.
//			//
//			result = deserializer->readAABB(getBoundingBox());
//			if(-1 == result) {
//				XDEVL_MODULEX_ERROR(XdevLMesh, "Couldn't read AABB info.\n");
//				return -1;
//			}
//
//			//
//			// Read material information.
//			//
//			result = m_material->load(deserializer);
//			if(-1 == result) {
//				XDEVL_MODULEX_ERROR(XdevLMesh, "Couldn't read material info.\n");
//				return -1;
//			}
//
//			auto vertexDeclaration = XdevLProceduralSystem::Instance()->createVertexDeclaration();
//			result = deserializer->readVertexDeclaration(vertexDeclaration);
//			if(-1 == result) {
//				XDEVL_MODULEX_ERROR(XdevLMesh, "Couldn't read Vertex Declaration info.\n");
//				return -1;
//			}
//
//			//
//			// Read the number of vertex buffers used in this mesh.
//			//
//			xdl_uint64 numberOfVertexBuffers = 0;
//			result = deserializer->readU64(numberOfVertexBuffers);
//			if(-1 == result) {
//				XDEVL_MODULEX_ERROR(XdevLMesh, "Couldn't read the number of vertices.\n");
//				return -1;
//			}
//
//			if(RET_SUCCESS != m_vertexArray->init()) {
//				XDEVL_MODULEX_ERROR(XdevLMesh, "Couldn't initialize XdevLVertexArray.\n");
//				return -1;
//			}
//
//			for(xdl_uint64 idx = 0; idx < numberOfVertexBuffers; idx++) {
//				xdl_uint64 size = 0;
//				deserializer->readU64(size);
//
//				auto vertexBuffer = XdevLProceduralSystem::Instance()->createVertexBuffer();
//
//				vertexBuffer->init(size);
//
//				vertexBuffer->lock();
//				auto memory = vertexBuffer->map(XDEVL_BUFFER_WRITE_ONLY);
//				deserializer->read(memory, size);
//				vertexBuffer->unmap();
//				vertexBuffer->unlock();
//
//				m_vertexArray->add(vertexBuffer,
//				                   vertexDeclaration->get(idx)->numberOfComponents,
//				                   vertexDeclaration->get(idx)->elementType,
//				                   vertexDeclaration->get(idx)->shaderAttribute);
//			}
//
//			return result;
//		}
			private:
				IPXdevLRAI m_rai;
				IPXdevLVertexDeclaration m_vertexDeclaration;
				IPXdevLVertexArray m_vertexArray;
				IPXdevLModel m_parentModel;
				IPXdevLMaterial m_material;

				xdl_uint m_numberOfFaces;
				xdl_uint m_numberOfVertices;
				XdevLPrimitiveType m_primitive;
				XdevLAABB m_aabb;
		};

		using IXdevLMesh = XdevLMesh;
		using IPXdevLMesh = std::shared_ptr<XdevLMesh>;
	}
}

#endif
