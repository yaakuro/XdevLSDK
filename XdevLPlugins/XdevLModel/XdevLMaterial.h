/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_MATERIAL_H
#define XDEVL_MATERIAL_H

#include <XdevLID.h>
#include <XdevLRAI/XdevLTexture.h>
#include <XdevLModel/XdevLColor.h>

#include <algorithm>

namespace xdl {

	namespace gfx {

		enum class XdeLMaterialTextures {
			DIFFUSE_MAP,
			NORMAL_MAP,
			DISPLACEMENT_MAP,
			SPECULAR_MAP,
			ROUGHNESS_MAP
		};

		/**
		 * @class XdevLMaterial
		 * @brief Describes the properties of a material.
		 */
		class XdevLMaterial {
			public:
				XdevLMaterial() :
					m_roughness(1.0f),
					m_skyBoxTexture(nullptr),
					m_useDiffuseConst(xdl_true),
					m_useNormalConst(xdl_true),
					m_useSpecularConst(xdl_true),
					m_useRoughnessConst(xdl_true),
					m_useDiffuseMap(xdl_false),
					m_useNormalMap(xdl_false),
					m_useDisplacementMap(xdl_false),
					m_useSpecularMap(xdl_false),
					m_useRoughnessMap(xdl_false) {

					for(auto& texture : m_textures) {
						texture = nullptr;
					}

				}

				XdevLMaterial(const XdevLID& id) :
					XdevLMaterial() {
				}

				xdl_uint getNumTextures() {
					return 2;
				}

				xdl_float* getAmbient() {
					return ambient;
				}

				xdl_float* getDiffuse() {
					return diffuse;
				}

				xdl_float* getSpecular() {
					return specular;
				}

				xdl_float getRoughness() {
					return m_roughness;
				}

				XdevLTexture* getTexture(XdeLMaterialTextures textureID) {
					return m_textures[static_cast<xdl_int>(textureID)].get();
				}

				IPXdevLTexture getTextureRef(xdl_uint idx) {
					return m_textures[idx];
				}

				XdevLTextureCube* getSkyBoxTexture() {
					return m_skyBoxTexture.get();
				}

				IPXdevLTextureCube getSkyBoxTextureRef() {
					return m_skyBoxTexture;
				}

				xdl_bool getUseDiffuseMap() {
					return m_useDiffuseMap;
				}

				xdl_bool getUseNormalMap() {
					return m_useNormalMap;
				}

				xdl_bool  getUseDisplacementMap() {
					return m_useDisplacementMap;
				}

				xdl_bool getUseSpecularMap() {
					return m_useSpecularMap;
				}

				xdl_bool getUseRoughnessMap() {
					return m_useRoughnessMap;
				}

				xdl_uint getStateMask() {
					xdl_uint state = 0;
					if(m_useDiffuseConst) {
						state |= 1;
					}
					if(m_useNormalConst) {
						state |= 2;
					}
					if(m_useSpecularConst) {
						state |= 4;
					}
					if(m_useRoughnessConst) {
						state |= 8;
					}
					if(m_useDiffuseMap) {
						state |= 16;
					}
					if(m_useNormalMap) {
						state |= 32;
					}
					if(m_useDisplacementMap) {
						state |= 64;
					}
					if(m_useSpecularMap) {
						state |= 128;
					}
					if(m_useRoughnessMap) {
						state |= 256;
					}
					return state;
				}

				void setUseDiffuseConst(xdl_bool state) {
					m_useDiffuseConst = state;
				}

				void setUseNormalConst(xdl_bool state) {
					m_useNormalConst = state;
				}

				void setUseSpecularConst(xdl_bool state) {
					m_useSpecularConst = state;
				}

				void setRoughnessConst(xdl_bool state) {
					m_useRoughnessConst = state;
				}

				void setUseDiffuseMap(xdl_bool state) {
					m_useDiffuseMap = state;
				}

				void setUseNormalMap(xdl_bool state) {
					m_useNormalMap = state;
				}

				void  setUseDisplacementMap(xdl_bool state) {
					m_useDisplacementMap = state;
				}

				void setUseSpecularMap(xdl_bool state) {
					m_useSpecularMap = state;
				}

				void setRoughnessMap(xdl_bool state) {
					m_useRoughnessMap = state;
				}

				void setAmbient(xdl_float r, xdl_float g, xdl_float b, xdl_float a) {
					ambient[0] = r;
					ambient[1] = g;
					ambient[2] = b;
					ambient[3] = a;
				}

				void setDiffuse(xdl_float r, xdl_float g, xdl_float b, xdl_float a) {
					diffuse[0] = r;
					diffuse[1] = g;
					diffuse[2] = b;
					diffuse[3] = a;
				}

				void setSpecular(xdl_float r, xdl_float g, xdl_float b, xdl_float a) {
					specular[0] = r;
					specular[1] = g;
					specular[2] = b;
					specular[3] = a;
				}


				void setRoughness(xdl_float roughness) {
					m_roughness = roughness;
				}

				void setTexture(XdeLMaterialTextures texture_index, IPXdevLTexture texture) {
					m_textures[static_cast<xdl_int>(texture_index)] = texture;
				}

				void setTexture(IPXdevLTextureCube skyBoxTexture) {
					m_skyBoxTexture = skyBoxTexture;
				}

				/// Returns a referenced copy of the mesh.
				/**
					A referenced copy is a not a full copy. The mesh of this model will
					be shared.
				*/
				XdevLMaterial* refCopy() {
					auto nm = new XdevLMaterial();
					*nm = *this;
					return nm;
				}

				XdevLMaterial* copy() {
					auto nm = new XdevLMaterial();
					nm->diffuse = diffuse;
					nm->ambient = ambient;
					nm->specular = specular;
					nm->m_roughness = m_roughness;
					nm->m_useDiffuseConst = m_useDiffuseConst;
					nm->m_useNormalConst = m_useNormalConst;
					nm->m_useSpecularConst = m_useNormalConst;
					nm->m_useRoughnessConst = m_useNormalConst;
					nm->m_useDiffuseMap = m_useDiffuseMap;
					nm->m_useNormalMap = m_useNormalMap;
					nm->m_useDisplacementMap = m_useDisplacementMap;
					nm->m_useSpecularMap = m_useSpecularMap;
					nm->m_useRoughnessMap = m_useRoughnessMap;
					return nm;
				}
//		xdl_int save(IPSerializer& serializer) {
//			auto result = serializer->writeID(getID());
//			result += serializer->writeColor(m_diffuseColor);
//			return result;
//		}
//
//		xdl_int load(IPDeserializer& deserializer) {
//			auto result = deserializer->readID(getID());
//			result += deserializer->readColor(m_diffuseColor);
//			return result;
//		}


			private:

				// Holds the diffuse part of the material.
				XdevLColor diffuse;

				// Holds the ambient part of the material.
				XdevLColor ambient;

				// Holds the specular part of the material.
				XdevLColor specular;

				// Holds the roughness of the material.
				xdl_float m_roughness;

				// Material info encoded in textures.
				IPXdevLTexture m_textures[7];

				// Sky texture.
				IPXdevLTextureCube m_skyBoxTexture;

				xdl_bool m_useDiffuseConst;
				xdl_bool m_useNormalConst;
				xdl_bool m_useSpecularConst;
				xdl_bool m_useRoughnessConst;

				xdl_bool m_useDiffuseMap;
				xdl_bool m_useNormalMap;
				xdl_bool m_useDisplacementMap;
				xdl_bool m_useSpecularMap;
				xdl_bool m_useRoughnessMap;
		};

		using IXdevLMaterial = XdevLMaterial;
		using IPXdevLMaterial = std::shared_ptr<XdevLMaterial>;

	}
}
#endif
