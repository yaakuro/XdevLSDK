/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_RAYCAST_H
#define XDEVL_RAYCAST_H

#include <XdevLRAI/XdevLViewPort.h>
#include <tm/tm.h>

namespace xdl {

	/**
		@class XdevLRaycast
		@brief Raycasting helper class.
	*/
	class XdevLRaycast {
		public:

			XdevLRaycast();
			~XdevLRaycast();

			/// Converts screen space coordiantes to world space and creates a raycast vector.
			static tmath::vec3 screenToWorld(xdl_float x, xdl_float y, xdl_float nearClipPlane, xdl_bool normalized, const XdevLViewPort& viewPort, tmath::mat4& projectionMatrix, tmath::mat4& viewMatrix);
			static tmath::vec2 worldToScreen(const tmath::vec3& point, const XdevLViewPort& viewPort, tmath::mat4& projectionMatrix, tmath::mat4& viewMatrix);
	};

}

#endif
