/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLRaycast.h"

namespace xdl {

	XdevLRaycast::XdevLRaycast() {

	}

	XdevLRaycast::~XdevLRaycast() {

	}

	tmath::vec3 XdevLRaycast::screenToWorld(xdl_float x, xdl_float y, xdl_float nearClipPlane, xdl_bool normalized, const XdevLViewPort& viewPort, tmath::mat4& projectionMatrix, tmath::mat4& viewMatrix) {

		tmath::vec4 clipSpace {};
		if(normalized) {
			clipSpace = tmath::vec4(
			                x,
			                y,
							2.0f * nearClipPlane - 1.0f,
							1.0f
			            );
		} else {
			// Convert screen space coordinates into normalized device coordinates [-1, 1].
			clipSpace = tmath::vec4(
			                2.0f * ((x - viewPort.x) /viewPort.width) - 1.0f,
			                2.0f * ((y - viewPort.y) /viewPort.height) - 1.0f,
							2.0f * nearClipPlane - 1.0f,
							1.0f
			            );
		}
		// Convert into eye space.
		tmath::mat4 projectionView = projectionMatrix * viewMatrix;
		auto inv = tmath::inverse(projectionView);
		auto rayWorld = inv * clipSpace;
		if (rayWorld.w == 0.0f) {
			rayWorld = tmath::vec3();
			return rayWorld;
		}
		auto winv = 1.0f / rayWorld.w;
		return rayWorld * winv;
	}

	tmath::vec2 XdevLRaycast::worldToScreen(const tmath::vec3& point, const XdevLViewPort& viewPort, tmath::mat4& projectionMatrix, tmath::mat4& viewMatrix) {
		auto projectionView = projectionMatrix * viewMatrix;
		auto tmpPoint = projectionView * tmath::vec4(point, 1.0f);
		
		tmath::vec2 tmp {};
		tmp[0] = ((( tmpPoint.x + 1.0f ) / 2.0f) *  viewPort.width );
		tmp[1] = ((( tmpPoint.y + 1.0f ) / 2.0f) *  viewPort.height );
		return tmp;
	}
}
