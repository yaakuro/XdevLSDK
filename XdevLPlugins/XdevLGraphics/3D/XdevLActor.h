/*
	Copyright (c) 2015 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.


*/

#ifndef XDEVL_ACTOR_H
#define XDEVL_ACTOR_H

#include <XdevLID.h>
#include <XdevLModel/XdevLModel.h>
#include <XdevLGraphics/3D/XdevLTransformable.h>

namespace xdl {

	struct XdevLActorContructParameter {
		XdevLString name;
		gfx::IPXdevLModel model = nullptr;
		xdl_bool lit = xdl_true;
	};

	/**
	 * @class XdevLActor
	 * @brief A item that can be placed in the render scene.
	 */
	class XdevLActor : public XdevLTransformable {
		public:
			using Super = XdevLTransformable;

			XdevLActor(const XdevLActorContructParameter& parameter);

			/// Returns the name of this actor.
			const XdevLString& getName() const;

			/// Sets the model that can be rendered.
			void setModel(gfx::IPXdevLModel model);

			/// Returns the model that can be rendered.
			const gfx::IPXdevLModel& getModel() const;

			/// Is this actor if rendered lit?
			xdl_bool isLit() const;

			/// Do we have to render this actor?
			xdl::xdl_bool isRenderingEnabled();

			/// Set if this actor should be rendered.
			void setRenderingEnabled(xdl::xdl_bool state);

			/// Does this actor cast shadow?
			xdl::xdl_bool getCastShadow();

			/// Set if this actor should cast shadow.
			void setCastShadow(xdl::xdl_bool state);

		private:

			XdevLID m_id;
			gfx::IPXdevLModel m_model;
			xdl_bool m_lit;
			xdl_bool m_renderingEnabled;
			xdl_bool m_castShadow;
	};

}


#endif
