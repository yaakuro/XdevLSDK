/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SKYBOX_H
#define XDEVL_SKYBOX_H

#include <XdevLRAI/XdevLRAI.h>
#include <XdevLRAI/XdevLTextureCube.h>
#include <XdevLModel/XdevLMaterial.h>
#include <XdevLImage/XdevLImageServer.h>

namespace xdl {

	namespace gfx {

		/**
			@class XdevLSkyBox
			@brief Represents a SkyBox that holds 6 Textures as a Cube Texture.
		*/
		class XdevLSkyBox  {
			public:

				XdevLSkyBox(IPXdevLRAI rai, IPXdevLArchive archive, IPXdevLImageServer imageServer);

				virtual ~XdevLSkyBox();

				/// Initialize the XdevLSkyBox.
				xdl_int init();

				/// Returns the Cube Texture of this XdevLSkyBox.
				IPXdevLTextureCube getSkyBoxTexture();

				/// Returns the material.
				XdevLMaterial& getMaterial();

				/// Render the XdevLSkyBox.
				void render();

				void setSkyBoxBaseFolderName(const XdevLFileName& baseFolderName);

			private:

				IPXdevLRAI m_rai;
				IPXdevLArchive m_archive;
				IPXdevLImageServer m_imageServer;
				IPXdevLVertexArray	m_vertexArray;
				IPXdevLVertexBuffer m_vertexBuffer;
				IPXdevLTextureCube	m_textureCube;
				XdevLMaterial* m_material;
				XdevLFileName m_skyboxBaseFileName;

		};
	}
}

#endif
