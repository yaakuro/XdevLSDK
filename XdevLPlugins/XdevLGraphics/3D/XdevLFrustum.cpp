/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLGraphics/3D/XdevLFrustum.h>
#include <XdevLGraphics/3D/XdevLCamera.h>
#include <XdevLGraphics/3D/XdevLSphere.h>

namespace xdl {

	XdevLFrustum::XdevLFrustum() {

	}

	XdevLFrustum::~XdevLFrustum() {

	}

	tmath::matrix<xdl_float, 4, 6>& XdevLFrustum::getFrustumMatrix() {
		return m_frustumMatrix;
	}

	tmath::vec3& XdevLFrustum::getPoints(xdl_uint idx) {
		return m_points[idx];
	}

	void XdevLFrustum::update(IPXdevLCamera camera, xdl_bool normalize) {

		auto projViewMatrix = camera->getTransformationMatrix() * camera->getProjectionMatrix();

		m_points[0] =  tmath::vec3();
		m_points[1] =  tmath::vec3(projViewMatrix.row(0)*3);
		m_points[2] =  tmath::vec3(projViewMatrix.row(1)*3);
		m_points[3] =  tmath::vec3(projViewMatrix.row(2)*3);


		//
		// Right Plane.
		//
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::RIGHT), 0) = projViewMatrix.xw - projViewMatrix.xx;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::RIGHT), 1) = projViewMatrix.yw - projViewMatrix.yx;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::RIGHT), 2) = projViewMatrix.zw - projViewMatrix.zx;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::RIGHT), 3) = projViewMatrix.ww - projViewMatrix.wx;

		//
		// Left Plane.
		//
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::LEFT), 0) = projViewMatrix.xw + projViewMatrix.xx;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::LEFT), 1) = projViewMatrix.yw + projViewMatrix.yx;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::LEFT), 2) = projViewMatrix.zw + projViewMatrix.zx;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::LEFT), 3) = projViewMatrix.ww + projViewMatrix.wx;

		//
		// Bottom Plane.
		//
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::BOTTOM), 0) = projViewMatrix.xw + projViewMatrix.xy;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::BOTTOM), 1) = projViewMatrix.yw + projViewMatrix.yy;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::BOTTOM), 2) = projViewMatrix.zw + projViewMatrix.zy;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::BOTTOM), 3) = projViewMatrix.ww + projViewMatrix.wy;

		//
		// Top Plane.
		//
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::TOP), 0) = projViewMatrix.xw - projViewMatrix.xy;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::TOP), 1) = projViewMatrix.yw - projViewMatrix.yy;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::TOP), 2) = projViewMatrix.zw - projViewMatrix.zy;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::TOP), 3) = projViewMatrix.ww - projViewMatrix.wy;

		//
		// Near Plane.
		//
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::NEAR), 0) = projViewMatrix.xw + projViewMatrix.xz;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::NEAR), 1) = projViewMatrix.yw + projViewMatrix.yz;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::NEAR), 2) = projViewMatrix.zw + projViewMatrix.zz;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::NEAR), 3) = projViewMatrix.ww + projViewMatrix.wz;

		//
		// Far Plane.
		//
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::FAR), 0) = projViewMatrix.xw - projViewMatrix.xz;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::FAR), 1) = projViewMatrix.yw - projViewMatrix.yz;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::FAR), 2) = projViewMatrix.zw - projViewMatrix.zz;
		m_frustumMatrix.at(static_cast<xdl_uint>(XdevLFrustumPlane::FAR), 3) = projViewMatrix.ww - projViewMatrix.wz;

		if(normalize) {
			for(xdl_uint a = 0; a < 6; ++a) {
				auto t = 1.0f/sqrt(m_frustumMatrix.at(a, 0) * m_frustumMatrix.at(a, 0) +
				                   m_frustumMatrix.at(a, 1) * m_frustumMatrix.at(a, 1) +
				                   m_frustumMatrix.at(a, 2) * m_frustumMatrix.at(a, 2));

				m_frustumMatrix.at(a, 0) *= t;
				m_frustumMatrix.at(a, 1) *= t;
				m_frustumMatrix.at(a, 2) *= t;
				m_frustumMatrix.at(a, 3) *= t;
			}
		}
	}

	xdl_bool XdevLFrustum::isPointInside(xdl_float x, xdl_float y, xdl_float z) {
		for(xdl_int p = 0; p < 6; p++) {
			if(m_frustumMatrix.at(p, 0) * x + m_frustumMatrix.at(p, 1) * y + m_frustumMatrix.at(p, 2) * z + m_frustumMatrix.at(p, 3) <= 0) {
				return false;
			}
		}
		return true;
	}

	xdl_bool XdevLFrustum::isPointInside(const tmath::vec3& point) {
		return isPointInside(point.x, point.z, point.z);
	}

	xdl_bool XdevLFrustum::isSphereInside(xdl_float x, xdl_float y, xdl_float z, xdl_float radius) {
		for(xdl_int p = 0; p < 6; p++) {
			if(m_frustumMatrix.at(p, 0) * x + m_frustumMatrix.at(p, 1) * y + m_frustumMatrix.at(p, 2) * z + m_frustumMatrix.at(p, 3) <= -radius) {
				return false;
			}
		}
		return true;
	}

	xdl_bool XdevLFrustum::isSphereInside(const tmath::vec3& point, xdl_float radius) {
		return isSphereInside(point.x, point.y, point.z, radius);
	}

	xdl_bool XdevLFrustum::isSphereInside(const XdevLSphere& sphere) {
		return isSphereInside(sphere.getPosition(), sphere.getRadius());
	}

	xdl_bool XdevLFrustum::isCubeInside(xdl_float x, xdl_float y, xdl_float z, xdl_float size) {

		for(xdl_uint p = 0; p < 6; p++) {
			if(m_frustumMatrix.at(p,0) * (x - size) + m_frustumMatrix.at(p,1) * (y - size) + m_frustumMatrix.at(p,2) * (z - size) + m_frustumMatrix.at(p,3) > 0)
				continue;
			if(m_frustumMatrix.at(p,0) * (x + size) + m_frustumMatrix.at(p,1) * (y - size) + m_frustumMatrix.at(p,2) * (z - size) + m_frustumMatrix.at(p,3) > 0)
				continue;
			if(m_frustumMatrix.at(p,0) * (x - size) + m_frustumMatrix.at(p,1) * (y + size) + m_frustumMatrix.at(p,2) * (z - size) + m_frustumMatrix.at(p,3) > 0)
				continue;
			if(m_frustumMatrix.at(p,0) * (x + size) + m_frustumMatrix.at(p,1) * (y + size) + m_frustumMatrix.at(p,2) * (z - size) + m_frustumMatrix.at(p,3) > 0)
				continue;
			if(m_frustumMatrix.at(p,0) * (x - size) + m_frustumMatrix.at(p,1) * (y - size) + m_frustumMatrix.at(p,2) * (z + size) + m_frustumMatrix.at(p,3) > 0)
				continue;
			if(m_frustumMatrix.at(p,0) * (x + size) + m_frustumMatrix.at(p,1) * (y - size) + m_frustumMatrix.at(p,2) * (z + size) + m_frustumMatrix.at(p,3) > 0)
				continue;
			if(m_frustumMatrix.at(p,0) * (x - size) + m_frustumMatrix.at(p,1) * (y + size) + m_frustumMatrix.at(p,2) * (z + size) + m_frustumMatrix.at(p,3) > 0)
				continue;
			if(m_frustumMatrix.at(p,0) * (x + size) + m_frustumMatrix.at(p,1) * (y + size) + m_frustumMatrix.at(p,2) * (z + size) + m_frustumMatrix.at(p,3) > 0)
				continue;
			return false;
		}
		return true;
	}

	xdl_bool XdevLFrustum::isCubeInside(const tmath::vec3& point, xdl_float size) {
		return isCubeInside(point.x, point.y, point.z, size);
	}


}
