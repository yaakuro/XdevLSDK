/*
	Copyright (c) 2015 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.


*/

#ifndef XDEVL_LIGHT_H
#define XDEVL_LIGHT_H

#include <XdevLGraphics/3D/XdevLActor.h>

namespace xdl {

	enum class XdevLLightType {
		POINT,
		SPOT,
		DIRECTIONAL
	};

	struct XdevLLightContructParameter : public XdevLActorContructParameter {
		XdevLLightType lightType;
	};

	class XdevLLight : public XdevLActor {
		public:
			using Super = XdevLActor;

			XdevLLight(const XdevLLightContructParameter& parameter);

			virtual ~XdevLLight();

			/// Returns the type of the light.
			const XdevLLightType& getType() const;

			/// Returns the intensity of the light.
			xdl_float getIntensity();

			/// Set the intensity of the light.
			void setIntensity(xdl_float intensity);

		private:

			XdevLLightType m_type;
			xdl_float m_intensity;

	};

}


#endif
