/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SPHERE_TRANSFORMATON_GIZMO_H
#define XDEVL_SPHERE_TRANSFORMATON_GIZMO_H

#include <XdevLGraphics/3D/XdevLTransformable.h>

namespace xdl {

	/**
	 * @class XdevLSphereTransformationGizmo
	 * @brief Helper class to rotate 3D objects using the surface of a sphere.
	 */
	class XdevLSphereTransformationGizmo : public XdevLTransformable {
		public:

			/// Rotates the object on the surface of a sphere.
			void rotateSphere(xdl::xdl_float angleX, xdl::xdl_float angleY, xdl::xdl_float x, xdl::xdl_float y);

			/// Sets the sphere radius.
			/**
			 * @param radius Allowed values from [0.0, 1.0]
			 */
			void setSphereRadius(xdl_float radius);

		private:

			xdl::xdl_float m_sphereRadius = 0.4f;
	};
}

#endif
