/*
	Copyright (c) 2015 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.


*/

#ifndef XDEVL_AABB_H
#define XDEVL_AABB_H

#include <XdevLTypes.h>
#include <tm/tm.h>

namespace xdl {

	/**
	 * @class XdevLAABB
	 * @brief Axis aligned bounding box.
	 */
	class XdevLAABB {
		public:

			const tmath::vec3& getMin() const;
			const tmath::vec3& getMax() const;
			xdl_bool isPointInside(const tmath::vec3& p) const;
			xdl_bool intersects(const XdevLAABB& o) const;

			void update(const tmath::vec3& p);

			void setMin(const tmath::vec3& min);
			void setMax(const tmath::vec3& max);

		private:

			tmath::vec3 m_min;
			tmath::vec3 m_max;
	};

}


#endif
