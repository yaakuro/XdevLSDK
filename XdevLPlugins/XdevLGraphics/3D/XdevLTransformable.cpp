/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLGraphics/3D/XdevLTransformable.h>

namespace xdl {

	XdevLTransformable::XdevLTransformable()
		: m_transformation(nullptr) {
		//TODO This will be replaced later by a pool manager.
		//
		m_transformation = new XdevLTransformation();

		m_transformation->m_position = tmath::vec3(0.0f, 0.0f, 0.0f);
		m_transformation->m_scale = tmath::vec3(1.0f, 1.0f, 1.0f);

	}

	XdevLTransformable::~XdevLTransformable() {
		if(nullptr != m_transformation) {
			delete m_transformation;
			m_transformation = nullptr;
		}
	}

	tmath::vec3 XdevLTransformable::getForwardDirection() {
		tmath::vec3 tmp = m_transformation->m_orientation * tmath::vec3(0.0f, 0.0f, 1.0f);
		tmath::normalize(tmp);
		return tmp;
	}

	tmath::vec3 XdevLTransformable::getUpDirection() {
		tmath::vec3 tmp = m_transformation->m_orientation * tmath::vec3(0.0f, 1.0f, 0.0f);
		tmath::normalize(tmp);
		return tmp;
	}

	tmath::vec3 XdevLTransformable::getRightDirection() {
		tmath::vec3 tmp = m_transformation->m_orientation * tmath::vec3(1.0f, 0.0f, 0.0f);
		tmath::normalize(tmp);
		return tmp;
	}

	tmath::vec3 XdevLTransformable::getForwardVector() {
		tmath::vec4 tmp(getTransformationMatrix() * tmath::vec4(0.0f, 0.0f, -1.0f, 1.0f));
		return tmath::vec3(tmp);
	}

	tmath::vec3 XdevLTransformable::getRightVector() {
		tmath::vec4 tmp(getTransformationMatrix() * tmath::vec4(1.0f, 0.0f, 0.0f, 1.0f));
		return tmath::vec3(tmp);
	}

	tmath::vec3 XdevLTransformable::getUpVector() {
		tmath::vec4 tmp(getTransformationMatrix() * tmath::vec4(0.0f, 1.0f, 0.0f, 1.0f));
		return tmath::vec3(tmp);
	}

	tmath::vec3 XdevLTransformable::getPosition() {
		return m_transformation->m_position;
	}

	tmath::quat XdevLTransformable::getOrientation() {
		return m_transformation->m_orientation;
	}

	tmath::quat XdevLTransformable::getInverseOrientation() {
		return tmath::inverse(m_transformation->m_orientation);
	}

	tmath::vec3  XdevLTransformable::getScale() {
		return m_transformation->m_scale;
	}

	tmath::mat4 XdevLTransformable::getOrientationMatrix() {
		tmath::mat4 tmp = tmath::mat4::identity();
		tmath::convert(m_transformation->m_orientation, tmp);
		return tmp;
	}

	tmath::mat4 XdevLTransformable::getTransformationMatrix() {
		tmath::mat4 rotation = tmath::mat4::identity();
		tmath::mat4 trans = tmath::mat4::identity();
		tmath::mat4 sc = tmath::mat4::identity();

		tmath::translate(m_transformation->m_position, trans);
		tmath::scale(m_transformation->m_scale, sc);

		tmath::convert(m_transformation->m_orientation, rotation);

		return (trans * rotation * sc);

	}

	tmath::mat4 XdevLTransformable::getInverseTransformationMatrix() {

		// We have to calculate the inverse of the transformation.
		// The transformation                = Translation * Rotation*Scale
		// The inverse of the transformation = Scale^-1 * Rotation^-1 * Translation^-1

		tmath::mat4 rotation = tmath::mat4::identity();
		tmath::mat4 trans = tmath::mat4::identity();
		tmath::mat4 sc = tmath::mat4::identity();
		tmath::quat qtmp = tmath::quat::identity();

		// Create inverse scale matrix;
		sc.xx = 1.0f/m_transformation->m_scale.x;
		sc.yy = 1.0f/m_transformation->m_scale.y;
		sc.zz = 1.0f/m_transformation->m_scale.z;
		sc.ww = 1.0f;

		// Create inverse rotation matrix.
		qtmp = tmath::inverse(m_transformation->m_orientation);
		tmath::convert(qtmp, rotation);

		// Create inverse translation matrix.
		tmath::translate(-m_transformation->m_position, trans);

		return (sc * rotation * trans);
	}

	void XdevLTransformable::setPosition(xdl::xdl_float x, xdl::xdl_float y, xdl::xdl_float z) {
		m_transformation->m_position.x = x;
		m_transformation->m_position.y = y;
		m_transformation->m_position.z = z;
	}

	void  XdevLTransformable::setPosition(const tmath::vec3& position) {
		m_transformation->m_position = position;
	}

	void XdevLTransformable::setScale(xdl::xdl_float sx, xdl::xdl_float sy, xdl::xdl_float sz) {
		m_transformation->m_scale.x = sx;
		m_transformation->m_scale.y = sy;
		m_transformation->m_scale.z = sz;
	}


	void XdevLTransformable::setScale(const tmath::vec3& scale) {
		m_transformation->m_scale = scale;
	}

	void XdevLTransformable::setOrientation(xdl::xdl_float x, xdl::xdl_float y, xdl::xdl_float z, xdl::xdl_float w) {
		m_transformation->m_orientation.x = x;
		m_transformation->m_orientation.y = y;
		m_transformation->m_orientation.z = z;
		m_transformation->m_orientation.w = w;
	}

	void  XdevLTransformable::setOrientation(const tmath::quat& orientation) {
		m_transformation->m_orientation = orientation;
	}

	void XdevLTransformable::moveRelative(xdl::xdl_float x, xdl::xdl_float y, xdl::xdl_float z) {
		m_transformation->m_position.x += x;
		m_transformation->m_position.y += y;
		m_transformation->m_position.z += z;
	}

	void XdevLTransformable::moveRelative(const tmath::vec3& distance) {
		m_transformation->m_position += distance;
	}

	void XdevLTransformable::rotateWorldX(xdl::xdl_float angle) {

		// Create rotation quaternion around that axis.
		tmath::quat rotation = tmath::quat::identity();
		tmath::rotate(angle,  tmath::vec3(1.0f, 0.0f, 0.0f), rotation);

		// Rotate object.
		m_transformation->m_orientation = rotation * m_transformation->m_orientation;
		tmath::normalize(m_transformation->m_orientation);
	}

	void XdevLTransformable::rotateWorldY(xdl::xdl_float angle) {

		// Create rotation quaternion around that axis.
		tmath::quat rotation = tmath::quat::identity();
		tmath::rotate(angle, tmath::vec3(0.0f, 1.0f, 0.0f), rotation);

		// Rotate object.
		m_transformation->m_orientation = rotation * m_transformation->m_orientation;
		tmath::normalize(m_transformation->m_orientation);
	}

	void XdevLTransformable::rotateWorldZ(xdl::xdl_float angle) {

		// Create rotation quaternion around that axis.
		tmath::quat rotation = tmath::quat::identity();
		tmath::rotate(angle, tmath::vec3(0.0f, 0.0f, 1.0f), rotation);

		// Rotate object.
		m_transformation->m_orientation = rotation * m_transformation->m_orientation;
		tmath::normalize(m_transformation->m_orientation);
	}

	void  XdevLTransformable::rotateLocalX(xdl::xdl_float angle) {

		// Create x axis vector in global space.
		tmath::vec3 tmp = m_transformation->m_orientation * tmath::vec3(1.0f, 0.0f, 0.0f);

		// Create rotation quaternion around that axis.
		tmath::quat rotation = tmath::quat::identity();
		tmath::rotate(angle, tmp, rotation);

		// Rotate object.
		m_transformation->m_orientation = rotation * m_transformation->m_orientation;
		tmath::normalize(m_transformation->m_orientation);
	}

	void  XdevLTransformable::rotateLocalY(xdl::xdl_float angle) {

		// Create x axis vector in global space.
		tmath::vec3 tmp = m_transformation->m_orientation * tmath::vec3(0.0f, 1.0f, 0.0f);

		// Create rotation quaternion around that axis.
		tmath::quat rotation = tmath::quat::identity();
		tmath::rotate(angle, tmp, rotation);

		// Rotate object.
		m_transformation->m_orientation = rotation*m_transformation->m_orientation;
		tmath::normalize(m_transformation->m_orientation);

	}


	void  XdevLTransformable::rotateLocalZ(xdl::xdl_float angle) {

		// Create x axis vector in global space.
		tmath::vec3 tmp = m_transformation->m_orientation * tmath::vec3(0.0f, 0.0f, 1.0f);

		// Create rotation quaternion around that axis.
		tmath::quat rotation = tmath::quat::identity();
		tmath::rotate(angle, tmp, rotation);

		// Rotate object.
		m_transformation->m_orientation = rotation*m_transformation->m_orientation;
		tmath::normalize(m_transformation->m_orientation);
	}

}
