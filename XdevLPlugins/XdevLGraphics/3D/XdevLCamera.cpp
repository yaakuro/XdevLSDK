/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLCamera.h"

namespace xdl {


	XdevLCamera::XdevLCamera(const XdevLCameraContructParameter& parameter)
		: XdevLActor(parameter)
		, m_type(XdevLCameraProjectionType::PERSPECTIVE)
		, m_viewPort(parameter.viewPort)
		, m_trackedObject(nullptr)
		, m_pitch(0.0f)
		, m_heading(0.0f)
		, m_roll(0.0f)
		, m_forwardSpeed(10.0f)
		, m_sideSpeed(10.0f)
		, m_upSpeed(10.0f)
		, m_slerpSpeed(10.0f)
		, m_viewPortSet(xdl_false)
		, m_nearClipPlane(1.0f) {

		setProjection(m_type);

	}

	XdevLCamera::~XdevLCamera() {
	}

	xdl_float XdevLCamera::getNearClipPlane() const {
		return m_nearClipPlane;
	}

	const XdevLViewPort& XdevLCamera::getViewPort() const {
		return m_viewPort;
	}

	void  XdevLCamera::setViewPort(const XdevLViewPort& viewPort) {
		m_viewPort = viewPort;
		m_viewPortSet = xdl_true;
		updateProjectionMatrix();
	}

	void XdevLCamera::setProjection(XdevLCameraProjectionType type) {
		m_type = type;
		updateProjectionMatrix();
	}

	void XdevLCamera::updateProjectionMatrix() {
		if(m_type == XdevLCameraProjectionType::PERSPECTIVE) {
			xdl_float aspect_ratio = (xdl_float)m_viewPort.height/(xdl_float)m_viewPort.width;
			tmath::perspective(45.0f, aspect_ratio, m_nearClipPlane, 1000.0f, m_projection);
			//tmath::frustum(-0.35f, 0.35f, -0.25f, 0.25f, 0.5f, 10000.0f, m_projection);
		} else if(m_type == XdevLCameraProjectionType::ORTHO) {
			tmath::ortho(-10.0f, 10.0f, -10.0f, 10.0f, -10.0f, 20.0f, m_projection);
		}
	}

	tmath::mat4& XdevLCamera::getProjectionMatrix() {
		XDEVL_ASSERT(m_viewPortSet != xdl_false, "You have to specify a viewport to the camera.");

		return m_projection;
	}

	tmath::mat4  XdevLCamera::getProjectionsViewMatrix() {
		XDEVL_ASSERT(m_viewPortSet != xdl_false, "You have to specify a viewport to the camera.");

		return (m_projection * getTransformationMatrix());
	}

	void XdevLCamera::fpsView(xdl_float pitch, xdl_float yaw, xdl_float dT) {
		if(m_trackedObject != nullptr) {
			return;
		}

		m_pitch 	+= pitch;
		m_heading += yaw;

		tmath::quat q1,q2;
		q1 = m_transformation->m_orientation;
		tmath::euler_to_quaternion(m_heading, m_pitch, q2);

		tmath::slerp(q1, q2, m_slerpSpeed*dT, m_transformation->m_orientation);
	}

	void XdevLCamera::lookAt(tmath::vec3 target, tmath::vec3 up) {
		if(m_trackedObject != nullptr) {
			return;
		}
		tmath::mat4 tmp;
		tmp = tmath::look_at(m_transformation->m_position, target, up, tmp);

		tmath::convert(tmp, m_transformation->m_orientation);
	}

	void XdevLCamera::moveForward(xdl_float value) {
		if(m_trackedObject != nullptr) {
			return;
		}
		m_transformation->m_position += m_transformation->m_orientation * tmath::vec3(0.0f, 0.0f, -m_forwardSpeed)*value;
	}

	void XdevLCamera::moveSide(xdl_float value) {
		if(m_trackedObject != nullptr) {
			return;
		}
		m_transformation->m_position += m_transformation->m_orientation * tmath::vec3(m_sideSpeed, 0.0f, 0.0f)*value;
	}

	void XdevLCamera::moveUp(xdl_float value) {
		if(m_trackedObject != nullptr) {
			return;
		}
		m_transformation->m_position +=  m_transformation->m_orientation * tmath::vec3(0.0f, m_upSpeed, 0.0f)*value;
	}

	void XdevLCamera::doRoll(xdl_float value) {
		if(m_trackedObject != nullptr) {
			return;
		}
		rotateLocalZ(value);
	}

	void XdevLCamera::doPitch(xdl_float value) {
		if(m_trackedObject != nullptr) {
			return;
		}
		rotateLocalX(value);
	}

	void XdevLCamera::doYaw(xdl_float value) {
		if(m_trackedObject != nullptr) {
			return;
		}
		rotateLocalY(value);
	}

	void XdevLCamera::setForwardSpeed(xdl_float forwardSpeed) {
		m_forwardSpeed = forwardSpeed;
	}

	void XdevLCamera::setSideSpeed(xdl_float sideSpeed) {
		m_sideSpeed = sideSpeed;
	}

	void XdevLCamera::setUpSpeed(xdl_float upSpeed) {
		m_upSpeed = upSpeed;
	}

	void XdevLCamera::startTrackObject(XdevLTransformable* moveable) {
		m_trackedObject = moveable;
	}

	void XdevLCamera::stopTrackObject() {
		m_trackedObject = nullptr;

		tmath::quaternion_to_euler(m_transformation->m_orientation, m_pitch, m_heading, m_roll);
	}

	void  XdevLCamera::setSLERPSpeed(xdl_float slerp) {
		m_slerpSpeed = slerp;
	}

	bool XdevLCamera::isTrackingModeActive() {
		if(m_trackedObject != nullptr) {
			return true;
		}

		return false;
	}

	void XdevLCamera::setTrackingProperties(xdl_float heading, xdl_float pitch, xdl_float distance, xdl_float dT) {
		if(m_trackedObject == nullptr) {
			return;
		}

		//
		// This rotation represents an offset rotation for the camera around the object.
		//
		tmath::quat qtmp;
		tmath::euler_to_quaternion(tmath::d2r(heading), tmath::d2r(pitch), qtmp);

		//
		// To get a smooth rotation from the previous to the new orientation we use slerp.
		//
		tmath::quat q1,q2,qresult;

		// Get previous orientation.
		q1 = m_transformation->m_orientation;

		// Get target orientation.
		q2 = m_trackedObject->getOrientation() * qtmp;

		// Do slerp.
		tmath::slerp(q1, q2, m_slerpSpeed*dT, qresult);

		// This is the new position for the camera which is around the tracked object.
		// The vector (0.0f, 0.0f, distance) is in the tracked object space the back position.
		// We rotate that vector by the offset rotation. The multiplication with qresult will
		// transform the tracked object local position into world coordinate.
		// That plus the position of the tracked object is the new position for the camera.
		tmath::vec3 tmp = m_trackedObject->getPosition() + qresult * qtmp * tmath::vec3(0.0f, 0.0f, distance);


		setOrientation(qresult);
		setPosition(tmp);
	}

	XdevLCameraProjectionType XdevLCamera::getProjectionType() const {
		return m_type;
	}

}
