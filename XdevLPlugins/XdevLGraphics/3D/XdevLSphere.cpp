/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLSphere.h"

namespace xdl {

	XdevLSphere::XdevLSphere() {

	}

	const tmath::vec3& XdevLSphere::getPosition() const {
		return m_position;
	}

	xdl_float XdevLSphere::getRadius() const {
		return m_radius;
	}

	void XdevLSphere::setPosition(const tmath::vec3& position) {
		m_position = position;
	}

	void XdevLSphere::setRadius(xdl_float radius) {
		m_radius = radius;
	}

	xdl_bool XdevLSphere::isPointInside(const tmath::vec3& position) const {
		auto distance = std::abs(tmath::norm(m_position - position));
		return distance <= m_radius;
	}

	xdl_bool XdevLSphere::intersects(const XdevLSphere& sphere) const {
		auto distance = tmath::norm(m_position - sphere.m_position);
		return distance <= m_radius + sphere.m_radius;
	}

}
