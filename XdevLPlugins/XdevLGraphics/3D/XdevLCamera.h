/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_CAMERA_H
#define XDEVL_CAMERA_H

#include <XdevLTypes.h>
#include <XdevLGraphics/3D/XdevLActor.h>

namespace xdl {

	/**
		@enum XdevLCameraProjectionType
		@brief Projections type of the camera.
	*/
	enum class XdevLCameraProjectionType {
		ORTHO,		/// Orthographic projection.
		PERSPECTIVE	/// Perspective projection.
	};

	struct XdevLCameraContructParameter : public XdevLActorContructParameter {
		XdevLViewPort viewPort;
	};

	/**
		@class XdevLCamera
		@brief A camera in the virtual space..

		The camera supports 2 modes at the moment.

		-# FPS mode     : You can move the camera using heading and pitch.
		-# Tracking mode: XdevLCamera will track a object at a specific distance.

	*/
	class XdevLCamera : public XdevLActor {
		public:
			using Super = XdevLActor;

			explicit XdevLCamera(const XdevLCameraContructParameter& parameter);

			virtual ~XdevLCamera();

			/// Rotate camera like a First Person game.
			/**
				The angles must be in world coordinate system.
			*/
			void fpsView(xdl_float pitch, xdl_float yaw, xdl_float dT = 1.0f);

			/// Look at a specific point with a specified up vector.
			void lookAt(tmath::vec3 target, tmath::vec3 up);

			/// Move camera forward/backward.
			/**
				@param value Amount to move in forward direction.
			*/
			void moveForward(xdl_float value);

			/// Move camera left/right.
			/**
				@param value Amount to move in forward sideways.
			*/
			void moveSide(xdl_float value);

			/// Move camera up/down.
			/**
				@param value Amount to move in upward direction.
			*/
			void moveUp(xdl_float value);

			/// Roll camera.
			/**
				@param value Amount to do a roll rotation in degrees.
			*/
			void doRoll(xdl_float value);

			/// Pitch camera.
			/**
				@param value Amount to do a pitch rotation in degrees.
			*/
			void doPitch(xdl_float value);

			/// Yaw camera.
			/**
				@param value Amount to do a yaw rotation in degrees.
			*/
			void doYaw(xdl_float value);

			/// Return the cameras projections matrix.
			tmath::mat4& getProjectionMatrix();

			/// Returns the projection view matrix.
			tmath::mat4 getProjectionsViewMatrix();

			/// Returns the ViewPort.
			const XdevLViewPort& getViewPort() const;

			/// Sets the view ports dimensions.
			void setViewPort(const XdevLViewPort& viewPort);

			/// Sets the projections type.
			void setProjection(XdevLCameraProjectionType type);

			/// Start to track object.
			void startTrackObject(XdevLTransformable* moveable);

			/// Stop tracking object.
			void stopTrackObject();

			/// Set tracking object properties.
			void setTrackingProperties(xdl_float heading, xdl_float pitch, xdl_float distance, xdl_float dT);

			/// Returns if the tracking mode is active or not.
			bool isTrackingModeActive();

			/// Sets the forward speed.
			void setForwardSpeed(xdl_float forwardSpeed);

			/// Sets the side speed.
			void setSideSpeed(xdl_float sideSpeed);

			/// Sets the up speed.
			void setUpSpeed(xdl_float upSpeed);

			/// Sets the LERP speed
			void setSLERPSpeed(xdl_float slerp);

			/// Returns the projection type of this camera.
			XdevLCameraProjectionType getProjectionType() const;

			xdl_float getNearClipPlane() const;

		private:

			void updateProjectionMatrix();

		private:

			// Holds the type of projection.
			XdevLCameraProjectionType m_type;

			// Holds the projection matrix.
			tmath::mat4 m_projection;

			XdevLViewPort m_viewPort;

			// Holds the tracked object.
			XdevLTransformable* m_trackedObject;

			xdl_float m_pitch;
			xdl_float m_heading;
			xdl_float m_roll;
			xdl_float m_forwardSpeed;
			xdl_float m_sideSpeed;
			xdl_float m_upSpeed;
			xdl_float m_slerpSpeed;
			xdl_bool m_viewPortSet;
			xdl_float m_nearClipPlane;
	};

}

#endif
