/*
	Copyright (c) 2015 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.


*/

#include "XdevLSkyBox.h"
#include <vector>
#include <string>

namespace xdl {

	namespace gfx {

		XdevLSkyBox::XdevLSkyBox(IPXdevLRAI rai, IPXdevLArchive archive, IPXdevLImageServer imageServer)
			: m_rai(rai)
			, m_archive(archive)
			, m_imageServer(imageServer)
			, m_vertexArray(nullptr)
			, m_vertexBuffer(nullptr)
			, m_material(nullptr)
			, m_skyboxBaseFileName(TEXT("Skybox")) {

		}

		XdevLSkyBox::~XdevLSkyBox() {
			delete m_material;

			//
			// The Cube Texture will be deleted by the Texture Server.
			//

		}

		void XdevLSkyBox::setSkyBoxBaseFolderName(const XdevLFileName& baseFolderName) {
			m_skyboxBaseFileName = baseFolderName;
		}

		xdl_int XdevLSkyBox::init() {

			const XdevLFileName fileNames [] = {
				XdevLFileName(TEXT("right.jpg")),
				XdevLFileName(TEXT("left.jpg")),
				XdevLFileName(TEXT("bottom.jpg")),
				XdevLFileName(TEXT("top.jpg")),
				XdevLFileName(TEXT("back.jpg")),
				XdevLFileName(TEXT("front.jpg"))
			};

			struct imagesInfo {
				std::vector<xdl_uint8*> imageData;
				xdl_uint width;
				xdl_uint height;
			};

			imagesInfo info;

			for(auto& fileName : fileNames) {
				auto skyboxFileName = m_skyboxBaseFileName + XdevLFileName(TEXT("/")) + fileName;
				auto skyboxFile = m_archive->open(xdl::XdevLOpenForReadOnly(), skyboxFileName);
				auto imageObject = m_imageServer->import(skyboxFile);
				if(nullptr == imageObject) {
					// TODO We need to destroy the images here.
					return RET_FAILED;
				}
				info.imageData.push_back(imageObject->Images[0].Buffer);
				// TODO This is not so good. I can be that the images have different size but we don't allow that anyway.
				info.width = imageObject->Images[0].Width;
				info.height = imageObject->Images[0].Height;
			}

			m_textureCube = m_rai->createTextureCube();
			xdl::XdevLCubemapPosition cubemapPositionList[] = {
				xdl::XdevLCubemapPosition::POSITIVE_X,
				xdl::XdevLCubemapPosition::NEGATIVE_X,
				xdl::XdevLCubemapPosition::POSITIVE_Y,
				xdl::XdevLCubemapPosition::NEGATIVE_Y,
				xdl::XdevLCubemapPosition::POSITIVE_Z,
				xdl::XdevLCubemapPosition::NEGATIVE_Z
			};
			m_textureCube->init(info.width, info.height, XdevLTextureFormat::RGBA8, XdevLTexturePixelFormat::BGRA, cubemapPositionList, info.imageData.data());


			m_textureCube->lock();
			m_textureCube->generateMipMap();
			m_textureCube->unlock();


			xdl_float points[] = {

				1.0f, -1.0f, -1.0f,
				1.0f, -1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f,  1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,

				-1.0f, -1.0f,  1.0f,
				-1.0f, -1.0f, -1.0f,
				-1.0f,  1.0f, -1.0f,
				-1.0f,  1.0f, -1.0f,
				-1.0f,  1.0f,  1.0f,
				-1.0f, -1.0f,  1.0f,

				-1.0f,  1.0f, -1.0f,
				-1.0f, -1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,
				1.0f,  1.0f, -1.0f,
				-1.0f,  1.0f, -1.0f,

				-1.0f, -1.0f, 1.0f,
				-1.0f,  1.0f, 1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f, -1.0f,  1.0f,
				-1.0f, -1.0f, 1.0f,

				-1.0f,  1.0f, -1.0f,
				1.0f,  1.0f, -1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				-1.0f,  1.0f,  1.0f,
				-1.0f,  1.0f, -1.0f,

				-1.0f, -1.0f, -1.0f,
				-1.0f, -1.0f,  1.0f,
				1.0f, -1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,
				-1.0f, -1.0f,  1.0f,
				1.0f, -1.0f,  1.0f
			};

			//
			// Create the Vertex Buffer for the SkyBox Mesh.
			//
			auto vd = m_rai->createVertexDeclaration();
			vd->add(3, XdevLBufferElementTypes::FLOAT, 0);

			m_vertexBuffer = m_rai->createVertexBuffer();

			m_vertexBuffer->init((xdl_uint8*)points, vd->vertexSize()*36);

			//
			// Create the Vertex Array for this SkyBox.
			//
			m_vertexArray = m_rai->createVertexArray();
			m_vertexArray->init(m_vertexBuffer, vd);

			m_material = new gfx::XdevLMaterial();
			m_material->setTexture(m_textureCube);

			return RET_SUCCESS;
		}

		void XdevLSkyBox::render() {
			m_rai->setActiveVertexArray(m_vertexArray);
			m_rai->drawVertexArray(XDEVL_PRIMITIVE_TRIANGLES, 36);
		}

		IPXdevLTextureCube  XdevLSkyBox::getSkyBoxTexture() {
			return m_textureCube;
		}

		XdevLMaterial& XdevLSkyBox::getMaterial() {
			return *m_material;
		}
	}
}
