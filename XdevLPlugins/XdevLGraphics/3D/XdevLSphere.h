/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_SPHERE_H
#define XDEVL_SPHERE_H

#include <XdevLTypes.h>
#include <tm/tm.h>

namespace xdl {

	/**
		@class XdevLSphere
		@brief Bounding sphere helper class
	*/
	class XdevLSphere {
		public:

			XdevLSphere();

			const tmath::vec3& getPosition() const;
			xdl_float getRadius() const;

			XDEVL_INLINE xdl_bool isPointInside(const tmath::vec3& position) const;
			XDEVL_INLINE xdl_bool intersects(const XdevLSphere& sphere) const;
			XDEVL_INLINE void setPosition(const tmath::vec3& position);
			XDEVL_INLINE void setRadius(xdl_float radius);

		private:

			tmath::vec3 m_position = tmath::vec3(0.0f, 0.0f, 0.0f);
			xdl_float m_radius = 1.0f;
	};

}

#endif
