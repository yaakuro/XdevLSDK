/*
	Copyright (c) 2015 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.


*/

#include "XdevLAABB.h"

namespace xdl {

	const tmath::vec3& XdevLAABB::getMin() const {
		return m_min;
	}

	const tmath::vec3& XdevLAABB::getMax() const {
		return m_max;
	}

	xdl_bool XdevLAABB::isPointInside(const tmath::vec3& p) const {
		return ((p.x >= m_min.x) && (p.x <= m_max.x) &&
		        (p.y >= m_min.y) && (p.y <= m_max.y) &&
		        (p.z >= m_min.z) && (p.z <= m_max.z));
	}

	xdl_bool XdevLAABB::intersects(const XdevLAABB& o) const {
		return (m_min.x < o.m_max.x) && (m_max.x > o.m_min.x) &&
		       (m_min.y < o.m_max.y) && (m_max.y > o.m_min.y) &&
		       (m_min.z < o.m_max.z) && (m_max.z > o.m_min.z);
	}

	void XdevLAABB::update(const tmath::vec3& p) {
		if(m_max.x < p.x) {
			m_max.x = p.x;
		}
		if(m_max.y < p.y) {
			m_max.y = p.y;
		}
		if(m_max.z < p.z) {
			m_max.z = p.z;
		}
		if(m_min.x > p.x) {
			m_min.x = p.x;
		}
		if(m_min.y > p.y) {
			m_min.y = p.y;
		}
		if(m_min.z > p.z) {
			m_min.z = p.z;
		}
	}

	void XdevLAABB::setMin(const tmath::vec3& min) {
		m_min = min;
	}

	void XdevLAABB::setMax(const tmath::vec3& max) {
		m_max = max;
	}
}
