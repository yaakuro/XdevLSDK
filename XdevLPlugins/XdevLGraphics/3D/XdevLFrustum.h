/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_FRUSTUM_H
#define XDEVL_FRUSTUM_H

#include <XdevLTypes.h>
#include <XdevLGraphics/3D/XdevLGraphics3DFWD.h>

namespace xdl {

	/**
		@enum XdevLFrustumPlane
		@brief Frustum planes.
	*/
	enum class XdevLFrustumPlane {
		LEFT,
		RIGHT,
		TOP,
		BOTTOM,
		NEAR,
		FAR
	};

	/**
		@enum XdevLFrustum
		@brief Frustum helper class.
	*/
	class XdevLFrustum {
		public:

			XdevLFrustum();

			~XdevLFrustum();

			/// Returns the frustum matrix.
			tmath::matrix<xdl_float, 4, 6>& getFrustumMatrix();

			/// Updates the frustum matrix.
			void update(IPXdevLCamera camera, xdl_bool normalize);

			/// Checks if a point is inside the frustum.
			xdl_bool isPointInside(xdl_float x, xdl_float y, xdl_float z);

			/// Checks if a point is inside the frustum.
			xdl_bool isPointInside(const tmath::vec3& point);

			/// Checks if a sphere is inside the frustum.
			xdl_bool isSphereInside(xdl_float x, xdl_float y, xdl_float z, xdl_float radius);

			/// Checks if a sphere is inside the frustum.
			xdl_bool isSphereInside(const tmath::vec3& point, xdl_float radius);

			/// Checks if a sphere is inside the frustum.
			xdl_bool isSphereInside(const XdevLSphere& sphere);

			/// Checks if a bounding box inside the frustum.
			xdl_bool isCubeInside(xdl_float x, xdl_float y, xdl_float z, xdl_float size);

			/// Checks if a bounding box is inside the frustum.
			xdl_bool isCubeInside(const tmath::vec3& point, xdl_float size);

			/// Returns the edge points of the frustum.
			tmath::vec3& getPoints(xdl_uint idx);

		private:

			tmath::matrix<xdl_float, 4, 6> m_frustumMatrix;
			tmath::vec3 m_points[8];

	};

}

#endif // FRUSTUM_H
