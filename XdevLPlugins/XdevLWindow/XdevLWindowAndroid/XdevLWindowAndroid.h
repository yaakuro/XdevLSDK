/*
	Copyright (c) 2005 - 2016 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_WINDOW_ANDROID_H
#define XDEVL_WINDOW_ANDROID_H

#include <tinyxml.h>
#include <XdevLPlatform.h>
#include <XdevLPlugin.h>
#include <XdevLTypes.h>
#include <XdevLPluginImpl.h>
#include <XdevLCoreMediator.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLWindow/XdevLWindowImpl.h>

#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <android/log.h>

namespace xdl {

	static const XdevLString windowPluginName {
		"XdevLWindowAndroid"
	};

	static const XdevLString windowDescription {
		"Support for creating a window for Android."
	};

	static const XdevLString cursorDescription {
		"Support for handling the cursor for Android."
	};

	class XdevLWindowAndroid : public XdevLWindowImpl {
		public:
			XdevLWindowAndroid(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLWindowAndroid();

			//
			// XdevLModule related methods.
			//

			virtual xdl_int init() override;
			virtual xdl_int shutdown() override;
			virtual void* getInternal(const XdevLInternalName& id) override;
			virtual xdl_int update() override;

			//
			// XdevLWindow related methods.
			//

			virtual xdl_int create(const XdevLWindowAttribute& attribute);
			virtual xdl_int create() override;
			virtual const XdevLWindowPosition& getPosition() override;
			virtual const XdevLWindowSize& getSize() override;
			virtual XdevLWindowPosition::type getX() override;
			virtual XdevLWindowPosition::type getY() override;
			virtual XdevLWindowSize::type getWidth() override;
			virtual XdevLWindowSize::type getHeight() override;
			virtual const XdevLWindowTitle& getTitle() override;
			virtual xdl_bool isFullscreen() override;
			virtual xdl_bool isPointerHidden() override;
			virtual void setPosition(const XdevLWindowPosition& position) override;
			virtual void setSize(const XdevLWindowSize& size) override;
			virtual void setX(XdevLWindowPosition::type x) override;
			virtual void setY(XdevLWindowPosition::type y) override;
			virtual void setWidth(XdevLWindowSize::type width) override;
			virtual void setHeight(XdevLWindowSize::type height) override;
			virtual void setTitle(const XdevLWindowTitle& title) override;
			virtual void setFullscreen(xdl_bool state) override;
			virtual void show() override;
			virtual void hide() override;
			virtual xdl_bool isHidden() override;
			virtual void raise() override;
			virtual void setInputFocus() override;
			virtual xdl_bool hasFocus() override;
			virtual void setParent(XdevLWindow* window) override;
			virtual void setType(XdevLWindowTypes type) override;

			//
			// Internal used methods
			//

			ANativeWindow* getNativeWindow() {
				return m_window;
			}

		protected:

		protected:

			ANativeWindow* m_window;

	};


	class XdevLWindowServerAndroid : public XdevLWindowServerImpl {
		public:
			XdevLWindowServerAndroid(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLWindowServerAndroid();

			/// Creates a new window.
			xdl_int create(XdevLWindow** window, const XdevLWindowAttribute& attribute) override;
	};


	class XdevLWindowEventServerAndroid : public XdevLWindowEventServerImpl {
		public:
			XdevLWindowEventServerAndroid(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual xdl_int init() override;
			virtual xdl_int shutdown() override;
			virtual void* getInternal(const XdevLInternalName& id) override;
			virtual xdl_int update() override;
			virtual xdl_int notify(XdevLEvent& event) override;

			virtual xdl_int registerWindowForEvents(XdevLWindow* window) override;
			virtual xdl_int unregisterWindowFromEvents(XdevLWindow* window) override;
			void flush() override;
		private:
			int pollEvents();
	};


	class XdevLCursorAndroid : public XdevLModuleImpl<XdevLCursor>  {
		public:
			virtual ~XdevLCursorAndroid() {}

			XdevLCursorAndroid(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

			virtual xdl_int init() override;
			virtual xdl_int shutdown() override;
			virtual void* getInternal(const XdevLInternalName& id) override;

			virtual xdl_int attach(XdevLWindow* window) override;
			virtual void show();
			virtual void hide();
			virtual void setPosition(xdl_uint x, xdl_uint y);
			virtual xdl_int clip(xdl_uint x, xdl_uint y, xdl_uint width, xdl_uint height);
			virtual void releaseClip() override;
			virtual xdl_int enableRelativeMotion();
			virtual void disableRelativeMotion();
			virtual xdl_bool isRelativeMotionEnabled() override;
		private:
			XdevLWindowAndroid* m_window;

	};

	int androidJNISetupThread();
	JNIEnv* androidJNIGetEnv();
	void androidJNIThreadDestroyed(void* value);

}


#endif
