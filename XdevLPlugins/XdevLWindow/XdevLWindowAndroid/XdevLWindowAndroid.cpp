/*
	Copyright (c) 2005 - 2016 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <iostream>
#include <XdevLCore.h>
#include <XdevLInputSystem.h>
#include <XdevLInput/XdevLKeyboard/XdevLKeyboard.h>
#include <XdevLInput/XdevLMouse/XdevLMouse.h>
#include <XdevLInput/XdevLJoystick/XdevLJoystick.h>
#include "XdevLWindowAndroid.h"
#include <XdevLXstring.h>
#include <XdevLUtils.h>
#include <sstream>
#include <cstddef>
#include <map>


xdl::XdevLModuleDescriptor windowAndroidModuleDescriptor {
	xdl::window_vendor,
	xdl::window_author,
	xdl::window_moduleNames[xdl::XDEVL_WINDOW_MODULE_NAME],
	xdl::window_copyright,
	xdl::windowDescription,
	XDEVLANDROID_MODULE_MAJOR_VERSION,
	XDEVLANDROID_MODULE_MINOR_VERSION,
	XDEVLANDROID_MODULE_PATCH_VERSION
};

xdl::XdevLModuleDescriptor windowAndroidServerModuleDesc {
	xdl::window_vendor,
	xdl::window_author,
	xdl::window_moduleNames[xdl::XDEVL_WINDOW_SERVER_MODULE_NAME],
	xdl::window_copyright,
	xdl::windowServerDescription,
	XDEVLANDROID_SERVER_MODULE_MAJOR_VERSION,
	XDEVLANDROID_SERVER_MODULE_MINOR_VERSION,
	XDEVLANDROID_SERVER_MODULE_PATCH_VERSION,
	xdl::XDEVL_MODULE_STATE_DISABLE_AUTO_DESTROY
};

xdl::XdevLModuleDescriptor windowAndroidEventServerModuleDesc {
	xdl::window_vendor,
	xdl::window_author,
	xdl::window_moduleNames[xdl::XDEVL_WINDOW_EVENT_SERVER_MODULE_NAME],
	xdl::window_copyright,
	xdl::windowServerDescription,
	XDEVLANDROID_EVENT_SERVER_MODULE_MAJOR_VERSION,
	XDEVLANDROID_EVENT_SERVER_MODULE_MINOR_VERSION,
	XDEVLANDROID_EVENT_SERVER_MODULE_PATCH_VERSION,
	xdl::XDEVL_MODULE_STATE_DISABLE_AUTO_DESTROY
};

xdl::XdevLModuleDescriptor cursorAndroidModuleDesc {
	xdl::window_vendor,
	xdl::window_author,
	xdl::window_moduleNames[xdl::XDEVL_CURSOR_MODULE_NAME],
	xdl::window_copyright,
	xdl::windowServerDescription,
	XDEVLANDROID_CURSOR_MODULE_MAJOR_VERSION,
	XDEVLANDROID_CURSOR_MODULE_MINOR_VERSION,
	XDEVLANDROID_CURSOR_MODULE_PATCH_VERSION
};


xdl::XdevLPluginDescriptor windowAndroidPluginDescriptor {
	xdl::windowPluginName,
	xdl::window_moduleNames,
	XDEVLANDROID_MAJOR_VERSION,
	XDEVLANDROID_MINOR_VERSION,
	XDEVLANDROID_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_CREATE_MODULE  {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowAndroid, windowAndroidModuleDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowServerAndroid, windowAndroidServerModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowEventServerAndroid, windowAndroidEventServerModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLCursorAndroid, cursorAndroidModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(windowAndroidPluginDescriptor)

namespace xdl {

	static const XdevLID ButtonPressed("XDEVL_BUTTON_PRESSED");
	static const XdevLID ButtonReleased("XDEVL_BUTTON_RELEASED");
	static const XdevLID MouseButtonPressed("XDEVL_MOUSE_BUTTON_PRESSED");
	static const XdevLID MouseButtonReleased("XDEVL_MOUSE_BUTTON_RELEASED");
	static const XdevLID MouseMotion("XDEVL_MOUSE_MOTION");
	static const XdevLID WindowEvent("XDEVL_WINDOW_EVENT");


	static xdl_int reference_counter = 0;
	static jclass mActivityClass;
	static ANativeWindow* nativeWindow = nullptr;
	static jmethodID midGetNativeSurface;

	XdevLWindowAndroid::XdevLWindowAndroid(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLWindowImpl(parameter, descriptor),
		m_window(nullptr) {
		//
		// Initialize connection to display server just once.
		//
		if(0 == reference_counter) {
			if(xdl::windowEventServer == nullptr) {
				// If there is no even server active, create and activate it.
				xdl::windowEventServer = static_cast<xdl::XdevLWindowEventServer*>(parameter->getMediator()->createModule(xdl::XdevLModuleName("XdevLWindowEventServer"), xdl::XdevLID("XdevLWindowEventServer"), xdl::XdevLPluginName("XdevLWindowAndroid")));
			}

			if(xdl::cursor == nullptr) {
				xdl::cursor = static_cast<xdl::XdevLCursor*>(parameter->getMediator()->createModule(xdl::XdevLModuleName("XdevLCursor"), xdl::XdevLID("XdevLCursor")));
			}
		}
		reference_counter++;
	}

	XdevLWindowAndroid::~XdevLWindowAndroid() {

		// Window got destroyed, did someone shut it down?
		if(m_window != nullptr) {
			shutdown();
		}

		if(reference_counter == 1) {

		}
		reference_counter--;
	}


	xdl_int XdevLWindowAndroid::init() {

		XdevLWindowImpl::init();

		if(create() != RET_SUCCESS) {
			return RET_FAILED;
		}

		XDEVL_MODULE_SUCCESS("SDL window created successfully: " << m_window << std::endl);
		return RET_SUCCESS;
	}

	xdl_int XdevLWindowAndroid::create(const XdevLWindowAttribute& attribute) {
		XdevLWindowImpl::create(attribute);
		return create();
	}

	xdl_int XdevLWindowAndroid::create() {

//		JNIEnv* env = androidJNIGetEnv();
//		jobject s = env->CallStaticObjectMethod(mActivityClass, midGetNativeSurface);
//		m_window = ANativeWindow_fromSurface(env, s);
//		env->DeleteLocalRef(s);

		windowEventServer->registerWindowForEvents(this);

		return RET_SUCCESS;
	}
	void XdevLWindowAndroid::setParent(XdevLWindow* window) {

		XdevLWindowImpl::setParent(window);
	}

	void* XdevLWindowAndroid::getInternal(const XdevLInternalName& id) {
		if(id == xdl::XdevLInternalName("ANDROID_WINDOW")) {
			return (void*)m_window;
		}
		return nullptr;
	}

	xdl_int XdevLWindowAndroid::shutdown() {

		XdevLWindowImpl::shutdown();

		return RET_SUCCESS;
	}

	xdl_int XdevLWindowAndroid::update() {
		return RET_SUCCESS;
	}

	void XdevLWindowAndroid::setSize(const XdevLWindowSize& size) {
		XdevLWindowImpl::setSize(size);
	}

	void XdevLWindowAndroid::setPosition(const XdevLWindowPosition& position) {
		XdevLWindowImpl::setPosition(position);
	}

	void XdevLWindowAndroid::setTitle(const XdevLWindowTitle& title) {
		XdevLWindowImpl::setTitle(title);
	}

	void XdevLWindowAndroid::setFullscreen(xdl_bool state) {
		XDEVL_MODULE_WARNING("Android does not support this function.\n");
	}

	void XdevLWindowAndroid::setType(XdevLWindowTypes type) {
		XDEVL_MODULE_WARNING("Android does not support this function.\n");
	}

	void XdevLWindowAndroid::setX(XdevLWindowPosition::type x) {
	}

	void XdevLWindowAndroid::setY(XdevLWindowPosition::type y) {
	}

	void XdevLWindowAndroid::setWidth(XdevLWindowSize::type width) {
	}

	void XdevLWindowAndroid::setHeight(XdevLWindowSize::type height) {
	}

	XdevLWindowPosition::type XdevLWindowAndroid::getX() {
		return 0;
	}

	XdevLWindowPosition::type XdevLWindowAndroid::getY() {
		return 0;
	}

	XdevLWindowSize::type XdevLWindowAndroid::getWidth() {
		return m_attribute.size.width;
	}

	XdevLWindowSize::type XdevLWindowAndroid::getHeight() {
		return m_attribute.size.height;
	}

	const XdevLWindowSize& XdevLWindowAndroid::getSize() {
		return m_attribute.size;
	}

	const XdevLWindowPosition& XdevLWindowAndroid::getPosition() {
		return m_attribute.position;
	}

	const XdevLWindowTitle& XdevLWindowAndroid::getTitle() {
		return getTitle();
	}

	xdl_bool  XdevLWindowAndroid::isFullscreen() {
		return XdevLWindowImpl::isFullscreen();
	}

	xdl_bool  XdevLWindowAndroid::isPointerHidden() {
		return XdevLWindowImpl::isPointerHidden();
	}

	void  XdevLWindowAndroid::show() {
	}

	void  XdevLWindowAndroid::hide() {
	}

	xdl_bool XdevLWindowAndroid::isHidden() {
		return xdl_false;
	}

	void XdevLWindowAndroid::raise() {
	}

	void XdevLWindowAndroid::setInputFocus() {
	}

	xdl_bool XdevLWindowAndroid::hasFocus() {
		return xdl_false;
	}


	//
	// -------------------------------------------------------------------------
	//

	XdevLWindowServerAndroid::XdevLWindowServerAndroid(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLWindowServerImpl(parameter, descriptor) {

	}

	XdevLWindowServerAndroid::~XdevLWindowServerAndroid() {

	}

	xdl_int XdevLWindowServerAndroid::create(XdevLWindow** window, const XdevLWindowAttribute& attribute) {
		XdevLWindowAndroid* wnd = new XdevLWindowAndroid(nullptr, getDescriptor());
		wnd->create(attribute);
		*window = wnd;
		m_windowList[wnd->getWindowID()] = wnd;
		return RET_SUCCESS;
	}





	XdevLWindowEventServerAndroid::XdevLWindowEventServerAndroid(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLWindowEventServerImpl(parameter, descriptor)
	{}


	xdl_int XdevLWindowEventServerAndroid::registerWindowForEvents(XdevLWindow* window) {
		return XdevLWindowEventServerImpl::registerWindowForEvents(window);
	}

	xdl_int XdevLWindowEventServerAndroid::unregisterWindowFromEvents(XdevLWindow* window) {
		return XdevLWindowEventServerImpl::unregisterWindowFromEvents(window);
	}

	xdl_int XdevLWindowEventServerAndroid::init() {
		XDEVL_MODULE_SUCCESS("Initialized successfully" << std::endl);
		return RET_SUCCESS;
	}

	void* XdevLWindowEventServerAndroid::getInternal(const XdevLInternalName& id) {
		return nullptr;
	}

	xdl_int XdevLWindowEventServerAndroid::shutdown() {
		XDEVL_MODULE_SUCCESS("Shutdown process was successful.\n");
		return RET_SUCCESS;
	}

	xdl_int XdevLWindowEventServerAndroid::update() {
		return pollEvents();
	}

	xdl_int XdevLWindowEventServerAndroid::pollEvents() {
		return RET_SUCCESS;
	}

	xdl_int XdevLWindowEventServerAndroid::notify(XdevLEvent& event) {
		return 	XdevLModuleAutoImpl::notify(event);;
	}

	void XdevLWindowEventServerAndroid::flush() {
		pollEvents();
	}

//
// -----------------------------------------------------------------------------
//

	XdevLCursorAndroid::XdevLCursorAndroid(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLCursor>(parameter, descriptor),
		m_window(nullptr) {

	}

	xdl_int XdevLCursorAndroid::init() {
		return RET_SUCCESS;
	}

	xdl_int XdevLCursorAndroid::shutdown() {
		return RET_SUCCESS;
	}

	void* XdevLCursorAndroid::getInternal(const XdevLInternalName& id) {
		return nullptr;
	}

	xdl_int XdevLCursorAndroid::attach(XdevLWindow* window) {
		XDEVL_ASSERT(window, "Invalid window parameter");

		m_window = static_cast<XdevLWindowAndroid*>(window);
		return RET_SUCCESS;
	}

	void XdevLCursorAndroid::show() {
	}

	void XdevLCursorAndroid::hide() {
	}

	void XdevLCursorAndroid::setPosition(xdl_uint x, xdl_uint y) {
		XDEVL_ASSERT(m_window, "Cursor not attached to a window.");
	}

	xdl_int XdevLCursorAndroid::clip(xdl_uint x, xdl_uint y, xdl_uint width, xdl_uint height) {
		XDEVL_ASSERT(0, "Not supported by Android");
		return RET_FAILED;
	}

	void XdevLCursorAndroid::releaseClip() {
		XDEVL_ASSERT(0, "Not supported by Android");
	}

	xdl_int XdevLCursorAndroid::enableRelativeMotion() {
		return RET_SUCCESS;
	}

	void XdevLCursorAndroid::disableRelativeMotion() {
	}

	xdl_bool XdevLCursorAndroid::isRelativeMotionEnabled() {
		return xdl_false;
	}

//
//
//

	JNIEXPORT void JNICALL Android_Init(JNIEnv* mEnv, jclass cls) {
		mActivityClass = (jclass)(mEnv)->NewGlobalRef(cls);
		midGetNativeSurface = mEnv->GetStaticMethodID(mActivityClass,"getNativeSurface","()Landroid/view/Surface;");
	}

	JNIEXPORT void JNICALL nativeSetSurface(JNIEnv* jenv, jobject obj, jobject surface) {
		if(surface != 0) {
			nativeWindow = ANativeWindow_fromSurface(jenv, surface);
		} else {
			ANativeWindow_release(nativeWindow);
		}
		return;
	}

	static JavaVM* javaVM;
	static pthread_key_t threadKey;

	// This function will be called when the library get's loaded.
	JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved) {
		JNIEnv* env = nullptr;
		javaVM = vm;

		if(((JavaVM*)javaVM)->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK) {
			return -1;
		}
		/*
		 * Create threadKey so we can keep track of the JNIEnv assigned to each thread
		 * Refer to http://developer.android.com/guide/practices/design/jni.html for the rationale behind this
		 */
		if(pthread_key_create(&threadKey, androidJNIThreadDestroyed) != 0) {
			__android_log_print(ANDROID_LOG_ERROR, "XdevLWindowAndroid", "Error initializing pthread key");
		}
		androidJNISetupThread();

		return JNI_VERSION_1_4;
	}

	JNIEnv* androidJNIGetEnv() {
		/* From http://developer.android.com/guide/practices/jni.html
		 * All threads are Linux threads, scheduled by the kernel.
		 * They're usually started from managed code (using Thread.start), but they can also be created elsewhere and then
		 * attached to the JavaVM. For example, a thread started with pthread_create can be attached with the
		 * JNI AttachCurrentThread or AttachCurrentThreadAsDaemon functions. Until a thread is attached, it has no JNIEnv,
		 * and cannot make JNI calls.
		 * Attaching a natively-created thread causes a java.lang.Thread object to be constructed and added to the "main"
		 * ThreadGroup, making it visible to the debugger. Calling AttachCurrentThread on an already-attached thread
		 * is a no-op.
		 * Note: You can call this function any number of times for the same thread, there's no harm in it
		 */

		JNIEnv* env = nullptr;
		int status = javaVM->AttachCurrentThread(&env, nullptr);
		if(status < 0) {
			XDEVL_MODULEX_ERROR(XdevLWindowAndroid, "Failed to attach current thread.\n");
			return 0;
		}

		/* From http://developer.android.com/guide/practices/jni.html
		 * Threads attached through JNI must call DetachCurrentThread before they exit. If coding this directly is awkward,
		 * in Android 2.0 (Eclair) and higher you can use pthread_key_create to define a destructor function that will be
		 * called before the thread exits, and call DetachCurrentThread from there. (Use that key with pthread_setspecific
		 * to store the JNIEnv in thread-local-storage; that way it'll be passed into your destructor as the argument.)
		 * Note: The destructor is not called unless the stored value is != nullptr
		 * Note: You can call this function any number of times for the same thread, there's no harm in it
		 *       (except for some lost CPU cycles)
		 */
		pthread_setspecific(threadKey, (void*) env);

		return env;
	}

	int androidJNISetupThread() {
		androidJNIGetEnv();
		return 1;
	}

	void androidJNIThreadDestroyed(void* value) {
		/* The thread is being destroyed, detach it from the Java VM and set the threadKey value to nullptr as required */
		JNIEnv *env = (JNIEnv*) value;
		if(env != nullptr) {
			javaVM->DetachCurrentThread();
			pthread_setspecific(threadKey, nullptr);
		}
	}

}
