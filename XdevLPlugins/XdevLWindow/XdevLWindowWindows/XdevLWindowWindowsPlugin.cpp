#include "XdevLWindowWindows.h"

extern xdl::XdevLModuleDescriptor windowModuleDesc;
extern xdl::XdevLModuleDescriptor windowServerModuleDesc;
extern xdl::XdevLModuleDescriptor windowEventServerModuleDesc;
extern xdl::XdevLModuleDescriptor windowCursorModuleDesc;

//
// The XdevLWindow plugin descriptor.
//

static const xdl::XdevLString windowWindowsPluginName {
	"XdevLWindowWindow"
};

xdl::XdevLPluginDescriptor windowWindowsPluginDescriptor {
	windowWindowsPluginName,
	xdl::window_moduleNames,
	XDEVLWINDOWS_PLUGIN_MAJOR_VERSION,
	XDEVLWINDOWS_PLUGIN_MINOR_VERSION,
	XDEVLWINDOWS_PLUGIN_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_CREATE_MODULE{
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowWindows, windowModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowServerWindows, windowServerModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowWindowsEventServer, windowEventServerModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLCursorWindows, windowCursorModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(windowWindowsPluginDescriptor)

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLWindow, xdl::XdevLWindowWindows, windowModuleDesc)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLWindowEventServer, xdl::XdevLWindowWindowsEventServer, windowEventServerModuleDesc)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLCursor, xdl::XdevLCursorWindows, windowCursorModuleDesc)

