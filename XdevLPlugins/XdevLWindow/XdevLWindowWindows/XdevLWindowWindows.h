/*
        XdevL eXtended DEVice Library.

        Copyright © 2005-2017 Cengiz Terzibas

        This library is free software; you can redistribute it and/or modify it under the
        terms of the GNU Lesser General Public License as published by the Free Software
        Foundation; either version 2.1 of the License, or (at your option) any later version.
        This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
        without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License along with
        this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
        Suite 330, Boston, MA 02111-1307 USA

        I would appreciate if you report all bugs to: cengiz@terzibas.de
*/

#ifndef XDEVL_WINDOW_WINDOWS_H
#define XDEVL_WINDOW_WINDOWS_H

#include <XdevLPlatform.h>
#include <XdevLPlugin.h>
#include <XdevLWindow/XdevLWindowImpl.h>
#include <XdevLTypes.h>
#include <XdevLCoreMediator.h>
#include <tinyxml.h>

namespace xdl {

	class XdevLWindowWindows : public XdevLWindowImpl {
		public:
			XdevLWindowWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& desriptor);

			virtual ~XdevLWindowWindows();

			//
			// XdevLModule related methods.
			//

			virtual xdl_int init() override;
			virtual xdl_int shutdown() override;
			virtual void* getInternal(const XdevLInternalName& id) override;
			virtual xdl_int update() override;

			//
			// XdevLWindow related methods.
			//

			virtual xdl_int create() override;
			virtual xdl_int create(const XdevLWindowAttribute& attribute) override;
			virtual const XdevLWindowPosition& getPosition() override;
			virtual const XdevLWindowSize& getSize() override;
			virtual XdevLWindowPosition::type getX() override;
			virtual XdevLWindowPosition::type getY() override;
			virtual XdevLWindowSize::type getWidth() override;
			virtual XdevLWindowSize::type getHeight() override;
			virtual const XdevLWindowTitle& getTitle() override;
			virtual xdl_bool isFullscreen() override;
			virtual xdl_bool isPointerHidden() override;
			virtual void setPosition(const XdevLWindowPosition& position) override;
			virtual void setSize(const XdevLWindowSize& size) override;
			virtual void setX(XdevLWindowPosition::type x) override;
			virtual void setY(XdevLWindowPosition::type y) override;
			virtual void setWidth(XdevLWindowSize::type width) override;
			virtual void setHeight(XdevLWindowSize::type height) override;
			virtual void setTitle(const XdevLWindowTitle& title) override;
			virtual void setFullscreen(xdl_bool state) override;
			virtual void show() override;
			virtual void hide() override;
			virtual xdl_bool isHidden() override;
			virtual void raise() override;
			virtual void setInputFocus() override;
			virtual xdl_bool hasFocus() override;
			virtual void setParent(XdevLWindow* window) override;
			virtual void setType(XdevLWindowTypes type) override;

			//
			// Internal used methods
			//

			HWND getNativeWindow();

		protected:

			XdevLString m_winClassId;

		protected:

			HINSTANCE m_instance = nullptr;
			HWND m_wnd = nullptr;
			HDC m_dc = nullptr;
			unsigned long m_windowStyle = 0;
			unsigned long m_windowStyleEx = 0;
			DEVMODE m_oldDevMode {};
			DEVMODE m_matchingVideoMode {};
			xdl_bool m_isHidden = xdl_false;
			std::vector<DISPLAY_DEVICE> displayInfoList;
	};

	class XdevLWindowServerWindows : public XdevLWindowServerImpl {
		public:
			XdevLWindowServerWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& desriptor);
			virtual ~XdevLWindowServerWindows();

			/// Creates a new window.
			xdl_int create(XdevLWindow** window, const XdevLWindowAttribute& attribute) override;
	};

	class XdevLWindowWindowsEventServer : public XdevLWindowEventServerImpl {
		public:
			XdevLWindowWindowsEventServer(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& desriptor);
			virtual xdl_int init() override;
			virtual xdl_int shutdown() override;
			virtual void* getInternal(const XdevLInternalName& id) override;
			virtual xdl_int update() override;

			virtual xdl_int registerWindowForEvents(XdevLWindow* window) override;
			virtual xdl_int unregisterWindowFromEvents(XdevLWindow* window) override;
			void flush() override;

			/// windows message function callback
			static LRESULT CALLBACK callbackProxy(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

		private:

			LRESULT callbackProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
			HWND m_pointerIsInsideWindow = nullptr;
			xdl_int32 m_currentMouseX = 0;
			xdl_int32 m_currentMouseY = 0;
	};

	class XdevLCursorWindows : public XdevLModuleImpl<XdevLCursor> {
		public:
			XdevLCursorWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& desriptor);

			virtual ~XdevLCursorWindows();

			virtual xdl_int init() override;
			virtual xdl_int shutdown() override;
			virtual void* getInternal(const XdevLInternalName& id) override;

			virtual xdl_int attach(XdevLWindow* window) override;
			virtual void show() override;
			virtual void hide() override;
			virtual void setPosition(xdl_uint x, xdl_uint y) override;
			virtual xdl_int clip(xdl_uint x1, xdl_uint y1, xdl_uint x2, xdl_uint y2) override;
			virtual void releaseClip() override;
			virtual xdl_int enableRelativeMotion() override;
			virtual void disableRelativeMotion() override;
			virtual xdl_bool isRelativeMotionEnabled() override;

		private:
		
			xdl_bool m_reltaiveModeEnabled = xdl_false;
	};


	class XdevLWindowWindowsInit {
		public:
			XdevLWindowWindowsInit(XdevLCoreMediator* core);
			virtual ~XdevLWindowWindowsInit();
			XdevLWindowWindowsEventServer* getWindowsEventServer();
			XdevLCursorWindows* getCursor();

		private:

			XdevLCoreMediator* m_core = nullptr;
			std::shared_ptr<XdevLWindowWindowsEventServer> windowEventServer;
			std::shared_ptr<XdevLCursorWindows> cursor;
	};
}

#endif
