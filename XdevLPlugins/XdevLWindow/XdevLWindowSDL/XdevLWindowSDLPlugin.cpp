/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLWindowSDL.h"

xdl::XdevLPluginDescriptor windowSDLPluginDescriptor {
	xdl::windowPluginName,
	xdl::window_moduleNames,
	XDEVLSDL_MAJOR_VERSION,
	XDEVLSDL_MINOR_VERSION,
	XDEVLSDL_PATCH_VERSION
};

xdl::XdevLModuleDescriptor windowSDLModuleDesc {
	xdl::window_vendor,
	xdl::window_author,
	xdl::window_moduleNames[xdl::XDEVL_WINDOW_MODULE_NAME],
	xdl::window_copyright,
	xdl::windowDescription,
	XDEVLSDL_MODULE_MAJOR_VERSION,
	XDEVLSDL_MODULE_MINOR_VERSION,
	XDEVLSDL_MODULE_PATCH_VERSION
};

xdl::XdevLModuleDescriptor windowServerModuleDesc {
	xdl::window_vendor,
	xdl::window_author,
	xdl::window_moduleNames[xdl::XDEVL_WINDOW_SERVER_MODULE_NAME],
	xdl::window_copyright,
	xdl::windowServerDescription,
	XDEVLSDL_SERVER_MODULE_MAJOR_VERSION,
	XDEVLSDL_SERVER_MODULE_MINOR_VERSION,
	XDEVLSDL_SERVER_MODULE_PATCH_VERSION,
	xdl::XDEVL_MODULE_STATE_DISABLE_AUTO_DESTROY
};

xdl::XdevLModuleDescriptor windowEventServerModuleDesc {
	xdl::window_vendor,
	xdl::window_author,
	xdl::window_moduleNames[xdl::XDEVL_WINDOW_EVENT_SERVER_MODULE_NAME],
	xdl::window_copyright,
	xdl::windowEventServerDescription,
	XDEVLSDL_EVENT_SERVER_MODULE_MAJOR_VERSION,
	XDEVLSDL_EVENT_SERVER_MODULE_MINOR_VERSION,
	XDEVLSDL_EVENT_SERVER_MODULE_PATCH_VERSION,
	xdl::XDEVL_MODULE_STATE_DISABLE_AUTO_DESTROY
};

xdl::XdevLModuleDescriptor cursorModuleDesc {
	xdl::window_vendor,
	xdl::window_author,
	xdl::window_moduleNames[xdl::XDEVL_CURSOR_MODULE_NAME],
	xdl::window_copyright,
	xdl::windowCursorDescription,
	XDEVLSDL_CURSOR_MODULE_MAJOR_VERSION,
	XDEVLSDL_CURSOR_MODULE_MINOR_VERSION,
	XDEVLSDL_CURSOR_MODULE_PATCH_VERSION
};


// TODO This might be changed in future releases
//static xdl::xdl_uint numberOfJoystickDevices = 0;
//
//struct XdevLJoysticks {
//	SDL_Joystick* instance;
//	xdl::xdl_uint numberOfJoystickButtons;
//	xdl::xdl_uint numberOfJoystickAxis;
//};
//
//static std::vector<XdevLJoysticks> joysticks;


XDEVL_PLUGIN_INIT {

//	SDL_Init(SDL_INIT_EVERYTHING);

//	numberOfJoystickDevices = SDL_NumJoysticks();
//	if(numberOfJoystickDevices > 0) {
//		for(xdl::xdl_uint idx = 0; idx < numberOfJoystickDevices; idx++) {
//			SDL_Joystick* js = SDL_JoystickOpen(idx);
//			if(js != nullptr) {
//				XdevLJoysticks jse;
//				jse.instance = js;
//				jse.numberOfJoystickButtons = SDL_JoystickNumButtons(js);
//				jse.numberOfJoystickAxis = SDL_JoystickNumAxes(js);
//				joysticks.push_back(jse);
//			} else {
//				numberOfJoystickDevices--;
//			}
//		}
//		SDL_JoystickEventState(SDL_ENABLE);
//	}

	// If there is not event server first create one.
//	if(xdl::windowEventServer == nullptr) {
//		// If there is no even server active, create and activate it.
//		xdl::windowEventServer = static_cast<xdl::XdevLWindowSDLEventServer*>(XDEVL_PLUGIN_CREATE_PARAMETER_MEDIATOR->createModule(xdl::XdevLModuleName("XdevLWindowEventServer"), xdl::XdevLID("XdevLWindowEventServer"), xdl::XdevLPluginName("XdevLWindowSDL")));
//	}
//
//	if(xdl::cursor == nullptr) {
//		xdl::cursor = static_cast<xdl::XdevLCursor*>(XDEVL_PLUGIN_CREATE_PARAMETER_MEDIATOR->createModule(xdl::XdevLModuleName("XdevLCursor"), xdl::XdevLID("XdevLCursor")));
//	}

	return xdl::RET_SUCCESS;
}

XDEVL_PLUGIN_SHUTDOWN {

	// If the last window was destroy make sure to destroy the event server too.
//	if(xdl::windowEventServer != nullptr) {
//		xdl::XdevLWindowEventServerParameter->getMediator()->deleteModule(xdl::windowEventServer->getID());
//		xdl::windowEventServer = nullptr;
//	}
//
//	if(joysticks.size() > 0) {
//		for(auto& js : joysticks) {
//			SDL_JoystickClose(js.instance);
//		}
//	}

	SDL_Quit();

	return xdl::RET_SUCCESS;
}


//XDEVL_PLUGIN_INIT_DEFAULT
//XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_CREATE_MODULE  {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowSDL, windowSDLModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowServerSDL, windowServerModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLWindowSDLEventServer, windowEventServerModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLCursorSDL, cursorModuleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(windowSDLPluginDescriptor)