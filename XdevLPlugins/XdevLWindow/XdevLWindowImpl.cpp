/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLWindowImpl.h"
#include <XdevLXstring.h>
#include <XdevLCoreMediator.h>
#include <XdevLError.h>
#include <sstream>

namespace xdl {

	xdl_int initDefaultWindowInstances(xdl::XdevLCoreMediator* mediator) {

		if(xdl::windowEventServer == nullptr) {
			xdl::windowEventServer = static_cast<xdl::XdevLWindowEventServer*>(mediator->createModule(xdl::XdevLModuleName("XdevLWindowEventServer"), xdl::XdevLID("XdevLWindowEventServer")));
			if(nullptr == xdl::windowEventServer) {
				return RET_FAILED;
			}
		}

		if(xdl::cursor == nullptr) {
			xdl::cursor = static_cast<xdl::XdevLCursor*>(mediator->createModule(xdl::XdevLModuleName("XdevLCursor"), xdl::XdevLID("XdevLCursor")));
			if(nullptr == xdl::cursor) {
				return RET_FAILED;
			}
		}

		return RET_SUCCESS;
	}

	xdl::XdevLWindowEventServer* windowEventServer = nullptr;
	xdl::XdevLCursor* cursor = nullptr;
	xdl::XdevLModuleCreateParameter* XdevLWindowEventServerParameter;

	static xdl_bool eventThreadIsRunning = xdl_false;

	thread::Mutex m_mutexThreadIsRunning;

	xdl_bool isEventThreadRunning() {
		thread::XdevLScopeLock lock(m_mutexThreadIsRunning);
		return eventThreadIsRunning;
	}

	void startEventThread() {
		thread::XdevLScopeLock lock(m_mutexThreadIsRunning);
		eventThreadIsRunning = xdl_true;
	}

	void stopEventThread() {
		thread::XdevLScopeLock lock(m_mutexThreadIsRunning);
		eventThreadIsRunning = xdl_false;
	}


	xdl_uint64 XdevLWindowImpl::m_currentID = 0;
	std::queue<xdl_uint64> XdevLWindowImpl::m_freeIDs;

	XdevLWindowImpl::XdevLWindowImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& desriptor)
		 : XdevLModuleAutoImpl<XdevLWindow>(parameter, desriptor)
		, m_rootWindow(NULL)
		, m_id(newID())
		, m_rootTitle("")
		, m_isfullScreenActivated(xdl_false)
		, m_colorDepth(32)
		, m_isPointerHidden(xdl_false)
		, m_isFullscreen(xdl_false) {

		// Set background color.
		m_backgroundColor[0] = 0;
		m_backgroundColor[1] = 0;
		m_backgroundColor[2] = 0;
		m_backgroundColor[3] = 255;

	}

	XdevLWindowImpl::~XdevLWindowImpl() {
		freeID(m_id);
	}

	xdl_uint64 XdevLWindowImpl::newID() {
		if(m_freeIDs.size() > 0) {
			xdl_uint64 tmp = m_freeIDs.front();
			m_freeIDs.pop();
			return tmp;
		}
		return m_currentID++;
	}

	void XdevLWindowImpl::freeID(xdl_uint64 id) {
		// TODO I might have to check if the id is already freed.
		m_freeIDs.push(id);
	}


	xdl_int XdevLWindowImpl::create(const XdevLWindowAttribute& attribute) {
		m_attribute = attribute;
		return RET_SUCCESS;
	}

	xdl_int XdevLWindowImpl::notify(XdevLEvent& event) {

		switch(event.type) {
			case XDEVL_WINDOW_EVENT: {
				switch(event.window.event) {
					case XDEVL_WINDOW_MOVED: {
						m_attribute.position.x = event.window.x;
						m_attribute.position.y = event.window.y;
						m_attribute.size.width = event.window.width;
						m_attribute.size.height = event.window.height;
					}
					break;
					case XDEVL_WINDOW_RESIZED:  {
						m_attribute.position.x = event.window.x;
						m_attribute.position.y = event.window.y;
						m_attribute.size.width = event.window.width;
						m_attribute.size.height = event.window.height;
					}
					break;
				}
			}
			break;
		}

		return 	XdevLModuleAutoImpl::notify(event);;
	}

	xdl_uint64 XdevLWindowImpl::getWindowID() {
		return m_id;
	}

	XdevLWindow* XdevLWindowImpl::getParent() {
		return m_rootWindow;
	}

	XdevLWindowSize::type XdevLWindowImpl::getWidth() {
		return m_attribute.size.width;
	}

	XdevLWindowSize::type XdevLWindowImpl::getHeight() {
		return m_attribute.size.height;
	}

	XdevLWindowPosition::type XdevLWindowImpl::getX() {
		return m_attribute.position.x;
	}

	XdevLWindowPosition::type XdevLWindowImpl::getY() {
		return m_attribute.position.y;
	}

	const XdevLWindowTitle& XdevLWindowImpl::getTitle() {
		return m_attribute.title;
	}

	const XdevLWindowPosition& XdevLWindowImpl::getPosition() {
		return m_attribute.position;
	}

	const XdevLWindowSize& XdevLWindowImpl::getSize() {
		return m_attribute.size;
	}

	xdl_bool XdevLWindowImpl::isFullscreenActivated() {
		return m_isfullScreenActivated;
	}

	xdl_int XdevLWindowImpl::getColorDepth() const {
		return m_colorDepth;
	}

	void XdevLWindowImpl::setX(XdevLWindowPosition::type x) {
		m_attribute.position.x = x;
	}

	void XdevLWindowImpl::setY(XdevLWindowPosition::type y) {
		m_attribute.position.y = y;
	}

	void XdevLWindowImpl::setWidth(XdevLWindowSize::type width) {
		m_attribute.size.width = width;
	}

	void XdevLWindowImpl::setHeight(XdevLWindowSize::type height) {
		m_attribute.size.height = height;
	}

	void XdevLWindowImpl::setColorDepth(int depth) {
		m_colorDepth = depth;
	}

	void XdevLWindowImpl::setTitle(const XdevLWindowTitle& title) {
		m_attribute.title = title;
	}

	xdl_bool XdevLWindowImpl::isPointerHidden() {
		return m_isPointerHidden;
	}

	void XdevLWindowImpl::setHidePointer(xdl_bool state) {
		m_isPointerHidden = state;
	}

	void XdevLWindowImpl::setParent(XdevLWindow* window) {
		m_rootWindow = window;
	}

	void XdevLWindowImpl::setWindowDecoration(xdl_bool enable) {

	}

	void XdevLWindowImpl::setType(XdevLWindowTypes type) {
		m_attribute.type = type;
	}

	XdevLWindowTypes XdevLWindowImpl::getType() {
		return  m_attribute.type;
	}

	int XdevLWindowImpl::readWindowInfo(TiXmlDocument& document) {
		TiXmlHandle docHandle(&document);
		TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toString().c_str()).FirstChildElement("XdevLWindow").ToElement();
		if(!root) {
			XDEVL_MODULEX_INFO(XdevLWindowImpl, "<XdevLWindow> section not found. Using default values for the device.\n");
			return RET_SUCCESS;
		}

		while(root != NULL) {

			// Does the user specified the id of the module?
			if(root->Attribute("id")) {
				XdevLID id(root->Attribute("id"));

				// Do only if we have the same ID.
				// TODO Maybe change comparison into string comparision.
				if(getID() == id) {
					TiXmlElement* child = nullptr;
					for(child = root->FirstChildElement(); child; child = child->NextSiblingElement()) {
						if(child->ValueTStr() == "Root")
							m_rootTitle = XdevLString(child->GetText());
						if(child->ValueTStr() == "Title")
							m_attribute.title = XdevLString(child->GetText());
						if(child->ValueTStr() == "Fullscreen")
							m_isfullScreenActivated = xstd::from_string<bool>(child->GetText());
						if(child->ValueTStr() == "X")
							m_attribute.position.x = xstd::from_string<int>(child->GetText());
						if(child->ValueTStr() == "Y")
							m_attribute.position.y = xstd::from_string<int>(child->GetText());
						if(child->ValueTStr() == "Width")
							m_attribute.size.width = xstd::from_string<int>(child->GetText());
						if(child->ValueTStr() == "Height")
							m_attribute.size.height = xstd::from_string<int>(child->GetText());
						if(child->ValueTStr() == "BackgroundColor") {
							std::vector<XdevLString> list;
							XdevLString value(child->GetText());
							value.tokenize(list, XdevLString(TEXT(", ")));
							std::stringstream r(list[0].toString());
							std::stringstream g(list[1].toString());
							std::stringstream b(list[2].toString());
							std::stringstream a(list[3].toString());
							r >> m_backgroundColor[0];
							g >> m_backgroundColor[1];
							b >> m_backgroundColor[2];
							a >> m_backgroundColor[3];
						}
					}
				}
				root = root->NextSiblingElement();
			}
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLWindowImpl::init() {

		// This might be the case when the window was created using the XdevLWindowServer.
		if(getMediator() == nullptr) {
			return RET_SUCCESS;
		}

		auto coreParameter = getMediator()->getCoreParameter();
		if(coreParameter.xmlBuffer.size() != 0) {
			TiXmlDocument xmlDocument;
			if(!xmlDocument.Parse((xdl_char*)coreParameter.xmlBuffer.data())) {
				XDEVL_MODULE_ERROR("Could not parse xml buffer.\n ");
				return RET_FAILED;
			}

			if(readWindowInfo(xmlDocument) != RET_SUCCESS) {
				XDEVL_MODULEX_WARNING(XdevLWindowImpl, "Some issues happend when parsing the XML file.\n");
			}
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLWindowImpl::shutdown() {

		if(nullptr == windowEventServer) {
			return RET_SUCCESS;
		}

		// TODO We flush here to receive pending events before we unregister from the events server. Maybe harmful?
		windowEventServer->flush();
		windowEventServer->unregisterWindowFromEvents(this);

		return RET_SUCCESS;
	}

	xdl_int XdevLWindowImpl::stringToWindowType(const XdevLString& tmp) {
		if(tmp == XdevLString(TEXT("XDEVL_WINDOW_TYPE_NORMAL"))) {
			return XDEVL_WINDOW_TYPE_NORMAL;
		}

		return XDEVL_WINDOW_TYPE_UNKNOWN;
	}

	xdl_bool XdevLWindowImpl::isPointerInside() {
		return m_pointerIsInside;
	}

	void XdevLWindowImpl::setPosition(const XdevLWindowPosition& position) {
		m_attribute.position = position;
	}

	void XdevLWindowImpl::setSize(const XdevLWindowSize& size) {
		m_attribute.size = size;
	}
	
	xdl_bool XdevLWindowImpl::isFullscreen() {
		return m_isFullscreen;
	}
	
	void XdevLWindowImpl::setIsFullscreen(xdl_bool enabled) {
		m_isFullscreen = enabled;
	}

	//
	// -------------------------------------------------------------------------
	//

	XdevLWindowServerImpl::XdevLWindowServerImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& desriptor) :
		XdevLModuleAutoImpl<XdevLWindowServer>(parameter, desriptor)  {

	}

	XdevLWindowServerImpl::~XdevLWindowServerImpl() {

	}

	xdl_int XdevLWindowServerImpl::destroy(XdevLWindow* window) {
		assert((window) && "XdevLWindowServerImpl::destroy: window == nullptr");

		// Find in the map and remove it and free all resources.
		auto tmp = m_windowList.find(window->getWindowID());

		if(tmp != m_windowList.end()) {
			m_windowList.erase(tmp);

			// We do not destroy using event system. Directly delete which is handles in the descructor.
			delete tmp->second;

			return RET_SUCCESS;
		}
		return RET_FAILED;
	}



	//
	// -------------------------------------------------------------------------
	//

	XdevLWindowEventServerImpl::XdevLWindowEventServerImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleAutoImpl<XdevLWindowEventServer>(parameter, descriptor) {

	}

	xdl_bool XdevLWindowEventServerImpl::isWindowRegistered(XdevLWindow* window) {
		XdevLWindowEventMapType::iterator it = m_windows.find(window->getWindowID());
		if(it == m_windows.end()) {
			return xdl_false;
		}
		return xdl_true;
	}

	xdl_int XdevLWindowEventServerImpl::registerWindowForEvents(XdevLWindow* window) {
		XdevLWindowEventMapType::iterator it = m_windows.find(window->getWindowID());
		if(it == m_windows.end()) {
			m_windows[window->getWindowID()] = window;
			return RET_SUCCESS;
		}
		return RET_FAILED;
	}

	xdl_int XdevLWindowEventServerImpl::unregisterWindowFromEvents(XdevLWindow* window) {
		XdevLWindowEventMapType::iterator it = m_windows.find(window->getWindowID());
		if(it != m_windows.end()) {
			m_windows.erase(it);
			return RET_SUCCESS;
		}
		return RET_FAILED;
	}

	XdevLWindow* XdevLWindowEventServerImpl::getWindow(xdl_uint64 id) {
		auto window = m_windows.find(id);
		if(window == m_windows.end()) {
			return nullptr;
		}
		return window->second;
	}

	XdevLWindow* XdevLWindowEventServerImpl::getFocus() const {
		return m_focusWindow;
	}

	void XdevLWindowEventServerImpl::focusGained(XdevLWindow* window) {
		m_focusWindow = window;
	}

	void XdevLWindowEventServerImpl::flush() {

	}


}
