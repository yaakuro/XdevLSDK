#ifndef XDEVL_OPENGL_CONTEXT_CGL_H
#define XDEVL_OPENGL_CONTEXT_CGL_H

#include <XdevLPluginImpl.h>
#include <XdevLOpenGLContext/XdevLOpenGLContextBase.h>

#import <Cocoa/Cocoa.h>
#include "mach-o/dyld.h"
#include <OpenGL/CGLTypes.h>
#include <OpenGL/OpenGL.h>
#include <OpenGL/CGLRenderers.h>
//#include <OpenGL/gl3.h>


@interface NXdevLOpenGLContext : NSOpenGLContext {
	xdl::XdevLWindow* window;
}

- (id)initWithFormat: (NSOpenGLPixelFormat *)format shareContext:(NSOpenGLContext *)share;
- (void)scheduleUpdate;
- (void)updateIfNeeded;
- (void)setWindow:
(xdl::XdevLWindow *)window;

@end

namespace xdl {


	static const std::vector<XdevLModuleName> moduleNames {
		XdevLModuleName("XdevLOpenGLContext")
	};

	/**
		@class XdevLOpenGLContextCocoa
		@brief
		@author Cengiz Terzibas
	*/
	class XdevLOpenGLContextCocoa : public XdevLOpenGLContextBase {

		public:
			XdevLOpenGLContextCocoa(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLOpenGLContextCocoa();

			xdl_int init() override final;
			xdl_int shutdown() override final;

			xdl_vptr getProcAddress(const xdl_char* func) override final;
			xdl_int getAttributes(XdevLOpenGLContextAttributes& attributes) override final;
			xdl_int setAttributes(const XdevLOpenGLContextAttributes& attributes) override final;
			xdl_int create(XdevLWindow* window, XdevLOpenGLContext* shareContext = nullptr) override final;
			xdl_int create(XdevLWindow* window, const XdevLOpenGLContextAttributes& attributes, XdevLOpenGLContext* shareContext = nullptr) override final;
			xdl_int makeCurrent(XdevLWindow* window) override final;
			xdl_int swapBuffers() override final;
			xdl_int setVSync(xdl_bool enableVSync)  override final;

		private:

			CGLContextObj m_context;
			NSOpenGLContext* m_openGLContext;
			XdevLOpenGLContextAttributes m_attributes;
	};

}


#endif
