#ifndef XDEVL_OPENGLCONTEXTEGL_IMPL_H
#define XDEVL_OPENGLCONTEXTEGL_IMPL_H

#include <XdevLPluginImpl.h>
#include <XdevLOpenGLContext/XdevLOpenGLContext.h>
#include <XdevLOpenGLContext/XdevLOpenGLContextBase.h>

#include <XdevLPluginImpl.h>

#if XDEVL_PLATFORM_UNIX
//#include <GLES2/gl2.h>
#endif

#include <EGL/egl.h>
#include <EGL/eglext.h>

namespace xdl {

	static const XdevLString vendor {
		"www.codeposer.net"
	};

	static const XdevLString author {
		"Cengiz Terzibas"
	};

	static const XdevLString copyright {
		"(c) 2005 - 2016 Cengiz Terzibas."
	};

	static const XdevLString pluginName {
		"XdevLOpenGLContextEGL"
	};

	static const XdevLString moduleDescription {
		"Module to create a EGL OpenGL context."
	};

	static std::vector<XdevLModuleName>	moduleNames {
		XdevLModuleName("XdevLOpenGLContext"),
	};

	class XdevLOpenGLContextEGL : public XdevLOpenGLContextBase {
		public:

			XdevLOpenGLContextEGL(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

			virtual ~XdevLOpenGLContextEGL();
			
			xdl_int create(XdevLWindow* window, XdevLOpenGLContext* shareContext = nullptr) override final;
			xdl_int create(XdevLWindow* window, const XdevLOpenGLContextAttributes& attributes, XdevLOpenGLContext* shareContext = nullptr) override final;
			xdl_int getAttributes(XdevLOpenGLContextAttributes& attributes) override final;
			xdl_int setAttributes(const XdevLOpenGLContextAttributes& attributes) override final;
			xdl_int makeCurrent(XdevLWindow* window) override final;
			xdl_int swapBuffers() override final;
			xdl_vptr getProcAddress(const xdl_char* func) override final;
			xdl_int setVSync(xdl_bool enableVSync) override final;
			void* getInternal(const XdevLInternalName& id) override final;

		private:
			EGLDisplay m_eglDisplay;
			EGLSurface m_eglSurface;
			EGLContext m_eglContext;
			EGLConfig m_eglConfig;
			EGLint m_major;
			EGLint m_minor;
			EGLint m_width;
			EGLint m_height;
	};


}

#endif
