/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLCoreMediator.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLPlatform.h>
#include <XdevLXstring.h>
#include <XdevLUtils.h>
#include "XdevLOpenGLContextGLX.h"
#include <tinyxml.h>

#include <sstream>
#include <cstddef>
#include <iostream>
#include <array>


#define GLX_CONTEXT_FLAG_NO_ERROR_BIT_KHR 0x00000008

namespace xdl {

	inline const xdl_char* errorCodeToString(xdl_int errorCode) {
		switch(errorCode) {
			case GLXBadContext: return "GLXBadContext";
			case GLXBadContextState: return "GLXBadContextState";
			case GLXBadDrawable: return "GLXBadDrawable";
			case GLXBadPixmap: return "GLXBadPixmap";
			case GLXBadContextTag: return "GLXBadContextTag";
			case GLXBadCurrentWindow: return "GLXBadCurrentWindow";
			case GLXBadRenderRequest: return "GLXBadRenderRequest";
			case GLXBadLargeRequest: return "GLXBadLargeRequest";
			case GLXUnsupportedPrivateRequest: return "GLXUnsupportedPrivateRequest";
			case GLXBadFBConfig: return "GLXBadFBConfig";
			case GLXBadPbuffer: return "GLXBadPbuffer";
			case GLXBadCurrentDrawable: return "GLXBadCurrentDrawable";
			case GLXBadWindow: return "GLXBadWindow";
			case GLXBadProfileARB: return "GLXBadProfileARB";
		}
		return "Unknown";
	}

	inline XdevLString drawableTypeToString(xdl_int type) {
		XdevLString tmp;
		if(type & GLX_WINDOW_BIT) {
			tmp += XdevLString(TEXT("GLX_WINDOW_BIT"));
		}
		if(type & GLX_PIXMAP_BIT) {
			if(tmp.size() > 0) {
				tmp += XdevLString(TEXT(" | "));
			}
			tmp += XdevLString(TEXT("GLX_PIXMAP_BIT"));
		}
		if(type & GLX_PBUFFER_BIT) {
			if(tmp.size() > 0) {
				tmp += XdevLString(TEXT(" | "));
			}
			tmp += XdevLString(TEXT("GLX_PBUFFER_BIT"));
		}
		return tmp;
	}

	inline XdevLString configCaveatTypeToString(xdl_int type) {
		XdevLString tmp("Unknown");
		switch(type) {
			case GLX_NONE: {
				tmp = XdevLString(TEXT("GLX_NONE"));
			} break;
			case GLX_SLOW_CONFIG: {
				tmp = XdevLString(TEXT("GLX_SLOW_CONFIG"));
			} break;
			case GLX_NON_CONFORMANT_CONFIG: {
				tmp = XdevLString(TEXT("GLX_NON_CONFORMANT_CONFIG"));
			}
			break;
			default:
				break;
		}
		return tmp;
	}

	inline XdevLString transparencyTypeToString(xdl_int type) {
		XdevLString tmp("Unknown");
		switch(type) {
			case GLX_NONE: {
				tmp = XdevLString(TEXT("GLX_NONE"));
			} break;
			case GLX_TRANSPARENT_RGB: {
				tmp = XdevLString(TEXT("GLX_TRANSPARENT_RGB"));
			} break;
			case GLX_TRANSPARENT_INDEX: {
				tmp = XdevLString(TEXT("GLX_TRANSPARENT_INDEX"));
			} break;
			default:
				break;
		}
		return tmp;
	}

	inline XdevLString booleanToString(xdl_bool type) {
		XdevLString tmp = type ? XdevLString(TEXT("True")) : XdevLString(TEXT("False"));
		return tmp;
	}

	static xdl_bool contextErrorOccured = xdl_false;

	static int GLXErrorHandler(Display* display, XErrorEvent* ev) {
		contextErrorOccured = xdl_true;

		std::array<char, 255> errorstring;
		if(XGetErrorText(display, ev->error_code, errorstring.data(), errorstring.size()) == Success) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLContextGLX, "Error Code   : " << errorstring.data() << std::endl);
			XDEVL_MODULEX_ERROR(XdevLOpenGLContextGLX, "Request Code : " << errorCodeToString(ev->request_code) << std::endl);
			XDEVL_MODULEX_ERROR(XdevLOpenGLContextGLX, "Minor Code   : " << (xdl_int)ev->minor_code << std::endl);
		}
		return 0;
	}

	XdevLOpenGLContextGLX::XdevLOpenGLContextGLX(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLOpenGLContextBase(parameter, descriptor)
		, m_display(nullptr)
		, m_window(None)
		, m_shareContext(nullptr)
		, m_screenNumber(0)
		, m_glxContext(nullptr)
		, m_glxMajorVersion(0)
		, m_glxMinorVersion(0)
		, m_visualInfo(nullptr)
		, glXCreateContextAttribs(nullptr)
		, glXSwapIntervalEXT(nullptr)
		, glXSwapIntervalSGI(nullptr)
		, m_oldErrorHandler(0) {
	}

	XdevLOpenGLContextGLX::~XdevLOpenGLContextGLX() {
		if(nullptr != m_glxContext) {
			shutdown();
		}
	}

	GLXContext XdevLOpenGLContextGLX::getNativeContext() const {
		return m_glxContext;
	}


	xdl_int XdevLOpenGLContextGLX::init() {

		contextErrorOccured = false;
		m_oldErrorHandler= XSetErrorHandler(&GLXErrorHandler);

		glXCreateContextAttribs = (PFNGLXCREATECONTEXTATTRIBSARBPROC)glXGetProcAddress((GLubyte *)"glXCreateContextAttribsARB");
		glXSwapIntervalEXT = (PFNGLXSWAPINTERVALEXTPROC)glXGetProcAddress((GLubyte *)"glXSwapIntervalEXT");
		glXSwapIntervalSGI = (PFNGLXSWAPINTERVALSGIPROC)glXGetProcAddress((GLubyte *)"glXSwapIntervalSGI");

		return XdevLOpenGLContextBase::init();
	}

	xdl_int XdevLOpenGLContextGLX::shutdown() {

		//
		// Free resources
		//
		glXDestroyContext(m_display, m_glxContext);
		XFree(m_visualInfo);

		//
		// Set back the error handler.
		//
		XSetErrorHandler(m_oldErrorHandler);

		//
		// Reset all used variables.
		//
		m_display = nullptr;
		m_window = None;
		m_screenNumber = 0;
		m_glxMajorVersion = 0;
		m_glxMinorVersion = 0;
		m_glxContext = nullptr;
		m_visualInfo = nullptr;
		glXCreateContextAttribs = nullptr;
		glXSwapIntervalEXT = nullptr;
		glXSwapIntervalSGI = nullptr;

		m_oldErrorHandler = 0; 

		return RET_SUCCESS;
	}

	xdl_vptr XdevLOpenGLContextGLX::getProcAddress(const xdl_char* func) {
		return reinterpret_cast<xdl_vptr>(glXGetProcAddress((const GLubyte *)func));
	}

	xdl_int XdevLOpenGLContextGLX::getAttributes(XdevLOpenGLContextAttributes& attributes) {
		attributes = m_attributes;
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLContextGLX::setAttributes(const XdevLOpenGLContextAttributes& attributes) {
		m_attributes = attributes;
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLContextGLX::create(XdevLWindow* window, const XdevLOpenGLContextAttributes& attributes, XdevLOpenGLContext* shareContext) {
		m_attributes = attributes;
		return create(window, shareContext);
	}

	int XdevLOpenGLContextGLX::create(XdevLWindow* window, XdevLOpenGLContext* shareContext) {
		XDEVL_ASSERT(m_display == nullptr, "XdevLOpenGLContextGLX already created.");

		if(nullptr != shareContext) {
			m_shareContext = static_cast<XdevLOpenGLContextGLX*>(shareContext);
		}

		window->getDescriptor().registerDependency(this);

		m_display = static_cast<Display*>(window->getInternal(XdevLInternalName("X11_DISPLAY")));
		if(nullptr == m_display) {
			XDEVL_MODULE_ERROR("Could not get native X11 display information.\n");
			return RET_FAILED;
		}

		m_window = (Window)(window->getInternal(XdevLInternalName("X11_WINDOW")));
		if(None == m_window) {
			XDEVL_MODULE_ERROR("Could not get native X11 window information.\n");
			return RET_FAILED;
		}

		if(glXQueryVersion(m_display, &m_glxMajorVersion, &m_glxMinorVersion) == False) {
			XDEVL_MODULE_ERROR("glXQueryVersion failed.\n");
			return RET_FAILED;
		}

		if(initOpenGL(m_display, m_window) == RET_FAILED) {
			XDEVL_MODULE_ERROR("Failed to initialize OpenGL.\n");
			return RET_FAILED;
		}

		if(glXIsDirect(m_display, m_glxContext)) {
			XDEVL_MODULE_INFO("Direct Rendering supported.\n");

		} else {
			XDEVL_MODULE_WARNING("Direct Rendering not supported.\n");
		}

		setVSync(getVSync());

		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLContextGLX::initOpenGL(Display* display, Window window) {

		//
		// Get all supported extensions.
		//
		std::string tmp(glXQueryExtensionsString(display, DefaultScreen(display)));
		std::vector<std::string> exlist;
		xstd::tokenize(tmp, exlist, " ");
		for(auto extension : exlist) {
			extensionsList.push_back(XdevLString(extension.c_str()));
		}


		std::vector<int> attribute_list;
		// ---------------------------------------------------------------------------
		// Prepare the attribute values for the glXChooseVisual function
		// We don't handle GLX_BUFFER_SIZE because we don't use GLX_COLOR_INDEX_BIT in GLX_RENDER_TYPE.

		// Indicates whether X can be used to render into a drawable created with the GLXFBConfig.
		attribute_list.push_back(GLX_X_RENDERABLE);
		attribute_list.push_back(True);

		// We are going to use the normal RGBA color type, not the color index type.
		attribute_list.push_back(GLX_RENDER_TYPE);
		attribute_list.push_back(GLX_RGBA_BIT);

		// This window is going to use only Window type, that means we can't use PBuffers.
		// Valid values are GLX WINDOW BIT, GLX PIXMAP BIT, GLX PBUFFER BIT. All can be used at the same time.
		attribute_list.push_back(GLX_DRAWABLE_TYPE);
		attribute_list.push_back(GLX_WINDOW_BIT);

		attribute_list.push_back(GLX_X_VISUAL_TYPE);
		attribute_list.push_back(GLX_TRUE_COLOR);

		if(m_attributes.red_size > 0) {
			attribute_list.push_back(GLX_RED_SIZE);
			attribute_list.push_back(m_attributes.red_size);
		}

		if(m_attributes.green_size > 0) {
			attribute_list.push_back(GLX_GREEN_SIZE);
			attribute_list.push_back(m_attributes.green_size);
		}

		if(m_attributes.blue_size > 0) {
			attribute_list.push_back(GLX_BLUE_SIZE);
			attribute_list.push_back(m_attributes.blue_size);
		}

		if(m_attributes.alpha_size > 0) {
			attribute_list.push_back(GLX_ALPHA_SIZE);
			attribute_list.push_back(m_attributes.alpha_size);
		}

		if(m_attributes.double_buffer > 0) {
			attribute_list.push_back(GLX_DOUBLEBUFFER);
			attribute_list.push_back(True);
		}

		attribute_list.push_back(GLX_DEPTH_SIZE);
		attribute_list.push_back(m_attributes.depth_size);

		if(m_attributes.stencil_size > 0) {
			attribute_list.push_back(GLX_STENCIL_SIZE);
			attribute_list.push_back(m_attributes.stencil_size);
		}

		if(m_attributes.accum_red_size > 0) {
			attribute_list.push_back(GLX_ACCUM_RED_SIZE);
			attribute_list.push_back(m_attributes.accum_red_size);
		}

		if(m_attributes.accum_green_size > 0) {
			attribute_list.push_back(GLX_ACCUM_GREEN_SIZE);
			attribute_list.push_back(m_attributes.accum_green_size);
		}

		if(m_attributes.accum_blue_size > 0) {
			attribute_list.push_back(GLX_ACCUM_BLUE_SIZE);
			attribute_list.push_back(m_attributes.accum_blue_size);
		}

		if(m_attributes.accum_alpha_size > 0) {
			attribute_list.push_back(GLX_ACCUM_ALPHA_SIZE);
			attribute_list.push_back(m_attributes.accum_alpha_size);
		}

		if(xdl_true == m_attributes.stereo) {
			attribute_list.push_back(GLX_STEREO);
			attribute_list.push_back(True);
		}

		// ARB_multisample
		if(m_attributes.multisample_buffers > 0) {
			// Enable Multisampling.
			attribute_list.push_back(GLX_SAMPLE_BUFFERS);
			attribute_list.push_back(GL_TRUE);
			// Set Multisampling value.
			attribute_list.push_back(GLX_SAMPLES);
			attribute_list.push_back(m_attributes.multisample_samples);
		}

		// ARB_framebuffer_sRGB
		if(xdl_true == m_attributes.srgb) {
			attribute_list.push_back(GLX_FRAMEBUFFER_SRGB_CAPABLE_ARB);
			attribute_list.push_back(True);
		}

		attribute_list.push_back(None);

		//
		// Get all supported visual configurations.
		//
		xdl_int numberOfConfigs {};
		GLXFBConfig *fbcfg = glXChooseFBConfig(display, DefaultScreen(display), static_cast<const int*>(attribute_list.data()), &numberOfConfigs);
		if(!fbcfg) {
			XDEVL_MODULE_ERROR("glXChooseFBConfig failed\n");
			return RET_FAILED;
		}

		/// Now get the GLXFBConfig for the best match.
		GLXFBConfig bestFbc = fbcfg[0];
		XFree(fbcfg);

		m_visualInfo = glXGetVisualFromFBConfig(display, bestFbc);

		// Change the windows color map.
		XSetWindowAttributes swa {};
		swa.colormap = XCreateColormap(display, window, m_visualInfo->visual, AllocNone);
		// TODO This part is causing BadMatch.
		XChangeWindowAttributes(display, window, CWColormap, &swa);
		XSync(display, False);

		//
		// Set OpenGL 3.0 > attributes.
		//
		std::vector<int> opengl_profile_attribute_list;
		opengl_profile_attribute_list.push_back(GLX_CONTEXT_MAJOR_VERSION_ARB);
		opengl_profile_attribute_list.push_back(m_attributes.context_major_version);
		opengl_profile_attribute_list.push_back(GLX_CONTEXT_MINOR_VERSION_ARB);
		opengl_profile_attribute_list.push_back(m_attributes.context_minor_version);

		auto contextFlags = 0;

		if(m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_DEBUG_BIT) {
			contextFlags |= GLX_CONTEXT_DEBUG_BIT_ARB;
		}

		if(m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_FORWARD_COMPATIBLE_BIT) {
			contextFlags |= GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB;
		}

		// KHR_no_error
		if(m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_NO_ERROR_BIT) {
			contextFlags |= GLX_CONTEXT_FLAG_NO_ERROR_BIT_KHR;
		}

		// ARB_create_context_robustness
		if(m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_ROBUST_ACCESS_BIT) {
			contextFlags |= GLX_CONTEXT_ROBUST_ACCESS_BIT_ARB;

			attribute_list.push_back(GLX_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB);
			if(XDEVL_OPENGL_CONTEXT_NO_RESET_NOTIFICATION == m_attributes.robustness) {
				attribute_list.push_back(GLX_NO_RESET_NOTIFICATION_ARB);
			} else if(XDEVL_OPENGL_CONTEXT_LOSE_CONTEXT_ON_RESET == m_attributes.robustness) {
				attribute_list.push_back(GLX_LOSE_CONTEXT_ON_RESET_ARB);
			}
		}

		if(contextFlags) {
			opengl_profile_attribute_list.push_back(GLX_CONTEXT_FLAGS_ARB);
			opengl_profile_attribute_list.push_back(contextFlags);
		}

		// KHR_context_flush_control
		if(m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_FLUSH) {
			attribute_list.push_back(GLX_CONTEXT_RELEASE_BEHAVIOR_ARB);
			if(XDEVL_OPENGL_CONTEXT_RELEASE_BEHAVIOR_NONE == m_attributes.flush_control) {
				attribute_list.push_back(GLX_CONTEXT_RELEASE_BEHAVIOR_NONE_ARB);
			} else if(XDEVL_OPENGL_CONTEXT_RELEASE_BEHAVIOR_FLUSH == m_attributes.flush_control) {
				attribute_list.push_back(GLX_CONTEXT_RELEASE_BEHAVIOR_FLUSH_ARB);
			}
		}

		// ARB_create_context
		if((m_attributes.context_profile_mask == XDEVL_OPENGL_CONTEXT_CORE_PROFILE) || (m_attributes.context_profile_mask == XDEVL_OPENGL_CONTEXT_COMPATIBILITY)) {
			opengl_profile_attribute_list.push_back(GLX_CONTEXT_PROFILE_MASK_ARB);
			if(m_attributes.context_profile_mask == XDEVL_OPENGL_CONTEXT_CORE_PROFILE) {
				opengl_profile_attribute_list.push_back(GLX_CONTEXT_CORE_PROFILE_BIT_ARB);
			} else if(m_attributes.context_profile_mask == XDEVL_OPENGL_CONTEXT_COMPATIBILITY) {
				opengl_profile_attribute_list.push_back(GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB);
			}
		}


		opengl_profile_attribute_list.push_back(None);

		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "OpenGL Context Version: " << m_attributes.context_major_version << "." << m_attributes.context_minor_version << std::endl);
		dumpConfigInfo(bestFbc);

		//
		// Try to use new ARB_create_context and ARB_create_context_profile
		//
		auto sharedContext = m_shareContext ? m_shareContext->getNativeContext() : nullptr;
		if(nullptr != glXCreateContextAttribs) {
			//
			// Create the context.
			//
			m_glxContext = glXCreateContextAttribs(display, bestFbc, sharedContext, True, opengl_profile_attribute_list.data());
			if(nullptr == m_glxContext) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLContextGLX, "glXCreateContext failed.\n");
				return RET_FAILED;
			}

			//
			// Check for other erros.
			//
			glXWaitGL();
			if(xdl_true == contextErrorOccured) {
				//
				m_glxContext = glXCreateContext(display, m_visualInfo, sharedContext, GL_TRUE);
			}

		} else {
			//
			// OK, not supported, let's use old function.
			//
			m_glxContext = glXCreateContext(display, m_visualInfo, sharedContext, GL_TRUE);
			if(nullptr == m_glxContext) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLContextGLX, "glXCreateContext failed.\n");
				return RET_FAILED;
			}
		}

		if(glXMakeCurrent(display, window, m_glxContext) == False) {
			return RET_FAILED;
		}

		m_extensions->init();

		return RET_SUCCESS;
	}

	int XdevLOpenGLContextGLX::makeCurrent(XdevLWindow* window) {
		if(glXMakeCurrent(m_display, m_window, m_glxContext) == True) {
			return RET_SUCCESS;
		}
		return RET_FAILED;
	}

	void* XdevLOpenGLContextGLX::getInternal(const XdevLInternalName& id) {

		if(id == XdevLString(TEXT("GLX_CONTEXT"))) {
			return m_glxContext;
		} else if(id == XdevLString(TEXT("X11_VisualInfo"))) {
			return m_visualInfo;
		}
		return nullptr;
	}

	int XdevLOpenGLContextGLX::swapBuffers() {
		glXSwapBuffers(m_display, m_window);
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLContextGLX::setVSync(xdl_bool enableVSync) {
		xdl_int state = enableVSync ? 1 : 0;
		if(glXSwapIntervalEXT) {
			GLXDrawable drawable = glXGetCurrentDrawable();
			glXSwapIntervalEXT(m_display, drawable, state);
			return RET_SUCCESS;
		}
		return RET_FAILED;
	}

	xdl_bool XdevLOpenGLContextGLX::initMultisample() {
		return xdl_false;
	}

	xdl_int XdevLOpenGLContextGLX::setEnableFSAA(xdl_bool enableFSAA) {
		enableFSAA ? glEnable(GL_MULTISAMPLE) : glDisable(GL_MULTISAMPLE);
		return RET_SUCCESS;
	}

	void XdevLOpenGLContextGLX::dumpConfigInfo(const GLXFBConfig& config) {
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "------------ GLXFBConfig -----------\n");

		int fbconfigid;
		glXGetFBConfigAttrib(m_display, config, GLX_FBCONFIG_ID, &fbconfigid);

		int visualID;
		glXGetFBConfigAttrib(m_display, config, GLX_VISUAL_ID, &visualID);

		int doublebuffer;
		glXGetFBConfigAttrib(m_display, config, GLX_DOUBLEBUFFER, &doublebuffer);

		int sampleBuffer;
		glXGetFBConfigAttrib(m_display, config, GLX_SAMPLE_BUFFERS, &sampleBuffer);

		int stereo;
		glXGetFBConfigAttrib(m_display, config, GLX_STEREO, &stereo);

		int xrenderable;
		glXGetFBConfigAttrib(m_display, config, GLX_X_RENDERABLE, &xrenderable);

		int drawabletype;
		glXGetFBConfigAttrib(m_display, config, GLX_DRAWABLE_TYPE, &drawabletype);

		int auxbuffers;
		glXGetFBConfigAttrib(m_display, config, GLX_AUX_BUFFERS, &auxbuffers);

		int level;
		glXGetFBConfigAttrib(m_display, config, GLX_LEVEL, &level);

		int color_buffer_size, alpha_size, red_size, green_size, blue_size, depth_size, stencil_size, samples;
		glXGetFBConfigAttrib(m_display, config, GLX_BUFFER_SIZE, &color_buffer_size);
		glXGetFBConfigAttrib(m_display, config, GLX_ALPHA_SIZE, &alpha_size);
		glXGetFBConfigAttrib(m_display, config, GLX_RED_SIZE, &red_size);
		glXGetFBConfigAttrib(m_display, config, GLX_GREEN_SIZE, &green_size);
		glXGetFBConfigAttrib(m_display, config, GLX_BLUE_SIZE, &blue_size);
		glXGetFBConfigAttrib(m_display, config, GLX_DEPTH_SIZE, &depth_size);
		glXGetFBConfigAttrib(m_display, config, GLX_STENCIL_SIZE, &stencil_size);
		glXGetFBConfigAttrib(m_display, config, GLX_SAMPLES, &samples);

		int accum_red, accum_green, accum_blue, accum_alpha;
		glXGetFBConfigAttrib(m_display, config, GLX_ACCUM_RED_SIZE, &accum_red);
		glXGetFBConfigAttrib(m_display, config, GLX_ACCUM_GREEN_SIZE, &accum_green);
		glXGetFBConfigAttrib(m_display, config, GLX_ACCUM_BLUE_SIZE, &accum_blue);
		glXGetFBConfigAttrib(m_display, config, GLX_ACCUM_ALPHA_SIZE, &accum_alpha);

		int configCaveat;
		glXGetFBConfigAttrib(m_display, config, GLX_CONFIG_CAVEAT, &configCaveat);

		int transparentType, transparentIndexValue, transparentRed, transparentGreen, transparentBlue, transparentAlpha;
		glXGetFBConfigAttrib(m_display, config, GLX_TRANSPARENT_TYPE, &transparentType);
		glXGetFBConfigAttrib(m_display, config, GLX_TRANSPARENT_INDEX_VALUE, &transparentIndexValue);
		glXGetFBConfigAttrib(m_display, config, GLX_TRANSPARENT_RED_VALUE, &transparentRed);
		glXGetFBConfigAttrib(m_display, config, GLX_TRANSPARENT_GREEN_VALUE, &transparentGreen);
		glXGetFBConfigAttrib(m_display, config, GLX_TRANSPARENT_BLUE_VALUE, &transparentBlue);
		glXGetFBConfigAttrib(m_display, config, GLX_TRANSPARENT_ALPHA_VALUE, &transparentAlpha);

		int maxPBufferWidth, maxPBufferHeight, maxPBufferPixels;
		glXGetFBConfigAttrib(m_display, config, GLX_MAX_PBUFFER_WIDTH, &maxPBufferWidth);
		glXGetFBConfigAttrib(m_display, config, GLX_MAX_PBUFFER_HEIGHT, &maxPBufferHeight);
		glXGetFBConfigAttrib(m_display, config, GLX_MAX_PBUFFER_PIXELS, &maxPBufferPixels);


		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_FBCONFIG_ID               : " << fbconfigid << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_VISUAL_ID                 : " << visualID << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_LEVEL                     : " << level << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_DOUBLEBUFFER              : " << booleanToString(doublebuffer) << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_STEREO                    : " << booleanToString(stereo) << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_X_RENDERABLE              : " << booleanToString(xrenderable) << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_DRAWABLE_TYPE             : " << drawableTypeToString(drawabletype) << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_AUX_BUFFERS               : " << auxbuffers << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_BUFFER_SIZE               : " << color_buffer_size << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_RED_SIZE                  : " << red_size << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_GREEN_SIZE                : " << green_size << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_BLUE_SIZE                 : " << blue_size << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_ALPHA_SIZE                : " << alpha_size << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_ACCUM_RED_SIZE            : " << accum_red << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_ACCUM_GREEN_SIZE          : " << accum_green << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_ACCUM_BLUE_SIZE           : " << accum_blue << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_ACCUM_ALPHA_SIZE          : " << accum_alpha << "\n");


		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_DEPTH_SIZE                : " << depth_size << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_STENCIL_SIZE              : " << stencil_size << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_SAMPLE_BUFFERS            : " << samples << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_SAMPLES                   : " << samples << "\n");

		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_CONFIG_CAVEAT             : " << configCaveatTypeToString(configCaveat) << "\n");


		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_TRANSPARENT_TYPE          : " << transparencyTypeToString(transparentType) << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_TRANSPARENT_INDEX_VALUE   : " << transparentIndexValue << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_TRANSPARENT_RED_VALUE     : " << transparentRed << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_TRANSPARENT_GREEN_VALUE   : " << transparentGreen << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_TRANSPARENT_BLUE_VALUE    : " << transparentBlue << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_TRANSPARENT_ALPHA_VALUE   : " << transparentAlpha << "\n");

		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_MAX_PBUFFER_WIDTH         : " << maxPBufferWidth << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_MAX_PBUFFER_HEIGHT        : " << maxPBufferHeight << "\n");
		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "GLX_MAX_PBUFFER_PIXELS        : " << maxPBufferPixels << "\n");

		XDEVL_MODULEX_INFO(XdevLOpenGLContextGLX, "-------------------------------\n");
	}

}
