/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_OPENGL_COCOA_H
#define XDEVL_OPENGL_COCOA_H


#include <XdevLPluginImpl.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLOpenGLContext/XdevLOpenGLContext.h>
#include <XdevLOpenGLContext/XdevLOpenGLContextBase.h>

#include <X11/Xlib.h>
#include <X11/extensions/renderproto.h>
#include <GL/glx.h>
#include <GL/glxext.h>
#include <GL/glext.h>
#include <GL/glxproto.h>
//#include <GL/glxew.h>

namespace xdl {

	/**
		@class XdevLOpenGLContextGLX
		@brief
		@author Cengiz Terzibas
	*/
	class XdevLOpenGLContextGLX : public XdevLOpenGLContextBase {

		public:
			XdevLOpenGLContextGLX(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLOpenGLContextGLX();

			virtual void* getInternal(const XdevLInternalName& id) override;
			xdl_int init() override final;
			xdl_int shutdown() override final;

			xdl_vptr getProcAddress(const xdl_char* func) override final;
			xdl_int getAttributes(XdevLOpenGLContextAttributes& attributes) override final;
			xdl_int setAttributes(const XdevLOpenGLContextAttributes& attributes) override final;

			xdl_int create(XdevLWindow* window, XdevLOpenGLContext* shareContext = nullptr) override final;
			xdl_int create(XdevLWindow* window, const XdevLOpenGLContextAttributes& attributes, XdevLOpenGLContext* shareContext = nullptr) override final;
			xdl_int makeCurrent(XdevLWindow* window) override final;
			xdl_int swapBuffers() override final;

			xdl_int setVSync(xdl_bool enableVSync) override;

			GLXContext getNativeContext() const;

		private:

			xdl_bool initMultisample();
			xdl_int initOpenGL(Display* display, Window window);
			virtual xdl_int setEnableFSAA(xdl_bool enableFSAA);
			void dumpConfigInfo(const GLXFBConfig& config);

		private:
			//
			// X11 related variables.
			//
			Display* m_display;
			Window m_window;
			XdevLOpenGLContextGLX* m_shareContext;
			xdl_int m_screenNumber;

			//
			// GLX related variables.
			//
			GLXContext m_glxContext;
			xdl_int m_glxMajorVersion;
			xdl_int	m_glxMinorVersion;
			XVisualInfo* m_visualInfo;

			PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribs;
			PFNGLXSWAPINTERVALEXTPROC glXSwapIntervalEXT;
			PFNGLXSWAPINTERVALSGIPROC glXSwapIntervalSGI;
			XErrorHandler m_oldErrorHandler;
			std::vector<XdevLString> extensionsList;
	};

}


#endif
