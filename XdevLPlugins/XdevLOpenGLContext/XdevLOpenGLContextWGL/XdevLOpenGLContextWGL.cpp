
#include "XdevLOpenGLContextWGL.h"
#include <XdevLCoreMediator.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLXstring.h>
#include <XdevLPlatform.h>
#include <GL/GL.h>
#include <GL/wglext.h>
#include <tinyxml.h>

#define WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB 0x20A9

xdl::XdevLModuleDescriptor moduleDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::description,
	XDEVLOPENGLCONTEXT_WGL_MAJOR_VERSION,
	XDEVLOPENGLCONTEXT_WGL_MINOR_VERSION,
	XDEVLOPENGLCONTEXT_WGL_PATCH_VERSION
};

xdl::XdevLPluginDescriptor wglPluginDescriptor {
	xdl::pluginName,
	xdl::moduleNames,
	XDEVLOPENGLCONTEXT_WGL_MODULE_MAJOR_VERSION,
	XDEVLOPENGLCONTEXT_WGL_MODULE_MINOR_VERSION,
	XDEVLOPENGLCONTEXT_WGL_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(wglPluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLOpenGLWGL, moduleDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}


namespace xdl {

	XdevLOpenGLWGL::XdevLOpenGLWGL(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLOpenGLContextBase(parameter, descriptor),
		m_wnd(NULL),
		m_DC(NULL),
		m_RC(NULL),
		m_ARBMultisampleSupported(false) {
	}

	XdevLOpenGLWGL::~XdevLOpenGLWGL() {
	}

	xdl_vptr XdevLOpenGLWGL::getProcAddress(const char* func) {
		return reinterpret_cast<xdl_vptr>(wglGetProcAddress(func));
	}

	xdl_int XdevLOpenGLWGL::getAttributes(XdevLOpenGLContextAttributes& attributes) {
		attributes = m_attributes;
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLWGL::setAttributes(const XdevLOpenGLContextAttributes& attributes) {
		m_attributes = attributes;
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLWGL::init() {
		if (XdevLOpenGLContextBase::init() == RET_FAILED) {
			return RET_FAILED;
		}
		XDEVL_MODULEX_SUCCESS(XdevLOpenGLWGL, "Successful.\n");
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLWGL::create(XdevLWindow* window, const XdevLOpenGLContextAttributes& attributes, XdevLOpenGLContext* shareContext) {
		m_attributes = attributes;
		return create(window);
	}

	xdl_int XdevLOpenGLWGL::create(XdevLWindow* window, XdevLOpenGLContext* shareContext) {
		XDEVL_MODULEX_INFO(XdevLOpenGLWGL, "Creating OpenGL context.\n");

		if(nullptr == window) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "Parameter invalid.\n");
			return RET_FAILED;
		}

		m_window = window;

		m_wnd = static_cast<HWND>(m_window->getInternal(XdevLInternalName("WINDOW_HWND")));
		if (nullptr == m_wnd) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "Get WIN32_HWND failed.\n");
			return RET_FAILED;
		}

		if (initOpenGL() == RET_FAILED) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "Failed to initialize OpenGL.\n");
			return RET_FAILED;
		}

		// Set vertical syncronisation.
		setVSync(getVSync());
		
		XDEVL_MODULEX_SUCCESS(XdevLOpenGLWGL, "OpenGL Context created successful.\n");

		return RET_SUCCESS;
	}

	void* XdevLOpenGLWGL::getInternal(const XdevLInternalName& id) {
		if (id == XdevLString(TEXT("WIN32_DC")))
			return m_DC;
		else if (id == XdevLString(TEXT("WIN32_RC")))
			return m_RC;
		return nullptr;
	}

	xdl_int XdevLOpenGLWGL::shutdown() {
		if (m_RC) {
			if (!wglMakeCurrent(nullptr, nullptr)) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "wglMakeCurrent() failed.\n");
				return RET_FAILED;
			}
			if (!wglDeleteContext( m_RC)) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "wglDeleteContext() failed.\n");
				return RET_FAILED;
			}
		}
		m_RC = nullptr;
		if ( m_DC && !ReleaseDC( m_wnd, m_DC)) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "ReleaseDC() failed.\n");
			return RET_FAILED;
		}
		m_DC = nullptr;
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLWGL::initOpenGL() {
		m_DC = GetDC(m_wnd);
		if(nullptr == m_DC) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "GetDC() failed.");
			return RET_FAILED;
		}

		// --------------------------------------------------------------------
		// Create the old style context (OpenGL 2.1 and before) because without 
		// a OpenGL context wglGetProcAddress will always fail and return 
		// nullptr.

		PIXELFORMATDESCRIPTOR pfd;
		memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = m_attributes.color_buffer_size;
		pfd.cDepthBits = m_attributes.depth_size;
		pfd.cStencilBits = m_attributes.stencil_size;
		pfd.cRedBits = m_attributes.red_size;
		pfd.cGreenBits = m_attributes.green_size;
		pfd.cBlueBits = m_attributes.blue_size;
		pfd.cAlphaBits = m_attributes.alpha_size;
		pfd.iLayerType = PFD_MAIN_PLANE;

		int iPixelFormat = ChoosePixelFormat(m_DC, &pfd);
		if (FALSE == iPixelFormat) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "ChoosePixelFormat failed.\n");
			return RET_FAILED;
		}

		if (SetPixelFormat(m_DC, iPixelFormat, &pfd) != TRUE) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "SetPixelFormat failed.\n");
			return RET_FAILED;
		}

		HGLRC hRC = wglCreateContext(m_DC);
		wglMakeCurrent(m_DC, hRC);

		// Now get the neccessary extensions.

		// WGL_ARB_create_context, WGL_ARB_create_context_profile
		wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");

		//  WGL_ARB_pixel_format
		wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
		wglGetPixelFormatAttribivARB = (PFNWGLGETPIXELFORMATATTRIBIVARBPROC)wglGetProcAddress("wglGetPixelFormatAttribivARB");
		wglGetPixelFormatAttribfvARB = (PFNWGLGETPIXELFORMATATTRIBFVARBPROC)wglGetProcAddress("wglGetPixelFormatAttribfvARB");

		// WGL_ARB_extensions_string
		wglGetExtensionsStringARB = (PFNWGLGETEXTENSIONSSTRINGARBPROC)wglGetProcAddress("wglGetExtensionsStringARB");

		// EXT_swap_control
		wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
		wglGetSwapIntervalEXT = (PFNWGLGETSWAPINTERVALEXTPROC)wglGetProcAddress("wglGetSwapIntervalEXT");

		XdevLString extensions(wglGetExtensionsStringARB(m_DC));

		std::vector<XdevLString> listOfExtensions;
		extensions.tokenize(listOfExtensions, XdevLString(TEXT(" ")));
		
		std::cout << "Supported WGL extensions: ------------------------------------------------------" << std::endl;

		for (auto& extension : listOfExtensions) {
			std::cout << extension << std::endl;
		}
		std::cout << "--------------------------------------------------------------------------------" << std::endl;

		// And get rid of the old context. We don't need it anymore.
		wglMakeCurrent(nullptr, nullptr);
		wglDeleteContext(hRC);

		// --------------------------------------------------------------------

		if (wglCreateContextAttribsARB) {
			// Lets start preparing the attribute names for wglChoosePixelFormatARB.

			std::vector<int> attribute_list;

			attribute_list.push_back(WGL_DRAW_TO_WINDOW_ARB);
			attribute_list.push_back(GL_TRUE);

			attribute_list.push_back(WGL_SUPPORT_OPENGL_ARB);
			attribute_list.push_back(GL_TRUE);

			attribute_list.push_back(WGL_ACCELERATION_ARB);
			attribute_list.push_back(WGL_FULL_ACCELERATION_ARB);

			attribute_list.push_back(WGL_SWAP_METHOD_ARB);
			attribute_list.push_back(WGL_SWAP_EXCHANGE_ARB);

			if (m_attributes.red_size > 0) {
				attribute_list.push_back(WGL_RED_BITS_ARB);
				attribute_list.push_back(m_attributes.red_size);
			}

			if (m_attributes.green_size > 0) {
				attribute_list.push_back(WGL_GREEN_BITS_ARB);
				attribute_list.push_back(m_attributes.green_size);
			}

			if (m_attributes.blue_size > 0) {
				attribute_list.push_back(WGL_BLUE_BITS_ARB);
				attribute_list.push_back(m_attributes.blue_size);
			}

			if (m_attributes.alpha_size > 0) {
				attribute_list.push_back(WGL_ALPHA_BITS_ARB);
				attribute_list.push_back(m_attributes.alpha_size);
			}

			if (m_attributes.double_buffer > 0) {
				attribute_list.push_back(WGL_DOUBLE_BUFFER_ARB);
				attribute_list.push_back(GL_TRUE);
			}

			attribute_list.push_back(WGL_DEPTH_BITS_ARB);
			attribute_list.push_back(m_attributes.depth_size);

			if (m_attributes.stencil_size > 0) {
				attribute_list.push_back(WGL_STENCIL_BITS_ARB);
				attribute_list.push_back(m_attributes.stencil_size);
			}

			if (m_attributes.accum_red_size > 0) {
				attribute_list.push_back(WGL_ACCUM_RED_BITS_ARB);
				attribute_list.push_back(m_attributes.accum_red_size);
			}

			if (m_attributes.accum_green_size > 0) {
				attribute_list.push_back(WGL_ACCUM_GREEN_BITS_ARB);
				attribute_list.push_back(m_attributes.accum_green_size);
			}

			if (m_attributes.accum_blue_size > 0) {
				attribute_list.push_back(WGL_ACCUM_BLUE_BITS_ARB);
				attribute_list.push_back(m_attributes.accum_blue_size);
			}

			if (m_attributes.accum_alpha_size > 0) {
				attribute_list.push_back(WGL_ACCUM_ALPHA_BITS_ARB);
				attribute_list.push_back(m_attributes.accum_alpha_size);
			}

			if (xdl_true == m_attributes.stereo) {
				attribute_list.push_back(WGL_STEREO_ARB);
				attribute_list.push_back(GL_TRUE);
			}

			// ARB_multisample
			if (m_attributes.multisample_buffers > 0) {
				// Enable Multisampling.
				attribute_list.push_back(WGL_SAMPLE_BUFFERS_ARB);
				attribute_list.push_back(GL_TRUE);
				// Set Multisampling value.
				attribute_list.push_back(WGL_SAMPLES_ARB);
				attribute_list.push_back(m_attributes.multisample_samples);
			}

			// ARB_framebuffer_sRGB
			if (xdl_true == m_attributes.srgb) {
				attribute_list.push_back(WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB);
				attribute_list.push_back(GL_TRUE);
			}

			attribute_list.push_back(0);

			const int iPixelFormatAttribList[] = {
				WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
				WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
				WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
				WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
				WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
				WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
				WGL_COLOR_BITS_ARB, m_attributes.color_buffer_size,
				WGL_DEPTH_BITS_ARB, m_attributes.depth_size,
				WGL_STENCIL_BITS_ARB, m_attributes.stencil_size,
				WGL_ACCUM_BITS_ARB, 0,
				WGL_RED_BITS_ARB, m_attributes.red_size,
				WGL_GREEN_BITS_ARB, m_attributes.green_size,
				WGL_BLUE_BITS_ARB, m_attributes.blue_size,
				WGL_ALPHA_BITS_ARB, m_attributes.alpha_size,
				0 // End of attributes list
			};

			int iPixelFormat, iNumFormats;
			if (wglChoosePixelFormatARB(m_DC, iPixelFormatAttribList, nullptr, 1, &iPixelFormat, (UINT*)&iNumFormats) != TRUE) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "wglChoosePixelFormatARB failed.\n");
				return RET_FAILED;
			}

			// PFD seems to be only redundant parameter now
			if (SetPixelFormat(m_DC, iPixelFormat, &pfd) != TRUE) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "SetPixelFormat failed.\n");
				return RET_FAILED;
			}

			//
			// Set the core profile attributes.
			//
			std::vector<xdl_int> contextAttributes;
			contextAttributes.push_back(WGL_CONTEXT_MAJOR_VERSION_ARB);
			contextAttributes.push_back(m_attributes.context_major_version);
			contextAttributes.push_back(WGL_CONTEXT_MINOR_VERSION_ARB);
			contextAttributes.push_back(m_attributes.context_minor_version);

			contextAttributes.push_back(WGL_CONTEXT_PROFILE_MASK_ARB);
			if (m_attributes.context_profile_mask == XDEVL_OPENGL_CONTEXT_CORE_PROFILE) {
				contextAttributes.push_back(WGL_CONTEXT_CORE_PROFILE_BIT_ARB);
			} else if (m_attributes.context_profile_mask == XDEVL_OPENGL_CONTEXT_COMPATIBILITY) {
				contextAttributes.push_back(WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB);
			} else if (m_attributes.context_profile_mask == XDEVL_OPENGL_CONTEXT_ES1) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "Not supported WGL_CONTEXT_PROFILE_MASK_ARB .\n");
				return RET_FAILED;
			} else if (m_attributes.context_profile_mask == XDEVL_OPENGL_CONTEXT_ES2) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "Not supported WGL_CONTEXT_PROFILE_MASK_ARB .\n");
				return RET_FAILED;
			} else {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "Not supported WGL_CONTEXT_PROFILE_MASK_ARB .\n");
				return RET_FAILED;
			}

			//
			// Set the WGL_CONTEXT_FLAGS_ARB
			//
			auto contextFlags = 0;

			if (m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_DEBUG_BIT) {
				contextFlags |= WGL_CONTEXT_DEBUG_BIT_ARB;
			}

			if (m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_FORWARD_COMPATIBLE_BIT) {
				contextFlags |= WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB;
			}

			// KHR_no_error
			if (m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_NO_ERROR_BIT) {
				contextFlags |= WGL_CONTEXT_FLAG_NO_ERROR_BIT_KHR;
			}

			// ARB_create_context_robustness
			if (m_attributes.context_flags & XDEVL_OPENGL_CONTEXT_FLAGS_ROBUST_ACCESS_BIT) {
				contextFlags |= WGL_CONTEXT_ROBUST_ACCESS_BIT_ARB;

				attribute_list.push_back(WGL_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB);
				if (XDEVL_OPENGL_CONTEXT_NO_RESET_NOTIFICATION == m_attributes.robustness) {
					attribute_list.push_back(WGL_NO_RESET_NOTIFICATION_ARB);
				}
				else if (XDEVL_OPENGL_CONTEXT_LOSE_CONTEXT_ON_RESET == m_attributes.robustness) {
					attribute_list.push_back(WGL_LOSE_CONTEXT_ON_RESET_ARB);
				}
			}

			if (contextFlags) {
				contextAttributes.push_back(WGL_CONTEXT_FLAGS_ARB);
				contextAttributes.push_back(contextFlags);
			}

			contextAttributes.push_back(0);

			m_RC = wglCreateContextAttribsARB(m_DC, 0, contextAttributes.data());
			if (nullptr == m_RC) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "wglCreateContextAttribsARB failed.\n");
				return RET_FAILED;
			}
			// If everything went OK
			if (wglMakeCurrent(m_DC, m_RC) != TRUE) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "wglMakeCurrent failed.\n");
				return RET_FAILED;
			}

		} else {
			// Are We Able To Get A Rendering Context?
			m_RC = wglCreateContext(m_DC);
			if (nullptr == m_RC) {
				XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "wglCreateContext failed.\n");
				return RET_FAILED;
			}
		}
		//m_extensions->init();
		std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << ", Vendor: " << glGetString(GL_VENDOR) << std::endl;
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLWGL::swapBuffers() {
		//	wglSwapLayerBuffers(m_DC, 1);
		SwapBuffers(m_DC);
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLWGL::makeCurrent(XdevLWindow* window) {
		if (wglMakeCurrent(m_DC, m_RC) != TRUE) {
			XDEVL_MODULEX_ERROR(XdevLOpenGLWGL, "wglMakeCurrent failed.\n");
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLOpenGLWGL::setVSync(xdl_bool state) {
		if (wglSwapIntervalEXT) {
			if(state)
				wglSwapIntervalEXT(1);
			else
				wglSwapIntervalEXT(0);

			return RET_SUCCESS;
		} else {
			XDEVL_MODULEX_WARNING(XdevLOpenGLWGL, "VSync not supported.\n");
		}
		return RET_FAILED;
	}

	xdl_int XdevLOpenGLWGL::setEnableFSAA(xdl_bool state) {
		return RET_FAILED;
	}

}
