#ifndef XDEVL_OPENGL_CONTEXT_WGL_H
#define XDEVL_OPENGL_CONTEXT_WGL_H

#include <XdevLPluginImpl.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLOpenGLContext/XdevLOpenGLContextBase.h>

namespace xdl {

	static const XdevLString description {
		"Module to create a WGL OpenGL context."
	};

	static const XdevLString pluginName {
		"XdevLOpenGLContextWGL"
	};

	static const std::vector<XdevLModuleName> moduleNames {
		XdevLModuleName("XdevLOpenGLContext")
	};

	//
	// Create Context ARB
	//
	#define WGL_CONTEXT_DEBUG_BIT_ARB 0x0001
	#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x0002
	#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
	#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
	#define WGL_CONTEXT_FLAGS_ARB 0x2094

	//
	// WGL_ARB_create_context_profile
	//
	#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
	#define WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB 0x00000002
	#define WGL_CONTEXT_PROFILE_MASK_ARB 0x9126

	// KHR_no_error
	#define WGL_CONTEXT_FLAG_NO_ERROR_BIT_KHR    0x00000008

	// WGL_ARB_create_context_robustness
	#define WGL_CONTEXT_ROBUST_ACCESS_BIT_ARB       0x00000004
	#define WGL_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB     0x8256
	#define  WGL_NO_RESET_NOTIFICATION_ARB                   0x8261
	#define WGL_LOSE_CONTEXT_ON_RESET_ARB                   0x8252


	typedef HGLRC(WINAPI * PFNWGLCREATECONTEXTATTRIBSARBPROC) (HDC hDC, HGLRC hShareContext, const int* attribList);
	typedef BOOL(WINAPI * PFNWGLGETPIXELFORMATATTRIBIVARBPROC) (HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, const int *piAttributes, int *piValues);
	typedef BOOL(WINAPI * PFNWGLGETPIXELFORMATATTRIBFVARBPROC) (HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, const int *piAttributes, FLOAT *pfValues);

	typedef BOOL(WINAPI * PFNWGLCHOOSEPIXELFORMATARBPROC) (HDC hdc, const int* piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats);
	typedef const char * (WINAPI * PFNWGLGETEXTENSIONSSTRINGARBPROC) (HDC hdc);

	typedef BOOL(WINAPI * PFNWGLSWAPINTERVALEXTPROC) (int interval);
	typedef int (WINAPI * PFNWGLGETSWAPINTERVALEXTPROC) (void);


	/**
		@class XdevLOpenGLWGL
		@brief Support for OpenGL Context under Windows.
		@author Cengiz Terzibas
	*/
	class XdevLOpenGLWGL : public XdevLOpenGLContextBase {
		public:
			XdevLOpenGLWGL(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

			virtual ~XdevLOpenGLWGL();

			xdl_int init() override final;
			xdl_int shutdown() override final;
			void* getInternal(const XdevLInternalName& id) override final;

			xdl_int create(XdevLWindow* window, XdevLOpenGLContext* shareContext = nullptr) override final;
			xdl_int create(XdevLWindow* window, const XdevLOpenGLContextAttributes& attributes, XdevLOpenGLContext* shareContext = nullptr) override final;
			xdl_int getAttributes(XdevLOpenGLContextAttributes& attributes) override final;
			xdl_int setAttributes(const XdevLOpenGLContextAttributes& attributes) override final;
			xdl_int makeCurrent(XdevLWindow* window) override final;
			xdl_int swapBuffers() override final;
			xdl_vptr getProcAddress(const xdl_char* func) override final;
			xdl_int setVSync(xdl_bool state) override final;
			xdl_int setEnableFSAA(xdl_bool state);

		private:

			///initializes win32 OpenGL
			xdl_int initOpenGL();

		protected:

			IPXdevLWindow m_window;

			/// Holds the win32 window handle.
			HWND				m_wnd;
			/// Holds the win32 graphics context.
			HDC					m_DC;
			/// Holds the OpenGL render context.
			HGLRC				m_RC;
			/// Holds  state if FSAA is supported.
			xdl_bool	m_ARBMultisampleSupported;


			PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB;
			PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;
			PFNWGLGETSWAPINTERVALEXTPROC wglGetSwapIntervalEXT;
			PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB;
			PFNWGLGETPIXELFORMATATTRIBIVARBPROC wglGetPixelFormatAttribivARB;
			PFNWGLGETPIXELFORMATATTRIBFVARBPROC wglGetPixelFormatAttribfvARB;
			PFNWGLGETEXTENSIONSSTRINGARBPROC wglGetExtensionsStringARB;

			 

	};

}

#endif
