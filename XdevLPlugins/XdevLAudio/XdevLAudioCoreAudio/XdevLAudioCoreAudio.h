/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_COREAUDIO_IMPL_H
#define XDEVL_COREAUDIO_IMPL_H

#include <XdevLAudio/XdevLAudio.h>

#include <XdevLPluginImpl.h>


#if !defined(__IPHONEOS__)
#define MACOSX_COREAUDIO 1
#endif

#if MACOSX_COREAUDIO
#include <CoreAudio/CoreAudio.h>
#include <CoreServices/CoreServices.h>
#else
#include <AudioToolbox/AudioToolbox.h>
#endif

#include <AudioUnit/AudioUnit.h>

namespace xdl {

	namespace audio {

		static const XdevLString pluginName {
			"XdevLAudioCoreAudio"
		};

		static const XdevLString moduleDescription {
			"This is an empty module"
		};

		static std::vector<XdevLModuleName>	moduleNames {
			XdevLModuleName("XdevLAudioPlayback"),
			XdevLModuleName("XdevLAudioCapture")
		};

		class XdevLAudioBufferCoreAudioBuffer : public XdevLAudioBuffer {
			public:
				virtual ~XdevLAudioBufferCoreAudioBuffer() {}

				virtual xdl_int getFormat() override;
				virtual xdl_int getSize() ;
				virtual xdl_int getSamplingRate();
				virtual xdl_int getChannels();
				virtual xdl_int getBits();
				virtual xdl_uint getID();
				virtual xdl_uint8* getData();
				virtual xdl_int lock();
				virtual xdl_int unlock();
				virtual xdl_int upload(xdl_int8* src, xdl_uint size);
			public:
				xdl_uint				m_bufferFormatSize;
				xdl_uint 				m_samplingRate;
				xdl_uint				m_channels;
				xdl_uint				m_bufferSizeInBytes;
				xdl_uint8*				m_buffer;
		};

		class XdevLAudioCoreAudio : public XdevLModuleImpl<XdevLAudio> {

			public:

				XdevLAudioCoreAudio(XdevLModuleCreateParameter* parameter);

				virtual ~XdevLAudioCoreAudio();
				virtual xdl_int init() override;
				virtual xdl_int shutdown() override;


				virtual IPXdevLAudioBuffer create(XdevLAudioStreamType streamType, XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels);
				virtual xdl_int write(xdl_uint8* buffer);
				virtual xdl_int read(xdl_uint8* buffer);
				virtual xdl_uint getBufferSize() override;
				virtual xdl_uint getNumberOfChannels() override ;
				virtual xdl_uint getFormatSizeInBytes() override;
				virtual xdl_uint getPeriodTime() override;
				virtual xdl_uint getSamplingRate() override;
				virtual xdl_uint getPeriodSize() override;
				virtual xdl_int update2() override;


				virtual void setCallbackFunction(callbackFunctionType callbackFuntion, void* userData) override;
				virtual IPXdevLAudioBuffer createAudioBufferFromFile(IPXdevLFile& file);
				virtual IPXdevLAudioBuffer createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data);
				virtual IPXdevLAudioSource createAudioSource(IPXdevLAudioBuffer buffer) override;
				virtual void setGain(xdl_float gain) override;
				virtual xdl_int makeCurrent() override;
				virtual xdl_int releaseCurrent() override;




			private:
				AudioUnit audioUnit;
				xdl_bool m_audioUnitOpen;
				UInt32 bufferOffset;
				UInt32 bufferSize;
#if MACOSX_COREAUDIO
				AudioDeviceID deviceID;
				AudioDeviceID* m_deviceList;
#endif
				XdevLAudioStreamType m_streamType;
				xdl_uint m_bufferSize;
				xdl_uint m_samplingRate;
				xdl_uint m_channels;
				xdl_uint m_bufferFormatSize;
				xdl_uint m_periodTime;
				xdl_float m_gain;

				callbackFunctionType m_callbackFunction;
				void* m_userData;
				xdl_uint8* m_buffer;

		};
	}
}

#endif
