/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_ASSET_SERVER_IMPL_H
#define XDEVL_AUDIO_ASSET_SERVER_IMPL_H

#include <XdevLPluginImpl.h>
#include <XdevLAudio/XdevLAudio.h>

namespace xdl {

	namespace audio {

		static const XdevLString pluginName {
			"XdevLAudio"
		};
		static const XdevLString description {
			"Creates Audio Playback System."
		};
		static const XdevLString description2 {
			"Creates Audio Capture System."
		};
		static const XdevLString description3 {
			"Import and Manage Audio Assets."
		};
		static std::vector<XdevLModuleName>	moduleNames {
			XdevLModuleName("XdevLAudioPlayback"),
			XdevLModuleName("XdevLAudioCapture"),
			XdevLModuleName("XdevLAudioAssetServer")
		};

		template<typename T>
		class XdevLAudioBase : public XdevLModuleImpl<T> {
			public:
				XdevLAudioBase(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
					: XdevLModuleImpl<T>(parameter, descriptor)
					, m_playbackCallbackFunction(nullptr)
					, m_userData(nullptr) {
				}

				void setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType playbackCallbackFuntion, void* userData) override final {
					m_playbackCallbackFunction = playbackCallbackFuntion;
					m_userData = userData;
				}

			protected:

				XdevLPlaybackCallbackFunctionType m_playbackCallbackFunction;
				void* m_userData;
		};



		class XdevLAudioAssetServerImpl : public XdevLModuleImpl<XdevLAudioAssetServer> {
			public:
				XdevLAudioAssetServerImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
				virtual ~XdevLAudioAssetServerImpl();

				xdl_int plug(const XdevLPluginName& pluginName, const XdevLVersion& version = xdl::XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, 0)) override final;
				IPXdevLAudioImportPlugin getImporter(const XdevLString& extension) override final;
				IPXdevLAudioBuffer import(IPXdevLFile& file, IPXdevLAudio audio) override final;

			private:

				std::map<xdl_uint64, XdevLAudioImportPluginInfo> m_pluginMap;
		};
	}
}

#endif
