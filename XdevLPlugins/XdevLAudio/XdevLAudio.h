/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_H
#define XDEVL_AUDIO_H

#include <XdevL.h>
#include <XdevLModule.h>

#include <XdevLFileSystem/XdevLFileSystem.h>
#include <XdevLAudio/XdevLAudioBuffer.h>
#include <XdevLAudio/XdevLAudioSource.h>

namespace xdl {

	namespace audio {

		/**
			@enum XdevLAudioStreamType
			@brief Types of audio stream types.
		*/
		enum XdevLAudioStreamType {
			AUDIO_STREAM_PLAYBACK,
			AUDIO_STREAM_CAPTURE
		};

		/**
			@enum XdevLAudioBufferFormat
			@brief Data type of one audio element in the buffer.
		*/
		enum XdevLAudioBufferFormat {
			AUDIO_BUFFER_FORMAT_UNKNOWN
			, AUDIO_BUFFER_FORMAT_S8
			, AUDIO_BUFFER_FORMAT_U8
			, AUDIO_BUFFER_FORMAT_S16
			, AUDIO_BUFFER_FORMAT_U16
			, AUDIO_BUFFER_FORMAT_S24
			, AUDIO_BUFFER_FORMAT_U24
			, AUDIO_BUFFER_FORMAT_S32
			, AUDIO_BUFFER_FORMAT_U32
			, AUDIO_BUFFER_FORMAT_FLOAT
			, AUDIO_BUFFER_FORMAT_DOUBLE
		};

		/**
		 * @brief Returns the size in bits of the specified XdevLAudioBufferFormat.
		 * @param format The audio buffer format to get the size of.
		 * @return The size in bits occupied by the format in memory.
		 */
		inline xdl_uint XdevLAudioBufferFormatBitSize(XdevLAudioBufferFormat format) {
			switch(format) {
				case AUDIO_BUFFER_FORMAT_S8:
				case AUDIO_BUFFER_FORMAT_U8:
					return 8;
				case AUDIO_BUFFER_FORMAT_S16:
				case AUDIO_BUFFER_FORMAT_U16:
					return 16;
				case AUDIO_BUFFER_FORMAT_S24:
				case AUDIO_BUFFER_FORMAT_U24:
					return 24;
				case AUDIO_BUFFER_FORMAT_S32:
				case AUDIO_BUFFER_FORMAT_U32:
				case AUDIO_BUFFER_FORMAT_FLOAT:
					return 32;
				case AUDIO_BUFFER_FORMAT_DOUBLE:
					return 64;
				default:
					break;
			}
			return AUDIO_BUFFER_FORMAT_UNKNOWN;
		}

		/**
		 * @brief Returns the size in bytes of the specified XdevLAudioBufferFormat.
		 * @param format The audio buffer format to get the size of.
		 * @return The size in bytes occupied by the format in memory.
		 */
		inline xdl_uint XdevLAudioBufferFormatBytes(XdevLAudioBufferFormat format) {
			switch(format) {
				case AUDIO_BUFFER_FORMAT_S8:
				case AUDIO_BUFFER_FORMAT_U8:
					return 1;
				case AUDIO_BUFFER_FORMAT_S16:
				case AUDIO_BUFFER_FORMAT_U16:
					return 2;
				case AUDIO_BUFFER_FORMAT_S24:
				case AUDIO_BUFFER_FORMAT_U24:
					return 3;
				case AUDIO_BUFFER_FORMAT_S32:
				case AUDIO_BUFFER_FORMAT_U32:
				case AUDIO_BUFFER_FORMAT_FLOAT:
					return 4;
				case AUDIO_BUFFER_FORMAT_DOUBLE:
					return 8;
				default:
					break;
			}
			return AUDIO_BUFFER_FORMAT_UNKNOWN;
		}

		/**
			@enum XdevLAudioSamplingRate
			@brief Audio sampling rates.
		*/
		enum XdevLAudioSamplingRate {
			AUDIO_SAMPLE_RATE_8000 	= 8000,
			AUDIO_SAMPLE_RATE_11025 = 11025,
			AUDIO_SAMPLE_RATE_22050 = 22050,
			AUDIO_SAMPLE_RATE_44100 = 44100,
			AUDIO_SAMPLE_RATE_48000 = 48000,
			AUDIO_SAMPLE_RATE_96000 = 96000
		};

		struct XdevLAudioSpecs {
			xdl_int numberOfChannels;
			XdevLAudioSamplingRate samplingRate;
			XdevLAudioBufferFormat format;
		};

		using XdevLPlaybackCallbackFunctionType = xdl_uint(*)(xdl_uint8* buffer, xdl_uint samples, void* userData);

		/**
			@class XdevLAudio
			@brief Class that manages audio context, buffers and sources.
			@author Cengiz Terzibas
		*/
		class XdevLAudio : public XdevLModule {
			public:
				virtual ~XdevLAudio() {};

				/// Sets the callback functions that allows modification of audio data.
				/**
				 * @param callbackFuntion A valid callback function.
				 * @param userData User data that should be passed to the callback function.
				 */
				virtual void setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType playbackCallbackFuntion, void* userData = nullptr) = 0;

				/// Create audio buffer from file.
				/**
					At the moment only .wav files are supported.

					@param filename Filename that points to a valid audio file.
					@param buffer A pointer to an XdevLAudioBuffer;
					@return
					- @b RET_SUCCESS If it was successful. The (*buffer) pointer will have a valid pointer to an
					XdevLAudioBuffer object.
					- @b RET_FAILED If it fails to create a buffer. The pointer (*buffer) will be set to NULL.
				*/
				virtual IPXdevLAudioBuffer createAudioBufferFromFile(IPXdevLFile& file) = 0;

				/// Creates an empty audio buffer.
				/**
					@param format Specifies the audioBufferFormat of the audio buffer.
					@param frequency Specifies the base frequence of the audio buffer.
					@param size The size of the buffer. The size depends on the format.
					@param data A buffer that will be copied into the audio buffer.
					@param buffer A pointer to an XdevLAudioBuffer;
					@return
					- @b RET_SUCCESS If it was successful. The (*buffer) pointer will have a valid pointer to an
					XdevLAudioBuffer object.
					- @b RET_FAILED If it fails to create a buffer. The pointer (*buffer) will be set to NULL.
				*/
				virtual IPXdevLAudioBuffer createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) = 0;

				/// Creates an audio source.
				/**
					@param src The valid pointer to an XdevLAudioSource pointer which will hold the source object.
					@param buffer A pointer to an valid XdevLAudioBuffer;
					@return
					- @b RET_SUCCESS If it was successful. The (*src) pointer will have a valid pointer to an
					XdevLAudioSource object.
					- @b RET_FAILED If it fails to create a source. The pointer (*src) will be set to NULL.
				*/
				virtual IPXdevLAudioSource createAudioSource(IPXdevLAudioBuffer buffer) = 0;

				/// Sets the gain of the listener.
				virtual void setGain(xdl_float gain) = 0;

				/// Makes the audio context to the current.
				virtual xdl_int makeCurrent() = 0;

				/// Release current context.
				virtual xdl_int releaseCurrent() = 0;

				virtual xdl_uint getSamplingRate() = 0;
				virtual xdl_uint getBufferSize() = 0;
				virtual xdl_uint getNumberOfChannels() = 0;
				virtual xdl_uint getFormatSizeInBytes() = 0;
				virtual xdl_uint getPeriodTime() = 0;
				virtual xdl_uint getPeriodSize() = 0;
				virtual xdl_int update2() = 0;
		};

		/**
		 * @class XdevLAudioPlayback
		 * @brief Interface that handles playing sounds.
		 */
		class XdevLAudioPlayback : public XdevLAudio {

			public:
				virtual ~XdevLAudioPlayback() {};
				virtual xdl_int write(xdl_uint8* src, xdl_int size) = 0;
		};

		/**
		 * @class XdevLAudioCapture
		 * @brief Interface that handles recording sounds.
		 */
		class XdevLAudioCapture : public XdevLAudio {

			public:
				virtual ~XdevLAudioCapture() {};
				virtual xdl_int read(xdl_uint8* src, xdl_int size) = 0;
		};

		/**
			@class XdevLAudioContextScope
			@brief Scope class for the audio interface

			Can be used to make the context current and release
			automatically.

		*/
		class XdevLAudioContextScope {
			public:
				XdevLAudioContextScope(XdevLAudio* audio) : m_audio(audio) {
					m_audio->makeCurrent();
				}
				virtual ~XdevLAudioContextScope() {
					m_audio->releaseCurrent();
				}
			private:
				XdevLAudio* m_audio;
		};

		using IXdevLAudio = XdevLAudio;
		using IPXdevLAudio = XdevLAudio*;

		using IXdevLAudioPlayback = XdevLAudioPlayback;
		using IPXdevLAudioPlayback = XdevLAudioPlayback*;

		using IXdevLAudioCapture = XdevLAudioCapture;
		using IPXdevLAudioCapture = XdevLAudioCapture*;

		//
		// Audio Import Plugin.
		//
		class XdevLAudioImportPlugin {
			public:
				virtual ~XdevLAudioImportPlugin() {}

				virtual IPXdevLAudioBuffer import(IPXdevLFile& file, IPXdevLAudio audio) = 0;
				virtual XdevLString getExtension() = 0;
		};

		using IXdevLAudioImportPlugin = XdevLAudioImportPlugin;
		using IPXdevLAudioImportPlugin = std::shared_ptr<XdevLAudioImportPlugin>;


		class XdevLAudioImportPluginCreateParameter {
			public:
				IPXdevLAudioImportPlugin pluginInstance;
		};

		using XdevLCreateAudioImportPluginFunction = void(*)(XdevLAudioImportPluginCreateParameter& parameter);

		class XdevLAudioImportPluginInfo {
			public:
				XdevLID id;
				IPXdevLAudioImportPlugin audioImportPlugin;
		};

		class XdevLAudioAssetServer : public XdevLModule {
			public:
				virtual ~XdevLAudioAssetServer() {}
				virtual xdl_int plug(const XdevLPluginName& pluginName, const XdevLVersion& version = xdl::XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, 0)) = 0;
				virtual IPXdevLAudioImportPlugin getImporter(const XdevLString& extension) = 0;
				virtual IPXdevLAudioBuffer import(IPXdevLFile& file, IPXdevLAudio audio) = 0;
		};

		using IXdevLAudioAssetServer = XdevLAudioAssetServer;
		using IPXdevLAudioAssetServer = XdevLAudioAssetServer*;

		XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLAudioPlayback)
		XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLAudioCapture)
		XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLAudioAssetServer)
	}
}

#endif
