/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLAudioAlsa.h"

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::audio::pluginName,
	xdl::audio::moduleNames,
	XDEVLAUDIOALSA_PLUGIN_MAJOR_VERSION,
	XDEVLAUDIOALSA_PLUGIN_MINOR_VERSION,
	XDEVLAUDIOALSA_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDescriptorPlayback {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::audio::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::audio::moduleDescription,
	XDEVLAUDIOALSA_MODULE_MAJOR_VERSION,
	XDEVLAUDIOALSA_MODULE_MINOR_VERSION,
	XDEVLAUDIOALSA_MODULE_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDescriptorCapture {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::audio::moduleNames[1],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::audio::moduleDescription,
	XDEVLAUDIOALSA_MODULE_MAJOR_VERSION,
	XDEVLAUDIOALSA_MODULE_MINOR_VERSION,
	XDEVLAUDIOALSA_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::audio::XdevLAudioPlaybackImpl, moduleDescriptorPlayback)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::audio::XdevLAudioCaptureImpl, moduleDescriptorCapture)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}