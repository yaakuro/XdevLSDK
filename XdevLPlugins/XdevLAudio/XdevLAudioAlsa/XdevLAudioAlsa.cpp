/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLAudioAlsa.h"

namespace xdl {

	namespace audio {

		_snd_pcm_format wrapXdevLAudioBufferFormatToAlsaFormat(XdevLAudioBufferFormat format) {
			switch(format) {
				case AUDIO_BUFFER_FORMAT_S8:
					return SND_PCM_FORMAT_S8;
				case AUDIO_BUFFER_FORMAT_U8:
					return SND_PCM_FORMAT_U8;
				case AUDIO_BUFFER_FORMAT_S16:
					return SND_PCM_FORMAT_S16;
				case AUDIO_BUFFER_FORMAT_U16:
					return SND_PCM_FORMAT_U16;
				case AUDIO_BUFFER_FORMAT_S24:
					return SND_PCM_FORMAT_S24;
				case AUDIO_BUFFER_FORMAT_U24:
					return SND_PCM_FORMAT_U24;
				case AUDIO_BUFFER_FORMAT_S32:
					return SND_PCM_FORMAT_S32;
				case AUDIO_BUFFER_FORMAT_U32:
					return SND_PCM_FORMAT_U32;
				case AUDIO_BUFFER_FORMAT_FLOAT:
					return SND_PCM_FORMAT_FLOAT;
				case AUDIO_BUFFER_FORMAT_DOUBLE:
					return SND_PCM_FORMAT_FLOAT64;
				default:
					break;
			}
			return SND_PCM_FORMAT_UNKNOWN;
		}

		XdevLAudioBufferAlsa::XdevLAudioBufferAlsa(_snd_pcm_format format, XdevLAudioSamplingRate samplingRate, xdl_uint channels) :
			m_bufferFormat(format),
			m_samplingRate(samplingRate),
			m_channels(channels) {
		}

		xdl_int XdevLAudioBufferAlsa::getFormat() {
			switch(m_bufferFormat) {
				case SND_PCM_FORMAT_S8:
					return AUDIO_BUFFER_FORMAT_S8;
					break;
				case SND_PCM_FORMAT_U8:
					return AUDIO_BUFFER_FORMAT_U8;
					break;
				case SND_PCM_FORMAT_S16:
					return AUDIO_BUFFER_FORMAT_S16;
					break;
				case SND_PCM_FORMAT_U16:
					return AUDIO_BUFFER_FORMAT_U16;
					break;
				case SND_PCM_FORMAT_S24:
					return AUDIO_BUFFER_FORMAT_S24;
					break;
				case SND_PCM_FORMAT_U24:
					return AUDIO_BUFFER_FORMAT_U24;
					break;
				case SND_PCM_FORMAT_S32:
					return AUDIO_BUFFER_FORMAT_S32;
					break;
				case SND_PCM_FORMAT_U32:
					return AUDIO_BUFFER_FORMAT_U32;
					break;
				case SND_PCM_FORMAT_FLOAT:
					return AUDIO_BUFFER_FORMAT_FLOAT;
					break;
				case SND_PCM_FORMAT_FLOAT64:
					return AUDIO_BUFFER_FORMAT_DOUBLE;
					break;
				case SND_PCM_FORMAT_S16_BE:
				case SND_PCM_FORMAT_U16_BE:
				case SND_PCM_FORMAT_S24_BE:
				case SND_PCM_FORMAT_U24_BE:
				case SND_PCM_FORMAT_S32_BE:
				case SND_PCM_FORMAT_U32_BE:
				case SND_PCM_FORMAT_FLOAT_BE:
				case SND_PCM_FORMAT_IEC958_SUBFRAME_LE:
				case SND_PCM_FORMAT_MU_LAW:
				case SND_PCM_FORMAT_A_LAW:
				case SND_PCM_FORMAT_IMA_ADPCM:
				default:
					assert(0 && "Not handled format");

			}
			return AUDIO_BUFFER_FORMAT_UNKNOWN;
		}

		xdl_uint XdevLAudioBufferAlsa::getSize() {
			return m_bufferSizeInBytes;
		}

		xdl_int XdevLAudioBufferAlsa::getSamplingRate() {
			return m_samplingRate;
		}

		xdl_int XdevLAudioBufferAlsa::getChannels() {
			return m_channels;
		}

		xdl_int XdevLAudioBufferAlsa::getBits() {
			return m_bufferFormatSize * 8;
		}

		xdl_uint XdevLAudioBufferAlsa::getID() {
			return 0;
		}

		xdl_uint8* XdevLAudioBufferAlsa::getData() {
			return nullptr;
		}

		xdl_int XdevLAudioBufferAlsa::lock() {
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioBufferAlsa::unlock() {
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioBufferAlsa::upload(xdl_int8* src, xdl_uint size) {
			return RET_SUCCESS;
		}



//
//
//




		XdevLAudioAlsaBase::XdevLAudioAlsaBase(const XdevLModuleDescriptor& descriptor) :
			m_descriptor(descriptor),
			m_handle(nullptr),
			m_hwParams(nullptr),
			m_name("plughw:0:0"),
			m_channels(1),
			m_playbackCallbackFunction(nullptr),
			m_buffer(nullptr) {
		}

		XdevLAudioAlsaBase::~XdevLAudioAlsaBase() {}

		xdl_int XdevLAudioAlsaBase::init() {

//		debugDump();

			return RET_SUCCESS;
		}

		xdl_int XdevLAudioAlsaBase::shutdown() {

			if(nullptr != m_hwParams) {
				snd_pcm_hw_params_free(m_hwParams);
				m_hwParams = nullptr;
			}

			if(nullptr != m_handle) {
				snd_pcm_drop(m_handle);
				snd_pcm_drain(m_handle);
				snd_pcm_close(m_handle);
			}
			if(nullptr != m_buffer) {
				delete [] m_buffer;
			}
			return RET_SUCCESS;
		}

		IPXdevLAudioBuffer XdevLAudioAlsaBase::createAudioBufferFromFile(IPXdevLFile& file) {
			return nullptr;
		}

		IPXdevLAudioBuffer XdevLAudioAlsaBase::create(XdevLAudioStreamType streamType, XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels) {

			m_supportedStreamType = streamType;
			m_samplingRate = samplingRate;
			m_channels = channels;
			m_bufferFormat = wrapXdevLAudioBufferFormatToAlsaFormat(bufferFormat);
			m_bufferFormatSize = XdevLAudioBufferFormatBitSize(bufferFormat)/8;

			switch(streamType) {
				case AUDIO_STREAM_PLAYBACK:
					m_streamType = SND_PCM_STREAM_PLAYBACK;
					break;
				case AUDIO_STREAM_CAPTURE:
					m_streamType = SND_PCM_STREAM_CAPTURE;
					break;
				default:
					m_streamType = SND_PCM_STREAM_PLAYBACK;
					break;
			}

			// Open device
			auto result = snd_pcm_open(&m_handle, getDeviceName().toString().c_str(), m_streamType, 0);
			if(result < 0) {
				XDEVL_MODULE_INFO("Could not open PCM device: " << getDeviceName() << " ->" << snd_strerror(result) << std::endl);
				return nullptr;
			}
			XDEVL_MODULE_SUCCESS("PCM Device: '" << getDeviceName() << "' opened successfully." << std::endl);

			// Set parameters for the device.
			if(setHardwareParameters(m_channels, m_bufferFormat, m_samplingRate) != RET_SUCCESS) {
				return nullptr;
			}

			// Apply the parameters
			if(applyHardwareParameters() != RET_SUCCESS) {
				return nullptr;
			}

			//
			// Print out some infos:
			//
			XDEVL_MODULE_INFO("PCM Device info \n");
			XDEVL_MODULE_INFO("PCM name        : " << snd_pcm_name(m_handle) << "\n");
			XDEVL_MODULE_INFO("PCM state       : " << snd_pcm_state_name(snd_pcm_state(m_handle)) << "\n");

			xdl_uint tmp;
			snd_pcm_hw_params_get_channels(m_hwParams, &tmp);
			XDEVL_MODULE_INFO("PCM channels    : " << tmp << "\n");

			snd_pcm_hw_params_get_rate(m_hwParams, &tmp, nullptr);
			XDEVL_MODULE_INFO("PCM rate        : " << tmp << "\n");

			snd_pcm_hw_params_get_period_time(m_hwParams, &tmp, nullptr);
			XDEVL_MODULE_INFO("PCM period time : " << tmp << "\n");
			XDEVL_MODULE_INFO("PCM period size : " << m_periodSize << "\n");
			XDEVL_MODULE_INFO("--------------------------------------------\n");

			// Now make the hardware ready to use.
			result = snd_pcm_prepare(m_handle);
			if(result < 0) {
				return nullptr;
			}

			auto buffer = std::shared_ptr<XdevLAudioBufferAlsa>(new XdevLAudioBufferAlsa(m_bufferFormat, samplingRate, m_channels));

			return buffer;
		}

		IPXdevLAudioBuffer XdevLAudioAlsaBase::createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) {
			m_samplingRate = samplingRate;
			m_channels = channels;
			m_bufferFormat = wrapXdevLAudioBufferFormatToAlsaFormat(format);
			m_bufferFormatSize = XdevLAudioBufferFormatBitSize(format)/8;

			auto tmp = std::shared_ptr<XdevLAudioBufferAlsa>(new XdevLAudioBufferAlsa(m_bufferFormat, samplingRate, channels));
			return tmp;
		}

		XdevLString XdevLAudioAlsaBase::getDeviceName() {
			const char* deviceName = getenv("AUDIODEV");
			if(nullptr != deviceName) {
				return XdevLString(deviceName);
			}

			switch(m_channels) {
				case 6:
					return XdevLString("plug:surround51");
				case 4:
					return XdevLString("plug:surround40");
				default:
					break;
			}

			return XdevLString("default");
		}

		xdl_int XdevLAudioAlsaBase::setHardwareParameters(xdl_uint numberOfChannels, _snd_pcm_format bufferformat, xdl_uint samplingRate) {
			xdl_int err;

			if(nullptr != m_hwParams) {
				snd_pcm_hw_params_free(m_hwParams);
				m_hwParams = nullptr;
			}

			// Allocate parameters object.
			// TODO Do we have to free this allocated parameter?
			auto result = snd_pcm_hw_params_malloc(&m_hwParams);
			if(result < 0) {
				XDEVL_MODULE_INFO("Could allocate memory for hardware parameter " << "->" << snd_strerror(result) << std::endl);
				return RET_FAILED;
			}

			// Get the default values and put it into the structure.
			if((err = snd_pcm_hw_params_any(m_handle, m_hwParams)) < 0) {
				return RET_FAILED;
			}

			// Set interleaved mode for data access. (This tells how the samples for the different channels are ordered in RAM.
			// Interleaving means that we alternate samples for the left and right channel (LRLRLR)
			if((err = snd_pcm_hw_params_set_access(m_handle, m_hwParams, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
				return RET_FAILED;
			}

			// Desired buffer format ( 8, 16, 32 etc. bit)
			if((err = snd_pcm_hw_params_set_format(m_handle, m_hwParams, bufferformat)) < 0) {
				XDEVL_MODULE_INFO("Could set hardware format:" << snd_strerror(err) << std::endl);
				return RET_FAILED;
			}

			// Desired sampling rate.
			if((err = snd_pcm_hw_params_set_rate_near(m_handle, m_hwParams, &samplingRate, 0)) < 0) {
				XDEVL_MODULE_INFO("Could not set the desired sampling rate: " << samplingRate << " : " << snd_strerror(err) << std::endl);
				return RET_FAILED;
			}

			// Number of channels.
			if((err = snd_pcm_hw_params_set_channels(m_handle, m_hwParams, numberOfChannels)) < 0) {
				if((err = snd_pcm_hw_params_get_channels(m_hwParams, &numberOfChannels)) < 0) {
					XDEVL_MODULE_INFO("Could not set the number of channels: " << numberOfChannels << " : " << snd_strerror(err) << std::endl);
					return RET_FAILED;
				}
			}
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioAlsaBase::applyHardwareParameters() {
			xdl_int err;

			// Write the parameters to the driver
			if((err = snd_pcm_hw_params(m_handle, m_hwParams)) < 0) {
				XDEVL_MODULE_INFO("Could write parameters to hardware: " << snd_strerror(err) << std::endl);
				return RET_FAILED;
			}

			//
			// Allocate buffer for one period
			//

			// TODO For now let's use this buffer to transfer data.
			snd_pcm_hw_params_get_period_size(m_hwParams, &m_periodSize, nullptr);
			m_bufferSize = m_periodSize * m_channels * m_bufferFormatSize;

			m_buffer = new xdl_uint8[m_bufferSize];

			snd_pcm_hw_params_get_period_time(m_hwParams, &m_periodTime, nullptr);

			return RET_SUCCESS;
		}


		xdl_int XdevLAudioAlsaBase::setPeriodSize(snd_pcm_uframes_t frames) {

			if(snd_pcm_hw_params_set_period_size_near(m_handle, m_hwParams, &frames, nullptr) < 0) {
				return RET_FAILED;
			}

			xdl_uint periods = 2;
			if(snd_pcm_hw_params_set_periods_near(m_handle, m_hwParams, &periods, nullptr) < 0) {
				return RET_FAILED;
			}

			return RET_SUCCESS;
		}

		xdl_int XdevLAudioAlsaBase::write(xdl_uint8* src, xdl_int size) {
			xdl_int err;
			if((err = snd_pcm_writei(m_handle, src, size)) == EPIPE) {
				snd_pcm_prepare(m_handle);
				XDEVL_MODULE_INFO("Alsa buffer overrun ..."<< std::endl);
			} else if(err < 0) {
				XDEVL_MODULE_INFO(snd_strerror(err) << std::endl);
			}
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioAlsaBase::read(xdl_uint8* src, xdl_int size) {
			xdl_int err;
			err = snd_pcm_readi(m_handle, src, size);
			if(err == -EPIPE) {
				/* EPIPE means overrun */
				fprintf(stderr, "overrun occurred\n");
				snd_pcm_prepare(m_handle);
				return RET_FAILED;
			} else if(err < 0) {
				fprintf(stderr,"error from read: %s\n", snd_strerror(err));
				return RET_FAILED;
			} else if(err != (int)size) {
				fprintf(stderr, "short read, read %d frames\n", err);
				return RET_FAILED;
			}
			return RET_SUCCESS;
		}

		void XdevLAudioAlsaBase::debugDump() {
			int val;

			printf("ALSA library version: %s\n", SND_LIB_VERSION_STR);

			printf("\nPCM stream types:\n");
			for(val = 0; val <= SND_PCM_STREAM_LAST; val++) {
				if(strcmp(snd_pcm_stream_name((snd_pcm_stream_t)val), "PLAYBACK") == 0) {

				} else if(strcmp(snd_pcm_stream_name((snd_pcm_stream_t)val), "CAPTURE") == 0) {

				}
			}
			printf("\nPCM access types:\n");
			for(val = 0; val <= SND_PCM_ACCESS_LAST; val++)
				printf("  %s\n", snd_pcm_access_name((snd_pcm_access_t)val));

			printf("\nPCM formats:\n");
			for(val = 0; val <= SND_PCM_FORMAT_LAST; val++) {
				if(snd_pcm_format_name((snd_pcm_format_t)val) != NULL)
					printf("  %s (%s)\n",  snd_pcm_format_name((snd_pcm_format_t)val), snd_pcm_format_description((snd_pcm_format_t)val));
			}
			printf("\nPCM subformats:\n");
			for(val = 0; val <= SND_PCM_SUBFORMAT_LAST; val++)
				printf("  %s (%s)\n", snd_pcm_subformat_name((snd_pcm_subformat_t)val), snd_pcm_subformat_description((snd_pcm_subformat_t)val));

			printf("\nPCM states:\n");
			for(val = 0; val <= SND_PCM_STATE_LAST; val++)
				printf("  %s\n",
				       snd_pcm_state_name((snd_pcm_state_t)val));
		}

		xdl_int XdevLAudioAlsaBase::update2() {

			// If no callback function is defined, just return back.
			if(nullptr == m_playbackCallbackFunction) {
				return RET_FAILED;
			}

			// Wait for a PCM to become ready.
			auto result = snd_pcm_wait(m_handle, 1000);
			if(0 == result) {
				XDEVL_MODULEX_ERROR(XdevLAudioAlsaBase, "snd_pcm_wait: Timeout: " << snd_strerror(result) << std::endl);
				return RET_FAILED;
			} else if(result < 0) {
				XDEVL_MODULEX_ERROR(XdevLAudioAlsaBase, "snd_pcm_wait: xrun: " << snd_strerror(result) << std::endl);
				return RET_FAILED;
			}

			// Find out how much space is available for playback data.
			snd_pcm_sframes_t frames_to_deliver;
			if((frames_to_deliver = snd_pcm_avail_update(m_handle)) < 0) {
				if(-EPIPE == frames_to_deliver) {
					XDEVL_MODULEX_ERROR(XdevLAudioAlsaBase, "snd_pcm_avail_update: xrun: " << snd_strerror(result) << std::endl);
					return RET_FAILED;
				} else {
					XDEVL_MODULEX_ERROR(XdevLAudioAlsaBase, "snd_pcm_avail_update: Unknown ALSA avail update return value: " << (xdl_uint)frames_to_deliver << std::endl);
					return RET_FAILED;
				}
			}

			frames_to_deliver = (frames_to_deliver > static_cast<snd_pcm_sframes_t>(m_periodSize)) ? m_periodSize : frames_to_deliver;

			// Deliver the data.
			xdl_uint frames = 0;
			do {
				frames += m_playbackCallbackFunction(m_buffer, frames_to_deliver, m_userData);
			} while(frames < frames_to_deliver);


			result = snd_pcm_writei(m_handle, m_buffer, frames_to_deliver);
			if(result == -EPIPE) {
				snd_pcm_prepare(m_handle);
				XDEVL_MODULE_INFO("Alsa buffer overrun ..."<< std::endl);
			} else if(result < 0) {
				XDEVL_MODULE_INFO(snd_strerror(result) << std::endl);
			}

			return RET_SUCCESS;
		}

		xdl_uint XdevLAudioAlsaBase::getSamplingRate() {
			return m_samplingRate;
		}

		xdl_uint XdevLAudioAlsaBase::getBufferSize() {
			return m_bufferSize;
		}

		xdl_uint XdevLAudioAlsaBase::getNumberOfChannels() {
			return m_channels;
		}
		xdl_uint XdevLAudioAlsaBase::getFormatSizeInBytes() {
			return m_bufferFormatSize;
		}
		xdl_uint XdevLAudioAlsaBase::getPeriodTime() {
			return m_periodTime;
		}

		xdl_uint XdevLAudioAlsaBase::getPeriodSize() {
			return m_periodSize;
		}

		void XdevLAudioAlsaBase::setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData) {
			m_playbackCallbackFunction = callbackFuntion;
			m_userData = userData;
		}

		void XdevLAudioAlsaBase::setGain(xdl_float gain) {
			m_gain = gain;
		}
		xdl_int XdevLAudioAlsaBase::makeCurrent() {
			return RET_SUCCESS;
		}
		xdl_int XdevLAudioAlsaBase::releaseCurrent() {
			return RET_SUCCESS;
		}

		//
		// Playback
		//


		XdevLAudioPlaybackImpl::XdevLAudioPlaybackImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
			XdevLAudioAlsaBase(descriptor),
			XdevLModuleImpl<XdevLAudioPlayback> (parameter, descriptor) {
		}

		XdevLAudioPlaybackImpl::~XdevLAudioPlaybackImpl() {}

		xdl_int XdevLAudioPlaybackImpl::init() {
			return XdevLAudioAlsaBase::init();
		}

		xdl_int XdevLAudioPlaybackImpl::shutdown() {
			return XdevLAudioAlsaBase::shutdown();
		}

		IPXdevLAudioBuffer XdevLAudioPlaybackImpl::createAudioBufferFromFile(IPXdevLFile& file) {
			return XdevLAudioAlsaBase::createAudioBufferFromFile(file);
		}

//	xdl_int XdevLAudioPlaybackImpl::create(XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels, XdevLAudioBuffer** buffer) {
//		return XdevLAudioAlsaBase::create(AUDIO_STREAM_PLAYBACK, bufferFormat, samplingRate, channels, buffer);
//	}

		IPXdevLAudioBuffer XdevLAudioPlaybackImpl::createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) {
			return XdevLAudioAlsaBase::create(AUDIO_STREAM_PLAYBACK, format, samplingRate, channels);
		}

		IPXdevLAudioSource XdevLAudioPlaybackImpl::createAudioSource(IPXdevLAudioBuffer buffer) {
			return nullptr;
		}

		xdl_int XdevLAudioPlaybackImpl::write(xdl_uint8* src, xdl_int size) {
			return XdevLAudioAlsaBase::write(src, size);
		}

		xdl_int XdevLAudioPlaybackImpl::update2() {
			return XdevLAudioAlsaBase::update2();
		}

		xdl_uint XdevLAudioPlaybackImpl::getSamplingRate() {
			return XdevLAudioAlsaBase::getSamplingRate();
		}

		xdl_uint XdevLAudioPlaybackImpl::getBufferSize() {
			return XdevLAudioAlsaBase::getBufferSize();
		}

		xdl_uint XdevLAudioPlaybackImpl::getNumberOfChannels() {
			return XdevLAudioAlsaBase::getNumberOfChannels();
		}
		xdl_uint XdevLAudioPlaybackImpl::getFormatSizeInBytes() {
			return XdevLAudioAlsaBase::getFormatSizeInBytes();
		}
		xdl_uint XdevLAudioPlaybackImpl::getPeriodTime() {
			return XdevLAudioAlsaBase::getPeriodTime();
		}

		xdl_uint XdevLAudioPlaybackImpl::getPeriodSize() {
			return XdevLAudioAlsaBase::getPeriodSize();
		}

		void XdevLAudioPlaybackImpl::setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData) {
			XdevLAudioAlsaBase::setPlaybackCallbackFunction(callbackFuntion, userData);
		}

		void XdevLAudioPlaybackImpl::setGain(xdl_float gain) {
			return XdevLAudioAlsaBase::setGain(gain);
		}
		xdl_int XdevLAudioPlaybackImpl::makeCurrent() {
			return XdevLAudioAlsaBase::makeCurrent();
		}
		xdl_int XdevLAudioPlaybackImpl::releaseCurrent() {
			return XdevLAudioAlsaBase::releaseCurrent();
		}



		//
		// Capture
		//

		XdevLAudioCaptureImpl::XdevLAudioCaptureImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
			XdevLAudioAlsaBase(descriptor),
			XdevLModuleImpl<XdevLAudioCapture> (parameter, descriptor) {
		}

		XdevLAudioCaptureImpl::~XdevLAudioCaptureImpl() {}

		xdl_int XdevLAudioCaptureImpl::init() {
			return XdevLAudioAlsaBase::init();
		}

		xdl_int XdevLAudioCaptureImpl::shutdown() {
			return XdevLAudioAlsaBase::shutdown();
		}

		IPXdevLAudioBuffer XdevLAudioCaptureImpl::createAudioBufferFromFile(IPXdevLFile& file) {
			return XdevLAudioAlsaBase::createAudioBufferFromFile(file);
		}

//	xdl_int XdevLAudioCaptureImpl::create(XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels, XdevLAudioBuffer** buffer) {
//		return XdevLAudioAlsaBase::create(AUDIO_STREAM_CAPTURE, bufferFormat, samplingRate, channels, buffer);
//	}

		IPXdevLAudioBuffer XdevLAudioCaptureImpl::createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) {
			return XdevLAudioAlsaBase::create(AUDIO_STREAM_CAPTURE, format, samplingRate, channels);
		}

		IPXdevLAudioSource XdevLAudioCaptureImpl::createAudioSource(IPXdevLAudioBuffer buffer) {
			return nullptr;
		}

		xdl_int XdevLAudioCaptureImpl::read(xdl_uint8* src, xdl_int size) {
			return XdevLAudioAlsaBase::read(src, size);
		}

		xdl_int XdevLAudioCaptureImpl::update2() {
			return XdevLAudioAlsaBase::update2();
		}

		xdl_uint XdevLAudioCaptureImpl::getSamplingRate() {
			return XdevLAudioAlsaBase::getSamplingRate();
		}

		xdl_uint XdevLAudioCaptureImpl::getBufferSize() {
			return XdevLAudioAlsaBase::getBufferSize();
		}

		xdl_uint XdevLAudioCaptureImpl::getNumberOfChannels() {
			return XdevLAudioAlsaBase::getNumberOfChannels();
		}
		xdl_uint XdevLAudioCaptureImpl::getFormatSizeInBytes() {
			return XdevLAudioAlsaBase::getFormatSizeInBytes();
		}
		xdl_uint XdevLAudioCaptureImpl::getPeriodTime() {
			return XdevLAudioAlsaBase::getPeriodTime();
		}

		xdl_uint XdevLAudioCaptureImpl::getPeriodSize() {
			return XdevLAudioAlsaBase::getPeriodSize();
		}

		void XdevLAudioCaptureImpl::setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData) {
			XdevLAudioAlsaBase::setPlaybackCallbackFunction(callbackFuntion, userData);
		}

		void XdevLAudioCaptureImpl::setGain(xdl_float gain) {
			return XdevLAudioAlsaBase::setGain(gain);
		}
		xdl_int XdevLAudioCaptureImpl::makeCurrent() {
			return XdevLAudioAlsaBase::makeCurrent();
		}
		xdl_int XdevLAudioCaptureImpl::releaseCurrent() {
			return XdevLAudioAlsaBase::releaseCurrent();
		}
	}
}
