/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_BUFFER_H
#define XDEVL_AUDIO_BUFFER_H

#include <XdevLTypes.h>

namespace xdl {

	namespace audio {

		class XdevLAudioSource;

		/**
			@class XdevLAudioBuffer
			@brief Buffer that holds audio data.
		*/
		class XdevLAudioBuffer {
			public:
				virtual ~XdevLAudioBuffer() {};

				/// Returns the format of the audio buffer.
				/**
				 * @return Type of XdevLAudioBufferFormat.
				 */
				virtual xdl_int getFormat() = 0;

				/// Returns the size of the audio buffer in bytes.
				/**
					@return Size of the buffer in bytes.
				*/
				virtual xdl_uint getSize() = 0;
				/// Returns the frequency of the audio data.
				/**
					@return The base frequency of the recorded audio data.
				*/
				virtual xdl_int getSamplingRate() = 0;
				/// Returns the number of channels.
				/**
					@return The number of channels used in this buffer.
				*/
				virtual xdl_int getChannels() = 0;
				/// Returns the resolution of one audio data element in bits.
				/**
					@return The resolution of one element in the audio buffer in bits.
				*/
				virtual xdl_int getBits() = 0;

				/// Returns the identification code.
				virtual xdl_uint getID()= 0;

				/// Returns a pointer to the data.
				virtual xdl_uint8* getData() = 0;

				/// Lock the buffer for modification.
				/**
					Locks the buffer for modification. Before one can upload/change data
					the buffer needs to be locked. After finishing the job unlock the buffer.
				*/
				virtual xdl_int lock() = 0;

				/// Unlock the buffer after modification.
				virtual xdl_int unlock() = 0;

				/// Upload data to the buffer.
				/**
					The buffer maybe be re-sized depended on the size value.

					@param src The pointer to the data array to upload.
					@param size The the number of bytes to upload from the 'src'.
				*/
				virtual xdl_int upload(xdl_int8* src, xdl_uint size) = 0;

		};

		using IXdevLAudioBuffer = XdevLAudioBuffer;
		using IPXdevLAudioBuffer = std::shared_ptr<XdevLAudioBuffer>;
	}
}

#endif
