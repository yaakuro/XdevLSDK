/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_AL_IMPL_H
#define XDEVL_AUDIO_AL_IMPL_H

#include <XdevLUtils.h>
#if defined(__APPLE__)
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif


#include <XdevLAudio/XdevLAudioBase.h>
#include "XdevLAudioBufferAL.h"
#include "XdevLAudioSourceAL.h"

namespace xdl {

	namespace audio {

		/**
			@class XdevLAudioPlaybackAL
			@brief Class to support mouse devices
		*/
		class XdevLAudioPlaybackAL : public XdevLAudioBase<XdevLAudio> {
			public:
				XdevLAudioPlaybackAL(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
				virtual ~XdevLAudioPlaybackAL();

				virtual xdl_int init() override;
				virtual xdl_int shutdown() override;

				virtual IPXdevLAudioBuffer createAudioBufferFromFile(IPXdevLFile& file) override;
				virtual IPXdevLAudioBuffer createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) override;
				virtual IPXdevLAudioSource createAudioSource(IPXdevLAudioBuffer buffer) override;
				virtual void setGain(xdl_float gain) override;
				virtual xdl_int makeCurrent() override;
				virtual xdl_int releaseCurrent() override;

				virtual xdl_uint getSamplingRate() override {
					return 0;
				}
				virtual xdl_uint getBufferSize() override {
					return 0;
				}
				virtual xdl_uint getNumberOfChannels() override {
					return 0;
				}
				virtual xdl_uint getFormatSizeInBytes() override {
					return 0;
				}
				virtual xdl_uint getPeriodTime() override {
					return 0;
				}
				virtual xdl_uint getPeriodSize() override {
					return 0;
				}
				virtual xdl_int update2() override {
					return 0;
				}
			protected:
				ALCdevice* m_device;
				ALCcontext* m_context;
				ALCcontext* m_previous;
				IPXdevLAudioAssetServer m_audioAssetServer;
		};


		/**
			@class XdevLAudioALRecord
			@brief Extended version of the XdevLAudioAL version which supports recording.
		*/
		class XdevLAudioALRecord : public XdevLAudioBase<XdevLAudioCapture> {
			public:
				XdevLAudioALRecord(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
				virtual ~XdevLAudioALRecord() {};

				virtual xdl_int init() override;
				virtual xdl_int shutdown() override;

				virtual void start();
				virtual void stop();
				virtual xdl_int device(const XdevLString& deviceName, XdevLAudioBuffer* audioBuffer, xdl_int samples);
				virtual xdl_int device(const XdevLString& deviceName, XdevLAudioBufferFormat format,  XdevLAudioSamplingRate samplerate, xdl_uint channels, xdl_int buffer_size);
				virtual xdl_int capture(XdevLAudioBuffer* audioBuffer);
				virtual xdl_int capture(xdl_int8* buffer, xdl_int samples);
				virtual xdl_int sampleSize();

				virtual xdl_int read(xdl_uint8* src, xdl_int size) override;

			protected:
				ALCdevice* 	m_recordDevice;
				ALenum 		m_audioFormat;
				ALCuint 	m_sampleRate;
				ALCsizei	m_bufferSize;
				ALCsizei	m_sampleSize;
				xdl_bool	m_recordingStarted;
		};

		/// Returns AL error codes as string.
		std::string getALErrorString(ALenum err);
		/// Returns ALC error codes as string.
		std::string getALCErrorString(ALenum err);
		ALenum wrapAudioBufferFormat(XdevLAudioBufferFormat format, xdl_uint numberOfChannels);
	}
}

#endif
