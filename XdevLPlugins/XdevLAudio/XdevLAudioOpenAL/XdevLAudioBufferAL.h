/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_BUFFER_IMPL_H
#define XDEVL_AUDIO_BUFFER_IMPL_H

#include <XdevLAudio/XdevLAudioBuffer.h>

namespace xdl {

	namespace audio {

		/**
			@class XdevLAudioBufferAL
			@brief Holds the raw audio stream data.
			@author Cengiz Terzibas
		*/
		class XdevLAudioBufferAL : public XdevLAudioBuffer {
			public:

				XdevLAudioBufferAL(ALenum format, ALsizei freq, ALsizei size);
				virtual ~XdevLAudioBufferAL();

				xdl_int getFormat() override final;
				xdl_uint getSize() override final;
				xdl_int getSamplingRate() override final;
				xdl_int getChannels() override final;
				xdl_int getBits() override final;
				xdl_uint getID() override final;
				xdl_uint8* getData() override final;
				xdl_int lock() override final;
				xdl_int unlock() override final;
				xdl_int upload(xdl_int8* src, xdl_uint size) override final;

			private:

				ALuint m_id;
				ALenum m_format;
				ALsizei m_freq;
				ALsizei m_size;
				xdl_bool m_locked;
				std::vector<xdl_uint8> m_buffer;
		};
	}
}

#endif
