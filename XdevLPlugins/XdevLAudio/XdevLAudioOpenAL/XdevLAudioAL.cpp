/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLCoreMediator.h>
#include <XdevLAudio/XdevLAudioBase.h>
#include "XdevLAudioAL.h"

namespace xdl {

	namespace audio {

		std::string getALErrorString(ALenum err) {
			switch(err) {
				case AL_NO_ERROR:
					return std::string("AL_NO_ERROR");
					break;

				case AL_INVALID_NAME:
					return std::string("AL_INVALID_NAME");
					break;

				case AL_INVALID_ENUM:
					return std::string("AL_INVALID_ENUM");
					break;

				case AL_INVALID_VALUE:
					return std::string("AL_INVALID_VALUE");
					break;

				case AL_INVALID_OPERATION:
					return std::string("AL_INVALID_OPERATION");
					break;

				case AL_OUT_OF_MEMORY:
					return std::string("AL_OUT_OF_MEMORY");
					break;
			};
			return std::string("UNKNOWN_AL_ERROR");
		}

		std::string getALCErrorString(ALenum err) {
			switch(err) {
				case ALC_NO_ERROR:
					return std::string("AL_NO_ERROR");
					break;

				case ALC_INVALID_DEVICE:
					return std::string("ALC_INVALID_DEVICE");
					break;

				case ALC_INVALID_CONTEXT:
					return std::string("ALC_INVALID_CONTEXT");
					break;

				case ALC_INVALID_ENUM:
					return std::string("ALC_INVALID_ENUM");
					break;

				case ALC_INVALID_VALUE:
					return std::string("ALC_INVALID_VALUE");
					break;

				case ALC_OUT_OF_MEMORY:
					return std::string("ALC_OUT_OF_MEMORY");
					break;
			};
			return std::string("UNKNOWN_ALC_ERROR");
		}



		ALenum wrapAudioBufferFormat(XdevLAudioBufferFormat format, xdl_uint numberOfChannels) {
			ALenum alFormat = 0;
			switch(format) {
				case AUDIO_BUFFER_FORMAT_S8: {
					if(numberOfChannels == 1) {
						alFormat = AL_FORMAT_MONO8;
					} else if(numberOfChannels == 2) {
						alFormat = AL_FORMAT_STEREO8;
					} else {
						alFormat = AL_FORMAT_MONO8;
					}
				}
				break;
				case AUDIO_BUFFER_FORMAT_S16: {
					if(numberOfChannels == 1) {
						alFormat = AL_FORMAT_MONO16;
					} else if(numberOfChannels == 2) {
						alFormat = AL_FORMAT_STEREO16;
					} else {
						alFormat = AL_FORMAT_MONO8;
					}
				}
				break;
				case AUDIO_BUFFER_FORMAT_UNKNOWN:
				default: {
					alFormat = AL_FORMAT_MONO8;
				}
				break;
			}
			return alFormat;
		}




		XdevLAudioPlaybackAL::XdevLAudioPlaybackAL(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
			XdevLAudioBase<XdevLAudio>(parameter, descriptor),
			m_device(nullptr),
			m_context(nullptr),
			m_previous(nullptr) {
		}

		XdevLAudioPlaybackAL::~XdevLAudioPlaybackAL() {
		}

		xdl_int XdevLAudioPlaybackAL::makeCurrent() {
			m_previous = alcGetCurrentContext();
			alcMakeContextCurrent(m_context);
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioPlaybackAL::releaseCurrent() {
			alcMakeContextCurrent(m_previous);
			return RET_SUCCESS;
		}
		xdl_int XdevLAudioPlaybackAL::init() {
			m_device = alcOpenDevice(nullptr);
			if(m_device == nullptr) {
				XDEVL_MODULE_ERROR(getALCErrorString(alGetError()) << std::endl);
				return RET_FAILED;
			}
			m_context = alcCreateContext(m_device, nullptr);
			if(m_context == nullptr) {
				XDEVL_MODULE_ERROR(getALCErrorString(alGetError()) << std::endl);
				return RET_FAILED;
			}

			alcMakeContextCurrent(m_context);

			alDistanceModel(AL_NONE);

			m_audioAssetServer = static_cast<IPXdevLAudioAssetServer>(getMediator()->createModule(XdevLModuleName("XdevLAudioAssetServer"), XdevLID("XdevLAudioALAudioAssetServer")));
			m_audioAssetServer->plug(XdevLPluginName("XdevLAudioImportPluginWAV"));

			return RET_SUCCESS;
		}

		xdl_int XdevLAudioPlaybackAL::shutdown() {
//		alutExit();
			if(nullptr != m_context) {
				alcDestroyContext(m_context);
				alcCloseDevice(m_device);
			}

			return RET_SUCCESS;
		}

		IPXdevLAudioBuffer XdevLAudioPlaybackAL::createAudioBufferFromFile(IPXdevLFile& file) {
			return m_audioAssetServer->import(file, this);
		}

		IPXdevLAudioBuffer XdevLAudioPlaybackAL::createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) {

			ALenum fmat = wrapAudioBufferFormat(format, channels);

			std::shared_ptr<XdevLAudioBuffer> tmp = std::shared_ptr<XdevLAudioBufferAL>(new XdevLAudioBufferAL(fmat, samplingRate, size));
			tmp->lock();
			tmp->upload((xdl_int8*)data, size);
			tmp->unlock();

			return tmp;
		}

		IPXdevLAudioSource XdevLAudioPlaybackAL::createAudioSource(IPXdevLAudioBuffer buffer) {

			std::shared_ptr<XdevLAudioSource> tmp = std::shared_ptr<XdevLAudioSourceAL>(new XdevLAudioSourceAL());
			alSourcei(tmp->getID(), AL_BUFFER,  buffer->getID());

			return tmp;
		}

		void XdevLAudioPlaybackAL::setGain(xdl_float gain) {
			alListenerf(AL_GAIN, (ALfloat)gain);
			xdl_int ret = AL_NO_ERROR;
			if((ret = alGetError()) != AL_NO_ERROR) {
				XDEVL_MODULE_WARNING(getALErrorString(ret) << "\n");
			}
		}

//
// XdevLAudioALRecord
//

		XdevLAudioALRecord::XdevLAudioALRecord(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
			: XdevLAudioBase<XdevLAudioCapture>(parameter, descriptor)
			, m_recordDevice(nullptr)
			, m_recordingStarted(xdl_false) {}

		xdl_int XdevLAudioALRecord::init() {

			if(alcIsExtensionPresent(nullptr, "AL_EXT_capture") == AL_TRUE) {
				XDEVL_MODULE_ERROR(getALCErrorString(alGetError()) << std::endl);
				return RET_FAILED;
			}

			std::cout << alcGetString(nullptr, ALC_CAPTURE_DEVICE_SPECIFIER) << std::endl;

			return RET_SUCCESS;
		}


		xdl_int XdevLAudioALRecord::shutdown() {
			if(alcCaptureCloseDevice(m_recordDevice) == AL_FALSE) {
				XDEVL_MODULE_ERROR(getALCErrorString(alGetError()) << std::endl);
				return RET_FAILED;
			}
			return RET_SUCCESS;
		}


		xdl_int XdevLAudioALRecord::device(const XdevLString& deviceName, XdevLAudioBuffer* audioBuffer, xdl_int samples) {
			m_bufferSize = samples;

			if(deviceName.toString() == "default") {
				m_recordDevice = alcCaptureOpenDevice(nullptr, audioBuffer->getSamplingRate(), audioBuffer->getFormat(), samples);
			} else {
				m_recordDevice = alcCaptureOpenDevice(deviceName.toString().c_str(), m_sampleRate, m_audioFormat, m_bufferSize);
			}
			if(m_recordDevice == nullptr) {
				XDEVL_MODULE_ERROR(getALCErrorString(alGetError()) << std::endl);
				return RET_FAILED;
			}

			return RET_SUCCESS;
		}

		xdl_int XdevLAudioALRecord::device(const XdevLString& deviceName, XdevLAudioBufferFormat format,  XdevLAudioSamplingRate samplerate, xdl_uint channels, xdl_int samples) {

			m_audioFormat = wrapAudioBufferFormat(format, channels);

			switch(format) {
				case AUDIO_BUFFER_FORMAT_S8:
					m_bufferSize = 1 * channels;
					break;
				case AUDIO_BUFFER_FORMAT_S16:
					m_bufferSize = 2 * channels;
					break;
				default:
					break;
			}

			m_sampleSize = m_bufferSize;

			m_sampleRate = AUDIO_SAMPLE_RATE_11025;
			switch(samplerate) {
				case AUDIO_SAMPLE_RATE_8000:
					m_sampleRate = AUDIO_SAMPLE_RATE_8000;
					break;
				case AUDIO_SAMPLE_RATE_11025:
					m_sampleRate = AUDIO_SAMPLE_RATE_11025;
					break;
				case AUDIO_SAMPLE_RATE_22050:
					m_sampleRate = AUDIO_SAMPLE_RATE_22050;
					break;
				case AUDIO_SAMPLE_RATE_44100:
					m_sampleRate = AUDIO_SAMPLE_RATE_44100;
					break;
				case AUDIO_SAMPLE_RATE_48000:
					m_sampleRate = AUDIO_SAMPLE_RATE_48000;
					break;
				case AUDIO_SAMPLE_RATE_96000:
					m_sampleRate = AUDIO_SAMPLE_RATE_96000;
					break;
				default:
					break;
			}

			m_bufferSize *=  samples;
			if(deviceName.toString() == "default") {
				m_recordDevice = alcCaptureOpenDevice(nullptr, m_sampleRate, m_audioFormat, m_bufferSize);
			} else {
				m_recordDevice = alcCaptureOpenDevice(deviceName.toString().c_str(), m_sampleRate, m_audioFormat, m_bufferSize);
			}
			if(m_recordDevice == nullptr) {
				XDEVL_MODULE_ERROR(getALCErrorString(alGetError()) << std::endl);
				return RET_FAILED;
			}

			return RET_SUCCESS;
		}

		void XdevLAudioALRecord::start() {
			alcCaptureStart(m_recordDevice);

			ALenum error = alGetError();
			if(error != AL_NO_ERROR) {
				std::cerr << getALErrorString(error) << std::endl;
			}

			m_recordingStarted = xdl_true;
		}

		void XdevLAudioALRecord::stop() {
			alcCaptureStop(m_recordDevice);

			ALenum error = alGetError();
			if(error != AL_NO_ERROR) {
				std::cerr << getALErrorString(error) << std::endl;
			}

			m_recordingStarted = xdl_false;
		}

		xdl_int XdevLAudioALRecord::capture(XdevLAudioBuffer* audioBuffer) {
			XDEVL_ASSERT(m_recordingStarted == true, "You have to use start() first");

			static std::vector<xdl_int8> buffer;
			if(buffer.size() < audioBuffer->getSize()) {
				buffer.reserve(audioBuffer->getSize());
				buffer.resize(audioBuffer->getSize());
			}

			ALint tmp = 0;
			ALuint capturedSamples = 0;
			while(capturedSamples < audioBuffer->getSize()) {
				alcGetIntegerv(m_recordDevice, ALC_CAPTURE_SAMPLES, (ALCsizei)sizeof(ALbyte), &tmp);
				alcCaptureSamples(m_recordDevice, (ALCvoid*)buffer.data(), m_bufferSize);

				capturedSamples += tmp;
				std::cout << capturedSamples << std::endl;
			}

			ALenum error = alGetError();
			if(error != AL_NO_ERROR) {
				std::cerr << getALErrorString(error) << std::endl;
			}

			audioBuffer->lock();
			audioBuffer->upload(buffer.data(), audioBuffer->getSize());
			audioBuffer->unlock();

			return RET_SUCCESS;
		}

		xdl_int XdevLAudioALRecord::capture(xdl_int8* buffer, xdl_int samples) {
			XDEVL_ASSERT(m_recordingStarted == true, "You have to use start() first");

			ALint tmp = 0;
			ALint capturedSamples = 0;
			while(tmp < samples) {
				alcGetIntegerv(m_recordDevice, ALC_CAPTURE_SAMPLES, (ALCsizei)sizeof(ALbyte), &tmp);
				capturedSamples += tmp;
			}
			alcCaptureSamples(m_recordDevice, (ALCvoid*)buffer, capturedSamples);
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioALRecord::sampleSize() {
			return m_sampleSize;
		}

		xdl_int XdevLAudioALRecord::read(xdl_uint8* src, xdl_int size) {
			return RET_SUCCESS;
		}
	}
}
