/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_AL_IMPL_H
#define XDEVL_AUDIO_AL_IMPL_H

#include <iostream>
#include <XdevLPluginImpl.h>
#include <XdevLUtils.h>
#if defined(__APPLE__)
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif
#include <vector>

#include "XdevLAudioBufferAL.h"
#include "XdevLAudioSourceAL.h"

namespace xdl {

	namespace audio {

		extern std::string getALErrorString(ALenum err);


		XdevLAudioBufferAL::XdevLAudioBufferAL(ALenum format, ALsizei freq, ALsizei size) :
			m_id(0),
			m_format(format),
			m_freq(freq),
			m_size(size),
			m_locked(xdl_false) {
			alGenBuffers(1, &m_id);
			m_buffer.reserve(size);
			m_buffer.resize(size);
		}

		XdevLAudioBufferAL::~XdevLAudioBufferAL() {
			alDeleteBuffers(1, &m_id);
		}

		xdl_uint XdevLAudioBufferAL::getSize() {
			xdl_int size = 0;
			alGetBufferi(m_id, AL_SIZE, &size);
			return (xdl_uint)size;
		}

		xdl_int XdevLAudioBufferAL::getFormat() {
			return m_format;
		}

		xdl_int XdevLAudioBufferAL::getSamplingRate() {
			xdl_int freq = 0;
			alGetBufferi(m_id, AL_FREQUENCY, &freq);
			return freq;
		}

		xdl_int XdevLAudioBufferAL::getChannels() {
			xdl_int chan = 0;
			alGetBufferi(m_id, AL_CHANNELS, &chan);
			return chan;
		}

		xdl_int XdevLAudioBufferAL::getBits() {
			xdl_int bits = 0;
			alGetBufferi(m_id, AL_BITS, &bits);
			return bits;
		}

		xdl_uint XdevLAudioBufferAL::getID() {
			return m_id;
		}

		xdl_uint8* XdevLAudioBufferAL::getData() {
			return m_buffer.data();
		}

		xdl_int XdevLAudioBufferAL::lock() {
			m_locked = xdl_true;
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioBufferAL::unlock() {
			m_locked = xdl_false;
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioBufferAL::upload(xdl_int8* src, xdl_uint size) {
			XDEVL_ASSERT((m_locked == xdl_true), "You must use lock() before you can upload data into the buffer");

			// Check if the specified buffer size is increasing the internal one.
			if(m_buffer.size() < size) {
				// Yes then increase buffer size.
				m_buffer.reserve(size);
				m_buffer.resize(size);
				memcpy((void*)m_buffer.data(), (void*)src, size);
			} else {
				// No, then just copy the data.
				memcpy((void*)m_buffer.data(), (void*)src, size);
			}

			// Now we have to upload the data to OpenAL
			alBufferData(m_id, m_format, (ALvoid*)m_buffer.data(), size, m_freq);
			ALenum error = alGetError();
			if(error != AL_NO_ERROR) {
				XDEVL_MODULEX_ERROR(XdevLAudioBufferAL, "alBufferData failed: " << getALErrorString(error) << std::endl);
				return RET_FAILED;
			}
			m_size = size;
			return RET_SUCCESS;
		}
	}
}

#endif
