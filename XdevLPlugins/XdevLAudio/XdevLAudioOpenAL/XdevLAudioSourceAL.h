/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_SOURCE_IMPL_H
#define XDEVL_AUDIO_SOURCE_IMPL_H

#include <XdevLAudio/XdevLAudioSource.h>

namespace xdl {

	namespace audio {

		/**
			@class XdevLAudioSourceAL
			@brief Source for audio data.
			@author Cengiz Terzibas
		*/
		class XdevLAudioSourceAL : public XdevLAudioSource {
				friend class XdevLAudioAL;
			public:
				XdevLAudioSourceAL();
				virtual ~XdevLAudioSourceAL();
				virtual void setAudioBuffer(XdevLAudioBuffer* buffer) override;
				virtual void setPitch(xdl_float pitch) override;
				virtual void setGain(xdl_float gain) override;
				virtual void setPlayPosSec(xdl_float pos) override;
				virtual void setPlayPosSample(xdl_int pos) override;
				virtual xdl_float getPlayPosSec() override;
				virtual xdl_int getPlayPosSample() override;
				virtual void setLoop(xdl_bool state) override;
				virtual xdl_bool isPlaying() override;
				virtual void play() override;
				virtual void stop() override;
				virtual void pause() override;
				virtual const xdl_uint& getID() const override;

			private:

				/// Holds the source identificator.
				ALuint				m_id;

				/// Holds the assigned XdevLAudioBuffer.
				XdevLAudioBuffer*	m_audioBuffer;
		};
	}
}

#endif
