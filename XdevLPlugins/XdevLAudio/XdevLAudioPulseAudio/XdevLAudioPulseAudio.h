/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_PULSE_AUDIO_H
#define XDEVL_AUDIO_PULSE_AUDIO_H

#include <XdevLThread.h>
#include <XdevLAudio/XdevLAudio.h>
#include <XdevLAudio/XdevLAudioBase.h>
#include <XdevLPluginImpl.h>

#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#include <pulse/error.h>

namespace xdl {

	namespace audio {

		static const XdevLString moduleDescription {
			"This is an empty module"
		};

		class XdevLAudioBufferPulseAudioBuffer : public XdevLAudioBuffer {
			public:
				XdevLAudioBufferPulseAudioBuffer(const XdevLAudioSpecs& audioSpecs);
				virtual ~XdevLAudioBufferPulseAudioBuffer() {}

				virtual xdl_int getFormat();
				virtual xdl_uint getSize() ;
				virtual xdl_int getSamplingRate();
				virtual xdl_int getChannels();
				virtual xdl_int getBits();
				virtual xdl_uint getID();
				virtual xdl_uint8* getData();
				virtual xdl_int lock();
				virtual xdl_int unlock();
				virtual xdl_int upload(xdl_int8* src, xdl_uint size);

				XdevLAudioSpecs m_audioSpecs;
				xdl_uint m_bufferFormatSize;
				xdl_uint m_bufferSizeInBytes;
				xdl_uint8* m_buffer;
		};

		class XdevLAudioSourcePulseAudio : public XdevLAudioSource {
				friend class XdevLAudio;
			public:
				virtual ~XdevLAudioSourcePulseAudio() {};
				void setAudioBuffer(XdevLAudioBuffer* buffer) override final;
				void setPitch(xdl_float pitch) override final;
				void setGain(xdl_float gain) override final;
				void setPlayPosSec(xdl_float pos) override final;
				void setPlayPosSample(xdl_int pos) override final;
				xdl_float getPlayPosSec() override final;
				xdl_int getPlayPosSample() override final;
				void setLoop(xdl_bool state) override final;
				xdl_bool isPlaying() override final;
				void play() override final;
				void stop() override final;
				void pause() override final;
				const xdl_uint& getID() const override final;
			private:
				xdl_uint m_id;
		};

		template<typename T>
		class XdevLAudioPulseAudioBase : public XdevLModuleImpl<T>, public thread::Thread {

			public:

				XdevLAudioPulseAudioBase(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

				virtual ~XdevLAudioPulseAudioBase();
				xdl_int init();
				xdl_int shutdown();
				xdl_int write(xdl_uint8* src, xdl_int size);
				xdl_int read(xdl_uint8* src, xdl_int size);

				xdl_uint getSamplingRate();
				xdl_uint getBufferSize();
				xdl_uint getNumberOfChannels();
				xdl_uint getFormatSizeInBytes();
				xdl_uint getPeriodSize();
				xdl_uint getPeriodTime();



				void setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData);
				IPXdevLAudioBuffer createAudioBufferFromFile(IPXdevLFile& file);
				IPXdevLAudioSource createAudioSource(IPXdevLAudioBuffer buffer);
				void setGain(xdl_float gain);
				xdl_int makeCurrent();
				xdl_int releaseCurrent();


				xdl_int update2();

				IPXdevLAudioBuffer create(XdevLAudioStreamType streamType, XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels);
				xdl_int initializeDevice();
				xdl_int RunThread(thread::ThreadArgument* arg) override final;

			private:

				XdevLString getDeviceName();

			private:

				pa_simple* m_pulseAudio;
				pa_threaded_mainloop* m_pulseAudioMainLoop;
				pa_context* m_pulseAudioContext;
				pa_channel_map m_pulseAudioChannelMap;
//				std::vector<pa_devices*> m_devices;

				xdl_int m_pulseAudioReady;
				xdl_uint m_samplingRate;
				xdl_uint m_bufferFormatSize;
				xdl_uint m_channels;
				xdl_uint m_samples;
				xdl_uint m_bufferSize;
				xdl_uint m_periodTime;

				XdevLAudioStreamType m_supportedStreamType;
				XdevLPlaybackCallbackFunctionType m_playbackCallbackFunction;
				xdl_float m_gain;
				void* m_userData;
				xdl_uint8* m_buffer;

				IPXdevLAudioAssetServer m_audioAssetServer;
				static void pulseAudioCallback(pa_context* context, void *userdata);
		};

		template<typename T>
		XdevLAudioPulseAudioBase<T>::XdevLAudioPulseAudioBase(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
			: XdevLModuleImpl<T>(parameter, descriptor)
			, m_pulseAudio(nullptr)
			, m_channels(1)
			, m_playbackCallbackFunction(nullptr)
			, m_buffer(nullptr) {
		}

		template<typename T>
		XdevLAudioPulseAudioBase<T>::~XdevLAudioPulseAudioBase() {}

		template<typename T>
		void XdevLAudioPulseAudioBase<T>::pulseAudioCallback(pa_context* context, void *userdata) {
			XdevLAudioPulseAudioBase<T>* audio = (XdevLAudioPulseAudioBase<T>*)(userdata);

			switch(pa_context_get_state(context)) {
				case PA_CONTEXT_CONNECTING: break;
				case PA_CONTEXT_AUTHORIZING: break;
				case PA_CONTEXT_SETTING_NAME: break;
				case PA_CONTEXT_UNCONNECTED: break;
				case PA_CONTEXT_TERMINATED:
				case PA_CONTEXT_FAILED:
					audio->m_pulseAudioReady = -1;
					break;

				case PA_CONTEXT_READY:
					audio->m_pulseAudioReady = 1;
					break;
			}
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::init() {

			// Create the main loop.
			m_pulseAudioMainLoop = pa_threaded_mainloop_new();
			if(nullptr == m_pulseAudioMainLoop) {
				XDEVL_MODULEX_ERROR(XdevLAudioPulseAudio, "Could not create main loop.\n")
				return RET_FAILED;
			}

			auto threadedMainLoop = pa_threaded_mainloop_get_api(m_pulseAudioMainLoop);
			auto properties = pa_proplist_new();

			m_pulseAudioContext = pa_context_new_with_proplist(threadedMainLoop, "XdevL", properties);
			if(nullptr == m_pulseAudioContext) {
				XDEVL_MODULEX_ERROR(XdevLAudioPulseAudio, "Could not create context.\n")
				return RET_FAILED;
			}

			m_pulseAudioReady = 0;
			pa_context_set_state_callback(m_pulseAudioContext, pulseAudioCallback, (void *)this);

			auto result = pa_context_connect(m_pulseAudioContext, nullptr, PA_CONTEXT_NOFLAGS, nullptr);
			if(result < 0) {
				return RET_FAILED;
			}

			if(m_pulseAudioReady < 0) {
				return RET_FAILED;
			}

			pa_threaded_mainloop_start(m_pulseAudioMainLoop);

			m_audioAssetServer = static_cast<IPXdevLAudioAssetServer>(this->getMediator()->createModule(XdevLModuleName("XdevLAudioAssetServer"), XdevLID("XdevLAudioALAudioAssetServer")));
			m_audioAssetServer->plug(XdevLPluginName("XdevLAudioImportPluginWAV"));

			return RET_SUCCESS;
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::initializeDevice() {
			pa_channel_map_init_stereo(&m_pulseAudioChannelMap);
			return RET_SUCCESS;
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::RunThread(thread::ThreadArgument* arg) {
			return 0;
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::shutdown() {
			if(m_pulseAudioContext) {
				pa_context_disconnect(m_pulseAudioContext);
				pa_context_unref(m_pulseAudioContext);
				m_pulseAudioContext = nullptr;
			}

			if(m_pulseAudioMainLoop) {
				pa_threaded_mainloop_free(m_pulseAudioMainLoop);
				m_pulseAudioMainLoop = nullptr;
			}

			if(nullptr != m_pulseAudio) {
				pa_simple_free(m_pulseAudio);
				m_pulseAudio = nullptr;
			}
			return RET_SUCCESS;
		}

		template<typename T>
		IPXdevLAudioBuffer XdevLAudioPulseAudioBase<T>::createAudioBufferFromFile(IPXdevLFile& file) {
			return m_audioAssetServer->import(file, this);
		}

		template<typename T>
		IPXdevLAudioBuffer XdevLAudioPulseAudioBase<T>::create(XdevLAudioStreamType streamType, XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels) {
			XdevLAudioSpecs audioSpecs {};
			audioSpecs.numberOfChannels = channels;
			audioSpecs.samplingRate = samplingRate;
			audioSpecs.format = format;
			std::shared_ptr<XdevLAudioBuffer> tmp = std::shared_ptr<XdevLAudioBufferPulseAudioBuffer>(new XdevLAudioBufferPulseAudioBuffer(audioSpecs));
			return tmp;
		}

		template<typename T>
		IPXdevLAudioSource XdevLAudioPulseAudioBase<T>::createAudioSource(IPXdevLAudioBuffer buffer) {
			std::shared_ptr<XdevLAudioSource> tmp = std::shared_ptr<XdevLAudioSourcePulseAudio>(new XdevLAudioSourcePulseAudio());
			return tmp;
		}

		template<typename T>
		XdevLString XdevLAudioPulseAudioBase<T>::getDeviceName() {
			return XdevLString("default");
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::write(xdl_uint8* src, xdl_int size) {
			return RET_SUCCESS;
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::read(xdl_uint8* src, xdl_int size) {
			return RET_SUCCESS;
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::update2() {
			return RET_SUCCESS;
		}

		template<typename T>
		xdl_uint XdevLAudioPulseAudioBase<T>::getSamplingRate() {
			return m_samplingRate;
		}

		template<typename T>
		xdl_uint XdevLAudioPulseAudioBase<T>::getBufferSize() {
			return m_bufferSize;
		}

		template<typename T>
		xdl_uint XdevLAudioPulseAudioBase<T>::getNumberOfChannels() {
			return m_channels;
		}

		template<typename T>
		xdl_uint XdevLAudioPulseAudioBase<T>::getFormatSizeInBytes() {
			return m_bufferFormatSize;
		}

		template<typename T>
		xdl_uint XdevLAudioPulseAudioBase<T>::getPeriodTime() {
			return m_periodTime;
		}

		template<typename T>
		xdl_uint XdevLAudioPulseAudioBase<T>::getPeriodSize() {
			return -1;
		}

		template<typename T>
		void XdevLAudioPulseAudioBase<T>::setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData) {
			m_playbackCallbackFunction = callbackFuntion;
			m_userData = userData;
		}

		template<typename T>
		void XdevLAudioPulseAudioBase<T>::setGain(xdl_float gain) {
			m_gain = gain;
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::makeCurrent() {
			return RET_SUCCESS;
		}

		template<typename T>
		xdl_int XdevLAudioPulseAudioBase<T>::releaseCurrent() {
			return RET_SUCCESS;
		}


		class XdevLAudioPlaybackPulseAudio : public XdevLAudioPulseAudioBase<XdevLAudioPlayback> {

			public:

				XdevLAudioPlaybackPulseAudio(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

				virtual ~XdevLAudioPlaybackPulseAudio();
				virtual xdl_int init() override;
				virtual xdl_int shutdown() override;
//			virtual xdl_int create(XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels, XdevLAudioBuffer** buffer) override;
				virtual xdl_int write(xdl_uint8* src, xdl_int size) override;

				virtual xdl_uint getSamplingRate();
				virtual xdl_uint getBufferSize() override;
				virtual xdl_uint getNumberOfChannels() override;
				virtual xdl_uint getFormatSizeInBytes() override;
				virtual xdl_uint getPeriodSize() override;
				virtual xdl_uint getPeriodTime() override;



				virtual void setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData) override;
				virtual IPXdevLAudioBuffer createAudioBufferFromFile(IPXdevLFile& file) override;
				virtual IPXdevLAudioBuffer createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) override;
				virtual IPXdevLAudioSource createAudioSource(IPXdevLAudioBuffer buffer) override;
				virtual void setGain(xdl_float gain) override;
				virtual xdl_int makeCurrent() override;
				virtual xdl_int releaseCurrent() override;


				virtual xdl_int update2();
		};



		class XdevLAudioCapturePulseAudio : public XdevLAudioPulseAudioBase<XdevLAudioCapture> {

			public:

				XdevLAudioCapturePulseAudio(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

				virtual ~XdevLAudioCapturePulseAudio();
				virtual xdl_int init() override;
				virtual xdl_int shutdown() override;
				//		virtual xdl_int create(XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels, XdevLAudioBuffer** buffer) override;
				virtual xdl_int read(xdl_uint8* src, xdl_int size) override;

				virtual xdl_uint getSamplingRate();
				virtual xdl_uint getBufferSize() override;
				virtual xdl_uint getNumberOfChannels() override;
				virtual xdl_uint getFormatSizeInBytes() override;
				virtual xdl_uint getPeriodSize() override;
				virtual xdl_uint getPeriodTime() override;



				virtual void setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData) override;
				virtual IPXdevLAudioBuffer createAudioBufferFromFile(IPXdevLFile& file) override;
				virtual IPXdevLAudioBuffer createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) override;
				virtual IPXdevLAudioSource createAudioSource(IPXdevLAudioBuffer buffer) override;
				virtual void setGain(xdl_float gain) override;
				virtual xdl_int makeCurrent() override;
				virtual xdl_int releaseCurrent() override;


				virtual xdl_int update2();
		};
	}
}
#endif
