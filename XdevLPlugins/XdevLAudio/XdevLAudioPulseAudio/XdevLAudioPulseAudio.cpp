/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLAudioPulseAudio.h"

namespace xdl {

	namespace audio {

		XdevLAudioBufferPulseAudioBuffer::XdevLAudioBufferPulseAudioBuffer(const XdevLAudioSpecs& audioSpecs) :
			m_audioSpecs(audioSpecs) {
		}

		xdl_int XdevLAudioBufferPulseAudioBuffer::getFormat() {
			return m_audioSpecs.format;
		}

		xdl_uint XdevLAudioBufferPulseAudioBuffer::getSize() {
			return m_bufferSizeInBytes;
		}

		xdl_int XdevLAudioBufferPulseAudioBuffer::getSamplingRate() {
			return m_audioSpecs.samplingRate;
		}

		xdl_int XdevLAudioBufferPulseAudioBuffer::getChannels() {
			return m_audioSpecs.numberOfChannels;
		}

		xdl_int XdevLAudioBufferPulseAudioBuffer::getBits() {
			return m_bufferFormatSize * 8;
		}

		xdl_uint XdevLAudioBufferPulseAudioBuffer::getID() {
			return 0;
		}

		xdl_uint8* XdevLAudioBufferPulseAudioBuffer::getData() {
			return nullptr;
		}

		xdl_int XdevLAudioBufferPulseAudioBuffer::lock() {
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioBufferPulseAudioBuffer::unlock() {
			return RET_SUCCESS;
		}

		xdl_int XdevLAudioBufferPulseAudioBuffer::upload(xdl_int8* src, xdl_uint size) {
			return RET_SUCCESS;
		}




		void XdevLAudioSourcePulseAudio::setAudioBuffer(XdevLAudioBuffer* buffer) {}
		void XdevLAudioSourcePulseAudio::setPitch(xdl_float pitch) {}
		void XdevLAudioSourcePulseAudio::setGain(xdl_float gain) {}
		void XdevLAudioSourcePulseAudio::setPlayPosSec(xdl_float pos) {}
		void XdevLAudioSourcePulseAudio::setPlayPosSample(xdl_int pos) {}
		xdl_float XdevLAudioSourcePulseAudio::getPlayPosSec() {return 0.0f;}
		xdl_int XdevLAudioSourcePulseAudio::getPlayPosSample() {return 0;}
		void XdevLAudioSourcePulseAudio::setLoop(xdl_bool state) {}
		xdl_bool XdevLAudioSourcePulseAudio::isPlaying() {return false;}
		void XdevLAudioSourcePulseAudio::play() {}
		void XdevLAudioSourcePulseAudio::stop() {}
		void XdevLAudioSourcePulseAudio::pause() {}
		const xdl_uint& XdevLAudioSourcePulseAudio::getID() const {return m_id;}







		//
		// Playback
		//


		XdevLAudioPlaybackPulseAudio::XdevLAudioPlaybackPulseAudio(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
			XdevLAudioPulseAudioBase(parameter, descriptor) {
		}

		XdevLAudioPlaybackPulseAudio::~XdevLAudioPlaybackPulseAudio() {}

		xdl_int XdevLAudioPlaybackPulseAudio::init() {
			return XdevLAudioPulseAudioBase::init();
		}

		xdl_int XdevLAudioPlaybackPulseAudio::shutdown() {
			return XdevLAudioPulseAudioBase::shutdown();
		}

		IPXdevLAudioBuffer XdevLAudioPlaybackPulseAudio::createAudioBufferFromFile(IPXdevLFile& file) {
			return XdevLAudioPulseAudioBase::createAudioBufferFromFile(file);
		}

//	xdl_int XdevLAudioPlaybackPulseAudio::create(XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels, XdevLAudioBuffer** buffer) {
//		return XdevLAudioPulseAudioBase::create(AUDIO_STREAM_PLAYBACK, bufferFormat, samplingRate, channels, buffer);
//	}

		IPXdevLAudioBuffer XdevLAudioPlaybackPulseAudio::createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) {
			return XdevLAudioPulseAudioBase::create(AUDIO_STREAM_PLAYBACK, format, samplingRate, channels);
		}

		IPXdevLAudioSource XdevLAudioPlaybackPulseAudio::createAudioSource(IPXdevLAudioBuffer buffer) {
			return XdevLAudioPulseAudioBase::createAudioSource(buffer);
		}

		xdl_int XdevLAudioPlaybackPulseAudio::write(xdl_uint8* src, xdl_int size) {
			return XdevLAudioPulseAudioBase::write(src, size);
		}

		xdl_int XdevLAudioPlaybackPulseAudio::update2() {
			return XdevLAudioPulseAudioBase::update2();
		}

		xdl_uint XdevLAudioPlaybackPulseAudio::getSamplingRate() {
			return XdevLAudioPulseAudioBase::getSamplingRate();
		}

		xdl_uint XdevLAudioPlaybackPulseAudio::getBufferSize() {
			return XdevLAudioPulseAudioBase::getBufferSize();
		}

		xdl_uint XdevLAudioPlaybackPulseAudio::getNumberOfChannels() {
			return XdevLAudioPulseAudioBase::getNumberOfChannels();
		}
		xdl_uint XdevLAudioPlaybackPulseAudio::getFormatSizeInBytes() {
			return XdevLAudioPulseAudioBase::getFormatSizeInBytes();
		}
		xdl_uint XdevLAudioPlaybackPulseAudio::getPeriodTime() {
			return XdevLAudioPulseAudioBase::getPeriodTime();
		}

		xdl_uint XdevLAudioPlaybackPulseAudio::getPeriodSize() {
			return XdevLAudioPulseAudioBase::getPeriodSize();
		}

		void XdevLAudioPlaybackPulseAudio::setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData) {
			XdevLAudioPulseAudioBase::setPlaybackCallbackFunction(callbackFuntion, userData);
		}

		void XdevLAudioPlaybackPulseAudio::setGain(xdl_float gain) {
			return XdevLAudioPulseAudioBase::setGain(gain);
		}
		xdl_int XdevLAudioPlaybackPulseAudio::makeCurrent() {
			return XdevLAudioPulseAudioBase::makeCurrent();
		}
		xdl_int XdevLAudioPlaybackPulseAudio::releaseCurrent() {
			return XdevLAudioPulseAudioBase::releaseCurrent();
		}



		//
		// Capture
		//

		XdevLAudioCapturePulseAudio::XdevLAudioCapturePulseAudio(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
			XdevLAudioPulseAudioBase<XdevLAudioCapture>(parameter, descriptor) {
		}

		XdevLAudioCapturePulseAudio::~XdevLAudioCapturePulseAudio() {}

		xdl_int XdevLAudioCapturePulseAudio::init() {
			return XdevLAudioPulseAudioBase::init();
		}

		xdl_int XdevLAudioCapturePulseAudio::shutdown() {
			return XdevLAudioPulseAudioBase::shutdown();
		}

		IPXdevLAudioBuffer XdevLAudioCapturePulseAudio::createAudioBufferFromFile(IPXdevLFile& file) {
			return XdevLAudioPulseAudioBase::createAudioBufferFromFile(file);
		}

//	xdl_int XdevLAudioCapturePulseAudio::create(XdevLAudioBufferFormat bufferFormat, XdevLAudioSamplingRate samplingRate, xdl_uint channels, XdevLAudioBuffer** buffer) {
//		return XdevLAudioPulseAudioBase::create(AUDIO_STREAM_CAPTURE, bufferFormat, samplingRate, channels, buffer);
//	}

		IPXdevLAudioBuffer XdevLAudioCapturePulseAudio::createAudioBuffer(XdevLAudioBufferFormat format, XdevLAudioSamplingRate samplingRate, xdl_uint channels, xdl_int size, void* data) {
			return XdevLAudioPulseAudioBase::create(AUDIO_STREAM_CAPTURE, format, samplingRate, channels);
		}

		IPXdevLAudioSource XdevLAudioCapturePulseAudio::createAudioSource(IPXdevLAudioBuffer buffer) {
			return nullptr;
		}

		xdl_int XdevLAudioCapturePulseAudio::read(xdl_uint8* src, xdl_int size) {
			return XdevLAudioPulseAudioBase::read(src, size);
		}

		xdl_int XdevLAudioCapturePulseAudio::update2() {
			return XdevLAudioPulseAudioBase::update2();
		}

		xdl_uint XdevLAudioCapturePulseAudio::getSamplingRate() {
			return XdevLAudioPulseAudioBase::getSamplingRate();
		}

		xdl_uint XdevLAudioCapturePulseAudio::getBufferSize() {
			return XdevLAudioPulseAudioBase::getBufferSize();
		}

		xdl_uint XdevLAudioCapturePulseAudio::getNumberOfChannels() {
			return XdevLAudioPulseAudioBase::getNumberOfChannels();
		}
		xdl_uint XdevLAudioCapturePulseAudio::getFormatSizeInBytes() {
			return XdevLAudioPulseAudioBase::getFormatSizeInBytes();
		}
		xdl_uint XdevLAudioCapturePulseAudio::getPeriodTime() {
			return XdevLAudioPulseAudioBase::getPeriodTime();
		}

		xdl_uint XdevLAudioCapturePulseAudio::getPeriodSize() {
			return XdevLAudioPulseAudioBase::getPeriodSize();
		}

		void XdevLAudioCapturePulseAudio::setPlaybackCallbackFunction(XdevLPlaybackCallbackFunctionType callbackFuntion, void* userData) {
			XdevLAudioPulseAudioBase::setPlaybackCallbackFunction(callbackFuntion, userData);
		}

		void XdevLAudioCapturePulseAudio::setGain(xdl_float gain) {
			return XdevLAudioPulseAudioBase::setGain(gain);
		}
		xdl_int XdevLAudioCapturePulseAudio::makeCurrent() {
			return XdevLAudioPulseAudioBase::makeCurrent();
		}
		xdl_int XdevLAudioCapturePulseAudio::releaseCurrent() {
			return XdevLAudioPulseAudioBase::releaseCurrent();
		}
	}
}
