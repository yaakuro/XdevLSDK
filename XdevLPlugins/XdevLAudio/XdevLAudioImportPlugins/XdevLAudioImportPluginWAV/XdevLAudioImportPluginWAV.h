/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_IMPORT_PLUGIN_WAV_H
#define XDEVL_AUDIO_IMPORT_PLUGIN_WAV_H

#include <XdevLAudio/XdevLAudio.h>

namespace xdl {
	namespace audio {


#if XDEVL_PLATFORM_WINDOWS
	#undef WAVE_FORMAT_PCM
#endif

#define WAVE_FORMAT_PCM 0x0001
#define WAVE_FORMAT_IEEE_FLOAT 0x0003
#define WAVE_FORMAT_ALAW 0x0006
#define WAVE_FORMAT_MULAW 0x0007
#define WAVE_FORMAT_EXTENSIBLE 0xFFFE

		struct RIFFHeader {
			xdl_int8 chunkID[4];
			xdl_int32 chunkSize;
		};

		struct RIFFType {
			xdl_int8 ID[4];
		};

		struct WAVEFormat {
			xdl_int16 audioFormat;
			xdl_int16 numChannels;
			xdl_int32 sampleRate;
			xdl_int32 byteRate;
			xdl_int16 blockAlign;
			xdl_int16 bitsPerSample;
		};

		struct WAVEBEXT {
			xdl_uint8 Description[256];
			xdl_uint8 Originator[32];
			xdl_uint8 OriginatorReference[32];
			xdl_uint8 OriginationDate[10];
			xdl_uint8 OriginationTime[8];
			xdl_uint64 TimeReference;
			xdl_uint32 Version;
			xdl_uint8 UMID[64];
			xdl_int16 LoudnessValue;
			xdl_int16 LoudnessRange;
			xdl_int16 MaxTruePeakLevel;
			xdl_int16 MaxMomentaryLoudness;
			xdl_int16 MaxShortTermLoudness;
			xdl_uint8 Reserved[180];
		};

		class XdevLAudioImportPluginWAV : public XdevLAudioImportPlugin {
			public:
				XdevLAudioImportPluginWAV();
				virtual ~XdevLAudioImportPluginWAV();
				IPXdevLAudioBuffer import(IPXdevLFile& file, IPXdevLAudio audio) override final;
				XdevLString getExtension() override final;
		};
	}
}
#endif
