#include "XdevLAudioImportPluginWAV.h"

namespace xdl {

	namespace audio {

		extern "C" XDEVL_EXPORT void _createAudioPlugin(XdevLAudioImportPluginCreateParameter& parameter) {
			parameter.pluginInstance = std::make_shared<XdevLAudioImportPluginWAV>();
		}


		xdl_bool isChunckID(const RIFFHeader& header, const xdl_char a,  const xdl_char b,  const xdl_char c,  const xdl_char d) {
			if(header.chunkID[0] != a || header.chunkID[1] != b || header.chunkID[2] != c || header.chunkID[3] != d) {
				return xdl_false;
			}
			return xdl_true;;
		}

		xdl_bool isRIFFID(const RIFFType& type, const xdl_char a,  const xdl_char b,  const xdl_char c,  const xdl_char d) {
			if(type.ID[0] != a || type.ID[1] != b || type.ID[2] != c || type.ID[3] != d) {
				return xdl_false;
			}
			return xdl_true;;
		}


		XdevLAudioImportPluginWAV::XdevLAudioImportPluginWAV() {

		}

		XdevLAudioImportPluginWAV::~XdevLAudioImportPluginWAV() {

		}

		IPXdevLAudioBuffer XdevLAudioImportPluginWAV::import(IPXdevLFile& file, IPXdevLAudio audio) {
			XdevLAudioBufferFormat audioFormat = AUDIO_BUFFER_FORMAT_UNKNOWN;
			xdl_uint numberOfChannels = 1;
			xdl_int samplingRate = 0;
			std::vector<xdl_uint8> rawData;

			RIFFHeader riff_header {};

			//
			// Read master RIFF chunk.
			//
			file->read((xdl_uint8*)&riff_header, sizeof(RIFFHeader));
			if(isChunckID(riff_header, 'R', 'I', 'F', 'F')) {

				//
				// Read the type of the RIFF.
				//
				RIFFType riff_type {};
				file->read((xdl_uint8*)&riff_type, sizeof(RIFFType));

				if(isRIFFID(riff_type, 'W', 'A', 'V', 'E')) {

					do {

						//
						// Read chunck header.
						//
						RIFFHeader waveFormatHeader {};
						file->read((xdl_uint8*)&waveFormatHeader, sizeof(RIFFHeader));

						if(isChunckID(waveFormatHeader, 'b', 'e', 'x', 't')) {
							WAVEBEXT waveBEXT {};
							file->read((xdl_uint8*)&waveBEXT, sizeof(WAVEBEXT));
							file->seek(XdevLSeekCurr(), waveFormatHeader.chunkSize - sizeof(WAVEBEXT));
						} else if(isChunckID(waveFormatHeader, 'i', 'X', 'M', 'L')) {
							file->seek(XdevLSeekCurr(), waveFormatHeader.chunkSize);
						}
						//
						// Check which chunck type it is.
						//
						else if(isChunckID(waveFormatHeader, 'f', 'm', 't', ' ')) {

							WAVEFormat waveFormat {};
							file->read((xdl_uint8*)&waveFormat, sizeof(WAVEFormat));
							//
							// Check if this is PCM format.
							//
							if(!(waveFormat.audioFormat & WAVE_FORMAT_PCM)) {
								XDEVL_MODULEX_ERROR(XdevLAudioImportPluginWAV, "The specified audio file is not a PCM.\n");
								return nullptr;
							}
							samplingRate = waveFormat.sampleRate;
							numberOfChannels = waveFormat.numChannels;

							if(waveFormat.bitsPerSample == 8) {
								audioFormat = AUDIO_BUFFER_FORMAT_S8;
							} else if(waveFormat.bitsPerSample == 16) {
								audioFormat = AUDIO_BUFFER_FORMAT_S16;
							}

						}
						//
						// Handle the data chunck.
						//
						else if(isChunckID(waveFormatHeader, 'd', 'a', 't', 'a')) {

							//
							// Read the raw data here.
							//
							rawData.reserve(waveFormatHeader.chunkSize);
							rawData.resize(waveFormatHeader.chunkSize);
							file->read((xdl_uint8*)rawData.data(), waveFormatHeader.chunkSize);

						} else {
							// TODO Do the info stuff
							file->seek(XdevLSeekCurr(), waveFormatHeader.chunkSize);
						}

						// Stop if we have a problem.
						// TODO better solution.
						auto pos = file->tell();
						if(pos == -1 || (file->length() == pos)) {
							break;
						}

					} while(true);

				} else {
					XDEVL_MODULEX_ERROR(XdevLAudioImportPluginWAV, "Wrong RIFF Type ID. Sould be: 'WAVE'.\n");
					return nullptr;
				}

			} else {
				XDEVL_MODULEX_ERROR(XdevLAudioImportPluginWAV, "Wrong RIFF HEADER.\n");
				return nullptr;
			}

			file->flush();

			auto audioBuffer = audio->createAudioBuffer(audioFormat, (XdevLAudioSamplingRate)samplingRate, numberOfChannels, rawData.size(), rawData.data());
			return audioBuffer;
		}

		XdevLString XdevLAudioImportPluginWAV::getExtension() {
			return XdevLString("wav");
		}
	}
}
