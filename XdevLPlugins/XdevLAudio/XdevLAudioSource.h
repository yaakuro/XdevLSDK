/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_AUDIO_SOURCE_H
#define XDEVL_AUDIO_SOURCE_H

#include <XdevLTypes.h>

namespace xdl {

	namespace audio {

		/**
			@class XdevLAudioSource
			@brief Audio source in space.
			@author Cengiz Terzibas

			An audio source means a source that creates sound. In real life this could be everything.
			For instance a telephone, car, plane, steps of a human etc. A source can have
			a direction, position and velocity.
		*/
		class XdevLAudioSource {
				friend class XdevLAudio;
			public:
				virtual ~XdevLAudioSource() {};

				/// Sets the audio buffer for this source.
				/**
				 * @param buffer A valid audio buffer.
				 */
				virtual void setAudioBuffer(XdevLAudioBuffer* buffer) = 0;

				/// Sets the pitch of the source.
				/**
					@param pitch Changes the frequence of the source.
				*/
				virtual void setPitch(xdl_float pitch) = 0;

				/// Sets the gain of the source.
				/**
					@param gain Controlls the gain (volume) of the source.
				*/
				virtual void setGain(xdl_float gain) = 0;

				/// Sets the play position.
				/**
					@param pos Sets the play position. This value uses second units.
				*/
				virtual void setPlayPosSec(xdl_float pos) = 0;

				/// Sets the play position.
				/**
					@param pos Sets the play position. This value uses sample units.
				*/
				virtual void setPlayPosSample(xdl_int pos) = 0;

				/// Returns the play position.
				/**
					@return Returns the current play position in second units.
				*/
				virtual xdl_float getPlayPosSec() = 0;

				/// Returns the play position.
				/**
					@return Returns the current play position in sample units.
				*/
				virtual xdl_int getPlayPosSample() = 0;

				/// Sets the loop state of the source.
				/**
					@param state Set state = false if the source shouldn't
					be looped. Default value for the loop state of the
					source is true.
				*/
				virtual void setLoop(xdl_bool state) = 0;

				/// Returns the playing state of the source.
				/**
					@return Returns true if the source is playing
					at the moment. Otherwise it returns false.
				*/
				virtual xdl_bool isPlaying() = 0;

				/// Starts to play a source.
				virtual void play() = 0;

				/// Stops a playing source.
				virtual void stop() = 0;

				/// Pauses a playing source.
				virtual void pause() = 0;

				/// Returns the identification code.
				virtual const xdl_uint& getID() const = 0;
		};

		using IXdevLAudioSource = XdevLAudioSource;
		using IPXdevLAudioSource = std::shared_ptr<XdevLAudioSource>;
	}
}

#endif
