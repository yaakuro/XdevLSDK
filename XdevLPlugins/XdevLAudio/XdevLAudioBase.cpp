/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevLModule.h>
#include <XdevLCoreMediator.h>
#include <XdevLPlatform.h>
#include <XdevLAudio/XdevLAudioBase.h>

#include <iostream>
#include <string>
#include <fstream>


namespace xdl {

	namespace audio {

		XdevLAudioAssetServerImpl::XdevLAudioAssetServerImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
			XdevLModuleImpl<XdevLAudioAssetServer>(parameter, descriptor) {
		}

		XdevLAudioAssetServerImpl::~XdevLAudioAssetServerImpl() {
		}

		xdl_int XdevLAudioAssetServerImpl::plug(const XdevLPluginName& pluginName, const XdevLVersion& version) {

			XdevLSharedLibrary* modulesSharedLibrary = nullptr;
			try {
				modulesSharedLibrary = new XdevLSharedLibrary();
			} catch(std::bad_alloc& e) {
				XDEVL_MODULE_ERROR(e.what());
				return RET_FAILED;
			}

			XdevLFileName tmp = getMediator()->getCoreParameter().pluginsPath;
#ifdef XDEVL_PLATFORM_ANDROID
			tmp += XdevLFileName("lib");
#endif
			tmp += XdevLFileName(pluginName);
			tmp += XdevLFileName("-");
			tmp += version.toString();
#ifdef XDEVL_DEBUG
			tmp += XdevLFileName("d");
#endif
			tmp += XdevLFileName(".xap");

			if(modulesSharedLibrary->open(tmp.toString().c_str(), xdl_false) == RET_FAILED) {
				delete modulesSharedLibrary;
				XDEVL_MODULE_ERROR("Could not open plugin: '" << tmp << "'.\n");
				return RET_FAILED;
			}

			XdevLCreateAudioImportPluginFunction createAudioImportPlugin = (XdevLCreateAudioImportPluginFunction)(modulesSharedLibrary->getFunctionAddress("_createAudioPlugin"));

			XdevLAudioImportPluginCreateParameter audioImportPluginCreateParameter {};
			createAudioImportPlugin(audioImportPluginCreateParameter);

			XdevLID id(audioImportPluginCreateParameter.pluginInstance->getExtension());

			auto found = m_pluginMap.find(id.getHashCode());
			if(m_pluginMap.end() != found) {
				return RET_FAILED;
			}

			XdevLAudioImportPluginInfo audioImportPluginInfo {};
			audioImportPluginInfo.id = id;
			audioImportPluginInfo.audioImportPlugin = audioImportPluginCreateParameter.pluginInstance;

			m_pluginMap[id.getHashCode()] = audioImportPluginInfo;

			return RET_SUCCESS;
		}

		IPXdevLAudioImportPlugin XdevLAudioAssetServerImpl::getImporter(const XdevLString& extension) {
			XdevLID id(extension);
			auto found = m_pluginMap.find(id.getHashCode());
			if(m_pluginMap.end() == found) {
				return nullptr;
			}
			return found->second.audioImportPlugin;
		}

		IPXdevLAudioBuffer XdevLAudioAssetServerImpl::import(IPXdevLFile& file, IPXdevLAudio audio) {
			auto importer = getImporter(file->getFileName().getExtension());
			if(nullptr != importer) {
				return importer->import(file, audio);
			}
			return nullptr;
		}
	}
}
