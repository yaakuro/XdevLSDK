/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BLUETOOTH_LINUX_IMPL_H
#define XDEVL_BLUETOOTH_LINUX_IMPL_H

#include <XdevLBluetooth.h>
#include <XdevLPluginImpl.h>
#include <XdevLUtils.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>
#include <bluetooth/l2cap.h>

namespace xdl {

	static const std::vector<XdevLModuleName>	moduleNames {
		XdevLModuleName("XdevLBluetooth")
	};

	class XdevLBluetoothAddressImpl : public XdevLBluetoothAddress {
		public:
			XdevLBluetoothAddressImpl(xdl::xdl_int socket, sockaddr_rc addr) : m_socket(socket), m_addr(addr) {}
			virtual~ XdevLBluetoothAddressImpl() {}
			sockaddr_rc get() {
				return m_addr;
			}
		protected:
			xdl::xdl_int 	m_socket;
			sockaddr_rc 	m_addr;
	};

	/**
		@class XdevLBluetoothLinuxImpl
		@brief Core class to support Bluetooth.
		@author Cengiz Terzibas
	*/

	class XdevLBluetoothLinuxImpl : public XdevLModuleImpl<XdevLBluetooth> {
		public:
			XdevLBluetoothLinuxImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLBluetoothLinuxImpl() {}

			xdl_int init() override final;
			xdl_int shutdown() override final;

			xdl_int notify(XdevLEvent& event) override final;

			xdl_int open() override final;
			xdl_int open(const XdevLFileName& host) override final;
			xdl_int close() override final;
			xdl_int bind(const XdevLString& host = XdevLString()) override final;
			xdl_int write(xdl_uint8* src, xdl_int size) override final;
			xdl_int read(xdl_uint8* dst, xdl_int size) override final;
			xdl_int flush() override final;
			xdl_int listen(xdl::xdl_int backlog) override final;
			xdl_int accept(XdevLBluetoothAddress** addr) override final;

		protected:

			xdl_int scan(const xdl_char* device);
			xdl_int readInfoFromXMLFile();

		private:

			xdl::xdl_int m_socket;
			std::vector<XdevLBluetoothAddressImpl*> m_acceptedSockets;
			XdevLFileName m_scanedDevices;
			XdevLFileName m_deviceName;
	};

}

#endif
