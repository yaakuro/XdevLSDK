/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BLUETOOTH_MACOSXIMPL_H
#define XDEVL_BLUETOOTH_MACOSXIMPL_H

#include <XdevLBluetooth.h>
#include <XdevLPluginImpl.h>
#include <XdevLUtils.h>
#include <IOBluetooth/IOBluetooth.h>
#include <IOBluetooth/IOBluetoothUserLib.h>
#include <IOBluetooth/IOBluetoothUtilities.h>
#include <IOBluetoothUI/IOBluetoothUI.h>
#include <IOBluetoothUI/IOBluetoothUIUserLib.h>

namespace xdl {


	static const std::vector<XdevLModuleName>	moduleNames	{
		XdevLModuleName("XdevLBluetooth")
	};

	class XdevLBluetoothAddressImpl : public XdevLBluetoothAddress {
		public:
			XdevLBluetoothAddressImpl() {}
			virtual~ XdevLBluetoothAddressImpl() {}

		protected:

	};

	/**
		@class XdevLBluetoothMacOSXImpl
		@brief Reference implementation for the MacOSX.
		@author Cengiz Terzibas
	*/

	class XdevLBluetoothMacOSXImpl : public XdevLModuleImpl<XdevLBluetooth> {
		public:
			XdevLBluetoothMacOSXImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLBluetoothMacOSXImpl() {}

			virtual xdl_int open();
			virtual xdl_int open(const XdevLFileName& host);
			virtual xdl_int close();
			virtual xdl_int flush();
			virtual xdl_int bind(const XdevLString& host = XdevLString());
			virtual xdl_int write(xdl_uint8* src, xdl_int size);
			virtual xdl_int read(xdl_uint8* dst, xdl_int size);
			virtual xdl_int listen(xdl::xdl_int backlog);
			virtual xdl_int accept(XdevLBluetoothAddress** addr);
		protected:
			virtual xdl_int init();
			virtual xdl_int shutdown();


			xdl_int readInfoFromXMLFile();
		private:
			std::vector<XdevLBluetoothAddressImpl*> m_acceptedSockets;
			XdevLFileName m_deviceName;

			IOBluetoothRFCOMMChannelRef	m_RFCOMMChannelRef;
	};

}

#endif
