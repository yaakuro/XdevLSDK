/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_BLUETOOTH_WINIMPL_H
#define XDEVL_BLUETOOTH_WINIMPL_H

#include <XdevLBluetooth.h>
#include <XdevLPluginImpl.h>
#include <XdevLUtils.h>
#include <WinBluetoothWrapper.h>

#include <WinSock2.h>

namespace xdl {


	static const std::vector<XdevLModuleName>	moduleNames	{
		XdevLModuleName("XdevLBluetooth")
	};

	class XdevLBluetoothAddressImpl : public XdevLBluetoothAddress {
		public:
			XdevLBluetoothAddressImpl(SOCKET socket, SOCKADDR_BTH addr) : m_socket(socket), m_addr(addr) {}
			virtual~ XdevLBluetoothAddressImpl() {}
			SOCKADDR_BTH get() {
				return m_addr;
			}
		protected:
			SOCKET 				m_socket;
			SOCKADDR_BTH 	m_addr;
	};



	/**
		@class XdevLBluetoothWinImpl
		@brief Core class to support Bluetooth.
		@author Cengiz Terzibas
	*/

	class XdevLBluetoothWinImpl : public XdevLModuleImpl<XdevLBluetooth> {
		public:
			XdevLBluetoothWinImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
				XdevLModuleImpl<XdevLBluetooth>(parameter, descriptor),
				m_handle(NULL),
				m_wVersionRequested(0x202) {};
			virtual ~XdevLBluetoothWinImpl() {}

			virtual xdl_int scan(const xdl_char* device);
			virtual xdl_int open();
			virtual xdl_int open(const XdevLFileName& name);
			virtual xdl_int open(const xdl_char* name);
			virtual xdl_int close();
			virtual xdl_int flush();
			virtual xdl_int bind(const XdevLString& host = XdevLString());
			virtual xdl_int write(xdl_uint8* src, xdl_int size);
			virtual xdl_int read(xdl_uint8* dst, xdl_int size);
			virtual xdl_int listen(xdl_int backlog);
			virtual xdl_int accept(XdevLBluetoothAddress** addr);
		protected:
			virtual xdl_int init();
			virtual xdl_int shutdown();



			xdl_int readInfoFromXMLFile();
		private:
			HANDLE						m_handle;
			WORD 							m_wVersionRequested;
			WSADATA 					m_wsaData;
			SOCKET 						m_socket;
			WSAPROTOCOL_INFO 	m_protocolInfo;
			SOCKADDR_BTH 			m_connection;
			std::vector<XdevLBluetoothAddressImpl*> m_acceptedSockets;

			std::string m_deviceName;
			xdl_int			m_port;
	};

}

#endif
