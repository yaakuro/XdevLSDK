/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLFileSystemWindows.h"
#include <algorithm>

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::XdevLFileSystemPluginName,
	xdl::XdevLFileSystemModuleName,
	XDEVL_FILESYSTEM_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MINOR_VERSION,
	XDEVL_FILESYSTEM_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleFileDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::XdevLFileSystemModuleName[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::XdevLDescriptionForFileSystem,
	XDEVL_FILESYSTEM_MODULE_FILE_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_FILE_MINOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_FILE_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDirectoryDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::XdevLFileSystemModuleName[1],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::XdevLDescriptionForDirectory,
	XDEVL_FILESYSTEM_MODULE_DIRECTORY_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_DIRECTORY_MINOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_DIRECTORY_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDirectoryWatcherDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::XdevLFileSystemModuleName[2],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::XdevLDescriptionForFileSystem,
	XDEVL_FILESYSTEM_MODULE_DIRECTORYWATCHER_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_DIRECTORYWATCHER_MINOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_DIRECTORYWATCHER_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleArchiveDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::XdevLFileSystemModuleName[3],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::XdevLDescriptionForFileSystem,
	XDEVL_FILESYSTEM_MODULE_ARCHIVE_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_ARCHIVE_MINOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_ARCHIVE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

extern "C" XDEVL_EXPORT xdl::XdevLModule* _createModule(const xdl::XdevLPluginDescriptor& pluginDescriptor, const xdl::XdevLModuleDescriptor& moduleDescriptor) {

	if(moduleDirectoryDescriptor.getName() == moduleDescriptor.getName()) {
		xdl::XdevLModule* obj = new xdl::XdevLDirectoryWindows(nullptr, moduleDirectoryDescriptor);
		if(!obj)
			return nullptr;

		return obj;

	} else if(moduleFileDescriptor.getName() == moduleDescriptor.getName()) {
		xdl::XdevLModule* obj = new xdl::XdevLFileSystemWindows(nullptr, moduleFileDescriptor);
		if(!obj) {
			return nullptr;
		}

		return obj;

	} else if(moduleDirectoryWatcherDescriptor.getName() == moduleDescriptor.getName()) {
		xdl::XdevLModule* obj = new xdl::XdevLDirectoryWatcherWindows(nullptr, moduleDirectoryWatcherDescriptor);
		if(!obj) {
			return nullptr;
		}

		return obj;

	} else if(moduleArchiveDescriptor.getName() == moduleDescriptor.getName()) {
		xdl::XdevLModule* obj = new xdl::XdevLArchivePhysFS(nullptr, moduleArchiveDescriptor);
		if(!obj) {
			return nullptr;
		}

		return obj;
	}

	return nullptr;
}


XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLFileSystemWindows, moduleFileDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLDirectoryWindows, moduleDirectoryDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLDirectoryWatcherWindows, moduleDirectoryWatcherDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLArchivePhysFS, moduleArchiveDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLFileSystem, xdl::XdevLFileSystemWindows, moduleFileDescriptor)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLDirectory, xdl::XdevLDirectoryWindows, moduleDirectoryDescriptor)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLDirectoryWatcher, xdl::XdevLDirectoryWatcherWindows, moduleDirectoryWatcherDescriptor)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLArchive, xdl::XdevLArchivePhysFS, moduleArchiveDescriptor)

XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DEFINITION_DEFAULT(XdevLFile)
XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DEFINITION_DEFAULT(XdevLDirectory)
XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DEFINITION_DEFAULT(XdevLDirectoryWatcher)
XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DEFINITION_DEFAULT(XdevLArchive)


namespace xdl {

	const char* local_dir = ".";
	CHAR errorMsgText[256];

	bool sort_name_dir_entitiy(const XdevLDirectoryEntity& i, const XdevLDirectoryEntity& j) {
		std::string si(i.getName());
		std::string sj(j.getName());
		return (si < sj);
	}


// usage
//     CHAR msgText[256];
//     getLastErrorText(msgText,sizeof(msgText));
	static CHAR *getLastErrorText(CHAR *pBuf, ULONG bufSize) {
		DWORD retSize;
		LPTSTR msg = nullptr;

		if(bufSize < 16) {
			if(bufSize > 0) {
				pBuf[0]='\0';
			}
			return(pBuf);
		}
		retSize=FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|
		                      FORMAT_MESSAGE_FROM_SYSTEM|
		                      FORMAT_MESSAGE_ARGUMENT_ARRAY,
		                      NULL,
		                      GetLastError(),
		                      LANG_NEUTRAL,
		                      (LPTSTR)&msg,
		                      0,
		                      NULL);
		if(!retSize || msg == NULL) {
			pBuf[0]='\0';
		} else {
			msg[strlen(msg)-2]='\0'; //remove cr and newline character
			sprintf(pBuf,"ERROR:%s\n",  pBuf);
			LocalFree((HLOCAL)msg);
		}
		return(pBuf);
	}

	XdevLDirectoryEntity::Type extractType(struct dirent* ent) {
//		switch(ent->d_type) {
//			case DT_REG:
//				return XdevLDirectoryEntity::Type::REGULAR;
//			case DT_DIR:
//				return XdevLDirectoryEntity::Type::DIRECTORY;
//			default:
//				return XdevLDirectoryEntity::Type::UNKNOWN;
//		}
		return XdevLDirectoryEntity::Type::UNKNOWN;
	}

//
//
//

	XdevLFileWindows::~XdevLFileWindows() {
	}

	xdl_int XdevLFileWindows::open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) {
		m_filename = filename;
		return 0;
	}

	xdl_int XdevLFileWindows::open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) {
		m_filename = filename;
		return 0;
	}

	xdl_int XdevLFileWindows::open(const XdevLOpenForReadWrite& readWrite, const XdevLFileName& filename) {
		m_filename = filename;
		return 0;
	}

	xdl_int XdevLFileWindows::open(const XdevLOpenForAppend& append, const XdevLFileName& filename) {
		m_filename = filename;
		return 0;
	}

	/// Close the file.
	xdl_int XdevLFileWindows::close() {
		return 0;
	}

	const XdevLFileName XdevLFileWindows::getFileName() const {
		return m_filename;
	}

	xdl_int XdevLFileWindows::open() {
		XDEVL_MODULEX_ERROR(XdevLFileWindows, "Can't open empty filename object.\n");
		return -1;
	}

	xdl_int XdevLFileWindows::open(const XdevLFileName& filename) {
		return open(XdevLOpenForReadOnly(), filename);
	}

	xdl_int64 XdevLFileWindows::length() {
		return 0;
	}

	xdl_int64 XdevLFileWindows::tell() {
		return 0;
	}

	xdl_int XdevLFileWindows::seek(const XdevLSeekSet& mode, xdl_uint64 offset) {
		return 0;
	}

	xdl_int XdevLFileWindows::seek(const XdevLSeekCurr& mode, xdl_uint64 offset) {
		return 0;
	}
	xdl_int XdevLFileWindows::seek(const XdevLSeekEnd& mode, xdl_uint64 offset) {
		return 0;
	}

	xdl_int XdevLFileWindows::read(xdl_uint8* dst, xdl_int size) {
		return 0;
	}

	xdl_int XdevLFileWindows::write(xdl_uint8* src, xdl_int size) {
		return 0;
	}

	xdl_int XdevLFileWindows::flush() {
		// TODO can we flush here?
		return 0;
	}

	xdl_int XdevLFileWindows::readU8(xdl_uint8& value) {
		return 0;
	}


	xdl_int XdevLFileWindows::read8(xdl_int8& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::readU16(xdl_uint16& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::read16(xdl_int16& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::readU32(xdl_uint32& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::read32(xdl_int32& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::readU64(xdl_uint64& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::read64(xdl_int64& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::readFloat(xdl_float& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::readDouble(xdl_double& value) {
		return 0;
	}

	xdl_int XdevLFileWindows::readStringLine(XdevLString& value) {
		// TODO NO UTF-16/32.
		xdl_char tmp = 0;

		value.clear();

		do {
			//tmp = ::read(m_fd, &tmp, 1); // TODO
			if(tmp == EOF) {
				break;
			}
			value += tmp;
		} while(tmp != '\0' || tmp != '\n');

		return 0;
	}

	xdl_int XdevLFileWindows::readString(XdevLString& value) {
		// TODO NO UTF-16/32.
		xdl_char tmp = 0;

		value.clear();

		do {
			//tmp = ::read(m_fd, &tmp, 1); // TODO
			value += tmp;
			if(tmp == EOF) {
				break;
			}
		} while(tmp != '\0');

		return 0;
	}

	xdl_int XdevLFileWindows::writeU8(xdl_uint8 value) {
		return 0;
	}

	xdl_int XdevLFileWindows::write8(xdl_int8 value) {
		return 0;
	}

	xdl_int XdevLFileWindows::writeU16(xdl_uint16 value) {
		return 0;
	}

	xdl_int XdevLFileWindows::write16(xdl_int16 value) {
		return 0;
	}

	xdl_int XdevLFileWindows::writeU32(xdl_uint32 value) {
		return 0;
	}

	xdl_int XdevLFileWindows::write32(xdl_int32 value) {
		return 0;
	}

	xdl_int XdevLFileWindows::writeU64(xdl_uint64 value) {
		return 0;
	}

	xdl_int XdevLFileWindows::write64(xdl_int64 value) {
		return 0;
	}

	xdl_int XdevLFileWindows::writeFloat(xdl_float value) {
		return 0;
	}

	xdl_int XdevLFileWindows::writeDouble(xdl_double value) {
		return 0;
	}

	xdl_int XdevLFileWindows::writeString(const XdevLString& str) {
		return 0;
	}

	XdevLArchive* XdevLFileWindows::getArchive() {
		return nullptr;
	}


//
//
//

	XdevLFileSystemWindows::XdevLFileSystemWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLFileSystem>(parameter, descriptor) {

	}

	std::shared_ptr<XdevLFile> XdevLFileSystemWindows::createFile() {
		return std::make_shared<XdevLFileWindows>();
	}


//
// XdevLDirectory
//

	XdevLDirectoryWindows::XdevLDirectoryWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLDirectory>(parameter, descriptor),
		m_handle(INVALID_HANDLE_VALUE) {
	}

	xdl_int XdevLDirectoryWindows::init() {
		return RET_SUCCESS;
	}

	void* XdevLDirectoryWindows::getInternal(const XdevLInternalName& id) {
		return nullptr;
	}

	xdl_int XdevLDirectoryWindows::shutdown() {
		return close();
	}

	xdl_int XdevLDirectoryWindows::open() {
		return open(XdevLString("."));
	}

	xdl_int XdevLDirectoryWindows::open(const XdevLString& dirName) {

		WIN32_FIND_DATA fdata;

		std::string dirname = dirName.toString();
		if(dirname[dirname.size()-1] == '\\' || dirname[dirname.size()-1] == '/') {
			dirname = dirname.substr(0,dirname.size()-1);
		}

		m_handle = FindFirstFile(std::string(dirname).append("\\*").c_str(), &fdata);
		if(m_handle != INVALID_HANDLE_VALUE) {
			do {
				if(strcmp(fdata.cFileName, ".") != 0 &&
				        strcmp(fdata.cFileName, "..") != 0) {
					if(fdata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
						XdevLDirectoryEntity entity(XdevLString(fdata.cFileName), XdevLDirectoryEntity::Type::DIRECTORY);
						m_directoryItems.push_back(entity);
					} else {
						XdevLDirectoryEntity entity(XdevLString(fdata.cFileName), XdevLDirectoryEntity::Type::REGULAR);
						m_directoryItems.push_back(entity);
					}
				}
			} while(FindNextFile(m_handle, &fdata) != 0);
		} else {
			XDEVL_MODULE_ERROR("Can't open directory\n");
			return RET_FAILED;
		}

		if(GetLastError() != ERROR_NO_MORE_FILES) {
			FindClose(m_handle);
			XDEVL_MODULE_ERROR("Can't open directory: " << GetLastError() << std::endl);
			return RET_FAILED;
		}
		m_directoryItemsIterator = m_directoryItems.begin();
		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryWindows::close() {
		if(INVALID_HANDLE_VALUE != m_handle) {
			FindClose(m_handle);
			m_handle = INVALID_HANDLE_VALUE;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryWindows::changeTo(const XdevLString& dirname) {
		if(::SetCurrentDirectory(dirname.toString().c_str()) == 0) {
			XDEVL_MODULE_ERROR(getLastErrorText(errorMsgText, sizeof(errorMsgText)));
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryWindows::make(const XdevLString& dirname) {
		if(::CreateDirectory(dirname.toString().c_str(), nullptr) == 0) {
			XDEVL_MODULE_ERROR(getLastErrorText(errorMsgText, sizeof(errorMsgText)));
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryWindows::remove(const XdevLString& dirname) {
		return RET_FAILED;
	}

	xdl_int XdevLDirectoryWindows::rename(const XdevLString& oldname, const XdevLString& newname) {
		return RET_FAILED;
	}

	XdevLString XdevLDirectoryWindows::getWorkingDirectory() {
		char buffer[MAX_PATH];
		GetModuleFileName(NULL, buffer, MAX_PATH);
		XdevLString tmp(buffer);
		return std::move(tmp);
	}

	xdl_uint64 XdevLDirectoryWindows::getNumberOfEntities() {
		return m_directoryItems.size();
	}

	xdl_int XdevLDirectoryWindows::getEntity(XdevLDirectoryEntity& entity) {
		entity = *m_directoryItemsIterator;
		return RET_SUCCESS;
	}

	std::vector<XdevLDirectoryEntity> XdevLDirectoryWindows::getEntities() {
		std::vector<XdevLDirectoryEntity>	m_entity_list(m_directoryItems);


		return std::move(m_entity_list);
	}

	xdl_bool XdevLDirectoryWindows::find(const XdevLString& name, XdevLDirectoryEntity& entity) {

		std::vector<XdevLDirectoryEntity> entities = getEntities();
		if(entities.size() == 0) {
			entity = XdevLDirectoryEntity();
			return xdl_false;
		}
		auto it = std::find(entities.begin(), entities.end(), name);
		if(it == entities.end()) {
			return xdl_false;
		}
		entity = *it;

		// Rewind back the pointer to the begining of the list.
//		rewind();

		return xdl_true;
	}

	std::vector<XdevLString> XdevLDirectoryWindows::getAllDirectories(const XdevLString& directoryName, xdl_bool recursive) {
		std::vector<XdevLString> tmp;
		//	getDirectoryList(tmp, directoryName, recursive);
		return std::move(tmp);
	}

	void XdevLDirectoryWindows::getDirectoryList(std::vector<XdevLString>& folderList, const XdevLString& currentDirectoryName, xdl_bool recursive) {
		close();

		if(open(currentDirectoryName) == RET_FAILED) {
			return;
		}

		std::vector<XdevLDirectoryEntity> folderEntities = getEntities();
		if(folderEntities.size() == 0) {
			return;
		}

		for(auto& entity : folderEntities) {

			if((entity.getType() != XdevLDirectoryEntity::Type::DIRECTORY) ||
			        (entity.getName() == XdevLString(".")) ||
			        (entity.getName() == XdevLString(".."))) {
				continue;
			}

			XdevLString tmp = currentDirectoryName + XdevLString(TEXT("/")) + entity.getName();
			folderList.push_back(tmp);
			if(recursive) {
				getDirectoryList(folderList, tmp, recursive);
			}
		}
	}

	std::vector<XdevLString>  XdevLDirectoryWindows::getAllFiles(const XdevLString& directoryName, xdl_bool recursive, const XdevLString& pattern) {
		std::vector<XdevLString> tmp;
		getFileList(tmp, directoryName, recursive, pattern);
		return std::move(tmp);
	}

	void  XdevLDirectoryWindows::getFileList(std::vector<XdevLString>& fileList, const XdevLString& currentDirectoryName, xdl_bool recursive, const XdevLString& pattern) {
		close();

		if(open(currentDirectoryName) == RET_FAILED) {
			return;
		}

		std::vector<XdevLDirectoryEntity> folderEntities = getEntities();
		if(folderEntities.size() == 0) {
			return;
		}

		for(auto& entity : folderEntities) {
			// TODO Maybe this goes wrong for a Unknown item?
			XdevLString tmp = currentDirectoryName + XdevLString(TEXT("/"));

			if(entity.isRegular()) {
				auto result = entity.getName().find(pattern);
				// TODO the comparison with -1 should be npos but for now we use this hack.
				if(result != (size_t)(-1)) {
					XdevLString filePath = tmp + entity.getName();
					fileList.push_back(filePath);
				}
			} else if(recursive &&
			          entity.isDirectory() &&
			          (entity.getName() != XdevLString(TEXT("."))) &&
			          (entity.getName() != XdevLString(TEXT("..")))) {
				XdevLString folder = tmp + entity.getName();
				getFileList(fileList, folder, recursive, pattern);
			}
		}
	}


	std::vector<XdevLString>  XdevLDirectoryWindows::getAllDirectoriesContainsFile(const XdevLString& directoryName, xdl_bool recursive, const XdevLString& pattern) {
		std::vector<XdevLString> tmp;
		getDirectoryContainsFileList(tmp, directoryName, recursive, pattern);
		return std::move(tmp);
	}

	void XdevLDirectoryWindows::getDirectoryContainsFileList(std::vector<XdevLString>& directoryList, const XdevLString& currentDirectoryName, xdl_bool recursive, const XdevLString& pattern) {
		close();

		if(open(currentDirectoryName) == RET_FAILED) {
			return;
		}

		std::vector<XdevLDirectoryEntity> folderEntities = getEntities();
		if(folderEntities.size() == 0) {
			return;
		}
		xdl_bool addFolderNames = xdl_true;
		for(auto& entity : folderEntities) {

			if((entity.getType() == XdevLDirectoryEntity::Type::REGULAR) && addFolderNames) {
				auto result = entity.getName().find(pattern);
				// TODO the comparison with -1 should be npos but for now we use this hack.
				if(result != (size_t)(-1)) {
					directoryList.push_back(currentDirectoryName);
					addFolderNames = false;
				}
			} else if(recursive &&
			          (entity.getType() == XdevLDirectoryEntity::Type::DIRECTORY) &&
			          (entity.getName() != XdevLString(".")) &&
			          (entity.getName() != XdevLString(".."))) {
				XdevLString recDir = currentDirectoryName + XdevLString(TEXT("/")) + entity.getName();
				getDirectoryContainsFileList(directoryList, recDir, recursive, pattern);
			}
		}
	}



//
// XdevLDirectoryWatcher
//

	XdevLDirectoryWatcherWindows::XdevLDirectoryWatcherWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLDirectoryWatcher>(parameter, descriptor),
		m_threadStarted(xdl_false) {
	}

	XdevLDirectoryWatcherWindows::~XdevLDirectoryWatcherWindows() {
	}

	xdl_int XdevLDirectoryWatcherWindows::init() {
		XDEVL_MODULE_INFO("Initialize XdevLDirectoryWatcher was successful.\n");

		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryWatcherWindows::shutdown() {
		if(xdl_true == m_threadStarted) {
			stop();
		}

		for(auto& watcher : m_watcherItems) {
			destroyWatch(watcher);
		}

		return 0;
	}

	int XdevLDirectoryWatcherWindows::start() {
		if(xdl_true == m_threadStarted) {
			return RET_FAILED;
		}

		m_watcherThread = std::thread(&XdevLDirectoryWatcherWindows::threadHandle, this);
		m_threadStarted = xdl_true;
		m_runThread	= xdl_true;

		return RET_SUCCESS;
	}

	int XdevLDirectoryWatcherWindows::stop() {
		if(xdl_true != m_threadStarted) {
			return RET_FAILED;
		}

		m_runThread = false;
//	m_watcherThread.join();

		return RET_SUCCESS;
	}

	XdevLDirectoryWatcherWindowsItem* XdevLDirectoryWatcherWindows::createWatch(LPCTSTR szDirectory, bool recursive, DWORD mNotifyFilter) {
		XdevLDirectoryWatcherWindowsItem* watcherItem = nullptr;
		size_t ptrsize = sizeof(*watcherItem);
		watcherItem = static_cast<XdevLDirectoryWatcherWindowsItem*>(HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, ptrsize));

		watcherItem->m_handle = ::CreateFile(szDirectory,
		                                     FILE_LIST_DIRECTORY,
		                                     FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
		                                     nullptr,
		                                     OPEN_EXISTING,
		                                     FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED,
		                                     nullptr);

		if(watcherItem->m_handle == INVALID_HANDLE_VALUE) {
			return nullptr;
		}
		watcherItem->m_overlapped.hEvent = CreateEvent(nullptr, TRUE, FALSE, nullptr);
		watcherItem->m_recursive = recursive;
		watcherItem->m_notifyFilter = mNotifyFilter;

		if(refreshWatch(watcherItem)) {
			return watcherItem;
		} else {
			CloseHandle(watcherItem->m_overlapped.hEvent);
			CloseHandle(watcherItem->m_handle);
		}

		HeapFree(GetProcessHeap(), 0, watcherItem);
		return nullptr;
	}

	void XdevLDirectoryWatcherWindows::destroyWatch(XdevLDirectoryWatcherWindowsItem* watcherItem) {
		if(watcherItem) {
			CancelIo(watcherItem->m_handle);
			refreshWatch(watcherItem, true);

			if(!HasOverlappedIoCompleted(&watcherItem->m_overlapped)) {
				SleepEx(5, TRUE);
			}

			CloseHandle(watcherItem->m_overlapped.hEvent);
			CloseHandle(watcherItem->m_handle);
			HeapFree(GetProcessHeap(), 0, watcherItem);
		}
	}



	xdl_int XdevLDirectoryWatcherWindows::addDirectoryToWatch(const XdevLString& folder) {
		xdl_bool recursive = xdl_false;
		XdevLDirectoryWatcherWindowsItem* watcherItem = createWatch(folder.toString().c_str(),
		        recursive,
		        FILE_NOTIFY_CHANGE_CREATION |
		        FILE_NOTIFY_CHANGE_SIZE |
		        FILE_NOTIFY_CHANGE_FILE_NAME |
		        FILE_NOTIFY_CHANGE_DIR_NAME);
		if(nullptr == watcherItem) {
			return RET_FAILED;
		}
		watcherItem->m_directorName = folder;
		watcherItem->m_directoryWatcher = this;
		m_watcherItems.push_back(watcherItem);
		XDEVL_MODULE_INFO("Adding directory: " << folder << " to watch.\n");

		return RET_SUCCESS;
	}

	bool XdevLDirectoryWatcherWindows::refreshWatch(XdevLDirectoryWatcherWindowsItem* watcherItem, xdl_bool clear) {
		return ReadDirectoryChangesW(watcherItem->m_handle, watcherItem->m_buffer, sizeof(watcherItem->m_buffer), watcherItem->m_recursive,
		                             watcherItem->m_notifyFilter, NULL, &watcherItem->m_overlapped, clear ? 0 : ChangeNotification) != 0;
	}


	int XdevLDirectoryWatcherWindows::registerDelegate(const XdevLDirectoryWatcherDelegateType& delegate) {
		m_delegates.push_back(delegate);
		return RET_SUCCESS;
	}

	int XdevLDirectoryWatcherWindows::unregisterDelegate(const XdevLDirectoryWatcherDelegateType& delegate) {
		//m_delegates.find(delegate);
		return RET_SUCCESS;
	}

	void XdevLDirectoryWatcherWindows::ChangeNotification(::DWORD Error, ::DWORD NumBytes, LPOVERLAPPED InOverlapped) {
		TCHAR szFile[MAX_PATH];
		PFILE_NOTIFY_INFORMATION pNotify;
		size_t offset = 0;

		if(NumBytes == 0)
			return;

		XdevLDirectoryWatcherWindowsItem* watcherItem = (XdevLDirectoryWatcherWindowsItem*) InOverlapped;
		if(Error == ERROR_SUCCESS) {
			do {
				pNotify = (PFILE_NOTIFY_INFORMATION) &watcherItem->m_buffer[offset];
				offset += pNotify->NextEntryOffset;

#if defined(UNICODE)
				{
					lstrcpynW(szFile, pNotify->FileName, min(MAX_PATH, pNotify->FileNameLength / sizeof(WCHAR) + 1));
				}
#else
				{
					int count = WideCharToMultiByte(CP_ACP, 0, pNotify->FileName, pNotify->FileNameLength / sizeof(WCHAR),  szFile, MAX_PATH - 1, NULL, NULL);
					szFile[count] = TEXT('\0');
				}
#endif


				watcherItem->m_directoryWatcher->handleChange(watcherItem, XdevLString(szFile), pNotify->Action);

			} while(pNotify->NextEntryOffset != 0);
		}

		if(!watcherItem->m_stopNow) {
			watcherItem->m_directoryWatcher->refreshWatch(watcherItem);
		}

	}

	void XdevLDirectoryWatcherWindows::handleChange(XdevLDirectoryWatcherWindowsItem* watchteritem, const XdevLString& filename, unsigned long action) {
		ItemTypes types = ItemTypes::FILE;
		EventTypes eventType = EventTypes::DW_UNKNOWN;

		switch(action) {
			case FILE_ACTION_RENAMED_NEW_NAME:
			case FILE_ACTION_ADDED:
				eventType = EventTypes::DW_CREATE;
				break;
			case FILE_ACTION_RENAMED_OLD_NAME:
			case FILE_ACTION_REMOVED:
				eventType = EventTypes::DW_DELETE;
				break;
			case FILE_ACTION_MODIFIED:
				eventType = EventTypes::DW_MODIFY;
				break;
		};

		//
		// Now check which event type occurred and use the delegate
		// to inform everyone.
		//
		if(eventType != EventTypes::DW_UNKNOWN) {
			for(auto& delegate : m_delegates) {
				delegate(types, eventType, filename);
			}
		}
	}

	void XdevLDirectoryWatcherWindows::threadHandle() {
		XDEVL_MODULE_INFO("Starting Thread ...");


		// TODO As you can see this part is quite inefficient. Using a vector and
		// collecting first all events and then call the delegates.
		while(m_runThread) {
			std::lock_guard<std::mutex> lock(m_mutex);

		}

		XDEVL_MODULE_INFO("Stopping Thread ...");
	}

}
