/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_FILESYSTEM_WINDOWS_H
#define XDEVL_FILESYSTEM_WINDOWS_H

#include <XdevLFileSystem/XdevLFileSystem.h>
#include <XdevLFileSystem/XdevLDirectory.h>
#include <XdevLFileSystem/XdevLDirectoryWatcher.h>
#include <XdevLFileSystem/PhysFS/XdevLArchivePhysFS.h>

#include <XdevLPluginImpl.h>
#include <vector>

#include <iostream>
#include <array>
#include <thread>
#include <mutex>
#include <atomic>

#include <vector>
#include <cassert>

namespace xdl {


	static const XdevLString XdevLFileSystemPluginName {
		"XdevLFileSystem"
	};

	static const XdevLString XdevLDescriptionForFileSystem {
		"Support for file handling."
	};

	static const XdevLString XdevLDescriptionForDirectory {
		"Support for directory handling."
	};

	static const XdevLString XdevLDescriptionForDirectoryWatcher {
		"Support for directory watching."
	};

	static const std::vector<XdevLModuleName> XdevLFileSystemModuleName	{
		XdevLModuleName("XdevLFile"),
		XdevLModuleName("XdevLDirectory"),
		XdevLModuleName("XdevLDirectoryWatcher"),
		XdevLModuleName("XdevLArchive")
	};

	class XdevLFileWindows;
	class XdevLDirectoryWatcherWindows;

	class XdevLFileWindows : public XdevLFile {
		
		public:
			virtual ~XdevLFileWindows();
			xdl_int open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) override final;
			xdl_int open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) override final;
			xdl_int open(const XdevLOpenForReadWrite& readWrite, const XdevLFileName& filename) override final;
			xdl_int open(const XdevLOpenForAppend& append, const XdevLFileName& filename) override final;
			xdl_int close() override final;
			const XdevLFileName getFileName() const override final;
			xdl_int open() override final;
			xdl_int open(const XdevLFileName& filename) override final;
			xdl_int seek(const XdevLSeekSet& mode, xdl_uint64 offset) override final;
			xdl_int seek(const XdevLSeekCurr& mode, xdl_uint64 offset) override final;
			xdl_int seek(const XdevLSeekEnd& mode, xdl_uint64 offset) override final;
			xdl_int read(xdl_uint8* dst, xdl_int size) override final;
			xdl_int write(xdl_uint8* src, xdl_int size) override final;
			xdl_int flush() override final;
			xdl_int64 length() override final;
			xdl_int64 tell() override final;
			xdl_int readU8(xdl_uint8& value) override final;
			xdl_int read8(xdl_int8& value) override final;
			xdl_int readU16(xdl_uint16& value) override final;
			xdl_int read16(xdl_int16& value) override final;
			xdl_int readU32(xdl_uint32& value) override final;
			xdl_int read32(xdl_int32& value) override final;
			xdl_int readU64(xdl_uint64& value) override final;
			xdl_int read64(xdl_int64& value) override final;
			xdl_int readFloat(xdl_float& value) override final;
			xdl_int readDouble(xdl_double& value) override final;
			xdl_int readStringLine(XdevLString& value) override final;
			xdl_int readString(XdevLString& value) override final;

			xdl_int writeU8(xdl_uint8 value) override final;
			xdl_int write8(xdl_int8 value) override final;
			xdl_int writeU16(xdl_uint16 value) override final;
			xdl_int write16(xdl_int16 value) override final;
			xdl_int writeU32(xdl_uint32 value) override final;
			xdl_int write32(xdl_int32 value) override final;
			xdl_int writeU64(xdl_uint64 value) override final;
			xdl_int write64(xdl_int64 value) override final;
			xdl_int writeFloat(xdl_float value) override final;
			xdl_int writeDouble(xdl_double value) override final;
			xdl_int writeString(const XdevLString& str) override final;
			XdevLArchive* getArchive() override final;

		private:
			XdevLFileName m_filename;
			std::vector<xdl_char*>	m_lineArray;
	};


	class XdevLFileSystemWindows : public XdevLModuleImpl<XdevLFileSystem> {
		public:
			XdevLFileSystemWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

			virtual ~XdevLFileSystemWindows() {}

			std::shared_ptr<XdevLFile> createFile() override final;
	};


	class XdevLDirectoryWindows : public XdevLModuleImpl<XdevLDirectory> {
		public:
			XdevLDirectoryWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLDirectoryWindows() {
			}

			virtual xdl_int init() override;
			virtual xdl_int shutdown() override;
			virtual void* getInternal(const XdevLInternalName& id) override;

			virtual xdl_int open(const XdevLString& dirname);
			virtual xdl_int open();
			virtual xdl_int close();
			virtual xdl_uint64 getNumberOfEntities();
			virtual xdl_int getEntity(XdevLDirectoryEntity& entity);
			virtual std::vector<XdevLDirectoryEntity> getEntities();

			xdl_int changeTo(const XdevLString& dirname) override final;
			xdl_int make(const XdevLString& dirname) override final;
			xdl_int remove(const XdevLString& dirname) override final;
			xdl_int rename(const XdevLString& oldname, const XdevLString& newname) override final;
			XdevLString getWorkingDirectory() override final;
			xdl_bool find(const XdevLString& name, XdevLDirectoryEntity& entity) override final;

			virtual std::vector<XdevLString> getAllFiles(const XdevLString& directoryName, xdl_bool recursive, const XdevLString& pattern = XdevLString(""));
			virtual std::vector<XdevLString> getAllDirectories(const XdevLString& directoryName, xdl_bool recursive);
			virtual std::vector<XdevLString> getAllDirectoriesContainsFile(const XdevLString& directoryName, xdl_bool recursive, const XdevLString& pattern = XdevLString(""));

		private:

			void getDirectoryList(std::vector<XdevLString>& folderList, const XdevLString& currentDirectoryName, xdl_bool recursive);
			void getFileList(std::vector<XdevLString>& fileList, const XdevLString& currentDirectoryName, xdl_bool recursive, const XdevLString& pattern);
			void getDirectoryContainsFileList(std::vector<XdevLString>& directoryList, const XdevLString& currentDirectoryName, xdl_bool recursive, const XdevLString& pattern);
		private:
			HANDLE m_handle;
			std::vector<XdevLDirectoryEntity> m_directoryItems;
			std::vector<XdevLDirectoryEntity>::iterator m_directoryItemsIterator;
	};


	class XdevLDirectoryWatcherWindowsItem {
		public:
			HANDLE m_handle;
			OVERLAPPED m_overlapped;
			DWORD m_notifyFilter;
			xdl_bool m_recursive;
			xdl_bool m_stopNow;
			XdevLDirectoryWatcherWindows* m_directoryWatcher;
			BYTE m_buffer[32 * 1024];
			XdevLString m_directorName;
	};

	class XdevLDirectoryWatcherWindows : public XdevLModuleImpl<XdevLDirectoryWatcher> {
		public:
			XdevLDirectoryWatcherWindows(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

			virtual ~XdevLDirectoryWatcherWindows();

			virtual xdl_int init();
			virtual xdl_int shutdown();
			virtual int start();
			virtual int stop();
			virtual xdl_int addDirectoryToWatch(const XdevLString& folder);
			virtual int registerDelegate(const XdevLDirectoryWatcherDelegateType& delegate);
			virtual int unregisterDelegate(const XdevLDirectoryWatcherDelegateType& delegate);

		public:

			void threadHandle();
			static void CALLBACK ChangeNotification(::DWORD Error, ::DWORD NumBytes, LPOVERLAPPED InOverlapped);
			void handleChange(XdevLDirectoryWatcherWindowsItem* watchteritem, const XdevLString& filename, unsigned long action);

			XdevLDirectoryWatcherWindowsItem* createWatch(LPCTSTR szDirectory, bool recursive, DWORD mNotifyFilter);
			void destroyWatch(XdevLDirectoryWatcherWindowsItem* watcherItem);
			bool refreshWatch(XdevLDirectoryWatcherWindowsItem* watcherItem, xdl_bool clear = xdl_false);

		private:
			std::vector<XdevLDirectoryWatcherWindowsItem*> m_watcherItems;
			std::thread m_watcherThread;
			std::mutex m_mutex;
			std::atomic<xdl_bool> m_runThread;
			std::vector<XdevLDirectoryWatcherDelegateType> m_delegates;
			xdl_bool m_threadStarted;

	};


}

#endif
