/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_DIRECTORY_H
#define XDEVL_DIRECTORY_H

#include <XdevLFileSystem/XdevLFileSystem.h>

namespace xdl {

	/**
		@class XdevLDirectoryEntity
		@brief Information about an entity in the directory.

	    This class holds information about one entity in the directory. Either
	    it is a file or a folder.
	*/
	class XdevLDirectoryEntity {
		public:

			enum class Type {
			    UNKNOWN, REGULAR, DIRECTORY
			};

			XdevLDirectoryEntity()
				:
				m_type(Type::UNKNOWN) {
			};

			XdevLDirectoryEntity(const xdl_char* filename, Type type)
				:
				m_filename(filename),
				m_type(type) {
			};

			/// Returns the name of the entity.
			const XdevLString& getName() const {
				return m_filename;
			}

			/// Returns the type.
			/**
			    @return One of the Type:
			*/
			Type getType() {
				return m_type;
			}

			/// Returns the type as string.
			XdevLString getTypeAsString() {
				switch(m_type) {
					case Type::REGULAR:
						return XdevLString("Regular");
					case Type::DIRECTORY:
						return XdevLString("Directory");
					case Type::UNKNOWN:
					default:
						return XdevLString("Unknown");
				}
			}

			/// Does this entity represent a directory?
			xdl_bool isDirectory() {
				return (m_type == Type::DIRECTORY ? xdl_true : xdl_false);
			}

			/// Is this entity a regular file?
			xdl_bool isRegular() {
				return (m_type == Type::REGULAR ? xdl_true : xdl_false);
			}

			bool operator==(const XdevLString& entityName) {
				return (m_filename == entityName);
			}

			bool operator!=(const XdevLString& entityName) {
				return !(m_filename == entityName);
			}

		protected:

			// Holds the name of the file or folder.
			XdevLString m_filename;

			// Holds the type of item.
			Type m_type;
	};


	/**
		@class XdevLDirectory
		@brief A class holding directory information.
	*/
	class XdevLDirectory : public XdevLModule {
		public:
			virtual ~XdevLDirectory() {}

			/// Open the specified directory.
			/**
				Opens the directory.
				@param dirname A null terminated string which represents the directory name to open.
				@return
					- @b RET_SUCCESS if it was successful.
					- @b RET_FAILED if an error occurred.
			*/
			virtual xdl_int open(const XdevLString& dirname) = 0;

			/// Close the specified directory.
			/**
				After opening the directory you should close it again.
				@return
					- @b RET_SUCCESS if it was successful.
					- @b RET_FAILED if an error occurred.
			*/
			virtual xdl_int close() = 0;

			/// Find all folders recursivly.
			virtual std::vector<XdevLString> getAllDirectories(const XdevLString& directoryName, xdl_bool recursive) = 0;

			/// Find all files recursivly.
			virtual std::vector<XdevLString> getAllFiles(const XdevLString& directoryName, xdl_bool recursive, const XdevLString& pattern = XdevLString("")) = 0;

			/// Find all directories that contains a file with a specific pattern.
			virtual std::vector<XdevLString> getAllDirectoriesContainsFile(const XdevLString& directoryName, xdl_bool recursive, const XdevLString& pattern = XdevLString("")) = 0;


			/// Returns the entities in a folder.
			virtual std::vector<XdevLDirectoryEntity> getEntities() = 0;

			/// Read one entity of the directory.
			/**
				Reads one entity from folder and moves the entity cursor to the next.
				@return
				- @b RET_SUCCESS if it was successful.
				- @b RET_FAILED if an error occurred. Error can be that the cursor reached the end of the items in the directory.
			*/
			virtual xdl_int getEntity(XdevLDirectoryEntity& entity) = 0;

			/// Returns the number of entities in the directory.
			virtual xdl_uint64 getNumberOfEntities() = 0;

			/// Change current directory.
			virtual xdl_int changeTo(const XdevLString& dirname) = 0;

			/// Makes a new directory.
			/**
				@param dirname A null terminated string which the name of the directory
				to make.
				@return
				- @b RET_SUCCESS if it was successful.
				- @b RET_FAILED if an error occurred.
			*/
			virtual xdl_int make(const XdevLString& dirname) = 0;

			/// Removes a directory.
			/**
				@param dirname A null terminated string which is the name of the directory
				to remove.
				@return
				- @b RET_SUCCESS if it was successful.
				- @b RET_FAILED if an error occurred.
			*/
			virtual xdl_int remove(const XdevLString& dirname) = 0;

			/// Rename a folder.
			/**
				@param oldname A null terminated string which represents the name of the old directory.
				@param newname A null terminated string which represents the new directory name.
				@return
				- @b RET_SUCCESS if it was successful.
				- @b RET_FAILED if an error occurred.
			*/
			virtual xdl_int rename(const XdevLString& oldname, const XdevLString& newname) = 0;

			/// Returns the current working directory path.
			/**
				@return A null terminated string which represents the call
			*/
			virtual XdevLString getWorkingDirectory() = 0;

			/// Searches in the current opened directory for an entity.
			/**
				@param name A null terminated string that represents the name of the entity. (File/Folder name).
			*/
			virtual xdl_bool find(const XdevLString& name, XdevLDirectoryEntity& entity) = 0;
	};

	using IXdevLDirectory = XdevLDirectory;
	using IPXdevLDirectory = XdevLDirectory*;

}
#endif
