/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_FILESYSTEM_H
#define XDEVL_FILESYSTEM_H

#include <XdevLStream.h>
#include <XdevLDelegate.h>

namespace xdl {

	class XdevLArchive;

	/**
	 * @class XdevLAccessMode
	 * @brief Access mode used as the base class for parameters.
	 */
	class XdevLAccessMode {};

	/**
	 * @class XdevLOpenForReadOnly
	 * @brief Used as parameter value for read only operations.
	 */
	class XdevLOpenForReadOnly : public XdevLAccessMode {};

	/**
	 * @class XdevLOpenForWriteOnly
	 * @brief Used as parameter value for write only operations.
	 */
	class XdevLOpenForWriteOnly : public XdevLAccessMode {};

	/**
	 * @class XdevLOpenForReadWrite
	 * @brief Used as parameter value for read/write operations.
	 */
	class XdevLOpenForReadWrite : public XdevLAccessMode {};

	/**
	 * @class XdevLOpenForAppend
	 * @brief Used as parameter value for append to a file.
	 */
	class XdevLOpenForAppend : public XdevLAccessMode {};

	/**
	 * @class XdevLSeekMode
	 * @brief Used as the base class for seek parameter.
	 */
	class XdevLSeekMode {};

	/**
	 * @class XdevLSeekSet
	 * @brief Cast class to set seek to set.
	 */
	class XdevLSeekSet : public XdevLSeekMode {};

	/**
	 * @class XdevLSeekCurr
	 * @brief Cast class to set seek to curr.
	 */
	class XdevLSeekCurr : public XdevLSeekMode {};

	/**
	 * @class XdevLSeekEnd
	 * @brief Cast class to set seek to end.
	 */
	class XdevLSeekEnd : public XdevLSeekMode {};


	/**
		@class XdevLFile
		@brief Helper class the gives access to files.

		@section xdevl_file Plugin & Module information

		This object can point to a file in a archive or a regular files in the OS.
	*/
	class XdevLFile : public XdevLStream {
		public:

			virtual ~XdevLFile() {}

			using XdevLStream::open;
			using XdevLStream::close;
			using XdevLStream::flush;

			/// Open an existing file to read only.
			virtual xdl_int open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) = 0;
			virtual xdl_int open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) = 0;
			virtual xdl_int open(const XdevLOpenForReadWrite& readWrite, const XdevLFileName& filename) = 0;
			virtual xdl_int open(const XdevLOpenForAppend& append, const XdevLFileName& filename) = 0;

			/// Moves the file pointer.
			virtual xdl_int seek(const XdevLSeekSet& mode, xdl_uint64 offset) = 0;
			virtual xdl_int seek(const XdevLSeekCurr& mode, xdl_uint64 offset) = 0;
			virtual xdl_int seek(const XdevLSeekEnd& mode, xdl_uint64 offset) = 0;

			/// Returns the length of the file in bytes.
			virtual xdl_int64 length() = 0;

			/// Returns the current position.
			virtual xdl_int64 tell() = 0;

			/// Returns the current filename of the open file.
			virtual const XdevLFileName getFileName() const = 0;

			/// Reads a unsigned 8-bit value from the stream.
			virtual xdl_int readU8(xdl_uint8& value) = 0;

			/// Reads a signed 8-bit value from the stream.
			virtual xdl_int read8(xdl_int8& value) = 0;

			/// Reads a unsigned 16-bit value from the stream.
			virtual xdl_int readU16(xdl_uint16& value) = 0;

			/// Reads a signed 16-bit value from the stream.
			virtual xdl_int read16(xdl_int16& value) = 0;

			/// Reads a unsigned 32-bit value from the stream.
			virtual xdl_int readU32(xdl_uint32& value) = 0;

			/// Reads a signed 32-bit value from the stream.
			virtual xdl_int read32(xdl_int32& value) = 0;

			/// Reads a unsigned 64-bit value from the stream.
			virtual xdl_int readU64(xdl_uint64& value) = 0;

			/// Reads a signed 64-bit value from the stream.
			virtual xdl_int read64(xdl_int64& value) = 0;

			/// Reads a float value from the stream.
			virtual xdl_int readFloat(xdl_float& value) = 0;

			/// Reads a double value from the stream.
			virtual xdl_int readDouble(xdl_double& value) = 0;

			/// Read one line of a string.
			virtual xdl_int readStringLine(XdevLString& value) = 0;

			/// Read string until EOF is reached.
			virtual xdl_int readString(XdevLString& value) = 0;

			/// Writes an unsigned 8-bit value to the stream.
			virtual xdl_int writeU8(xdl_uint8 value) = 0;

			/// Writes an signed 8-bit value to the stream.
			virtual xdl_int write8(xdl_int8 value) = 0;

			/// Writes an unsigned 16-bit value to the stream.
			virtual xdl_int writeU16(xdl_uint16 value) = 0;

			/// Writes an signed 16-bit value to the stream.
			virtual xdl_int write16(xdl_int16 value) = 0;

			/// Writes an unsigned 32-bit value to the stream.
			virtual xdl_int writeU32(xdl_uint32 value) = 0;

			/// Writes an signed 32-bit value to the stream.
			virtual xdl_int write32(xdl_int32 value) = 0;

			/// Writes an unsigned 64-bit value to the stream.
			virtual xdl_int writeU64(xdl_uint64 value) = 0;

			/// Writes an signed 64-bit value to the stream.
			virtual xdl_int write64(xdl_int64 value) = 0;

			/// Writes a float value to the stream.
			virtual xdl_int writeFloat(xdl_float value) = 0;

			/// Writes a double value to the stream.
			virtual xdl_int writeDouble(xdl_double value) = 0;

			/// Writes a line of a string.
			virtual xdl_int writeString(const XdevLString& str) = 0;

			/// Returns the archive if this file is within.
			virtual XdevLArchive* getArchive() = 0;
	};

	/**
	 * @class XdevLFileSystem
	 * @brief Create Native Filesystem instance.
	 */
	class XdevLFileSystem : public XdevLModule {
		public:
			virtual ~XdevLFileSystem() {}

			/// Create a file.
			virtual std::shared_ptr<XdevLFile> createFile() = 0;
	};


	using IXdevLFile = XdevLFile;
	using IPXdevLFile = std::shared_ptr<XdevLFile>;

	using IXdevLFileSystem = XdevLFileSystem;
	using IPXdevLFileSystem = XdevLFileSystem*;

}
#endif
