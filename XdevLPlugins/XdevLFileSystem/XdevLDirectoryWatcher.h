/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_DIRECTORY_WATCHER_H
#define XDEVL_DIRECTORY_WATCHER_H

#include <XdevLFileSystem/XdevLDirectory.h>

namespace xdl {

	/**
	 * @class XdevLDirectoryWatcher
	 * @brief Watches for changes in a directory.
	 */
	class XdevLDirectoryWatcher : public XdevLModule {
		public:
			enum ItemTypes {FILE, DIRECTORY};
			enum EventTypes { DW_OPEN, DW_CLOSE, DW_CREATE, DW_MODIFY, DW_ACCESS, DW_DELETE, DW_UNKNOWN };

			using XdevLDirectoryWatcherDelegateType = XdevLDelegate<void, const ItemTypes&, const EventTypes&, const XdevLString&>;

			virtual ~XdevLDirectoryWatcher() {};


			/// Start watching directories.
			/**
				@return Returns RET_SUCCESS for success else RET_FAILED.
			*/
			virtual int start() = 0;

			/// Stop watching directories.
			/**
				@return
				- @b RET_SUCCESS if it was successful.
				- @b RET_FAILED if an error occurred.
			*/
			virtual int stop() = 0;

			/// Add directory to watch.
			/**
				@return
				- @b RET_SUCCESS if it was successful.
				- @b RET_FAILED if an error occurred.
			*/
			virtual int addDirectoryToWatch(const XdevLString& folder) = 0;

			/// Register delegate for a watcher.
			/**
				@return
				- @b RET_SUCCESS if it was successful.
				- @b RET_FAILED if an error occurred.
			*/
			virtual int registerDelegate(const XdevLDirectoryWatcherDelegateType& delegate) = 0;

			/// Unregister delegate of a watcher.
			/**
				@return
				- @b RET_SUCCESS if it was successful.
				- @b RET_FAILED if an error occurred.
			*/
			virtual int unregisterDelegate(const XdevLDirectoryWatcherDelegateType& delegate) = 0;
	};

	using IXdevLDirectoryWatcher = XdevLDirectoryWatcher;
	using IPXdevLDirectoryWatcher = XdevLDirectoryWatcher*;
}
#endif
