/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_ARCHIVE_PHYSFS_H
#define XDEVL_ARCHIVE_PHYSFS_H

#include <XdevLPluginImpl.h>
#include <XdevLFileSystem/XdevLArchive.h>

#include <physfs.h>

#include <vector>

namespace xdl {


	/**
	 * @struct XdevLArchiveFilePhysFS
	 * @brief Holds information about a file in the archive.
	 */
	struct XdevLArchiveFilePhysFS : public XdevLFile {
		public:
			XdevLArchiveFilePhysFS(XdevLArchive* archive);
			XdevLArchiveFilePhysFS(PHYSFS_File* file, XdevLArchive* archive);
			XdevLArchiveFilePhysFS(PHYSFS_File* file, XdevLArchive* archive, const XdevLFileName& filename);
			XdevLArchiveFilePhysFS(const XdevLFileName& filename);
			virtual ~XdevLArchiveFilePhysFS();

			xdl_int open() override final;
			xdl_int open(const XdevLFileName& name) override final;
			xdl_int close() override final;

			xdl_int read(xdl_uint8* dst, xdl_int size) override final;
			xdl_int write(xdl_uint8* src, xdl_int size) override final;
			xdl_int flush() override final;

			xdl_int open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) override final;
			xdl_int open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) override final;
			xdl_int open(const XdevLOpenForReadWrite& readWrite, const XdevLFileName& filename) override final;
			xdl_int open(const XdevLOpenForAppend& append, const XdevLFileName& filename) override final;

			xdl_int seek(const XdevLSeekSet& mode, xdl_uint64 offset) override final;
			xdl_int seek(const XdevLSeekCurr& mode, xdl_uint64 offset) override final;
			xdl_int seek(const XdevLSeekEnd& mode, xdl_uint64 offset) override final;

			xdl_int64 length() override final;
			xdl_int64 tell() override final;

			const XdevLFileName getFileName() const override final;

			xdl_int readU8(xdl_uint8& value) override final;
			xdl_int read8(xdl_int8& value) override final;
			xdl_int readU16(xdl_uint16& value) override final;
			xdl_int read16(xdl_int16& value) override final;
			xdl_int readU32(xdl_uint32& value) override final;
			xdl_int read32(xdl_int32& value) override final;
			xdl_int readU64(xdl_uint64& value) override final;
			xdl_int read64(xdl_int64& value) override final;
			xdl_int readFloat(xdl_float& value) override final;
			xdl_int readDouble(xdl_double& value) override final;
			xdl_int readStringLine(XdevLString& value) override final;
			xdl_int readString(XdevLString& value) override final;
			xdl_int writeU8(xdl_uint8 value) override final;
			xdl_int write8(xdl_int8 value) override final;
			xdl_int writeU16(xdl_uint16 value) override final;
			xdl_int write16(xdl_int16 value) override final;
			xdl_int writeU32(xdl_uint32 value) override final;
			xdl_int write32(xdl_int32 value) override final;
			xdl_int writeU64(xdl_uint64 value) override final;
			xdl_int write64(xdl_int64 value) override final;
			xdl_int writeFloat(xdl_float value) override final;
			xdl_int writeDouble(xdl_double value) override final;
			xdl_int writeString(const XdevLString& str) override final;
			XdevLArchive* getArchive() override final;
		private:

			PHYSFS_File* m_file;
			XdevLFileName m_filename;
			XdevLArchive* m_archive;
	};

	/**
	 * @class XdevLArchivePhysFS
	 * @brief Handles archives.
	 */
	class XdevLArchivePhysFS : public XdevLModuleImpl<XdevLArchive> {
		public:
			XdevLArchivePhysFS(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

			virtual ~XdevLArchivePhysFS();

			xdl_int init() override final;
			xdl_int setWriteDir(const XdevLFileName& filename) override final;
			xdl_int mount(const XdevLFileName& filename) override final;
			xdl_int unmount(const XdevLFileName& filename) override final;

			std::shared_ptr<XdevLFile> create() override final;
			std::shared_ptr<XdevLFile> open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) override final;
			std::shared_ptr<XdevLFile> open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) override final;
			xdl_int makedir(const XdevLFileName& dirname) override final;
			xdl_int remove(const XdevLFileName& fileDirName) override final;

			xdl_bool fileExists(const XdevLFileName& filename) override final;

		private:

			xdl_int makediros(const XdevLFileName& dirname);

	};

}

#endif
