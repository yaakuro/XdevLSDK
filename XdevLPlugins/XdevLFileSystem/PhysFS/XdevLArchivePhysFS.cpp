#include "XdevLArchivePhysFS.h"

#include <XdevLXfstring.h>

#include <string.h>

#ifdef _WIN32
#  include <direct.h>
#  include <io.h>
#  define XDEVL_MKDIR(DIR) _mkdir(DIR);
#else
#  include <unistd.h>
#  include <utime.h>
#  include <sys/stat.h>
#  define XDEVL_MKDIR(DIR) mkdir(DIR,0775);
#endif

#include <fcntl.h>

namespace xdl {

	XdevLArchiveFilePhysFS::XdevLArchiveFilePhysFS(XdevLArchive* archive) :
		m_file(nullptr),
		m_archive(archive) {
	}

	XdevLArchiveFilePhysFS::XdevLArchiveFilePhysFS(PHYSFS_File* file, XdevLArchive* archive) :
		m_file(file),
		m_archive(archive) {
	}

	XdevLArchiveFilePhysFS::XdevLArchiveFilePhysFS(PHYSFS_File* file, XdevLArchive* archive, const XdevLFileName& filename) :
		m_file(file),
		m_filename(filename),
		m_archive(archive) {
	}

	XdevLArchiveFilePhysFS::XdevLArchiveFilePhysFS(const XdevLFileName& filename) :
		m_file(nullptr),
		m_filename(filename),
		m_archive(nullptr) {
	}

	XdevLArchiveFilePhysFS::~XdevLArchiveFilePhysFS() {

	}

	xdl_int XdevLArchiveFilePhysFS::open() {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		m_file = PHYSFS_openRead(m_filename.toString().c_str());
		if(NULL == m_file) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_openRead failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		return RET_SUCCESS;

	}

	xdl_int XdevLArchiveFilePhysFS::open(const XdevLFileName& filename) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		m_file = PHYSFS_openRead(filename.toString().c_str());
		if(NULL == m_file) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_openRead failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		m_filename = filename;
		return RET_SUCCESS;
	}

	xdl_int XdevLArchiveFilePhysFS::close() {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is already nullptr.");

		if(PHYSFS_close(m_file) == 0) {
			XDEVL_MODULEX_ERROR(XdevLArchiveFilePhysFS, "PHYSFS_close failed: " << PHYSFS_getLastError() << "\n.");
			return RET_FAILED;
		}
		m_file = nullptr;
		return RET_SUCCESS;
	}

	xdl_int XdevLArchiveFilePhysFS::flush() {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		if(PHYSFS_flush(m_file) == 0) {
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLArchiveFilePhysFS::read(xdl_uint8* dst, xdl_int size) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		auto bytesRead = PHYSFS_readBytes(m_file, dst, size);
		if(-1 == bytesRead) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::write(xdl_uint8* src, xdl_int size) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		auto bytesWrite = PHYSFS_writeBytes(m_file, src, size);
		if(-1 == bytesWrite) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		m_file = PHYSFS_openRead(filename.toString().c_str());
		if(NULL == m_file) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_openRead failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		m_filename = filename;
		return RET_SUCCESS;
	}

	xdl_int XdevLArchiveFilePhysFS::open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		m_file = PHYSFS_openWrite(filename.toString().c_str());
		if(NULL == m_file) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_openWrite failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		m_filename = filename;
		return RET_SUCCESS;
	}

	xdl_int XdevLArchiveFilePhysFS::open(const XdevLOpenForReadWrite& readWrite, const XdevLFileName& filename) {
		XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "XdevLOpenForReadWrite not supported. \n");
		return RET_FAILED;
	}

	xdl_int XdevLArchiveFilePhysFS::open(const XdevLOpenForAppend& append, const XdevLFileName& filename) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		m_file = PHYSFS_openAppend(filename.toString().c_str());
		if(NULL == m_file) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_openAppend failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		m_filename = filename;
		return RET_SUCCESS;
	}


	xdl_int XdevLArchiveFilePhysFS::seek(const XdevLSeekSet& mode, xdl_uint64 offset) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		if(PHYSFS_seek(m_file, offset) == 0) {
			XDEVL_MODULEX_ERROR(XdevLArchiveFilePhysFS, "PHYSFS_seek failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLArchiveFilePhysFS::seek(const XdevLSeekCurr& mode, xdl_uint64 offset) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		PHYSFS_sint64 filePos = PHYSFS_tell(m_file);
		if(PHYSFS_seek(m_file, filePos + offset) == 0) {
			XDEVL_MODULEX_ERROR(XdevLArchiveFilePhysFS, "PHYSFS_seek failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLArchiveFilePhysFS::seek(const XdevLSeekEnd& mode, xdl_uint64 offset) {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		PHYSFS_sint64 fileLength = PHYSFS_fileLength(m_file);
		if(PHYSFS_seek(m_file, fileLength - offset) == 0) {
			XDEVL_MODULEX_ERROR(XdevLArchiveFilePhysFS, "PHYSFS_seek failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	const XdevLFileName XdevLArchiveFilePhysFS::getFileName() const {
		return m_filename;
	}

	xdl_int64 XdevLArchiveFilePhysFS::length() {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		PHYSFS_sint64 fileSize = PHYSFS_fileLength(m_file);
		return fileSize;
	}

	xdl_int64 XdevLArchiveFilePhysFS::tell() {
		XDEVL_ASSERT(m_file != nullptr, "File pointer is != nullptr.");

		PHYSFS_sint64 filePos = PHYSFS_tell(m_file);
		return filePos;
	}

	xdl_int XdevLArchiveFilePhysFS::readU8(xdl_uint8& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 1);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::read8(xdl_int8& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 1);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::readU16(xdl_uint16& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 2);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::read16(xdl_int16& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 2);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::readU32(xdl_uint32& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 4);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::read32(xdl_int32& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 4);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::readU64(xdl_uint64& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 8);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::read64(xdl_int64& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 8);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::readFloat(xdl_float& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 4);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::readDouble(xdl_double& value) {
		auto bytesRead = PHYSFS_readBytes(m_file, &value, 8);
		if(bytesRead == -1) {
			return -1;
		}
		return bytesRead;
	}

	xdl_int XdevLArchiveFilePhysFS::readStringLine(XdevLString& value)  {
		// TODO NO UTF-16/32.
		xdl_char tmp = 0;

		value.clear();

		do {
			PHYSFS_readBytes(m_file, &tmp, 1);
			if(tmp == EOF) {
				break;
			} else if(tmp == '\n') {
				break;
			}
			value += tmp;
		} while(tmp != '\0');

		return 0;
	}

	xdl_int XdevLArchiveFilePhysFS::readString(XdevLString& value)  {
		// TODO NO UTF-16/32.
		xdl_char tmp = 0;

		value.clear();

		do {
			auto bytesRead = PHYSFS_readBytes(m_file, &tmp, 1);
			if((-1 == bytesRead)) {
				break;
			}
			value += tmp;
			if(PHYSFS_eof(m_file) > 0) {
				break;
			}
		} while(tmp != '\0');

		return 0;
	}

	xdl_int XdevLArchiveFilePhysFS::writeU8(xdl_uint8 value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 1);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::write8(xdl_int8 value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 1);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::writeU16(xdl_uint16 value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 2);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::write16(xdl_int16 value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 2);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::writeU32(xdl_uint32 value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 4);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::write32(xdl_int32 value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 4);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::writeU64(xdl_uint64 value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 8);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::write64(xdl_int64 value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 8);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::writeFloat(xdl_float value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 4);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::writeDouble(xdl_double value) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, &value, 8);
		if(bytesWrite == -1) {
			return -1;
		}
		return bytesWrite;
	}

	xdl_int XdevLArchiveFilePhysFS::writeString(const XdevLString& str) {
		auto bytesWrite = PHYSFS_writeBytes(m_file, str.toString().data(), str.toString().length());
		if(bytesWrite == -1) {
			return -1;
		}
		char eol = '\0';
		bytesWrite = PHYSFS_writeBytes(m_file, &eol, 1);
		return str.toString().length();
	}

	XdevLArchive* XdevLArchiveFilePhysFS::getArchive()  {
		return m_archive;
	}

//
//
//


	XdevLArchivePhysFS::XdevLArchivePhysFS(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLArchive>(parameter, descriptor) {

		if(PHYSFS_init(nullptr) == 0) {
			XDEVL_MODULEX_INFO(XdevLArchivePhysFS, "PHYSFS_init failed: " << PHYSFS_getLastError() << "\n");
		} else {
			XDEVL_MODULEX_SUCCESS(XdevLArchivePhysFS, "Initialize PhysFS library.\n");
		}

	}

	XdevLArchivePhysFS::~XdevLArchivePhysFS() {

		if(PHYSFS_deinit() == 0) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_init failed: " << PHYSFS_getLastError() << "\n");
		} else {
			XDEVL_MODULEX_SUCCESS(XdevLArchivePhysFS, "De-Initialize PhysFS library.\n");
		}

	}

	xdl_int XdevLArchivePhysFS::init() {
		mount(XdevLFileName("."));
		setWriteDir(XdevLFileName("."));
		return RET_SUCCESS;
	}

	xdl_int XdevLArchivePhysFS::setWriteDir(const XdevLFileName& filename) {
		auto result = PHYSFS_setWriteDir(filename.toString().c_str());
		if(0 == result) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_setWriteDir failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLArchivePhysFS::mount(const XdevLFileName& filename) {
		auto result = PHYSFS_mount(filename.toString().c_str(), NULL, 1);
		if(0 == result) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_mount failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLArchivePhysFS::unmount(const XdevLFileName& filename) {
		auto result = PHYSFS_unmount(filename.toString().c_str());
		if(0 == result) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_unmount failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	std::shared_ptr<XdevLFile> XdevLArchivePhysFS::create() {
		return std::make_shared<XdevLArchiveFilePhysFS>(this);
	}

	std::shared_ptr<XdevLFile> XdevLArchivePhysFS::open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) {
		PHYSFS_File* file = PHYSFS_openRead(filename.toString().c_str());
		if(NULL == file) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_openRead failed: " << PHYSFS_getLastError() << "\n");
			return nullptr;
		}

		return std::make_shared<XdevLArchiveFilePhysFS>(file, this, filename);
	}

	std::shared_ptr<XdevLFile> XdevLArchivePhysFS::open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) {
		PHYSFS_File* file = PHYSFS_openWrite(filename.toString().c_str());
		if(NULL == file) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_openWrite failed: " << PHYSFS_getLastError() << "\n");
			return nullptr;
		}
		return std::make_shared<XdevLArchiveFilePhysFS>(file, this, filename);
	}

	xdl_int XdevLArchivePhysFS::makedir(const XdevLFileName& dirname) {
		if(PHYSFS_mkdir(dirname.toString().c_str()) == 0) {
			XDEVL_MODULEX_ERROR(XdevLArchivePhysFS, "PHYSFS_mkdir failed: " << PHYSFS_getLastError() << "\n");
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_bool XdevLArchivePhysFS::fileExists(const XdevLFileName& filename) {
		if(PHYSFS_exists(filename.toString().c_str()) == 0) {
			return xdl_false;
		}
		return xdl_true;
	}

	xdl_int XdevLArchivePhysFS::remove(const XdevLFileName& fileDirName) {
		if(PHYSFS_delete(fileDirName.toString().c_str()) == 0) {
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLArchivePhysFS::makediros(const XdevLFileName& dirname) {
		//
		// What we do here is to create the folders recursively.
		//
		char tmp[256];
		char *p = NULL;
		xdl_int ret = 0;

		snprintf(tmp, sizeof(tmp),"%s", dirname.toString().c_str());
		size_t len = strlen(tmp);
		if(tmp[len - 1] == '/') {
			tmp[len - 1] = 0;
		}

		for(p = tmp + 1; *p; p++) {
			if(*p == '/') {
				*p = 0;
				XDEVL_MKDIR(tmp);
				*p = '/';
			}
		}
		XDEVL_MKDIR(tmp);

		return ret;
	}




}
