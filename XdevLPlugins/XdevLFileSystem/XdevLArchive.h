/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_ARCHIVE_H
#define XDEVL_ARCHIVE_H

#include <XdevLFileSystem/XdevLFileSystem.h>

namespace xdl {

	/**
	 * @class XdevLArchive
	 * @brief Handles archives.
	 */
	class XdevLArchive : public XdevLModule {
		public:
			virtual ~XdevLArchive() {};

			/// Sets the write directory for all write operations.
			virtual xdl_int setWriteDir(const XdevLFileName& filename) = 0;

			/// Mount the specified archive file.
			virtual xdl_int mount(const XdevLFileName& filename) = 0;
			
			/// Mount the specified archive file.
			virtual xdl_int unmount(const XdevLFileName& filename) = 0;

			/// Create a XdevLFile instance.
			virtual std::shared_ptr<XdevLFile> create() = 0;

			/// Open file for read only.
			virtual std::shared_ptr<XdevLFile> open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) = 0;

			/// Open file for write only.
			virtual std::shared_ptr<XdevLFile> open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) = 0;

			/// Create a folder.
			virtual xdl_int makedir(const XdevLFileName& dirname) = 0;
			
			/// Removed a file or folder.
			virtual xdl_int remove(const XdevLFileName& fileDirName) = 0;

			/// Checks if a file exists.
			virtual xdl_bool fileExists(const XdevLFileName& filename) = 0;
	};

	using IXdevLArchive = XdevLArchive;
	using IPXdevLArchive = XdevLArchive*;
}

#endif
