/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_FILESYSTEM_MACOSX_H
#define XDEVL_FILESYSTEM_MACOSX_H

#include <XdevLFileSystem/XdevLFileSystem.h>
#include <XdevLFileSystem/Unix/XdevLFileSystemUnix.h>
#include <XdevLFileSystem/PhysFS/XdevLArchivePhysFS.h>
#include <vector>

#include <CoreServices/CoreServices.h>

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <array>
#include <thread>
#include <mutex>
#include <atomic>

#include <vector>
#include <cassert>

namespace xdl {

	class XdevLDirectoryWatcherMacOSX : public XdevLModuleImpl<XdevLDirectoryWatcher> {
		public:
			XdevLDirectoryWatcherMacOSX(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
				: XdevLModuleImpl<XdevLDirectoryWatcher>(parameter, descriptor) {};

			virtual ~XdevLDirectoryWatcherMacOSX() {};

			virtual xdl_int init();
			virtual xdl_int shutdown();

			virtual int start();
			virtual int stop();

			virtual xdl_int addDirectoryToWatch(const XdevLString& folder);

			virtual int registerDelegate(const XdevLDirectoryWatcherDelegateType& delegate);
			virtual int unregisterDelegate(const XdevLDirectoryWatcherDelegateType& delegate);
			void handleChanges(ItemTypes itemType, EventTypes eventType, const XdevLString& path);

		private:

			static void FileSystemEventCallback(ConstFSEventStreamRef,
			                                    void* clientCallBackInfo,
			                                    size_t numEvents,
			                                    void* eventPaths,
			                                    const FSEventStreamEventFlags eventFlags[],
			                                    const FSEventStreamEventId eventIds[]);
		private:
			std::vector<XdevLDirectoryWatcherDelegateType> m_delegates;
		private:
			std::vector<FSEventStreamRef> m_streams;

	};
}

#endif
