/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLFileSystemMacOSX.h"

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::XdevLFileSystemPluginName,
	xdl::XdevLFileSystemModuleName,
	XDEVL_FILESYSTEM_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MINOR_VERSION,
	XDEVL_FILESYSTEM_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleFileDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::XdevLFileSystemModuleName[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::XdevLDescriptionForFileSystem,
	XDEVL_FILESYSTEM_MODULE_FILE_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_FILE_MINOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_FILE_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDirectoryDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::XdevLFileSystemModuleName[1],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::XdevLDescriptionForDirectory,
	XDEVL_FILESYSTEM_MODULE_DIRECTORY_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_DIRECTORY_MINOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_DIRECTORY_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDirectoryWatcherDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::XdevLFileSystemModuleName[2],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::XdevLDescriptionForFileSystem,
	XDEVL_FILESYSTEM_MODULE_DIRECTORYWATCHER_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_DIRECTORYWATCHER_MINOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_DIRECTORYWATCHER_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleArchiveDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::XdevLFileSystemModuleName[3],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::XdevLDescriptionForFileSystem,
	XDEVL_FILESYSTEM_MODULE_ARCHIVE_MAJOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_ARCHIVE_MINOR_VERSION,
	XDEVL_FILESYSTEM_MODULE_ARCHIVE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLFileSystemUnix, moduleFileDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLDirectoryUnix, moduleDirectoryDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLDirectoryWatcherMacOSX, moduleDirectoryWatcherDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLArchivePhysFS, moduleArchiveDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLFileSystem, xdl::XdevLFileSystemUnix, moduleFileDescriptor)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLDirectory, xdl::XdevLDirectoryUnix, moduleDirectoryDescriptor)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLDirectoryWatcher, xdl::XdevLDirectoryWatcherMacOSX, moduleDirectoryWatcherDescriptor)
XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLArchive, xdl::XdevLArchivePhysFS, moduleArchiveDescriptor)

XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DEFINITION_DEFAULT(XdevLFile)
XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DEFINITION_DEFAULT(XdevLDirectory)
XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DEFINITION_DEFAULT(XdevLDirectoryWatcher)
XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DEFINITION_DEFAULT(XdevLArchive)



extern "C" XDEVL_EXPORT xdl::XdevLModule* _createModule(const xdl::XdevLPluginDescriptor& pluginDescriptor, const xdl::XdevLModuleDescriptor& moduleDescriptor) {

	if(moduleDirectoryDescriptor.getName() == moduleDescriptor.getName()) {
		xdl::XdevLModule* obj = new xdl::XdevLDirectoryUnix(nullptr, moduleDirectoryDescriptor);
		if(!obj)
			return nullptr;

		return obj;

	} else if(moduleFileDescriptor.getName() == moduleDescriptor.getName()) {
		xdl::XdevLModule* obj = new xdl::XdevLFileSystemUnix(nullptr, moduleFileDescriptor);
		if(!obj) {
			return nullptr;
		}

		return obj;

	} else if(moduleDirectoryWatcherDescriptor.getName() == moduleDescriptor.getName()) {
		xdl::XdevLModule* obj = new xdl::XdevLDirectoryWatcherMacOSX(nullptr, moduleDirectoryWatcherDescriptor);
		if(!obj) {
			return nullptr;
		}

		return obj;

	} else if(moduleArchiveDescriptor.getName() == moduleDescriptor.getName()) {
		xdl::XdevLModule* obj = new xdl::XdevLArchivePhysFS(nullptr, moduleArchiveDescriptor);
		if(!obj) {
			return nullptr;
		}

		return obj;
	}

	return nullptr;
}

namespace xdl {

	xdl_int XdevLDirectoryWatcherMacOSX::init() {
		return 0;
	}

	xdl_int XdevLDirectoryWatcherMacOSX::shutdown() {
		for(auto stream : m_streams) {
			FSEventStreamStop(stream);
			FSEventStreamInvalidate(stream);
			FSEventStreamRelease(stream);
		}
		return 0;
	}

	int XdevLDirectoryWatcherMacOSX::start() {
		for(auto stream : m_streams) {
			FSEventStreamScheduleWithRunLoop(stream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
			FSEventStreamStart(stream);
		}
		CFRunLoopRun();
		return RET_SUCCESS;
	}

	int XdevLDirectoryWatcherMacOSX::stop() {
		for(auto stream : m_streams) {
			FSEventStreamStop(stream);
			FSEventStreamInvalidate(stream);
			FSEventStreamRelease(stream);
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryWatcherMacOSX::addDirectoryToWatch(const XdevLString& folder) {

		CFStringRef pathToWatchCF = CFStringCreateWithCString(kCFAllocatorDefault, folder.toString().c_str(), kCFStringEncodingUTF8);
		CFArrayRef pathsToWatch = CFArrayCreate(nullptr, (const void **)&pathToWatchCF, 1, nullptr);

		FSEventStreamContext context {0, this, nullptr, nullptr, nullptr};
		FSEventStreamRef streamRef = FSEventStreamCreate(nullptr,
		                             &XdevLDirectoryWatcherMacOSX::FileSystemEventCallback,
		                             &context,
		                             pathsToWatch,
		                             kFSEventStreamEventIdSinceNow,
		                             1.0,
		                             kFSEventStreamCreateFlagFileEvents);

		CFRelease(pathToWatchCF);

		m_streams.push_back(streamRef);

		return RET_SUCCESS;
	}

	int XdevLDirectoryWatcherMacOSX::registerDelegate(const XdevLDirectoryWatcherDelegateType& delegate) {
		m_delegates.push_back(delegate);
		return RET_SUCCESS;
	}

	int XdevLDirectoryWatcherMacOSX::unregisterDelegate(const XdevLDirectoryWatcherDelegateType& delegate) {
		auto foundDelegate = std::find(m_delegates.begin(), m_delegates.end(), delegate);
		if(foundDelegate != m_delegates.end()) {
			m_delegates.erase(foundDelegate);
		}
		return RET_SUCCESS;
	}

	void XdevLDirectoryWatcherMacOSX::handleChanges(ItemTypes itemType, EventTypes eventType, const XdevLString& path) {
		//
		// Now check which event type occurred and use the delegate
		// to inform everyone.
		//
		if(eventType != EventTypes::DW_UNKNOWN) {
			for(auto& delegate : m_delegates) {
				delegate(itemType, eventType, path);
			}
		}
	}

	void XdevLDirectoryWatcherMacOSX::FileSystemEventCallback(ConstFSEventStreamRef streamRef,
	    void *clientCallBackInfo,
	    size_t numEvents,
	    void *eventPaths,
	    const FSEventStreamEventFlags eventFlags[],
	    const FSEventStreamEventId eventIds[]) {
		char **paths = (char **)eventPaths;

		XdevLDirectoryWatcherMacOSX* directoryWatcher = static_cast<XdevLDirectoryWatcherMacOSX*>(clientCallBackInfo);

		ItemTypes types = ItemTypes::FILE;
		EventTypes eventType = EventTypes::DW_UNKNOWN;
		for(size_t i=0; i<numEvents; i++) {
			if(eventFlags[i] & kFSEventStreamEventFlagItemCreated) {
				eventType = EventTypes::DW_CREATE;
			} else if(eventFlags[i] & kFSEventStreamEventFlagItemRemoved) {
				eventType = EventTypes::DW_DELETE;
			} else if(eventFlags[i] & kFSEventStreamEventFlagItemModified) {
				eventType = EventTypes::DW_MODIFY;
			}

			if(eventFlags[i] & kFSEventStreamEventFlagItemIsFile) {
				types = ItemTypes::FILE;
			} else if(eventFlags[i] & kFSEventStreamEventFlagItemIsDir) {
				types = ItemTypes::DIRECTORY;
			}

			directoryWatcher->handleChanges(types, eventType, XdevLString(paths[i]));

		}

	}


}
