/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLFileSystemUnix.h"

#include <algorithm>

namespace xdl {
	const char* parent_dir = "..";
	const char* local_dir = ".";

	bool sort_name_dir_entitiy(const XdevLDirectoryEntity& i, const XdevLDirectoryEntity& j) {
		std::string si(i.getName());
		std::string sj(j.getName());
		return (si < sj);
	}


	XdevLFileUnix::~XdevLFileUnix() {
	}

	xdl_int XdevLFileUnix::open(const XdevLOpenForReadOnly& readOnly, const XdevLFileName& filename) {
		m_fd = ::open(filename, O_RDONLY);
		if(m_fd == -1) {
			XDEVL_MODULEX_ERROR(XdevLFileUnix, "Could not open file: " << filename << std::endl);
			return -1;
		}
		m_filename = filename;
		return 0;
	}

	xdl_int XdevLFileUnix::open(const XdevLOpenForWriteOnly& writeOnly, const XdevLFileName& filename) {
		mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
		m_fd = ::open(filename, O_CREAT | O_WRONLY, mode);
		if(m_fd == -1) {
			XDEVL_MODULEX_ERROR(XdevLFileUnix, "Could not open file: " << filename << std::endl);
			return -1;
		}
		m_filename = filename;
		return 0;
	}

	xdl_int XdevLFileUnix::open(const XdevLOpenForReadWrite& readWrite, const XdevLFileName& filename) {
		mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
		m_fd = ::open(filename, O_CREAT | O_RDWR, mode);
		if(m_fd == -1) {
			XDEVL_MODULEX_ERROR(XdevLFileUnix, "Could not open file: " << filename << std::endl);
			return -1;
		}
		m_filename = filename;
		return 0;
	}

	xdl_int XdevLFileUnix::open(const XdevLOpenForAppend& append, const XdevLFileName& filename) {
		mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
		m_fd = ::open(filename, O_CREAT | O_RDWR | O_APPEND, mode);
		if(m_fd == -1) {
			XDEVL_MODULEX_ERROR(XdevLFileUnix, "Could not open file: " << filename << std::endl);
			return -1;
		}
		m_filename = filename;
		return 0;
	}

	/// Close the file.
	xdl_int XdevLFileUnix::close() {
		if(::close(m_fd) == -1) {
			// TODO What to do with the file descriptor?
			return -1;
		}
		m_fd = -1;
		m_filename = XdevLFileName();
		return 0;
	}

	const XdevLFileName XdevLFileUnix::getFileName() const {
		return m_filename;
	}

	xdl_int XdevLFileUnix::open() {
		XDEVL_MODULEX_ERROR(XdevLFileUnix, "Can't open empty filename object.\n");
		return -1;
	}

	xdl_int XdevLFileUnix::open(const XdevLFileName& filename) {
		return open(XdevLOpenForReadOnly(), filename);
	}

	xdl_int XdevLFileUnix::seek(const XdevLSeekSet& mode, xdl_uint64 offset) {
		ssize_t ret = -1;
		if((ret = ::lseek(m_fd, offset, SEEK_SET)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::seek(const XdevLSeekCurr& mode, xdl_uint64 offset) {
		ssize_t ret = -1;
		if((ret = ::lseek(m_fd, offset, SEEK_CUR)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::seek(const XdevLSeekEnd& mode, xdl_uint64 offset) {
		ssize_t ret = -1;
		if((ret = ::lseek(m_fd, offset, SEEK_END)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::read(xdl_uint8* dst, xdl_int size) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, dst, size)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::write(xdl_uint8* src, xdl_int size) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, src, size)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::flush() {
		// TODO can we flush here?
		return 0;
	}

	xdl_int64 XdevLFileUnix::length() {
		return 0;
	}

	xdl_int64 XdevLFileUnix::tell() {
		return 0;
	}

	xdl_int XdevLFileUnix::readU8(xdl_uint8& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 1)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::read8(xdl_int8& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 1)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::readU16(xdl_uint16& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 2)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::read16(xdl_int16& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 2)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::readU32(xdl_uint32& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 4)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::read32(xdl_int32& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 4)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::readU64(xdl_uint64& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 8)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::read64(xdl_int64& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 8)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::readFloat(xdl_float& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 4)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::readDouble(xdl_double& value) {
		ssize_t ret = -1;
		if((ret = ::read(m_fd, &value, 8)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::readStringLine(XdevLString& value)  {
		// TODO NO UTF-16/32.
		xdl_char tmp = 0;

		value.clear();

		do {
			::read(m_fd, &tmp, 1);
			if(tmp == EOF) {
				break;
			} else if(tmp == '\n') {
				break;
			} else if(tmp == '\0') {
				break;
			}
			value += tmp;
		} while(true);

		return 0;
	}

	xdl_int XdevLFileUnix::readString(XdevLString& value)  {
		// TODO NO UTF-16/32.
		xdl_char tmp = 0;

		value.clear();

		do {
			::read(m_fd, &tmp, 1);
			if(tmp == EOF) {
				break;
			} else if(tmp == '\0') {
				break;
			}
			value += tmp;
		} while(true);

		return 0;
	}

	xdl_int XdevLFileUnix::writeU8(xdl_uint8 value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 1)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::write8(xdl_int8 value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 1)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::writeU16(xdl_uint16 value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 2)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::write16(xdl_int16 value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 2)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::writeU32(xdl_uint32 value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 4)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::write32(xdl_int32 value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 4)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::writeU64(xdl_uint64 value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 8)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::write64(xdl_int64 value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 8)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::writeFloat(xdl_float value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 4)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::writeDouble(xdl_double value) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, &value, 8)) == -1) {
			return -1;
		}
		return ret;
	}

	xdl_int XdevLFileUnix::writeString(const XdevLString& str) {
		ssize_t ret = -1;
		if((ret = ::write(m_fd, str.toString().data(), str.toString().length())) == -1) {
			return -1;
		}
		char eol = '\0';
		::write(m_fd, &eol, 1);
		return ret;
	}

	XdevLArchive* XdevLFileUnix::getArchive()  {
		return nullptr;
	}
//
//
//

	XdevLFileSystemUnix::XdevLFileSystemUnix(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLFileSystem>(parameter, descriptor) {

	}

	std::shared_ptr<XdevLFile> XdevLFileSystemUnix::createFile() {
		return std::make_shared<XdevLFileUnix>();
	}

//
//
//



	XdevLDirectoryEntity::Type extractType(struct dirent* ent) {
		switch(ent->d_type) {
			case DT_REG:
				return XdevLDirectoryEntity::Type::REGULAR;
			case DT_DIR:
				return XdevLDirectoryEntity::Type::DIRECTORY;
			default:
				return XdevLDirectoryEntity::Type::UNKNOWN;
		}
	}

	XdevLDirectoryUnix::XdevLDirectoryUnix(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLDirectory>(parameter, descriptor), m_dir(nullptr) {}

	xdl_int XdevLDirectoryUnix::init() {
		m_workingDirectoryBuffer = new char[512];

		return RET_SUCCESS;
	}

	void* XdevLDirectoryUnix::getInternal(const XdevLInternalName& id) {
		return nullptr;
	}

	xdl_int XdevLDirectoryUnix::shutdown() {
		delete [] m_workingDirectoryBuffer;
		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryUnix::open(const XdevLString& dirname) {
		m_dir = opendir(dirname.toString().c_str());
		if(nullptr == m_dir) {
			errno = ENOENT;
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryUnix::close() {
		if(m_dir) {
			if(-1 == closedir(m_dir)) {
				return RET_FAILED;
			}
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryUnix::changeTo(const XdevLString& dirname) {
		if(::chdir(dirname.toString().c_str()) == -1)
			return RET_FAILED;

		if(open() == RET_FAILED) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryUnix::open() {
		m_dir = opendir(".");
		if(nullptr == m_dir) {
			errno = ENOENT;
			return RET_FAILED;
		}

		return 0;
	}

	xdl_int XdevLDirectoryUnix::make(const XdevLString& dirname) {
		if(::mkdir(dirname.toString().c_str(), S_IRWXU | S_IRGRP |  S_IROTH | S_IXGRP | S_IXOTH) == -1) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryUnix::remove(const XdevLString& dirname) {
		if(rmdir(dirname.toString().c_str()) == -1) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLDirectoryUnix::rename(const XdevLString& oldname, const XdevLString& newname) {
		if(::rename(oldname.toString().c_str(), newname.toString().c_str()) == -1) {
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	void XdevLDirectoryUnix::seek(xdl_uint64 pos) {
		if(m_dir) {
#ifdef XDEVL_PLATFORM_ANDROID
#else
			seekdir(m_dir, pos);
#endif
		}
	}

	void XdevLDirectoryUnix::rewind() {
		if(m_dir) {
			rewinddir(m_dir);
		}
	}

	xdl_uint64 XdevLDirectoryUnix::tell() {
#ifdef XDEVL_PLATFORM_ANDROID
		return 0;
#else
		return telldir(m_dir);
#endif
	}

	XdevLString XdevLDirectoryUnix::getWorkingDirectory() {
		if(getcwd(m_workingDirectoryBuffer, 512) != m_workingDirectoryBuffer) {
			return XdevLString("");
		}
		XdevLString tmp(m_workingDirectoryBuffer);

		return tmp;
	}

	xdl_uint64 XdevLDirectoryUnix::getNumberOfEntities() {
		xdl_uint64 numberOfEntities = 0;

		if(nullptr == m_dir) {
			return numberOfEntities;
		}

		int ret = 0;
		do {
			XdevLDirectoryEntity ent;
			ret = getEntity(ent);
			if(ret != RET_FAILED) {
				numberOfEntities++;
			}
		} while(ret != RET_FAILED);

		// Rewind back the pointer to the begining of the list.
		rewind();

		return numberOfEntities;
	}

	xdl_int XdevLDirectoryUnix::getEntity(XdevLDirectoryEntity& entity) {
		struct dirent* ent = nullptr;

		// Read one item from the directory.
		ent = readdir(m_dir);

		if(ent == nullptr) {
			// TODO How to deal with the error EBADF?
			if(errno == EBADF) {
				return RET_FAILED;
			}
			return RET_FAILED;
		}

		// Extract the type of item. (File? Folder?)
		XdevLDirectoryEntity::Type type = extractType(ent);

		// Assign the new entity to the variable.
		entity = XdevLDirectoryEntity(ent->d_name, type);

		return RET_SUCCESS;
	}

	std::vector<XdevLDirectoryEntity> XdevLDirectoryUnix::getEntities() {
		// If we do not have an open directory, return empty.
		if(nullptr == m_dir) {
			return std::vector<XdevLDirectoryEntity>();
		}

		std::vector<XdevLDirectoryEntity>	m_entity_list;

		int ret = 0;
		do {
			XdevLDirectoryEntity ent;
			ret = getEntity(ent);
			if(ret != RET_FAILED) {
				m_entity_list.push_back(ent);
			}
		} while(ret != RET_FAILED);

		// Rewind back the pointer to the begining of the list.
		rewind();

		return m_entity_list;
	}

	xdl_bool XdevLDirectoryUnix::find(const XdevLString& name, XdevLDirectoryEntity& entity) {

		std::vector<XdevLDirectoryEntity> entities = getEntities();
		if(entities.size() == 0) {
			entity = XdevLDirectoryEntity();
			return xdl_false;
		}
		std::vector<XdevLDirectoryEntity>::iterator it = std::find(entities.begin(), entities.end(), name);
		if(it == entities.end()) {
			return xdl_false;
		}
		entity = *it;

		// Rewind back the pointer to the begining of the list.
		rewind();

		return xdl_true;
	}

	std::vector<XdevLString> XdevLDirectoryUnix::getAllDirectories(const XdevLString& directoryName, xdl_bool recursive) {
		std::vector<XdevLString> tmp;
		getDirectoryList(tmp, directoryName, recursive);
		return tmp;
	}

	void XdevLDirectoryUnix::getDirectoryList(std::vector<XdevLString>& folderList, const XdevLString& currentDirectoryName, xdl_bool recursive) {
		close();

		if(open(currentDirectoryName) == RET_FAILED) {
			return;
		}

		std::vector<XdevLDirectoryEntity> folderEntities = getEntities();
		if(folderEntities.size() == 0) {
			return;
		}

		for(auto& entity : folderEntities) {

			if((entity.getType() != XdevLDirectoryEntity::Type::DIRECTORY) ||
			        (entity.getName() == XdevLString(".")) ||
			        (entity.getName() == XdevLString(".."))) {
				continue;
			}

			XdevLString tmp = currentDirectoryName + XdevLString(TEXT("/")) + entity.getName();
			folderList.push_back(tmp);
			if(recursive) {
				getDirectoryList(folderList, tmp, recursive);
			}
		}
	}

	std::vector<XdevLString>  XdevLDirectoryUnix::getAllFiles(const XdevLString& directoryName, xdl_bool recursive, const XdevLString& pattern) {
		std::vector<XdevLString> tmp;
		getFileList(tmp, directoryName, recursive, pattern);
		return tmp;
	}

	void  XdevLDirectoryUnix::getFileList(std::vector<XdevLString>& fileList, const XdevLString& currentDirectoryName, xdl_bool recursive, const XdevLString& pattern) {
		close();

		if(open(currentDirectoryName) == RET_FAILED) {
			return;
		}

		std::vector<XdevLDirectoryEntity> folderEntities = getEntities();
		if(folderEntities.size() == 0) {
			return;
		}

		for(auto& entity : folderEntities) {
			// TODO Maybe this goes wrong for a Unknown item?
			XdevLString tmp = currentDirectoryName + XdevLString(TEXT("/"));

			if(entity.isRegular()) {
				auto result = entity.getName().find(pattern);
				// TODO the comparison with -1 should be npos but for now we use this hack.
				if(result != (size_t)(-1)) {
					XdevLString filePath = tmp + entity.getName();
					fileList.push_back(filePath);
				}
			} else if(recursive &&
			          entity.isDirectory() &&
			          (entity.getName() != XdevLString(".")) &&
			          (entity.getName() != XdevLString(".."))) {
				XdevLString folder = tmp + entity.getName();
				getFileList(fileList, folder, recursive, pattern);
			}
		}
	}


	std::vector<XdevLString>  XdevLDirectoryUnix::getAllDirectoriesContainsFile(const XdevLString& directoryName, xdl_bool recursive, const XdevLString& pattern) {
		std::vector<XdevLString> tmp;
		getDirectoryContainsFileList(tmp, directoryName, recursive, pattern);
		return tmp;
	}

	void XdevLDirectoryUnix::getDirectoryContainsFileList(std::vector<XdevLString>& directoryList, const XdevLString& currentDirectoryName, xdl_bool recursive, const XdevLString& pattern) {
		close();

		if(open(currentDirectoryName) == RET_FAILED) {
			return;
		}

		std::vector<XdevLDirectoryEntity> folderEntities = getEntities();
		if(folderEntities.size() == 0) {
			return;
		}
		xdl_bool addFolderNames = xdl_true;
		for(auto& entity : folderEntities) {

			if((entity.getType() == XdevLDirectoryEntity::Type::REGULAR) && addFolderNames) {
				auto result = entity.getName().find(pattern);
				// TODO the comparison with -1 should be npos but for now we use this hack.
				if(result != (size_t)(-1)) {
					directoryList.push_back(currentDirectoryName);
					addFolderNames = false;
				}
			} else if(recursive &&
			          (entity.getType() == XdevLDirectoryEntity::Type::DIRECTORY) &&
			          (entity.getName() != XdevLString(".")) &&
			          (entity.getName() != XdevLString(".."))) {
				XdevLString recDir = currentDirectoryName;
				recDir += XdevLString(TEXT("/"));
				recDir += entity.getName();
				getDirectoryContainsFileList(directoryList, recDir, recursive, pattern);
			}
		}
	}



}
