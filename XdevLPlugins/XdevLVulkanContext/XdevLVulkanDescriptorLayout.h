/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_DESCRIPTOR_LAYOUT_H
#define XDEVL_VULKAN_DESCRIPTOR_LAYOUT_H

#include <XdevLVulkanContext/XdevLVulkanContextFwd.h>

namespace xdl {

	/**
	 * @class XdevLVulkanDescriptorLayout
	 * @brief Interface for the Vulkan Descriptor Layout handling.
	 * @author Cengiz Terzibas
	 */
	class XdevLVulkanDescriptorLayout {
		public:
			virtual ~XdevLVulkanDescriptorLayout() {}

			virtual xdl_int create() = 0;
			virtual xdl_int create(VkDevice device) = 0;
			virtual xdl_int addBinding(xdl::xdl_uint32 binding, VkDescriptorType descriptorType, VkShaderStageFlags shaderStageFlags) = 0;
			virtual VkDescriptorSetLayout getNativeHandle() const = 0;
			virtual const VkDescriptorSetLayout* getNativeHandlePtr() const = 0;

	};

}


#endif
