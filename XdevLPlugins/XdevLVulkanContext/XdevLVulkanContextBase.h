/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_CONTEXT_BASE_H
#define XDEVL_VULKAN_CONTEXT_BASE_H

#include <XdevLVulkanContext/XdevLVulkanContextFwd.h>

namespace xdl {

	/**
		@class XdevLVulkanContextBase
		@brief Base class of the Vulkan Context class.
	*/
	class XdevLVulkanContextBase : public XdevLModuleAutoImpl<XdevLVulkanContext> {

		public:
			XdevLVulkanContextBase(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
				XdevLModuleAutoImpl(parameter, descriptor),
				m_debugMode(xdl_false) {

			}

			virtual ~XdevLVulkanContextBase() {};

			xdl_int init() override final {
				if(getMediator() == nullptr) {
					return RET_SUCCESS;
				}

				auto coreParameter = getMediator()->getCoreParameter();
				if(coreParameter.xmlBuffer.size() != 0) {
					TiXmlDocument xmlDocument;
					if(!xmlDocument.Parse((xdl_char*)coreParameter.xmlBuffer.data())) {
						XDEVL_MODULE_ERROR("Could not parse xml buffer.\n ");
						return RET_FAILED;
					}

					if(readModuleInformation(&xmlDocument) != RET_SUCCESS) {
						return RET_FAILED;
					}
				}

				return RET_SUCCESS;
			}

			xdl_int readModuleInformation(TiXmlDocument* document) {
				TiXmlHandle docHandle(document);
				TiXmlElement* root = docHandle.FirstChild(XdevLCorePropertiesName.toString().c_str()).FirstChildElement("XdevLOpenGLContext").ToElement();

				if(nullptr == root) {
					XDEVL_MODULE_INFO("<XdevLOpenGLContext> section not found, skipping proccess.\n");
					return RET_SUCCESS;
				}

				while(root != nullptr) {
					if(root->Attribute("id")) {
						XdevLID id(root->Attribute("id"));
						if(getID() == id) {
							if(root->Attribute("debug")) {
								m_debugMode = xstd::from_string<xdl_bool>(root->Attribute("debug"));
								XDEVL_MODULE_INFO("Debug Mode                    : " << (m_debugMode ? "On" : "Off") << std::endl);
							}
						}
					} else {
						XDEVL_MODULE_ERROR("No 'id' attribute specified. Using default values for the device\n");
					}
					root = root->NextSiblingElement();
				}
				return RET_SUCCESS;
			}
			xdl_bool getDebugMode() {
				return m_debugMode;
			}
		protected:
			xdl_bool m_debugMode;
	};

}


#endif
