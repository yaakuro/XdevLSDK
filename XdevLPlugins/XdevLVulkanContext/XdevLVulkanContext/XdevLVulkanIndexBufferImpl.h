/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_INDEX_BUFFER_IMPL_H
#define XDEVL_VULKAN_INDEX_BUFFER_IMPL_H

#include "XdevLVulkanBufferBaseImpl.h"

namespace xdl {

	/**
	* @class XdevLVulkanIndexBufferImpl
	* @brief Implementation of the XdevLVulkanIndexBuffer interface.
	* @author Cengiz Terzibas
	*/
	class XdevLVulkanIndexBufferImpl : public XdevLVulkanBufferBaseImpl<XdevLVulkanIndexBuffer, VK_BUFFER_USAGE_INDEX_BUFFER_BIT> {
		public:
			XdevLVulkanIndexBufferImpl(VkDevice device, VkPhysicalDeviceMemoryProperties memoryProperties)
				: XdevLVulkanBufferBaseImpl<XdevLVulkanIndexBuffer, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>(device, memoryProperties)
				, m_numberOfIndices(0) {
			}

			virtual ~XdevLVulkanIndexBufferImpl() {};

			xdl_int init(xdl_uint8* src, xdl_uint size, xdl_uint elements) override final {
				m_numberOfIndices = elements;
				return XdevLVulkanBufferBaseImpl<XdevLVulkanIndexBuffer, VK_BUFFER_USAGE_INDEX_BUFFER_BIT>::init(src, size);
			}

			xdl_uint32 getNumberOfIndices() const override final {
				return m_numberOfIndices;
			}

		private:

			xdl_uint32 m_numberOfIndices;
	};

}

#endif
