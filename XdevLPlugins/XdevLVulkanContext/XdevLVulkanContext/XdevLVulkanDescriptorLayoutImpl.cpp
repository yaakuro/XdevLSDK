/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/


#include "XdevLVulkanDescriptorLayoutImpl.h"

namespace xdl {

	XdevLVulkanDescriptorLayoutImpl::XdevLVulkanDescriptorLayoutImpl()
		: m_deviceImpl(nullptr)
		, m_device(VK_NULL_HANDLE)
		, m_descriptorSetLayout(VK_NULL_HANDLE) {

	}

	XdevLVulkanDescriptorLayoutImpl::XdevLVulkanDescriptorLayoutImpl(XdevLVulkanDeviceImpl* deviceImpl)
		: m_deviceImpl(deviceImpl)
		, m_device(deviceImpl->getNativeHandle())
		, m_descriptorSetLayout(VK_NULL_HANDLE) {

	}

	XdevLVulkanDescriptorLayoutImpl::~XdevLVulkanDescriptorLayoutImpl() {
		if(VK_NULL_HANDLE != m_descriptorSetLayout) {
			vkDestroyDescriptorSetLayout(m_device, m_descriptorSetLayout, nullptr);
		}
	}

	xdl_int XdevLVulkanDescriptorLayoutImpl::create() {

		VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo {};
		descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		descriptorSetLayoutCreateInfo.bindingCount = (uint32_t)m_descriptorSetLayoutBindings.size();
		descriptorSetLayoutCreateInfo.pBindings = m_descriptorSetLayoutBindings.data();
		auto result = vkCreateDescriptorSetLayout(m_device, &descriptorSetLayoutCreateInfo, NULL, &m_descriptorSetLayout);
		if(VK_SUCCESS != result) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanDescriptorLayoutImpl::create(VkDevice device) {
		m_device = device;
		return create();
	}


	xdl_int XdevLVulkanDescriptorLayoutImpl::addBinding(xdl::xdl_uint32 binding, VkDescriptorType descriptorType, VkShaderStageFlags shaderStageFlags) {

		VkDescriptorSetLayoutBinding descriptorSetLayoutBinding {};
		descriptorSetLayoutBinding.binding = binding;
		descriptorSetLayoutBinding.descriptorType = descriptorType;
		descriptorSetLayoutBinding.stageFlags = shaderStageFlags;
		descriptorSetLayoutBinding.descriptorCount = 1;

		m_descriptorSetLayoutBindings.push_back(descriptorSetLayoutBinding);

		return RET_SUCCESS;
	}

	VkDescriptorSetLayout XdevLVulkanDescriptorLayoutImpl::getNativeHandle() const {
		return m_descriptorSetLayout;
	}

	const VkDescriptorSetLayout* XdevLVulkanDescriptorLayoutImpl::getNativeHandlePtr() const {
		return &m_descriptorSetLayout;
	}

}
