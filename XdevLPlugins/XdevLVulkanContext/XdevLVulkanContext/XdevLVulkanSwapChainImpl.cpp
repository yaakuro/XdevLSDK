/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLVulkanSwapChainImpl.h"
#include "XdevLVulkanFrameBufferImpl.h"

#include <array>
#include <vector>
#include <algorithm>

namespace xdl {

	xdl_bool XdevLVulkanSwapChainImpl::m_connectFunctions = xdl_false;

	XdevLVulkanSwapChainImpl::XdevLVulkanSwapChainImpl()
		: m_instanceImpl(nullptr)
		, m_surfaceImpl(nullptr)
		, m_device(VK_NULL_HANDLE)
		, m_renderPass(VK_NULL_HANDLE)
		, m_swapChain(VK_NULL_HANDLE)
		, m_swapChainOld(VK_NULL_HANDLE)
		, m_currentBuffer(0) {
	}

	XdevLVulkanSwapChainImpl::XdevLVulkanSwapChainImpl(XdevLVulkanInstanceImpl* instance, IPXdevLVulkanDevice device, XdevLVulkanSurfaceImpl* surface)
		: m_instanceImpl(instance)
		, m_surfaceImpl(surface)
		, m_device(VK_NULL_HANDLE)
		, m_renderPass(VK_NULL_HANDLE)
		, m_swapChain(VK_NULL_HANDLE)
		, m_swapChainOld(VK_NULL_HANDLE)
		, m_depthStencilImage {}
	, m_currentBuffer(0) {
		m_device = (VkDevice)device->getNativeHandle();
		m_deviceImpl = static_cast<XdevLVulkanDeviceImpl*>(device.get());
	}

	XdevLVulkanSwapChainImpl::~XdevLVulkanSwapChainImpl() {

		// Assuming that if .memory is allocated we can destroy all of it.
		if(m_depthStencilImage.memory) {
			vkFreeMemory(m_device, m_depthStencilImage.memory, nullptr);
			vkDestroyImage(m_device, m_depthStencilImage.image, nullptr);
			vkDestroyImageView(m_device, m_depthStencilImage.imageView, nullptr);
		}

		for(const auto& framebuffer : m_frameBuffersImpl) {
			delete framebuffer;
		}
		m_frameBuffersImpl.clear();

		for(const auto& swapChainImage : m_swapChainImages) {
			vkDestroyImageView(m_device, swapChainImage.imageView, nullptr);
		}

		if(VK_NULL_HANDLE != m_swapChain) {
			vkDestroySwapchainKHR(m_device, m_swapChain, nullptr);
			m_swapChain = VK_NULL_HANDLE;
		}

		if(VK_NULL_HANDLE != m_renderPass) {
			vkDestroyRenderPass(m_device, m_renderPass, nullptr);
		}
	}

	void XdevLVulkanSwapChainImpl::connectFunctions(VkDevice device) {
		GET_DEVICE_PROC_ADDR(device, CreateSwapchainKHR);
		GET_DEVICE_PROC_ADDR(device, DestroySwapchainKHR);
		GET_DEVICE_PROC_ADDR(device, GetSwapchainImagesKHR);
		GET_DEVICE_PROC_ADDR(device, AcquireNextImageKHR);
		GET_DEVICE_PROC_ADDR(device, QueuePresentKHR);
		m_connectFunctions = xdl_true;
	}

	xdl_int XdevLVulkanSwapChainImpl::create() {

		auto surfaceCaps = m_surfaceImpl->getSurfaceCapabilities();
		if(create(m_instanceImpl->getNativeHandle(), m_instanceImpl->getPhysicalDevice(), m_device,  m_surfaceImpl->getNativeHandle(), m_surfaceImpl->getColorFormat(), m_deviceImpl->getSupportedDepthStencilFormat(), m_surfaceImpl->getColorSpace(), surfaceCaps, m_instanceImpl->getQueueFamilyIndices()) != RET_SUCCESS) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanSwapChainImpl::create(VkInstance instance,
	        VkPhysicalDevice physicalDevice,
	        VkDevice device,
	        VkSurfaceKHR surface,
	        VkFormat imageFormat,
	        VkFormat depthStencilFormat,
	        VkColorSpaceKHR colorSpace,
	        const VkSurfaceCapabilitiesKHR& surfaceCapabilities,
	        const IPXdevLQueueFamilyIndices& queueFamilyIndices) {

		if(!m_connectFunctions) {
			connectFunctions(device);
		}
		m_instance = instance;
		m_physicalDevice = physicalDevice;
		m_device = device;
		m_surface = surface;
		m_imageFormat = imageFormat;
		m_colorSpace = colorSpace;
		m_depthStencilFormat = depthStencilFormat;


		m_swapChainOld = m_swapChain;
		uint32_t desiredNumberOfSwapchainImages = surfaceCapabilities.minImageCount + 1;
		if ((surfaceCapabilities.maxImageCount > 0) && (desiredNumberOfSwapchainImages > surfaceCapabilities.maxImageCount)) {
			desiredNumberOfSwapchainImages = surfaceCapabilities.maxImageCount;
		}


		VkSurfaceTransformFlagsKHR preTransform {};
		if(surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
			preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		} else {
			preTransform = surfaceCapabilities.currentTransform;
		}

		//
		// Get the extend of the Surface.
		//
		if (surfaceCapabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
			m_extent2D = surfaceCapabilities.currentExtent;
		} else {
			VkExtent2D actualExtent = {m_surfaceImpl->getExtend().width, m_surfaceImpl->getExtend().height};

			actualExtent.width = std::max(surfaceCapabilities.minImageExtent.width, std::min(surfaceCapabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(surfaceCapabilities.minImageExtent.height, std::min(surfaceCapabilities.maxImageExtent.height, actualExtent.height));

			m_extent2D = actualExtent;
		}

		VkSwapchainCreateInfoKHR swapChainCreateInfo {};
		swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapChainCreateInfo.surface = m_surface;
		swapChainCreateInfo.minImageCount = desiredNumberOfSwapchainImages;

		swapChainCreateInfo.imageArrayLayers = 1;
		swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		swapChainCreateInfo.imageFormat = m_imageFormat;
		swapChainCreateInfo.imageColorSpace = m_colorSpace;
		swapChainCreateInfo.imageExtent = m_extent2D;

		swapChainCreateInfo.preTransform = (VkSurfaceTransformFlagBitsKHR)preTransform;

		std::vector<uint32_t> queueFamilyIndicesList;
		if(queueFamilyIndices->getGraphicsQueueIndex() != UINT32_MAX) {
			queueFamilyIndicesList.push_back((uint32_t) queueFamilyIndices->getGraphicsQueueIndex());
		}
		if(queueFamilyIndices->getPresentQueueIndex() != UINT32_MAX) {
			queueFamilyIndicesList.push_back((uint32_t) queueFamilyIndices->getPresentQueueIndex());
		}

		if (queueFamilyIndices->getGraphicsQueueIndex() != (queueFamilyIndices->getPresentQueueIndex() != UINT32_MAX)) {
			swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			swapChainCreateInfo.queueFamilyIndexCount = queueFamilyIndicesList.size();
			swapChainCreateInfo.pQueueFamilyIndices = queueFamilyIndicesList.data();
		} else {
			swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		}

		swapChainCreateInfo.presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
		swapChainCreateInfo.oldSwapchain = m_swapChainOld;
		swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

		if(VK_NULL_HANDLE != m_swapChainOld) {
			vkDestroySwapchainKHR(device, m_swapChainOld, nullptr);
		}

		VkResult result = vkCreateSwapchainKHR(device, &swapChainCreateInfo, nullptr, &m_swapChain);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSwapChainImpl, "vkCreateSwapchainKHR failed: " << vkVkResultToString(result) << std::endl);
			return result;
		}

		//
		// Now let's get the number of available images from the swapchain.
		//
		uint32_t imageCount;
		result = vkGetSwapchainImagesKHR(device, m_swapChain, &imageCount, nullptr);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSwapChainImpl, "vkGetSwapchainImagesKHR failed: " << vkVkResultToString(result) << std::endl);
			return result;
		}

		std::vector<VkImage> images(imageCount);
		result = vkGetSwapchainImagesKHR(device, m_swapChain, &imageCount, images.data());
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSwapChainImpl, "vkGetSwapchainImagesKHR failed: " << vkVkResultToString(result) << std::endl);
			return result;
		}

		//
		// Now let's create the VkImageView for the VkImage's.
		//
		m_swapChainImages.reserve(imageCount);
		m_swapChainImages.resize(imageCount);

		for(size_t i = 0; i < images.size(); i++) {

			m_swapChainImages[i].image = images[i];

			VkImageViewCreateInfo imageViewCreateInfo {};
			imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewCreateInfo.image = images[i];
			imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			imageViewCreateInfo.format = swapChainCreateInfo.imageFormat;
			imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
			imageViewCreateInfo.subresourceRange.levelCount = 1;
			imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
			imageViewCreateInfo.subresourceRange.layerCount = 1;

			result = vkCreateImageView(device, &imageViewCreateInfo, nullptr, &m_swapChainImages[i].imageView);
			if(VK_SUCCESS != result) {
				XDEVL_MODULEX_ERROR(XdevLVulkanSwapChainImpl, "vkCreateImageView failed: " << vkVkResultToString(result) << std::endl);
				return result;
			}
		}
		vkGetPhysicalDeviceMemoryProperties(m_physicalDevice, &m_physicalDeviceMemoryProperties);
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanSwapChainImpl::acquireNextImage(VkImage& image, VkFramebuffer& framebuffer, VkSemaphore presentSemaphore) {

		VkResult result = vkAcquireNextImageKHR(m_device, m_swapChain, UINT64_MAX, presentSemaphore, VK_NULL_HANDLE, &m_currentBuffer);
		if(VK_SUCCESS == result || result == VK_SUBOPTIMAL_KHR) {
			image = m_swapChainImages[m_currentBuffer].image;
			framebuffer = m_frameBuffers[m_currentBuffer];
		} else {
			image = VK_NULL_HANDLE;
		}
		return result;
	}

	std::vector<VkFramebuffer> XdevLVulkanSwapChainImpl::getSwapChainFramebuffers() const {
		return m_frameBuffers;
	}

	VkRenderPass XdevLVulkanSwapChainImpl::getRenderPass() const {
		return m_renderPass;
	}

	VkExtent2D XdevLVulkanSwapChainImpl::getExtent2D() const {
		return m_extent2D;
	}

	VkSwapchainKHR XdevLVulkanSwapChainImpl::getNativeHandle() const {
		return m_swapChain;
	}

	const VkSwapchainKHR* XdevLVulkanSwapChainImpl::getNativeHandlePtr() const {
		return &m_swapChain;
	}

	const std::vector<XdevLVulkanImage>& XdevLVulkanSwapChainImpl::getSwapChainImages() const {
		return m_swapChainImages;
	}

	xdl_int XdevLVulkanSwapChainImpl::createFramebuffers() {

		setupRenderPass();
		setupDepthStencil();

		return createFramebuffers(m_renderPass);
	}

	xdl_int XdevLVulkanSwapChainImpl::createFramebuffers(VkRenderPass renderPass) {

		m_frameBuffers.reserve(m_swapChainImages.size());
		m_frameBuffers.resize(m_swapChainImages.size());

		for(size_t idx = 0; idx < m_swapChainImages.size(); idx++) {

			XdevLVulkanFrameBufferImpl* frameBufferImpl = nullptr;
			if(nullptr != m_instanceImpl) {
				frameBufferImpl = new XdevLVulkanFrameBufferImpl(m_instanceImpl, m_deviceImpl);
			} else {
				frameBufferImpl = new XdevLVulkanFrameBufferImpl(m_instance, m_physicalDevice, m_device);
			}

			frameBufferImpl->setRenderPass(renderPass);
			frameBufferImpl->addColorTarget(0, m_imageFormat, m_swapChainImages[idx]);
			frameBufferImpl->addDepthStencil(m_depthStencilFormat, m_depthStencilImage);
			if(frameBufferImpl->create(m_extent2D.width, m_extent2D.height) != RET_SUCCESS) {
				return RET_FAILED;
			}

			m_frameBuffersImpl.push_back(frameBufferImpl);
			m_frameBuffers[idx] = frameBufferImpl->getNativeHandle();

		}
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanSwapChainImpl::swapBuffer(VkQueue queue, VkSemaphore renderSemaphore) {
		//
		// When ready present the framebuffer.
		//
		VkPresentInfoKHR present {};
		present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		present.swapchainCount = 1;
		present.waitSemaphoreCount = 1;
		present.pWaitSemaphores = &renderSemaphore;
		present.pSwapchains = &m_swapChain;
		present.pImageIndices = &m_currentBuffer;

		VkResult result = vkQueuePresentKHR(queue, &present);
		if (result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR) {
			result = vkQueueWaitIdle(queue);
		}
		return result;
	}

	void XdevLVulkanSwapChainImpl::setupDepthStencil() {

		VkImageCreateInfo image {};
		image.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		image.imageType = VK_IMAGE_TYPE_2D;
		image.format = m_depthStencilFormat;
		image.extent = { m_extent2D.width, m_extent2D.height, 1 };
		image.mipLevels = 1;
		image.arrayLayers = 1;
		image.samples = VK_SAMPLE_COUNT_1_BIT;
		image.tiling = VK_IMAGE_TILING_OPTIMAL;
		image.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		image.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		VK_CHECK_RESULT(vkCreateImage(m_device, &image, nullptr, &m_depthStencilImage.image));

		// Allocate memory for the image (device local) and bind it to our image
		VkMemoryAllocateInfo memAlloc {};
		memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		VkMemoryRequirements memReqs;
		vkGetImageMemoryRequirements(m_device, m_depthStencilImage.image, &memReqs);
		memAlloc.allocationSize = memReqs.size;
		memAlloc.memoryTypeIndex = xdl::getMemoryTypeIndex(m_physicalDeviceMemoryProperties, memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		VK_CHECK_RESULT(vkAllocateMemory(m_device, &memAlloc, nullptr, &m_depthStencilImage.memory));
		VK_CHECK_RESULT(vkBindImageMemory(m_device, m_depthStencilImage.image, m_depthStencilImage.memory, 0));

		VkImageViewCreateInfo depthStencilView {};
		depthStencilView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		depthStencilView.viewType = VK_IMAGE_VIEW_TYPE_2D;
		depthStencilView.format = m_depthStencilFormat;
		depthStencilView.subresourceRange = {};
		depthStencilView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
		depthStencilView.subresourceRange.baseMipLevel = 0;
		depthStencilView.subresourceRange.levelCount = 1;
		depthStencilView.subresourceRange.baseArrayLayer = 0;
		depthStencilView.subresourceRange.layerCount = 1;
		depthStencilView.image = m_depthStencilImage.image;

		VK_CHECK_RESULT(vkCreateImageView(m_device, &depthStencilView, nullptr, &m_depthStencilImage.imageView));
	}

	void XdevLVulkanSwapChainImpl::setupRenderPass() {

		// -------------------------------------------------------------------------
		// Create Render Pass.
		//

		VkAttachmentDescription colorAttachmentDescription {};
		colorAttachmentDescription.format = m_imageFormat;
		colorAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentDescription depthStencilAttachmentDescription {};
		depthStencilAttachmentDescription.format = m_depthStencilFormat;
		depthStencilAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
		depthStencilAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthStencilAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthStencilAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depthStencilAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthStencilAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		depthStencilAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference colorReference {};
		colorReference.attachment = 0;
		colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkAttachmentReference depthReference = {};
		depthReference.attachment = 1;
		depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;


		std::array<VkSubpassDependency, 1> dependencies = {};

		// First dependency at the start of the renderpass
		// Does the transition from final to initial layout
		dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
		dependencies[0].dstSubpass = 0;
		dependencies[0].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[0].srcAccessMask = 0;
		dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		VkSubpassDescription subpassDescription {};
		subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpassDescription.colorAttachmentCount = 1;
		subpassDescription.pColorAttachments = &colorReference;
		subpassDescription.pDepthStencilAttachment = &depthReference;

		std::vector<VkAttachmentDescription> attachments;
		attachments.push_back(colorAttachmentDescription);
		attachments.push_back(depthStencilAttachmentDescription);

		VkRenderPassCreateInfo renderPassCreateInfo {};
		renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassCreateInfo.attachmentCount = (uint32_t)attachments.size();
		renderPassCreateInfo.pAttachments = attachments.data();
		renderPassCreateInfo.subpassCount = 1;
		renderPassCreateInfo.pSubpasses = &subpassDescription;
		renderPassCreateInfo.dependencyCount = (uint32_t)dependencies.size();
		renderPassCreateInfo.pDependencies = dependencies.data();

		VK_CHECK_RESULT(vkCreateRenderPass(m_device, &renderPassCreateInfo, nullptr, &m_renderPass));

		// -------------------------------------------------------------------------
	}

}
