/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_DEBUG_IMPL_H
#define XDEVL_VULKAN_DEBUG_IMPL_H

#include <XdevLVulkanContext/XdevLVulkanContext.h>

namespace xdl {

	/**
	 * @class XdevLVulkanDebugImpl
	 * @brief Helper class for enable Vulkan debugging.
	 * @author Cengiz Terzibas
	 */
	class XdevLVulkanDebugImpl {
		public:
			XdevLVulkanDebugImpl(VkInstance instance);

			virtual ~XdevLVulkanDebugImpl();

			VkResult init();

			static VKAPI_ATTR VkBool32 VKAPI_CALL myDebugReportCallback(VkDebugReportFlagsEXT flags,
			        VkDebugReportObjectTypeEXT objectType,
			        uint64_t object,
			        size_t location,
			        int32_t messageCode,
			        const char* pLayerPrefix,
			        const char* pMessage,
			        void* pUserData);

			operator VkDebugReportCallbackEXT() {
				return m_callback;
			}

		private:
			VkInstance m_instance;
			VkDebugReportCallbackEXT m_callback;

			PFN_vkCreateDebugReportCallbackEXT fpCreateDebugReportCallbackEXT;
			PFN_vkDebugReportMessageEXT fpDebugReportMessageEXT;
			PFN_vkDestroyDebugReportCallbackEXT fpDestroyDebugReportCallbackEXT;
	};
}


#endif
