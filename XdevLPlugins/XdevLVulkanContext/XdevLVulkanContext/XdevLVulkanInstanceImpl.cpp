/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLVulkanInstanceImpl.h"
#include "XdevLVulkanDeviceImpl.h"
#include "XdevLVulkanSurfaceImpl.h"

namespace xdl {


	XdevLQueueFamilyIndicesImpl::XdevLQueueFamilyIndicesImpl()
		: graphicsQueueIndex(UINT32_MAX)
		, presentQueueIndex(UINT32_MAX) {

	}

	XdevLQueueFamilyIndicesImpl::~XdevLQueueFamilyIndicesImpl() {

	}

	xdl_uint32 XdevLQueueFamilyIndicesImpl::getNumberOfQueues() const  {
		return numberOfQueues;
	}

	xdl_uint32 XdevLQueueFamilyIndicesImpl::getGraphicsQueueIndex() const  {
		return graphicsQueueIndex;
	}

	xdl_uint32 XdevLQueueFamilyIndicesImpl::getPresentQueueIndex() const  {
		return presentQueueIndex;
	}

	void XdevLQueueFamilyIndicesImpl::setNumberOfQueues(xdl_uint32 nQueues) {
		numberOfQueues = nQueues;
	}

	void XdevLQueueFamilyIndicesImpl::setGraphicsQueueIndex(xdl_uint32 index) {
		graphicsQueueIndex = index;
	}

	void XdevLQueueFamilyIndicesImpl::setPresentQueueIndex(xdl_uint32 index) {
		presentQueueIndex = index;
	}

	xdl_bool XdevLQueueFamilyIndicesImpl::hasGraphicsQueueIndex() const {
		return (UINT32_MAX != graphicsQueueIndex);
	}

	xdl_bool XdevLQueueFamilyIndicesImpl::hasPresentQueueIndex() const {
		return (UINT32_MAX != presentQueueIndex);
	}

	//
	//
	//

	XdevLVulkanInstanceImpl::XdevLVulkanInstanceImpl()
		: m_instance(VK_NULL_HANDLE)
		, m_physicalDevice(VK_NULL_HANDLE)
		, m_resetInteralExtensions(xdl_false)
		, m_debug(nullptr)
		, m_deviceProperties {}
	, m_deviceFeatures {}
	, m_physicalDeviceMemoryProperties {} {
		m_queueFamilyIndices = std::make_shared<XdevLQueueFamilyIndicesImpl>();
	}

	XdevLVulkanInstanceImpl::~XdevLVulkanInstanceImpl() {

		if(m_debug) {
			m_debug = nullptr;
			m_debug = nullptr;
		}

		if(VK_NULL_HANDLE != m_instance) {
			vkDestroyInstance(m_instance, nullptr);
			m_instance = VK_NULL_HANDLE;
		}

		m_resetInteralExtensions = xdl_false;
		m_physicalDevice = VK_NULL_HANDLE;

		for(auto& extension : m_passedExtensions) {
			delete [] extension;
		}
	}

	VkPhysicalDevice XdevLVulkanInstanceImpl::getPhysicalDevice() const {
		return m_physicalDevice;
	}

	void XdevLVulkanInstanceImpl::addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal) {
		m_resetInteralExtensions = resetInternal;

		char* tmp = nullptr;
		for(auto& extension : extensionsToEnable) {
			auto str = extension.toString();
			tmp = new char[str.length() + 1];
			strcpy(tmp, str.c_str());
			tmp[str.length()] = '\0';
			m_passedExtensions.push_back(tmp);
		}
	}

	xdl_int XdevLVulkanInstanceImpl::create(xdl_bool enableLayers) {
		const VkApplicationInfo applicationInfo {
			VK_STRUCTURE_TYPE_APPLICATION_INFO,
			nullptr,
			"XdevLVulkanApp",
			0,
			"XdevLEngine",
			0,
			VK_MAKE_VERSION(1, 0, 1)
		};
		return create(applicationInfo, enableLayers);
	}

	xdl_int XdevLVulkanInstanceImpl::create(const VkApplicationInfo& applicationInfo, xdl_bool enableLayers) {

		// ---------------------------------------------------------------------
		// Let's prepare the list which instance extensions we like to use.
		//

		//
		// Let's check the supported instance extensions.
		//
		uint32_t propertyCount = 0;
		VkResult result = vkEnumerateInstanceExtensionProperties(nullptr, &propertyCount, nullptr);
		if(VK_SUCCESS != result) {
			std::cerr << "vkEnumerateInstanceExtensionProperties failed: " << vkVkResultToString(result) << std::endl;
			return result;
		}

		std::vector<VkExtensionProperties> extensionsSupported(propertyCount);
		vkEnumerateInstanceExtensionProperties(nullptr, &propertyCount, extensionsSupported.data());
		XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--- Supported Instance Extensions --------------------\n");
		for(auto extension : extensionsSupported) {
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Name: " << extension.extensionName << " Version: " << extension.specVersion << "\n");
		}
		XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--------------------------------------------------\n");

		std::vector<const char*> enabledExtensions = { VK_KHR_SURFACE_EXTENSION_NAME };
#if defined(XDEVL_PLATFORM_WINDOWS)
		enabledExtensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#elif defined(__ANDROID__)
		enabledExtensions.push_back(VK_KHR_ANDROID_SURFACE_EXTENSION_NAME);
#elif defined(VKF_SURFACE_XCB)
		enabledExtensions.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
#elif defined(VKF_SURFACE_XLIB)
		enabledExtensions.push_back(VK_KHR_XLIB_SURFACE_EXTENSION_NAME);
#elif VKF_SURFACE_WAYLAND
		enabledExtensions.push_back(VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME);
#elif VKF_SURFACE_MIR
		enabledExtensions.push_back(VK_KHR_MIR_SURFACE_EXTENSION_NAME);
#endif
		enabledExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);

		if(m_passedExtensions.size()) {
			if(m_resetInteralExtensions) {
				enabledExtensions.clear();
			}
			enabledExtensions.insert(enabledExtensions.end(), m_passedExtensions.begin(), m_passedExtensions.end());
		}

		//
		// Let's check the supported instance layers.
		//
		uint32_t layerPropertyCount = 0;
		result = vkEnumerateInstanceLayerProperties(&layerPropertyCount, nullptr);
		if(VK_SUCCESS != result) {
			std::cerr << "vkEnumerateInstanceExtensionProperties failed: " << vkVkResultToString(result) << std::endl;
			return result;
		}

		std::vector<VkLayerProperties> layersSupported(layerPropertyCount);
		vkEnumerateInstanceLayerProperties(&layerPropertyCount, layersSupported.data());

		// ---------------------------------------------------------------------
		// Here we add layers we like to activate only if supported.
		//

		std::vector<const char*> layerExtensionsWeLikeToUse;
		if(enableLayers) {
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--- Supported Instance Layers ------------------------\n");
			for(auto layer : layersSupported) {
				if(strcmp("VK_LAYER_LUNARG_standard_validation", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_LUNARG_standard_validation");
				} else if(strcmp("VK_LAYER_GOOGLE_threading", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_GOOGLE_threading");
				} else if(strcmp("VK_LAYER_LUNARG_parameter_validation", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_LUNARG_parameter_validation");
				} else if(strcmp("VK_LAYER_LUNARG_device_limits", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_LUNARG_device_limits");
				} else if(strcmp("VK_LAYER_LUNARG_object_tracker", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_LUNARG_object_tracker");
				} else if(strcmp("VK_LAYER_LUNARG_image", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_LUNARG_image");
				} else if(strcmp("VK_LAYER_LUNARG_core_validation", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_LUNARG_core_validation");
				} else if(strcmp("VK_LAYER_LUNARG_swapchain", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_LUNARG_swapchain");
				} else if(strcmp("VK_LAYER_GOOGLE_unique_objects", layer.layerName) == 0) {
					layerExtensionsWeLikeToUse.push_back("VK_LAYER_GOOGLE_unique_objects");
				}
				XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Name: " << layer.layerName << " Version: " << layer.specVersion << "\n");
			}
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--------------------------------------------------\n");
		} else {
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--- Instance Layers Disabled ---------------------\n");
		}
		// ---------------------------------------------------------------------


		// ---------------------------------------------------------------------
		// Create the Vulkan instance.
		//

		VkInstanceCreateInfo instanceCreateInfo {};
		instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceCreateInfo.pApplicationInfo = &applicationInfo;
		instanceCreateInfo.enabledExtensionCount = (uint32_t)enabledExtensions.size();
		instanceCreateInfo.ppEnabledExtensionNames = enabledExtensions.data();
		if(layerExtensionsWeLikeToUse.size() > 0) {
			instanceCreateInfo.enabledLayerCount = (uint32_t)layerExtensionsWeLikeToUse.size();
			instanceCreateInfo.ppEnabledLayerNames = layerExtensionsWeLikeToUse.data();
		}

		result = vkCreateInstance(&instanceCreateInfo, nullptr, &m_instance);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanInstance, "vkCreateInstance failed: " << vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}

		// ---------------------------------------------------------------------

		XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--- Enabled Instance Extensions --------------------\n");
		for(auto extension : enabledExtensions) {
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Name: " << extension << "\n");
		}
		XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--------------------------------------------------\n");


		//
		// Get first the number of physical devices.
		//
		uint32_t deviceCount = 0;
		result = vkEnumeratePhysicalDevices(m_instance, &deviceCount, nullptr);
		if(result != VK_SUCCESS) {
			XDEVL_MODULEX_ERROR(XdevLVulkanInstance, "vkEnumeratePhysicalDevices failed: " << vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}

		//
		// Do the same but this time get the info for the physical devices too.
		//
		m_physicalDevices.reserve(deviceCount);
		m_physicalDevices.resize(deviceCount);
		result = vkEnumeratePhysicalDevices(m_instance, &deviceCount, m_physicalDevices.data());
		if(result != VK_SUCCESS) {
			XDEVL_MODULEX_ERROR(XdevLVulkanInstance, "vkEnumeratePhysicalDevices failed: " << vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}

		//
		// Print out some info about the physical devices.
		//
		m_physicalDevice = m_physicalDevices[0];
		for(auto physicalDevice : m_physicalDevices) {

			VkPhysicalDeviceProperties deviceProperties;
			vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);

			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Vulkan API Version : " <<
			                   ((deviceProperties.apiVersion>>22)&0x3FF) << "." <<
			                   ((deviceProperties.apiVersion>>12)&0x3FF) << "." <<
			                   ((deviceProperties.apiVersion&0x3FF)) << std::endl);

			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Driver Version     : " <<
			                   ((deviceProperties.driverVersion>>22)&0x3FF) << "." <<
			                   ((deviceProperties.driverVersion>>12)&0x3FF) << "." <<
			                   ((deviceProperties.driverVersion&0x3FF)) << std::endl);

			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Physical Device Name        : " << deviceProperties.deviceName << std::endl);
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Physical Device Type        : " << vkVkPhysicalDeviceTypeToString(deviceProperties.deviceType) << std::endl);
			switch(deviceProperties.deviceType) {
				case VK_PHYSICAL_DEVICE_TYPE_OTHER:
					break;
				case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
					break;
				case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: {
					m_physicalDevice = physicalDevice;
				}
				break;
				case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
					break;
				case VK_PHYSICAL_DEVICE_TYPE_CPU:
					break;
				default:
					break;
			}

			//
			// Find which queue families are supported.
			//
			uint32_t queueFamilyCount = 0;
			vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

			std::vector<VkQueueFamilyProperties> familyProperties(queueFamilyCount);
			vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, familyProperties.data());
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Number of Queues Families: " << queueFamilyCount << std::endl);


			for(uint32_t j = 0; j < queueFamilyCount; j++) {
				XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--- Queue Family Number: " << j << " ------------------------\n");
				XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Number of Queues: " << familyProperties[j].queueCount << std::endl);
				XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Supported operation on this Queues: " << std::endl);


				if(familyProperties[j].queueCount > 0 && familyProperties[j].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
					XDEVL_MODULEX_INFO(XdevLVulkanInstance, "VK_QUEUE_GRAPHICS_BIT\n");
					m_queueFamilyIndices->setGraphicsQueueIndex(j);

					// TODO This is a filthy hack.We have to set the number of supported queues somewhere else.
					m_queueFamilyIndices->setNumberOfQueues(familyProperties[j].queueCount);

				}
				if(familyProperties[j].queueFlags & VK_QUEUE_COMPUTE_BIT)
					XDEVL_MODULEX_INFO(XdevLVulkanInstance, "VK_QUEUE_COMPUTE_BIT\n");
				if(familyProperties[j].queueFlags & VK_QUEUE_TRANSFER_BIT)
					XDEVL_MODULEX_INFO(XdevLVulkanInstance, "VK_QUEUE_TRANSFER_BIT\n");
				if(familyProperties[j].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT)
					XDEVL_MODULEX_INFO(XdevLVulkanInstance, "VK_QUEUE_SPARSE_BINDING_BIT\n");
				XDEVL_MODULEX_INFO(XdevLVulkanInstance, "---------------------------------------------------\n");
			}
		}
		if(UINT32_MAX == m_queueFamilyIndices->getGraphicsQueueIndex()) {
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Couldn't find any queue for GFX rendering.\n");
			return RET_FAILED;
		}

		vkGetPhysicalDeviceFeatures(m_physicalDevice, &m_deviceFeatures);
		vkGetPhysicalDeviceMemoryProperties(m_physicalDevice, &m_physicalDeviceMemoryProperties);
		std::cout << "Memory Type Count: " << m_physicalDeviceMemoryProperties.memoryTypeCount << std::endl;
		std::cout << "Memory Heap Count: " << m_physicalDeviceMemoryProperties.memoryHeapCount << std::endl;

		if(!m_debug) {
			m_debug = std::make_shared<XdevLVulkanDebugImpl>(m_instance);
			m_debug->init();
		}
		return RET_SUCCESS;
	}

	IPXdevLVulkanDevice XdevLVulkanInstanceImpl::createDevice() {
		auto tmp = std::make_shared<XdevLVulkanDeviceImpl>(this);
		return tmp;
	}

	IPXdevLVulkanSurface XdevLVulkanInstanceImpl::createSurface() {
		auto tmp = std::make_shared<XdevLVulkanSurfaceImpl>(this);
		return tmp;
	}

	VkInstance XdevLVulkanInstanceImpl::getNativeHandle() const {
		return m_instance;
	}

	VkPhysicalDeviceMemoryProperties XdevLVulkanInstanceImpl::getPhysicalDeviceMemoryProperties() const {
		return m_physicalDeviceMemoryProperties;
	}

	IPXdevLQueueFamilyIndices XdevLVulkanInstanceImpl::getQueueFamilyIndices() {
		return m_queueFamilyIndices;
	}

}
