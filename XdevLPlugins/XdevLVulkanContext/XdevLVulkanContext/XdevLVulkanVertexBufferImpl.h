/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_VERTEX_BUFFER_IMPL_H
#define XDEVL_VULKAN_VERTEX_BUFFER_IMPL_H

#include "XdevLVulkanBufferBaseImpl.h"

namespace xdl {

	/**
	* @class XdevLVulkanVertexBufferImpl
	* @brief Implementation of the XdevLVulkanVertexBuffer interface.
	* @author Cengiz Terzibas
	*/
	class XdevLVulkanVertexBufferImpl : public XdevLVulkanBufferBaseImpl<XdevLVulkanVertexBuffer, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT> {
		public:

			XdevLVulkanVertexBufferImpl(VkDevice device, VkPhysicalDeviceMemoryProperties memoryProperties)
				: XdevLVulkanBufferBaseImpl<XdevLVulkanVertexBuffer, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT>(device, memoryProperties)
				, m_stride(0) {
			}

			virtual ~XdevLVulkanVertexBufferImpl() {};

			void addElement(uint32_t binding, uint32_t location, VkFormat format, uint32_t offset) override final {
				VkVertexInputAttributeDescription vertexInputAttributeDescription {};
				vertexInputAttributeDescription.binding = binding;
				vertexInputAttributeDescription.location = location;
				vertexInputAttributeDescription.format = format;
				vertexInputAttributeDescription.offset = offset;

				m_vertexInputAttributeDescription.push_back(std::move(vertexInputAttributeDescription));

				auto size_of_formats = sizeOfVulkanFormatsElementInBytes(format);
				auto number_of_elements = numberOfElementsOfVulkanFormats(format);
				XDEVL_ASSERT(size_of_formats != UINT32_MAX, "sizeOfFormats didn't implement this format.");

				m_stride += number_of_elements * size_of_formats;

				m_vertexInputBindingDescription.binding = 0;
				m_vertexInputBindingDescription.stride = m_stride;
				m_vertexInputBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
			}

			const std::vector<VkVertexInputAttributeDescription>& getVertexInputAttributeDescription() const override final {
				return m_vertexInputAttributeDescription;
			}

			VkPipelineVertexInputStateCreateInfo getPipelineVertexInputStateCreateInfo() const {

				VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo {};
				pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
				pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
				pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = &m_vertexInputBindingDescription;
				pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
				pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = (uint32_t)m_vertexInputAttributeDescription.size();
				pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = m_vertexInputAttributeDescription.data();

				return pipelineVertexInputStateCreateInfo;
			}

			VkVertexInputBindingDescription getVertexInputBindingDescription() const override final {
				return m_vertexInputBindingDescription;
			}

			xdl_uint32 getStride() const override final {
				return m_stride;
			}

		private:
			VkVertexInputBindingDescription m_vertexInputBindingDescription;
			std::vector<VkVertexInputAttributeDescription> m_vertexInputAttributeDescription;
			xdl_uint32 m_stride;
	};

}

#endif
