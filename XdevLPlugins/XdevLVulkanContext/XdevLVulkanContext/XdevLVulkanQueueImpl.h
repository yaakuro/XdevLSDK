/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_QUEUE_IMPL_H
#define XDEVL_VULKAN_QUEUE_IMPL_H

#include "XdevLVulkanDeviceImpl.h"

namespace xdl {

	/**
	 * @class XdevLVulkanQueueImpl
	 * @brief Implementation of the XdevLVulkanQueue interface.
	 * @author Cengiz Terzibas
	 */
	class XdevLVulkanQueueImpl : public XdevLVulkanQueue {
		public:
			XdevLVulkanQueueImpl();
			XdevLVulkanQueueImpl(XdevLVulkanDeviceImpl* deviceImpl);
			XdevLVulkanQueueImpl(VkDevice device, VkQueue queue);
			virtual ~XdevLVulkanQueueImpl();

			VkQueue getNativeHandle() const override final;

		private:
			XdevLVulkanDeviceImpl* m_deviceImpl;
			VkDevice m_device;
			VkQueue m_queue;
	};

}


#endif
