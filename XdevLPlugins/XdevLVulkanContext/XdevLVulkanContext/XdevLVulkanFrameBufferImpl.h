/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_FRAME_BUFFER_IMPL_H
#define XDEVL_VULKAN_FRAME_BUFFER_IMPL_H

#include <XdevLVulkanContext/XdevLVulkanContext.h>

namespace xdl {

	class XdevLVulkanInstanceImpl;
	class XdevLVulkanDeviceImpl;

	struct XdevLVulkanCreateImageInfo {
		XdevLVulkanCreateImageInfo()
			: usage(0)
			, format(VK_FORMAT_UNDEFINED)
			, attachment(0)
			, create(xdl_true) {}

		VkImageUsageFlags usage;
		VkFormat format;
		uint32_t attachment;
		xdl_bool create;
	};

	/**
	 * @class XdevLVulkanFrameBufferImpl
	 * @brief Implementation of the XdevLVulkanFrameBuffer internface.
	 * @author Cengiz Terzibas
	 */
	class XdevLVulkanFrameBufferImpl : public XdevLVulkanFrameBuffer {
		public:
			XdevLVulkanFrameBufferImpl();
			XdevLVulkanFrameBufferImpl(XdevLVulkanInstanceImpl* instanceImpl, XdevLVulkanDeviceImpl* deviceImpl);
			XdevLVulkanFrameBufferImpl(VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device);
			virtual ~XdevLVulkanFrameBufferImpl();

			xdl_int create(xdl_uint width, xdl_uint height) override final;
			xdl_int create(VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device, xdl_uint width, xdl_uint height) override final;
			xdl_int addColorTarget(xdl_uint index, VkFormat colorTargetFormat, const XdevLVulkanImage& colorTargetImage) override final;
			xdl_int addColorTarget(xdl_uint index, VkFormat colorTargetFormat) override final;
			xdl_int addDepthStencil(VkFormat depthStencilFormat, const XdevLVulkanImage& colorTargetImage) override final;
			xdl_int addDepthStencil(VkFormat depthStencilFormat) override final;
			xdl_int setRenderPass(VkRenderPass renderPass) override final;
			VkRenderPass getRenderPass() const override final;
			VkFramebuffer getNativeHandle() const override final;
			XdevLVulkanImage getColorAttachment(xdl_uint idx) override final;
			const VkExtent2D& getExtent2D() const;

		private:

			xdl_int createRenderPass();
			xdl_int createVulkanImage(const XdevLVulkanCreateImageInfo& info, XdevLVulkanImage& image, bool depthStencil);

		protected:

			XdevLVulkanInstanceImpl* m_instanceImpl;
			XdevLVulkanDeviceImpl* m_deviceImpl;
			VkInstance m_instance;
			VkPhysicalDevice m_physicalDevice;
			VkDevice m_device;
			VkFramebuffer m_frameBuffer;
			VkRenderPass m_renderPass;
			xdl_bool m_renderPassSpecified;
			std::vector<XdevLVulkanImage> m_colorAttachments;
			std::vector<XdevLVulkanCreateImageInfo> m_colorAttachmentsInfo;
			std::vector<VkImageView> m_attachments;
			XdevLVulkanImage m_depthStencilImage;
			XdevLVulkanCreateImageInfo m_depthStencilInfo;
			xdl_bool m_depthStencilImageSpecified;
			VkExtent2D m_extent2D;
			VkPhysicalDeviceMemoryProperties m_physicalDeviceMemoryProperties;
	};

}


#endif
