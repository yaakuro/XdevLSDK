/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/


#include <set>

#include "XdevLVulkanDeviceImpl.h"
#include "XdevLVulkanVertexBufferImpl.h"
#include "XdevLVulkanIndexBufferImpl.h"
#include "XdevLVulkanUniformBufferImpl.h"
#include "XdevLVulkanQueueImpl.h"
#include "XdevLVulkanCommandPoolImpl.h"
#include "XdevLVulkanDescriptorLayoutImpl.h"

namespace xdl {

	XdevLVulkanDeviceImpl::XdevLVulkanDeviceImpl()
		: m_instanceImpl(nullptr)
		, m_device(nullptr)
		, m_depthStencilFormat(VK_FORMAT_UNDEFINED)
		, m_memoryProperties {}
	, m_resetInteralExtensions(xdl_false) {

	}

	XdevLVulkanDeviceImpl::XdevLVulkanDeviceImpl(XdevLVulkanInstanceImpl* instanceImpl)
		: m_instanceImpl(instanceImpl)
		, m_device(nullptr)
		, m_depthStencilFormat(VK_FORMAT_UNDEFINED)
		, m_memoryProperties {}
	, m_resetInteralExtensions(xdl_false) {

	}

	XdevLVulkanDeviceImpl::~XdevLVulkanDeviceImpl() {
		if(VK_NULL_HANDLE != m_device) {
			vkDestroyDevice(m_device, nullptr);
			m_device = VK_NULL_HANDLE;
		}
		m_resetInteralExtensions = xdl_false;
		for(auto& extension : m_passedExtensions) {
			delete [] extension;
		}
	}

	xdl_int XdevLVulkanDeviceImpl::create() {
		XDEVL_ASSERT(m_instanceImpl != nullptr, "Not allowed to use XdevLVulkanDeviceImpl::create(). Use XdevLVulkanDevice::create(VkPhysicalDevice physicalDevice) instead.");
		auto physicalDevice = m_instanceImpl->getPhysicalDevice();
		m_memoryProperties = m_instanceImpl->getPhysicalDeviceMemoryProperties();

		return create(physicalDevice, m_instanceImpl->getQueueFamilyIndices());
	}

	void XdevLVulkanDeviceImpl::addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal) {
		m_resetInteralExtensions = resetInternal;

		char* tmp = nullptr;
		for(auto& extension : extensionsToEnable) {
			auto str = extension.toString();
			tmp = new char[str.length() + 1];
			strcpy(tmp, str.c_str());
			tmp[str.length()] = '\0';
			m_passedExtensions.push_back(tmp);
		}
	}

	xdl_int XdevLVulkanDeviceImpl::create(VkPhysicalDevice physicalDevice, const IPXdevLQueueFamilyIndices& queueFamilyIndicies) {

		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &m_memoryProperties);
		m_queueFamilyIndices = queueFamilyIndicies;
		//
		// Here we will create the real physical device connection.
		//
		VkDeviceCreateInfo deviceInfo {};
		deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;  // Again, spec says this must be like this.

		std::vector<const char*> enabledExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
		if(m_passedExtensions.size()) {
			if(m_resetInteralExtensions) {
				enabledExtensions.clear();
			}
			enabledExtensions.insert(enabledExtensions.end(), m_passedExtensions.begin(), m_passedExtensions.end());
		}

		// We don't do anything with layer or extensions here.
		deviceInfo.enabledLayerCount = 0;
		deviceInfo.ppEnabledLayerNames = nullptr;
		deviceInfo.enabledExtensionCount = (uint32_t)enabledExtensions.size();
		deviceInfo.ppEnabledExtensionNames = enabledExtensions.data();

		//
		// Here's where we initialize our queues
		//
		std::vector<VkDeviceQueueCreateInfo> deviceQueueCreateInfos;

		//
		// Set Queue Family Indices must be unique. So we use a set to get those values.
		//
		std::set<uint32_t> uniqueQueueFamilies = {queueFamilyIndicies->getGraphicsQueueIndex(), queueFamilyIndicies->getPresentQueueIndex()};
		std::vector<float> queuePriorities(queueFamilyIndicies->getNumberOfQueues());
		for(auto& priority : queuePriorities) {
			priority = 1.0f;
		}

		for(const auto& queueFamilyIndex : uniqueQueueFamilies) {
			VkDeviceQueueCreateInfo deviceQueueInfo {};
			deviceQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			deviceQueueInfo.queueFamilyIndex = queueFamilyIndex;
			deviceQueueInfo.queueCount = queuePriorities.size();
			deviceQueueInfo.pQueuePriorities = queuePriorities.data();
			deviceQueueCreateInfos.push_back(deviceQueueInfo);
		}

		deviceInfo.queueCreateInfoCount = deviceQueueCreateInfos.size();
		deviceInfo.pQueueCreateInfos = deviceQueueCreateInfos.data();

		VkResult result = vkCreateDevice(physicalDevice, &deviceInfo, nullptr, &m_device);
		if(result != VK_SUCCESS) {
			XDEVL_MODULEX_ERROR(XdevLVulkanInstance, "vkCreateDevice failed: " << vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}

		getSupportedDepthFormat(physicalDevice, &m_depthStencilFormat);

		XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--- Enabled Device Extensions --------------------\n");
		for(auto extension : enabledExtensions) {
			XDEVL_MODULEX_INFO(XdevLVulkanInstance, "Name: " << extension << "\n");
		}
		XDEVL_MODULEX_INFO(XdevLVulkanInstance, "--------------------------------------------------\n");

		XDEVL_MODULEX_SUCCESS(XdevLVulkanInstance, "Vulkan Device: " << m_device << " created successfully.\n");
		return RET_SUCCESS;
	}

	VkFormat XdevLVulkanDeviceImpl::getSupportedDepthStencilFormat() const {
		return m_depthStencilFormat;
	}

	IPXdevLVulkanShader XdevLVulkanDeviceImpl::createShader(VkShaderStageFlagBits shaderStageFalgBits, const XdevLString& entryName) {
		auto tmp = std::make_shared<XdevLVulkanShaderImpl>(m_device, shaderStageFalgBits, entryName);
		return tmp;
	}

	IPXdevLVulkanShader XdevLVulkanDeviceImpl::createVertexShader(const XdevLString& entryName) {
		auto tmp = std::make_shared<XdevLVulkanShaderImpl>(m_device, VK_SHADER_STAGE_VERTEX_BIT, entryName);
		return tmp;
	}

	IPXdevLVulkanShader XdevLVulkanDeviceImpl::createFragmentShader(const XdevLString& entryName) {
		auto tmp = std::make_shared<XdevLVulkanShaderImpl>(m_device, VK_SHADER_STAGE_FRAGMENT_BIT, entryName);
		return tmp;
	}

	IPXdevLVulkanShader XdevLVulkanDeviceImpl::createGeometryShader(const XdevLString& entryName) {
		auto tmp = std::make_shared<XdevLVulkanShaderImpl>(m_device, VK_SHADER_STAGE_GEOMETRY_BIT, entryName);
		return tmp;
	}

	IPXdevLVulkanShader XdevLVulkanDeviceImpl::createTessellationControlShader(const XdevLString& entryName) {
		auto tmp = std::make_shared<XdevLVulkanShaderImpl>(m_device, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT, entryName);
		return tmp;
	}

	IPXdevLVulkanShader XdevLVulkanDeviceImpl::createTessellationEvaluationShader(const XdevLString& entryName) {
		auto tmp = std::make_shared<XdevLVulkanShaderImpl>(m_device, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, entryName);
		return tmp;
	}

	IPXdevLVulkanVertexBuffer XdevLVulkanDeviceImpl::createVertexBuffer() {
		auto tmp = std::make_shared<XdevLVulkanVertexBufferImpl>(m_device, m_memoryProperties);
		return tmp;
	}

	IPXdevLVulkanIndexBuffer XdevLVulkanDeviceImpl::createIndexBuffer() {
		auto tmp = std::make_shared<XdevLVulkanIndexBufferImpl>(m_device, m_memoryProperties);
		return tmp;
	}

	IPXdevLVulkanUniformBuffer XdevLVulkanDeviceImpl::createUniformBuffer() {
		auto tmp = std::make_shared<XdevLVulkanUniformBufferImpl>(m_device, m_memoryProperties);
		return tmp;
	}

	IPXdevLVulkanQueue XdevLVulkanDeviceImpl::createQueue(XdevLVulkanQueueType queueType) {
		VkQueue queue = VK_NULL_HANDLE;
		uint32_t index = 0;
		switch(queueType) {
			case XdevLVulkanQueueType::GRAPHICS:
				index = m_queueFamilyIndices->getGraphicsQueueIndex();
				break;
			case XdevLVulkanQueueType::PRESENT:
				index = m_queueFamilyIndices->getPresentQueueIndex();
				break;
			default:
				break;
		}
		vkGetDeviceQueue(m_device, index, 0, &queue);
		auto tmp = std::make_shared<XdevLVulkanQueueImpl>(m_device, queue);
		return tmp;
	}

	IPXdevLVulkanCommandPool XdevLVulkanDeviceImpl::createCommandPool(XdevLVulkanCommandPoolType commandPoolType) {

		VkCommandPoolCreateInfo commandPoolCreateInfo {};
		commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		switch(commandPoolType) {
			case XdevLVulkanCommandPoolType::GRAPHICS:
				commandPoolCreateInfo.queueFamilyIndex = m_queueFamilyIndices->getGraphicsQueueIndex();
				break;
			case XdevLVulkanCommandPoolType::PRESENT:
				commandPoolCreateInfo.queueFamilyIndex = m_queueFamilyIndices->getPresentQueueIndex();
				break;
			default:
				break;
		}

		VkCommandPool commandPool = VK_NULL_HANDLE;
		auto result = vkCreateCommandPool(m_device, &commandPoolCreateInfo, nullptr, &commandPool);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanDevice, "Failed to create command pool.\n");
			return nullptr;
		}

		auto tmp = std::make_shared<XdevLVulkanCommandPoolImpl>(m_device, commandPool);
		return tmp;
	}

	IPXdevLVulkanDescriptorLayout XdevLVulkanDeviceImpl::createDescriptorLayout() {
		auto tmp = std::make_shared<XdevLVulkanDescriptorLayoutImpl>(this);
		return tmp;
	}

	xdl_uint32 XdevLVulkanDeviceImpl::getGraphicsFamilyQueueIndex() const {
		return m_queueFamilyIndices->getGraphicsQueueIndex();
	}

	VkDevice XdevLVulkanDeviceImpl::getNativeHandle() const {
		return m_device;
	}

}
