/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_CONTEXT_IMPL_H
#define XDEVL_VULKAN_CONTEXT_IMPL_H

#include <XdevLPluginImpl.h>
#include <XdevLVulkanContext/XdevLVulkanContext.h>
#include <XdevLVulkanContext/XdevLVulkanContextBase.h>

#define XDEVL_VK_API_VERSION VK_MAKE_VERSION(1, 0, 1)

namespace xdl {

	static const XdevLString description {
		"Module to create a Vulkan instance."
	};

	static const XdevLString pluginName {
		"XdevLVulkanContext"
	};

	static const std::vector<XdevLModuleName> moduleNames {
		XdevLModuleName("XdevLVulkanContext")
	};

	/**
		@class XdevLVulkanContextImpl
		@brief Implementation of the XdevLVulkanContext interface.
		@author Cengiz Terzibas
	*/
	class XdevLVulkanContextImpl : public XdevLVulkanContextBase {

		public:
			XdevLVulkanContextImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLVulkanContextImpl();

			void* getInternal(const XdevLInternalName& id) override final;
			xdl_int shutdown() override final;

			void addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal) override final;
			xdl_int createContext(IPXdevLWindow window, const VkApplicationInfo& attributes) override final;

			IPXdevLVulkanInstance createInstance() override final;
			IPXdevLVulkanDevice createDevice() override final;
			IPXdevLVulkanSurface createSurface() override final;
			IPXdevLVulkanDescriptorLayout createDescriptorLayout() override final;
			IPXdevLVulkanFrameBuffer createFrameBuffer() override final;
			IPXdevLVulkanSwapChain createSwapChain() override final;
			IPXdevLVulkanShader createShaderModule() override final;
			IPXdevLVulkanTexture createTexture() override final;
			IPXdevLVulkanInstance getInstance() const override final;
			IPXdevLVulkanDevice getDevice() const override final;
			IPXdevLVulkanSurface getSurface() const override final;
			IPXdevLVulkanSwapChain getSwapChain() const override final;

			xdl_vptr getProcAddress(const XdevLString& func) override final;

		private:

			IPXdevLVulkanInstance m_instance;
			IPXdevLVulkanDevice m_device;
			IPXdevLVulkanSurface m_surface;
			IPXdevLVulkanSwapChain m_swapChain;

			std::vector<XdevLString> m_passedExtensions;
			xdl_bool m_resetInternalExtensions;

	};

}


#endif
