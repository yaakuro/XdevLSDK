/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <vector>

#include <XdevLWindow/XdevLWindow.h>

#include "XdevLVulkanContextImpl.h"
#include "XdevLVulkanInstanceImpl.h"
#include "XdevLVulkanDeviceImpl.h"
#include "XdevLVulkanFrameBufferImpl.h"
#include "XdevLVulkanSurfaceImpl.h"
#include "XdevLVulkanSwapChainImpl.h"
#include "XdevLVulkanTextureImpl.h"
#include "XdevLVulkanDescriptorLayoutImpl.h"

xdl::XdevLModuleDescriptor moduleDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::description,
	XDEVLVULKANCONTEXT_MAJOR_VERSION,
	XDEVLVULKANCONTEXT_MINOR_VERSION,
	XDEVLVULKANCONTEXT_PATCH_VERSION
};

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::pluginName,
	xdl::moduleNames,
	XDEVLVULKANCONTEXT_MODULE_MAJOR_VERSION,
	XDEVLVULKANCONTEXT_MODULE_MINOR_VERSION,
	XDEVLVULKANCONTEXT_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLVulkanContextImpl, moduleDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

namespace xdl {

//
// XdevLVulkanContextImpl
//

	XdevLVulkanContextImpl::XdevLVulkanContextImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLVulkanContextBase(parameter, descriptor) {
	}

	XdevLVulkanContextImpl::~XdevLVulkanContextImpl() {
		if(m_swapChain) {
			m_swapChain = nullptr;
		}
		if(m_surface) {
			m_surface = nullptr;
		}
		if(m_device) {
			m_device = nullptr;
		}
		if(m_instance) {
			m_instance = nullptr;
		}
	}

	xdl_int XdevLVulkanContextImpl::shutdown() {
		return RET_SUCCESS;
	}

	xdl_vptr XdevLVulkanContextImpl::getProcAddress(const XdevLString& func) {
		return 0;
	}

	void XdevLVulkanContextImpl::addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal) {
		m_resetInternalExtensions = resetInternal;
		m_passedExtensions = extensionsToEnable;
	}

	xdl_int XdevLVulkanContextImpl::createContext(IPXdevLWindow window, const VkApplicationInfo& attributes) {

		m_instance = createInstance();
		if(m_passedExtensions.size() > 0) {
			m_instance->addExtensionsToEnable(m_passedExtensions, m_resetInternalExtensions);
		}
		if(m_instance->create(attributes) != RET_SUCCESS) {
			return RET_FAILED;
		}

		m_surface = m_instance->createSurface();
		if(m_surface->create(window) != RET_SUCCESS) {
			return RET_FAILED;
		}

		m_device = m_instance->createDevice();
		if(m_device->create() != RET_SUCCESS) {
			return RET_FAILED;
		}


		m_swapChain = m_surface->createSwapChain(m_device);
		if(m_swapChain->create() != RET_SUCCESS) {
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	IPXdevLVulkanInstance XdevLVulkanContextImpl::createInstance() {
		auto tmp = std::make_shared<XdevLVulkanInstanceImpl>();
		return tmp;
	}

	IPXdevLVulkanDevice XdevLVulkanContextImpl::createDevice() {
		auto tmp = std::make_shared<XdevLVulkanDeviceImpl>();
		return tmp;
	}

	IPXdevLVulkanFrameBuffer XdevLVulkanContextImpl::createFrameBuffer() {
		auto tmp = std::make_shared<XdevLVulkanFrameBufferImpl>();
		return tmp;
	}

	IPXdevLVulkanSurface XdevLVulkanContextImpl::createSurface() {
		auto tmp = std::make_shared<XdevLVulkanSurfaceImpl>();
		return tmp;
	}

	IPXdevLVulkanSwapChain XdevLVulkanContextImpl::createSwapChain() {
		auto tmp = std::make_shared<XdevLVulkanSwapChainImpl>();
		return tmp;
	}

	IPXdevLVulkanDescriptorLayout XdevLVulkanContextImpl::createDescriptorLayout() {
		auto tmp = std::make_shared<XdevLVulkanDescriptorLayoutImpl>();
		return tmp;
	}

	IPXdevLVulkanShader XdevLVulkanContextImpl::createShaderModule() {
		auto tmp = std::make_shared<XdevLVulkanShaderImpl>();
		return tmp;
	}

	IPXdevLVulkanTexture XdevLVulkanContextImpl::createTexture() {
		auto tmp = std::make_shared<XdevLVulkanTextureImpl>();
		return tmp;
	}

	IPXdevLVulkanInstance XdevLVulkanContextImpl::getInstance() const {
		return m_instance;
	}

	IPXdevLVulkanDevice XdevLVulkanContextImpl::getDevice() const {
		return m_device;
	}

	IPXdevLVulkanSurface XdevLVulkanContextImpl::getSurface() const {
		return m_surface;
	}

	IPXdevLVulkanSwapChain XdevLVulkanContextImpl::getSwapChain() const {
		return m_swapChain;
	}

	void* XdevLVulkanContextImpl::getInternal(const XdevLInternalName& id) {
		return nullptr;
	}
}
