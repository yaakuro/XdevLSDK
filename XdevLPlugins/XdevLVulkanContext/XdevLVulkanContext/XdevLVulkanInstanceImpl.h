/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_INSTANCE_IMPL_H
#define XDEVL_VULKAN_INSTANCE_IMPL_H

#include "XdevLVulkanContextImpl.h"
#include "XdevLVulkanDebugImpl.h"

namespace xdl {

	/**
	 * @class XdevLQueueFamilyIndicesImpl
	 * @brief Implementation of the XdevLQueueFamilyIndices interface.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLQueueFamilyIndicesImpl : public XdevLQueueFamilyIndices {
		public:
			XdevLQueueFamilyIndicesImpl();
			virtual ~XdevLQueueFamilyIndicesImpl();

			xdl_uint32 getNumberOfQueues() const override final;
			xdl_uint32 getGraphicsQueueIndex() const override final;
			xdl_uint32 getPresentQueueIndex() const override final;
			void setNumberOfQueues(xdl_uint32 nQueues) override final;
			void setGraphicsQueueIndex(xdl_uint32 index) override final;
			void setPresentQueueIndex(xdl_uint32 index) override final;
			xdl_bool hasGraphicsQueueIndex() const override final;
			xdl_bool hasPresentQueueIndex() const override final;

		private:

			xdl_uint32 numberOfQueues;
			xdl_uint32 graphicsQueueIndex;
			xdl_uint32 presentQueueIndex;
	};

	/**
	 * @class XdevLVulkanInstanceImpl
	 * @brief Implementation of the XdevLVulkanInstance interface.
	 * @author Cengiz Terzibas
	 */
	class XdevLVulkanInstanceImpl : public XdevLVulkanInstance {
		public:
			XdevLVulkanInstanceImpl();
			virtual ~XdevLVulkanInstanceImpl();

			void addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal) override final;
			xdl_int create(xdl_bool enableLayers) override final;
			xdl_int create(const VkApplicationInfo& applicationInfo, xdl_bool enableLayers) override final;
			IPXdevLVulkanDevice createDevice() override final;
			IPXdevLVulkanSurface createSurface() override final;
			VkPhysicalDeviceMemoryProperties getPhysicalDeviceMemoryProperties() const override final;
			VkInstance getNativeHandle() const override final;
			IPXdevLQueueFamilyIndices getQueueFamilyIndices() override final;
			VkPhysicalDevice getPhysicalDevice() const override final;


		private:

			VkInstance m_instance;
			VkPhysicalDevice m_physicalDevice;
			xdl_bool m_resetInteralExtensions;
			std::shared_ptr<XdevLQueueFamilyIndicesImpl> m_queueFamilyIndices;
			std::shared_ptr<XdevLVulkanDebugImpl> m_debug;
			VkPhysicalDeviceProperties m_deviceProperties;
			VkPhysicalDeviceFeatures m_deviceFeatures;
			VkPhysicalDeviceMemoryProperties m_physicalDeviceMemoryProperties;
			std::vector<VkPhysicalDevice> m_physicalDevices;
			std::vector<const char*> m_passedExtensions;
	};
}


#endif
