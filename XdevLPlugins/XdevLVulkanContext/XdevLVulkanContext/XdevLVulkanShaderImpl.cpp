/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/


#include "XdevLVulkanShaderImpl.h"

namespace xdl {

	XdevLVulkanShaderImpl::XdevLVulkanShaderImpl()
		: m_device(VK_NULL_HANDLE)
		, m_shaderModule(VK_NULL_HANDLE)
		, m_pipelineShaderStageCreateInfo {}
		, m_name("main") {
	}

	XdevLVulkanShaderImpl::XdevLVulkanShaderImpl(VkDevice device, VkShaderStageFlagBits shaderStageFalgBits, const XdevLString& entryName)
		: m_device(device)
		, m_shaderModule(VK_NULL_HANDLE)
		, m_shaderStageFlagBits(shaderStageFalgBits)
		, m_pipelineShaderStageCreateInfo {}
		, m_name(entryName) {
	}

	XdevLVulkanShaderImpl::~XdevLVulkanShaderImpl() {
		if(VK_NULL_HANDLE != m_shaderModule) {
			vkDestroyShaderModule(m_device, m_shaderModule, nullptr);
			m_shaderModule = VK_NULL_HANDLE;
		}
	}

	VkShaderModule XdevLVulkanShaderImpl::getNativeHandle() const {
		return m_shaderModule;
	}

	xdl_int XdevLVulkanShaderImpl::create() {
		m_pipelineShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		m_pipelineShaderStageCreateInfo.stage = m_shaderStageFlagBits;
		m_tmpName = m_name.toUTF8();
		m_pipelineShaderStageCreateInfo.pName = m_tmpName.c_str();
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanShaderImpl::create(VkDevice device, VkShaderStageFlagBits shaderStageFalgBits, const XdevLString& entryName) {
		if(VK_NULL_HANDLE == device) {
			return RET_FAILED;
		}
		m_shaderStageFlagBits = shaderStageFalgBits;
		m_name = entryName;
		m_pipelineShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		m_pipelineShaderStageCreateInfo.stage = m_shaderStageFlagBits;
		m_tmpName = m_name.toUTF8();
		m_pipelineShaderStageCreateInfo.pName = m_tmpName.c_str();
		return RET_SUCCESS;
	}


	xdl_int XdevLVulkanShaderImpl::load(IPXdevLFile& file) {

		//
		// Get the length of the file.
		//
		auto size = file->length();

		//
		// Read the whole file.
		//
		std::vector<xdl_uint8> shader_code((size_t)size);
		file->read(shader_code.data(), static_cast<xdl_int>(size));

		//
		// Create the vertex shader.
		//
		VkShaderModuleCreateInfo moduleCreateInfo {};
		moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		moduleCreateInfo.codeSize = (size_t)size;
		moduleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(shader_code.data());
		VkResult result = vkCreateShaderModule(m_device, &moduleCreateInfo, nullptr, &m_shaderModule);
		if(VK_SUCCESS != result) {
			m_pipelineShaderStageCreateInfo.module = VK_NULL_HANDLE;
			XDEVL_MODULEX_ERROR(XdevLVulkanShaderModule, "vkCreateShaderModule failed: " << xdl::vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}
		m_pipelineShaderStageCreateInfo.module = m_shaderModule;
		return RET_SUCCESS;
	}

	void XdevLVulkanShaderImpl::destroy() {
		if(VK_NULL_HANDLE != m_shaderModule) {
			vkDestroyShaderModule(m_device, m_shaderModule, nullptr);
			m_shaderModule = VK_NULL_HANDLE;
		}
	}

	VkShaderStageFlagBits XdevLVulkanShaderImpl::getShaderStageFlagBits() const {
		return m_shaderStageFlagBits;
	}

	VkPipelineShaderStageCreateInfo XdevLVulkanShaderImpl::getPipelineShaderStageCreateInfo() const {
		return m_pipelineShaderStageCreateInfo;
	}

	const XdevLString& XdevLVulkanShaderImpl::getEntryName() const {
		return m_name;
	}

}
