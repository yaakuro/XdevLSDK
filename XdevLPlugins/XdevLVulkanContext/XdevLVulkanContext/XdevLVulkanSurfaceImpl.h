/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_SURFACE_IMPL_H
#define XDEVL_VULKAN_SURFACE_IMPL_H

#include "XdevLVulkanInstanceImpl.h"

namespace xdl {

	/**
	 * @class XdevLVulkanSurfaceImpl
	 * @brief Implementation of the XdevLVulkanSurface interface.
	 * @author Cengiz Terzibas
	 */
	class XdevLVulkanSurfaceImpl : public XdevLVulkanSurface {
		public:
			XdevLVulkanSurfaceImpl();
			XdevLVulkanSurfaceImpl(XdevLVulkanInstanceImpl* instance);
			virtual ~XdevLVulkanSurfaceImpl();

			xdl_int create(IPXdevLWindow window) override final;
			xdl_int create(VkInstance instance, VkPhysicalDevice physicalDevice, IPXdevLWindow window, const IPXdevLQueueFamilyIndices& queueFamilyIndices) override final;
			IPXdevLVulkanSwapChain createSwapChain(IPXdevLVulkanDevice device) override final;
			VkSurfaceKHR getNativeHandle() const override final;
			const VkSurfaceCapabilitiesKHR& getSurfaceCapabilities() const override final;
			VkFormat getColorFormat() const override final;
			VkColorSpaceKHR getColorSpace() const override final;
			VkExtent2D getExtend() const override final;
			xdl_uint32 getPresentFamilyQueueIndex() const override final;

		private:

			void connectFunctions(VkInstance instance);

			PFN_vkGetPhysicalDeviceSurfaceSupportKHR fpGetPhysicalDeviceSurfaceSupportKHR;
			PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR fpGetPhysicalDeviceSurfaceCapabilitiesKHR;
			PFN_vkGetPhysicalDeviceSurfaceFormatsKHR fpGetPhysicalDeviceSurfaceFormatsKHR;
			PFN_vkGetPhysicalDeviceSurfacePresentModesKHR fpGetPhysicalDeviceSurfacePresentModesKHR;

		private:

			XdevLVulkanInstanceImpl* m_instanceImpl;
			VkInstance m_instance;
			VkSurfaceKHR m_surface;
			uint32_t m_presentQueueNodeIndex;
			IPXdevLQueueFamilyIndices m_queueFamilyIndices;
			VkSurfaceCapabilitiesKHR m_surfCaps;
			VkPresentModeKHR m_presentMode;
			VkFormat m_colorFormat;
			VkColorSpaceKHR m_colorSpace;
			VkExtent2D m_extent2D;
			static xdl_bool m_connectFunctions;
	};

}


#endif
