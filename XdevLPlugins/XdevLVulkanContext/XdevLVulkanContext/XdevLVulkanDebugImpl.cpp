/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <vector>

#include "XdevLVulkanDebugImpl.h"


namespace xdl {

	XdevLVulkanDebugImpl::XdevLVulkanDebugImpl(VkInstance instance)
		: m_instance(instance)
		, m_callback(VK_NULL_HANDLE)
		, fpCreateDebugReportCallbackEXT(nullptr)
		, fpDebugReportMessageEXT(nullptr)
		, fpDestroyDebugReportCallbackEXT(nullptr) {

		GET_INSTANCE_PROC_ADDR(m_instance, CreateDebugReportCallbackEXT);
		GET_INSTANCE_PROC_ADDR(m_instance, DebugReportMessageEXT);
		GET_INSTANCE_PROC_ADDR(m_instance, DestroyDebugReportCallbackEXT);
	}

	XdevLVulkanDebugImpl::~XdevLVulkanDebugImpl() {

		if(VK_NULL_HANDLE != m_callback) {
			fpDestroyDebugReportCallbackEXT(m_instance, m_callback, nullptr);
			m_callback = VK_NULL_HANDLE;
		}
	}

	VkResult XdevLVulkanDebugImpl::init() {
		VkDebugReportCallbackCreateInfoEXT callbackCreateInfo {};
		callbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
		callbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT |
		                           VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
		callbackCreateInfo.pfnCallback = &myDebugReportCallback;

		VkResult result = fpCreateDebugReportCallbackEXT(m_instance, &callbackCreateInfo, nullptr, &m_callback);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanDebugImpl, "vkCreateDebugReportCallbackEXT failed: " << xdl::vkVkResultToString(result) << std::endl);
			return result;
		}
		return result;
	}

	VKAPI_ATTR VkBool32 VKAPI_CALL XdevLVulkanDebugImpl::myDebugReportCallback(VkDebugReportFlagsEXT flags,
	        VkDebugReportObjectTypeEXT objectType,
	        uint64_t object,
	        size_t location,
	        int32_t messageCode,
	        const char* pLayerPrefix,
	        const char* pMessage,
	        void* pUserData) {
		std::cerr << "Debug: " << pMessage << std::endl;
		return VK_FALSE;
	}
}
