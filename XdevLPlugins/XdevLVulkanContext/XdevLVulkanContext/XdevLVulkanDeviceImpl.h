/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_DEVICE_IMPL_H
#define XDEVL_VULKAN_DEVICE_IMPL_H

#include "XdevLVulkanContextImpl.h"
#include "XdevLVulkanInstanceImpl.h"
#include "XdevLVulkanShaderImpl.h"

namespace xdl {

	/**
	 * @class XdevLVulkanDeviceImpl
	 * @brief Implementation of the XdevLVulkanDevice interface.
	 * @author Cengiz Terzibas
	 */
	class XdevLVulkanDeviceImpl : public XdevLVulkanDevice {
		public:
			XdevLVulkanDeviceImpl();
			XdevLVulkanDeviceImpl(XdevLVulkanInstanceImpl* instanceImpl);
			virtual ~XdevLVulkanDeviceImpl();

			void addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal) override final;

			xdl_int create() override final;
			xdl_int create(VkPhysicalDevice physicalDevice, const IPXdevLQueueFamilyIndices& queueFamilyIndicies) override final;
			IPXdevLVulkanShader createShader(VkShaderStageFlagBits shaderStageFalgBits, const XdevLString& entryName) override final;
			IPXdevLVulkanShader createVertexShader(const XdevLString& entryName = XdevLString("main")) override final;
			IPXdevLVulkanShader createFragmentShader(const XdevLString& entryName = XdevLString("main")) override final;
			IPXdevLVulkanShader createGeometryShader(const XdevLString& entryName = XdevLString("main")) override final;
			IPXdevLVulkanShader createTessellationControlShader(const XdevLString& entryName = XdevLString("main")) override final;
			IPXdevLVulkanShader createTessellationEvaluationShader(const XdevLString& entryName = XdevLString("main")) override final;

			IPXdevLVulkanVertexBuffer createVertexBuffer() override final;
			IPXdevLVulkanIndexBuffer createIndexBuffer() override final;
			IPXdevLVulkanUniformBuffer createUniformBuffer() override final;
			IPXdevLVulkanQueue createQueue(XdevLVulkanQueueType queueType) override final;
			IPXdevLVulkanCommandPool createCommandPool(XdevLVulkanCommandPoolType commandPoolType) override final;
			IPXdevLVulkanDescriptorLayout createDescriptorLayout() override final;
			xdl_uint32 getGraphicsFamilyQueueIndex() const override final;

			VkDevice getNativeHandle() const override final;
			VkFormat getSupportedDepthStencilFormat() const override final;

		private:

			XdevLVulkanInstanceImpl* m_instanceImpl;
			VkDevice m_device;
			VkFormat m_depthStencilFormat;
			VkPhysicalDeviceMemoryProperties m_memoryProperties;
			IPXdevLQueueFamilyIndices m_queueFamilyIndices;
			std::vector<const char*> m_passedExtensions;
			xdl_bool m_resetInteralExtensions;
	};

}


#endif
