/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLVulkanFrameBufferImpl.h"
#include "XdevLVulkanInstanceImpl.h"
#include "XdevLVulkanDeviceImpl.h"

#include <array>

namespace xdl {

	XdevLVulkanFrameBufferImpl::XdevLVulkanFrameBufferImpl()
		: m_instanceImpl(nullptr)
		, m_deviceImpl(nullptr)
		, m_instance(VK_NULL_HANDLE)
		, m_physicalDevice(VK_NULL_HANDLE)
		, m_device(VK_NULL_HANDLE)
		, m_frameBuffer(VK_NULL_HANDLE)
		, m_renderPass(VK_NULL_HANDLE)
		, m_renderPassSpecified(xdl_false)
		, m_depthStencilImage {}
		, m_depthStencilInfo {}
		, m_depthStencilImageSpecified(xdl_false)
		, m_extent2D {} {

	}

	XdevLVulkanFrameBufferImpl::XdevLVulkanFrameBufferImpl(XdevLVulkanInstanceImpl* instanceImpl, XdevLVulkanDeviceImpl* deviceImpl)
		: m_instanceImpl(instanceImpl)
		, m_deviceImpl(deviceImpl)
		, m_instance(instanceImpl->getNativeHandle())
		, m_physicalDevice(instanceImpl->getPhysicalDevice())
		, m_device(deviceImpl->getNativeHandle())
		, m_frameBuffer(VK_NULL_HANDLE)
		, m_renderPass(VK_NULL_HANDLE)
		, m_renderPassSpecified(xdl_false)
		, m_depthStencilImage {}
		, m_depthStencilInfo {}
		, m_depthStencilImageSpecified(xdl_false)
		, m_extent2D {} {

	}

	XdevLVulkanFrameBufferImpl::XdevLVulkanFrameBufferImpl(VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device)
		: m_instanceImpl(nullptr)
		, m_deviceImpl(nullptr)
		, m_instance(instance)
		, m_physicalDevice(physicalDevice)
		, m_device(device)
		, m_frameBuffer(VK_NULL_HANDLE)
		, m_renderPass(VK_NULL_HANDLE)
		, m_renderPassSpecified(xdl_false)
		, m_depthStencilImage {}
		, m_depthStencilInfo {}
		, m_depthStencilImageSpecified(xdl_false)
		, m_extent2D {} {

	}

	XdevLVulkanFrameBufferImpl::~XdevLVulkanFrameBufferImpl() {


		if(!m_depthStencilImageSpecified && m_depthStencilImage.memory) {
			vkFreeMemory(m_device, m_depthStencilImage.memory, nullptr);
			vkDestroyImage(m_device, m_depthStencilImage.image, nullptr);
			vkDestroyImageView(m_device, m_depthStencilImage.imageView, nullptr);
		}

		if(!m_renderPassSpecified) {
			vkDestroyRenderPass(m_device, m_renderPass, nullptr);
		}

		if(VK_NULL_HANDLE != m_frameBuffer) {
			vkDestroyFramebuffer(m_device, m_frameBuffer, nullptr);
			m_frameBuffer = VK_NULL_HANDLE;
		}
	}

	XdevLVulkanImage XdevLVulkanFrameBufferImpl::getColorAttachment(xdl_uint idx) {
		return m_colorAttachments[idx];
	}

	xdl_int XdevLVulkanFrameBufferImpl::setRenderPass(VkRenderPass renderPass) {
		if(VK_NULL_HANDLE == renderPass) {
			XDEVL_MODULEX_ERROR(XdevLVulkanFrameBuffer, "Invalid RenderPass.\n");
			return RET_FAILED;
		}
		m_renderPass = renderPass;
		m_renderPassSpecified = xdl_true;
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanFrameBufferImpl::create(VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device, xdl_uint width, xdl_uint height) {
		m_extent2D.width = width;
		m_extent2D.height = height;

		m_instance = instance;
		m_physicalDevice = physicalDevice;
		m_device = device;

		vkGetPhysicalDeviceMemoryProperties(m_physicalDevice, &m_physicalDeviceMemoryProperties);

		// Only create if requested.
		if(m_depthStencilInfo.create) {
			if(VK_FORMAT_UNDEFINED != m_depthStencilInfo.format) {
				if(createVulkanImage(m_depthStencilInfo, m_depthStencilImage, true) != RET_SUCCESS) {
					return RET_FAILED;
				}
			}
		}

		// Only create if requested.
		for(const auto& colorAttachmentInfo : m_colorAttachmentsInfo) {
			if(colorAttachmentInfo.create) {
				XdevLVulkanImage colorImage;
				if(createVulkanImage(colorAttachmentInfo, colorImage, false) != RET_SUCCESS) {
					return RET_FAILED;
				}
				m_colorAttachments.push_back(colorImage);
			}
		}

		/// Only create Render Pass if use didn't specify one.
		if(VK_NULL_HANDLE == m_renderPass) {
			if(createRenderPass() != RET_SUCCESS) {
				return RET_FAILED;
			}
		}

		VkFramebufferCreateInfo framebufferCreateInfo {};
		framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferCreateInfo.renderPass = m_renderPass;
		framebufferCreateInfo.width = m_extent2D.width;
		framebufferCreateInfo.height = m_extent2D.height;
		framebufferCreateInfo.layers = 1;

		for(auto& colorAttachment : m_colorAttachments) {
			if(m_depthStencilImage.imageView) {
				m_attachments.push_back(colorAttachment.imageView);
			}
		}

		if(m_depthStencilImage.imageView != VK_NULL_HANDLE)  {
			m_attachments.push_back(m_depthStencilImage.imageView);
		}

		framebufferCreateInfo.attachmentCount = (uint32_t)m_attachments.size();
		framebufferCreateInfo.pAttachments = m_attachments.data();

		VkResult result = vkCreateFramebuffer(m_device, &framebufferCreateInfo, nullptr, &m_frameBuffer);
		if(VK_SUCCESS != result) {
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanFrameBufferImpl::create(xdl_uint width, xdl_uint height) {
		return create(m_instance, m_physicalDevice, m_device, width, height);
	}

	xdl_int XdevLVulkanFrameBufferImpl::createVulkanImage(const XdevLVulkanCreateImageInfo& info, XdevLVulkanImage& vulkanImage, bool depthStencil) {

		VkImageCreateInfo image {};
		image.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		image.imageType = VK_IMAGE_TYPE_2D;
		image.format = info.format;
		image.extent = { m_extent2D.width, m_extent2D.height, 1 };
		image.mipLevels = 1;
		image.arrayLayers = 1;
		image.samples = VK_SAMPLE_COUNT_1_BIT;
		image.tiling = VK_IMAGE_TILING_OPTIMAL;
		image.usage = info.usage | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		image.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		auto result = vkCreateImage(m_device, &image, nullptr, &vulkanImage.image);
		if(VK_SUCCESS != result) {
			return RET_FAILED;
		}

		VkMemoryAllocateInfo memAlloc {};
		memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		VkMemoryRequirements memReqs;
		vkGetImageMemoryRequirements(m_device, vulkanImage.image, &memReqs);
		memAlloc.allocationSize = memReqs.size;
		memAlloc.memoryTypeIndex = xdl::getMemoryTypeIndex(m_physicalDeviceMemoryProperties, memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		result = vkAllocateMemory(m_device, &memAlloc, nullptr, &vulkanImage.memory);
		if(VK_SUCCESS != result) {
			return RET_FAILED;
		}

		result = vkBindImageMemory(m_device, vulkanImage.image, vulkanImage.memory, 0);
		if(VK_SUCCESS != result) {
			return RET_FAILED;
		}

		VkImageViewCreateInfo imageViewCreateInfo {};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.format = info.format;
		imageViewCreateInfo.subresourceRange = {};
		if(depthStencil) {
			imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
		} else {
			imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		}
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;
		imageViewCreateInfo.image = vulkanImage.image;

		result = vkCreateImageView(m_device, &imageViewCreateInfo, nullptr, &vulkanImage.imageView);
		if(VK_SUCCESS != result) {
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanFrameBufferImpl::addDepthStencil(VkFormat depthStencilFormat, const XdevLVulkanImage& colorTargetImage) {
//		auto tmp = getSupportedDepthFormats(m_physicalDevice);
//		auto found = std::find(tmp.begin(), tmp.end(), depthStencilFormat);
//		if(tmp.end() == found) {
//			XDEVL_MODULEX_ERROR(XdevLVulkanFrameBuffer, "Specified DepthStencil format not supported.\n");
//			return RET_FAILED;
//		}
		m_depthStencilInfo.format = depthStencilFormat;
		m_depthStencilInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		m_depthStencilInfo.create = xdl_false;

		m_depthStencilImage = colorTargetImage;

		m_depthStencilImageSpecified = xdl_true;
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanFrameBufferImpl::addDepthStencil(VkFormat depthStencilFormat) {
//		auto tmp = getSupportedDepthFormats(m_physicalDevice);
//		auto found = std::find(tmp.begin(), tmp.end(), depthStencilFormat);
//		if(tmp.end() == found) {
//			XDEVL_MODULEX_ERROR(XdevLVulkanFrameBuffer, "Specified DepthStencil format not supported.\n");
//			return RET_FAILED;
//		}

		m_depthStencilInfo.format = depthStencilFormat;
		m_depthStencilInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanFrameBufferImpl::createRenderPass() {


		uint32_t attechmentIndexMax = 0;

		// TODO Let the user specify this.
		std::vector<VkAttachmentReference> colorAttachments;
		for(auto& colorAttachmentInfo : m_colorAttachmentsInfo) {

			attechmentIndexMax = std::max(attechmentIndexMax, colorAttachmentInfo.attachment);

			VkAttachmentReference colorReference {};
			colorReference.attachment = colorAttachmentInfo.attachment;
			colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			colorAttachments.push_back(colorReference);
		}

		std::array<VkSubpassDependency, 1> dependencies {};

		// First dependency at the start of the renderpass
		// Does the transition from final to initial layout
		dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
		dependencies[0].dstSubpass = 0;
		dependencies[0].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[0].srcAccessMask = 0;
		dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		VkSubpassDescription subpassDescription {};
		subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpassDescription.colorAttachmentCount = (uint32_t)colorAttachments.size();
		subpassDescription.pColorAttachments = colorAttachments.data();

		std::vector<VkAttachmentDescription> attachments;
		for(auto& colorAttachmentInfo : m_colorAttachmentsInfo) {
			VkAttachmentDescription colorAttachmentDescription {};
			colorAttachmentDescription.format = colorAttachmentInfo.format;
			colorAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
			colorAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			colorAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			colorAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			colorAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			colorAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			colorAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

			attachments.push_back(colorAttachmentDescription);
		}

		// Add Depth/Stencil only if user specified one.
		if(VK_FORMAT_UNDEFINED != m_depthStencilInfo.format) {
			VkAttachmentReference depthReference {0, VK_IMAGE_LAYOUT_UNDEFINED};
			depthReference.attachment = attechmentIndexMax + 1;
			depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			subpassDescription.pDepthStencilAttachment = &depthReference;

			VkAttachmentDescription depthStencilAttachmentDescription {};
			depthStencilAttachmentDescription.format = m_depthStencilInfo.format;
			depthStencilAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
			depthStencilAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			depthStencilAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			depthStencilAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			depthStencilAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			depthStencilAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			depthStencilAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			attachments.push_back(depthStencilAttachmentDescription);
		}


		VkRenderPassCreateInfo renderPassCreateInfo {};
		renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassCreateInfo.attachmentCount = (uint32_t)attachments.size();
		renderPassCreateInfo.pAttachments = attachments.data();
		renderPassCreateInfo.dependencyCount = (uint32_t)dependencies.size();
		renderPassCreateInfo.pDependencies = dependencies.data();
		// TODO Get more into the Subpass of the Render Pass.
		renderPassCreateInfo.subpassCount = 1;
		renderPassCreateInfo.pSubpasses = &subpassDescription;

		auto result = vkCreateRenderPass(m_device, &renderPassCreateInfo, nullptr, &m_renderPass);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanFrameBuffer, "vkCreateRenderPass failed: " << vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanFrameBufferImpl::addColorTarget(xdl_uint index, VkFormat colorTargetFormat, const XdevLVulkanImage& colorTargetImage) {
		XdevLVulkanCreateImageInfo info {};
		info.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		info.format = colorTargetFormat;
		info.create = xdl_false;
		info.attachment = index;

		m_colorAttachmentsInfo.push_back(info);
		m_colorAttachments.push_back(colorTargetImage);
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanFrameBufferImpl::addColorTarget(xdl_uint index, VkFormat colorTargetFormat) {
		XdevLVulkanCreateImageInfo info {};
		info.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		info.format = colorTargetFormat;
		info.attachment = index;

		m_colorAttachmentsInfo.push_back(info);
		return RET_SUCCESS;
	}

	VkRenderPass XdevLVulkanFrameBufferImpl::getRenderPass() const {
		return m_renderPass;
	}

	VkFramebuffer XdevLVulkanFrameBufferImpl::getNativeHandle() const {
		return m_frameBuffer;
	}

	const VkExtent2D& XdevLVulkanFrameBufferImpl::getExtent2D() const {
		return m_extent2D;
	}


}
