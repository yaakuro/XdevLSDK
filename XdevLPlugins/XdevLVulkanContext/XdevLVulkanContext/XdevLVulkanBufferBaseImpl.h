/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_BUFFER_BASE_IMPL_H
#define XDEVL_VULKAN_BUFFER_BASE_IMPL_H

#include <string.h>

#include <XdevLTypes.h>
#include <XdevLVulkanContext/XdevLVulkanBufferBase.h>
#include <XdevLVulkanContext/XdevLVulkanUtils.h>

namespace xdl {

	// Note: We are not using the final keyword here because we will use this in the XdevLRAIVulkan module.

	template<class T, VkBufferUsageFlags TARGET>
	class XdevLVulkanBufferBaseImpl : public T {
		public:

			XdevLVulkanBufferBaseImpl(VkDevice device, VkPhysicalDeviceMemoryProperties memoryProperties) :
				m_device(device),
				m_buffer(VK_NULL_HANDLE),
				m_deviceMemory(VK_NULL_HANDLE),
				m_memoryProperties(memoryProperties),
				m_locked(xdl_false),
				m_mapped(xdl_false),
				m_size(0) {

			}

			virtual ~XdevLVulkanBufferBaseImpl() {
				if(m_deviceMemory) {
					vkFreeMemory(m_device, m_deviceMemory, nullptr);
					m_deviceMemory = VK_NULL_HANDLE;
				}
				if(m_buffer) {
					vkDestroyBuffer(m_device, m_buffer, nullptr);
					m_buffer = VK_NULL_HANDLE;
				}
			}

			xdl_int init() override {
				return RET_SUCCESS;
			}

			xdl_int init(xdl_uint size) override {
				return init(nullptr, size);
			}

			xdl_int init(xdl_uint8* src, xdl_uint size) override {

				//
				// Create the buffer object..
				//
				m_bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
				m_bufferCreateInfo.pNext = nullptr;
				m_bufferCreateInfo.usage = TARGET;
				m_bufferCreateInfo.size = size;
				m_bufferCreateInfo.queueFamilyIndexCount = 0;
				m_bufferCreateInfo.pQueueFamilyIndices = nullptr;
				m_bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
				m_bufferCreateInfo.flags = 0;

				VkResult result = vkCreateBuffer(m_device, &m_bufferCreateInfo, nullptr, &m_buffer);
				if(VK_SUCCESS != result) {
					return RET_FAILED;
				}

				//
				// Allocate the memory for the buffer object.
				//
				vkGetBufferMemoryRequirements(m_device, m_buffer, &m_memoryRequirements);

				VkMemoryAllocateInfo memoryAllocateInfo {};
				memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
				memoryAllocateInfo.memoryTypeIndex = getMemoryTypeIndex(m_memoryProperties, m_memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
				memoryAllocateInfo.allocationSize = m_memoryRequirements.size;

				result = vkAllocateMemory(m_device, &memoryAllocateInfo, nullptr, &(m_deviceMemory));
				if(VK_SUCCESS != result) {
					return RET_FAILED;
				}
				m_size = size;

				//
				// Copy data into buffer if specified.
				//
				if(nullptr != src) {
					lock();
					if(upload(src, size, 0) != RET_SUCCESS) {
						unlock();
						return RET_FAILED;
					}
					unlock();
				}

				//
				// Bind the memory to the buffer object.
				//
				result = vkBindBufferMemory(m_device, m_buffer, m_deviceMemory, 0);
				if(VK_SUCCESS != result) {
					return RET_FAILED;
				}
				return RET_SUCCESS;
			}

			xdl_int lock() override {
				assert(!m_locked && "XdevLVulkanBufferBase::lock: Was locked already.");
				m_locked = xdl_true;
				return RET_SUCCESS;
			}

			xdl_int unlock() override {
				assert(m_locked && "XdevLVulkanBufferBase::unlock: Was not locked.");
				m_locked = xdl_false;
				return RET_SUCCESS;
			}

			xdl_uint8* map(VkMemoryMapFlags mapFlags) override {
				assert(m_locked && "XdevLVulkanBufferBase::map: Was not locked.");
				assert(!m_mapped && "XdevLVulkanBufferBase::map: Was mapped already.");

				xdl_uint8* pData;
				VkResult result = vkMapMemory(m_device, m_deviceMemory, 0, m_memoryRequirements.size, mapFlags, (void **)&pData);
				if(VK_SUCCESS != result) {
					return nullptr;
				}
				m_mapped = xdl_true;
				return pData;
			}

			xdl_int unmap() override {
				assert(m_mapped && "XdevLVulkanBufferBase::map: Was not mapped.");

				vkUnmapMemory(m_device, m_deviceMemory);
				m_mapped = xdl_false;
				return RET_SUCCESS;
			}

			// TODO If offset != 0 we have to check if the total size will be exceeded.
			xdl_int upload(xdl_uint8* src, xdl_uint size, xdl_uint64 offset) override {
				assert(m_locked && "XdevLVulkanBufferBase::unlock: Was not locked.");

				xdl_uint8* buffer;
				VkResult result = vkMapMemory(m_device, m_deviceMemory, offset, m_memoryRequirements.size, 0, (void **)&buffer);
				if(VK_SUCCESS != result) {
					return RET_FAILED;
				}

				memcpy(buffer, src, size);

				vkUnmapMemory(m_device, m_deviceMemory);
				return RET_SUCCESS;
			}

			xdl_uint id() override {
				return 0;
			}

			xdl_uint getSize() override {
				return m_size;
			}

			VkBuffer getBufferHandle() const override {
				return m_buffer;
			}

		protected:

			VkDevice m_device;
			VkBufferCreateInfo m_bufferCreateInfo;
			VkBuffer m_buffer;
			VkDeviceMemory m_deviceMemory;
			VkMemoryRequirements m_memoryRequirements;
			VkPhysicalDeviceMemoryProperties m_memoryProperties;
			xdl_bool m_locked;
			xdl_bool m_mapped;
			xdl_uint m_size;
	};

}

#endif
