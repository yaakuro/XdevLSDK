/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_SWAPCHAIN_IMPL_H
#define XDEVL_VULKAN_SWAPCHAIN_IMPL_H

#include "XdevLVulkanDeviceImpl.h"
#include "XdevLVulkanSurfaceImpl.h"

namespace xdl {

	class XdevLVulkanFrameBufferImpl;

	/**
	 * @class XdevLVulkanSwapChainImpl
	 * @brief Implementation of the XdevLVulkanSwapChain interface.
	 * @author Cengiz Terzibas
	 */
	class XdevLVulkanSwapChainImpl : public XdevLVulkanSwapChain {
		public:
			XdevLVulkanSwapChainImpl();
			XdevLVulkanSwapChainImpl(XdevLVulkanInstanceImpl* instance, IPXdevLVulkanDevice device, XdevLVulkanSurfaceImpl* surface);

			virtual ~XdevLVulkanSwapChainImpl();

			xdl_int create() override final;
			xdl_int create(VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, VkFormat imageFormat, VkFormat depthStencilFormat, VkColorSpaceKHR colorSpace, const VkSurfaceCapabilitiesKHR& surfaceCapabilities, const IPXdevLQueueFamilyIndices& queueFamilyIndices) override final;
			xdl_int createFramebuffers() override final;
			xdl_int createFramebuffers(VkRenderPass renderPass) override final;
			xdl_int acquireNextImage(VkImage& image, VkFramebuffer& framebuffer, VkSemaphore presentSemaphore) override final;
			xdl_int swapBuffer(VkQueue queue, VkSemaphore renderSemaphore) override final;
			const std::vector<XdevLVulkanImage>& getSwapChainImages() const override final;
			std::vector<VkFramebuffer> getSwapChainFramebuffers() const override final;
			VkExtent2D getExtent2D() const override final;
			VkSwapchainKHR getNativeHandle() const override final;
			const VkSwapchainKHR* getNativeHandlePtr() const override final;
			VkRenderPass getRenderPass() const override final;
			void setupDepthStencil();
			void setupRenderPass();

		private:

			void connectFunctions(VkDevice device);

			PFN_vkCreateSwapchainKHR fpCreateSwapchainKHR;
			PFN_vkDestroySwapchainKHR fpDestroySwapchainKHR;
			PFN_vkGetSwapchainImagesKHR fpGetSwapchainImagesKHR;
			PFN_vkAcquireNextImageKHR fpAcquireNextImageKHR;
			PFN_vkQueuePresentKHR fpQueuePresentKHR;

		private:
			XdevLVulkanInstanceImpl* m_instanceImpl;
			XdevLVulkanDeviceImpl* m_deviceImpl;
			XdevLVulkanSurfaceImpl* m_surfaceImpl;
			VkInstance m_instance;
			VkPhysicalDevice m_physicalDevice;
			VkDevice m_device;
			VkSurfaceKHR m_surface;
			VkFormat m_imageFormat;
			VkFormat m_depthStencilFormat;
			VkColorSpaceKHR m_colorSpace;
			VkPhysicalDeviceMemoryProperties m_physicalDeviceMemoryProperties;
			VkRenderPass m_renderPass;
			VkSwapchainKHR m_swapChain;
			VkSwapchainKHR m_swapChainOld;
			std::vector<XdevLVulkanImage> m_swapChainImages;
			std::vector<XdevLVulkanFrameBufferImpl*> m_frameBuffersImpl;
			std::vector<VkFramebuffer> m_frameBuffers;
			XdevLVulkanImage m_depthStencilImage;
			xdl::xdl_uint32 m_currentBuffer;
			VkExtent2D m_extent2D;
			static xdl_bool m_connectFunctions;
	};

}


#endif
