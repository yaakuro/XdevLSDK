/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_TEXTURE_IMPL_H
#define XDEVL_VULKAN_TEXTURE_IMPL_H

#include <XdevLVulkanContext/XdevLVulkanContext.h>

namespace xdl {

	//
	// Forward declarations.
	//
	class XdevLVulkanInstanceImpl;
	class XdevLVulkanDeviceImpl;

	/**
	 * @class XdevLVulkanTextureImpl
	 * @brief Implementation of the XdevLVulkanTexture internface.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLVulkanTextureImpl : public XdevLVulkanTexture {
		public:
			XdevLVulkanTextureImpl();
			XdevLVulkanTextureImpl(XdevLVulkanInstanceImpl* instanceImpl, XdevLVulkanDeviceImpl* deviceImpl);
			virtual ~XdevLVulkanTextureImpl();

			xdl_int create(xdl_uint width, xdl_uint height, VkFormat format) override final;
			xdl_int create(VkDevice device, VkPhysicalDeviceMemoryProperties deviceMemoryProperties, xdl_uint width, xdl_uint height, VkFormat format) override final;
			xdl_int lock() override;
			xdl_int unlock() override;
			xdl_uint8* map(VkMemoryMapFlags mapFlags) override final;
			xdl_int unmap() override final;
			xdl_int upload(xdl_uint8* src, xdl_uint size, xdl_uint64 offset) override final;
			VkImageView getNativeHandle() const override final;
			VkSubresourceLayout getSubresourceLayout() const override final;

			const XdevLVulkanImage& getVulkanImage() const;
			VkFormat getFormat() const;

		private:

			XdevLVulkanInstanceImpl* m_instanceImpl;
			XdevLVulkanDeviceImpl* m_deviceImpl;
			VkDevice m_device;
			VkFormat m_format;
			XdevLVulkanImage m_vulkanImage;
			VkMemoryRequirements m_memoryRequirements;
			VkSubresourceLayout m_imageLayout;
			VkExtent2D m_extent2D;
			xdl_bool m_locked;
			xdl_bool m_mapped;
			xdl_uint m_size;
	};

}


#endif
