/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLVulkanTextureImpl.h"
#include "XdevLVulkanInstanceImpl.h"
#include "XdevLVulkanDeviceImpl.h"

namespace xdl {

	XdevLVulkanTextureImpl::XdevLVulkanTextureImpl()
		: m_instanceImpl(nullptr)
		, m_deviceImpl(nullptr)
		, m_device(VK_NULL_HANDLE)
		, m_vulkanImage {}
		, m_memoryRequirements {}
		, m_locked(xdl_false)
		, m_mapped(xdl_false)
		, m_size(0) {

	}

	XdevLVulkanTextureImpl::XdevLVulkanTextureImpl(XdevLVulkanInstanceImpl* instanceImpl, XdevLVulkanDeviceImpl* deviceImpl)
		: m_instanceImpl(instanceImpl)
		, m_deviceImpl(deviceImpl)
		, m_device(deviceImpl->getNativeHandle())
		, m_format(VK_FORMAT_UNDEFINED)
		, m_vulkanImage {}
		, m_memoryRequirements {}
		, m_locked(xdl_false)
		, m_mapped(xdl_false)
		, m_size(0) {

	}

	XdevLVulkanTextureImpl::~XdevLVulkanTextureImpl() {

		if(VK_NULL_HANDLE != m_vulkanImage.memory) {
			VkDevice device = VK_NULL_HANDLE;
			if(m_deviceImpl) {
				device = m_deviceImpl->getNativeHandle();
			} else {
				device = m_device;
			}

			if(VK_NULL_HANDLE != m_vulkanImage.memory) {
				vkFreeMemory(device, m_vulkanImage.memory, nullptr);
			}

			if(VK_NULL_HANDLE != m_vulkanImage.image) {
				vkDestroyImage(device, m_vulkanImage.image, nullptr);
			}

			if(VK_NULL_HANDLE != m_vulkanImage.imageView) {
				vkDestroyImageView(device, m_vulkanImage.imageView, nullptr);
			}

			m_vulkanImage = {};
		}
	}

	xdl_int XdevLVulkanTextureImpl::create(VkDevice device, VkPhysicalDeviceMemoryProperties deviceMemoryProperties, xdl_uint width, xdl_uint height, VkFormat format) {
		m_device = device;
		m_extent2D.width = width;
		m_extent2D.height = height;
		m_format = format;

		VkImageCreateInfo imageCreateInfo {};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
		imageCreateInfo.format = format;
		imageCreateInfo.extent = { width, height, 1 };
		imageCreateInfo.mipLevels = 1;
		imageCreateInfo.arrayLayers = 1;
		imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCreateInfo.tiling = VK_IMAGE_TILING_LINEAR;
		imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
		imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		VkResult result = vkCreateImage(device, &imageCreateInfo, nullptr, &m_vulkanImage.image);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanTexture, "vkCreateImage failed: " << vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}

		VkMemoryAllocateInfo memoryAllocateInfo {};
		memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;

		vkGetImageMemoryRequirements(device, m_vulkanImage.image, &m_memoryRequirements);
		memoryAllocateInfo.allocationSize = m_memoryRequirements.size;
		memoryAllocateInfo.memoryTypeIndex = xdl::getMemoryTypeIndex(deviceMemoryProperties, m_memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

		// When allocating using VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT Vulkan behaves strange when deleting this memory imitatively after creation. The device gets lost.
		result = vkAllocateMemory(device, &memoryAllocateInfo, nullptr, &m_vulkanImage.memory);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanTexture, "vkAllocateMemory failed: " << vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}

		result = vkBindImageMemory(device, m_vulkanImage.image, m_vulkanImage.memory, 0);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanTexture, "vkBindImageMemory failed: " << vkVkResultToString(result) << std::endl);
			return RET_FAILED;
		}


		//
		// Because we don't know how the pixels are arranged in the memory we have to get the information about it first.
		//
		VkImageSubresource subresource = {};
		subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresource.mipLevel = 0;
		subresource.arrayLayer = 0;

		vkGetImageSubresourceLayout(device, m_vulkanImage.image, &subresource, &m_imageLayout);

		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanTextureImpl::create(xdl_uint width, xdl_uint height, VkFormat format) {
		return create(m_deviceImpl->getNativeHandle(), m_instanceImpl->getPhysicalDeviceMemoryProperties(), width, height, format);
	}

	xdl_int XdevLVulkanTextureImpl::lock() {
		assert(!m_locked && "XdevLVulkanTexture::lock: Was locked already.");
		m_locked = xdl_true;
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanTextureImpl::unlock() {
		assert(m_locked && "XdevLVulkanTexture::unlock: Was not locked.");
		m_locked = xdl_false;
		return RET_SUCCESS;
	}

	xdl_uint8* XdevLVulkanTextureImpl::map(VkMemoryMapFlags mapFlags) {
		assert(m_locked && "XdevLVulkanTexture::map: Was not locked.");
		assert(!m_mapped && "XdevLVulkanTexture::map: Was mapped already.");

		xdl_uint8* pData;
		VkResult result = vkMapMemory(m_deviceImpl->getNativeHandle(), m_vulkanImage.memory, 0, m_memoryRequirements.size, mapFlags, (void **)&pData);
		if(VK_SUCCESS != result) {
			return nullptr;
		}
		m_mapped = xdl_true;
		return pData;

	}

	xdl_int XdevLVulkanTextureImpl::unmap() {
		assert(m_mapped && "XdevLVulkanTexture::map: Was not mapped.");

		vkUnmapMemory(m_deviceImpl->getNativeHandle(), m_vulkanImage.memory);
		m_mapped = xdl_false;
		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanTextureImpl::upload(xdl_uint8* src, xdl_uint size, xdl_uint64 offset) {
		assert(m_locked && "XdevLVulkanTexture::unlock: Was not locked.");

		xdl_uint8* buffer;
		VkResult result = vkMapMemory(m_deviceImpl->getNativeHandle(), m_vulkanImage.memory, offset, m_memoryRequirements.size, 0, (void **)&buffer);
		if(VK_SUCCESS != result) {
			return RET_FAILED;
		}

		//
		// We can't just copy with memcpy because we don't know how the pixels are arranged.
		//
		if(m_imageLayout.rowPitch == m_extent2D.width * 4) {
			memcpy(buffer, src, size);
		} else {
			uint8_t* dataBytes = reinterpret_cast<uint8_t*>(buffer);
			for(uint32_t y = 0; y < m_extent2D.height; y++) {
				memcpy(&dataBytes[y * m_imageLayout.rowPitch], &src[y * m_extent2D.width * 4], m_extent2D.width * 4);
			}
		}

		vkUnmapMemory(m_deviceImpl->getNativeHandle(), m_vulkanImage.memory);
		return RET_SUCCESS;

	}

	VkImageView XdevLVulkanTextureImpl::getNativeHandle() const {
		return m_vulkanImage.imageView;
	}

	VkSubresourceLayout XdevLVulkanTextureImpl::getSubresourceLayout() const {
		return m_imageLayout;
	}

	const XdevLVulkanImage& XdevLVulkanTextureImpl::getVulkanImage() const {
		return m_vulkanImage;
	}

	VkFormat XdevLVulkanTextureImpl::getFormat() const {
		return m_format;
	}

}
