/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <vector>

#include "XdevLVulkanSurfaceImpl.h"
#include "XdevLVulkanSwapChainImpl.h"

namespace xdl {

	xdl_bool XdevLVulkanSurfaceImpl::m_connectFunctions = xdl_false;

	XdevLVulkanSurfaceImpl::XdevLVulkanSurfaceImpl()
		: m_instanceImpl(nullptr)
		, m_instance(VK_NULL_HANDLE)
		, m_surface(VK_NULL_HANDLE)
		, m_presentQueueNodeIndex(UINT32_MAX) {
	}

	XdevLVulkanSurfaceImpl::XdevLVulkanSurfaceImpl(XdevLVulkanInstanceImpl* instance)
		: m_instanceImpl(instance)
		, m_instance(VK_NULL_HANDLE)
		, m_surface(VK_NULL_HANDLE)
		, m_presentQueueNodeIndex(UINT32_MAX) {
	}

	XdevLVulkanSurfaceImpl::~XdevLVulkanSurfaceImpl() {
		if(VK_NULL_HANDLE != m_surface) {
			vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
			m_surface = VK_NULL_HANDLE;
		}
	}

	void XdevLVulkanSurfaceImpl::connectFunctions(VkInstance instance) {
		GET_INSTANCE_PROC_ADDR(instance, GetPhysicalDeviceSurfaceSupportKHR);
		GET_INSTANCE_PROC_ADDR(instance, GetPhysicalDeviceSurfaceCapabilitiesKHR);
		GET_INSTANCE_PROC_ADDR(instance, GetPhysicalDeviceSurfaceFormatsKHR);
		GET_INSTANCE_PROC_ADDR(instance, GetPhysicalDeviceSurfacePresentModesKHR);
		m_connectFunctions = xdl_true;
	}

	VkSurfaceKHR XdevLVulkanSurfaceImpl::getNativeHandle() const {
		return m_surface;
	}

	xdl_int XdevLVulkanSurfaceImpl::create(IPXdevLWindow window) {
		XDEVL_ASSERT(nullptr != m_instanceImpl, "When using XdevLVulkanSurface::create(IPXdevLWindow window) method you must retrieve this XdevLVulkanSurface object from a valid XdevLVulkanInstance object.");

		//
		// Create the Vulkan surface using native handles.
		//
		VkInstance instance = (VkInstance)m_instanceImpl->getNativeHandle();
		VkPhysicalDevice physicalDevice = m_instanceImpl->getPhysicalDevice();

		if(create(instance, physicalDevice, window, m_instanceImpl->getQueueFamilyIndices()) == RET_FAILED) {
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLVulkanSurfaceImpl::create(VkInstance instance, VkPhysicalDevice physicalDevice, IPXdevLWindow window, const IPXdevLQueueFamilyIndices& queueFamilyIndices) {
		if(!m_connectFunctions) {
			connectFunctions(instance);
		}
		m_instance = instance;

#if XDEVL_PLATFORM_WINDOWS
		HINSTANCE hinstance = static_cast<HINSTANCE>(window->getInternal(XdevLInternalName("WINDOW_INSTANCE")));
		HWND hwnd = (HWND)(window->getInternal(XdevLInternalName("WINDOW_HWND")));

		VkWin32SurfaceCreateInfoKHR surfaceCreateInfo {};
		surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		surfaceCreateInfo.hinstance = hinstance;
		surfaceCreateInfo.hwnd = hwnd;
		VkResult result = vkCreateWin32SurfaceKHR(instance, &surfaceCreateInfo, nullptr, &m_surface);
#elif XDEVL_PLATFORM_ANDROID
		ANativeWindow* nativeWindow = static_cast<ANativeWindow*>(window->getInternal(XdevLInternalName("NATIVE_WINDOW")));

		VkAndroidSurfaceCreateInfoKHR surfaceCreateInfo;
		surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
		surfaceCreateInfo.window = nativeWindow;
		VkResult result = vkCreateAndroidSurfaceKHR(instance, &surfaceCreateInfo, nullptr, &m_surface);
#elif XDEVL_PLATFORM_LINUX
		Display* display = static_cast<Display*>(window->getInternal(XdevLInternalName("X11_DISPLAY")));
		if(nullptr == display) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "Could not get native X11 display information.\n");
			return RET_FAILED;
		}

		Window wnd = (Window)(window->getInternal(XdevLInternalName("X11_WINDOW")));
		if(None == wnd) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurfaceImpl, "Could not get native X11 window information.\n");
			return RET_FAILED;
		}

		VkXcbSurfaceCreateInfoKHR surfaceCreateInfo {};
		surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
		surfaceCreateInfo.connection = XGetXCBConnection(display);
		surfaceCreateInfo.window = (xcb_window_t)wnd;

		VkResult result = vkCreateXcbSurfaceKHR(instance, &surfaceCreateInfo, nullptr, &m_surface);
#else
#error "Not supported Vulkan Surface."
#endif
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "Failed to create Vulkan surface: " << vkVkResultToString(result) << std::endl);
			return result;
		}
		XDEVL_MODULEX_SUCCESS(XdevLVulkanSurface, "Vulkan Surface created successful.\n");

		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilyProperties.data());

		std::vector<VkBool32> supportsPresent(queueFamilyProperties.size());

		for(uint32_t i = 0; i < queueFamilyProperties.size(); i++) {
			fpGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, m_surface, &supportsPresent[i]);

			VkBool32 supported = VK_FALSE;
			VkResult result = vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, m_surface, &supported);
			if(VK_SUCCESS != result) {
				XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "vkGetPhysicalDeviceSurfaceSupportKHR failed: " << vkVkResultToString(result) << std::endl);
				return result;
			}
			if (queueFamilyProperties[i].queueCount > 0 && supported) {
				m_presentQueueNodeIndex = i;
			}
		}

		if (m_presentQueueNodeIndex == UINT32_MAX) {
			// If there's no queue that supports both present and graphics
			// try to find a separate present queue
			for (uint32_t i = 0; i < queueFamilyProperties.size(); ++i) {
				if (supportsPresent[i] == VK_TRUE) {
					m_presentQueueNodeIndex = i;
					break;
				}
			}
		}
		// Exit if either a graphics or a presenting queue hasn't been found
		if (m_presentQueueNodeIndex == UINT32_MAX) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "Could not find a graphics and/or presenting queue.\n");
		}
		// Get list of supported surface formats
		uint32_t formatCount = 0;
		result = fpGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, m_surface, &formatCount, nullptr);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "vkGetPhysicalDeviceSurfaceFormatsKHR failed: " << vkVkResultToString(result) << std::endl);
			return result;
		}

		std::vector<VkSurfaceFormatKHR> surfaceFormats(formatCount);
		result = fpGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, m_surface, &formatCount, surfaceFormats.data());
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "vkGetPhysicalDeviceSurfaceFormatsKHR failed: " << vkVkResultToString(result) << std::endl);
			return result;
		}

		// If the surface format list only includes one entry with VK_FORMAT_UNDEFINED,
		// there is no preferered format, so we assume VK_FORMAT_B8G8R8A8_UNORM
		if((formatCount == 1) && (surfaceFormats[0].format == VK_FORMAT_UNDEFINED)) {
			m_colorFormat = VK_FORMAT_B8G8R8A8_UNORM;
		} else {
			// Always select the first available color format
			// If you need a specific format (e.g. SRGB) you'd need to
			// iterate over the list of available surface format and
			// check for it's presence
			m_colorFormat = surfaceFormats[0].format;
		}
		surfaceFormats.clear();


		m_colorSpace = surfaceFormats[0].colorSpace;

		// Get physical device surface properties and formats.
		result = fpGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, m_surface, &m_surfCaps);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR failed: " << vkVkResultToString(result) << std::endl);
			return result;
		}

		if(m_surfCaps.currentExtent.width != UINT32_MAX) {
			m_extent2D.width = m_surfCaps.currentExtent.width;
			m_extent2D.height = m_surfCaps.currentExtent.height;
		}

		//
		// Get the number of Present Modes. (That is how stuff are drawn like using VSYNC or not ...)
		//
		uint32_t presentModeCount = 0;
		result = fpGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, m_surface, &presentModeCount, nullptr);
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "vkGetPhysicalDeviceSurfacePresentModesKHR failed: " << vkVkResultToString(result) << std::endl);
			return result;
		}

		// Use the number of modes and create an array of modes.
		std::vector<VkPresentModeKHR> presentModes(presentModeCount);
		result = fpGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, m_surface, &presentModeCount, presentModes.data());
		if(VK_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLVulkanSurface, "vkGetPhysicalDeviceSurfacePresentModesKHR failed: " << vkVkResultToString(result) << std::endl);
			return result;
		}

		//
		// Select the Present mode we prefer.
		//
		m_presentMode = VK_PRESENT_MODE_FIFO_KHR;
		for(const auto& presentMode : presentModes) {
			XDEVL_MODULEX_INFO(XdevLVulkanSurface, "Supported SwapChain Present Modes: " << vkVkPresentModeKHRToString(presentMode) << std::endl);

			// We will try to use the Present Mode that waits for VSYNC.
			if(presentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				m_presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
				break;
			}

			if((m_presentMode != VK_PRESENT_MODE_MAILBOX_KHR) && (presentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)) {
				m_presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
			}
		}

		//
		// Store the present queue index into the XdevLVulkanInstance object.
		//
		queueFamilyIndices->setPresentQueueIndex(m_presentQueueNodeIndex);
		return RET_SUCCESS;
	}


	const VkSurfaceCapabilitiesKHR& XdevLVulkanSurfaceImpl::getSurfaceCapabilities() const {
		return m_surfCaps;
	}

	VkFormat XdevLVulkanSurfaceImpl::getColorFormat() const  {
		return m_colorFormat;
	}

	VkColorSpaceKHR XdevLVulkanSurfaceImpl::getColorSpace() const {
		return m_colorSpace;
	}

	VkExtent2D XdevLVulkanSurfaceImpl::getExtend() const {
		return m_extent2D;
	}

	xdl_uint32 XdevLVulkanSurfaceImpl::getPresentFamilyQueueIndex() const {
		return m_presentQueueNodeIndex;
	}

	IPXdevLVulkanSwapChain XdevLVulkanSurfaceImpl::createSwapChain(IPXdevLVulkanDevice device) {
		auto tmp = std::make_shared<XdevLVulkanSwapChainImpl>(m_instanceImpl, device, this);
		return tmp;
	}

}
