/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_DEVICE_H
#define XDEVL_VULKAN_DEVICE_H

#include <XdevLVulkanContext/XdevLVulkanContextFwd.h>

namespace xdl {

	/**
	 * @class XdevLVulkanDevice
	 * @brief Interface for the Vulkan Device handling.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLVulkanDevice {
		public:
			virtual ~XdevLVulkanDevice() {};

			virtual void addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal = xdl_false) = 0;

			/// Don't use this method manually. This will be used when creating the XdevLVulkanContext automatically.
			virtual xdl_int create() = 0;

			/// Manually create the Vulkan Device when not using XdevLVulkanContext.
			virtual xdl_int create(VkPhysicalDevice physicalDevice, const IPXdevLQueueFamilyIndices& queueFamilyIndicies) = 0;

			/// Create a Vertex Shader Module.
			virtual IPXdevLVulkanShader createShader(VkShaderStageFlagBits shaderStageFalgBits, const XdevLString& entryName = XdevLString("main")) = 0;

			/// Create a Vertex Shader.
			virtual IPXdevLVulkanShader createVertexShader(const XdevLString& entryName = XdevLString("main")) = 0;

			/// Create a Fragment Shader.
			virtual IPXdevLVulkanShader createFragmentShader(const XdevLString& entryName = XdevLString("main")) = 0;

			/// Create a Geometry Shader.
			virtual IPXdevLVulkanShader createGeometryShader(const XdevLString& entryName = XdevLString("main")) = 0;

			/// Create a Tessellation Control Shader.
			virtual IPXdevLVulkanShader createTessellationControlShader(const XdevLString& entryName = XdevLString("main")) = 0;

			/// Create a Tessellation Evaluations Shader.
			virtual IPXdevLVulkanShader createTessellationEvaluationShader(const XdevLString& entryName = XdevLString("main")) = 0;

			/// Create a Vertex Buffer Object.
			virtual IPXdevLVulkanVertexBuffer createVertexBuffer() = 0;

			/// Create a Index Buffer Object.
			virtual IPXdevLVulkanIndexBuffer createIndexBuffer() = 0;

			/// Create a Uniform Buffer Object.
			virtual IPXdevLVulkanUniformBuffer createUniformBuffer() = 0;

			/// Creates a Queue.
			virtual IPXdevLVulkanQueue createQueue(XdevLVulkanQueueType queueType) = 0;

			/// Creates a Command Pool
			virtual IPXdevLVulkanCommandPool createCommandPool(XdevLVulkanCommandPoolType commandPoolType) = 0;

			/// Creates a Descriptor Layout.
			virtual IPXdevLVulkanDescriptorLayout createDescriptorLayout() = 0;

			/// Returns the graphics queue index used of this device.
			virtual xdl_uint32 getGraphicsFamilyQueueIndex() const = 0;

			/// Returns the supported Depth/Stencil format.
			virtual VkFormat getSupportedDepthStencilFormat() const = 0;

			/// Returns the native Vulkan Device handle.
			virtual VkDevice getNativeHandle() const = 0;
	};

}


#endif
