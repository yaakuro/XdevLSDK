/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_SHADER_MODULE_H
#define XDEVL_VULKAN_SHADER_MODULE_H

#include <XdevLFileSystem/XdevLFileSystem.h>

namespace xdl {

	/**
	* @class XdevLVulkanShader
	* @brief Interface for the Vulkan Shader Module handling.
	* @author Cengiz Terzibas
	*/
	class XDEVL_EXPORT XdevLVulkanShader {
		public:
			virtual ~XdevLVulkanShader() {};

			/// Don't use this method manually. This will be used when creating the XdevLVulkanContext automatically.
			virtual xdl_int create() = 0;

			/// Manually create the Vulkan Device when not using XdevLVulkanContext.
			virtual xdl_int create(VkDevice device, VkShaderStageFlagBits shaderStageFalgBits, const XdevLString& entryName) = 0;

			/// Load a SPIRV shader from file.
			virtual xdl_int load(IPXdevLFile& file) = 0;

			/// Destroy the current Shader Module.
			virtual void destroy() = 0;

			/// Returns the Shader Stage Flag bits.
			virtual VkShaderStageFlagBits getShaderStageFlagBits() const = 0;

			/// Returns the VkPipelineShaderStageCreateInfo.
			virtual VkPipelineShaderStageCreateInfo getPipelineShaderStageCreateInfo() const = 0;

			/// Returns the entry name of this Shader Module.
			virtual const XdevLString& getEntryName() const = 0;

			/// Returns the native Vulkan Shader Module handle.
			virtual VkShaderModule getNativeHandle() const = 0;
	};

}


#endif
