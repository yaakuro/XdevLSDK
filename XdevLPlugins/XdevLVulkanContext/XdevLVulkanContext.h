/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_CONTEXT_H
#define XDEVL_VULKAN_CONTEXT_H

#include <XdevLModule.h>
#include <XdevLWindow/XdevLWindow.h>

#include <XdevLVulkanContext/XdevLVulkanUtils.h>
#include <XdevLVulkanContext/XdevLVulkanInstance.h>
#include <XdevLVulkanContext/XdevLVulkanDevice.h>
#include <XdevLVulkanContext/XdevLVulkanSurface.h>
#include <XdevLVulkanContext/XdevLVulkanFrameBuffer.h>
#include <XdevLVulkanContext/XdevLVulkanSwapChain.h>
#include <XdevLVulkanContext/XdevLVulkanBufferBase.h>
#include <XdevLVulkanContext/XdevLVulkanVertexBuffer.h>
#include <XdevLVulkanContext/XdevLVulkanIndexBuffer.h>
#include <XdevLVulkanContext/XdevLVulkanUniformBuffer.h>
#include <XdevLVulkanContext/XdevLVulkanShaderModule.h>
#include <XdevLVulkanContext/XdevLVulkanTexture.h>
#include <XdevLVulkanContext/XdevLVulkanQueue.h>
#include <XdevLVulkanContext/XdevLVulkanCommandPool.h>
#include <XdevLVulkanContext/XdevLVulkanDescriptorLayout.h>

namespace xdl {

	/**
		@class XdevLVulkanContext
		@brief Creates the context for Vulkan.
		@author Cengiz Terzibas
	*/
	class XdevLVulkanContext : public XdevLModule {

		public:
			virtual ~XdevLVulkanContext() {};

			/// Pass the list of extensions to enable.
			virtual void addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal = xdl_false) = 0;

			/// Use this method to create a "raw" uninitialzed Vulkan Instance object.
			virtual IPXdevLVulkanInstance createInstance() = 0;

			/// Use this method to create a "raw" uninitialzed Vulkan Device object.
			virtual IPXdevLVulkanDevice createDevice() = 0;

			/// Use this method to create a "raw" uninitialzed Vulkan Surface object.
			virtual IPXdevLVulkanSurface createSurface() = 0;

			/// Use this method to create a "raw" uninitialzed Vulkan Descriptor Layout object.
			virtual IPXdevLVulkanDescriptorLayout createDescriptorLayout() = 0;

			/// Use this method to create a "raw" uninitialized Vulkan FrameBuffer object.
			virtual IPXdevLVulkanFrameBuffer createFrameBuffer() = 0;

			/// Use this method to create a "raw" uninitialized Vulkan SwapChain object.
			virtual IPXdevLVulkanSwapChain createSwapChain() = 0;

			/// Use this method to create a "raw" uninitialized Vulkan Shader Module object.
			virtual IPXdevLVulkanShader createShaderModule() = 0;

			/// Use this method to create a "raw" uninitialized Vulkan Texture object.
			virtual IPXdevLVulkanTexture createTexture() = 0;

			/// Create the Vulkan Instance, Device, Surface and SwapChain.
			virtual xdl_int createContext(IPXdevLWindow window, const VkApplicationInfo& attributes = VkApplicationInfo()) = 0;

			/// Get the IPXdevLInstance that was created by the createContext method.
			virtual IPXdevLVulkanInstance getInstance() const = 0;

			/// Get the IPXdevLDevice that was created by the createContext method.
			virtual IPXdevLVulkanDevice getDevice() const = 0;

			/// Get the IPXdevLSurface that was created by the createContext method.
			virtual IPXdevLVulkanSurface getSurface() const = 0;

			/// Get the IPXdevLSwapChain that was created by the createContext method.
			virtual IPXdevLVulkanSwapChain getSwapChain() const = 0;

			virtual xdl_vptr getProcAddress(const XdevLString& func) = 0;
	};

	using IXdevLVulkanContext = XdevLVulkanContext;
	using IPXdevLVulkanContext = XdevLVulkanContext*;

	XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLVulkanContext)
}


#endif
