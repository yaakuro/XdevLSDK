/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_SURFACE_H
#define XDEVL_VULKAN_SURFACE_H

#include <XdevLVulkanContext/XdevLVulkanContextFwd.h>

namespace xdl {

	/**
	 * @class XdevLVulkanSurface
	 * @brief Interface for the Vulkan Surface handling.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLVulkanSurface {
		public:
			virtual ~XdevLVulkanSurface() {};

			/// Create the Surface based on a window.
			virtual xdl_int create(IPXdevLWindow window) = 0;

			/// Create the Surface using native handles and IPXdevLWindow.
			virtual xdl_int create(VkInstance instance, VkPhysicalDevice physicalDevice, IPXdevLWindow window, const IPXdevLQueueFamilyIndices& queueFamilyIndices) = 0;

			/// Create a SwapChain using this Surface.
			virtual IPXdevLVulkanSwapChain createSwapChain(IPXdevLVulkanDevice device) = 0;

			/// Get the Surface capabilities.
			virtual const VkSurfaceCapabilitiesKHR& getSurfaceCapabilities() const = 0;

			/// Returns the Queue Index form the Present Family.
			virtual xdl_uint32 getPresentFamilyQueueIndex() const = 0;

			/// Returns the color format of the surface.
			virtual VkFormat getColorFormat() const = 0;

			/// Returns the color space.
			virtual VkColorSpaceKHR getColorSpace() const = 0;

			/// Returns the extend.
			virtual VkExtent2D getExtend() const = 0;

			/// Returns the native handle.
			virtual VkSurfaceKHR getNativeHandle() const = 0;
	};

}


#endif
