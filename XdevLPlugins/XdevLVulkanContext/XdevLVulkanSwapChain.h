/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_SWAPCHAIN_H
#define XDEVL_VULKAN_SWAPCHAIN_H

namespace xdl {

	/**
	 * @class XdevLVulkanSwapChain
	 * @brief Interface for the Vulkan SwapChain handling.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLVulkanSwapChain {
		public:
			virtual ~XdevLVulkanSwapChain() {};

			/// Don't use this method manually. This will be used when creating the XdevLVulkanContext automatically.
			virtual xdl_int create() = 0;

			/// Manually create the Vulkan SwapChain when not using XdevLVulkanContext.
			virtual xdl_int create(VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, VkFormat imageFormat, VkFormat depthStencilFormat, VkColorSpaceKHR colorSpace, const VkSurfaceCapabilitiesKHR& surfaceCapabilities, const IPXdevLQueueFamilyIndices& queueFamilyIndices) = 0;

			// Create framebuffers using interal Render Pass.
			virtual xdl_int createFramebuffers() = 0;

			/// Create the framebuffers using a specified RenderPass.
			virtual xdl_int createFramebuffers(VkRenderPass renderPass) = 0;

			/// Aquire the next image for rendering.
			virtual xdl_int acquireNextImage(VkImage& image, VkFramebuffer& framebuffer, VkSemaphore presentSemaphore = VK_NULL_HANDLE) = 0;

			/// Swap to the next SwapChain image.
			virtual xdl_int swapBuffer(VkQueue queue, VkSemaphore renderSemaphore) = 0;

			/// Returns the internal RenderPass.
			virtual VkRenderPass getRenderPass() const = 0;

			/// Returns all SwapChain images.
			virtual const std::vector<XdevLVulkanImage>& getSwapChainImages() const = 0;

			/// Returns the SwapChain framebuffers.
			virtual std::vector<VkFramebuffer> getSwapChainFramebuffers() const = 0;

			/// Returns the extent of this SwapChain.
			virtual VkExtent2D getExtent2D() const = 0;

			/// Returns the native Vulkan SwapChain handle.
			virtual VkSwapchainKHR getNativeHandle() const = 0;
			virtual const VkSwapchainKHR* getNativeHandlePtr() const = 0;
	};

}


#endif
