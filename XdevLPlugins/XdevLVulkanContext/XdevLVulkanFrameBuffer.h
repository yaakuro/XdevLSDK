/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_FRAME_BUFFER_H
#define XDEVL_VULKAN_FRAME_BUFFER_H

namespace xdl {

	/**
	 * @class XdevLVulkanFrameBuffer
	 * @brief Interface for the Vulkan Framebuffer handling.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLVulkanFrameBuffer {
		public:
			virtual ~XdevLVulkanFrameBuffer() {};

			/// Creates the framebuffer.
			virtual xdl_int create(xdl_uint width, xdl_uint height) = 0;
			virtual xdl_int create(VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device, xdl_uint width, xdl_uint height) = 0;
			/// Add a Color Target layer using XdevLVulkanImage.
			virtual xdl_int addColorTarget(xdl_uint index, VkFormat colorTargetFormat, const XdevLVulkanImage& colorTargetImage) = 0;

			/// Adds a Color Target layer to the framebuffer.
			virtual xdl_int addColorTarget(xdl_uint index, VkFormat colorTargetFormat) = 0;

			/// Add a Depth/Stencil layer using XdevLVulkanImage.
			virtual xdl_int addDepthStencil(VkFormat depthStencilFormat, const XdevLVulkanImage& colorTargetImage) = 0;

			/// Adds a Depth/Stencil layer to the framebuffer.
			virtual xdl_int addDepthStencil(VkFormat depthStencilFormat) = 0;

			/// Sets the RenderPass to use.
			virtual xdl_int setRenderPass(VkRenderPass renderPass) = 0;

			virtual XdevLVulkanImage getColorAttachment(xdl_uint idx) = 0;

			/// Returns the internal used Render Pass.
			virtual VkRenderPass getRenderPass() const = 0;

			/// Returns the Vulkan native handle.
			virtual VkFramebuffer getNativeHandle() const = 0;
	};

}


#endif
