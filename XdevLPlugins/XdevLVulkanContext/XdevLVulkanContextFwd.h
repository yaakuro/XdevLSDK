/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_CONTEXT_FWD_H
#define XDEVL_VULKAN_CONTEXT_FWD_H

namespace xdl {

	class XdevLVulkanInstance;
	class XdevLVulkanDevice;
	class XdevLVulkanSurface;
	class XdevLVulkanSwapChain;
	class XdevLVulkanShader;
	class XdevLVulkanBufferBase;
	class XdevLVulkanVertexBuffer;
	class XdevLVulkanIndexBuffer;
	class XdevLVulkanUniformBuffer;
	class XdevLVulkanFrameBuffer;
	class XdevLVulkanTexture;
	class XdevLQueueFamilyIndices;
	class XdevLVulkanQueue;
	class XdevLVulkanCommandPool;
	class XdevLVulkanDescriptorLayout;

	enum class XdevLVulkanQueueType;
	enum class XdevLVulkanCommandPoolType;

	typedef XdevLVulkanInstance							IXdevLVulkanInstance;
	typedef std::shared_ptr<XdevLVulkanInstance>		IPXdevLVulkanInstance;
	typedef XdevLVulkanDevice							IXdevLVulkanDevice;
	typedef std::shared_ptr<XdevLVulkanDevice>			IPXdevLVulkanDevice;
	typedef XdevLVulkanSurface							IXdevLVulkanSurface;
	typedef std::shared_ptr<XdevLVulkanSurface>			IPXdevLVulkanSurface;
	typedef XdevLVulkanSwapChain						IXdevLVulkanSwapChain;
	typedef std::shared_ptr<XdevLVulkanSwapChain>		IPXdevLVulkanSwapChain;
	typedef XdevLVulkanShader							IXdevLVulkanShader;
	typedef std::shared_ptr<XdevLVulkanShader>			IPXdevLVulkanShader;
	typedef XdevLVulkanVertexBuffer						IXdevLVulkanVertexBuffer;
	typedef std::shared_ptr<XdevLVulkanVertexBuffer>	IPXdevLVulkanVertexBuffer;
	typedef XdevLVulkanIndexBuffer						IXdevLVulkanIndexBuffer;
	typedef std::shared_ptr<XdevLVulkanIndexBuffer>		IPXdevLVulkanIndexBuffer;
	typedef XdevLVulkanUniformBuffer					IXdevLVulkanUniformBuffer;
	typedef std::shared_ptr<XdevLVulkanUniformBuffer>	IPXdevLVulkanUniformBuffer;
	typedef XdevLVulkanFrameBuffer						IXdevLVulkanFrameBuffer;
	typedef std::shared_ptr<XdevLVulkanFrameBuffer>		IPXdevLVulkanFrameBuffer;
	typedef XdevLVulkanTexture							IXdevLVulkanTexture;
	typedef std::shared_ptr<XdevLVulkanTexture>			IPXdevLVulkanTexture;
	typedef XdevLQueueFamilyIndices						IXdevLQueueFamilyIndices;
	typedef std::shared_ptr<XdevLQueueFamilyIndices>	IPXdevLQueueFamilyIndices;
	typedef XdevLVulkanQueue							IXdevLVulkanQueue;
	typedef std::shared_ptr<XdevLVulkanQueue>			IPXdevLVulkanQueue;
	typedef XdevLVulkanCommandPool						IXdevLVulkanCommandPool;
	typedef std::shared_ptr<XdevLVulkanCommandPool>		IPXdevLVulkanCommandPool;
	typedef XdevLVulkanDescriptorLayout					IXdevLVulkanDescriptorLayout;
	typedef std::shared_ptr<XdevLVulkanDescriptorLayout>IPXdevLVulkanDescriptorLayout;

}


#endif
