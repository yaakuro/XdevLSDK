/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_INSTANCE_H
#define XDEVL_VULKAN_INSTANCE_H

#include <XdevLVulkanContext/XdevLVulkanContextFwd.h>

namespace xdl {

	/**
	 * @class XdevLQueueFamilyIndices
	 * @brief Helper Interface for the Vulkan queue families handling.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLQueueFamilyIndices {
		public:
			virtual ~XdevLQueueFamilyIndices() {}

			virtual xdl_uint32 getNumberOfQueues() const = 0;

			/// Returns the Graphics Queue Index.
			virtual xdl_uint32 getGraphicsQueueIndex() const = 0;

			/// Returns the Present Queue Index.
			virtual xdl_uint32 getPresentQueueIndex() const = 0;

			virtual void setNumberOfQueues(xdl_uint32 nQueues) = 0;

			/// Sets the Graphics Queue Index.
			virtual void setGraphicsQueueIndex(xdl_uint32 index) = 0;

			/// Sets the Present Queue Index.
			virtual void setPresentQueueIndex(xdl_uint32 index) = 0;

			/// Checks if the Graphics Queue Index is valid.
			virtual xdl_bool hasGraphicsQueueIndex() const = 0;

			/// Checks if the Present Queue Index is valid.
			virtual xdl_bool hasPresentQueueIndex() const = 0;
	};

	/**
	 * @class XdevLVulkanInstance
	 * @brief Interface for the Vulkan Instance handling.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLVulkanInstance {
		public:
			virtual ~XdevLVulkanInstance() {};

			/// Add extensions names you like to enable.
			virtual void addExtensionsToEnable(const std::vector<XdevLString>& extensionsToEnable, xdl_bool resetInternal = xdl_false) = 0;

			/// Create the Vulkan instance.
			virtual xdl_int create(xdl_bool enableLayers = xdl_true) = 0;

			/// Create the Vulkan instance.
			virtual xdl_int create(const VkApplicationInfo& applicationInfo, xdl_bool enableLayers = xdl_true) = 0;

			/// Create the XdevLVulkanDevice object.
			virtual IPXdevLVulkanDevice createDevice() = 0;

			/// Create the XdevLVulkanSurface object.
			virtual IPXdevLVulkanSurface createSurface() = 0;

			/// Returns the Physical Device memory properties.
			virtual VkPhysicalDeviceMemoryProperties getPhysicalDeviceMemoryProperties() const = 0;

			/// Returns the Queue Family indices object.
			virtual IPXdevLQueueFamilyIndices getQueueFamilyIndices() = 0;

			virtual VkPhysicalDevice getPhysicalDevice() const = 0;

			/// Returns the native Vulkan instance handle.
			virtual VkInstance getNativeHandle() const = 0;
	};

}


#endif
