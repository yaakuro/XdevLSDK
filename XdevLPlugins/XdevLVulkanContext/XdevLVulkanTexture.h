/*
	Copyright (c) 2005 - 2017 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VULKAN_TEXTURE_H
#define XDEVL_VULKAN_TEXTURE_H

namespace xdl {

	/**
	 * @class XdevLVulkanTexture
	 * @brief Interface for the Vulkan Texture handling.
	 * @author Cengiz Terzibas
	 */
	class XDEVL_EXPORT XdevLVulkanTexture {
		public:
			virtual ~XdevLVulkanTexture() {};

			/// Create texture.
			virtual xdl_int create(xdl_uint width, xdl_uint height, VkFormat format) = 0;

			/// Create texture.
			virtual xdl_int create(VkDevice device, VkPhysicalDeviceMemoryProperties deviceMemoryProperties, xdl_uint width, xdl_uint height, VkFormat format) = 0;

			/// Lock the buffer for modification.
			/**
				Locks the buffer for modification. Before one can upload/change data
				the buffer needs to be locked. After finishing the job unlock the buffer.
			*/
			virtual xdl_int lock() = 0;

			/// Unlock the buffer after modification.
			virtual xdl_int unlock() = 0;

			/// Maps the buffer.
			/**
				Works only if the buffer was locked before.
			*/
			virtual xdl_uint8* map(VkMemoryMapFlags mapFlags) = 0;

			/// Unmaps the buffer.
			virtual xdl_int unmap() = 0;

			/// Upload streaming data to the buffer.
			/**
				The buffer maybe be rezied dependend on the size value.

				@param src The pointer to the data array to upload.
				@param size The the number of bytes to upload from the 'src'.
			*/
			virtual xdl_int upload(xdl_uint8* src, xdl_uint size, xdl_uint64 offset = 0) = 0;

			/// Returns the layout of this image.
			/**
			 * @brief When we use XdevLVulkanTexture::map we can't always use memcpy because the pixel
			 * arrangement might be not as we assume. Use this method to get more information.
			 * @return The VkSubresourceLayout structure.
			 */
			virtual VkSubresourceLayout getSubresourceLayout() const = 0;

			/// Returns the native handle.
			virtual VkImageView getNativeHandle() const = 0;
	};

}


#endif
