/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <iostream>
#include <string>
#include <XdevLModule.h>
#include <XdevLCoreMediator.h>
#include <XdevLPlatform.h>

#include "XdevLImageBase.h"

#include <fstream>


xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::pluginName,
	xdl::moduleNames,
	XDEVLIMAGE_PLUGIN_MAJOR_VERSION,
	XDEVLIMAGE_PLUGIN_MINOR_VERSION,
	XDEVLIMAGE_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::description,
	XDEVLIMAGE_SERVER_MODULE_MAJOR_VERSION,
	XDEVLIMAGE_SERVER_MODULE_MINOR_VERSION,
	XDEVLIMAGE_SERVER_MODULE_PATCH_VERSION
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLImageServerImpl, moduleDescriptor)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DEFINITION(XdevLImageServer, xdl::XdevLImageServerImpl, moduleDescriptor)

namespace xdl {

	XdevLImageServerImpl::XdevLImageServerImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor) :
		XdevLModuleImpl<XdevLImageServer>(parameter, descriptor) {
	}

	XdevLImageServerImpl::~XdevLImageServerImpl() {
	}

	xdl_int  XdevLImageServerImpl::create(IPXdevLRAI rai) {
		XDEVL_ASSERT(nullptr != rai, "Invaild IPXdevLRAI specified.");

		m_rai = rai;

		auto result = plug(XdevLPluginName("XdevLImageImportPluginBMP"));
		if(RET_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLModelServer, "Faild to plug BMP importer plugin.\n");
		}
		result = plug(XdevLPluginName("XdevLImageImportPluginPNG"));
		if(RET_SUCCESS != result) {
			XDEVL_MODULEX_ERROR(XdevLModelServer, "Faild to plug PNG importer plugin.\n");
		}

		return result;
	}

	xdl_int XdevLImageServerImpl::plug(const XdevLPluginName& pluginName, const XdevLVersion& version) {

		XdevLSharedLibrary* modulesSharedLibrary = nullptr;
		try {
			modulesSharedLibrary = new XdevLSharedLibrary();
		} catch(std::bad_alloc& e) {
			XDEVL_MODULE_ERROR(e.what());
			return RET_FAILED;
		}

		XdevLFileName tmp = getMediator()->getCoreParameter().pluginsPath;
#ifdef XDEVL_PLATFORM_ANDROID
		tmp += XdevLFileName("lib");
#endif
		tmp += XdevLFileName(pluginName);
		tmp += XdevLFileName("-");
		tmp += version.toString();
#ifdef XDEVL_DEBUG
		tmp += XdevLFileName("d");
#endif
		tmp += XdevLFileName(".xip");

		if(modulesSharedLibrary->open(tmp.toString().c_str(), xdl_false) == RET_FAILED) {
			delete modulesSharedLibrary;
			XDEVL_MODULE_ERROR("Could not open plugin: '" << tmp << "'.\n");
			return RET_FAILED;
		}

		XdevLCreateImageImportPluginFunction createImportPlugin = (XdevLCreateImageImportPluginFunction)(modulesSharedLibrary->getFunctionAddress("_createImagePlugin"));

		XdevLImageImportPluginCreateParameter importPluginCreateParameter {};
		createImportPlugin(importPluginCreateParameter);

		XdevLID id(importPluginCreateParameter.pluginInstance->getExtension());

		auto found = m_pluginMap.find(id.getHashCode());
		if(m_pluginMap.end() != found) {
			return RET_FAILED;
		}

		XdevLImageImportPluginInfo importPluginInfo {};
		importPluginInfo.id = id;
		importPluginInfo.imageImportPlugin = importPluginCreateParameter.pluginInstance;

		m_pluginMap[id.getHashCode()] = importPluginInfo;

		return RET_SUCCESS;
	}

	xdl_int XdevLImageServerImpl::readInfo(IPXdevLFile& file, XdevLImageObject* imageObject) {
		return RET_FAILED;
	}

	IPXdevLImageImportPlugin XdevLImageServerImpl::getImporter(const XdevLString& extension) {
		XdevLID id(extension);
		auto found = m_pluginMap.find(id.getHashCode());
		if(m_pluginMap.end() == found) {
			return nullptr;
		}
		return found->second.imageImportPlugin;
	}

	IPXdevLImageObject XdevLImageServerImpl::import(IPXdevLFile& file) {
		auto importer = getImporter(file->getFileName().getExtension());
		if(nullptr != importer) {
			return importer->import(file);
		}
		return nullptr;
	}

}
