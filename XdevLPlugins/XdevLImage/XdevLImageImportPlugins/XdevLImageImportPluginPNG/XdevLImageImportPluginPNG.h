/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of 
	this software and associated documentation files (the "Software"), to deal in the 
	Software without restriction, including without limitation the rights to use, copy, 
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
	and to permit persons to whom the Software is furnished to do so, subject to the 
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies 
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_IMAGE_IMPORT_PLUGIN_PNG_H
#define XDEVL_IMAGE_IMPORT_PLUGIN_PNG_H

#include <XdevLImage/XdevLImage.h>

namespace xdl {

	class XdevLImagePluginPNG : public XdevLImageImportPlugin {

		public:

			XdevLImagePluginPNG();

			virtual ~XdevLImagePluginPNG();

			xdl_int readInfo(IPXdevLFile& file, XdevLImageObject* pInfo) override;
			IPXdevLImageObject import(IPXdevLFile& file) override;
			XdevLString getExtension() override;

	};


}

#endif
