/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLImageImportPluginPNG.h"
#include <XdevLArrayModifier.h>

#include "lodepng.h"
#include <fstream>

extern "C" XDEVL_EXPORT void _createImagePlugin(xdl::XdevLImageImportPluginCreateParameter& parameter) {
	parameter.pluginInstance = std::make_shared<xdl::XdevLImagePluginPNG>();
}

namespace xdl {

	XdevLImagePluginPNG::XdevLImagePluginPNG() {
	}

	XdevLImagePluginPNG::~XdevLImagePluginPNG() {
	}

	xdl_int XdevLImagePluginPNG::readInfo(IPXdevLFile& file, XdevLImageObject* pInfo) {
		// No, let's use the lodepng project import PNG files.
		static std::vector<xdl_uint8> imageIn, imageOut;

		imageIn.reserve(static_cast<size_t>(file->length()));
		imageIn.resize(static_cast<size_t>(file->length()));

		file->read(imageIn.data(), static_cast<size_t>(file->length()));
		file->close();

		xdl_uint width, height;

		xdl_int error = lodepng::decode(imageOut, width, height, imageIn);
		if(error) {
			std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
			return RET_FAILED;
		}

		auto image = new XdevLImage();
		image->Width = width;
		image->Height = height;
		image->BitsPerPixel = 32;
		pInfo->NumImages = 1;
		pInfo->Images = image;


		return RET_SUCCESS;
	}

	IPXdevLImageObject XdevLImagePluginPNG::import(IPXdevLFile& file) {
		// No, let's use the lodepng project import PNG files.
		std::vector<xdl_uint8> imageIn, imageOut;

		imageIn.reserve(static_cast<size_t>(file->length()));
		imageIn.resize(static_cast<size_t>(file->length()));

		file->read(imageIn.data(), static_cast<size_t>(file->length()));
		file->close();

		xdl_uint width, height;

		xdl_int error = lodepng::decode(imageOut, width, height, imageIn);
		if(error) {
			std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
			return nullptr;
		}

		// This flipping is necessary because the library flips the picture up side down.
//		for(unsigned int y = 0; y < height/2; y++) {
//			for(unsigned int x = 0; x < width * 4; x++) {
//				unsigned char tmp = image[x + 4 * width * y];
//				image[x + 4 * width * y] = image[x + 4 * width * (height - 1 - y)];
//				image[x + 4 * width * (height - 1 - y)] = tmp;
//			}
//		}


		auto imageObject = std::make_shared<IXdevLImageObject>();
		auto image = new XdevLImage();
		image->Width = width;
		image->Height = height;
		image->BitsPerPixel = 32;
		image->Buffer = new xdl_uint8[width * height * 4];
		memcpy(image->Buffer, imageOut.data(), imageOut.size());

		imageObject->Images = image;
		imageObject->NumImages = 1;

		return imageObject;
	}

	XdevLString XdevLImagePluginPNG::getExtension() {
		return XdevLString("png");
	}
}
