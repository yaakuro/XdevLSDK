/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include "XdevLImageImportPluginBMP.h"
#include <XdevLArrayModifier.h>

#include <fstream>

extern "C" XDEVL_EXPORT void _createImagePlugin(xdl::XdevLImageImportPluginCreateParameter& parameter) {
	parameter.pluginInstance = std::make_shared<xdl::XdevLImagePluginBMP>();
}

namespace xdl {

	XdevLImagePluginBMP::XdevLImagePluginBMP() {
	}

	XdevLImagePluginBMP::~XdevLImagePluginBMP() {
	}

	xdl_int XdevLImagePluginBMP::readInfo(IPXdevLFile& file, XdevLImageObject* pInfo) {

		// Read the bmp file and info header into buffer. (we store driver access)
		// if we load all directly into buffer.
		xdl_uint8 buffer[54] = {0};
		file->read(buffer, 54);

		XdevLArrayModifier arrayModifier(buffer, 54);

		// Extract file header the information.
		BmpFileHeader fheader;
		arrayModifier.read16(fheader.bfType);
		if(fheader.bfType != BMP_TYPE) {
			XDEVL_MODULEX_ERROR(XdevLImagePluginBMP, "\"" << file->getFileName() << "\" has not the right Bmp format.\n");
			return RET_FAILED;
		}
		arrayModifier.read32(fheader.bfSize);
		arrayModifier.read16(fheader.bfReserved1);
		arrayModifier.read16(fheader.bfReserved2);
		arrayModifier.read32(fheader.bfOffBits);

		// Extract the info header information.
		BmpInfoHeader iheader;
		arrayModifier.readU32(iheader.biSize);
		arrayModifier.read32(iheader.biWidth);
		arrayModifier.read32(iheader.biHeight);
		arrayModifier.readU16(iheader.biPlanes);

		arrayModifier.readU16(iheader.biBitCount);
		arrayModifier.readU32(iheader.biCompression);
		arrayModifier.readU32(iheader.biSizeImage);
		arrayModifier.read32(iheader.biXPelsPerMeter);

		arrayModifier.read32(iheader.biYPelsPerMeter);
		arrayModifier.readU32(iheader.biClrUsed);
		arrayModifier.readU32(iheader.biClrImportant);

		pInfo->NumImages							= 1;
		if(!pInfo->Images) {
			pInfo->Images								= new XdevLImage[1];
		}
		pInfo->Images[0].Width				= iheader.biWidth;
		pInfo->Images[0].Height				=	iheader.biHeight;
		pInfo->Images[0].BitsPerPixel	= iheader.biBitCount;

		return RET_SUCCESS;
	}

	IPXdevLImageObject XdevLImagePluginBMP::import(IPXdevLFile& file) {
		// Read the bmp file and info header into buffer. (we store driver access)
		// if we load all directly into buffer.
		xdl_uint8 buffer[54] = {0};
		file->read(buffer, 54);

		XdevLArrayModifier arrayModifier(buffer, 54);

		// Extract file header the information.
		BmpFileHeader fheader;
		arrayModifier.read16(fheader.bfType);
		if(fheader.bfType != BMP_TYPE) {
			XDEVL_MODULEX_ERROR(XdevLImagePluginBMP, "\"" << file->getFileName() << "\" has not the right Bmp format.\n");
			return nullptr;
		}
		arrayModifier.read32(fheader.bfSize);
		arrayModifier.read16(fheader.bfReserved1);
		arrayModifier.read16(fheader.bfReserved2);
		arrayModifier.read32(fheader.bfOffBits);

		// Extract the info header information.
		BmpInfoHeader iheader;
		arrayModifier.readU32(iheader.biSize);
		arrayModifier.read32(iheader.biWidth);
		arrayModifier.read32(iheader.biHeight);
		arrayModifier.readU16(iheader.biPlanes);

		arrayModifier.readU16(iheader.biBitCount);
		arrayModifier.readU32(iheader.biCompression);
		arrayModifier.readU32(iheader.biSizeImage);
		arrayModifier.read32(iheader.biXPelsPerMeter);

		arrayModifier.read32(iheader.biYPelsPerMeter);
		arrayModifier.readU32(iheader.biClrUsed);
		arrayModifier.readU32(iheader.biClrImportant);

		auto imageOjbect = std::make_shared<XdevLImageObject>();

		// We have only one image in a BMP file.
		imageOjbect->NumImages										= 1;
		if(!imageOjbect->Images) {
			imageOjbect->Images										= new XdevLImage[1];
		}
		// We dont have lod images in BMP files.
		imageOjbect->Images[0].Width				= 0;
		imageOjbect->Images[0].Height				=	0;
		imageOjbect->Images[0].BitsPerPixel	= 0;
		imageOjbect->Images[0].Buffer				= 0;

		// We make allways an RGB image.
		imageOjbect->Images[0].Buffer = new xdl_uint8[iheader.biHeight*iheader.biWidth*3];
//		int compression = UNKNOWN;
//		compression =  iheader.biCompression;
		switch(iheader.biCompression) {
			// Read uncompressed data
			case BMP_RGB: {
				switch(iheader.biBitCount) {
					case 1:
						Read1Bit(file, &iheader, imageOjbect->Images[0].Buffer);
						break;
					case 4:
						Read4Bit(file, &iheader, imageOjbect->Images[0].Buffer);
						break;
					case 8:
						Read8Bit(file, &iheader, imageOjbect->Images[0].Buffer);
						break;
					case 24:
						Read24Bit(file, &iheader, imageOjbect->Images[0].Buffer);
						break;
					case 32:
						Read24Bit(file, &iheader, imageOjbect->Images[0].Buffer);
						break;
					default:
						XDEVL_MODULEX_ERROR(XdevLImagePluginBMP, "Unknown BMP pixel size.\n");
						return nullptr;
				}
			}
			break;
			// Read compressed data
			case BMP_RLE4:
			case BMP_RLE8:
				ReadRLE8(file, &iheader, imageOjbect->Images[0].Buffer);
				break;
		}
		imageOjbect->Images[0].Height				= iheader.biHeight;
		imageOjbect->Images[0].Width					= iheader.biWidth;
		imageOjbect->Images[0].BitsPerPixel	= 24;

		return imageOjbect;
	}

	XdevLString XdevLImagePluginBMP::getExtension() {
		return XdevLString("bmp");
	}



	void XdevLImagePluginBMP::Read1Bit(IPXdevLFile& file, BmpInfoHeader* pHeader, xdl_uint8* pBuffer) {
		xdl_uint8 map[2*4];
		xdl_uint8 stridetmp[4];
		// Read the color index map (2 values)
		file->read(map, 2*4);

		xdl_uint8 index = 0;
		xdl_uint8 stride = ((32 - (pHeader->biWidth % 32)) / 8) % 4;
		for(int y = pHeader->biHeight - 1; y >= 0; --y)
			for(int x = 0; x < pHeader->biWidth; x+=8) {
				// Get one index for the color map.
				file->read(&index, 1);
				int pos = (y*pHeader->biWidth + x)*3;

				for(int bit = 0; bit < 8; ++bit) {
					xdl_uint8 idx = (index&(128L >> bit)) ? 4 : 0;
					pBuffer[pos + bit*3 + 0 ] = map[idx + 2];
					pBuffer[pos + bit*3 + 1 ] = map[idx + 1];
					pBuffer[pos + bit*3 + 2 ] = map[idx + 0];

				}
				if(stride > 0)
					file->read(stridetmp, stride);
			}

	}

	void XdevLImagePluginBMP::Read4Bit(IPXdevLFile& file, BmpInfoHeader* pHeader, xdl_uint8* pBuffer) {
		xdl_uint8 map[16*4];
		// Read the color index map (16 values)
		file->read(map, 16*4);

		xdl_uint8 index = 0;
		for(int y = pHeader->biHeight - 1; y >= 0; --y)
			for(int x = 0; x < pHeader->biWidth; x+=2) {
				// Get one index for the color map.
				file->read(&index, 1);
				int pos = (y*pHeader->biWidth + x)*3;
				xdl_uint8 low = (index&0x0f)*4;
				xdl_uint8 high = ((index&0xf0)*4 >> 4);
				pBuffer[pos + 0] = map[high + 2];
				pBuffer[pos + 1] = map[high + 1];
				pBuffer[pos + 2] = map[high + 0];

				pBuffer[pos + 3] = map[low + 2];
				pBuffer[pos + 4] = map[low + 1];
				pBuffer[pos + 5] = map[low + 0];

			}
	}

	void XdevLImagePluginBMP::Read8Bit(IPXdevLFile& file, BmpInfoHeader* pHeader, xdl_uint8* pBuffer) {
		xdl_uint8 map[256*4];
		// Read the color index map (256 values)
		file->read(map, 256*4);

		xdl_uint8 index = 0;
		for(int y = pHeader->biHeight - 1; y >= 0; --y)
			for(int x = 0; x < pHeader->biWidth; ++x) {
				// Get one index for the color map.
				file->read(&index, 1);
				int pos = (y*pHeader->biWidth + x)*3;
				pBuffer[pos + 0] = map[index*4 + 2];
				pBuffer[pos + 1] = map[index*4 + 1];
				pBuffer[pos + 2] = map[index*4 + 0];
			}
	}

	void XdevLImagePluginBMP::Read24Bit(IPXdevLFile& file, BmpInfoHeader* pHeader, xdl_uint8* pBuffer) {
		xdl_uint8 tmp[3] = {0};
		for(int y = pHeader->biHeight - 1; y >= 0; --y)
			for(int x = 0 ; x < pHeader->biWidth; ++x) {
				file->read(tmp, 3);
				int pos = (y*pHeader->biWidth + x)*3;
				pBuffer[pos + 0] = tmp[2];
				pBuffer[pos + 1] = tmp[1];
				pBuffer[pos + 2] = tmp[0];
			}
	}

	void XdevLImagePluginBMP::ReadRLE8(IPXdevLFile& file, BmpInfoHeader* pHeader, xdl_uint8* pBuffer) {
		xdl_uint8 map[256*4];
		// Read the color index map (256 values)
		if(pHeader->biClrUsed > 0)
			file->read(map, pHeader->biClrUsed*4);
		else
			file->read(map, 256*4);


		for(int y = 0; y < pHeader->biHeight; ++y) {
			for(int x = 0; x < pHeader->biHeight; ++x) {
				xdl_uint32 pos = (x + y*pHeader->biWidth)*3;
				pBuffer[pos + 0] = map[0];
				pBuffer[pos + 1] = map[1];
				pBuffer[pos + 2] = map[2];
			}
		}

		xdl_uint8 tmp[2] = {0};
		for(int y = 0; y < pHeader->biHeight; ++y) {
			int x = 0;
			while(true) {
				file->read(tmp, 2);

				if(tmp[0] == 0) {
					if(tmp[1] == 0)
						break;
					else if(tmp[1] == 0) {
						y = pHeader->biHeight;
						break;
					} else if(tmp[1] == 2) {
						file->read(tmp, 2);
						x += tmp[0];
						y += tmp[1];
						if(y >= pHeader->biHeight)
							break;
					} else {
						if(pHeader->biWidth - x < tmp[1])
							return;
						//
						//uint8_t val;
						//for(int a = 0; a < tmp[1]; ++a){
						//	in.read((xdl_uint8*)(&val), 1);
						//	pBuffer[(y * pHeader->biWidth + x)*3 + 0] = map[val + 2];
						//	pBuffer[(y * pHeader->biWidth + x)*3 + 1] = map[val + 1];
						//	pBuffer[(y * pHeader->biWidth + x)*3 + 2] = map[val + 0];
						//	x++;
						//}
						//if (tmp[1] % 2)
						//	in.read((xdl_uint8*)&tmp[0], 2);
					}
				} else { // if(tmp[0] == 0)
					for(int a = 0; a < tmp[0]; ++a) {
						pBuffer[(y * pHeader->biWidth + x)*3 + 0] = map[tmp[1] + 2];
						pBuffer[(y * pHeader->biWidth + x)*3 + 1] = map[tmp[1] + 1];
						pBuffer[(y * pHeader->biWidth + x)*3 + 2] = map[tmp[1] + 0];
						x++;
					}
				}
			}
		}
		//
		//int y = 0;
		//do{
		//	in.read((xdl_uint8*)(&tmp), 2);
		//	if(tmp[0] == 0 && tmp[1] == 1)
		//		break;
		//	if(tmp[0] == 0 && tmp[1] == 0){
		//		y += pHeader->biWidth;
		//		x = 0;
		//		continue;
		//	}
		//	if(tmp[0] == 0 && tmp[1] == 2){
		//		in.read((xdl_uint8*)(&tmp), 2);
		//		x = tmp[0];
		//		y = tmp[1];
		//		continue;
		//	}
		//	if(tmp[0] == 0 && (tmp[1] >= 0x03 && tmp[1] <= 0xff)){
		//		uint8_t val;
		//		for(int a = 0; a < tmp[1]; ++a){
		//			in.read((xdl_uint8*)(&val), 1);
		//			pBuffer[(x + y)*3 + 0] = map[val + 2];
		//			pBuffer[(x + y)*3 + 1] = map[val + 1];
		//			pBuffer[(x + y)*3 + 2] = map[val + 0];
		//			x++;
		//		}
		//	}
		//	if(tmp[0] >= 3){
		//		for(int a = 0; a<tmp[0]; ++a){
		//			pBuffer[(x + y)*3 + 0] = map[tmp[1]*4	+ 2];
		//			pBuffer[(x + y)*3 + 1] = map[tmp[1]*4 + 1];
		//			pBuffer[(x + y)*3 + 2] = map[tmp[1]*4 + 0];
		//			x++;
		//		}
		//	}
		//}while(true);
	}

}
