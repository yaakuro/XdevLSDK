/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_IMAGE_SERVER_IMPL_H
#define XDEVL_IMAGE_SERVER_IMPL_H

#include <XdevLPluginImpl.h>
#include <XdevLRAI/XdevLRAI.h>
#include <XdevLImage/XdevLImageServer.h>

namespace xdl {

	static const XdevLString pluginName {
		"XdevLImage"
	};
	static const XdevLString description {
		"Creates Image Import System."
	};

	static std::vector<XdevLModuleName>	moduleNames {
		XdevLModuleName("XdevLImageServer")
	};

	class XdevLImageServerImpl : public XdevLModuleImpl<XdevLImageServer> {
		public:
			XdevLImageServerImpl(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLImageServerImpl();

			xdl_int create(IPXdevLRAI rai) override final;
			xdl_int plug(const XdevLPluginName& pluginName, const XdevLVersion& version = xdl::XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, 0)) override final;
			xdl_int readInfo(IPXdevLFile& file, XdevLImageObject* imageObject) override final;
			IPXdevLImageImportPlugin getImporter(const XdevLString& extension) override final;
			IPXdevLImageObject import(IPXdevLFile& file) override final;

		private:
			IPXdevLRAI m_rai;
			std::map<xdl_uint64, XdevLImageImportPluginInfo> m_pluginMap;
	};

}

#endif
