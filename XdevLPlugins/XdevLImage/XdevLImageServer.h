/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_IMAGE_SERVER_H
#define XDEVL_IMAGE_SERVER_H

#include <XdevL.h>
#include <XdevLModule.h>
#include <XdevLImage/XdevLImage.h>
#include <XdevLRAI/XdevLRAI.h>

namespace xdl {

	/**
	 * @class XdevLImageServer
	 * @brief The server that manages all image importer plugins.
	 */
	class XdevLImageServer : public XdevLModule {
		public:
			virtual ~XdevLImageServer() {}

			virtual xdl_int create(IPXdevLRAI rai) = 0;

			/// Plug a new importer plugin to the system.
			virtual xdl_int plug(const XdevLPluginName& pluginName, const XdevLVersion& version = xdl::XdevLVersion(XDEVL_MAJOR_VERSION, XDEVL_MINOR_VERSION, 0)) = 0;

			/// Returns the importer for the specified extensions.
			virtual IPXdevLImageImportPlugin getImporter(const XdevLString& extension) = 0;

			/// Reads infos from the specified image.
			virtual xdl_int readInfo(IPXdevLFile& file, XdevLImageObject* imageObject) = 0;

			/// Loads the specified image an converts it into a RGB or RGBA image format.
			virtual IPXdevLImageObject import(IPXdevLFile& file) = 0;

	};

	using IXdevLImageServer = XdevLImageServer;
	using IPXdevLImageServer = XdevLImageServer*;

	XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLImageServer)
}

#endif
