/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_IMAGE_H
#define XDEVL_IMAGE_H

#include <XdevL.h>
#include <XdevLFileSystem/XdevLFileSystem.h>

namespace xdl {

	/**
	 * @class XdevLImageInfo
	 * @brief Describes the image.
	 */
	class XdevLImageInfo {
		public:
			/// The width of the image.
			xdl_uint32 Width;

			/// The height of the image.
			xdl_uint32 Height;

			/// The number of pixel used for one element.
			xdl_uint16 BitsPerPixel;
	};

	/**
	  @struct XdevLImage
	  @brief Holds raw data of an image.
	*/
	class XdevLImage : public XdevLImageInfo {
		public:

			XdevLImage() : Buffer(nullptr) {}
			virtual ~XdevLImage() {
			}
			void destroy() {
				if(nullptr != Buffer) {
					delete [] Buffer;
					Buffer = nullptr;
				}
			}
			xdl_uint8* Buffer;
	};

	using IXdevLImage = XdevLImage;
	using IPXdevLImage = XdevLImage*;

	/**
	 * @class XdevLImageObject
	 * @brief Holds an image object.
	 */
	class XdevLImageObject {
		public:

			XdevLImageObject() : NumImages(0), Images(nullptr) {}
			~XdevLImageObject() {
			}
			
			void destroy() {
				if(NumImages > 0) {
					for(xdl_int idx = 0; idx < NumImages; idx++) {
						Images[idx].destroy();
					}
				}
			}

			/// Holds the number of images that this image object holds.
			int NumImages;

			/// Array of images.
			XdevLImage* Images;
	};

	using IXdevLImageObject = XdevLImageObject;
	using IPXdevLImageObject = std::shared_ptr<XdevLImageObject>;

	/**
	 * @class XdevLImageImportPlugin
	 * @brief Interface for a image importer plugin.
	 */
	class XdevLImageImportPlugin {
		public:
			virtual ~XdevLImageImportPlugin() {}

			/// Read information about an image file.
			virtual xdl_int readInfo(IPXdevLFile& file, XdevLImageObject* imageObject) = 0;

			/// Imports the image file.
			virtual IPXdevLImageObject import(IPXdevLFile& file) = 0;

			/// Returns the extension of the plugin.
			virtual XdevLString getExtension() = 0;
	};

	using IXdevLImageImportPlugin = XdevLImageImportPlugin;
	using IPXdevLImageImportPlugin = std::shared_ptr<XdevLImageImportPlugin>;

	/**
	 * @class XdevLImageImportPluginCreateParameter
	 * @brief Image importer plugin constructor parameter.
	 */
	class XdevLImageImportPluginCreateParameter {
		public:
			IPXdevLImageImportPlugin pluginInstance;
	};

	using XdevLCreateImageImportPluginFunction = void(*)(const XdevLImageImportPluginCreateParameter& parameter);

	/**
	 * @class XdevLImageImportPluginInfo
	 * @brief Info about a image imprter plugin.
	 */
	class XdevLImageImportPluginInfo {
		public:
			XdevLID id;
			IPXdevLImageImportPlugin imageImportPlugin;
	};
}

#endif
