/*
	Copyright (c) 2005 - 2018 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_IMAGE_UTILS_H
#define XDEVL_IMAGE_UTILS_H

#include <XdevLRAI/XdevLRAI.h>
#include <XdevLImage/XdevLImage.h>

namespace xdl {

	IPXdevLTexture convert(IPXdevLImageObject& imageObject, IPXdevLRAI rai) {
		XDEVL_ASSERT(nullptr != imageObject, "Invalid IPXdevLImageObject specified.");
		XDEVL_ASSERT(nullptr != rai, "Invalid IPXdevLRAI specified.");

		auto texture = rai->createTexture();

		auto image = imageObject->Images[0];
		auto result = texture->init(image.Width, image.Height, XdevLTextureFormat::RGBA8, XdevLTexturePixelFormat::RGBA, image.Buffer);
		if(RET_SUCCESS != result) {
			return nullptr;
		}

		const XdevLTextureSamplerFilterStage filterStage {XdevLTextureFilterMin::LINEAR_MIPMAP_LINEAR, XdevLTextureFilterMag::LINEAR};
		const XdevLTextureSamplerWrap wrap {XdevLTextureWrap::CLAMP_TO_BORDER, XdevLTextureWrap::CLAMP_TO_BORDER, XdevLTextureWrap::CLAMP_TO_BORDER};

		texture->lock();
		texture->setTextureFilter(filterStage);
		texture->setTextureWrap(wrap);
		texture->generateMipMap();
		texture->unlock();

		return texture;
	}

}

#endif
