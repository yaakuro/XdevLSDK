/*
	Copyright (c) 2015 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.


*/

#ifndef XDEVL_FONT_GENERATOR_FFT_H
#define XDEVL_FONT_GENERATOR_FFT_H

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H
#include FT_TRIGONOMETRY_H
#include FT_LCD_FILTER_H

#include <string>

#include <XdevLPluginImpl.h>
#include <XdevLFontGenerator/XdevLFontGenerator.h>

namespace xdl {

	static const XdevLString description {
		"Plugin that creates Fonts using FreeTypeFont."
	};

	static const XdevLString pluginName {
		"XdevLFontGenerator"
	};

	static std::vector<XdevLModuleName>	moduleNames {
		XdevLModuleName("XdevLFontGenerator")
	};


	struct Pixel8 {
		unsigned char channel1;
	};

	struct Pixel16 {
		unsigned char channel1;
		unsigned char channel2;
	};

	struct Pixel24 {
		unsigned char channel1;
		unsigned char channel2;
		unsigned char channel3;
	};

	struct Pixel32 {
		unsigned char channel1;
		unsigned char channel2;
		unsigned char channel3;
		unsigned char channel4;
	};

	struct Point {
		xdl_int dx, dy;

		xdl_int DistSq() const {
			return dx*dx + dy*dy;
		}
	};

	struct Grid {
		Grid(xdl_int width, xdl_int height) : w(width), h(height) {
			grid = new Point[w*h];
		}
		~Grid() {
			delete [] grid;
		}

		Point* grid;
		xdl_int w;
		xdl_int h;
	};

	/**
	 * @class XdevLFaceScope
	 * @brief Scope class for FT_Face.
	 */
	class XdevLFaceScope {
		public:
			XdevLFaceScope(FT_Face& face)
				: m_face(face) {

			}
			~XdevLFaceScope() {
				FT_Done_Face(m_face);
			}

		private:

			FT_Face m_face;
	};

	class XdevLImageScope {
		public:
			XdevLImageScope(xdl_uint8* buffer)
				: m_buffer(buffer) {

			}
			~XdevLImageScope() {
				delete [] m_buffer;
			}

		private:

			xdl_uint8* m_buffer;
	};


	class XdevLFontGeneratorFFT : public XdevLModuleImpl<XdevLFontGenerator>  {

		public:
			XdevLFontGeneratorFFT(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);
			virtual ~XdevLFontGeneratorFFT();

			xdl_int init() override final;
			xdl_int shutdown() override final;

			xdl_int create(IPXdevLArchive archive, IPXdevLFile inputFile, const XdevLFileName& outputFileName, xdl_uint firstCharacter, xdl_uint numberOfCharacters, const XdevLFontSize& fontSize, const XdevLTextureSize& textureSize) override final;

			void setGapBetweenGlyphBitmap(xdl_uint width, xdl_uint height) override final;

		protected:

			xdl_int createDistanceImage(xdl_uint8* distanceImageData, xdl_uint8* imageData, const XdevLTextureSize& textureSize);

			xdl_int initFace(FT_Face& face, IPXdevLArchive archive, IPXdevLFile inputFile, xdl_uint fontSize, xdl_uint textureSize);
			void initCharacter(FT_Face& face, FT_ULong character, xdl_uint& leftCursor, xdl_uint& topCursor, std::stringstream& values);
			void writeBitmapToImage(const XdevLTextureSize& textureSize, xdl_uint8* imageData, FT_Bitmap& bitmap, xdl_uint leftCursor, xdl_uint topCursor);

			xdl_int encodeAndSave(IPXdevLArchive archive, const XdevLFileName& fileName, xdl_uint8* buffer, const XdevLTextureSize& textureSize);
			xdl_int saveFontInfoFile(IPXdevLArchive archive, const XdevLFileName& fileName, const XdevLFontSize& fontSize, xdl_uint texture, const XdevLFileName& fileNames, const std::string& glyphInfo);


			void getPosition(xdl_uint idx, xdl_uint size, xdl_uint& x, xdl_uint& y);

			//	void transform( int scale_down, float spread);
			//		float signedDistance(int cx, int cy, float clamp);
			Point get(Grid &g, xdl_int x, xdl_int y);
			void put(Grid &g, xdl_int x, xdl_int y, const Point &p);
			void compare(Grid &g, Point &p, xdl_int x, xdl_int y, xdl_int offsetx, xdl_int offsety);
			void generateSDF(Grid &g);
			void getPixel(xdl_uint8* buffer, const XdevLTextureSize& textureSize, xdl_uint x, xdl_uint y, xdl_uint8* pixel);
			void setPixel(xdl_uint8* buffer, const XdevLTextureSize& textureSize, xdl_uint x, xdl_uint y, xdl_uint8* pixel);

		protected:

			xdl_int m_rescaleWidth;
			xdl_int m_rescaleHeight;
			bool m_centerHorizontal;
			bool m_centerVertical;

			FT_Library library;

			float m_counter;
			xdl_uint m_gapX;
			xdl_uint m_gapY;

			std::vector<xdl_uint8> m_faceBuffer;
	};

}

#endif
