/*
	Copyright (c) 2015 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.


*/

#include "XdevLFontGeneratorTTF.h"
#include "lodepng.h"

#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>

class sdffnel;


xdl::XdevLPluginDescriptor pluginFontGeneratorTTFDescriptor {
	xdl::pluginName,
	xdl::moduleNames,
	XDEVLFONTGENERATOR_MAJOR_VERSION,
	XDEVLFONTGENERATOR_MINOR_VERSION,
	XDEVLFONTGENERATOR_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleFontGeneratorTTFDescriptor {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::description,
	XDEVLFONTGENERATOR_MODULE_MAJOR_VERSION,
	XDEVLFONTGENERATOR_MODULE_MINOR_VERSION,
	XDEVLFONTGENERATOR_MODULE_PATCH_VERSION
};


XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginFontGeneratorTTFDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLFontGeneratorFFT, moduleFontGeneratorTTFDescriptor);
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

namespace xdl {


	Point inside = { 0, 0 };
	Point empty = { 9999, 9999 };

	XdevLFontGeneratorFFT::XdevLFontGeneratorFFT(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLModuleImpl<XdevLFontGenerator>(parameter, descriptor),
		  m_centerHorizontal(true),
		  m_centerVertical(true),
		  m_counter(0.0f),
		  m_gapX(0),
		  m_gapY(0) {

	}

	XdevLFontGeneratorFFT::~XdevLFontGeneratorFFT() {

	}

	xdl_int XdevLFontGeneratorFFT::init() {
		if(FT_Init_FreeType(&library) != 0) {
			return RET_FAILED;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLFontGeneratorFFT::shutdown() {
		FT_Done_FreeType(library);
		return RET_SUCCESS;
	}

	void XdevLFontGeneratorFFT::setGapBetweenGlyphBitmap(xdl_uint width, xdl_uint height) {
		m_gapX = width;
		m_gapY = height;
	}

	xdl_int XdevLFontGeneratorFFT::initFace(FT_Face& face, IPXdevLArchive archive, IPXdevLFile inputFile, xdl_uint fontSize, xdl_uint textureSize) {
		if(nullptr ==  inputFile) {
			XDEVL_MODULEX_ERROR(XdevLFontGenerator, "File not valid.\n");
			return RET_FAILED;
		}

		//
		// We have to keep the buffer as long as we use this font. Mean only delete the font when we finish or reload a new font.
		//
		size_t size = inputFile->length();
		if(m_faceBuffer.size() < size) {
			m_faceBuffer.reserve(size);
			m_faceBuffer.resize(size);
		}

		inputFile->read(m_faceBuffer.data(), size);

		if(FT_New_Memory_Face(library, m_faceBuffer.data(), m_faceBuffer.size(), 0, &face)) {
			XDEVL_MODULEX_ERROR(XdevLFontGenerator, "Could not create new Freetype face.\n");
			return RET_FAILED;
		}

		if(FT_Set_Pixel_Sizes(face, fontSize, fontSize) != 0) {
			XDEVL_MODULEX_ERROR(XdevLFontGenerator, "Could not set the pixel size of the Freetype face.\n");
			return RET_FAILED;
		}

		return RET_SUCCESS;
	}

	void XdevLFontGeneratorFFT::initCharacter(FT_Face& face, FT_ULong character, xdl_uint& leftCursor, xdl_uint& topCursor, std::stringstream& values) {

	}

	xdl_int XdevLFontGeneratorFFT::encodeAndSave(IPXdevLArchive archive, const XdevLFileName& fileName, xdl_uint8* buffer, const XdevLTextureSize& textureSize) {
		auto file = archive->open(xdl::XdevLOpenForWriteOnly(), fileName);
		if(nullptr == file) {
			XDEVL_MODULEX_ERROR(XdevLFontGenerator, "Couldn't open Font information file to store information.\n");
			return RET_FAILED;
		}

		lodepng::State state;
		// input color type
		state.info_raw.colortype = LCT_RGBA;
		state.info_raw.bitdepth = 8;
		// output color type
		state.info_png.color.colortype = LCT_RGBA;
		state.info_png.color.bitdepth = 8;
		state.encoder.auto_convert = 0; // without this, it would ignore the output color type specified above and choose an optimal one instead

		//encode and save
		std::vector<unsigned char> tmp;
		unsigned error = lodepng::encode(tmp, (unsigned char*)buffer, textureSize.width, textureSize.height, state);
		if(error) {
			XDEVL_MODULEX_ERROR(XdevLFontGenerator, "Couldn't encode image: " << lodepng_error_text(error) << std::endl);
			return RET_FAILED;
		}


		file->write(tmp.data(), tmp.size());
		file->close();
		memset(buffer, 0, textureSize.width * textureSize.height * 4);
		return RET_SUCCESS;
	}

	void XdevLFontGeneratorFFT::writeBitmapToImage(const XdevLTextureSize& textureSize, xdl_uint8* imageData, FT_Bitmap& bitmap, xdl_uint leftCursor, xdl_uint topCursor) {
		for(xdl_uint y = 0; y < bitmap.rows; ++y) {
			for(xdl_uint x = 0; x < bitmap.width; ++x) {

				xdl_int xpos = leftCursor + x;
				xdl_int ypos = topCursor + y;

				uint8_t alpha = bitmap.buffer[x + bitmap.pitch * y];

				xdl_uint8 pixel[4] {};
				pixel[0] = (255 - alpha) + alpha;
				pixel[1] = (255 - alpha) + alpha;
				pixel[2] = (255 - alpha) + alpha;
				pixel[3] = alpha;

				setPixel(imageData, textureSize, xpos, ypos, pixel);
			}
		}
	}

	xdl_int XdevLFontGeneratorFFT::saveFontInfoFile(IPXdevLArchive archive, const XdevLFileName& fileName, const XdevLFontSize& fontSize, xdl_uint textureIndex, const XdevLFileName& fileNames, const std::string& glyphInfo) {
		XdevLFileName outputFileNameString(fileName);
		XdevLFileName glyphInfoFile = fileName.getWithoutExtension() + XdevLFileName("_info.txt");

		std::stringstream fs;
		fs << "(texnumber character left right width height horizontal_advance vertical_advance h_bearing_x h_bearing_y v_bearing_x v_bearing_y left top right bottom) \n";
		fs << "version: 1\n";
		fs << fontSize.size << std::endl;
		fs << textureIndex << std::endl;
		fs << fileNames.toString() << std::endl;
		fs << glyphInfo;

		auto fileOut = archive->open(xdl::XdevLOpenForWriteOnly(), glyphInfoFile);
		if(nullptr == fileOut) {
			XDEVL_MODULEX_ERROR(XdevLFontGenerator, "Couldn't open Font information file to store information.\n");
			return RET_FAILED;
		}
		fileOut->write((xdl_uint8*)fs.str().data(), fs.str().size());
		fileOut->close();
		return RET_SUCCESS;
	}

	xdl_int XdevLFontGeneratorFFT::create(IPXdevLArchive archive,
	                                      IPXdevLFile inputFile,
	                                      const XdevLFileName& outputFileName,
	                                      xdl_uint firstCharacter,
	                                      xdl_uint numberOfCharacters,
	                                      const XdevLFontSize& fontSize,
	                                      const XdevLTextureSize& textureSize) {

		XdevLFileName outputFileNameString(outputFileName);

		// Calculate texturesize
		auto fontSizeWidth = fontSize.size + m_gapX;
		auto fontSizeHeight = fontSize.size + m_gapY;

		xdl_uint MaxCols = textureSize.width/(fontSize.size + m_gapX);

		FT_Face face;
		if(initFace(face, archive, inputFile, fontSize.size, textureSize.width) != RET_SUCCESS) {
			return RET_FAILED;
		}

		std::stringstream values;
		XdevLFileName fileNames("");
		xdl_uint textureNumber = 0;

		auto distanceImageData = new xdl_uint8[textureSize.width * textureSize.height * 4];
		memset(distanceImageData, 0, textureSize.width * textureSize.height * 4);
		{
			XdevLImageScope image(distanceImageData);
			{
				XdevLFaceScope faceScope(face);
				auto imageData = new xdl_uint8[textureSize.width * textureSize.height * 4];
				memset(imageData, 0, textureSize.width * textureSize.height * 4);

				{
					XdevLImageScope image(imageData);



					XdevLFileName outputSDFFilename = outputFileNameString.getWithoutExtension() + XdevLFileName("_dft.png");

					//
					// Go through all glyphs
					//

					xdl_uint i = 0;
					xdl_uint rows = 0;
					xdl_uint cols = 0;

					//
					// Start extracting the glyph bitmap data.
					//
					while(i < numberOfCharacters) {

						auto glyph_index = FT_Get_Char_Index(face, i + firstCharacter);

						if(glyph_index != 0) {
							// Load glyph image into the slot (erase previous one)
							if(FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT)) {
								// Something happend and we don't know what.
							}

							// Convert to an anti-aliased bitmap
							if(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL)) {
								// Something happend and we don't know what.
							}

							FT_GlyphSlot glyphSlot		= face->glyph;
							FT_Glyph_Metrics metrics	= face->glyph->metrics;
							FT_Bitmap& bitmap			= glyphSlot->bitmap;

							xdl_uint currentRow = rows*fontSizeHeight;
							xdl_uint currentColumn = cols*fontSizeWidth;

							// TODO Somtimes the fontize is smaller then the bitmap with or rows. I think I am doing something wrong here.
							xdl_int center_dx = (fontSizeWidth >= bitmap.width) ? (fontSizeWidth - bitmap.width)/2 : 0;
							xdl_int center_dy = (fontSizeHeight >= bitmap.rows) ? (fontSizeHeight - bitmap.rows)/2 : 0;

							if(m_centerHorizontal == false) {
								center_dx = 0;
							}

							if(m_centerVertical == false) {
								center_dy = 0;
							}

							//
							// Copy glyph image into memory.
							//
							xdl_int left = (center_dx + currentColumn + m_gapX) ;
							xdl_int top  = (center_dy + currentRow + m_gapX);

							writeBitmapToImage(textureSize, imageData, bitmap, left, top);

							//
							// Add glyph information into list.
							//
							values << textureNumber	// The texture id.
							       << " " << firstCharacter + i // UTF character
							       << " " << (xdl_double)(glyphSlot->bitmap_left/64.0)	// left
							       << " " << (xdl_double)(glyphSlot->bitmap_top/64.0)	// top
							       << " " << (xdl_double)(metrics.width/64.0)			// width
							       << " " << (xdl_double)(metrics.height/64.0)			// height
							       << " " << (xdl_double)(glyphSlot->advance.x/64.0)	// Advance to next glyph for horizontal layout.
							       << " " << (xdl_double)(glyphSlot->advance.y/64.0)	// Advance to next glyph for horizontal layout.
							       << " " << (xdl_double)(metrics.horiBearingX/64.0)	// Horizontal layout bearing in x.
							       << " " << (xdl_double)(metrics.horiBearingY/64.0)	// Horizontal layout bearing in y.
							       << " " << left										// Texture coordinate.
							       << " " << top										// Texture coordinate.
							       << " " << (left + bitmap.width)						// Texture coordinate.
							       << " " << (top + bitmap.rows)						// Texture coordinate.
							       << std::endl;

							//
							// Next column and if column exceeds switch to next row.
							//
							cols++;
							if(cols % MaxCols == 0) {
								cols = 0;
								rows++;
							}
						}

						i++;

						//
						// Save image if we filled up the whole texture or if we handled the last character.
						//
						if(rows == MaxCols || (i == numberOfCharacters)) {

							std::stringstream ss;
							ss << textureNumber;
							XdevLFileName suffix = XdevLFileName(TEXT("_")) + XdevLFileName(ss.str().c_str()) + XdevLFileName(".png");
							XdevLFileName sdfSuffix = XdevLFileName(TEXT("_")) + XdevLFileName(ss.str().c_str()) + XdevLFileName("_dft.png");

							outputFileNameString = outputFileName.getWithoutExtension() + suffix;
							outputSDFFilename = outputFileName.getWithoutExtension() + sdfSuffix;

							fileNames+= outputFileNameString + XdevLFileName("\n");


							//
							// Create SDF's
							//
							createDistanceImage(distanceImageData,  imageData, textureSize);

							if(encodeAndSave(archive, outputSDFFilename, distanceImageData, textureSize) != RET_SUCCESS) {
								return RET_FAILED;
							}

							if(encodeAndSave(archive, outputFileNameString, imageData, textureSize) != RET_SUCCESS) {
								return RET_FAILED;
							}

							cols = 0;
							rows = 0;
							textureNumber++;
						}
					}

				}

			} // XdevLImageScope image(imageData);

		} // XdevLImageScope image(distanceImageData);

		//
		// Save font information file.
		//
		saveFontInfoFile(archive, outputFileName, fontSize, textureNumber, fileNames, values.str());

		return RET_SUCCESS;
	}

	void XdevLFontGeneratorFFT::getPosition(xdl_uint idx, xdl_uint size, xdl_uint& x, xdl_uint& y) {
		x = idx % size;
		y = idx / size;
	}

	xdl_int XdevLFontGeneratorFFT::createDistanceImage(xdl_uint8* distanceImageData, xdl_uint8* imageData, const XdevLTextureSize& textureSize) {


		std::cout << "Initializing Grid 1&2 ... " << std::flush;

		Grid grid1(textureSize.width, textureSize.height);
		Grid grid2(textureSize.width, textureSize.height);

		for(xdl_uint y = 0; y < textureSize.height; y++) {
			for(xdl_uint x = 0; x < textureSize.width; x++) {

				xdl_uint8 pixel[4] {};
				getPixel(imageData, textureSize, x, y, pixel);

				// Points inside get marked with a dx/dy of zero.
				// Points outside get marked with an infinitely large distance.
				if(pixel[0] < 128) {
					put(grid1, x, y, inside);
					put(grid2, x, y, empty);
				} else {
					put(grid2, x, y, inside);
					put(grid1, x, y, empty);
				}
			}
		}

		std::cout << "finished." << std::endl;

		//
		// Generate the SDF.
		//
		std::cout << "Generating SDF for Grid 1 ... " << std::flush;
		generateSDF(grid1);
		std::cout << "finished." << std::endl;

		std::cout << "Generating SDF for Grid 2 ... " << std::flush;
		generateSDF(grid2);
		std::cout << "finished." << std::endl;


		std::cout << "Calculating signed distance values ... " << std::flush;
		//
		// Render out the results.
		//
		for(xdl_uint y = 0; y < textureSize.height; y++) {
			for(xdl_uint x = 0; x < textureSize.width; x++) {

				// Calculate the actual distance from the dx/dy
				xdl_int dist1 = (xdl_int)(sqrt((xdl_double)get(grid1, x, y).DistSq()));
				xdl_int dist2 = (xdl_int)(sqrt((xdl_double)get(grid2, x, y).DistSq()));
				xdl_int dist = dist1 - dist2;

				// Clamp and scale it, just for display purposes.
				xdl_int c = dist*3 + 128;
				if(c < 0) c = 0;
				if(c > 255) c = 255;

				xdl_uint8 pixel[4] {};
				getPixel(imageData, textureSize, x, y, pixel);

				pixel[3] = c;

				setPixel(distanceImageData, textureSize, x, y, pixel);
			}

		}

		std::cout << " ... finished" << std::endl;


		return 0;
	}

	void XdevLFontGeneratorFFT::getPixel(xdl_uint8* buffer, const XdevLTextureSize& textureSize, xdl_uint x, xdl_uint y, xdl_uint8* pixel) {
		pixel[0] = buffer[x * 4 + y * textureSize.width * 4 + 0];
		pixel[1] = buffer[x * 4 + y * textureSize.width * 4 + 1];
		pixel[2] = buffer[x * 4 + y * textureSize.width * 4 + 2];
		pixel[3] = buffer[x * 4 + y * textureSize.width * 4 + 3];
	}

	void XdevLFontGeneratorFFT::setPixel(xdl_uint8* buffer, const XdevLTextureSize& textureSize, xdl_uint x, xdl_uint y, xdl_uint8* pixel) {
		buffer[x * 4 + y * textureSize.width * 4 + 0] = pixel[0];
		buffer[x * 4 + y * textureSize.width * 4 + 1] = pixel[1];
		buffer[x * 4 + y * textureSize.width * 4 + 2] = pixel[2];
		buffer[x * 4 + y * textureSize.width * 4 + 3] = pixel[3];
	}

	Point XdevLFontGeneratorFFT::get(Grid &g, xdl_int x, xdl_int y) {
		// OPTIMIZATION: you can skip the edge check code if you make your grid
		// have a 1-pixel gutter.
		if(x >= 0 && y >= 0 && x < g.w && y < g.h)
			return g.grid[x + y*g.w];
		else
			return empty;
	}

	void XdevLFontGeneratorFFT::put(Grid &g, xdl_int x, xdl_int y, const Point &p) {
		g.grid[x + y*g.w] = p;
	}

	void XdevLFontGeneratorFFT::compare(Grid &g, Point &p, xdl_int x, xdl_int y, xdl_int offsetx, xdl_int offsety) {
		Point other = get(g, x+offsetx, y+offsety);
		other.dx += offsetx;
		other.dy += offsety;

		if(other.DistSq() < p.DistSq()) {
			p = other;
		}
	}

	void XdevLFontGeneratorFFT::generateSDF(Grid &g) {

		// Pass 0
		for(xdl_int y=0; y<g.h; y++) {
			for(xdl_int x=0; x<g.w; x++) {
				Point p = get(g, x, y);
				compare(g, p, x, y, -1,  0);
				compare(g, p, x, y,  0, -1);
				compare(g, p, x, y, -1, -1);
				compare(g, p, x, y,  1, -1);
				put(g, x, y, p);
			}

			for(xdl_int x=g.w-1; x>=0; x--) {
				Point p = get(g, x, y);
				compare(g, p, x, y, 1, 0);
				put(g, x, y, p);
			}
		}

		// Pass 1
		for(xdl_int y=g.w-1; y>=0; y--) {
			for(xdl_int x=g.h-1; x>=0; x--) {
				Point p = get(g, x, y);
				compare(g, p, x, y,  1,  0);
				compare(g, p, x, y,  0,  1);
				compare(g, p, x, y, -1,  1);
				compare(g, p, x, y,  1,  1);
				put(g, x, y, p);

			}

			for(xdl_int x=0; x<g.w; x++) {
				Point p = get(g, x, y);
				compare(g, p, x, y, -1, 0);
				put(g, x, y, p);
			}

		}

	}



}
