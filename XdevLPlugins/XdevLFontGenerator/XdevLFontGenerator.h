/*
	Copyright (c) 2015 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.


*/

#ifndef XDEVL_FONT_GENERATOR_H
#define XDEVL_FONT_GENERATOR_H

#include <XdevLModule.h>
#include <XdevLFileSystem/XdevLArchive.h>
#include <XdevLFileSystem/XdevLFileSystem.h>

namespace xdl {

	class XdevLFontSize {
		public:
			XdevLFontSize(xdl_uint s)
				: size(s) {}

			operator xdl_uint() {
				return size;
			}

			xdl_uint size;
	};

	class XdevLTextureSize {
		public:
			XdevLTextureSize(xdl_uint w, xdl_uint h)
				: width(w)
				, height(h) {
			}

			xdl_uint width;
			xdl_uint height;
	};

	class XdevLFontGenerator : public XdevLModule {

		public:

			virtual ~XdevLFontGenerator() {};

			virtual xdl_int create(IPXdevLArchive archive, IPXdevLFile inputFile, const XdevLFileName& outputFileName, xdl_uint firstCharacter, xdl_uint numberOfCharacters, const XdevLFontSize& fontSize, const XdevLTextureSize& textureSize) = 0;
			virtual void setGapBetweenGlyphBitmap(xdl_uint width, xdl_uint height) = 0;

	};

	using IXdevLFontGenerator = XdevLFontGenerator;
	using IPXdevLFontGenerator = XdevLFontGenerator*;

	XDEVL_EXPORT_MODULE_CREATE_FUNCTION_DECLARATION(XdevLFontGenerator)
	XDEVL_EXPORT_PLUGIN_INIT_FUNCTION_DECLARATION(XdevLFontGenerator)
	XDEVL_EXPORT_PLUGIN_SHUTDOWN_FUNCTION_DECLARATION(XdevLFontGenerator)
}

#endif
