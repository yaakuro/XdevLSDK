/*
	Copyright (c) 2005 - 2016 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/
#ifndef XDEVL_VR_H
#define XDEVL_VR_H

#include <XdevLModule.h>
#include <XdevLWindow/XdevLWindow.h>
#include <XdevLVulkanContext/XdevLVulkanContext.h>
#include <XdevLRAI/XdevLRAI.h>

#include <tm/tm.h>

namespace xdl {

	struct XdevLVRSubmitParameter {
		xdl::IPXdevLTexture textureLeftEye = nullptr;
		xdl::IPXdevLTexture textureRightEye = nullptr;
	};

	enum class XdevLVREye {
	    LEFT,
	    RIGHT
	};

	enum class XdevLVRControllerHand {
	    LEFT,
	    RIGHT
	};

	struct XdevLVRSubmitParameterVulkan {
		VkInstance instance;
		VkPhysicalDevice physicalDevice;
		VkDevice device;
		VkQueue queue;
		uint32_t queueFamilyIndex;
		VkSemaphore renderFinishedSemaphore;
		VkCommandBuffer commandBuffer;

		VkImage image;
		uint32_t width;
		uint32_t height;
	};

	struct XdevLVRRenderModel {
		XdevLVRRenderModel(const XdevLString& name)
			: m_name(name) {
		}

		const XdevLString& getName() const {
			return m_name;
		}

		XdevLString m_name;
	};

	class XdevLVRDevice {
		public:
			virtual ~XdevLVRDevice() {}

			virtual XdevLString getName() = 0;
			virtual xdl_uint32 getID() = 0;
			virtual tmath::mat4 getPoseMatrix() = 0;
			virtual XdevLVRRenderModel* getRenderModel() = 0;
			virtual void triggerHapticPulse(xdl_uint32 axisID, xdl_uint usDurationMicroSec ) = 0;
	};

	using IXdevLVRDevice = XdevLVRDevice;
	using IPXdevLVRDevice = std::shared_ptr<XdevLVRDevice>;

	class XdevLVRControllerAccesor {
		public:
			XdevLVRControllerAccesor() : m_device(nullptr) {}
			XdevLVRControllerAccesor(XdevLVRDevice** device) : m_device(device) {}

			XdevLVRDevice* operator -> () {
				return *m_device;
			}

		private:
			XdevLVRDevice** m_device;
	};

	using IXdevLVRControllerAccesor = XdevLVRControllerAccesor;
	using IPXdevLVRControllerAccesor = XdevLVRControllerAccesor*;


	/**
		@class XdevLVR
		@brief Base class for VR support.
	*/
	class XdevLVR : public XdevLModule {
		public:

			virtual ~XdevLVR() {}

			virtual xdl_int create(IPXdevLVulkanContext context) = 0;
			virtual IPXdevLVRDevice getDevice(xdl_uint idx) = 0;
			virtual IXdevLVRControllerAccesor getController(XdevLVRControllerHand controllerHand) = 0;

			virtual xdl_int submitVulkan(XdevLVREye eye, const XdevLVRSubmitParameterVulkan& parameter) = 0;
			/// Submits the texture(s) to the VR system.
			/**
			 * @brief If you want to specify just one texture with split view just provide the textureLeftEye of the
			 * XdevLVRSubmitParameter structure.
			 * @param parameter A filled XdevLVRSubmitParameter that describes the submit.
			 * @return RET_SUCCESS if successfull else RET_FAILED.
			 */
			virtual xdl_int submit(const XdevLVRSubmitParameter& parameter) = 0;

			/// Returns the recommend size of the frambuffer for the HMD.
			virtual const XdevLSizeUI& getRecommendedSize() const = 0;

			virtual tmath::mat4 getProjectionForLeftEye() = 0;
			virtual tmath::mat4 getProjectionForRightEye() = 0;
			virtual tmath::mat4 getPosLeftEye() = 0;
			virtual tmath::mat4 getPosRightEye() = 0;
			virtual tmath::mat4 getHMDPose() = 0;
			virtual tmath::mat4 getCurrentViewProjectionMatrixLeftEye() = 0;
			virtual tmath::mat4 getCurrentViewProjectionMatrixRightEye() = 0;

			virtual void setNearFar(xdl_float nearClip, xdl_float farClip) = 0;

			virtual std::vector<XdevLString> getInstanceExtensionsRequired() = 0;
			virtual std::vector<XdevLString> getDeviceExtensionsRequired(VkPhysicalDevice physicalDevice) = 0;
	};

	using IXdevLVR = XdevLVR;
	using IPXdevLVR = XdevLVR*;

}

#endif
