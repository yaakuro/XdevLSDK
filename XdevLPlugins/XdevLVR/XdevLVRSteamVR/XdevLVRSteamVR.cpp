/*
	Copyright (c) 2005 - 2016 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#include <XdevL.h>
#include <XdevLInput/XdevLInputSystem.h>

#include "XdevLVRSteamVR.h"

xdl::XdevLPluginDescriptor pluginDescriptor {
	xdl::pluginName,
	xdl::moduleNames,
	XDEVLGUI_PLUGIN_MAJOR_VERSION,
	XDEVLGUI_PLUGIN_MINOR_VERSION,
	XDEVLGUI_PLUGIN_PATCH_VERSION
};

xdl::XdevLModuleDescriptor moduleDesc {
	XDEVL_MODULE_DEFAULT_VENDOR,
	XDEVL_MODULE_DEFAULT_AUTHOR,
	xdl::moduleNames[0],
	XDEVL_MODULE_DEFAULT_COPYRIGHT_HOLDER,
	xdl::description,
	XDEVLGUI_MODULE_MAJOR_VERSION,
	XDEVLGUI_MODULE_MINOR_VERSION,
	XDEVLGUI_MODULE_PATCH_VERSION,
	xdl::XDEVL_MODULE_STATE_DISABLE_AUTO_DESTROY
};

XDEVL_PLUGIN_INIT_DEFAULT
XDEVL_PLUGIN_SHUTDOWN_DEFAULT
XDEVL_PLUGIN_DELETE_MODULE_DEFAULT
XDEVL_PLUGIN_GET_DESCRIPTOR_DEFAULT(pluginDescriptor)

XDEVL_PLUGIN_CREATE_MODULE {
	XDEVL_PLUGIN_CREATE_MODULE_INSTANCE(xdl::XdevLVRSteamVR, moduleDesc)
	XDEVL_PLUGIN_CREATE_MODULE_NOT_FOUND
}

#define ARRAYSIZE(a) \
	((sizeof(a) / sizeof(*(a))) / \
	 static_cast<size_t>(!(sizeof(a) % sizeof(*(a)))))

namespace xdl {

	const XdevLID ControllerButtonPressed("XDEVL_CONTROLLER_BUTTON_PRESSED");
	const XdevLID ControllerButtonReleased("XDEVL_CONTROLLER_BUTTON_RELEASED");
	const XdevLID ControllerTouchpadTouched("XDEVL_CONTROLLER_TOUCHPAD_TOUCHED");
	const XdevLID ControllerTouchpadUntouched("XDEVL_CONTROLLER_TOUCHPAD_UNTOUCHED");
	const XdevLID ControllerTouchpadPressed("XDEVL_CONTROLLER_TOUCHPAD_PRESSED");
	const XdevLID ControllerTouchpadReleased("XDEVL_CONTROLLER_TOUCHPAD_RELEASED");
	const XdevLID ControllerTouchpadMove("XDEVL_CONTROLLER_TOUCHPAD_MOVE");

	inline xdl_uint trackedControllerRoleToIdx(vr::ETrackedControllerRole role) {
		switch(role) {
			case vr::TrackedControllerRole_RightHand:
				return 0;
			case vr::TrackedControllerRole_LeftHand:
				return 1;
			case vr::TrackedControllerRole_Invalid:
			default:
				break;
		}
		return 2;
	}

	inline XdevLButtonId toXdevLButton(xdl_int button) {
		switch(button) {
			case vr::k_EButton_System:
				return CONTROLLER_SYSTEM;
			case vr::k_EButton_ApplicationMenu:
				return CONTROLLER_APPLICATIONMENU;
			case vr::k_EButton_Grip:
				return CONTROLLER_GRIP;
			case vr::k_EButton_ProximitySensor:
				return CONTROLLER_PROXIMITYSENSOR;
			case vr::k_EButton_Axis0:
				return CONTROLLER_AXIS0;
			case vr::k_EButton_Axis1:
				return CONTROLLER_AXIS1;
			case vr::k_EButton_Axis2:
				return CONTROLLER_AXIS2;
			case vr::k_EButton_Axis3:
				return CONTROLLER_AXIS3;
			case vr::k_EButton_Axis4:
				return CONTROLLER_AXIS4;
			default:
				break;
		}
		return KEY_UNKNOWN;
	}

	tmath::mat4 convertSteamVRMatrixToMatrix4( const vr::HmdMatrix34_t &matPose ) {
		tmath::mat4 matrix(
		    matPose.m[0][0], matPose.m[1][0], matPose.m[2][0], 0.0,
		    matPose.m[0][1], matPose.m[1][1], matPose.m[2][1], 0.0,
		    matPose.m[0][2], matPose.m[1][2], matPose.m[2][2], 0.0,
		    matPose.m[0][3], matPose.m[1][3], matPose.m[2][3], 1.0f
		);
		return matrix;
	}


	XdevLVRDeviceImpl::XdevLVRDeviceImpl()
		: m_transformation(nullptr)
		, m_renderModel(nullptr) {

	}

	XdevLVRDeviceImpl::~XdevLVRDeviceImpl() {
		if(m_renderModel) {
			delete m_renderModel;
		}
	}

	xdl_uint32 XdevLVRDeviceImpl::getID() {
		return m_id;
	}

	tmath::mat4 XdevLVRDeviceImpl::getPoseMatrix() {
		return *m_transformation;
	}

	XdevLVRRenderModel* XdevLVRDeviceImpl::getRenderModel() {
		return m_renderModel;
	}

	XdevLString XdevLVRDeviceImpl::getName() {
		return m_name;
	}

	void XdevLVRDeviceImpl::setID(xdl_uint32 id) {
		m_id = id;
	}

	void XdevLVRDeviceImpl::setName(const XdevLString& name) {
		m_name = name;
	}

	void XdevLVRDeviceImpl::setRenderModel(XdevLVRRenderModel* renderModel) {
		m_renderModel = renderModel;
	}

	void XdevLVRDeviceImpl::setTransformation(tmath::mat4* matrix) {
		m_transformation = matrix;
	}

	void XdevLVRDeviceImpl::triggerHapticPulse(xdl_uint32 axisID, xdl_uint usDurationMicroSec ) {
		m_vrSystem->TriggerHapticPulse(m_id, axisID, usDurationMicroSec);
	}

	void  XdevLVRDeviceImpl::setVRSystem(vr::IVRSystem* vrsystem) {
		m_vrSystem = vrsystem;
	}



//
//
//

	XdevLVRSteamVR::XdevLVRSteamVR(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor)
		: XdevLModuleAutoImpl<XdevLVR>(parameter, descriptor)
		, m_vrSystem(nullptr)
		, m_compositor(nullptr) {
		m_devices.reserve(vr::k_unMaxTrackedDeviceCount);
		m_devices.resize(vr::k_unMaxTrackedDeviceCount);
		m_devicesRole.reserve(vr::k_unMaxTrackedDeviceCount);
		m_devicesRole.resize(vr::k_unMaxTrackedDeviceCount);

		for (xdl_uint32 device = 0; device < vr::k_unMaxTrackedDeviceCount; ++device) {
			m_devices[device].setTransformation(&m_devicePoseMat[device]);
			m_devicesRole[device] = &m_devices[device];
		}


//		for(auto& controller : m_devicesRole) {
//			controller = std::make_shared<XdevLVRDeviceImpl>();
//		}
	}

	XdevLVRSteamVR::~XdevLVRSteamVR() {

	}

	IPXdevLVRDevice XdevLVRSteamVR::getDevice(xdl_uint idx) {
		auto device = m_devices[idx];
		auto tmp = std::make_shared<XdevLVRDeviceImpl>(device);
		return tmp;
	}

	xdl_int XdevLVRSteamVR::create(IPXdevLVulkanContext context) {
		context->getDescriptor().registerDependency(this);
		return RET_SUCCESS;
	}

	xdl_int XdevLVRSteamVR::init() {
		if(!vr::VR_IsHmdPresent()) {
			XDEVL_MODULEX_ERROR(XdevLVRSteamVR, "HMD not present.\n");
			return RET_FAILED;
		}

		if (!vr::VR_IsRuntimeInstalled()) {
			XDEVL_MODULEX_ERROR(XdevLVRSteamVR, "OpenVR Runtime not detected on the system.\n");
			return RET_FAILED;
		}

		vr::EVRInitError err = vr::VRInitError_None;
		m_vrSystem = vr::VR_Init(&err, vr::VRApplication_Scene);
		if (err != vr::VRInitError_None) {
			XDEVL_MODULEX_ERROR(XdevLVRSteamVR, "vr::VR_Init failed: " << vr::VR_GetVRInitErrorAsEnglishDescription(err) << std::endl);
			return RET_FAILED;
		}

		vr::IVRCompositor* m_compositor = vr::VRCompositor();
		if (nullptr == m_compositor) {
			XDEVL_MODULEX_ERROR(XdevLVRSteamVR, "Unable to initialize VR compositor.\n");
			return RET_FAILED;
		}

		m_renderModels = (vr::IVRRenderModels *)vr::VR_GetGenericInterface( vr::IVRRenderModels_Version, &err );
		if(nullptr == m_renderModels) {
			XDEVL_MODULEX_ERROR(XdevLVRSteamVR, "Unable to initialize VR rendermodels.\n");
			return RET_FAILED;
		}

		m_vrSystem->GetRecommendedRenderTargetSize(&m_size.width, &m_size.height);



		for (xdl_uint32 device = 0; device < vr::k_unMaxTrackedDeviceCount; ++device) {
			switch (m_vrSystem->GetTrackedDeviceClass(device)) {
				case vr::TrackedDeviceClass_Controller: {
					m_devices[device].setVRSystem(m_vrSystem);
					const vr::ETrackedControllerRole role = m_vrSystem->GetControllerRoleForTrackedDeviceIndex(device);
					m_devicesRole[trackedControllerRoleToIdx(role)] = &m_devices[device];
					updateControllerStates(device);
				}
				break;
				default:
					break;
			}
		}

		return RET_SUCCESS;
	}

	xdl_int XdevLVRSteamVR::shutdown() {
		if (m_vrSystem) {
			vr::VR_Shutdown();
			m_vrSystem = nullptr;
		}
		return RET_SUCCESS;
	}

	xdl_int XdevLVRSteamVR::notify(XdevLEvent& event) {
		return XdevLModuleAutoImpl<XdevLVR>::notify(event);
	}

	void XdevLVRSteamVR::setNearFar(xdl_float nearClip, xdl_float farClip) {
		m_nearClip = nearClip;
		m_farClip = farClip;

		updateMatrices();
	}

	std::vector<XdevLString> XdevLVRSteamVR::getInstanceExtensionsRequired() {
		std::vector<char> tmp;
		std::vector<XdevLString> tokens;
		if ( vr::VRCompositor() ) {
			uint32_t bufferSize = vr::VRCompositor()->GetVulkanInstanceExtensionsRequired( nullptr, 0 );

			if(bufferSize > 0) {
				tmp.reserve(bufferSize);
				tmp.resize(bufferSize);
				vr::VRCompositor()->GetVulkanInstanceExtensionsRequired((char*)tmp.data(), bufferSize);

				XdevLString extensions(tmp.data());

				extensions.tokenize(tokens, XdevLString(" "));

			}
		}
		return tokens;
	}

	std::vector<XdevLString> XdevLVRSteamVR::getDeviceExtensionsRequired(VkPhysicalDevice physicalDevice) {
		std::vector<char> tmp;
		std::vector<XdevLString> tokens;
		if ( vr::VRCompositor() ) {
			uint32_t bufferSize = vr::VRCompositor()->GetVulkanDeviceExtensionsRequired( physicalDevice, nullptr, 0 );

			if(bufferSize > 0) {
				tmp.reserve(bufferSize);
				tmp.resize(bufferSize);
				vr::VRCompositor()->GetVulkanDeviceExtensionsRequired(physicalDevice, (char*)tmp.data(), bufferSize);

				XdevLString extensions(tmp.data());

				extensions.tokenize(tokens, XdevLString(" "));

			}
		}
		return tokens;
	}

	void XdevLVRSteamVR::updateMatrices() {
		auto mat = m_vrSystem->GetProjectionMatrix( vr::Eye_Left, m_nearClip, m_farClip );

		// NOTE: Flip y for Vulkan
		m_projectionLeftEye = tmath::mat4 (
		                          mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
		                          mat.m[0][1], -mat.m[1][1], mat.m[2][1], mat.m[3][1],
		                          mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
		                          mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
		                      );
		mat = m_vrSystem->GetProjectionMatrix( vr::Eye_Right, m_nearClip, m_farClip );

		// NOTE: Flip y for Vulkan
		m_projectionRightEye =  tmath::mat4(
		                            mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
		                            mat.m[0][1], -mat.m[1][1], mat.m[2][1], mat.m[3][1],
		                            mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
		                            mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
		                        );

		auto eyeToHeadMatrix = m_vrSystem->GetEyeToHeadTransform( vr::Eye_Left );
		m_poseLeftEye = tmath::mat4(
		                    eyeToHeadMatrix.m[0][0], eyeToHeadMatrix.m[1][0], eyeToHeadMatrix.m[2][0], 0.0,
		                    eyeToHeadMatrix.m[0][1], eyeToHeadMatrix.m[1][1], eyeToHeadMatrix.m[2][1], 0.0,
		                    eyeToHeadMatrix.m[0][2], eyeToHeadMatrix.m[1][2], eyeToHeadMatrix.m[2][2], 0.0,
		                    eyeToHeadMatrix.m[0][3], eyeToHeadMatrix.m[1][3], eyeToHeadMatrix.m[2][3], 1.0f
		                );
		m_poseLeftEye = tmath::inverse(m_poseLeftEye);

		eyeToHeadMatrix = m_vrSystem->GetEyeToHeadTransform( vr::Eye_Right );
		m_poseRightEye = tmath::mat4(
		                     eyeToHeadMatrix.m[0][0], eyeToHeadMatrix.m[1][0], eyeToHeadMatrix.m[2][0], 0.0,
		                     eyeToHeadMatrix.m[0][1], eyeToHeadMatrix.m[1][1], eyeToHeadMatrix.m[2][1], 0.0,
		                     eyeToHeadMatrix.m[0][2], eyeToHeadMatrix.m[1][2], eyeToHeadMatrix.m[2][2], 0.0,
		                     eyeToHeadMatrix.m[0][3], eyeToHeadMatrix.m[1][3], eyeToHeadMatrix.m[2][3], 1.0f
		                 );
		m_poseRightEye = tmath::inverse(m_poseRightEye);
	}

	tmath::mat4 XdevLVRSteamVR::getProjectionForLeftEye() {
//		vr::HmdMatrix44_t mat = m_vrSystem->GetProjectionMatrix( vr::Eye_Left, m_nearClip, m_farClip );
//
//		// NOTE: Flip y for Vulkan
//		return tmath::mat4(
//		           mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
//		           mat.m[0][1], -mat.m[1][1], mat.m[2][1], mat.m[3][1],
//		           mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
//		           mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
//		       );
		return m_projectionLeftEye;
	}

	tmath::mat4 XdevLVRSteamVR::getProjectionForRightEye() {
//		vr::HmdMatrix44_t mat = m_vrSystem->GetProjectionMatrix( vr::Eye_Right, m_nearClip, m_farClip );
//
//		// NOTE: Flip y for Vulkan
//		return tmath::mat4(
//		           mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
//		           mat.m[0][1], -mat.m[1][1], mat.m[2][1], mat.m[3][1],
//		           mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
//		           mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
//		       );
		return m_projectionRightEye;
	}

	tmath::mat4 XdevLVRSteamVR::getPosLeftEye() {

//		vr::HmdMatrix34_t matEyeLeft = m_vrSystem->GetEyeToHeadTransform( vr::Eye_Left );
//		tmath::mat4 mat(
//		    matEyeLeft.m[0][0], matEyeLeft.m[1][0], matEyeLeft.m[2][0], 0.0,
//		    matEyeLeft.m[0][1], matEyeLeft.m[1][1], matEyeLeft.m[2][1], 0.0,
//		    matEyeLeft.m[0][2], matEyeLeft.m[1][2], matEyeLeft.m[2][2], 0.0,
//		    matEyeLeft.m[0][3], matEyeLeft.m[1][3], matEyeLeft.m[2][3], 1.0f
//		);
//		return tmath::inverse(mat);
		return m_poseLeftEye;
	}

	tmath::mat4 XdevLVRSteamVR::getPosRightEye() {

//		vr::HmdMatrix34_t matEyeRight = m_vrSystem->GetEyeToHeadTransform( vr::Eye_Right );
//		tmath::mat4 mat(
//		    matEyeRight.m[0][0], matEyeRight.m[1][0], matEyeRight.m[2][0], 0.0,
//		    matEyeRight.m[0][1], matEyeRight.m[1][1], matEyeRight.m[2][1], 0.0,
//		    matEyeRight.m[0][2], matEyeRight.m[1][2], matEyeRight.m[2][2], 0.0,
//		    matEyeRight.m[0][3], matEyeRight.m[1][3], matEyeRight.m[2][3], 1.0f
//		);
//		return tmath::inverse(mat);
		return m_poseRightEye;
	}

	tmath::mat4 XdevLVRSteamVR::getHMDPose() {
		return m_hmdPose;
	}

	tmath::mat4 XdevLVRSteamVR::getCurrentViewProjectionMatrixLeftEye() {
		return m_projectionLeftEye * m_poseLeftEye * m_hmdPose;
	}

	tmath::mat4 XdevLVRSteamVR::getCurrentViewProjectionMatrixRightEye() {
		return m_projectionRightEye * m_poseRightEye * m_hmdPose;
	}

	xdl_int XdevLVRSteamVR::submitVulkan(XdevLVREye eye, const XdevLVRSubmitParameterVulkan& parameter) {

		vr::VRTextureBounds_t textureBounds {};
		textureBounds.uMin = 0.0f;
		textureBounds.uMax = 1.0f;
		textureBounds.vMin = 0.0f;
		textureBounds.vMax = 1.0f;

		vr::VRVulkanTextureData_t vulkanData {};
		vulkanData.m_nImage				= (uint64_t)parameter.image;
		vulkanData.m_pDevice			= parameter.device;
		vulkanData.m_pPhysicalDevice	= parameter.physicalDevice;
		vulkanData.m_pInstance			= parameter.instance;
		vulkanData.m_pQueue				= parameter.queue;
		vulkanData.m_nQueueFamilyIndex	= parameter.queueFamilyIndex;
		vulkanData.m_nWidth				= parameter.width;
		vulkanData.m_nHeight			= parameter.height;
		vulkanData.m_nFormat			= (uint32_t)VK_FORMAT_R8G8B8A8_SRGB;
		vulkanData.m_nSampleCount = 1;

		vr::Texture_t texture = {&vulkanData, vr::TextureType_Vulkan, vr::ColorSpace_Auto};
		vr::EVREye vreye = (eye == XdevLVREye::LEFT) ? vr::Eye_Left : vr::Eye_Right;

		vr::VRCompositor()->Submit(vreye, &texture, &textureBounds );

		return RET_SUCCESS;
	}

	xdl_int XdevLVRSteamVR::submit(const XdevLVRSubmitParameter& parameter) {
		XDEVL_ASSERT(parameter.textureLeftEye != nullptr, "You must specify at least the left eye texture");

		vr::Texture_t leftEyeTexture = {
			reinterpret_cast<void*>(parameter.textureLeftEye->id()),
			vr::TextureType_OpenGL,
			vr::ColorSpace_Linear
		};

		vr::Texture_t rightEyeTexture = {
			(nullptr == parameter.textureRightEye) ? leftEyeTexture.handle : reinterpret_cast<void*>(parameter.textureRightEye->id()),
			vr::TextureType_OpenGL,
			vr::ColorSpace_Linear
		};

		vr::VRCompositor()->Submit(vr::Eye_Left, &leftEyeTexture);
		vr::VRCompositor()->Submit(vr::Eye_Right, &rightEyeTexture);

		return RET_SUCCESS;
	}

	const XdevLSizeUI& XdevLVRSteamVR::getRecommendedSize() const {
		return m_size;
	}

	xdl_int XdevLVRSteamVR::update() {
		if(nullptr != m_vrSystem) {

			vr::VRCompositor()->WaitGetPoses(m_trackedPoses, vr::k_unMaxTrackedDeviceCount , nullptr, 0);

			for (xdl_uint32 device = 0; device < vr::k_unMaxTrackedDeviceCount; ++device) {

				m_devicePoseMat[device] = convertSteamVRMatrixToMatrix4( m_trackedPoses[device].mDeviceToAbsoluteTracking );

				switch (m_vrSystem->GetTrackedDeviceClass(device)) {
					case vr::TrackedDeviceClass_Controller: {
						const vr::ETrackedControllerRole role = m_vrSystem->GetControllerRoleForTrackedDeviceIndex(device);
						m_devicesRole[trackedControllerRoleToIdx(role)] = &m_devices[device];
						updateControllerStates(device);
					}
					break;
					case vr::TrackedDeviceClass_HMD:
						updateHMDPoseMatrices(device);
						break;
					case vr::TrackedDeviceClass_Invalid:
						break;
					case vr::TrackedDeviceClass_GenericTracker:
						break;
					case vr::TrackedDeviceClass_TrackingReference:
						break;
					default:
						break;
				}

			}

			handleEvents();

		}

		return XdevLModuleAutoImpl<XdevLVR>::update();
	}

	void XdevLVRSteamVR::sendButtonPressedEvent(XdevLEvent ev, xdl_bool pressed, xdl_int16 controllerid, vr::EVRButtonId id) {
		ev.type 				= pressed ? ControllerButtonPressed.getHashCode() : ControllerButtonReleased.getHashCode();
		ev.cbutton.controllerid	= controllerid;
		ev.cbutton.buttonid		= toXdevLButton(id);
		getMediator()->fireEvent(ev);
	}

	void XdevLVRSteamVR::sendTouchpadTouchedEvent(XdevLEvent ev, xdl_bool pressed, xdl_int16 controllerid, vr::EVRButtonId id) {
		ev.type 				= pressed ? ControllerTouchpadTouched.getHashCode() : ControllerTouchpadUntouched.getHashCode();
		ev.cbutton.controllerid	= controllerid;
		ev.cbutton.buttonid		= toXdevLButton(id);
		getMediator()->fireEvent(ev);
	}

	void XdevLVRSteamVR::sendTouchpadPressedEvent(XdevLEvent ev, xdl_bool pressed, xdl_int16 controllerid, vr::EVRButtonId id) {
		ev.type 				= pressed ? ControllerTouchpadPressed.getHashCode() : ControllerTouchpadReleased.getHashCode();
		ev.cbutton.controllerid	= controllerid;
		ev.cbutton.buttonid		= toXdevLButton(id);
		getMediator()->fireEvent(ev);
	}

	void XdevLVRSteamVR::updateControllerStates(xdl_uint32 deviceIndex) {

		// Don't do anything if the device is not connected.
		if(!m_vrSystem->IsTrackedDeviceConnected(deviceIndex)) {
			return;
		}

		vr::VRControllerState_t state {};
		if( m_vrSystem->GetControllerState( deviceIndex, &state, sizeof(state) ) ) {

			if(previousControllerStates[deviceIndex].unPacketNum != state.unPacketNum) {

				xdl_bool states[vr::k_EButton_Max] {};
				states[vr::k_EButton_System] 			= (state.ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_System));
				states[vr::k_EButton_ApplicationMenu] 	= (state.ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_ApplicationMenu));
				states[vr::k_EButton_Grip] 				= (state.ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_Grip));
				states[vr::k_EButton_SteamVR_Touchpad]	= (state.ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_SteamVR_Touchpad));
				states[vr::k_EButton_SteamVR_Trigger] 	= (state.ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_SteamVR_Trigger));
				states[vr::k_EButton_DPad_Left] 		= (state.ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_DPad_Left));

				XdevLEvent ev;
				ev.common.timestamp = getMediator()->getTimer().getTime64();

				// Handle the application button.
				if( states[vr::k_EButton_ApplicationMenu] != m_devices[deviceIndex].buttons[vr::k_EButton_ApplicationMenu]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_ApplicationMenu] = states[vr::k_EButton_ApplicationMenu];
					sendButtonPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_ApplicationMenu], deviceIndex, vr::k_EButton_ApplicationMenu);
				}

				// Handle the grip button.
				if( states[vr::k_EButton_Grip] != m_devices[deviceIndex].buttons[vr::k_EButton_Grip]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_Grip] = states[vr::k_EButton_Grip];
					sendButtonPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_Grip], deviceIndex, vr::k_EButton_Grip);
				}

				// Handle touchpad press events.
				if( states[vr::k_EButton_SteamVR_Touchpad] != m_devices[deviceIndex].buttons[vr::k_EButton_SteamVR_Touchpad]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_SteamVR_Touchpad] = states[vr::k_EButton_SteamVR_Touchpad];
					sendTouchpadPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_SteamVR_Touchpad], deviceIndex, vr::k_EButton_SteamVR_Touchpad);
				}

				// Handle the system button.
				if( states[vr::k_EButton_System] != m_devices[deviceIndex].buttons[vr::k_EButton_System]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_System] = states[vr::k_EButton_System];
					sendButtonPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_System], deviceIndex, vr::k_EButton_System);
				}

				// Handle the trigger button.
				if( states[vr::k_EButton_SteamVR_Trigger] != m_devices[deviceIndex].buttons[vr::k_EButton_SteamVR_Trigger]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_SteamVR_Trigger] = states[vr::k_EButton_SteamVR_Trigger];
					sendButtonPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_SteamVR_Trigger], deviceIndex, vr::k_EButton_SteamVR_Trigger);
				}

				//
				// Handle other button press events..
				//
				if( states[vr::k_EButton_Axis2] != m_devices[deviceIndex].buttons[vr::k_EButton_Axis2]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_Axis2] = states[vr::k_EButton_Axis2];
					sendButtonPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_Axis2], deviceIndex, vr::k_EButton_Axis2);
				}

				if( states[vr::k_EButton_Axis3] != m_devices[deviceIndex].buttons[vr::k_EButton_Axis3]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_Axis3] = states[vr::k_EButton_Axis3];
					sendButtonPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_Axis3], deviceIndex, vr::k_EButton_Axis3);
				}

				if( states[vr::k_EButton_Axis4] != m_devices[deviceIndex].buttons[vr::k_EButton_Axis4]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_Axis4] = states[vr::k_EButton_Axis4];
					sendButtonPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_Axis4], deviceIndex, vr::k_EButton_Axis4);
				}

				if( states[vr::k_EButton_Dashboard_Back] != m_devices[deviceIndex].buttons[vr::k_EButton_Dashboard_Back]) {
					m_devices[deviceIndex].buttons[vr::k_EButton_Dashboard_Back] = states[vr::k_EButton_Dashboard_Back];
					sendButtonPressedEvent(ev, m_devices[deviceIndex].buttons[vr::k_EButton_Dashboard_Back], deviceIndex, vr::k_EButton_Dashboard_Back);
				}

				auto touchpadTouched = (state.ulButtonTouched & vr::ButtonMaskFromId(vr::k_EButton_SteamVR_Touchpad));
				if(touchpadTouched) {
					m_devices[deviceIndex].rAxis[0] = state.rAxis[0];
					ev.type 						= ControllerTouchpadMove.getHashCode();
					ev.ctouchpadaxis.controllerid	= deviceIndex;
					ev.ctouchpadaxis.touchpadid 	= 0;
					ev.ctouchpadaxis.x				= m_devices[deviceIndex].rAxis[0].x;
					ev.ctouchpadaxis.y				= m_devices[deviceIndex].rAxis[0].y;
					getMediator()->fireEvent(ev);
				}
			}
		}
	}

	XdevLString XdevLVRSteamVR::getTrackedDeviceString(vr::TrackedDeviceIndex_t deviceIndex, vr::TrackedDeviceProperty deviceProperty, vr::TrackedPropertyError *propertyError) {
		uint32_t unRequiredBufferLen = m_vrSystem->GetStringTrackedDeviceProperty( deviceIndex, deviceProperty, nullptr, 0, propertyError );
		if( unRequiredBufferLen == 0 )
			return XdevLString();

		char *pchBuffer = new char[ unRequiredBufferLen ];
		unRequiredBufferLen = m_vrSystem->GetStringTrackedDeviceProperty( deviceIndex, deviceProperty, pchBuffer, unRequiredBufferLen, propertyError );
		XdevLString sResult(pchBuffer);
		delete [] pchBuffer;
		return sResult;
	}

	void XdevLVRSteamVR::setupRenderModelForTrackedDevice( vr::TrackedDeviceIndex_t trackedDeviceIndex ) {
		if( trackedDeviceIndex >= vr::k_unMaxTrackedDeviceCount )
			return;

		// try to find a model we've already set up
		auto renderModelName = getTrackedDeviceString( trackedDeviceIndex, vr::Prop_RenderModelName_String );
		XdevLVRRenderModel *renderModel = findOrLoadRenderModel(trackedDeviceIndex, renderModelName);
		if( !renderModel ) {
			auto sTrackingSystemName = getTrackedDeviceString(trackedDeviceIndex, vr::Prop_TrackingSystemName_String );
			XDEVL_MODULEX_ERROR(XdevLVR, "Could not load render model for device: " << trackedDeviceIndex << " " << sTrackingSystemName << ", " << renderModelName << std::endl);
		} else {
			m_devices[trackedDeviceIndex].setRenderModel(renderModel);
//			m_rbShowTrackedDevice[ trackedDeviceIndex ] = true;
			XDEVL_MODULEX_SUCCESS(XdevLVR, "Loaded render model for device: " << trackedDeviceIndex << " " << renderModelName << std::endl);
		}
	}

	XdevLVRRenderModel* XdevLVRSteamVR::findOrLoadRenderModel(vr::TrackedDeviceIndex_t deviceIndex, const XdevLString& renderModelName ) {

		XdevLVRRenderModel *renderModel = nullptr;

		// Check if we have the model already.
		for(auto& device : m_devices ) {
			if( device.getName() == renderModelName ) {
				renderModel = device.getRenderModel();
				break;
			}
		}


		// OK, we didn't find it so let's load it.
		if( !renderModel ) {
			vr::RenderModel_t* model = nullptr;
			vr::EVRRenderModelError error;
			while (true) {
				error = m_renderModels->LoadRenderModel_Async( renderModelName.toString().c_str(), &model );
				if ( error != vr::VRRenderModelError_Loading )
					break;

				xdl::sleep( 1 );
			}

			if ( error != vr::VRRenderModelError_None ) {
				XDEVL_MODULEX_ERROR(XdevLVR, "LoadRenderModel_Async failed for: " << renderModelName << std::endl);
				return nullptr;
			}

			vr::RenderModel_TextureMap_t* texture = nullptr;
			while (true) {
				error = vr::VRRenderModels()->LoadTexture_Async( model->diffuseTextureId, &texture );
				if ( error != vr::VRRenderModelError_Loading )
					break;

				xdl::sleep( 1 );
			}

			if (error != vr::VRRenderModelError_None ) {
				XDEVL_MODULEX_ERROR(XdevLVR, "LoadTexture_Async failed for: " << renderModelName << std::endl);
				vr::VRRenderModels()->FreeRenderModel( model );
				return nullptr;
			}

			renderModel = new XdevLVRRenderModel( renderModelName );
			m_devices[deviceIndex].setRenderModel(renderModel);
			m_devices[deviceIndex].setName(renderModelName);
			m_devices[deviceIndex].setID(deviceIndex);
			m_devices[deviceIndex].setVRSystem(m_vrSystem);

//			m_controllers[deviceIndex]->setRenderModel(renderModel);
//			m_controllers[deviceIndex]->setName(renderModelName);
//			m_controllers[deviceIndex]->setID(deviceIndex);

			vr::VRRenderModels()->FreeRenderModel(model);
			vr::VRRenderModels()->FreeTexture(texture);
		}

		return renderModel;
	}

	IXdevLVRControllerAccesor XdevLVRSteamVR::getController(XdevLVRControllerHand controllerHand) {
		XdevLVRDevice** device = &m_devicesRole[(xdl_uint)controllerHand];
//		auto tmp = std::shared_ptr<XdevLVRDeviceImpl>(device);
		return IXdevLVRControllerAccesor(device);
	}

	void XdevLVRSteamVR::updateHMDPoseMatrices(xdl_uint32 deviceIndex) {
		if (!m_trackedPoses[deviceIndex].bPoseIsValid ) {
			return;
		}

		m_hmdPose = tmath::inverse(m_devicePoseMat[vr::k_unTrackedDeviceIndex_Hmd]);
	}

	void XdevLVRSteamVR::updateControllerMatrices(xdl_uint32 deviceIndex, xdl_uint32 controller) {
		if (!m_vrSystem->IsTrackedDeviceConnected( deviceIndex ) )
			return;

		if(!m_trackedPoses[deviceIndex].bPoseIsValid )
			return;

//		m_controllers[controller++]->setTransformation(&m_devicePoseMat[deviceIndex]);
//		m_controllers[deviceIndex]->setTransformation(&m_devicePoseMat[deviceIndex]);
	}

	void XdevLVRSteamVR::handleEvents() {
		vr::VREvent_t event;
		while (m_vrSystem->PollNextEvent(&event, sizeof(event))) {
			switch (event.eventType) {
				case vr::VREvent_TrackedDeviceActivated: {
					setupRenderModelForTrackedDevice( event.trackedDeviceIndex );
//					XDEVL_MODULEX_INFO(XdevLVR, "VR tracked device activated.\n");
				}
				break;
				case vr::VREvent_Quit:
					XDEVL_MODULEX_INFO(XdevLVR, "VR quit.\n");
					break;
				case vr::VREvent_InputFocusCaptured:
					XDEVL_MODULEX_INFO(XdevLVR, "VR Input focus captured.\n");
					break;
				case vr::VREvent_InputFocusReleased:
					XDEVL_MODULEX_INFO(XdevLVR, "VR Input focus released.\n");
					break;
				case vr::VREvent_TrackedDeviceUserInteractionStarted:
					XDEVL_MODULEX_INFO(XdevLVR, "VR User interaction started: " << event.trackedDeviceIndex  << std::endl);
					break;
				case vr::VREvent_TrackedDeviceUserInteractionEnded:
					XDEVL_MODULEX_INFO(XdevLVR, "VR User interaction ended: "  << event.trackedDeviceIndex  << std::endl);
					if ((event.trackedDeviceIndex == vr::k_unTrackedDeviceIndex_Hmd)) {
					}
					break;
				case vr::VREvent_ChaperoneDataHasChanged:
					XDEVL_MODULEX_INFO(XdevLVR, "VR Chaperone data has changed.\n");
					break;
				case vr::VREvent_ChaperoneUniverseHasChanged:
					XDEVL_MODULEX_INFO(XdevLVR, "VR Chaperone universe has changed.\n");
					break;
				case vr::VREvent_ChaperoneTempDataHasChanged:
					XDEVL_MODULEX_INFO(XdevLVR, "VR Chaperone temp data has changed.\n");
					break;
				case vr::VREvent_ChaperoneSettingsHaveChanged:
					XDEVL_MODULEX_INFO(XdevLVR, "VR Chaperone setting has changed.\n");
					if ((event.trackedDeviceIndex == vr::k_unTrackedDeviceIndex_Hmd)) {
					}
					break;
			}
		}
	}
}
