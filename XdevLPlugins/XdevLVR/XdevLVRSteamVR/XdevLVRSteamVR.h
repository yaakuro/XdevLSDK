/*
	Copyright (c) 2005 - 2016 Cengiz Terzibas

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in the
	Software without restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
	and to permit persons to whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be included in all copies
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
	FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.


*/

#ifndef XDEVL_VR_STEAMVR_H
#define XDEVL_VR_STEAMVR_H

#include <XdevLPluginImpl.h>
#include <XdevLVR/XdevLVR.h>
#include <XdevLRAI/XdevLTexture.h>

#include <openvr.h>

namespace xdl {

	static const XdevLString pluginName {
		"XdevLVR"
	};

	static const XdevLString description {
		"This plugins supports UI rendering using RAI."
	};

	static std::vector<XdevLModuleName>	moduleNames {
		XdevLModuleName("XdevLVR")
	};


	class XdevLVRDeviceImpl : public XdevLVRDevice {
		public:
			XdevLVRDeviceImpl();
			virtual ~XdevLVRDeviceImpl();

			xdl_uint32 getID() override final;
			XdevLString getName() override final;
			tmath::mat4 getPoseMatrix() override final;
			XdevLVRRenderModel* getRenderModel() override final;
			void triggerHapticPulse(xdl_uint32 axisID, xdl_uint usDurationMicroSec ) override final;

			void setID(xdl_uint32 id);
			void setName(const XdevLString& name);
			void setRenderModel(XdevLVRRenderModel* renderModel);
			void setTransformation(tmath::mat4* matrix);
			void setVRSystem(vr::IVRSystem* vrsystem);

		protected:

			vr::IVRSystem* m_vrSystem;
			xdl_uint32 m_id;
			XdevLString m_name;
			tmath::mat4* m_transformation;
			XdevLVRRenderModel* m_renderModel;

		public:

			xdl_bool buttons[vr::k_EButton_Max];
			vr::VRControllerAxis_t rAxis[6];

	};

	/**
	 * @class XdevLVRSteamVR
	 * @brief Implementation of the XdevLUI class using ImGUI and XdevLRAI.
	 */
	class XdevLVRSteamVR : public XdevLModuleAutoImpl<XdevLVR> {
		public:

			XdevLVRSteamVR(XdevLModuleCreateParameter* parameter, const XdevLModuleDescriptor& descriptor);

			virtual ~XdevLVRSteamVR();

			xdl_int init() override final;
			xdl_int shutdown() override final;
			xdl_int notify(XdevLEvent& event) override final;
			xdl_int update() override final;

			xdl_int create(IPXdevLVulkanContext context) override final;
			IPXdevLVRDevice getDevice(xdl_uint idx) override final;
			IXdevLVRControllerAccesor getController(XdevLVRControllerHand controllerHand) override final;

			xdl_int submitVulkan(XdevLVREye eye, const XdevLVRSubmitParameterVulkan& parameter) override final;
			xdl_int submit(const XdevLVRSubmitParameter& parameter) override final;
			const XdevLSizeUI& getRecommendedSize() const override final;
			tmath::mat4 getProjectionForLeftEye() override final;
			tmath::mat4 getProjectionForRightEye() override final;
			tmath::mat4 getPosLeftEye() override final;
			tmath::mat4 getPosRightEye() override final;
			tmath::mat4 getHMDPose() override final;
			tmath::mat4 getCurrentViewProjectionMatrixLeftEye() override final;
			tmath::mat4 getCurrentViewProjectionMatrixRightEye() override final;
			std::vector<XdevLString> getInstanceExtensionsRequired() override final;
			std::vector<XdevLString> getDeviceExtensionsRequired(VkPhysicalDevice physicalDevice) override final;
			void setNearFar(xdl_float nearClip, xdl_float farClip) override final;

		private:

			void updateHMDPoseMatrices(xdl_uint32 deviceIndex);
			void updateMatrices();
			void updateControllerMatrices(xdl_uint32 deviceIndex, xdl_uint32 controller);
			void updateControllerStates(xdl_uint32 deviceIndex);
			void handleEvents();

			void sendButtonPressedEvent(XdevLEvent ev, xdl_bool pressed, xdl_int16 controllerid, vr::EVRButtonId id);
			void sendTouchpadTouchedEvent(XdevLEvent ev, xdl_bool pressed, xdl_int16 controllerid, vr::EVRButtonId id);
			void sendTouchpadPressedEvent(XdevLEvent ev, xdl_bool pressed, xdl_int16 controllerid, vr::EVRButtonId id);


			XdevLString getTrackedDeviceString(vr::TrackedDeviceIndex_t deviceIndex, vr::TrackedDeviceProperty deviceProperty, vr::TrackedPropertyError *propertyError = NULL );
			void setupRenderModelForTrackedDevice( vr::TrackedDeviceIndex_t trackedDeviceIndex );
			XdevLVRRenderModel* findOrLoadRenderModel(vr::TrackedDeviceIndex_t deviceIndex, const XdevLString& pchRenderModelName );

		private:

			vr::IVRSystem* m_vrSystem;
			vr::IVRCompositor* m_compositor;
			vr::IVRRenderModels* m_renderModels;

			// Recommend witdh and height.
			XdevLSizeUI m_size;
			tmath::mat4 m_projectionLeftEye;
			tmath::mat4 m_projectionRightEye;
			tmath::mat4 m_poseLeftEye;
			tmath::mat4 m_poseRightEye;

			tmath::mat4 m_devicePoseMat[ vr::k_unMaxTrackedDeviceCount ];
			tmath::mat4 m_hmdPose;
			xdl_float m_nearClip;
			xdl_float m_farClip;
			vr::TrackedDevicePose_t m_trackedPoses[vr::k_unMaxTrackedDeviceCount];

//			std::vector< std::shared_ptr<XdevLVRDeviceImpl> > m_controllers;
			XdevLVRControllerHand m_controllerHand;
			vr::VRControllerState_t previousControllerStates[vr::k_unMaxTrackedDeviceCount];
			vr::VRControllerState_t m_vrControllerStates[vr::k_EButton_Max];
			std::vector<XdevLVRDeviceImpl> m_devices;
			std::vector<XdevLVRDevice*> m_devicesRole;
//			std::vector<XdevLVRRenderModel*> m_renderModelsInfo;
	};

}
#endif
