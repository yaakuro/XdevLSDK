#!/bin/bash

if [ ! -f XdevLExternalsLibs.tar.gz  ]; then
  wget --show-progress -q https://github.com/yaakuro/XdevLSDK/releases/download/0.7.0/XdevLExternalsLibs.tar.gz
fi

tar -xvf XdevLExternalsLibs.tar.gz -C XdevLExternals